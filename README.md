# ChannelFlow Boundary Condition package for revision v452

## Installation of the CFBC package
 
The package was implemented for Channelflow revision v447 and then v452.  
Firsly, you need to install `Channelflow`, then add the `Channelflow-controlled.git` repository within the source files.

## Commands for installation
 
```
cd ~
svn co http://svn.channelflow.org/channelflow
cd channelflow
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=~/channelflow ~/channelflow/trunk
make
make test
cd ~/channelflow
git init
git remote add origin https://claisse@bitbucket.org/claisse/channelflow_controlled.git
git fetch
rm trunk/CMakeLists.txt
   trunk/channelflow/tausolver.h
   trunk/channelflow/tausolver.cpp
   trunk/channelflow/dns.h
   trunk/channelflow/dns.cpp
   trunk/channelflow/Makefile.am
   trunk/channelflow/CMakeLists.txt
git checkout -t origin/master
cd build
make
make test
make install
```

## Possible errors
### HDF5_NOT_FOUND

If the cmake operation does not find the HDF5 libraries (`HDF5: HDF5_NOT_FOUND`), it is possible to give a hint to the method `FIND_HDF5` by adding the following flag to the cmake:
   
```bash
-DWITH_HDF5=~/anaconda3/lib
```
   
as in the below command line:  
```bash
cmake -DCMAKE_INSTALL_PREFIX=~/channelflow -DWITH_HDF5=~/anaconda3/lib ~/channelflow/trunk
```

### Undefined reference to H5::XXX

If you get an error in the make process such as
```bash
undefined reference to H5:Attribute:getName() const
```
check the following indication from `~/channelflow/trunk/CMakeLists.txt`:


> Set compiler flags.
> If you have link errors with undefined references to functions that are definitely in the libraries (e.g. undefined reference to H5::Attribute::getName() const) you might be running into changes in the C++ ABI, say having compiled channelflow with C++11 ABI but linking to HDF5 libs compiled with C++03 ABI. Try adding -D_GLIBCXX_USE_CXX11_ABI=0 to the add_definitions(...) for C flags immediately below. 

```
string(TOLOWER ${CMAKE_BUILD_TYPE} BUILD_TYPE)
message(STATUS "Build type: ${BUILD_TYPE}")
if(BUILD_TYPE STREQUAL release)
   add_definitions (-Wall -fomit-frame-pointer -DNDEBUG)
elseif(BUILD_TYPE STREQUAL debug)
   add_definitions (-Wall -fno-inline -g)
elseif(BUILD_TYPE STREQUAL profile)
   add_definitions (-Wall -pg -DDEBUG)
 else()
   message("Unknown build type ${CMAKE_BUILD_TYPE}")
 endif()
 ```

for iridis5

module load cmake
install anaconda
conda install -c salilab fftw
conda install -c omnia eigen3
conda install -c anaconda hdf5

change channelflow/trunk/CMakeLists.txt
for FIND_EIGEN3

change -D_GLIBCXX_USE_CXX11_ABI=0
