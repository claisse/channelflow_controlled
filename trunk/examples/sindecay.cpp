#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"

using namespace std;
using namespace channelflow;

int main() {

  cout << "================================================================\n";
  cout << "This program simulates a flow with a sinusoidal initial condition\n";
  cout << "and compares the DNS solution to the known analytic solution.\n";
  cout << "Take base flow U(y) == 0 and constrain the system to have zero\n";
  cout << "pressure gradient. For the initial condition \n\n";
  cout << "  u(x,y,z,0) = eps*sin(2*pi*ky*y[ny]/Ly),\n\n";
  cout << "the exact solution of the Navier-Stokes equation is\n\n";
  cout << "  u(x,y,z,t) = u(x,y,z,0)*exp(-nu*t*square(2*pi*ky/Ly))\n\n";
  cout << "In the following output,\n\n";
  cout << "  un == the numerical solution.\n";
  cout << "  ut == the true solution.\n\n";


  const int Nx=8;
  const int Ny=65;
  const int Nz=8;
  const int Nxd=(2*Nx)/3;
  const int Nyd=(2*Ny)/3;
  const int Nzd=(2*Nz)/3;

  char s = ' ';
  cout << "Nx  Ny  Nz == " << Nx << s << Ny << s << Nz << endl;
  cout << "Nxd Nyd Nzd == " << Nxd << s << Nyd << s << Nzd << endl;

  const int Nd=3;
  const Real Lx=2*pi;
  const Real Lz=2*pi;
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Ly= b-a;
  const Real Reynolds = 10.0;
  const Real nu = 1.0/Reynolds;
  const Real dt = 0.01;
  const Real T = 5;
  const int plotmod = 20;
  const int nSteps = int(T/dt);
  DNSFlags flags;
  flags.baseflow     = Zero;
  flags.timestepping = SBDF4;
  flags.constraint = PressureGradient;
  flags.dPdx = 0.0;

  // sinusoid perturbation params.
  const Real eps = 1;
  const int ky = 1;

  cout << setprecision(14);
  cout << "Nx Ny Nz Nd == " << Nx << ' ' << Ny << ' ' << Nz << ' ' << Nd << endl;
  cout << "Lx Ly Lz == " << Lx << " 2 "  << Lz << ' ' << endl;

  Vector y = chebypoints(Ny,a,b);
  y.save("y");
  ChebyTransform trans(Ny);

  ChebyCoeff sinusoid(Ny,a,b,Physical);
  for (int ny=0; ny<Ny; ++ny)
    sinusoid[ny] = eps*sin(2*pi*ky*y[ny]/Ly);
  sinusoid.makeSpectral(trans);

  FlowField un(Nx,Ny,Nz,Nd,Lx,Lz,a,b);
  for (int ny=0; ny<Ny; ++ny)
    un.cmplx(0,ny,0,0) = sinusoid[ny];

  FlowField ut = un;

  FlowField Pn(Nx,Ny,Nz,1,Lx,Lz,a,b);
  //FlowField vortn(Nx,Ny,Nz,Nd,Lx,Lz,a,b);
  //FlowField fn1(Nx,Ny,Nz,Nd,Lx,Lz,a,b);
  //FlowField fn(Nx,Ny,Nz,Nd,Lx,Lz,a,b);

  cout << "=================================================" << endl;
  cout << "Initial data, prior to time stepping" << endl;
  cout << "L2Norm(un)  == " << L2Norm(un) << endl;
  cout << "L2Norm(Pn)  == " << L2Norm(Pn) << endl;
  cout << "divNorm(un) == " << divNorm(un) << endl;
  cout << endl;
  DNS dns(un, nu, dt, flags);

  cout << "=================================================" << endl;
  cout << "Starting time stepping..." << endl;
  ofstream ns("unorms.asc");
  ns << setprecision(14);

  ofstream uts("ut.asc");
  uts << setprecision(14);
  uts << "% true solution ut(y,t), (y,t) == (columns, rows)" << endl;

  ofstream uns("un.asc");
  uns << setprecision(14);
  uns << "% numerical solution un(y,t), (y,t) == (columns, rows)" << endl;

  ofstream ts("t.asc");
  ts << "% time" << endl;

  for (int step=0; step<nSteps; step += plotmod) {
    Real t = dt*step;
    cout << "t == " << t << endl;
    ts << t << '\n';

    ut.setToZero();
    ut += sinusoid;
    ut *= exp(-nu*t *square(2*pi*ky/Ly));

    Real unnorm = L2Norm(un);
    Real utnorm = L2Norm(ut); // exp(-nu*t *square(2*pi*ky/Ly))*u0norm;
    Real udist  = L2Dist(ut,un);
    Real CFL = dns.CFL();

    ns << unnorm << ' ' << utnorm << ' ' << udist << ' '
       << divNorm(un) << ' ' << bcNorm(un) << '\n';

    cout << "CFL == " << CFL << endl;
    cout << "L2Norm(un) == " << unnorm << endl;
    cout << "L2Norm(ut) == " << utnorm << endl;
    cout << "L2Norm(un - ut)             == " << udist << endl;
    cout << "(L2Norm(un - ut)/L2Norm(ut) == " << udist/utnorm << endl;

    ChebyCoeff un00 = Re(un.profile(0,0,0));
    ChebyCoeff ut00 = Re(ut.profile(0,0,0));
    //ChebyCoeff ut00 = sinusoid;
    //ut00 *= exp(-nu*t *square(2*pi*ky/Ly));

    un00.makePhysical(trans);
    ut00.makePhysical(trans);

    for (int ny=0; ny<Ny; ++ny)
      uns << un00(ny) << ' ';
    uns << '\n';
    for (int ny=0; ny<Ny; ++ny)
      uts << ut00(ny) << ' ';
    uts << '\n';

    if (CFL > 2.0 || unnorm > 1) {
      cerr << "Problem!" << endl;
      cerr << "CFL  == " << CFL << endl;
      cerr << "norm == " << unnorm << endl;
      break;
    }

    dns.advance(un, Pn, plotmod);
    cout << endl;
  }
}
