#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "channelflow/dns.h"
#include "channelflow/complexdefs.h"

using namespace std;
using namespace channelflow;

const Real EPSILON=1e-12;
void RK3update(Complex& an, Complex lambda_v, Complex lambda_b, Real dt,
		int nsteps);

int main() {

  cout << "This program compares the growth of OrrSommerfeld eigenfunctions\n";
  cout << "integrated three ways\n";
  cout << " (1) direct numerical simulation (fully 3d with nonlinearity)\n";
  cout << " (2) analytic expression exp(-i omega t) * OS eigenfunction\n";
  cout << " (3) an 1d ODE integration of the linearized equations\n";
  cout << "The DNS is represented by  FlowField un (u nonlinear)\n";
  cout << "The analytic expression by FlowField ul (u linear)\n";
  cout << "The ODE is represented by a Complex ark (a runge-kutta)\n";
  cout << "Admittedly the names of these variables are poorly chosen.\n\n";

  cout << "To compare the DNS and analytic expr to the ODE integration,\n";
  cout << "we compute the inner product of un and ul with the OS eigenfunc\n";
  cout << "Complex an = L2InnerProduct(un, u_oseig);\n";
  cout << "Complex al = L2InnerProduct(ul, u_oseig);\n";
  cout << "If the three integration methods are identical, an == al == ark.\n";
  cout << endl << endl;

  fftw_loadwisdom();

  const int Nx=8;
  const int Ny=65;
  const int Nz=4;
  const int Nd=3;
  const Real Lx=2*pi;
  const Real Lz=2*pi;
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Reynolds = 7500.0;
  const Real nu = 1.0/Reynolds;
  const Real t0 = 0.00;
  const Real  T = 0.09;
  const int   N = 1;  // plot results at interval N dt
  const Real dt = 0.01;

  const bool plotmodes = false;
  const bool pause = true;

  DNSFlags flags;
  flags.nonlinearity = LinearAboutProfile;
  flags.constraint = BulkVelocity;
  flags.timestepping = SBDF3;
  flags.initstepping = SMRK2;
  flags.dPdx = -2.0*nu;    // mean streamwise pressure gradient.
  flags.Ubulk = 2.0/3.0;   // mean bulk velocity

  Real scale =  0.01;
  const char s = ' ';

  cout << setprecision(17);
  ChebyTransform trans(Ny);
  Vector y = chebypoints(Ny,a,b);
  y.save("y");

  // Get Orr-Sommerfeld eigenfunction from file. The data is the
  // y-dependence of the eigenfunction's (u,v) components. x dependence
  // is exp(2 pi i x / Lx). Eigfunction is const in z. Reconstruct
  // w from velocity field, pressure from OS eqns.
  BasisFunc ueig("../data/os_ueig10_65");
  BasisFunc peig("../data/os_peig10_65");
  ueig.makeSpectral(trans);
  peig.makeSpectral(trans);

  cout << "divNorm(ueig)      == " << divNorm(ueig) << endl;
  cout << "L2Norm(ueig)       == " << L2Norm(ueig) << endl;

  BasisFunc unit_ueig = ueig;
  BasisFunc unit_peig = peig;
  unit_ueig *= 1.0/L2Norm(ueig);
  unit_peig *= 1.0/L2Norm(peig);
  BasisFunc unit_ueig_conj = conjugate(ueig);
  BasisFunc unit_peig_conj = conjugate(peig);

  cout << "divNorm(unit_ueig)      == " << divNorm(unit_ueig) << endl;
  cout << "divNorm(unit_ueig_conj) == " << divNorm(unit_ueig_conj) << endl;
  cout << "L2Norm(unit_ueig)       == " << L2Norm(unit_ueig) << endl;
  cout << "L2Norm(unit_ueig_conj)  == " << L2Norm(unit_ueig_conj) << endl;

  // The temporal variation of the eigenfunction is exp(lambda t)
  Complex omega = 0.24989153647208251 + I*0.0022349757548207664;
  Complex lambda = -1.0*I*omega;

  // lambda_v == the component of the eigenvalue due to viscosity
  // lambda_b == the component of the eigenvalue due to the base flow
  // This is useful for integrating the eigenfunction numerically with
  // the same operator-splitting algorithm as the DNS.
  Complex lambda_v(-0.006564724662891, 0.0);
  Complex lambda_b = lambda - lambda_v;

  cout << "   omega = " << omega << endl;
  cout << "  lambda = " << lambda << endl;

  ChebyCoeff Ubase(Ny,a,b,Physical);
  for (int ny=0; ny<Ny; ++ny)
    Ubase[ny] = 1.0 - square(y[ny]);

  FlowField un(Nx,Ny,Nz,Nd,Lx,Lz,a,b);  // numerical velocity field
  FlowField pn(Nx,Ny,Nz,1,Lx,Lz,a,b);   // numerical pressure field
  FlowField ul(Nx,Ny,Nz,Nd,Lx,Lz,a,b);  // linear-solution velocity
  FlowField pl(Nx,Ny,Nz,1,Lx,Lz,a,b);   // linear-solution pressure

  BasisFunc scaled_ueig(ueig);
  BasisFunc scaled_peig(peig);
  BasisFunc scaled_ueig_conj = conjugate(ueig);
  BasisFunc scaled_peig_conj = conjugate(peig);

  // Calculate L2Norm of poisseuille base flow and perturbation field
  // and rescale so that perturb/poisseuille is equal to the given scale.
  un.setState(Spectral, Physical);
  un += Ubase;
  un.makeSpectral();
  Real uPoissNorm = L2Norm(un);
  un.setToZero();
  un += scaled_ueig;
  un += scaled_ueig_conj;
  Real uPerturbNorm = L2Norm(un);
  Real rescale = scale*uPoissNorm/uPerturbNorm;
  scaled_ueig *= rescale;
  scaled_peig *= rescale;
  scaled_ueig_conj *= rescale;
  scaled_peig_conj *= rescale;

  // Reset perturbation velocity and pressure for t=0, numerical and linear
  un.setToZero();
  ul.setToZero();
  pn.setToZero();
  pl.setToZero();

  un += scaled_ueig;
  un += scaled_ueig_conj;
  ul += scaled_ueig;
  ul += scaled_ueig_conj;

  pn += scaled_peig;
  pn += scaled_peig_conj;
  pl += scaled_peig;
  pl += scaled_peig_conj;

  cout << "divNorm(scaled_ueig)      == " << divNorm(scaled_ueig) << endl;
  cout << "divNorm(scaled_ueig_conj) == " << divNorm(scaled_ueig_conj) << endl;
  cout << "L2Norm(scaled_ueig)       == " << L2Norm(scaled_ueig) << endl;
  cout << "L2Norm(scaled_ueig_conj)  == " << L2Norm(scaled_ueig_conj) << endl;

  cout << "building DNS..." << flush;
  DNS dns(un, Ubase, nu, dt, flags);
  cout << "done" << endl;

  Real CFL = dns.CFL();
  Real u0norm   = L2Norm(un);
  Complex L2IP0 = L2InnerProduct(un, unit_ueig);
  Complex ark   = L2InnerProduct(ul, unit_ueig); // coeff of u from RK integ.
  Complex brk   = L2InnerProduct(pl, unit_peig); // coeff of p from RK integ.

  ofstream norms("norms.asc");
  ofstream times("t.asc");
  ofstream unrs("unr.asc");
  ofstream unis("uni.asc");
  ofstream vnrs("vnr.asc");
  ofstream vnis("vni.asc");
  ofstream pnrs("pnr.asc");
  ofstream pnis("pni.asc");
  ofstream ulrs("ulr.asc");
  ofstream ulis("uli.asc");
  ofstream vlrs("vlr.asc");
  ofstream vlis("vli.asc");
  ofstream plrs("plr.asc");
  ofstream plis("pli.asc");

  unrs << "% real part of u numerical (DNS)\n" << setprecision(17);
  unis << "% imag part of u numerical (DNS)\n" << setprecision(17);
  vnrs << "% real part of v numerical (DNS)\n" << setprecision(17);
  vnis << "% imag part of v numerical (DNS)\n" << setprecision(17);
  pnrs << "% real part of p numerical (DNS)\n" << setprecision(17);
  pnis << "% imag part of p numerical (DNS)\n" << setprecision(17);
  ulrs << "% real part of u linear (analytic solution)\n" << setprecision(17);
  ulis << "% imag part of u linear (analytic solution)\n" << setprecision(17);
  vlrs << "% real part of v linear (analytic solution)\n" << setprecision(17);
  vlis << "% imag part of v linear (analytic solution)\n" << setprecision(17);
  plrs << "% real part of p linear (analytic solution)\n" << setprecision(17);
  plis << "% imag part of p linear (analytic solution)\n" << setprecision(17);

  for (Real t=t0; t<=T; t += N*dt) {
    cout << "================================================" << endl;
    cout << "t == " << t << endl;

    Real unnorm = L2Norm(un);
    Real ulnorm = L2Norm(ul);
    Real pnnorm = L2Norm(pl);
    Real plnorm = L2Norm(pl);
    Real udist  = L2Dist(un,ul);
    Real pdist  = L2Dist(pn,pl);

    cout << "CFL == " << CFL << endl;
    cout << "Norms\n";
    cout << "divNorm(un) == " << divNorm(un) << endl;
    cout << "divNorm(ul) == " << divNorm(ul) << endl;
    cout << "L2Norm(un)  == " << L2Norm(un) << endl;
    cout << "L2Norm(ul)  == " << L2Norm(ul) << endl;
    cout << "L2Norm(pn)  == " << L2Norm(pn) << endl;
    cout << "L2Norm(pl)  == " << L2Norm(pl) << endl;
    cout << "divNorm(un)/L2Norm(un)    == " << divNorm(un)/L2Norm(un) << endl;
    cout << "L2Dist(un,ul)/L2Norm(ul)  == " << L2Dist(un, ul)/ulnorm <<endl;
    cout << "L2Dist(pn,pl)/L2Norm(pl)  == " << L2Dist(pn, pl)/plnorm <<endl;

    Complex an = L2InnerProduct(un, unit_ueig);
    Complex al = L2InnerProduct(ul, unit_ueig);
    Complex bn = L2InnerProduct(pn, unit_peig);
    Complex bl = L2InnerProduct(pl, unit_peig);

    cout << "Projection of velocity fields onto a fixed unit orr-somm eigenfunction\n";
    cout << "al  == " << al << endl;
    cout << "ark == " << ark << endl;
    cout << "an  == " << an << endl;
    cout << "arg al  == " << arg(al) << endl;
    cout << "arg ark == " << arg(ark) << endl;
    cout << "arg an  == " << arg(an) << endl;
    cout << "(arg(an)-arg(al)) == " << (arg(an)-arg(al)) << endl;
    cout << "abs al  == " << abs(al) << endl;
    cout << "abs ark == " << abs(ark) << endl;
    cout << "abs an  == " << abs(an) << endl;
    cout << "(abs(an)-abs(al))/abs(al) == " << (abs(an)-abs(al))/abs(al) << endl;
    cout << "Projection of pressure fields onto a fixed unit orr-somm eigenfunction\n";
    cout << "bl  == " << bl << endl;
    cout << "brk == " << brk << endl;
    cout << "bn  == " << bn << endl;
    cout << "arg bl  == " << arg(bl) << endl;
    cout << "arg brk == " << arg(brk) << endl;
    cout << "arg bn  == " << arg(bn) << endl;
    cout << "(arg(bn)-arg(bl)) == " << (arg(bn)-arg(bl)) << endl;
    cout << "abs bl  == " << abs(bl) << endl;
    cout << "abs brk == " << abs(brk) << endl;
    cout << "abs bn  == " << abs(bn) << endl;
    cout << "(abs(bn)-abs(bl))/abs(bl) == " << (abs(bn)-abs(bl))/abs(bl) << endl;

    Complex L2IP = L2InnerProduct(un, unit_ueig);
    cout << "L2IP(u,os)/L2(u0,os) == " << L2IP/L2IP0  << endl;
    cout << "exp(lambda*t)        == " << exp(lambda*t) << endl;
    cout << "lambda      == " << lambda <<endl;
    cout << "lambda appr == " << log(L2IP/L2IP0)/t <<endl;
    cout << "growth rate ==  "<< log(unnorm/u0norm)/t << endl;

    if (plotmodes) {
      for (int kx=0; kx<=2; ++kx) {
	int mx = ul.mx(kx);
	for (int kz=0; kz<=2; ++kz) {
	  int mz = ul.mz(kz);
	  ul.saveProfile(mx, mz, string("ul")+i2s(kx)+i2s(kz), trans);
	  un.saveProfile(mx, mz, string("un")+i2s(kx)+i2s(kz), trans);
	  pl.saveProfile(mx, mz, string("pl")+i2s(kx)+i2s(kz), trans);
	  pn.saveProfile(mx, mz, string("pn")+i2s(kx)+i2s(kz), trans);

	  // Extract the 0-component of kx,kz p profile, transform, and save
	  ComplexChebyCoeff pn_profile = pn.profile(mx, mz, 0);
	  pn_profile.makePhysical(trans);
	  pn_profile.save(string("pn")+i2s(kx)+i2s(kz));
	}
      }
    }

    times << t << endl;
    norms << unnorm << s << ulnorm << s << udist << s
	  << pnnorm << s << plnorm << s << pdist << endl;

    // Save velocity and pressure data to files
    BasisFunc un10 = un.profile(1,0);
    BasisFunc ul10 = ul.profile(1,0);
    ComplexChebyCoeff pn10 = pn.profile(1,0,0);
    ComplexChebyCoeff pl10 = pl.profile(1,0,0);
    un10.makePhysical();
    ul10.makePhysical();
    pn10.makePhysical();
    pl10.makePhysical();

    for (int ny=0; ny<Ny; ++ny) {
      unrs << Re(un10[0][ny]) << s;
      unis << Im(un10[0][ny]) << s;
      vnrs << Re(un10[1][ny]) << s;
      vnis << Im(un10[1][ny]) << s;
      pnrs << Re(pn10[ny]) << s;
      pnis << Im(pn10[ny]) << s;
      ulrs << Re(ul10[0][ny]) << s;
      ulis << Im(ul10[0][ny]) << s;
      vlrs << Re(ul10[1][ny]) << s;
      vlis << Im(ul10[1][ny]) << s;
      plrs << Re(pl10[ny]) << s;
      plis << Im(pl10[ny]) << s;
    }
    unrs << endl;
    unis << endl;
    vnrs << endl;
    vnis << endl;
    pnrs << endl;
    pnis << endl;
    ulrs << endl;
    ulis << endl;
    vlrs << endl;
    vlis << endl;
    plrs << endl;
    plis << endl;

    // time integration for everybody
    if (pause) {
      cout << "hit return to continue..." << flush;
      char buff[10];
      cin.getline(buff,10);
    }
    ul.setToZero();
    pl.setToZero();

    BasisFunc exp_ueig(scaled_ueig);  // exponentiated velocity eigenfunc
    BasisFunc exp_peig(scaled_peig);
    exp_ueig *= exp(lambda*(t+N*dt));
    exp_peig *= exp(lambda*(t+N*dt));

    BasisFunc exp_ueig_conj = conjugate(scaled_ueig);
    BasisFunc exp_peig_conj = conjugate(scaled_peig);

    ul += exp_ueig;
    ul += exp_ueig_conj;
    pl += exp_peig;
    pl += exp_peig_conj;

    RK3update(ark, lambda_v, lambda_b, dt, N);
    RK3update(brk, lambda_v, lambda_b, dt, N);

    dns.advance(un, pn, N);
    cout << endl;
    Real CFL = dns.CFL();
    if (CFL > 2.0 || unnorm > 10) {
      cerr << "DNS trouble: CFL==" << CFL << " L2Norm(un)==" << unnorm << endl;
      exit(1);
    }
  }
  fftw_savewisdom();
}

Real alpha[] = {29.0/96.0, -3.0/40.0, 1.0/6.0};
Real beta[]  = {37.0/160.0, 5.0/24.0, 1.0/6.0};
Real gamm[]  = {8.0/15.0, 5.0/12.0, 3.0/4.0};
Real zeta[]  = {0.0, -17.0/60.0, -5.0/12.0};

void RK3update(Complex& an, Complex lambda_v, Complex lambda_b, Real dt, int steps) {
  for (int n=0; n<steps; ++n) {
    Complex an1(0.0, 0.0);
    Complex tmp;
    for (int i=0; i<3; ++i) {
      tmp = an;
      an =
	((1.0+dt*alpha[i]*lambda_v)*an + dt*lambda_b*(gamm[i]*an + zeta[i]*an1))
	/ (1.0-dt*beta[i]*lambda_v);
      an1 = tmp;
    }
  }
}
