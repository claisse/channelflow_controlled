#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"

using namespace std;
using namespace channelflow;

// Start a pressure-driven Poisseuille flow with either 
// U(y) == 0      and u(x,y,z) == 1-y^2 or
// U(y) == 1-y^20 and u(x,y,z) == 0
// Integrate and verify that nothing changes
// 
// Or start U and u from zero and check convergence onto utot = 1-y^2 

int main() {

    const int Nx=8;
    const int Ny=33;
    const int Nz=8;

    const int Nd=3;
    const Real Lx=2*pi;
    const Real Lz=2*pi;
    const Real a= -1.0;
    const Real b=  1.0;
    const Real Reynolds = 400.0;
    const Real nu = 1.0/Reynolds;
    const Real dPdx0 = -2.0*nu;    // mean streamwise pressure gradient.
    const Real Ubulk0 = 2.0/3.0; 
    const Real dt = 0.1;
    const int  n = 10;
    const Real T0 = 0.0;
    const Real T1 = 100.0;

    // utot = Ubase + un
    // baseflow == true   =>  Ubase,un == parabola,zero
    // baseflow == false  =>  Ubase,un == zero,parabola,
    const bool baseflow = false;  

    DNSFlags flags;
    flags.timestepping = SBDF2;
    flags.constraint = BulkVelocity;
    flags.dealiasing = DealiasXZ;
    flags.nonlinearity = Rotational;
    flags.Ubulk = Ubulk0;
    flags.dPdx = dPdx0;
    flags.dPdx  = 0.0;
    flags.nu    = 1.0/Reynolds;
    flags.dt    = dt;

    cout << setprecision(14);
    cout << "Nx Ny Nz Nd == " << Nx << ' ' << Ny << ' ' << Nz << ' ' << Nd << endl;
    cout << "Lx Ly Lz == " << Lx << " 2 "  << Lz << ' ' << endl;

    Vector y = chebypoints(Ny,a,b);
    y.save("y");
    ChebyTransform trans(Ny);

    // Build DNS
    ChebyCoeff zero(Ny,a,b,Spectral);
    ChebyCoeff parabola(Ny,a,b,Spectral);
    parabola[0] =  0.5;
    parabola[2] = -0.5;

    FlowField un(Nx,Ny,Nz,Nd,Lx,Lz,a,b);

    ChebyCoeff Ubase = (baseflow) ? parabola : zero;
    ChebyCoeff uprof = (baseflow) ? zero : parabola;
    un += uprof;

    FlowField ut = un;
    FlowField qn(Nx,Ny,Nz,1,Lx,Lz,a,b);

    cout << "building DNS..." << flush;
    DNS dns(un, Ubase, nu, dt, flags, T0);
    cout << "done" << endl;

    cout << "========================================================" << endl;
    cout << "Initial data, prior to time stepping" << endl;
    cout << "L2Norm(un)    == " << L2Norm(un) << endl;
    cout << "divNorm(un)   == " << divNorm(un) << endl;  
    cout << "L2Norm(qn)    == " << L2Norm(qn) << endl;
    cout << "........................................................" << endl;
    cout << endl;

    cout << "======================================================" << endl;
    cout << "Starting time stepping..." << endl;
    ofstream ns("unorms.asc");
    ns << setprecision(14);

    ofstream uts("ut.asc");
    uts << setprecision(14);
    uts << "% true solution ut(y,t), (y,t) == (columns, rows)" << endl;

    ofstream uns("un.asc");
    uns << setprecision(14);
    uns << "% numerical solution un(y,t), (y,t) == (columns, rows)" << endl;

    ofstream ts("t.asc");
    ts << "% time" << endl;

    Ubase.save("Ubase");

    for (Real t=T0; t<T1; t += n*dt) {
        cout << "t == " << t << endl;
        cout << "CFL == " << dns.CFL() << endl;
        cout << "L2Norm(un)    == " << L2Norm(un) << endl;
        cout << "L2Dist(un,ut) == " << L2Dist(un,ut) << endl;
        cout << "dPdx  == " << dns.dPdx() << endl;
        cout << "Ubulk == " << dns.Ubulk() << endl;

        ns << L2Dist(un,ut) << ' ' 
            << fabs(dPdx0 - dns.dPdx()) << ' '
            << fabs(Ubulk0 - dns.Ubulk()) << ' '
            << divNorm(un) << ' ' << bcNorm(un) << '\n';

        ChebyCoeff un00 = Re(un.profile(0,0,0));
        ChebyCoeff ut00 = Re(ut.profile(0,0,0));

        un00.makePhysical(trans);
        ut00.makePhysical(trans);

        for (int ny=0; ny<Ny; ++ny)
            uns << un00(ny) << ' ';
        uns << '\n';
        for (int ny=0; ny<Ny; ++ny)
            uts << ut00(ny) << ' ';
        uts << '\n';

        ts << t << '\n';

        dns.advance(un, qn, n);
        cout << endl;
    }
}

