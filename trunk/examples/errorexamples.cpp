#include <iostream>
#include <iomanip>
#include "channelflow/dns.h"
#include "channelflow/flowfield.h"

using namespace std;
using namespace channelflow;

// Compile with debugging flags to see how errors are caught:
//    make errorexamples.dx; ./errorexamples.dx

// Compile with optimizing flags to see what happens without error checks:
//    make errorexamples.x; ./errorexamples.x


int main() {

  // Define gridsize
  const int Nx=24;
  const int Ny=33;
  const int Nz=24;

  // Define box size
  const Real Lx=1.75*pi;
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Lz=1.2*pi;

  // Constructing flow fields
  FlowField u(Nx,Ny,Nz,  3,Lx,Lz,a,b,Physical,Physical);
  FlowField v(Nx,Ny,Nz-1,3,Lx,Lz,a,b,Physical,Physical);

  cout << "(deliberately causing an index-out-of-bounds error...)" << endl;
  u(0,Ny+2,0,0) = 0.0;

  cout << "(deliberately causing a field-state error...)" << endl;
  u.cmplx(0,0,0,0) = Complex(0.0, 0.0);

  cout << "(deliberately causing a field incompatibility error...)" << endl;
  u += v;

}
