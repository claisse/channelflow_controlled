//
// controller.h
//
// Created by:
//
// Geoffroy Claisse
// University of Southampton, 08/2018
//
// Peter Heins on 27/02/2012.
// Copyright 2012 University of Sheffield. All rights reserved.
//
#ifndef CHANNELFLOW_CONTROLLER_H
#define CHANNELFLOW_CONTROLLER_H

#include <iostream>
#include <Eigen/Dense>
#include "channelflow/flowfield.h"
//#include "channelflow/utilfuncs.h"

extern "C" {
#include <atlas/clapack.h>
#include <atlas/cblas.h>
}

namespace channelflow {

typedef double Real;

//############################################################
// Created by Geoffroy Claisse
// University of Southampton, 08/2018
//############################################################

// NotImplemented exception handling class
class NotImplementedException : public std::logic_error {
    public:
        NotImplementedException() : logic_error("Function not implemented yet") {};
};

// Reads matrices in hdf5 format
int MatIn_hdf5(
        double** matrix,
        const size_t N1,
        const size_t N2,
        const std::string& filename);

// Reads real (and real only) matrices/vectors in hdf5 format
int MatIn_hdf5_real(
        Eigen::MatrixXcd& matrix,
        const std::string& filename);
int MatIn_hdf5_real(
        Eigen::VectorXcd& vector,
        const std::string& filename);

// Reads complex (and complex only) matrices/vectors in hdf5 format
int MatIn_hdf5_complex(
        Eigen::MatrixXcd& matrix,
        const std::string& filename);
int MatIn_hdf5_complex(
        Eigen::VectorXcd& vector,
        const std::string& filename);

//############################################################
// Created by Peter Heins on 27/02/2012.
// Copyright 2012 University of Sheffield. All rights reserved.
//############################################################

// General functions
double** MatIn(const char* FileName);           // Reads matrices in binary format
double** MatIn_asc(const char* FileName);       // Reads matrices in ascii format

int NumRow(const char* FileName);               // Outputs no. of rows of matrix
int NumCol(const char* FileName);               // Outputs no. of columns of matrix
double* MatVec(
        double** Mat,
        double Vec[],
        int Matrow,
        int Matcol);         // Performs matrix-vector multiplication
void gauss(double** A, double* B, int N);                                   // Performs Gauss-Seidel iteration
double** eyeF(int numCstates);                                              // Outputs identity matrix

//############################################################
// CONTROLLER MOTHER CLASS
// Created by Geoffroy Claisse
// University of Southampton, 08/2018
//############################################################

class Controller {
    public:
        virtual double** CStateInfo();      // 2D Array for no. of controller states for controlled wavenumber pairs
        virtual double*** ConStates();      // 3D Array containing controller states for controlled wavenumber pairs
        virtual int kx_c(int mx_c);

        // EMPTY // Advances discrete-time controller forward in time with fixed time-step dT
        virtual void advance_Con(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO);

        // EMPTY // Advances continuous-time controller forward in time with variable time-step dT
        virtual void advance_Con_CON(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO);
};

//############################################################
// CONTROLLER_HEINS CLASSinherited from Controller class
// Created by Peter Heins on 27/02/2012.
// Copyright 2012 University of Sheffield. All rights reserved.
//############################################################

class Controller_Heins : public Controller {
    public:
        // Creates controller object
        Controller_Heins();
        Controller_Heins(int Ny, int minKx, int maxKx, int minKz, int maxKz, int uvw, const char* StateInfoFile, int NumConInputs, double tau, bool Spectral_states);

        double** CStateInfo();      // 2D Array for no. of controller states for controlled wavenumber pairs
        double*** ConStates();      // 3D Arra containing controller states for controlled wavenumber pairs
        int kx_c(int mx_c);

        // Advances discrete-time controller forward in time with fixed time-step dT
        void advance_Con(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO);

        // Advances continuous-time controller forward in time with variable time-step dT
        void advance_Con_CON(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO);

    protected:
        int Ny_;       // No. of simulation wall-normal gridpoints
        int minKx_;    // Minimum kx to control
        int maxKx_;    // Maximum kx to control
        int minKz_;    // Minimum kz to control
        int maxKz_;    // Maximum kz to control
        int uvw_;      // Actuation via u, v or w
        const char* StateInfoFile_;     // Filename for file containing information of no. of controller states for each controlled wavenumber pair
        int NumConInputs_;              // No. of inputs INTO the controller, i.e. flow measurements
        double tau_;                    // Actuator time-constant
        bool Spectral_states_;          // States physical or Chebyshev spectral state
};

//############################################################
// CONTROLLER K CLASS inherited from Controller class
// Controller with constant K gain
// Created by Geoffroy Claisse
// University of Southampton, 08/2018
//############################################################

class Controller_K : public Controller {
    protected:
        // parameters to set at construction
        // here we care about the number of modes as we want them to be equal
        // for the present ChannelFlow Controller_K and in the matrix K.
        const int _Nd_C;
        const int _Mx_C; // streamwise modes
        const int _My_C; // wall-normal modes
        const int _Mz_C; // spanwise modes
        const int _M_C_in; // nb of controller input state (FlowField state)
        // number of controller output state: the actuation at the BCs
        // ATTENTION: IT DOES ONLY CONSIDER WALL-NORMAL VELOCITY ACTUATION
        // ON >>ALL<< THE MODES (kx, kz) < (_Mx_C, _Mz_C) (no reduction)
        const int _M_C_out;
        // folder containing K and Cs
        const std::string _matrix_filename;
        // file containing the baseflow targeted by the controller
        // used to removed from the FlowField u of channelflow
        // before applying control of the perturbation only
        const std::string _target_baseflow_filename;
        double _tau;
        Eigen::MatrixXcd K;
        Eigen::MatrixXcd Cs;
        FlowField _target_baseflow_flowfield;

    public:
        Controller_K();
        Controller_K(
                int Mx_C,
                int My_C,
                int Mz_C,
                std::string matrix_filename,
                std::string target_baseflow_filename,
                double tau);

        void test();

        // Advances discrete-time controller forward in time with fixed time-step dT
        void advance_Con(FlowField& u, FlowField& p, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO);

        // Advances continuous-time controller forward in time with variable time-step dT
        void advance_Con_CON(FlowField& u, FlowField& p, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO);

        // Advances continuous-time controller forward in time with variable time-step dT
        //void advance_Con_CN(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO);

        void FlowField_to_EigenVector(FlowField& ff, Eigen::VectorXcd& eigenVector);
        void EigenVector_to_FlowField(Eigen::VectorXcd& eigenVector, FlowField& ff);
        void changegrid(FlowField& u_in, FlowField& u_out, bool fixdiv=true, bool padded=true);
        void substract_ff(FlowField& u, FlowField& ff_to_substract);
        void add_ff(FlowField& u, FlowField& ff_to_add);

};

}


#endif
