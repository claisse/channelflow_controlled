//
// controller.cpp
//
// Created by:
//
// Geoffroy Claisse
// University of Southampton, 08/2018
//
// Peter Heins on 27/02/2012.
// Copyright 2012 University of Sheffield. All rights reserved.
//
#include <iostream>
#include <sstream>
#include <string.h>
#include <Eigen/Dense>

#include "config.h"
#include "channelflow/controller.h"

#ifdef HDF5_FOUND // set by cmake in config.h
#include "H5Cpp.h"
#define HAVE_HDF5 1
#else
#define HAVE_HDF5 0
#endif

using namespace std;

namespace channelflow
{
    //############################################################
    // Created by Geoffroy Claisse
    // University of Southampton, 08/2018
    //############################################################

    #ifdef HAVE_HDF5
    // read the matrix dataset 'mat' in 'h5file' and store it to 'matrix'
    int hdf5read(
            double** matrix,
            const size_t N0,
            const size_t N1,
            H5::H5File& h5file);
    int hdf5read_real(
            Eigen::MatrixXcd& matrix,
            H5::H5File& h5file);
    int hdf5read_real(
            Eigen::VectorXcd& vector,
            H5::H5File& h5file);
    int hdf5read_complex(
            Eigen::MatrixXcd& matrix,
            H5::H5File& h5file);
    int hdf5read_complex(
            Eigen::VectorXcd& vector,
            H5::H5File& h5file);
    #endif // HAVE_HDF5

    int MatIn_hdf5(
            double **matrix,
            const size_t N0,
            const size_t N1,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read(matrix, N0, N1, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }

    int MatIn_hdf5_real(
            Eigen::MatrixXcd& matrix,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read_real(matrix, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }

    int MatIn_hdf5_real(
            Eigen::VectorXcd& vector,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read_real(vector, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }

    int MatIn_hdf5_complex(
            Eigen::MatrixXcd& matrix,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read_complex(matrix, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }

    int MatIn_hdf5_complex(
            Eigen::VectorXcd& vector,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read_complex(vector, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }


    //############################################################
    // Created by Peter Heins on 27/02/2012.
    // Copyright 2012 University of Sheffield. All rights reserved.
    //############################################################

    // Reads in matrix from file
    double** MatIn(const char* FileName) {
        double* memblock;
        int size;

        ifstream Matfile(FileName, ios_base::in|ios_base::binary|ios::ate);
        if (Matfile.is_open()) {

            size = (int) Matfile.tellg();
            memblock = new double[size];
            Matfile.seekg(0,ios::beg);
            Matfile.read((char*)memblock,size);
            Matfile.close();

            // read in matrices' dimensions
            int num_row = iround(memblock[0]);
            int num_col = iround(memblock[1]);


            double **Mat;
            Mat = new double*[num_row];
            for (int i=0;i<num_row;++i){
                Mat[i] = new double[num_col];
            }

            int Vecsize = num_row*num_col;
            double Vec[Vecsize];

            for (int i=0; i<Vecsize; ++i) { // vector
                Vec[i] = memblock[i+2];
            }

            int count =0;
            for (int row=0; row<num_row; ++row) {
                for (int col=0; col<num_col; ++col) { // matrix
                    Mat[row][col] = Vec[count];
                    ++count;
                }
            }

            delete[] memblock;
            return Mat;
            //delete[] Mat;

        }
        else {
            cout << "Unable to open file" << endl;
            return 0;
        }
    }

    double** MatIn_asc(const char* FileName) {
        fstream Matfile(FileName, ios_base::in|ios_base::out);
        Matfile.seekg(0);

        // read in matrices' dimensions
        int num_row;
        int num_col;
        Matfile >> num_row >> num_col;

        double **Mat;
        Mat = new double*[num_row];
        for (int i=0;i<num_row;++i){
            Mat[i] = new double[num_col];
        }

        int Vecsize = num_row*num_col;
        double Vec[Vecsize];

        for (int i=0; i<Vecsize; ++i) { // vector
            Matfile >> Vec[i];
        }

        int count =0;
        for (int row=0; row<num_row; ++row) {
            for (int col=0; col<num_col; ++col) { // matrix
                Mat[row][col] = Vec[count];
                ++count;
            }
        }
        return Mat;
    }

    // Reads in number of rows of matrix in file
    int NumRow(const char* FileName) {
        double* memblock;
        int size;

        ifstream Matfile(FileName, ios_base::in|ios_base::binary|ios::ate);
        size = (int) Matfile.tellg();
        memblock = new double[size];
        Matfile.seekg(0,ios::beg);
        Matfile.read((char*)memblock,size);
        Matfile.close();

        // read in matrices' dimensions
        int num_row = iround(memblock[0]);

        delete[] memblock;

        return num_row;

    }
    // Reads in number of columnss of matrix in file
    int NumCol(const char* FileName) {
        double* memblock;
        int size;

        ifstream Matfile(FileName, ios_base::in|ios_base::binary|ios::ate);
        size = (int) Matfile.tellg();
        memblock = new double[size];
        Matfile.seekg(0,ios::beg);
        Matfile.read((char*)memblock,size);
        Matfile.close();

        // read in matrices' dimensions
        int num_col = iround(memblock[1]);


        delete[] memblock;

        return num_col;

    }
    // Multiplies matrix with vector
    double* MatVec(double** Mat, double Vec[], int Matrow, int Matcol) {
        double *Result;
        Result = new double[Matrow];

        for (int i=0;i<Matrow;++i){ // Initialise result vector
            Result[i] = 0.0;
        }

        double resultmat[Matrow][Matcol];
        for (int j=0;j<Matcol;++j) {
            for (int i=0;i<Matrow;++i) {
                resultmat[i][j] = Mat[i][j]*Vec[j];
            }
        }

        for (int i=0;i<Matrow;++i) {
            for (int j=0;j<Matcol;++j) {
                Result[i] += resultmat[i][j] ;
            }
        }
        return Result;
        //delete[] Result;
    }

    // Solves Ax=B for x
    void gauss(double** A, double* B, int N){
        float* alpha;
        alpha = new float[N*N];
        float* beta;
        beta = new float[N];

        int cnt = 0;

        for (int i=0;i<N;++i){
            beta[i] = B[i];
            for (int j=0;j<N;++j){
                alpha[cnt] = A[j][i];
                ++cnt;
            }
        }

        int *IPIV = new int[N+1];

        int INFO;

        int NRHS = 1;


        INFO = clapack_sgetrf(CblasColMajor,N,N,alpha,N,IPIV); 

        if (INFO==0){
            INFO = clapack_sgetrs(CblasColMajor,CblasNoTrans,N,NRHS,alpha,N,IPIV,beta,N);

            if (INFO==0){

                for (int i=0;i<N;++i){
                    B[i] = beta[i];
                }
            }
            else {
                cerr << "Error: Gauss LU" << endl;
            }
        }
        else {
            cerr << "Error: Gauss" << endl;
        }

        delete[] IPIV;
        delete[] alpha;
        delete[] beta;
    }

    // returns square identity matrix dimension numCstates
    double** eyeF(int numCstates) {
        // Identity matrix, size of Ak
        double** eyeA;
        eyeA = new double*[numCstates];
        for (int i=0;i<numCstates;++i){
            eyeA[i] = new double[numCstates];
        }


        for (int i=0;i<numCstates;++i){
            for (int j=0;j<numCstates;++j){
                if (i-j==0) {
                    eyeA[i][j] = 1.0;
                }
                else {
                    eyeA[i][j] = 0.0;
                }
            }
        }

        return eyeA;
        //delete[] eyeA;
    }


    //############################################################
    // CONTROLLER MOTHER CLASS
    // Created by Geoffroy Claisse
    // University of Southampton, 08/2018
    //############################################################

    // Only use for compiling and polymorphism.
    // Functions are not implemented here, but overridden in child classes.

    // NOT IMPLEMENTED
    // Peter Heins do call the method Controller.ConStates
    // and then use it as parameter of dns.advance,
    // which called itself the method Controller.advance_Con
    // I do not understand why Constates is not used
    // privately in the class Controller as a class member ?
    double*** Controller::ConStates() {
        cout << "controller.cpp: Controller::ConStates" << endl;
        throw NotImplementedException();
    }

    // NO IMPLEMENTED
    // returns array containing number of controller states
    // at each kx,kz pair
    double** Controller::CStateInfo() {
        cout << "controller.cpp: Controller::CStateInfo" << endl;
        throw NotImplementedException();
    }

    // NOT IMPLEMENTED
    // Returns kx wavenumber based on controlled mx_c mode number
    // (which differs from sim mx mode number)
    int Controller::kx_c(int mx_c) {
        cout << "controller.cpp: Controller::kx_c" << endl;
        throw NotImplementedException();
    }

    // NOT IMPLEMENTED
    void Controller::advance_Con(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO)
    {
        cout << "controller.cpp: Controller::advance_Con" << endl;
        throw NotImplementedException();
    }

    // NOT IMPLEMENTED
    void Controller::advance_Con_CON(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO)
    {
        cout << "controller.cpp: Controller::advance_Con_CON" << endl;
        throw NotImplementedException();
    }


    //############################################################
    // CONTROLLER_HEINS CLASS
    // Created by Peter Heins on 27/02/2012.
    // Copyright 2012 University of Sheffield. All rights reserved.
    //############################################################

    Controller_Heins::Controller_Heins()
        :
            Ny_(0),
            minKx_(0),
            maxKx_(0),
            minKz_(0),
            maxKz_(0),
            uvw_(0),
            StateInfoFile_("Mult_Control_Mat/StateInfo.bin"),
            NumConInputs_(0),
            tau_(0.0),
            Spectral_states_(false)
    {}

    Controller_Heins::Controller_Heins(
            int Ny,
            int minKx,
            int maxKx,
            int minKz,
            int maxKz,
            int uvw,
            const char* StateInfoFile,
            int NumConInputs,
            double tau,
            bool Spectral_states)
        :
            Ny_(Ny),
            minKx_(minKx),
            maxKx_(maxKx),
            minKz_(minKz),
            maxKz_(maxKz),
            uvw_(uvw),
            StateInfoFile_(StateInfoFile),
            NumConInputs_(NumConInputs),
            tau_(tau),
            Spectral_states_(Spectral_states)
    {}

    // returns array containing number of controller states
    // at each kx,kz pair
    double** Controller_Heins::CStateInfo() {
        double** InfoMat;
        int minKx = minKx_;
        int maxKx = maxKx_;
        int minKz = minKz_;
        int maxKz = maxKz_;

        int maxMx = (maxKx-minKx)+1;
        int maxMz = (maxKz-minKz)+1;

        InfoMat = new double*[maxMx];
        for (int mx=0;mx<maxMx;++mx){
            InfoMat[mx] = new double[maxMz];
        }

        double** InMat = MatIn(StateInfoFile_);
        int cnt=0;

        for (int mx=0;mx<maxMx;++mx){
            for (int mz=0;mz<maxMz;++mz){
                InfoMat[mx][mz] = InMat[cnt][2];
                cnt += 1;
            }
        }

        for (int i=0;i<maxMx*maxMz;++i){
            delete[] InMat[i];
        }

        delete[] InMat;
        return InfoMat;
        //delete[] InfoMat;
    }

    // returns 3D array which is used to store controller states
    // at each kx,kz pair
    double*** Controller_Heins::ConStates() {
        int minKx = minKx_;
        int maxKx = maxKx_;
        int minKz = minKz_;
        int maxKz = maxKz_;

        int maxMx = (maxKx-minKx)+1;
        int maxMz = (maxKz-minKz)+1;

        double** StateInfo = CStateInfo();
        int cnt=0;
        int tmpNS;
        double*** StateMat;
        StateMat = new double**[maxMx];
        for (int mx=0;mx<maxMx;++mx) {
            StateMat[mx] = new double*[maxMz];

            for (int mz=0;mz<maxMz;++mz){
                tmpNS = StateInfo[mx][mz];
                StateMat[mx][mz] = new double[tmpNS];
                cnt +=1;
            }
        }

        for (int mx=0;mx<maxMx;++mx){
            for (int mz=0;mz<maxMz;++mz) {
                int tmp = StateInfo[mx][mz];
                for (int i=0;i<tmp;++i){
                    StateMat[mx][mz][i] = 0.0;
                }
            }
        }

        for (int i=0;i<maxMx;++i){
            delete[] StateInfo[i];
        }

        delete[] StateInfo;
        return StateMat;

        //delete[] StateMat;
    }

    // Returns kx wavenumber based on controlled mx_c mode number
    // (which differs from sim mx mode number)
    int Controller_Heins::kx_c(int mx_c) {
        int minKx = minKx_;
        int maxKx = maxKx_;
        int maxMx_c = (maxKx-minKx)+1;
        int kx_c;

        if (mx_c >= 0 && mx_c <= maxKx) {
            kx_c = mx_c;
        }
        else {
            kx_c = mx_c-maxMx_c;
        }

        return kx_c;
    }

    // integrates controller forward in time by dT
    void Controller_Heins::advance_Con(
            FlowField& u,
            FlowField& q,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        int IOcnt = 0;
        int minKx = minKx_;
        int maxKx = maxKx_;
        int minKz = minKz_;
        int maxKz = maxKz_;
        int Ny = Ny_;
        int uvw = uvw_;

        int maxMx_c = (maxKx-minKx)+1;
        int maxMz_c = (maxKz-minKz)+1;

        double** StateInfo =  CStateInfo();

        for (int mx_c=0;mx_c<maxMx_c;++mx_c){
            int kx = kx_c(mx_c);
            for(int mz_c=0;mz_c<maxMz_c;++mz_c){
                int kz = mz_c + minKz;

                if (kx!=0 || kz!=0)
                {
                    cout << "(" << kx << "," << kz << ") " ;

                    int mx = u.mx(kx); // Simulation mode numbers
                    int mz = u.mz(kz);

                    // Get number of controller states, simulation states and control inputs
                    int numCStates = StateInfo[mx_c][mz_c]; // Controller states
                    int numSStates = 4*Ny; // Sim states
                    int numInputs = NumConInputs_; // Controller inputs
                    int numOutputs = 4; // Controller outputs - cannot change from 4

                    // Extract number of states from state array
                    double CStates[numCStates];

                    for (int i=0;i<numCStates;++i){
                        // Controller state matrix
                        CStates[i] = CStateMat[mx_c][mz_c][i];
                    }

                    // Read in A,B,C,D and Cs matrices for correct kx,kz pair
                    string KaF = "Mult_Control_Mat/Ka_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KbF = "Mult_Control_Mat/Kb_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KcF = "Mult_Control_Mat/Kc_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KdF = "Mult_Control_Mat/Kd_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string CsF = "Mult_Control_Mat/Cs_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    const char* KaFile = KaF.c_str();
                    const char* KbFile = KbF.c_str();
                    const char* KcFile = KcF.c_str();
                    const char* KdFile = KdF.c_str();
                    const char* CsFile = CsF.c_str();
                    double** Ka = MatIn(KaFile);
                    double** Kb = MatIn(KbFile);
                    double** Kc = MatIn(KcFile);
                    double** Kd = MatIn(KdFile);
                    double** Cs = MatIn(CsFile);

                    // Create sim state vector and find controller input
                    //int Nys = u.Ny(); // num of Cheb Ny points
                    double RSimStates[numSStates]; // real part of states
                    double ISimStates[numSStates]; // imag part of states
                    u.makeState(Spectral, Physical); // Make physical in y
                    q.makeState(Spectral, Physical);

                    for (int i=0;i<Ny;++i)
                    {
                        RSimStates[i] =      real(u.cmplx(mx,i,mz,0));
                        RSimStates[i+Ny] =   real(u.cmplx(mx,i,mz,1));
                        RSimStates[i+2*Ny] = real(u.cmplx(mx,i,mz,2));
                        RSimStates[i+3*Ny] = real(q.cmplx(mx,i,mz,0));
                        ISimStates[i] =      imag(u.cmplx(mx,i,mz,0));
                        ISimStates[i+Ny] =   imag(u.cmplx(mx,i,mz,1));
                        ISimStates[i+2*Ny] = imag(u.cmplx(mx,i,mz,2));
                        ISimStates[i+3*Ny] = imag(q.cmplx(mx,i,mz,0));
                    }

                    u.makeState(Spectral, Spectral);
                    q.makeState(Spectral, Spectral);

                    // Collect real and imaginary sim states in one vector
                    double SimStates[2*numSStates];
                    for (int i=0;i<numSStates;++i){
                        SimStates[i] = RSimStates[i];
                    }
                    for (int i=numSStates;i<2*numSStates;++i){
                        SimStates[i] = ISimStates[i-numSStates];
                    }

                    // Multiply state vector by Cs to get sim output/controller input
                    double* uk; // Controller input vector
                    double* yk; // Controller output vector
                    yk = new double[4];
                    uk = new double[numInputs];

                    uk = MatVec(Cs,SimStates,numInputs,2*numSStates);

                    // Integrate Discrete Controller forward in time
                    double* AkXk = MatVec(Ka,CStates,numCStates,numCStates);
                    double* BkUk = MatVec(Kb,uk,numCStates,numInputs);

                    // Update controller states
                    for (int i=0;i<numCStates;++i){
                        CStates[i] = AkXk[i] + BkUk[i];
                    }

                    // Calculate controller output
                    double* CkXk;
                    double* DkUk;

                    CkXk = MatVec(Kc,CStates,numOutputs,numCStates);
                    DkUk = MatVec(Kd,uk,numOutputs,numOutputs);
                    for (int i=0; i<numOutputs;++i){
                        // Calculate controller output
                        yk[i] = CkXk[i] + DkUk[i];
                    }

                    // Assign controller output to BCs flowfield
                    double Ad = exp((-1/tau_)*dT);
                    double Bd = -( exp((-1/tau_)*dT) -1);

                    // Simple low-pass filter
                    BC.cmplx(mx,1,mz,uvw) = (Ad*BC.cmplx(mx,1,mz,uvw)) + (Bd*(yk[0] + yk[2]*I));
                    BC.cmplx(mx,0,mz,uvw) = (Ad*BC.cmplx(mx,0,mz,uvw)) + (Bd*(yk[1] + yk[3]*I));


                    // Store updated states in state array
                    for (int i=0;i<numCStates;++i){
                        CStateMat[mx_c][mz_c][i] = CStates[i];
                    }

                    if (numInputs == 8){
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[4]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[5]; // Imag Str lower
                        IO[0][7][IOcnt]    = uk[2]; // Real Spa upper
                        IO[0][8][IOcnt]    = uk[6]; // Imag Spa upper
                        IO[0][9][IOcnt]    = uk[3]; // Real Spa lower
                        IO[0][10][IOcnt]   = uk[7]; // Imag Spa lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                        IO[1][7][IOcnt]    = 0.0;
                        IO[1][8][IOcnt]    = 0.0;
                        IO[1][9][IOcnt]    = 0.0;
                        IO[1][10][IOcnt]    = 0.0;
                    }
                    else {
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[2]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[3]; // Imag Str lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                    }

                    IOcnt += 1;

                    for (int i=0;i<numCStates;++i){
                        delete[] Ka[i];
                        //delete[] eyeA[i];
                        //delete[] alpha[i];
                        //delete[] beta[i];
                        delete[] Kb[i];
                    }

                    for (int i=0;i<4;++i){
                        delete[] Kc[i];
                        delete[] Kd[i];
                    }

                    for (int i=0;i<numInputs;++i){
                        delete[] Cs[i];

                    }

                    delete[] Ka;
                    delete[] Kb;
                    delete[] Kc;
                    delete[] Kd;
                    delete[] Cs;
                    //delete[] alpha;
                    //delete[] beta;
                    //delete[] bXk;
                    delete[] BkUk;
                    //delete[] Gam;
                    //delete[] eyeA;
                    delete[] CkXk;
                    delete[] DkUk;
                    delete[] uk;
                    delete[] yk;
                }
                else
                {
                    IO[0][0][IOcnt]    = t;
                    IO[0][1][IOcnt]    = kx;
                    IO[0][2][IOcnt]    = kz;
                    IO[0][3][IOcnt]    = 0; // Real upper
                    IO[0][4][IOcnt]    = 0; // Imag upper
                    IO[0][5][IOcnt]    = 0; // Real lower
                    IO[0][6][IOcnt]    = 0; // Imag lower

                    IO[1][0][IOcnt]    = t;
                    IO[1][1][IOcnt]    = kx;
                    IO[1][2][IOcnt]    = kz;
                    IO[1][3][IOcnt]    = 0; // Real upper
                    IO[1][4][IOcnt]    = 0; // Imag upper
                    IO[1][5][IOcnt]    = 0; // Real lower
                    IO[1][6][IOcnt]    = 0; // Imag lower

                    IOcnt += 1;
                }
            }

        }
        cout << " " << endl;
        cout << "Control applied to WNs kx="+i2s(minKx)+":"+i2s(maxKx)+", kz="+i2s(minKz)+":"+i2s(maxKz) << endl;

        for (int i=0;i<maxMx_c;++i){
            delete[] StateInfo[i];
        }

        delete[] StateInfo;
    }

    // integrates controller forward in time by dT
    void Controller_Heins::advance_Con_CON(
            FlowField& u,
            FlowField& q,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        int IOcnt = 0;
        int minKx = minKx_;
        int maxKx = maxKx_;
        int minKz = minKz_;
        int maxKz = maxKz_;
        int Ny = Ny_;
        int uvw = uvw_;

        int maxMx_c = (maxKx-minKx)+1;
        int maxMz_c = (maxKz-minKz)+1;

        bool Spectral_states = Spectral_states_;

        double** StateInfo = CStateInfo();

        for (int mx_c = 0; mx_c < maxMx_c; ++mx_c)
        {
            int kx = kx_c(mx_c);
            for(int mz_c = 0; mz_c < maxMz_c; ++mz_c)
            {
                int kz = mz_c + minKz;
                if (kx != 0 || kz != 0)
                {
                    //cout << "(" << kx << "," << kz << ") " ;
                    int mx = u.mx(kx); // Simulation mode numbers
                    int mz = u.mz(kz);

                    // Get number of controller states, simulation states and control inputs
                    int numCStates = StateInfo[mx_c][mz_c]; // Controller states
                    int numSStates = 4*Ny; // Sim states
                    int numInputs = NumConInputs_; // Controller inputs
                    int numOutputs = 4; // Controller outputs - cannot change from 4

                    // Extract number of states from state array
                    double CStates[numCStates];

                    for (int i = 0; i < numCStates; ++i)
                    {
                        CStates[i] = CStateMat[mx_c][mz_c][i]; // Controller state matrix
                    }

                    // Read in A,B,C,D and Cs matrices for correct kx,kz pair
                    string KaF = "Mult_Control_Mat/Ka_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KbF = "Mult_Control_Mat/Kb_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KcF = "Mult_Control_Mat/Kc_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KdF = "Mult_Control_Mat/Kd_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string CsF = "Mult_Control_Mat/Cs_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    const char* KaFile = KaF.c_str();
                    const char* KbFile = KbF.c_str();
                    const char* KcFile = KcF.c_str();
                    const char* KdFile = KdF.c_str();
                    const char* CsFile = CsF.c_str();
                    double** Ka = MatIn(KaFile);
                    double** Kb = MatIn(KbFile);
                    double** Kc = MatIn(KcFile);
                    double** Kd = MatIn(KdFile);
                    double** Cs = MatIn(CsFile);

                    // Create sim state vector and find controller input
                    //int Nys = u.Ny(); // num of Cheb Ny points
                    double RSimStates[numSStates]; // real part of states
                    double ISimStates[numSStates]; // imag part of states

                    if (Spectral_states)
                    {
                        u.makeState(Spectral, Spectral); // Make spectral in y
                        q.makeState(Spectral, Spectral);

                        for (int i=0;i<Ny;++i){
                            RSimStates[i]       = real(u.cmplx(mx,i,mz,0));
                            RSimStates[i+Ny]    = real(u.cmplx(mx,i,mz,1));
                            RSimStates[i+2*Ny]  = real(u.cmplx(mx,i,mz,2));
                            RSimStates[i+3*Ny]  = real(q.cmplx(mx,i,mz,0));
                            ISimStates[i]       = imag(u.cmplx(mx,i,mz,0));
                            ISimStates[i+Ny]    = imag(u.cmplx(mx,i,mz,1));
                            ISimStates[i+2*Ny]  = imag(u.cmplx(mx,i,mz,2));
                            ISimStates[i+3*Ny]  = imag(q.cmplx(mx,i,mz,0));
                        }
                    } // fi (Spectral_states)
                    else
                    {
                        u.makeState(Spectral, Physical); // Make physical in y
                        q.makeState(Spectral, Physical);

                        for (int i=0;i<Ny;++i){
                            RSimStates[i]       = real(u.cmplx(mx,i,mz,0));
                            RSimStates[i+Ny]    = real(u.cmplx(mx,i,mz,1));
                            RSimStates[i+2*Ny]  = real(u.cmplx(mx,i,mz,2));
                            RSimStates[i+3*Ny]  = real(q.cmplx(mx,i,mz,0));
                            ISimStates[i]       = imag(u.cmplx(mx,i,mz,0));
                            ISimStates[i+Ny]    = imag(u.cmplx(mx,i,mz,1));
                            ISimStates[i+2*Ny]  = imag(u.cmplx(mx,i,mz,2));
                            ISimStates[i+3*Ny]  = imag(q.cmplx(mx,i,mz,0));
                        }

                        u.makeState(Spectral, Spectral);
                        q.makeState(Spectral, Spectral);
                    }

                    // Collect real and imaginary sim states in one vector
                    double SimStates[2 * numSStates];
                    for (int i = 0; i < numSStates; ++i)
                    {
                        SimStates[i] = RSimStates[i];
                    }
                    for (int i = numSStates; i < 2 * numSStates; ++i)
                    {
                        SimStates[i] = ISimStates[i - numSStates];
                    }

                    // Multiply state vector by Cs to get sim output/controller input
                    double* uk;   // Controller input vector
                    double* yk;   // Controller output vector
                    yk = new double[4];
                    uk = new double[numInputs];

                    uk = MatVec(Cs, SimStates, numInputs, 2 * numSStates);

                    ////////////////////////////////////////////////////////////////
                    // Integrate controller forward in time USING CRANK-NICOLSON

                    // Calculate alpha and beta
                    // alpha = I-delT/2*A, beta = I+delT/2*A
                    double** alpha; //alpha matrix
                    alpha = new double*[numCStates];
                    for (int i = 0; i < numCStates; ++i)
                    {
                        alpha[i] = new double[numCStates];
                    }

                    double** beta; // beta matrix
                    beta = new double*[numCStates];
                    for (int i = 0; i < numCStates; ++i){
                        beta[i] = new double[numCStates];
                    }

                    double** eyeA = eyeF(numCStates); // Identity matrix

                    for (int i = 0; i < numCStates; ++i){
                        for (int j = 0; j < numCStates; ++j){
                            alpha[i][j] = eyeA[i][j] - ((dT / 2) * Ka[i][j]);
                            beta[i][j] = eyeA[i][j] + ((dT / 2) * Ka[i][j]);
                        }
                    }

                    // Calculate beta*Xk, Bk*uk and Gam
                    double* bXk; // beta*Xk
                    double* Gam; // bXk + dt*Bkuk
                    double* BkUk; // Bk*uk

                    Gam = new double[numCStates];

                    bXk = MatVec(beta,CStates,numCStates,numCStates);
                    BkUk = MatVec(Kb,uk,numCStates,numInputs);

                    // Calculate gamma
                    for (int i = 0; i < numCStates; ++i){
                        Gam[i] = bXk[i] + (dT * BkUk[i]);
                    }

                    // Calculate new state vector Xk
                    gauss(alpha, Gam, numCStates); // Solves alpha*Xk=Gam
                    // Gam now equal to Xk

                    // Update controller states
                    for (int i = 0; i < numCStates; ++i){
                        CStates[i] = Gam[i];
                    }
                    ////////////////////////////////////////////////////////////////

                    // Calculate controller output
                    double* CkXk;
                    double* DkUk;

                    CkXk = MatVec(Kc,CStates,numOutputs,numCStates);
                    DkUk = MatVec(Kd,uk,numOutputs,numOutputs);
                    for (int i = 0; i < numOutputs; ++i){
                        yk[i] = CkXk[i] + DkUk[i]; // Calculate controller output
                    }

                    // Crank-Nicolson low-pass filter integration
                    double alpha_lpf = 1 + (dT / (2 * tau_));
                    double beta_lpf =  1 - (dT / (2 * tau_));

                    BC.cmplx(mx, 1, mz, uvw) =
                        (1 / alpha_lpf) * (  (beta_lpf * BC.cmplx(mx, 1, mz, uvw)) + ((dT / tau_) * (yk[0] + yk[2] * I)) );
                    BC.cmplx(mx, 0, mz, uvw) =
                        (1 / alpha_lpf) * (  (beta_lpf * BC.cmplx(mx, 0, mz, uvw)) + ((dT / tau_) * (yk[1] + yk[3] * I)) );

                    // Store updated states in state array
                    for (int i=0;i<numCStates;++i){
                        CStateMat[mx_c][mz_c][i] = CStates[i];
                    }

                    if (numInputs == 12){
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[6]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[7]; // Imag Str lower
                        IO[0][7][IOcnt]    = uk[2]; // Real Spa upper
                        IO[0][8][IOcnt]    = uk[8]; // Imag Spa upper
                        IO[0][9][IOcnt]    = uk[3]; // Real Spa lower
                        IO[0][10][IOcnt]   = uk[9]; // Imag Spa lower
                        IO[0][11][IOcnt]   = uk[4]; // Real P upper
                        IO[0][12][IOcnt]   = uk[10]; // Imag P upper
                        IO[0][13][IOcnt]   = uk[5]; // Real P lower
                        IO[0][14][IOcnt]   = uk[11]; // Imag P lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                        IO[1][7][IOcnt]    = 0.0;
                        IO[1][8][IOcnt]    = 0.0;
                        IO[1][9][IOcnt]    = 0.0;
                        IO[1][10][IOcnt]    = 0.0;
                    }
                    else if (numInputs == 8){
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[4]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[5]; // Imag Str lower
                        IO[0][7][IOcnt]    = uk[2]; // Real Spa upper
                        IO[0][8][IOcnt]    = uk[6]; // Imag Spa upper
                        IO[0][9][IOcnt]    = uk[3]; // Real Spa lower
                        IO[0][10][IOcnt]    = uk[7]; // Imag Spa lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                        IO[1][7][IOcnt]    = 0.0;
                        IO[1][8][IOcnt]    = 0.0;
                        IO[1][9][IOcnt]    = 0.0;
                        IO[1][10][IOcnt]    = 0.0;
                    }
                    else if (numInputs == 4) {
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[2]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[3]; // Imag Str lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                    }
                    else {
                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                    }

                    IOcnt += 1;
                    for (int i=0;i<numCStates;++i){
                        delete[] Ka[i];
                        delete[] eyeA[i];
                        delete[] alpha[i];
                        delete[] beta[i];
                        delete[] Kb[i];
                    }
                    for (int i=0;i<4;++i){
                        delete[] Kc[i];
                        delete[] Kd[i];
                    }
                    for (int i=0;i<numInputs;++i){
                        delete[] Cs[i];
                    }
                    delete[] Ka;
                    delete[] Kb;
                    delete[] Kc;
                    delete[] Kd;
                    delete[] Cs;
                    delete[] alpha;
                    delete[] beta;
                    delete[] bXk;
                    delete[] BkUk;
                    delete[] Gam;
                    delete[] eyeA;
                    delete[] CkXk;
                    delete[] DkUk;
                    delete[] uk;
                    delete[] yk;
                } //fi (kx != 0 || kz != 0)
                else {
                    IO[0][0][IOcnt]    = t;
                    IO[0][1][IOcnt]    = kx;
                    IO[0][2][IOcnt]    = kz;
                    IO[0][3][IOcnt]    = 0; // Real upper
                    IO[0][4][IOcnt]    = 0; // Imag upper
                    IO[0][5][IOcnt]    = 0; // Real lower
                    IO[0][6][IOcnt]    = 0; // Imag lower

                    IO[1][0][IOcnt]    = t;
                    IO[1][1][IOcnt]    = kx;
                    IO[1][2][IOcnt]    = kz;
                    IO[1][3][IOcnt]    = 0; // Real upper
                    IO[1][4][IOcnt]    = 0; // Imag upper
                    IO[1][5][IOcnt]    = 0; // Real lower
                    IO[1][6][IOcnt]    = 0; // Imag lower

                    IOcnt += 1;
                }
            }
        }
        //cout << " " << endl;
        //cout << "Control applied to WNs kx="+i2s(minKx)+":"+i2s(maxKx)+", kz="+i2s(minKz)+":"+i2s(maxKz) << endl;
        cout << "C";

        for (int i = 0; i < maxMx_c; ++i)
        {
            delete[] StateInfo[i];
        }
        delete[] StateInfo;
    }


    //############################################################
    // CONTROLLER K CLASS
    // Created by Geoffroy Claisse
    // University of Southampton, 08/2018
    //############################################################

    // add controller_K constructor loading the matrices K and Cs
    // add function parameter K in the initialization field

    Controller_K::Controller_K()
        :
            _Nd_C(3),
            _Mx_C(0),
            _My_C(0),
            _Mz_C(0),
            _matrix_filename(""),
            _target_baseflow_filename(""),
            _tau(0),
            _M_C_in(_Nd_C * _Mx_C * _My_C * _Mz_C),
            _M_C_out(1 * _Mx_C * 2 * _Mz_C)
    {
        // number of modes of the Controller input (FlowField state)
        //_M_C_in = _Nd_C * _Mx_C * _My_C * _Mz_C;

        // number of modes of the Controller output: the actuation at the BCs
        // IT DOES ONLY CONSIDER FOR NOW WALL-NORMAL VELOCITY ACTUATION
        //_M_C_out = 1 * _Mx_C * 2 * _Mz_C;
    }

    // TODO
    Controller_K::Controller_K(
            int Mx_C,
            int My_C,
            int Mz_C,
            std::string matrix_filename,
            std::string target_baseflow_filename,
            double tau)
        :
            _Nd_C(3),
            _Mx_C(Mx_C),
            _My_C(My_C),
            _Mz_C(Mz_C),
            _matrix_filename(matrix_filename),
            _target_baseflow_filename(target_baseflow_filename),
            _tau(tau),
            _M_C_in(_Nd_C * _Mx_C * _My_C * _Mz_C),
            _M_C_out(1 * _Mx_C * 2 * _Mz_C)
    {
        // number of modes of the Controller state
        // number of modes of the Controller output, the Boundary Cdt

        // LOAD MATRIX K for [u,v,w]
        // TODO load proper matrix K
        if (_matrix_filename == "None")
            K = Eigen::MatrixXcd(_M_C_out, _M_C_in);
        else
        {
            K = Eigen::MatrixXcd(_M_C_out, _M_C_in);
            cout << _M_C_out << endl;
            cout << _M_C_in << endl;
            MatIn_hdf5_complex(K, _matrix_filename);
        }

        // LOAD MATRIX CS
        // -> use function MatIn
        // -> perhaps not necessary

        // LOAD BASEFLOW
        // TODO check if baseflow and other flowfield dimension fitted together
        if (_target_baseflow_filename != "None")
        {
            _target_baseflow_flowfield = FlowField(_target_baseflow_filename);
            _target_baseflow_flowfield.makeState(Spectral, Spectral);
        }
        else
        {
            _target_baseflow_flowfield = FlowField(_Mx_C,_My_C,2 * (_Mz_C - 1),_Nd_C,2*pi,2*pi,-1,1);
            // set to zero
            for (int i = 0; i < _Nd_C; ++i) {
                for (int mx = 0; mx < _Mx_C; ++mx) {
                    for (int my = 0; my < _My_C; ++my) {
                        for (int mz = 0; mz < _Mz_C; ++mz) {
                            _target_baseflow_flowfield.cmplx(mx, my, mz, i) = 0;
                        }
                    }
                }
            }
        }
        cout << "TARGET BASEFLOW : " << endl;
        cout << "b : " << _target_baseflow_flowfield.Lx() << endl;
        cout << "Nx     : " << _target_baseflow_flowfield.Nx() << endl;
        cout << "Ny     : " << _target_baseflow_flowfield.Ny() << endl;
        cout << "Nz     : " << _target_baseflow_flowfield.Nz() << endl;
        cout << "Nd     : " << _target_baseflow_flowfield.Nd() << endl;
        cout << "Mx     : " << _target_baseflow_flowfield.Mx() << endl;
        cout << "My     : " << _target_baseflow_flowfield.My() << endl;
        cout << "Mz     : " << _target_baseflow_flowfield.Mz() << endl;
        cout << "Lx     : " << _target_baseflow_flowfield.Lx() << endl;
        cout << "Lz     : " << _target_baseflow_flowfield.Lz() << endl;
        cout << "a      : " << _target_baseflow_flowfield.a() << endl;
        cout << "b      : " << _target_baseflow_flowfield.b() << endl;
    }

    // NOT IMPLEMENTED
    void Controller_K::advance_Con(
            FlowField& u,
            FlowField& p,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        cout << "controller.cpp: Controller_K::advance_Con" << endl;
        throw NotImplementedException();
    }

    // TODO
    void Controller_K::advance_Con_CON(
            FlowField& u,
            FlowField& p,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        // TODO
        // memory and perf profiling
        //
        // TODO
        // in wave-dmd, reduce the actuation to :
        // for v, streamwise modes : Mx = Nx
        // for v, spanwise modes : Mz = int(Nx/2) + 1
        // for eta, streamwise : 0
        // for eta, spanwse : 0
        // for uw : 0
        // AND ONLY COMPARE SPECTRAL WD WITH SPECTRAL CF
        // as CF spectral state has dimension Mz = Nz / 2
        // meaning that :
        // Nz_wd_controller = 10 implying that Mz_wd_controller = 10
        // Nz_cf_controller = 20 implying that Mz_cf_controller = 10
        //
        // DO NOT CONTROL ON ETA
        // DO NOT CONTROL UVW

        // double*** CStateMat, Real t, double*** IO are not used here
        // they are used to export/import the state of the field
        // so might be useful, but not required

        cout << "K";
        const int Nx = u.Nx();
        const int Ny = u.Ny();
        const int Nz = u.Nz();
        const int Nd = u.Nd();
        const int Mx = u.Mx();
        const int My = u.My();
        const int Mz = u.Mz();

        const int N = Nx * Ny * Nz * Nd;
        const int M = Mx * My * Mz * Nd;

        // write a function instead of calling a matrix Cs
        // as it is full flow information, so there is no transformation
        // other than resizing.
        // needs to do:
        // 0. check the states (fourier, chebyshev or spectral)
        //    make it fourier x chebyshev x fourier
        //cout << "    State : " << u.xzstate() << " / " << u.ystate() << endl;
        u.makeState(Spectral, Spectral);

        // 2. remove the baseflow form the channelflow state
        //cout << "    SUBSTRACT FF" << endl;
        this->substract_ff(u, _target_baseflow_flowfield);

        // 3. interpolate the channeflow velocity field u to controller dimension
        const int Nx_C = _Mx_C;
        const int Ny_C = _My_C;
        const int Nz_C = 2 * (_Mz_C - 1); // so that the number of modes in CF and wave-dmd are the same
        FlowField u_reduced = FlowField(Nx_C, Ny_C, Nz_C, u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
        //cout << "    Expected number of Z modes : " << _Mz_C << endl;
        //cout << "    Number of Z modes : " << u_reduced.Mz() << endl;

        this->changegrid(u, u_reduced);
        //cout << "    check after changegrid : " << endl;
        //cout << "    -> u shape: " << u.Mx() << " / " << u.My() << " / " << u.Mz() << endl;
        //cout << "    -> u_reduced expected: " << _Mx_C << " / " << _My_C << " / " << _Mz_C << endl;
        //cout << "    -> u_reduced shape   : " << u_reduced.Mx() << " / " << u_reduced.My() << " / " << u_reduced.Mz() << endl;

        // 5. return the new state of simulation yk in vector format
        //    as an Eigen::VectorXcd
        // (I could still multiply somewhere by the FULLINFORMATION MATRIX Cs
        // and use matarray function ?)
        //cout << "    changing u_reduced to u_reduced_vector " << endl;
        Eigen::VectorXcd u_reduced_vector(_M_C_in);
        //cout << "    -> Size 1: " << u_reduced_vector.size() << endl;
        //cout << "    -> Rows 1: " << u_reduced_vector.rows() << endl;
       // cout << "    -> Cols 1: " << u_reduced_vector.cols() << endl;
        this->FlowField_to_EigenVector(u_reduced, u_reduced_vector);
        //cout << "    -> Size 2: " << u_reduced_vector.size() << endl;
        //cout << "    -> Rows 2: " << u_reduced_vector.rows() << endl;
        //cout << "    -> Cols 2: " << u_reduced_vector.cols() << endl;

        // GET THE FORCING q [yk in Heins controller class]
        Eigen::VectorXcd q_reduced_vector (_M_C_out);
        //cout << "    M_C_in  : " << _M_C_in << endl;
        //cout << "    M_C_out : " << _M_C_out << endl;
        //cout << "    K : " << K.rows() << " / " << K.cols() << endl;
        q_reduced_vector = K * u_reduced_vector;

        //cout << "    changing q_reduced_vector to q " << endl;
        //cout << "    -> q_reduced_vector: " << q_reduced_vector.rows() << " / " << q_reduced_vector.cols() << endl;
        // FlowField q_reduced correspond to the actuation at the boundaries,
        // implying only for the wall-normal velocity at the upper and lower wall
        FlowField q_reduced = FlowField(Nx_C, 2, Nz_C, 1, BC.Lx(), BC.Lz(), BC.a(), BC.b(), Spectral, Spectral);
        this->EigenVector_to_FlowField(q_reduced_vector, q_reduced);
        //cout << "    -> q_reduced: " << q_reduced.Mx() << " / " << q_reduced.My() << " / " << q_reduced.Mz() << endl;

        FlowField q = FlowField(u.Nx(), 2, u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
        //cout << "    q: " << q.Nx() << " / " << q.Ny() << " / " << q.Nz() << endl;
        //cout << "    CHANGEGRID " << endl;
        this->changegrid(q_reduced, q);

        // UPDATE THE FLOWFIELD BC
        //cout << "    UPDATE BC " << endl;

        // Crank-Nicolson low-pass filter integration
        double alpha_lpf = 1 + (dT / (2 * _tau));
        double beta_lpf =  1 - (dT / (2 * _tau));

        // TODO check if complex numbers are dealt with well
        //for (int index = 0; index < BC.Nd(); ++index) {
            for (int mx = 0; mx < BC.Mx(); ++mx) {
                for (int mz = 0; mz < BC.Mz(); ++mz) {
                    //lower wall, for u, v and w
                    BC.cmplx(mx, 0, mz, 0) = 0;
                    BC.cmplx(mx, 0, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC.cmplx(mx, 0, mz, 1)) + ((dT / _tau) * q.cmplx(mx, 0, mz, 0)) );//* (yk[0] + yk[2] * I)) );
                    BC.cmplx(mx, 0, mz, 2) = 0;

                    //upper  wall, for u, v and w
                    BC.cmplx(mx, 1, mz, 0) = 0;
                    BC.cmplx(mx, 1, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC.cmplx(mx, 1, mz, 1)) + ((dT / _tau) * q.cmplx(mx, 1, mz, 0)) );//* (yk[0] + yk[2] * I)) );
                    BC.cmplx(mx, 1, mz, 2) = 0;
                }
            }
        //}

        // add baseflow back to the BCs
        //cout << "    ADD FF" << endl;
        this->add_ff(u, _target_baseflow_flowfield);

    }

    void Controller_K::test()
    {
        cout << setprecision(3);
        cout << "TEST" << endl;
        int Nd = 3;
        int Nx = 3;
        int Ny = 1;
        int Nz = 4;
        int N = Nx * Ny * Nz * Nd;

        FlowField ff = FlowField(Nx, Ny, Nz, Nd, 2, 2, 1, -1);
        cout << "DIMENSION : " << ff.Nx() << " / " << ff.Ny() << " / " << ff.Nz() << " / " << ff.Nd() << endl;
        cout << "MODES : " << ff.Mx() << " / " << ff.My() << " / " << ff.Mz() << " / " << ff.Nd() << endl;
        int Mx = ff.Mx();
        int My = ff.My();
        int Mz = ff.Mz();
        int M = ff.Mx() * ff.My() * ff.Mz() * ff.Nd();
        cout << "total modes : " << M << endl;

        /*
        cout << " ----------- " << endl;
        cout << ff.kzmin() << endl;
        cout << ff.kzmax() << endl;
        cout << ff.Mz() << endl;
        cout << " ----------- " << endl;
        cout << ff.mz(0) << endl;
        cout << ff.mz(1) << endl;
        cout << ff.mz(2) << endl;
        cout << ff.mz(3) << endl;
        cout << ff.mz(-2) << endl;
        cout << ff.mz(-1) << endl;
        cout << " ----------- " << endl;
        */

        Eigen::VectorXcd vect = Eigen::VectorXcd(M);
        for (int i = 0; i< M; ++i)
            vect(i) = 0;

        int mx1 = 1;
        int mz1 = 0;
        int i1 = 0;
        int M1 = i1 * (Mx*My*Mz) + mx1 * (My*Mz) + mz1 * My  + 0;
        cout << " M1 : " << M1 << endl;

        int mx2 = 0;
        int mz2 = 1;
        int i2 = 0;
        int M2 = i2 * (Mx*My*Mz) + mx2 * (My*Mz) + mz2 * My  + 0;
        cout << " M2 : " << M2 << endl;

        int mx3 = 2;
        int mz3 = 2;
        int i3 = 2;
        int M3 = i3 * (Mx*My*Mz) + mx3 * (My*Mz) + mz3 * My  + 0;
        cout << " M3 : " << M3 << endl;

        cout << endl;
        cout << "VECT" << endl;
        for (int my = 0; my < My; ++my)
        {
            vect[i1 * (Mx*My*Mz) + mx1 * (My*Mz) + mz1 * My  + my] = 310;
            vect[i2 * (Mx*My*Mz) + mx2 * (My*Mz) + mz2 * My  + my] = 101;
            vect[i3 * (Mx*My*Mz) + mx3 * (My*Mz) + mz3 * My  + my] = 411;
        }
        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mx = 0; mx < ff.Mx(); ++mx) {
                for (int mz = 0; mz < ff.Mz(); ++mz) {
                    cout << i << "/" << mx << "/" << mz
                         << " =  " << i * (Mx*My*Mz) + mx * (My*Mz) + mz * My  + 0
                         << " : "  << vect[i * (Mx*My*Mz) + mx * (My*Mz) + mz * My  + 0]
                         << endl;
                }
            }
            cout << "----" << endl;
        }

        cout << endl;
        cout << "ff" << endl;
        this->EigenVector_to_FlowField(vect, ff);
        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mz = 0; mz < ff.Mz(); ++mz) {
                for (int mx = 0; mx < ff.Mx(); ++mx) {
                    cout << ff.cmplx(mx, 0, mz, i) << " ";
                }
                cout << endl;
            }
            cout << " ----------- " << endl;
        }

        cout << endl;
        cout << "VECT" << endl;
        this->FlowField_to_EigenVector(ff, vect);
        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mx = 0; mx < ff.Mx(); ++mx) {
                for (int mz = 0; mz < ff.Mz(); ++mz) {
                    cout << i << "/" << mx << "/" << mz
                         << " =  " << i * (Mx*My*Mz) + mx * (My*Mz) + mz * My  + 0
                         << " : "  << vect[i * (Mx*My*Mz) + mx * (My*Mz) + mz * My  + 0]
                         << endl;
                }
            }
            cout << "----" << endl;
        }


    }

    void Controller_K::FlowField_to_EigenVector(FlowField& ff, Eigen::VectorXcd& eigenVector)
    {
        // insure that ff is spectral
        ff.makeState(Spectral, Spectral);

        int Mx = ff.Mx();
        int My = ff.My();
        int Mz = ff.Mz();
        int Nd = ff.Nd();
        int M = Mx * My * Mz * Nd;

        // Check if dimensions fit
        if (M != eigenVector.size())
        {
            cout << "MODAL DIMENSIONS DIFFERS" << endl;
            cout << " M : " << M << " / M_eigenvector : " << eigenVector.size() << endl;
        }

        for (int i = 0; i < Nd; ++i) {
            for (int mx = 0; mx < Mx; ++mx) {
                for (int my = 0; my < My; ++my) {
                    for (int mz = 0; mz < Mz; ++mz) {
                        //int mx = ff.mx(kx);
                        //int mz = ff.mz(kz);
                        eigenVector[i * (Mx*My*Mz) + mx * (My*Mz) + mz * My + my] = ff.cmplx(mx, my, mz, i);
                    }
                }
            }
        }
    }

    void Controller_K::EigenVector_to_FlowField(Eigen::VectorXcd& eigenVector, FlowField& ff)
    {
        // insure that ff is spectral
        ff.makeState(Spectral, Spectral);

        int Mx = ff.Mx();
        int My = ff.My();
        int Mz = ff.Mz();
        int Nd = ff.Nd();
        int M = Mx * My * Mz * Nd;

        // Check if dimensions fit
        if (M != eigenVector.size())
        {
            cout << "MODAL DIMENSIONS DIFFERS" << endl;
            cout << " M : " << M << " / M_eigenvector : " << eigenVector.size() << endl;
        }

        // TODO Check if the passing order is right
        for (int i = 0; i < Nd; ++i) {
            for (int mx = 0; mx < Mx; ++mx) {
                for (int my = 0; my < My; ++my) {
                    for (int mz = 0; mz < Mz; ++mz) {
                        ff.cmplx(mx, my, mz, i) = eigenVector[i * (Mx*My*Mz) + mx * (My*Mz) + mz * My + my];
                    }
                }
            }
        }
    }

    void Controller_K::changegrid(FlowField& u_in, FlowField& u_out, bool fixdiv, bool padded)
    {
        // from channelflow/trunk/program/changegrid.cpp

        const int Nx = u_out.Nx();
        const int Ny = u_out.Ny();
        const int Nz = u_out.Nz();

        u_out.interpolate(u_in);

        if (u_out.Lx() != u_in.Lx() || u_out.Lz() != u_in.Lz())
            u_out.rescale(u_in.Lx(), u_in.Lz());

        if (u_in.Nd() == 3) {
            if (padded || (Nx >= (3*u_in.Nx())/2 && Nx >= (3*u_in.Nz())/2))
                u_out.zeroPaddedModes();
        }
    }

    void Controller_K::substract_ff(FlowField& ff, FlowField& ff_to_substract)
    {
        ff.makeState(Spectral, Spectral);
        ff_to_substract.makeState(Spectral, Spectral);

        FlowField ff_to_substract_fitted = FlowField(ff.Nx(), ff.Ny(), ff.Nz(), ff.Nd(), ff.Lx(), ff.Lz(), ff.a(), ff.b(), Spectral, Spectral);
        this->changegrid(ff_to_substract, ff_to_substract_fitted);

        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mx = 0; mx < ff.Mx(); ++mx) {
                for (int my = 0; my < ff.My(); ++my) {
                    for (int mz = 0; mz < ff.Mz(); ++mz) {
                        ff.cmplx(mx, my, mz, i) -= ff_to_substract_fitted.cmplx(mx, my, mz, i);
                    }
                }
            }
        }
    }

    void Controller_K::add_ff(FlowField& ff, FlowField& ff_to_add)
    {
        ff.makeState(Spectral, Spectral);
        ff_to_add.makeState(Spectral, Spectral);

        FlowField ff_to_add_fitted = FlowField(ff.Nx(), ff.Ny(), ff.Nz(), ff.Nd(), ff.Lx(), ff.Lz(), ff.a(), ff.b(), Spectral, Spectral);
        this->changegrid(ff_to_add, ff_to_add_fitted);

        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mx = 0; mx < ff.Mx(); ++mx) {
                for (int my = 0; my < ff.My(); ++my) {
                    for (int mz = 0; mz < ff.Mz(); ++mz) {
                        ff.cmplx(mx, my, mz, i) += ff_to_add_fitted.cmplx(mx, my, mz, i);
                    }
                }
            }
        }
    }

    //############################################################
    // HDF5 reading functions
    // Created by Geoffroy Claisse
    // University of Southampton, 08/2018
    //############################################################

    #ifdef HAVE_HDF5
    int hdf5read(double **matrix, const size_t N0, const size_t N1, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the matrix
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank > 2) {
                channelflow::cferror("Matrix rank should be 1 or 2.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, NULL);
            int N = dims_out[0] * dims_out[1];
            /*
               cout << rank << " / Dimensions : "
               << (unsigned long) (dims_out[0]) << " x "
               << (unsigned long) (dims_out[1]) << endl;
            //*/
            if (dims_out[0] != N0 && dims_out[1] != N1)
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(matrix, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset matrix size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into matrix
            double* buff = new double[N];
            //cout << typeid(buff).name() << endl;

            dataset.read(buff, H5::PredType::IEEE_F64BE, memspace, dataspace);
            H5Tconvert(H5T_IEEE_F64BE, H5T_NATIVE_DOUBLE, N, buff, NULL, H5P_DEFAULT);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                for (size_t jj = 0; jj < dims_out[1]; jj++) {
                    matrix[ii][jj] = buff[ii * dims_out[0] + jj];
                }
            }

            dataset.close();
            delete buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException error ) {
            cout << "Error hdf5read_real FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException error ) {
            cout << "Error hdf5read_real DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException error ) {
            cout << "Error hdf5read_real DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException error ) {
            cout << "Error hdf5read_real DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5read_real(Eigen::MatrixXcd& matrix, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the matrix
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank != 2) {
                channelflow::cferror("Matrix rank should be 2.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, NULL);

            int N = dims_out[0] * dims_out[1];

            //*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " x "
                << (unsigned long) (dims_out[1]) << " / "
                << N << endl;
            //*/

            if ((unsigned) dims_out[0] != matrix.rows()
                    && (unsigned) dims_out[1] != matrix.cols())
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(matrix, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset matrix size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into matrix
            double* buff = new double[N];
            dataset.read(buff, H5::PredType::IEEE_F64BE, memspace, dataspace);
            H5Tconvert(H5T_IEEE_F64BE,
                    H5T_NATIVE_DOUBLE,
                    N, buff, NULL, H5P_DEFAULT);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                for (size_t jj = 0; jj < dims_out[1]; jj++) {
                    matrix(ii, jj) = buff[ii * dims_out[1] + jj];
                }
            }

            dataset.close();
            delete buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException error ) {
            cout << "Error hdf5read_real FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException error ) {
            cout << "Error hdf5read_real DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException error ) {
            cout << "Error hdf5read_real DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException error ) {
            cout << "Error hdf5read_real DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5read_real(Eigen::VectorXcd& vector, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the vector
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank != 1) {
                channelflow::cferror("Vector rank should be 1.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, NULL);

            int N = dims_out[0];

            //*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " / "
                << N << endl;
            //*/

            if ((unsigned) dims_out[0] != vector.rows())
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(vector, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset vector size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into vector
            double* buff = new double[N];
            dataset.read(buff, H5::PredType::IEEE_F64BE, memspace, dataspace);
            H5Tconvert(H5T_IEEE_F64BE,
                    H5T_NATIVE_DOUBLE,
                    N, buff, NULL, H5P_DEFAULT);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                vector(ii) = buff[ii];
            }

            dataset.close();
            delete buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException error ) {
            cout << "Error hdf5read_real FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException error ) {
            cout << "Error hdf5read_real DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException error ) {
            cout << "Error hdf5read_real DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException error ) {
            cout << "Error hdf5read_real DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5read_complex(Eigen::MatrixXcd& matrix, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            typedef struct complex_type{
                double r;
                double i;
            } complex_type;

            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the matrix
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank != 2) {
                channelflow::cferror("Matrix rank should be 2.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, NULL);

            int N = dims_out[0] * dims_out[1];

            //*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " x "
                << (unsigned long) (dims_out[1]) << " / "
                << N << endl;
            //*/
            if ((unsigned) dims_out[0] != matrix.rows()
                    && (unsigned) dims_out[1] != matrix.cols())
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(matrix, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset matrix size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into matrix
            H5::CompType complex_compound( sizeof(complex_type) );
            complex_compound.insertMember("r", HOFFSET(complex_type, r), H5::PredType::NATIVE_DOUBLE);
            complex_compound.insertMember("i", HOFFSET(complex_type, i), H5::PredType::NATIVE_DOUBLE);

            complex_type* buff = new complex_type[N];
            dataset.read(buff, complex_compound, memspace, dataspace);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                for (size_t jj = 0; jj < dims_out[1]; jj++) {
                    matrix(ii, jj) = buff[ii * dims_out[1] + jj].r + 1il * buff[ii * dims_out[1] + jj].i;
                }
            }

            dataset.close();
            delete buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException error ) {
            cout << "Error hdf5read_complex FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException error ) {
            cout << "Error hdf5read_complex DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException error ) {
            cout << "Error hdf5read_complex DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException error ) {
            cout << "Error hdf5read_complex DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5read_complex(Eigen::VectorXcd& vector, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            typedef struct complex_type{
                double r;
                double i;
            } complex_type;

            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the vector
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank != 1) {
                channelflow::cferror("Vector rank should be 1.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, NULL);

            int N = dims_out[0];

            //*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " x "
                << (unsigned long) (dims_out[1]) << " / "
                << N << endl;
            //*/

            if ((unsigned) dims_out[0] != vector.rows())
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(vector, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset vector size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into vector
            H5::CompType complex_compound( sizeof(complex_type) );
            complex_compound.insertMember("r", HOFFSET(complex_type, r), H5::PredType::NATIVE_DOUBLE);
            complex_compound.insertMember("i", HOFFSET(complex_type, i), H5::PredType::NATIVE_DOUBLE);

            complex_type* buff = new complex_type[N];
            dataset.read(buff, complex_compound, memspace, dataspace);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                vector(ii) = buff[ii].r + 1il * buff[ii].i;
            }

            dataset.close();
            delete buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException error ) {
            cout << "Error hdf5read_complex FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException error ) {
            cout << "Error hdf5read_complex DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException error ) {
            cout << "Error hdf5read_complex DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException error ) {
            cout << "Error hdf5read_complex DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

} // namespace channelfow
