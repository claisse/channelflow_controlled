/*
 * MIT License
 *
 * Copyright (c) 2018 Takamasa Horibe
 *
 * riccati_solver.h
 *
 * author of the header only: Geoffroy Claisse
 * author of the cpp file : Horibe Takamasa
 *
 */

#include <Eigen/Dense>
#include <iostream>
#include <time.h>
#include <vector>

bool solveRiccatiIterationC(
        const Eigen::MatrixXd &A,
        const Eigen::MatrixXd &B,
        const Eigen::MatrixXd &Q,
        const Eigen::MatrixXd &R,
        Eigen::MatrixXd &P,
        const double dt = 0.001,
        const double &tolerance = 1.E-5,
        const uint iter_max = 100000);

bool solveRiccatiIterationD(
        const Eigen::MatrixXd &Ad,                    
        const Eigen::MatrixXd &Bd,
        const Eigen::MatrixXd &Q,
        const Eigen::MatrixXd &R,
        Eigen::MatrixXd &P,
        const double &tolerance = 1.E-5,
        const uint iter_max = 100000);

bool solveRiccatiArimotoPotter(
        const Eigen::MatrixXd &A,
        const Eigen::MatrixXd &B,
        const Eigen::MatrixXd &Q,
        const Eigen::MatrixXd &R,
        Eigen::MatrixXd &P);



