// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("compute L2Dist(ua(ta), ub(tb)) as a function of (ta,tb)");
  ArgList args(argc, argv, purpose);

  const Real T0       = args.getreal("-T0",   "--T0", 0,   "start time");
  const Real T1       = args.getreal("-T1",   "--T1", 500, "end time");
  const Real dT       = args.getreal("-dT",   "--dT", 1.0, "save interval");
  const Real tmax     = args.getreal("-tmax", "--tmax", 200, "maximum t' for L2Dist(u(t), t(t+t'))");
  const string udirA  = args.getpath("-da", "--datadirA", "data", "flowfield series directory");
  const string udirB  = args.getpath("-db", "--datadirB", "data", "flowfield series directory");
  const string ulabel = args.getstr("-ul", "--ulabel", "u",  "flowfield filename label");
  const string ofile  = args.getstr("-o",  "--outfile", "dist", "output filename");
  const int digits    = args.getint("-dg", "--digits", 8,    "# digits in output");
  const bool xshift   = args.getflag("-xs", "--xshift", "translate ub by Lx/2");
  const bool zshift   = args.getflag("-zs", "--zshift", "translate ub by Lz/2");
  const bool ashift   = args.getflag("-as", "--autoshift", "report smallest dist among four translations");
  const int kxmax     = args.getint("-kx", "--kxmax", 0, "L2Norm for |kx|<kxmax, 0=>all");
  const int kzmax     = args.getint("-kz", "--kzmax", 0, "L2Norm for |kz|<kzmax, 0=>all");

  //const bool inttimeA = (abs(dTa - int(dTa)) < 1e-12) ? true : false;
  const bool inttime = (abs(dT - int(dT)) < 1e-12) ? true : false;

  args.check();
  args.save("./");

  const char s = ' ';

  FieldSymmetry tx(1,1,1,0.5,0.0);
  FieldSymmetry tz(1,1,1,0.0,0.5);
  FieldSymmetry txz(1,1,1,0.5,0.5);

  FieldSymmetry tau;
  if (xshift && zshift)
    tau = FieldSymmetry(1,1,1,0.5,0.5);
  else if (xshift)
    tau = FieldSymmetry(1,1,1,0.5,0.0);
  else if (zshift)
    tau = FieldSymmetry(1,1,1,0.0,0.5);

  ofstream os(appendSuffix(ofile, ".asc").c_str());

  os << "% ";
  for (int n=0; n<argc; ++n) {
    os << argv[n] << s;
  }
  os << setprecision(lesser(digits, 17));
  os << '\n';

  cout << "Projecting fields onto basis..." << endl;
  if (ashift) {

    string afile = ofile + "fund";
    string xfile = ofile + "x";
    string zfile = ofile + "z";
    string xzfile = ofile + "xz";

    ofstream aos(appendSuffix(afile, ".asc").c_str());
    ofstream xos(appendSuffix(xfile, ".asc").c_str());
    ofstream zos(appendSuffix(zfile, ".asc").c_str());
    ofstream xzos(appendSuffix(xzfile, ".asc").c_str());
    for (Real ta=T0; ta<=T1; ta += dT) {
      string uaname = ulabel + t2s(ta, inttime);
      cout << ta << ' ' << flush;
      FlowField ua(udirA + uaname);
      FlowField uax = tx(ua);
      FlowField uaz = tz(ua);
      FlowField uaxz = txz(ua);

      for (Real tb=ta; tb<=ta+tmax; tb += dT) {
	string ubname = ulabel + t2s(tb, inttime);
	FlowField ub(udirB + ubname);

	Real d   = L2Dist(ua,ub,kxmax,kzmax);
	Real dx  = L2Dist(uax,ub,kxmax,kzmax);
	Real dz  = L2Dist(uaz,ub,kxmax,kzmax);
	Real dxz = L2Dist(uaxz,ub,kxmax,kzmax);

	Real da;
	da = lesser(d,dx);
	da = lesser(da,dz);
	da = lesser(da,dxz);

	os << d << ' ';
	aos << da << ' ';
	xos << dx << ' ';
	zos << dz << ' ';
	xzos << dxz << ' ';
      }
      os << endl;
      aos << endl;
      xos << endl;
      zos << endl;
      xzos << endl;
    }
    cout << endl;
  }
  else {
    for (Real ta=T0; ta<=T1; ta += dT) {
      string uaname = ulabel + t2s(ta, inttime);
      cout << ta << ' ' << flush;
      FlowField ua(udirA + uaname);
      if (xshift || zshift) // doesn't matter if we shift ua or ub by halfbox
	ua *= tau;

      for (Real tb=ta; tb<=ta+tmax; tb += dT) {
	string ubname = ulabel + t2s(tb, inttime);
	FlowField ub(udirB + ubname);

	Real d = L2Dist(ua,ub,kxmax,kzmax);
	os << d << ' ';
      }
      os << endl;
    }
    cout << endl;
  }
  cout << "done!" << endl;
}
