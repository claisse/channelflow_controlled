// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("create a set of orthonormal basis set from linearly "
		 "independent fields.\n"
		 "usage: makebasis.x [options] u0 u1 u2 ...");

  ArgList args(argc, argv, purpose);

  const string odir  = args.getpath("-o", "--outdir", "", "output directory");
  //const bool orthog  = args.getbool("-g", "--orthog", true, "orthogonalize basis set");
  //const bool normal  = args.getbool("-n", "--normal", true, "normalize basis set");
  const string originname = args.getstr("-u0", "--origin", "", "origin for orthogonalization");

  if (args.helpmode())
    exit(1);

  mkdir(odir);
  args.save("./");

  // n0 is the argument index of the 0th (1st) field for basis construction
  const int n0 = 1 + (odir != "" ? 2:0) + (originname != "" ? 2:0);
  const int Nb = argc - n0;

  channelflow::array<FlowField> e(Nb);
  for (int n=0; n<Nb; ++n) {
    string uname = args.getstr(Nb-n, "<field>", "field to orthogonalize for basis set" );
    cout << "reading field " << uname << endl;
    e[n] = FlowField(uname);
    if (n>0 && !e[n].congruent(e[0])) {
      cerr << "FlowField " << argv[n0+n]
	   << " is not compatibale with FlowField " << argv[n0] << endl;
      exit(1);
    }
  }

  if (originname.length() != 0) {
    FlowField origin(originname);
    if (Nb > 0 && !e[0].congruent(origin)) {
      cerr << "origin FlowField " << originname
	   << " is not compatibale with FlowField " << argv[n0] << endl;
	exit(1);
      }
    for (int n=0; n<Nb; ++n)
      e[n] -= origin;
  }

  /*********************************
#ifdef HAVE_OCTAVE
  Matrix IP(Nb, Nb);
  for (int m=0; m<Nb; ++m) {
    for (int n=0; n<m; ++n) {
      IP(m,n) = L2IP(e[m], e[n]);
      IP(n,m) = IP(m,n);
    }
    IP(m,m) = L2Norm2(e[m]);
  }

  SVD svd(IP);
  DiagMatrix sigma = svd.singular_values();
  Real condition = sigma(0,0)/sigma(Nb-1,Nb-1);
  cout << "condition number of inner product matrix == " << condition << endl;
  if (condition < 1e-7)
    cerr << "WARNING : input basis set is pretty close to singular!" << endl;
#endif
  *********************************/

  //if (orthog && normal)
  //orthonormalize(e);
  //else if (orthog)
  //orthogonalize(e);
  //else if (normal)
  //normalize(e);

  orthonormalize(e);

  for (int n=0; n<Nb; ++n)
    e[n].save(odir + "e" + i2s(n));
}

/* makebasis.cpp: make a basis set from a set of fields onto a given basis
 * channelflow-1.1 PCF-utils
 *
 * Copyright (C) 2001-2007  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu  jfg@member.fsf.org
 *
 * Center for Nonlinear Science
 * School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 * 404 385 2509
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation version 2
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
