// License declaration at end of file

// Program for writing HDF5 files from flowfield data, essentially a
// modification of field2ascii. Some of the code and the accopmanying
// comments are taken from the HDF group website (www.hdfgroup.org).

#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("convert FlowField .ff file to  HDF5 file");
  ArgList args(argc, argv, purpose);

  const string iname = args.getstr(2, "<fieldname>",   "flowfield input file");
  const string oname = args.getstr(1, "<hdf5file>",    "hdf5 output file");

  args.check();

  FlowField u(iname);
  u.hdf5Save(oname);
}
