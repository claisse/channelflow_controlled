#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("make a FlowField that is an x-constant eigenfunction of\n"
		 "the linearized NS about the laminar equilibrium, of type\n"
		 "cos(pi ky y/2) cos(2pi kz z/Lz)) ex (even mode) or\n"
		 "sin(pi ky y/2) cos(2pi kz z/Lz)) ex (odd mode)");

  ArgList args(argc, argv, purpose);

  const string odir   = args.getpath("-o", "--outdir", ".", "output directory");
  //const Real Reynolds = args.getreal("-R", "--Reynolds", 400.0, "Reynolds number");
  const Real alpha    = args.getreal("-a", "--alpha", 1.14, "Lx/2 pi");
  const Real gamma    = args.getreal("-g", "--gamma", 2.5,  "Lz/2 pi");
  const int Nx = args.getint("-Nx", "--Nx", 48, "number of x grid points");
  const int Ny = args.getint("-Ny", "--Ny", 35, "number of y grid points");
  const int Nz = args.getint("-Nz", "--Nz", 48, "number of z grid points");
  const int kz    = args.getint("-kz", "--kz", 1, "cos(2pi kz z/Lz) variation");
  const int ky    = args.getint("-ky", "--ky", 2, "sin,cos(pi ky z/Lz) variation");

  const Real a = -1;
  const Real b = 1;
  args.check();
  args.save(odir);

  Vector y = chebypoints(Ny,a,b);
  FlowField u(Nx, Ny, Nz, 3, 2*M_PI/alpha, 2*M_PI/gamma, -1, 1, Spectral, Physical);

  Real piky2 = 0.5*pi*ky;
  if (ky%2 == 1)
    for(int ny=0; ny<Ny; ++ny)
      u.cmplx(0,ny,kz,0) = cos(piky2*y[ny]);
  else
    for(int ny=0; ny<Ny; ++ny)
      u.cmplx(0,ny,kz,0) = sin(piky2*y[ny]);

  u.makeSpectral();
  u *= 1.0/L2Norm(u);
  u.save(odir + "uheat" + i2s(ky) + i2s(kz));

  cout << "divNorm(u) == " << divNorm(u) << endl;
  cout << "bcNorm(u)  == " << bcNorm(u) << endl;
}
