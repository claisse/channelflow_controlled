// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/turbstats.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("produce I,D,E, time series and means from u(x,t) time series");
  ArgList args(argc, argv, purpose);

  const Real T0        = args.getreal("-T0", "--T0", 0,   "start time");
  const Real T1        = args.getreal("-T1", "--T1", 500, "end time");
  /***/ Real dT        = args.getreal("-dT", "--dT", 1.0, "save interval");
   const bool adjustdT = args.getflag("-adT","--adjustdT", "adjust dT so that it evenly divides (T1-T0)");
  const string udir    = args.getpath("-d",  "--datadir", "data/", "flowfield series directory");
  const string ulabel  = args.getstr ("-ul", "--ulabel",  "u",  "flowfield filename label");
  /***/ string odir    = args.getpath("-o",  "--outdir",  "", "output directory, default==datadir");
  const bool closed    = args.getflag("-c",  "--closed",  "closed orbit, so don't count connecting point twice");
  const bool turbstats = args.getflag("-t",  "--turbstats",  "compute reynolds stresses");

  const string Uname   = args.getstr("-U", "--Ubase", "", "base flow, one of [zero|linear|parabolic|<filename>]");
  const int  normstyle = args.getint("-nrm",  "--normstyle", 0, "output I,D,E as e.g. 0: I(u+U)/Lx*Ly*Lz, 1:I(u)/Lx, 2:I(u), 3:I(u+U)/Lx)");

  const bool stddev    = args.getflag("-sd",  "--stddev",  "compute std dev <|u-<u>|^2>^(1/2)");
  /***/ int digits     = args.getint ("-dg", "--digits",  8,  "# digits in output");
  const Real Reynolds  = args.getreal("-R", "--Reynolds", 0, "Reynolds numbers");
  /***/ Real Restar    = args.getreal("-Rs", "--Restar", 0, "wall Reynolds number (0 => use nu)");
  /***/ Real nu        = args.getreal("-nu", "--nu",     0.0025, "kinematic viscosity");

  args.check();
  args.save("./");

  if (odir == "")
    odir = udir;
  else
    mkdir(odir);

  const char s = ' ';

  if (adjustdT) {
    // Use timestep dt simply to adjust dT to evenly fit (T1-T0)
    TimeStep dt(0.01, 0.001, 0.1, dT, 0, 1, false);
    dt.adjust_for_T(T1-T0, true);
    cout << setprecision(16);
    cout << "Adjusting time steps to evenly divide T1-T0:\n";
    cout << "dt == " << dt.dt() << endl;
    cout << "dT == " << dt.dT() << endl;
    cout << setprecision(6);    
    dT = dt.dT();
  }
  const bool inttime = (abs(dT - int(dT)) < 1e-12) ? true : false;
  digits = lesser(abs(digits), 17);
  const int width = digits + 6;

  FlowField umean(udir + ulabel + t2s(T0, inttime));
  umean.setToZero();

  const int Nx = umean.Nx();
  const int Ny = umean.Ny();
  const int Nz = umean.Nz();
  const Real Lx = umean.Lx();
  const Real Lz = umean.Lz();
  const Real ya = umean.a();
  const Real yb = umean.b();
  FlowField tmp(Nx,Ny,Nz,9,Lx,Lz,ya,yb);

  Vector y = chebypoints(Ny, ya, yb);
  ChebyCoeff U(Ny,ya,yb,Spectral);

  if (Uname == "parabolic" || Uname == "channel") {
    U[0] = 0.5;
    U[2] = -0.5;
  }
  else if (Uname == "linear" || Uname == "planecouette" || Uname == "pcf") 
    U[1] = 1;
  else if (Uname == "zero")
    ;
  else if (Uname == "")
    U = ChebyCoeff(udir + "Ubase");
  else
    U = ChebyCoeff(Uname);

  U.makeSpectral();
  
  if (Reynolds != 0.0)
    nu = 1.0/Reynolds;
  else if (Restar != 0.0) {
    Real dUdy = abs(U.slope_a());
    Real h = 0.5*(yb-ya);
    nu = dUdy * square(h/Restar);
  }

  TurbStats stats;
  if (turbstats)
    stats = TurbStats(U, nu);
  
  ofstream os((odir + "tIDE.asc").c_str());

  os << "% ";
  for (int n=0; n<argc; ++n)
    os << argv[n] << s;
  os << endl;
  os << "%t I D E E3d with ";
  switch (normstyle) {
  case 0: os << "I(u+U)/V normalization\n"; break;
  case 1: os << "I(u)/Lx normalization\n"; break;
  case 2: os << "I(u) unnormalized\n"; break;
  case 3: os << "I(u+U)/Lx normalized\n"; break;
  }
  os << setprecision(digits);

  Real I = 0.0;
  Real D = 0.0;
  Real E = 0.0;
  Real En = 0.0;
  Real E3 = 0.0;
  int count = 0;

  for (Real t=T0; t<=T1+dT/2; t += dT) {

    string uname = ulabel + t2s(t, inttime);
    cout << uname << s << flush;
    FlowField u(udir + uname);
    umean += u;
    if (turbstats)
      stats.addData(u,tmp);

    Real en = 0.5*L2Norm2(u);

    Real i,d,e,e3;
    switch (normstyle) {
    case 1: { // u alone normalized by Lx
      i  = wallshear(u,false)/Lx;
      d  = dissipation(u,false)/Lx;
      e  = 0.5*L2Norm2(u,false)/Lx;
      e3 = 0.5*L2Norm2_3d(u,false)/Lx;
      break;
    }
    case 2: { // u alone unnormalized
      i  = wallshear(u,false);
      d  = dissipation(u,false);
      e  = 0.5*L2Norm2(u,false);
      e3 = 0.5*L2Norm2_3d(u,false);
      break;
    }
    case 3: { // u+U normalized by Lx
      u += U;
      i  = wallshear(u,false)/Lx;
      d  = dissipation(u,false)/Lx;
      e  = 0.5*L2Norm2(u,false)/Lx;
      e3 = 0.5*L2Norm2_3d(u,false)/Lx;
      u -= U;
      break;
    }
    default: { // u+Ubase normalized by volume, case 0:
      u += U;
      i  = wallshear(u);
      d  = dissipation(u);
      e  = 0.5*L2Norm2(u);
      e3 = 0.5*L2Norm2_3d(u);
      u -= U;
      break;
    }}

    os << setw(width) << left << t 
       << setw(width) << left << i
       << setw(width) << left << d 
       << setw(width) << left << e 
       << setw(width) << left << e3 << endl;

    // If orbit is closed, skip first value so as not to count it twice
    if (closed && t==T0)
      continue;

    I += i;
    D += d;
    E += e;
    En += en;
    E3 = e3;    
    ++count;
    }
    ofstream os2((odir + "IDE.asc").c_str());
  os2 << "% <wallshear(u+U)> <dissipation(u+U)> <energy(u+U)> <energy(u)> <energy3d(u)>" << endl;
  os2 << I/count <<s<< D/count <<s<< E/count <<s<< En/count <<s<< E3/count << endl;

  umean *= 1.0/count;
  umean.save(odir + "umean");

  stats.msave(odir + "stats", false);
  stats.msave(odir + "wallstats", true);

  if (stddev) {
    cout << "Computing std dev ..." << flush;
    Real sum = 0.0;
    count = 0;
    for (Real t=T0; t<=T1; t += dT) {
      string uname = ulabel + t2s(t, inttime);
      cout << uname << s << flush;
      FlowField u(udir + uname);
      u -= umean;
      sum += L2Norm2(u);
      ++count;
    }
    cout << endl;
    cout << "std dev == " << sqrt(sum/count) << endl;
  }
  cout << "done!" << endl;
}

/* project.cpp: project time series of fields onto a given basis
 * channelflow-1.1 PCF-utils
 *
 * Copyright (C) 2001-2007  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu  jfg@member.fsf.org
 *
 * Center for Nonlinear Science
 * School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 * 404 385 2509
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation version 2
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
