#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("save FlowField as ascii file of gridpoint values, in format\n"
		 "\tfor (int nx=0; nx<Nx; ++nx) \n"
		 "\t  for (int ny=0; ny<Ny; ++ny) \n"
		 "\t    for (int nz=0; nz<Nz; ++nz) \n"
		 "\t      for (int i=0; i<Nd; ++i) \n"
		 "\t        os << u(nx,ny,nz,i) << '\\n';\n"
		 "\tFields with 2/3-style padding for dealiasing are saved on reduced grids (Nx,Nz -> 2Nx/3, 2Nz/3).");

  ArgList args(argc, argv, purpose);

  const bool padding = args.getflag("-p", "--padding", "save full Nx x Ny x Nz grid rather eliminating padded modes");
  const bool gridpoints  = args.getflag("-g", "--gridpoints",  "write x.asc, y.asc, z.asc gridpoint files");
  const string uname = args.getstr(2, "<fieldname>",      "flowfield input file");
  const string afile = args.getstr(1, "<asciifile>",      "ascii output file");

  args.check();

  FlowField u(uname);

  if (!padding && u.padded()) {
    int Nx = (2*u.Nx())/3;
    int Nz = (2*u.Nz())/3;
    FlowField v(Nx,u.Ny(),Nz,u.Nd(),u.Lx(),u.Lz(),u.a(), u.b());
    v.interpolate(u);
    u = v;
  }
  if (gridpoints) {
    Vector x = u.xgridpts();
    Vector y = u.ygridpts();
    Vector z = u.zgridpts();
    x.save("x");
    y.save("y");
    z.save("z");
  }

  u.makeState(Physical, Physical);
  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();
  const int Nd = u.Nd();


  int p = 16;  // digits of precision
  int w = p+7; // width of field

  ofstream os(appendSuffix(afile, ".asc").c_str());
  os << setprecision(p) << scientific;
  for (int nx=0; nx<Nx; ++nx)
    for (int ny=0; ny<Ny; ++ny)
      for (int nz=0; nz<Nz; ++nz)
	for (int i=0; i<Nd; ++i)
	  os << u(nx,ny,nz,i) << '\n';


  ofstream gs(appendSuffix(removeSuffix(afile, ".asc"), ".geom").c_str());
  gs << setprecision(p);
  gs.setf(ios::left);
  gs << setw(w) << u.Nx() << " %Nx\n";
  gs << setw(w) << u.Ny() << " %Ny\n";
  gs << setw(w) << u.Nz() << " %Nz\n";
  gs << setw(w) << u.Nd() << " %Nd\n";
  gs << setw(w) << u.Lx() << " %Lx\n";
  gs << setw(w) << u.Lz() << " %Lz\n";
  gs << setw(w) << u.Lx()/(2*pi) << " %lx=Lx/(2pi)\n";
  gs << setw(w) << u.Lz()/(2*pi) << " %lz=Lz/(2pi)\n";
  gs << setw(w) << 2*pi/u.Lx()   << " %alpha=2pi/Lx\n";
  gs << setw(w) << 2*pi/u.Lz()   << " %gamma=2pi/Lz\n";
}
