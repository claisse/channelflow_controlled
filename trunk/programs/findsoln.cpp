#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
//#include <octave/oct.h>
//#include <octave/CColVector.h>
//#include <octave/dColVector.h>
//#include <octave/dMatrix.h>
//#include <octave/EIG.h>
//#include <octave/dbleSVD.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/matrixutils.h"


using namespace std;
using namespace channelflow;


// This program is an implementation of Divakar Viswanath's Newton-hookstep
// GMRES algorithm for computing invariant solutions of Navier-Stokes. The
// basic idea is to find solutions of sigma f^T(u) - u = 0 where f^T(u) is the
// time-T CFD integration of the fluid velocity field u and sigma is a symmetry
// operation. T and sigma can be held fixed or varied. Viswanath's algorithm
// combines a Newton-hookstep trust-region minimization of ||sigma f^T(u) - u||^2
// with iterative GMRES solution of the Newton-step equations.

// In this program, the details of GMRES algorithm are modeled on Lecture 35
// of Trefethen and Bau's "Numerical Linear Algebra". The details of the hookstep
// algorithm are based on section 6.4.2 of Dennis and Schnabel's "Numerical Methods
// for Unconstrained Optimization and Nonlinear Equations."
//
// See octutils.h for a detailed description of the algorithm, notation,
// and references.
//
// John Gibson, Wed Oct 10 16:10:46 EDT 2007


int main(int argc, char* argv[]) {

  string purpose("Newton-Krylov-hookstep search for (relative) equilibrium or periodic orbit solution of plane Couette flow");
  ArgList args(argc, argv, purpose);

  GMRESHookstepFlags srchflags;

  const bool eqb        = args.getflag("-eqb",  "--equilibrium",   "search for equilibrium or relative equilibrium (trav wave)");
  const bool orb        = args.getflag("-orb",  "--periodicorbit", "search for periodic orbit or relative periodic orbit");
  srchflags.poinconst   = args.getflag("-poincconst",  "--poincareconstraint",   "periodic orbit search with u constrained to I-D=0 Poincare section via DNS");
  srchflags.poincsrch   = args.getflag("-poincsrch",  "--poincaresearch",        "periodic orbit search with I-D=0 as a search equation");
  srchflags.xrelative   = args.getflag("-xrel",  "--xrelative",    "search over x phase shift for relative orbit or eqb");
  srchflags.zrelative   = args.getflag("-zrel",  "--zrelative",    "search over z phase shift for relative orbit or eqb");

  const Real Reynolds   = args.getreal("-R", "--Reynolds", 400.0, "pseudo-Reynolds number == 1/nu");
  const Real nuarg      = args.getreal("-nu", "--nu",        0.0, "kinematic viscosity (takes precedence over Reynolds, if nonzero)");
  /***/ Real T          = args.getreal("-T", "--maptime",  20.0,  "initial guess for orbit period or time of eqb/reqb map f^T(u)");
  
  const string sigstr   = args.getstr ("-sigma", "--sigma", "",    "file containing symmetry of relative solution (default == identity)");
  const string symmstr  = args.getstr ("-symms", "--symmetries", "", "file containing generators of isotropy group for symmetry-constrained search");
  const string initstr  = args.getstr ("-is", "--initstepping", "smrk2", "timestepping algorithm for initializing multistep algorithms, "
					" one of [befe1 cnrk2 smrk2 sbdf1]");
  const string stepstr  = args.getstr ("-ts", "--timestepping", "sbdf3", "timestepping algorithm, one of [befe1 cnab2 smrk2 sbdf1 sbdf2 sbdf3 sbdf4]");
  const string nonlstr  = args.getstr ("-nl", "--nonlinearity", "rot", "method of calculating nonlinearity, one of [rot conv div skew alt]");
  
  const string meanstr= args.getstr("-mc", "--meanconstraint", "gradp", "gradp|bulkv : hold one fixed");
  const Real dPdx     = args.getreal("-dPdx", "--dPdx",   0.0, "value for fixed pressure gradient, x direction (streamwise)");
  const Real dPdz     = args.getreal("-dPdz", "--dPdz",   0.0, "value for fixed pressure gradient, z direction (spanwise)");
  const Real Ubulk    = args.getreal("-Ubulk", "--Ubulk",  0.0, "value for fixed bulk velocity, x direction (streamwise)");
  const Real Wbulk    = args.getreal("-Wbulk", "--Wbulk",  0.0, "value for fixed bulk velocity, z direction (spanwise)");
  const Real Uwall    = args.getreal("-Uwall", "--Uwall",  1.0, "magnitude of wall velocity, walls move at +/-Uwall");
  const Real theta    = args.getreal("-theta", "--theta",  0.0, "angle of wall velocity to x-axis");

  //const string velstr = args.getstr("-vs", "--velocityscale",  "wall", "wall|parab|bulk : velocity scale for Reynolds");
  
  const bool vardt     = args.getflag("-vdt","--variableDt", "adjust dt to keep CFLmin<=CFL<CFLmax");
  const Real dtarg     = args.getreal("-dt","--timestep",  0.03125, "(initial) integration timestep");
  const Real dtmin     = args.getreal("-dtmin", "--dtmin", 0.001, "minimum timestep");
  const Real dtmax     = args.getreal("-dtmax", "--dtmax", dtarg,  "maximum timestep");
  const Real dTCFL     = args.getreal("-dTCFL", "--dTCFL", 1.00,  "check CFL # at this interval");
  const Real CFLmin    = args.getreal("-CFLmin", "--CFLmin", 0.55, "minimum CFL number");
  const Real CFLmax    = args.getreal("-CFLmax", "--CFLmax", 0.65, "maximum CFL number");

  srchflags.epsSearch = args.getreal("-es",  "--epsSearch",   1e-13, "stop search if L2Norm(s f^T(u) - u) < epsEQB");
  srchflags.epsKrylov = args.getreal("-ek",  "--epsKrylov",   1e-14, "min. condition # of Krylov vectors");
  srchflags.epsDu     = args.getreal("-edu", "--epsDuLinear", 1e-7,  "relative size of du to u in linearization");
  srchflags.epsDt     = args.getreal("-edt", "--epsDtLinear", 1e-7,  "size of dT in linearization of f^T about T");
  srchflags.epsGMRES  = args.getreal("-eg",  "--epsGMRES",    1e-3,  "stop GMRES iteration when Ax=b residual is < this");
  srchflags.epsGMRESf = args.getreal("-egf",  "--epsGMRESfinal",  0.05,  "accept final GMRES iterate if residual is < this");
  srchflags.centdiff  = args.getflag("-cd", "--centerdiff", "centered differencing to estimate differentials");

  srchflags.Nnewton   = args.getint( "-Nn", "--Nnewton",   20,    "max number of Newton steps ");
  srchflags.Ngmres    = args.getint( "-Ng", "--Ngmres",   120,    "max number of GMRES iterations per restart");
  srchflags.Nhook     = args.getint( "-Nh", "--Nhook",     20,    "max number of hookstep iterations per Newton");

  srchflags.delta     = args.getreal("-d",    "--delta",      0.01,  "initial radius of trust region");
  srchflags.deltaMin  = args.getreal("-dmin", "--deltaMin",   1e-12, "stop if radius of trust region gets this small");
  srchflags.deltaMax  = args.getreal("-dmax", "--deltaMax",   0.1,   "maximum radius of trust region");
  srchflags.deltaFuzz = args.getreal("-df",   "--deltaFuzz",  1e-06,  "accept steps within (1+/-deltaFuzz)*delta");
  srchflags.lambdaMin = args.getreal("-lmin",   "--lambdaMin", 0.2,  "minimum delta shrink rate");
  srchflags.lambdaMax = args.getreal("-lmax",   "--lambdaMax", 1.5,  "maximum delta expansion rate");
  srchflags.lambdaRequiredReduction = 0.5; // when reducing delta, reduce by at least this factor.
  srchflags.improvReq = args.getreal("-irq",  "--improveReq", 1e-3,  "reduce delta and recompute hookstep if improvement "
				      " is worse than this fraction of what we'd expect from gradient");
  srchflags.improvOk  = args.getreal("-iok",  "--improveOk",   0.10, "accept step and keep same delta if improvement "
				      "is better than this fraction of quadratic model");
  srchflags.improvGood= args.getreal("-igd",  "--improveGood", 0.75, "accept step and increase delta if improvement "
				      "is better than this fraction of quadratic model");
  srchflags.improvAcc = args.getreal("-iac",  "--improveAcc",  0.10, "recompute hookstep with larger trust region if "
				      "improvement is within this fraction quadratic prediction.");

  srchflags.TscaleRel = args.getreal("-Tsc", "--Tscale",     20,    "scale time in hookstep: |T| = Ts*|u|/L2Norm(du/dt)");
  srchflags.dudtortho = args.getbool("-tc",  "--dudtConstr", true,  "require orthogonality of step to dudt");
  srchflags.orthoscale= args.getreal("-os",  "--orthoScale", 1,     "rescale orthogonality constraint");
  srchflags.dudxortho = args.getbool("-xc", "--dudxConstraint", true,"require orthogonality of step to dudx and dudz");
  srchflags.Rscale    = args.getreal("-rs", "--relativeScale", 1, "scale relative-search variables by this factor");
  srchflags.unormalize= args.getreal("-un", "--unormalize",  0.0, "normalize residual by 1/(L2Norm3d(f^T(u))-unormalize), to keep away from u=0 laminar soln");
  srchflags.saveall   = args.getflag("-sn",  "--savenewtsteps", "save each newton step");
  srchflags.outdir    = args.getpath("-o",  "--outdir", "./", "output directory");
  const string logfile  = args.getstr ("-log","--logfile", "stdout",  "output log (filename or \"stdout\")");
  const string uname    = args.getstr(1, "<flowfield>", "initial guess for Newton search");

  if (eqb && !orb)
    srchflags.solntype = Equilibrium;
  else if (orb && !eqb)
    srchflags.solntype = PeriodicOrbit;
  else {
    cerr << "Please choose either -eqb or -orb option to search for (relative) equilibrium or (relative) periodic orbit" << endl;
    exit(1);
  }
  args.check();
  args.save("./");

  PoincareCondition* hpoincare = (srchflags.poinconst || srchflags.poincsrch) ? new DragDissipation() : 0;

  TimeStep dt(dtarg, dtmin, dtmax, dTCFL, CFLmin, CFLmax, vardt);
  FieldSymmetry sigma;
  if (sigstr.length() != 0)
    sigma = FieldSymmetry(sigstr);

  FlowField u(uname);
  Real CFL;
  //Real h = (u.b() - u.a())/2;
  //VelocityScale vscale = s2velocityscale(velstr);

  DNSFlags dnsflags;
  dnsflags.dealiasing   = DealiasXZ;
  dnsflags.initstepping = s2stepmethod(initstr);
  dnsflags.timestepping = s2stepmethod(stepstr);
  dnsflags.nonlinearity = s2nonlmethod(nonlstr);
  dnsflags.constraint   = s2constraint(meanstr);
  dnsflags.baseflow     = LaminarBase;
  dnsflags.dPdx         = dPdx;
  dnsflags.dPdz         = dPdz;
  dnsflags.Ubulk        = Ubulk;
  dnsflags.Wbulk        = Wbulk;
  dnsflags.ulowerwall   = -Uwall*cos(theta);
  dnsflags.uupperwall   =  Uwall*cos(theta);
  dnsflags.wlowerwall   = -Uwall*sin(theta);
  dnsflags.wupperwall   =  Uwall*sin(theta);
  dnsflags.nu           = (nuarg != 0) ? nuarg : 1.0/Reynolds;
  dnsflags.t0           = 0.0;
  dnsflags.verbosity    = Silent;
  dnsflags.dt           = dt;
  //dnsflags.save("dnsflags-orig");

  Real ubulk = Re(u.profile(0,0,0)).mean();
  Real wbulk = Re(u.profile(0,0,2)).mean();
  if (abs(ubulk) < 1e-15) ubulk = 0.0;
  if (abs(wbulk) < 1e-15) wbulk = 0.0;
  
  ChebyCoeff Ubase =  laminarProfile(dnsflags.nu, dnsflags.constraint, dnsflags.dPdx, dnsflags.Ubulk-ubulk, 
				     u.a(), u.b(), dnsflags.ulowerwall, dnsflags.uupperwall, u.Ny());
  Ubase.save("Ubase");

  ofstream logstream;
  if (logfile == "stdout" || logfile == "cout") {
    srchflags.logstream = &cout;
    dnsflags.logstream = &cout;
  }
  else {
    logstream.open((srchflags.outdir+logfile).c_str());
    srchflags.logstream = &logstream;
    dnsflags.logstream = &logstream;
  }
  ostream* os = srchflags.logstream; // a short name for ease of use

  if (symmstr.length() > 0) {
    SymmetryList symms(symmstr);
    *os << "Restricting flow to invariant subspace generated by symmetries" << endl;
    *os << symms << endl;
    dnsflags.symmetries = symms;
  }

  *os << "Working directory == " << pwd() << endl;
  *os << "Command-line args == ";
  for (int i=0; i<argc; ++i) *os << argv[i] << ' '; *os << endl;

  *os << setprecision(16);
  *os << " 1/nu == " << 1/dnsflags.nu << " (pseudo-Reynolds)" << endl;
  *os << "   nu == " << dnsflags.nu << endl;
  *os << "sigma == " << sigma << endl;
  *os << "    T == " << T << endl;
  *os << "   dt == " << dt << endl;
  //*os << "DNSFlags == " << dnsflags << endl << endl;
  //cout << "searchflags == " << srchflags << endl;
  *os << setprecision(8);

  Real residual = GMRESHookstep(u, T, sigma, hpoincare, srchflags, dnsflags, dt, CFL);

  *os << "Final search residual is " << residual << endl;

  // No need to save data, the GMRESHookstep function saves results to disk at each step.

  //*os << "Saving final values of u,T,sigma residual is " << residual << endl;
  //u.save(srchflags.outdir + "usoln");
  //sigma.save(srchflags.outdir + "sigmasoln");
  //save(T, srchflags.outdir + "Tsoln");
  //save(Reynolds, srchflags.outdir + "Resoln");

  //fftw_cleanup();
}




/* findorbit.cpp, channelflow-1.3, www.channelflow.org
 *
 * Copyright (C) 2001-2009  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu
 * Center for Nonlinear Sciences, School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
