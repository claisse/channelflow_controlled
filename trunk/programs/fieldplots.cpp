#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <sys/stat.h>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("produces matlab-readable ascii files of slices and "
		 "spectral properties of a FlowField");

  ArgList args(argc, argv, purpose);

  /***/ string label  = args.getstr ("-l", "--label", "",   "output file stub, defaults to input field stub");
  const string outdir = args.getpath("-o", "--outdir", ".", "output directory");
  const string Uname  = args.getstr ("-U", "--Ubase", "",   "base flow profile");
  const bool nolog    = args.getflag("-nl", "--nolog",      "don't save command-line arguments to log file");
  const bool spectra  = args.getflag("-s", "--spectra",     "output Fourier and Chebyshev spectra");
  const bool padding  = args.getflag("-pd","--showpad",     "include padded modes in spectra");
  const bool xavg     = args.getflag("-xa", "--xavg",       "plot x-avg u,v,w");
  const bool yavg     = args.getflag("-ya", "--yavg",       "plot y-avg u,w");
  const bool zenergy  = args.getflag("-ze", "--zenergy",    "plot xy-average energy density as function of z");
  const bool yenergy  = args.getflag("-ye", "--yenergy",    "plot xz-average energy density as function of y");
  const bool yavgenergy  = args.getflag("-yae", "--yavgenergy",    "plot y-average energy density as function of x,z");
  const bool profile  = args.getflag("-p", "--profiles",    "plot y-profiles of Fourier modes");
  const bool range    = args.getflag("-r", "--range",       "plot a range of Fourier modes");
  const bool uu       = args.getflag("-uu", "--uu",         "plot <uu>(y) (xz-spatial avg of u component)");
  const bool vv       = args.getflag("-vv", "--vv",         "plot <vv>(y) (xz-spatial avg of v component)");
  const bool ww       = args.getflag("-ww", "--ww",         "plot <ww>(y) (xz-spatial avg of w component)");

  //const bool swirl    = args.getflag("-sw", "--swirl",      "output swirling strength on same planes as u,v,w");
  const int Kx        = args.getint ("-kx","--kx", 0,       "max kx for Fourier mode profile");
  const int Kz        = args.getint ("-kz","--kz", 0,       "max kz for Fourier mode profile");
  const int xstride   = args.getint ("-xs","--xstride", 1,  "output every xs-th gridpoint value");
  const int ystride   = args.getint ("-ys","--ystride", 1,  "output every ys-th gridpoint value");
  const int zstride   = args.getint ("-zs","--zstride", 1,  "output every zs-th gridpoint value");
  const int nx        = args.getint ("-nx","--nx",  0,      "output (y,z) planes at nxth Fourier gridpoint");
  const int ny        = args.getint ("-ny","--ny", -1,      "output (x,z) planes at nyth Chebyshev gridpoint (default midplane");
  const int nz        = args.getint ("-nz","--nz",  0,      "output (x,y) planes at nzth Fourier gridpoint");

  const string uname  = args.getstr (1, "<flowfield>",      "input field");

  args.check();
  if (!nolog) 
    args.save();

  if (label.length() == 0) {
    label = stub(uname, ".ff");
    label = stub(label, ".h5");
  }

  FlowField u(uname);
  u.makeSpectral();

  if (Uname == "parabolic") {
    ChebyCoeff U(u.Ny(), u.a(), u.b(), Spectral);
    U[0] =  0.5;
    U[2] = -0.5;
    u += U;
  }
  else if (Uname == "linear") {
    ChebyCoeff U(u.Ny(), u.a(), u.b(), Spectral);
    U[1] = 1.0;
    u += U;
  }
  else if (Uname != "") {
    ChebyCoeff U(Uname);
    U.makeSpectral();
    u += U;
  }

  if (spectra)
    plotspectra(u, outdir, label, padding);

  if (xavg)
    plotxavg(u, outdir, label);

  if (yavg)
    plotyavg(u, outdir, label);

  if (yavgenergy)
    plotyavg_energy(u, outdir, label);

  if (zenergy || yenergy) {
    FlowField e = energy(u);
    e.makeSpectral();
    if (zenergy) {
      PeriodicFunc exyavg(e.Nz(), e.Lz(), Spectral);
      for (int mz=0; mz<e.Mz(); ++mz)
	exyavg.cmplx(mz) = e.cmplx(0,0,mz,0);
      exyavg.save(outdir+label+"_zenergy");
    }
    if (yenergy) {
      ChebyCoeff  exzavg(e.Ny(), e.a(), e.b());
      for (int ny=0; ny<e.Ny(); ++ny)
	exzavg[ny] = Re(e.cmplx(0,ny,0,0));
      exzavg.save(outdir+label+"_yenergy");
    }
  }

  if (profile) {
    if (range) {
      if (-abs(Kx) < u.kxmin() || abs(Kx) > u.kxmax() || abs(Kz) > u.kzmax()) {
	cerr << "kx,kz out of range for given flowfield. run fieldprops.x --spec <flowfield>" << endl;
	exit(1);
      }
      for (int kx=0; kx<=Kx; ++kx)
	for (int kz=0; kz<=Kz; ++kz)
	  u.profile(u.mx(kx), u.mz(kz)).save(outdir + label + "_" + i2s(kx) + "_" + i2s(kz));
    }
    else {
      if (Kx < u.kxmin() || Kx > u.kxmax() || Kz < 0 || Kz > u.kzmax()) {
	cerr << "kx,kz out of range for given flowfield. run fieldprops.x --spec <flowfield>" << endl;
	exit(1);
      }
      u.profile(u.mx(Kx), u.mz(Kz)).save(outdir + label + i2s(Kx) + i2s(Kz));
    }
  }
  if (uu || vv || ww) {
    FlowField u2 = u;
    u2.makePhysical();
    for (int i = 0; i<u.Nd(); ++i)
      for (int ny = 0; ny<u.Ny(); ++ny)
	for (int nx = 0; nx<u.Nx(); ++nx)
	  for (int nz = 0; nz<u.Nz(); ++nz)
	    u2(nx,ny,nz,i) = square(u2(nx,ny,nz,i));

    u2.makeSpectral();
    if (uu) u2.profile(0,0,0).save(outdir + label + "_uu");
    if (vv) u2.profile(0,0,1).save(outdir + label + "_vv");
    if (ww) u2.profile(0,0,2).save(outdir + label + "_ww");
  }

  plotfield(u, outdir, label, xstride, ystride, zstride, nx, ny, nz);

  Vector x = periodicpoints(u.Nx(), u.Lx());
  Vector y = chebypoints(u.Ny(), u.a(), u.b());
  Vector z = periodicpoints(u.Nz(), u.Lz());
  ofstream osx((outdir + label + "_x.asc").c_str());
  ofstream osy((outdir + label + "_y.asc").c_str());
  ofstream osz((outdir + label + "_z.asc").c_str());
  for (int nx=0; nx<=u.Nx(); nx += xstride)
    osx << x(nx) << '\n';
  for (int ny=0; ny<u.Ny(); ny += ystride)
    osy << y(ny) << '\n';
  for (int nz=0; nz<=u.Nz(); nz += zstride)
    osz << z(nz) << '\n';
}
