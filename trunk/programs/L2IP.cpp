#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <sys/stat.h>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("compute the inner product of two fields : L2IP(u0,u1)");

  ArgList args(argc, argv, purpose);

  const int  digits    = args.getint ("-d", "--digits", 7, "number of digits");
  const bool normalize = args.getflag("-n", "--normalize", "normalize by norms of each field");
  const string u0name  = args.getstr (2, "<flowfield>", "input field #1");
  const string u1name  = args.getstr (1, "<flowfield>", "input field #2");
  cout << setprecision(digits);

  args.check();

  FlowField u0(u0name);
  FlowField u1(u1name);

  u0.makeSpectral();
  u1.makeSpectral();
  if (!u0.congruent(u1))
    cferror("fields are not congruent. exiting");

  if (normalize)
    cout << L2IP(u0,u1)/(L2Norm(u0)*L2Norm(u1)) << endl;
  else
    cout << L2IP(u0,u1) << endl;
}
