#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;


// This code needs better documentation. Write-up of stokes modes is in gibson blog circa 2006.

int main(int argc, char* argv[]) {

  string purpose("produce a Stokes mode of a given quantum number for given geometry and Reynolds");
  ArgList args(argc, argv, purpose);

  /***/ Real alpha   = args.getreal("-a", "--alpha", 0.0, "Lx = 2 pi/alpha");
  /***/ Real gamma   = args.getreal("-g", "--gamma", 0.0, "Lz = 2 pi/gamma");
  const Real lx      = (alpha == 0) ? args.getreal("-lx", "--lx", 0.0,   "new Lx = 2 pi lx") : 1/alpha;
  const Real lz      = (gamma == 0) ? args.getreal("-lz", "--lz", 0.0,   "new Lz = 2 pi lz") : 1/gamma;
  const Real Re      = args.getreal("-R","--Reynolds",400.0,"Reynolds number");
  const int Nx       = args.getint ("-Nx","--Nx",48,"number of x grid points");
  const int Ny       = args.getint ("-Ny","--Ny",49,"number of y grid points");
  const int Nz       = args.getint ("-Nz","--Nz",48,"number of z grid points");
  const int n        = args.getint ("-n","--n-stokesmode",1,"quantum number of stokes mode");
  const int Niter    = args.getint ("-i","--iterations",20, "number of newton iterations for soln of Stokes eqn");
  const Real eps     = args.getreal("-e","--eps", 1e-10, "max residual for newton search");
  const Real V       = args.getreal("-V","--Vmagnitude",0.0, "Waleffe PhysFluid 97 normalization");
  const Real magn    = args.getreal("-m","--magnitude", 1.0, "value of L2Norm");
  const bool test    = args.getflag("-t","--test","Run a test after computing Stokes modes");
  const string ename = args.getstr(2, "<flowfield>", "even Stokes mode filename");
  const string oname = args.getstr(1, "<flowfield>", "odd  Stokes mode filename");
  cout << "even name == " << ename << endl;
  cout << "odd  name == " << oname << endl;

  const Real a = -1;
  const Real b = 1;

  if (alpha == 0.0 && lx == 0.0) {
    cerr << "Please set the $x$ length scale via the -a or -lx options" << endl;
    exit(1);
  }
  if (gamma == 0.0 && lz == 0.0) {
    cerr << "Please set the $z$ length scale via the -g or -lz options" << endl;
    exit(1);
  }


  const Real Lx = alpha == 0.0 ? 2*pi*lx : 2*pi/alpha;
  const Real Lz = gamma == 0.0 ? 2*pi*lz : 2*pi/gamma;

  if (alpha == 0.0) alpha = 1.0/lx;
  if (gamma == 0.0) gamma = 1.0/lz;

  args.check();
  args.save("./");

  Vector yy = chebypoints(Ny,a,b);
  FlowField even(Nx, Ny, Nz, 3, Lx, Lz, -1, 1, Spectral, Physical);
  FlowField odd(even);

  // Compute beta for nth even and odd mode using newton search to solve beta
  const Real Ce = gamma * tanh(gamma);
  const Real Co = gamma / tanh(gamma);
  Real ebeta = n*M_PI - atan(n*M_PI / Ce);
  Real obeta = n*M_PI + atan(n*M_PI / Co);
  Real even_residual = ebeta*tan(ebeta) + gamma*tanh(gamma);
  Real  odd_residual = obeta/tan(obeta) - gamma/tanh(gamma);

  for(int i=0; i<Niter; ++i)  {
     Real fe = ebeta + atan(Ce / ebeta) - n*M_PI;
     Real fpe = 1 - Ce / (Ce*Ce + ebeta*ebeta);
     ebeta -= fe / fpe;
     even_residual = abs(ebeta*tan(ebeta) + gamma*tanh(gamma));
     if (even_residual < 1e-14)
       break;
  }
  for(int i=0; i<Niter; ++i)  {
     Real fo = obeta - atan(obeta / Co) - n*M_PI;
     Real fpo = 1 - Co / (Co*Co + obeta*obeta);
     obeta -= fo / fpo;
     odd_residual  = abs(obeta/tan(obeta) - gamma/tanh(gamma));
     if (odd_residual < 1e-14)
       break;
  }

  cout << "even-solution beta == " << ebeta << endl;
  cout << "odd-solution  beta == " << obeta << endl;

  cout << "residual of even beta eqn == " << even_residual << endl;
  cout << "residual of  odd beta eqn == " << odd_residual << endl;

  //cout << setprecision(17);
  //cout << ebeta << ' ' << obeta << endl;
  //cout << -(ebeta * ebeta + gamma*gamma) / Re << ' ' << -(obeta * obeta + gamma *gamma) / Re << endl;
  Real coshgamma = cosh(gamma);
  Real sinhgamma = sinh(gamma);
  Real cosebeta = cos(ebeta);
  Real sinobeta = sin(obeta);
  Real ucoeff2 = Re / (2 * ebeta * cosebeta);
  Real bg = ebeta * ebeta + gamma * gamma;
  Real lambda = -bg/Re;

  cout << "cos (even beta) == " << cosebeta << endl;
  for (int ny = 0; ny < Ny; ++ny) {
    Real y = yy[ny];
    Real eby = ebeta * y;
    Real oby = obeta * y;
    Real gy = gamma * y;

    even.cmplx(0,ny,1,0) = (ucoeff2*(y*sin(eby) - cos(eby)*tan(ebeta)) - (cos(eby)/cosebeta - cosh(gy)/coshgamma)/lambda);
    even.cmplx(0,ny,1,1) = cos(eby) / cosebeta - cosh(gy) / coshgamma;
    even.cmplx(0,ny,1,2) = -(-ebeta*sin(eby) / cosebeta - gamma*sinh(gy) / coshgamma ) / (Complex(0,even.kz(1)*gamma));

    odd.cmplx(0,ny,1,1)  = sin(oby) / sinobeta - sinh(gy) / sinhgamma;
    odd.cmplx(0,ny,1,2) = -(obeta*cos(oby) / sinobeta - gamma*cosh(gy) / sinhgamma) / (Complex(0,even.kz(1)*gamma));
  }

  // A quick and dirty computation of max(v(y)) for Wally-style normalization.
  Real dy = 1e-5;
  Real vmax = 0;
  for (Real y=-1; y<=1; y += dy)
    vmax = Greater(vmax, abs(cos(ebeta*y) / cosebeta - cosh(gamma*y) / coshgamma));

  cout << "vmax == " << vmax << endl;

  even.makeSpectral();
  odd.makeSpectral();

  if (V != 0.0) {
    cout << "Rescaling by V V == " << V << endl;
    // don't ask me about why the factor of 2 is needed.
    // but it produces fields w max(abs(v)) = V
    // the negative sign produces even mode in the z-phase preferred by jfg
    even *= -V/(2*vmax);
    odd *= -V/(2*vmax);
  }
  else {
    cout << "Normalizing to L2Norm == " << magn << endl;
    even *= magn/L2Norm(even);
    odd  *= magn/L2Norm(odd);
  }

  //
  FieldSymmetry tauz(1,1,1,0,0.5);
  even *= tauz;
  odd  *= tauz;

  if (even_residual < eps)
    even.save(ename);
  if (odd_residual < eps)
    odd.save(oname);

  save(lambda, "lambda" + i2s(n));

  if (test) {
    cout << "Even mode divergence: " << divNorm(even) << endl;
    cout << "Odd  mode divergence: " << divNorm(odd) << endl;
    cout << "Even mode norm: " << L2Norm(even) << endl;
    cout << "Odd  mode norm: " << L2Norm(odd) << endl;
  }
}
