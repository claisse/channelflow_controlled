#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

//#include "dnspoincare.h"

using namespace std;
using namespace channelflow;


int main(int argc, char* argv[]) {

  string purpose("integrate plane Couette or channel flow from a given initial condition until it hits\n"
		 "Poincare section defined by I-D == 0 (default) or (u(t)-ustar, estar) == 0 (-psect option)");

  ArgList args(argc, argv, purpose);

  const Real Reynolds = args.getreal("-R", "--Reynolds", 400,  "Reynolds number, U scale == maxdiff "
				     "btwn mean U and walls, for U(y) == laminar flow for these "
				     "parameters");
  /***/ Real nu       = args.getreal("-nu", "--viscosity", 0, "kinematic viscosity (takes precedence "
				     "over Reynolds, if nonzero");
  const string meanstr= args.getstr ("-mc", "--meanconstraint", "gradp", "gradp|bulkv : hold one fixed");
  const Real dPdx     = args.getreal("-P", "--dPdx",   0.0, "fixed pressure gradient");
  const Real Ubulk    = args.getreal("-U", "--Ubulk",  0.0, "fixed bulk velocity");
  const Real ua       = args.getreal("-ua", "--ulower",   -1.0, "lower wall speed");
  const Real ub       = args.getreal("-ub", "--uupper",    1.0, "upper wall speed");
  
  const int crosssign  = args.getint("-cr", "--crosssign" ,  0, "req. sign of dh/dt<0 for crossing h(u)==0 (0 means either)");
  const string bdir    = args.getpath("-bd", "--bdir", "./", "dir of basis functions that define fundamental domain");
  const int    Nb      = args.getint("-Nb", "--Nbasis", 0, "number of basis fields");

  const bool planeSection = args.getflag("-psect", "--planeSection", "poincare section defined by (u(t) - ustar, estar) == 0");
  const string ustar_str  = args.getstr("-us", "--ustar", "ustar", "a point (field) on poincare section");
  const string estar_str  = args.getstr("-es", "--estar", "estar", "a normal (field), defines poincare section");

  const bool u0cross  = args.getflag("-u0c", "--u0cross", "count initial condition as 0th crossing");
  const Real T0       = args.getreal("-T0", "--T0", 0.0, "start time");
  const Real T1       = args.getreal("-T1", "--T1", 100, "end time");
  const bool vardt    = args.getflag("-vdt", "--variabledt", "adjust dt to keep CFLmin<=CFL<CFLmax");
  const Real dtarg    = args.getreal("-dt",     "--dt", 0.03125, "timestep");
  const Real dtmin    = args.getreal("-dtmin",  "--dtmin", 0.001, "minimum time step");
  const Real dtmax    = args.getreal("-dtmax",  "--dtmax", dtarg,  "maximum time step");
  const Real dT       = args.getreal("-dT",     "--dT", 1.0,      "Poincare crossing check interval");
  const Real CFLmin   = args.getreal("-CFLmin", "--CFLmin", 0.40, "minimum CFL number");
  const Real CFLmax   = args.getreal("-CFLmax", "--CFLmax", 0.60, "maximum CFL number");
  const Real minnorm  = args.getreal("-mn", "--minnorm", 0.10, "stop if chebyNorm(u) < minnorm");
  const string stepstr= args.getstr ("-ts", "--timestepping", "sbdf3", "timestepping algorithm, one of "
				     "[befe1 cnab2 smrk2 sbdf1 sbdf2 sbdf3 sbdf4]");
  const string nonlstr= args.getstr ("-nl", "--nonlinearity", "rot", "method of calculating nonlinearity, "
				     "one of [rot conv div skew alt]");
  const string symmstr= args.getstr ("-symms","--symmetries", "", "constrain u(t) to invariant symmetric subspace, argument is the filename for a file listing the generators of the isotropy group");

  const bool savesteps= args.getflag("-ss", "--savesteps", "save u(t) at intervals dT");
  const string outdir = args.getpath("-o", "--outdir", "data-poinc/", "output directory");
  const string label  = args.getstr ("-l", "--label", "u", "output field prefix");

  const string uname  = args.getstr (1, "<flowfield>", "initial condition");


  args.check();
  args.save("./");
  mkdir(outdir);

  cout << "Constructing u and optimizing FFTW..." << endl;
  FlowField u(uname);
  //u.optimizeFFTW(FFTW_PATIENT);
  u.makeSpectral();
  FlowField q(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b());

  TimeStep dt(dtarg, dtmin, dtmax, dT, CFLmin, CFLmax, vardt);
  const bool inttime = (abs(dT - int(dT)) < 1e-12) && (abs(T0 - int(T0)) < 1e-12) ? true : false;


  FlowField uU(u);
  ChebyCoeff U(u.Ny(), u.a(), u.b(), Spectral);
  U[1] = 1;
  uU += U;
  cout << "dissip (u)     == " << dissipation(u) << endl;
  cout << "wallshear(u)   == " << wallshear(u) << endl;
  cout << "energy (u)     == " << 0.5*L2Norm2(u) << endl;
  cout << "dissip (u+U)   == " << dissipation(uU) << endl;
  cout << "wallshear(u+U) == " << wallshear(uU) << endl;
  cout << "energy (u+U)   == " << 0.5*L2Norm2(uU) << endl;  cout << "D(u) == " << dissipation(uU) << endl;
  // Construct time-stepping algorithm

  DNSFlags flags;
  flags.timestepping = s2stepmethod(stepstr);
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = s2nonlmethod(nonlstr);
  flags.constraint   = s2constraint(meanstr);
  flags.baseflow     = LaminarBase;
  flags.ulowerwall   = ua;
  flags.uupperwall   = ub;
  flags.nu           = (nu != 0.0) ? nu : abs(0.25*(ub-ua)*(u.b()-u.a())/Reynolds);
  flags.dPdx         = dPdx;
  flags.Ubulk        = Ubulk;
  flags.t0           = T0;
  flags.dt           = dt;

  if (symmstr.length() > 0) {
    SymmetryList symms(symmstr);
    cout << "Restricting flow to invariant subspace generated by symmetries" << endl;
    cout << symms << endl;
    flags.symmetries = symms;
  }

  // A basis set to define fundamental domain: (u(t), efund[n]) > 0
  // and symmetries that map crossings back into fundamental domain
  channelflow::array<FlowField> efund(Nb);
  channelflow::array<FieldSymmetry> sfund(Nb);
  for (int n=0; n<Nb; ++n) {
    efund[n] = FlowField(bdir + "e" + i2s(n));
    sfund[n] = FieldSymmetry(bdir + "s" + i2s(n));

    FlowField se = sfund[n](efund[n]);
    cout << "L2IP(e,se) == " << L2IP(efund[n], se) << " for n == " << n << endl;
  }

  PoincareCondition* h;
  if (planeSection) {
    FlowField estar(estar_str);
    FlowField ustar(ustar_str);
    h = new PlaneIntersection(ustar, estar);
  }
  else
    h = new DragDissipation();

  //cout << "h(u) == " << (*h)(u) << endl;

  DNSPoincare dns(u, efund, sfund, h, flags);

  int pluscount = 0;
  int minuscount = 0;

  ofstream tmios((outdir+"tmi.asc").c_str());
  ofstream tplos((outdir+"tpl.asc").c_str());
  tmios << setprecision(16);
  tplos << setprecision(16);

  Real t = T0;

  ofstream hos("hIDE.asc");

  if (u0cross) {

    if (savesteps)
      u.save(outdir + label + t2s(t, inttime));

    FlowField u0(u);
    Real hu0 = (*h)(u);
    dns.advance(u,q,dt.n());
    t += dt.dT();
    Real hu1 = (*h)(u);
    Real dhdt = (hu1-hu0)/dt.dT();

    cout << "Counting initial field as zeroth crossing" << endl;
    cout << "h(u0) == " << hu0 << endl;
    cout << "h(u1) == " << hu1 << endl;
    cout << "dhdt  == " << dhdt << endl;

    if (dhdt < 0) {
      string countstr = i2s(minuscount++);
      u0.save(outdir + "umi" + countstr);
      //save(T0, outdir + "tmi" + countstr);
      tmios << countstr << '\t' << t << endl;
    }
    else {
      string countstr = i2s(pluscount++);
      u0.save(outdir + "upl" + countstr);
      //save(T0, outdir + "tpl" + countstr);
      tplos << countstr << '\t' << t << endl;
    }
  }

  cout.setf(ios::left);
  cout << setw(10) << "t"
       << setw(12) << "h(u)"
       << setw(12) << "L2Norm(u)"
       << setw(12) << "CFL" << endl;

  hos.setf(ios::left);
  hos << setw(10) << "%t"
      << setw(12) << "h(u)"
      << setw(12) << "I(u+U)"
      << setw(12) << "D(u+U)"
      << setw(12) << "E(u+U)" << endl;

  hos << setprecision(8);

  for ( ; t<=T1; t += dT) {

    Real CFL = dns.CFL();
    int it = iround(t);
    Real hval = (*h)(u);
    Real nrm = L2Norm(u);

    // Compute I,D,E of total velocity
    uU = u;
    uU += U;
    hos.setf(ios::left);
    hos << setw(10) << t
	<< setw(16) << hval
	<< setw(16) << wallshear(uU)
	<< setw(16) << dissipation(uU)
	<< setw(16) << 0.5*L2Norm2(uU) << endl;

    cout.setf(ios::left);
    cout << setw(10) << it
	 << setw(16) << hval
	 << setw(16) << nrm
	 << setw(16) << CFL << endl;

    if (savesteps)
      u.save(outdir + label + t2s(t, inttime));

    bool crossed = dns.advanceToSection(u, q, dt.n(), crosssign);


    if (crossed) {
      FlowField ucross = dns.ucrossing();
      Real tcross = dns.tcrossing();
      //Real hcross = dns.hcrossing();
      int  scross = dns.scrossing();

      if (scross > 0) {
	string countstr = i2s(pluscount++);
	ucross.save(outdir + "upl" + countstr);
	//save(tcross, outdir + "tpl" + countstr);
	tplos << countstr << '\t' << tcross << endl;
      }
      else {
	string countstr = i2s(minuscount++);
	ucross.save(outdir + "umi" + countstr);
	//save(tcross, outdir + "tmi" + countstr);
	tmios << countstr << '\t' << tcross << endl;
      }
    }
    if (chebyNorm(u) < minnorm) {
      cerr << "Reached minimum chebyNorm(u). Saving last datapoint as laminar and exiting." << endl;
      u.save("uend");
      u.setToZero();
      string countstr = i2s(minuscount++);
      u.save(outdir + "umi" + countstr);
      //save(tcross, outdir + "tmi" + countstr);
      tmios << countstr << '\t' << "Inf" << endl;
      break;
    }

    /**********************
    Real CFL = dns.CFL();
    int it = iround(t);
    if (CFL < CFLmin)
      cout << it << " < " << flush;
    else if (CFL > CFLmax)
      cout << it << " > " << flush;
    else
      cout << it << ' ' << flush;
    **********************/
  }
  cout << endl;
}
