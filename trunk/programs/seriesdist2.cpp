// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("compute L2Dist(u(t), u_n) as a function of t and n");
  ArgList args(argc, argv, purpose);

  const Real T0       = args.getreal("-T0",   "--T0", 0,   "start time");
  const Real T1       = args.getreal("-T1",   "--T1", 500, "end time");
  const Real dT       = args.getreal("-dT",   "--dT", 1.0, "save interval");
  const string udir   = args.getpath("-d",    "--datadir", "data", "flowfield series directory");
  const string ulabel = args.getstr("-ul", "--ulabel", "u",  "flowfield filename label");
  const string ofile  = args.getstr("-o",  "--outfile", "dist", "output filename");
  const int digits    = args.getint("-dg", "--digits", 8,    "# digits in output");
  const int kxmax     = args.getint("-kx", "--kxmax", 0, "L2Norm for |kx|<kxmax, 0=>all");
  const int kzmax     = args.getint("-kz", "--kzmax", 0, "L2Norm for |kz|<kzmax, 0=>all");
  const string u0name = args.getstr(1, "<flowfield>", "filename of field u0");
  //const bool inttimeA = (abs(dTa - int(dTa)) < 1e-12) ? true : false;
  const bool inttime = (abs(dT - int(dT)) < 1e-12) ? true : false;

  args.check();
  args.save("./");

  const char s = ' ';

  ofstream os(appendSuffix(ofile, ".asc").c_str());

  os << "% ";
  for (int n=0; n<argc; ++n) {
    os << argv[n] << s;
  }
  os << setprecision(lesser(digits, 17));
  os << '\n';

  FlowField u0(u0name);
  cout << "Projecting fields onto basis..." << endl;

  for (Real t=T0; t<=T1; t += dT) {
    string uname = ulabel + t2s(t, inttime);
    cout << t << ' ' << flush;
    FlowField u(udir + uname);
    os << L2Dist(u,u0,kxmax,kzmax) << '\n';
  }
  cout << endl;
  cout << "done!" << endl;
}
