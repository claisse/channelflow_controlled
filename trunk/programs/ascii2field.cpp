#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("convert from ascii file of gridpoint values to a channelflow FlowField");
  ArgList args(argc, argv, purpose);

  string geomfile = args.getstr("-ge", "--geom", "", "geometry parameter file");

  int Nx = args.getint("-Nx", "--Nx", 0, "# x gridpoints of field");
  int Ny = args.getint("-Ny", "--Ny", 0, "# y gridpoints of field");
  int Nz = args.getint("-Nz", "--Nz", 0, "# z gridpoints of field");
  int Nd = args.getint("-Nd", "--Nd", 3, "# of components of field");
  bool pad = args.getbool("-p", "--pad",  "true", "pad for 2/3-style dealiasing");

  Real alpha = args.getreal("-a", "--alpha", 0.0, "2pi/Lx");
  Real gamma = args.getreal("-g", "--gamma", 0.0, "2pi/Lz");
  Real lx = (alpha != 0.0) ? 1/alpha : args.getreal("-lx", "--lx", 0.0, "Lx/(2*pi)") ;
  Real lz = (alpha != 0.0) ? 1/gamma : args.getreal("-lz", "--lz", 0.0, "Lz/(2*pi)");
  Real Lx = (lx != 0.0)    ? 2*pi*lx : args.getreal("-Lx", "--Lx", 0.0, "box length in x");
  Real Lz = (lz != 0.0)    ? 2*pi*lz : args.getreal("-Lz", "--Lz", 0.0, "box length in z");
  Real a  = -1.0;
  Real b  =  1.0;
  const string aname = args.getstr(2, "<asciifile>", "input ascii file");
  const string uname = args.getstr(1, "<fieldname>", "output flowfield file");

  args.check();
  string junk;
  if (geomfile.length() != 0) {
    ifstream gs(appendSuffix(geomfile, ".geom").c_str());
    gs >> Nx >> junk;
    gs >> Ny >> junk;
    gs >> Nz >> junk;
    gs >> Nd >> junk;
    gs >> Lx >> junk;
    gs >> Lz;
  }
  cout << setprecision(16);
  //cout << "Nx, Ny, Nz, Nd == " << Nx << ", " << Ny << ", " << Nz << ", " << Nd << endl;
  //cout << "Lx, Lz == " << Lx << ", " << Lz << endl;
  if (Nx == 0 || Ny == 0 || Nz == 0 || Lx == 0.0 || Lz == 0.0) {
    cerr << "error: at least one Nx,Ny,Nz, Lx,Lz is zero. Please use flags or a .geom file to set these parameters" << endl;
    exit(1);
  }

  ifstream is(appendSuffix(aname, ".asc").c_str());
  FlowField u(Nx,Ny,Nz,Nd,Lx,Lz,a,b,Physical,Physical);
  for (int nx=0; nx<Nx; ++nx)
    for (int ny=0; ny<Ny; ++ny)
      for (int nz=0; nz<Nz; ++nz)
	for (int i=0; i<Nd; ++i)
	  is >> u(nx,ny,nz,i);

  u.makeState(Spectral, Spectral);
  cout << " L2Norm(u) == " << L2Norm(u) << endl;
  cout << "divNorm(u) == " << divNorm(u) << endl;
  cout << " bcNorm(u) == " << bcNorm(u) << endl;

  if (pad) {
    FlowField v((3*Nx)/2, Ny, (3*Nz)/2, Nd, Lx, Lz, a,b);
    v.interpolate(u);
    v.save(uname);
  }
  else
    u.save(uname);
}
