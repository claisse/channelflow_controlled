#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/utilfuncs.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/matrixutils.h"

using namespace std;
using namespace channelflow;

typedef ChebyCoeff    Cheby;
typedef FieldSymmetry Symm;
typedef FlowField     Field;

template <class T> void push(const T& t, channelflow::array<T>& arrayt);

void load(Real& x, const string& filebase, Real default_value);

int main(int argc, char* argv[]) {

  string purpose("interpolate invariant solution u = sigma f^T(u), given three successive values of u,sigma,T");
  ArgList args(argc, argv, purpose);

  const Real NaN = 0.0/0.0;
  //cout << "NaN == " << NaN << endl;
  //cout << "isnan(NaN) == " << isnan(NaN) << endl;

  const Real muRe    = args.getreal("-R",   "--Reynolds", NaN, "interpolate with Reynolds number, use this value");
  const Real muRp    = args.getreal("-Rp",  "--ReynoldsPressure", NaN, "interpolate with pressure-based Reynolds numberfor channel flow, use this value");
  const Real muD0    = args.getreal("-D0","--dissipation0", NaN, "interpolate with D0 = dissip(u+laminar)/LxLz, use this value");
  const Real muD1    = args.getreal("-D1","--dissipation1", NaN, "interpolate with D1 = dissip(u)/Lx, use this value");
  const Real muD2    = args.getreal("-D2","--dissipation2", NaN, "interpolate with D2 = dissip(u), use this value");
  const Real muD3    = args.getreal("-D3","--dissipation3", NaN, "interpolate with D3 = dissip(u+U)/Lx, use this value");
  const Real mudPdx  = args.getreal("-dPdx", "--dPdx",   NaN, "interpolate with pressure gradient, use this value");
  const Real muUbulk = args.getreal("-Ubulk", "--Ubulk",  NaN, "interpolate with bulk velocity, use this value");
  const Real muUwall = args.getreal("-Uwall", "--Uwall",  NaN, "interpolate with magnitude of wall velocity, use this value");
  const Real mutheta = args.getreal("-theta", "--theta",  NaN, "interpolate with theta, angle of wall velocity to x axis");
  const Real muLx    = args.getreal("-Lx",  "--Lx",       NaN, "interpolate with Lx");
  const Real muLz    = args.getreal("-Lz",  "--Lz",       NaN, "interpolate with Lz");
  const Real muasp   = args.getreal("-asp", "--aspect",   NaN, "interpolate with aspect ratio Lx/Lz");
  const Real mudiag  = args.getreal("-diag","--diagonal", NaN, "interpolate with diagonal");
  const Real mucz    = args.getreal("-cz",  "--cz",       NaN, "interpolate with z wavespeed cz");
  const Real mumu    = args.getreal("-mu",  "--mu",       NaN, "interpolate with arbitray parameter mu");
  const string Ubasestr = args.getstr("-Ubase", "--Ubase", "", "zero|linear|parabolic (none/default is read from soln dirs)");
  const string meanstr= args.getstr("-mc", "--meanconstraint", "gradp", "gradp|bulkv : which one is fixed for the input data?");

  const bool ff  = args.getflag("-ff",  "--flowfield",   "save output data in native channelflow .ff binary file");

  const string odir     = args.getpath("-o", "--outdir", "./", "output directory");
  string solndir[3];
  solndir[2]  = args.getpath(3, "<solndir0>", "directory containing u0, T0, sigma0, Reynolds0, dPdx0");
  solndir[1]  = args.getpath(2, "<solndir1>", "directory containing u1, T1, sigma1, Reynolds1, dPdx1");
  solndir[0]  = args.getpath(1, "<solndir2>", "directory containing u2, T2, sigma2, Reynolds2, dPdx2");

  const Real EPSILON = 1e-13;

  args.check();

  const int Nmu = !isnan(muRe) + !isnan(muRp) + !isnan(muD0) + !isnan(muD1) + !isnan(muD2) + !isnan(muD3) + !isnan(mudPdx) + !isnan(muUwall) 
    + !isnan(muUbulk) + !isnan(mutheta) + !isnan(muLx) + !isnan(muLz) + !isnan(muasp) + !isnan(mudiag) + !isnan(mucz) + !isnan(mumu);

  cout << setprecision(8);
  if (Nmu != 1) {
    cout << "Please specify one and only one of the -Re -Rp -D0 -D1 -D2 -D3 -dPdx -Uwall -Ubulk -theta -Lx -Lz -asp -diag, -dPdx, and -cz options." << endl;
    cout << "muRe == " << muRe << endl;
    cout << "muRp == " << muRp << endl;
    cout << "muD0 == " << muD0 << endl;
    cout << "muD1 == " << muD1 << endl;
    cout << "muD2 == " << muD2 << endl;
    cout << "mudP == " << mudPdx << endl;
    cout << "muUw == " << muUwall << endl;
    cout << "muUb == " << muUbulk << endl;
    cout << "muth == " << mutheta << endl;
    cout << "muLx == " << muLx << endl;
    cout << "muLz == " << muLz << endl;
    cout << "muas == " << muasp << endl;
    cout << "mudg == " << mudiag<< endl;
    cout << "mucz == " << mucz << endl;
    cout << "mum  == " << mumu << endl;
    cout << "number of specified options == " << Nmu << endl;
    exit(1);
  }
  args.save("./");


  // this is gross, sorry
  int Dtype  = 0;
  Real muD = NaN;
  if (!isnan(muD0)) {
    muD = muD0;
    Dtype = 0;
  }
  if (!isnan(muD1)) {
    muD = muD1;
    Dtype = 1;
  }
  if (!isnan(muD2)) {
    muD = muD2;
    Dtype = 2;
  }
  if (!isnan(muD3)) {
    muD = muD3;
    Dtype = 3;
  }

  channelflow::array<Field>  u(3); // solutions
  channelflow::array<Cheby>  U(3); // base flow
  channelflow::array<Real>   T(3); // period
  channelflow::array<Real>   R(3); // Reynolds
  channelflow::array<Real>  Rp(3); // ReynoldsPressure
  channelflow::array<Real>   P(3); // pressure gradient, shorthand for dPdx
  channelflow::array<Real>  Ub(3); // Ubulk
  channelflow::array<Real>  Uw(3); // wallspeed
  channelflow::array<Real>  th(3); // theta
  channelflow::array<Real>   D(3); // dissipation (avg for orbits)
  channelflow::array<Symm>   s(3); // symmetry
  channelflow::array<Real>  mu(3); // continuation parameter
  Real munew = NaN;

  for (int i=0; i<3; ++i) {
    u[i] = FlowField(solndir[i] + "ubest");
    u[i].makeSpectral();

    if (Ubasestr.length() == 0) {
      U[i] = ChebyCoeff(solndir[i] + "Ubase");
      U[i].makeSpectral();
    }
    else { 
      U[i] = ChebyCoeff(u[i].Ny(), u[i].a(), u[i].b());
      if (Ubasestr == "linear") 
	U[i][1] = 1;
      else if (Ubasestr == "parabolic") {
	U[i][0] = 0.5;
	U[i][2] = -0.5;
      }
    }

    load(T[i], solndir[i] + "Tbest");
    load(R[i], solndir[i] + "Reynolds");
    load(P[i], solndir[i] + "dPdx", 0.0);
    load(Ub[i], solndir[i] + "Ubulk", 0.0);
    load(Uw[i], solndir[i] + "Uwall", 1.0);
    load(th[i], solndir[i] + "theta", 0.0);
    if (abs(th[i]) < 1e-13) th[i] = 0.0;
    //ChebyCoeff Ubase(solndir[i] + "Ubase");
    //Ubase.makeSpectral();
    //Uw[i] = Ubase.eval_b();
    //Ub[i] = Ubase.mean() + Re(u[i].profile(0,0,0)).mean();
    //cout << "Ubase.mean() == " << Ubase.mean() << endl;      
    //cout << "u mean       == " << Re(u[i].profile(0,0,0)).mean() << endl;
    s[i] = Symm(solndir[i] + "sigmabest");
    if (!isnan(mumu))
      load(mu[i], solndir[i] + "mu", 0.0);
    
	   // Calculate pressure-grad Reynolds for channel flows
    // Do following dPdx calculation because you can't trust the dPdx on disk!
    ChebyCoeff utot00 = Re(u[i].profile(0,0,0));
    utot00 += U[i];
    ChebyCoeff utot00y = diff(utot00);
    Real utotya = utot00y.eval_a();
    Real utotyb = utot00y.eval_b();

    Real nu = 1/R[i];
    Real h = 0.5*(u[i].b()-u[i].a());
    Real dPdx = 0.5*nu*(utotyb - utotya)/h; 
    Rp[i] = abs(dPdx)*cube(h)/(2*square(nu));
    P[i] = dPdx;
  }

  if (Dtype == 0)
    for (int i=0; i<3; ++i) {
      u[i] += U[i];
      D[i] = dissipation(u[i], false)/(2*u[i].Lx()*u[i].Lz()); //      load(D[i], solndir[i] + "D");
      u[i] -= U[i];      
    }
  if (Dtype == 1) 
    for (int i=0; i<3; ++i) 
      D[i] = dissipation(u[i], false)/u[i].Lx();
  if (Dtype == 2) 
    for (int i=0; i<3; ++i) 
      D[i] = dissipation(u[i], false);
  if (Dtype == 3) 
    for (int i=0; i<3; ++i) {
      u[i] += U[i];      
      D[i] = dissipation(u[i], false)/u[i].Lx();;
      u[i] -= U[i];      
    }
  
  Real aspect   = u[1].Lx()/u[1].Lz();
  Real diagonal = pythag(u[1].Lx(), u[1].Lz());
  Real phi    = atan(1.0/aspect);

  if (!isnan(muRe)) {
    mu[2] = R[2];
    mu[1] = R[1];
    mu[0] = R[0];
    munew = muRe;
  }
  else if (!isnan(muRp)) {
    mu[2] = Rp[2];
    mu[1] = Rp[1];
    mu[0] = Rp[0];
    munew = muRp;
  }
  else if (!isnan(mudPdx)) {
    mu[2] = P[2];
    mu[1] = P[1];
    mu[0] = P[0];
    munew = mudPdx;
  }
  else if (!isnan(muUwall)) {
    mu[2] = Uw[2];
    mu[1] = Uw[1];
    mu[0] = Uw[0];
    munew = muUwall;
  }
  else if (!isnan(muUbulk)) {
    mu[2] = Ub[2];
    mu[1] = Ub[1];
    mu[0] = Ub[0];
    munew = muUbulk;
  }
  else if (!isnan(mutheta)) {
    mu[2] = th[2];
    mu[1] = th[1];
    mu[0] = th[0];
    munew = mutheta;
  }
  else if (!isnan(muD)) {
    mu[2] = D[2];
    mu[1] = D[1];
    mu[0] = D[0];
    munew = muD;
  }
  else if (!isnan(muLx)) {
    mu[2] = u[2].Lx();
    mu[1] = u[1].Lx();
    mu[0] = u[0].Lx();
    munew = muLx;
  }
  else if (!isnan(muLz)) {
    mu[2] = u[2].Lz();
    mu[1] = u[1].Lz();
    mu[0] = u[0].Lz();
    munew = muLz;
  }
  else if (!isnan(muasp)) {
    mu[2] = u[2].Lx()/u[2].Lz();
    mu[1] = u[1].Lx()/u[1].Lz();
    mu[0] = u[0].Lx()/u[0].Lz();

    // Check that diagonal of u[2]'s is same as u[0]. Rescale Lx,Lz keeping aspect ratio
    // constant if diagonals are different. Normally reloaded aspect-continued data will
    // have constant diagonals, but do check and fix in order to restart data with some
    // small fuzz in diagonal.
    for (int n=2; n>=0; n -=2) {
      if (abs(diagonal - pythag(u[n].Lx(), u[n].Lz())) >= EPSILON) {
	cout << "Diagonal of u[" << n << "] != diagonal of u[1]. Rescaling Lx,Lz...." << endl;
	Real phi_n = atan(1/mu[n]);
	u[n].rescale(diagonal*cos(phi_n), diagonal*sin(phi_n));
      }
    }
    munew = muasp;
  }
  else if (!isnan(mudiag)) {
    mu[2] = pythag(u[2].Lx(), u[2].Lz());
    mu[1] = pythag(u[1].Lx(), u[1].Lz());
    mu[0] = pythag(u[0].Lx(), u[0].Lz());

    // Check that aspect ratio of u[2]'s is same u[0]. Rescale Lx,Lz keeping diagonal
    // constant if aspect ratios differ. Normally reloaded diagonl-continued data will
    // have constant aspect ratio, but do check and fix in order to restart data with some
    // small fuzz in aspect ratio.
    for (int n=2; n>=0; n -=2) {
      if (abs(aspect - u[n].Lx()/u[n].Lz()) >= EPSILON) {
	cout << "Aspect ratio of u[" << n << "] != aspect ratio of u[1]. Rescaling Lx,Lz...." << endl;
	Real diagonal_n =  pythag(u[n].Lx(), u[n].Lz());
	u[n].rescale(diagonal_n*cos(phi), diagonal_n*sin(phi));
      }
    }
    munew = mudiag;
  }
  else if (!isnan(mucz)) {
    mu[2] = s[2].az()*u[2].Lz()/T[2];
    mu[1] = s[1].az()*u[1].Lz()/T[1];
    mu[0] = s[0].az()*u[0].Lz()/T[0];
    munew = mucz;
  }
  else if (!isnan(mumu)) {
    ;
  }
  else {
    cerr << "Error in specification of continuation parameter! The control flow needs fixing (A)" << endl;
    exit(1);
  }

  int W = 16;
  cout << "Loaded data..." << endl;
  cout.setf(ios::left);
  cout << setw(W) << "mu"
       << setw(W) << "Re"
       << setw(W) << "Rp"
       << setw(W) << "Ubulk"
       << setw(W) << "dPdx"
       << setw(W) << "Uwall"
       << setw(W) << "theta"
       << setw(W) << "D"
       << setw(W) << "Lx"
       << setw(W) << "Lz"
       << setw(W) << "L2Norm(u)"
       << setw(W) << "T"
       << setw(W) << "sigma" << endl;

  for (int i=2; i>=0; --i) {
    cout.setf(ios::left);
    cout << setw(W) << mu[i]
	 << setw(W) << R[i]
	 << setw(W) << Rp[i]
	 << setw(W) << Ub[i]
	 << setw(W) << P[i]
	 << setw(W) << Uw[i]
	 << setw(W) << th[i]
	 << setw(W) << D[i]
	 << setw(W) << u[i].Lx()
	 << setw(W) << u[i].Lz()
	 << setw(W) << L2Norm(u[i])
	 << setw(W) << T[i]
	 << setw(W) << s[i] << endl;
  }
  Real Tnew  = quadraticInterpolate(T,  mu, munew);
  Real Rnew  = quadraticInterpolate(R,  mu, munew);
  Real Rpnew = quadraticInterpolate(Rp,  mu, munew);
  Real Ubnew = quadraticInterpolate(Ub, mu, munew);
  Real Uwnew = quadraticInterpolate(Uw, mu, munew);
  Real Pnew  = quadraticInterpolate(P,  mu, munew);
  Real thnew = quadraticInterpolate(th, mu, munew);
  Real Dnew  = quadraticInterpolate(D,  mu, munew);
  Symm snew  = quadraticInterpolate(s,  mu, munew);
  Field unew = quadraticInterpolate(u,  mu, munew);

  if (!isnan(muRe))
    Rnew = muRe;
  else if (!isnan(muRp))
    Rpnew = muRp;
  else if (!isnan(muUwall))
    Uwnew = muUwall;
  else if (!isnan(muUbulk)) 
    Ubnew = muUbulk;
  else if (!isnan(mudPdx))
    Pnew = mudPdx;
  else if (!isnan(mutheta))
    thnew = mutheta;
  else if (!isnan(muD))
    Dnew = muD;
  else if (!isnan(mucz))
    snew = FieldSymmetry(snew.sx(), snew.sy(), snew.sz(), snew.ax(), mucz*Tnew/unew.Lz(), snew.s());

  // FlowField quadratic interpolation interpolates Lx and Lz independently.
  // Aspect and diagonal continuation and need to stay on a particular (Lx,Lz)
  // curve, so revise them.
  if (!isnan(muasp)) {
    Real phi_new = atan2(unew.Lz(), unew.Lx());
    unew.rescale(diagonal*cos(phi_new), diagonal*sin(phi_new));
  }
  else if (!isnan(mudiag)) {
    Real diagonal_new = pythag(unew.Lx(), unew.Lz());
    unew.rescale(diagonal_new*cos(phi), diagonal_new*sin(phi));
  }
  else if (!isnan(mumu))
    munew = mumu;

  mkdir(odir);
  save(Tnew, odir + "T");
  //save(Dnew, odir + "Dguess");
  save(Rnew, odir + "Reynolds");
  save(Ubnew, odir + "Ubulk");
  save(Uwnew, odir + "Uwall");
  save(Pnew, odir + "dPdx");
  save(thnew, odir + "theta");
  snew.save(odir + "sigma");
  if (!isnan(mumu))
    save(munew, odir + "mu");
  if (ff)
    unew.save(odir + "uguess.ff");
  else
    unew.save(odir + "uguess");

  cout << "interpolated solution..." << endl;
  cout.setf(ios::left);
  
  cout << setw(W) << munew
       << setw(W) << Rnew
       << setw(W) << Rpnew
       << setw(W) << Ubnew
       << setw(W) << Pnew
       << setw(W) << Uwnew
       << setw(W) << thnew
       << setw(W) << Dnew
       << setw(W) << unew.Lx()
       << setw(W) << unew.Lz()
       << setw(W) << L2Norm(unew)
       << setw(W) << Tnew
       << setw(W) << snew
       << endl;
  
}
  //project(dnsflags.symmetries, unew, "quadratic extrapolant guess unew", cout);


void load(Real& x, const string& filebase, Real default_value) {
  ifstream is((filebase + ".asc").c_str());
  if (is.good())
    is >> x;
  else
    x = default_value;
}
