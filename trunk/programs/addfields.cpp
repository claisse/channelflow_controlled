#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("create a linear combination of flowfields\n\n"
		 "usage : addfields.x  c0 u0  c1 u1 c2 u2 ... <outfield>\n"
		 "where cn are real constants and un are flowfields\n");
  ArgList args(argc, argv, purpose);
  string outname = args.getstr(1, "<string>", "filename for output field");

  if (argc % 2 != 0 && argc < 4) {
    cerr << purpose << endl;
    cerr << "please use at least one real/field pair and an output file as command-line arguments\n";
    exit(1);
  }

  FlowField sum;

  // Minimum command line: argv0 c0 u0 uname
  //                             3   2    1

  for (int n=2; n<argc-1; n += 2) {
    string uname = args.getstr(n, "<field>", "flowfield");
    Real c       = args.getreal(n+1, "<Real>", "coefficient");
    FlowField u(uname);
    u *= c;

    if (n==2) {
      cout << outname << "  = " << c << " * " << uname << endl;
      sum = u;
    }
    else {
      if (!sum.congruent(u)) {
	cerr << "FlowField " << uname << " is not compatible with previous fields.\n";
	exit(1);
      }
      cout << outname << " += " << c << " * " << uname << endl;
      sum += u;
    }
  }

  sum.save(outname);
  args.save("./");
}
