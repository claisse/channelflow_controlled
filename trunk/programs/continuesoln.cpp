#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/utilfuncs.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/matrixutils.h"

// John Gibson, Wed Oct 10 16:10:46 EDT 2007

using namespace std;
using namespace channelflow;

typedef FieldSymmetry Symm;
template <class T> void push(const T& t, channelflow::array<T>& arrayt);

void fixuUbasehack(FlowField& u, ChebyCoeff U);

Real meandissipation(const FlowField& u, Real T, DNSFlags dnsflags,
		     const TimeStep& dt, SolutionType solntype);

Real observable(FlowField& u);

// Return distance with sign along unit vector along phi
Real spythag(Real Lx, Real Lxtarg, Real Lz, Real Lztarg, Real phi);

int main(int argc, char* argv[]) {


  string purpose("parametric continuation of invariant solution");
  ArgList args(argc, argv, purpose);

  GMRESHookstepFlags srchflags;

  const bool restart    = args.getflag("-r",    "--restart",       "start from three previously computed solutions");
  const bool eqb        = args.getflag("-eqb",  "--equilibrium",   "search for equilibrium or relative equilibrium (trav wave)");
  const bool orb        = args.getflag("-orb",  "--periodicorbit", "search for periodic orbit or relative periodic orbit");
  srchflags.poinconst   = args.getflag("-poincconst",  "--poincareconstraint",   "periodic orbit search with u constrained to I-D=0 Poincare section via DNS");
  srchflags.poincsrch   = args.getflag("-poincsrch",  "--poincaresearch",        "periodic orbit search with I-D=0 as a search equation");
  srchflags.xrelative   = args.getflag("-xrel",  "--xrelative",    "search over x phase shift for relative orbit or eqb");
  srchflags.zrelative   = args.getflag("-zrel",  "--zrelative",    "search over z phase shift for relative orbit or eqb");

  const bool Recontinue = args.getflag("-contRe",     "--continueRe",      "continue solution in pseudo-Reynolds number == 1/nu");
  const bool Pcontinue  = args.getflag("-contdPdx",   "--continuedPdx",    "continue solution in imposed pressure gradient");
  const bool RePcontinue= args.getflag("-contdPdxRe", "--continuedPdxRe",  "continue solution in Re and dPdx = const/Re");  
  const bool Ubcontinue = args.getflag("-contUbulk",  "--continuedUbulk",  "continue solution in imposed bulk velocity");
  const bool Uwcontinue = args.getflag("-contUwall",  "--continuedUwall",  "continue solution in imposed wall speed");
  const bool Lxcontinue = args.getflag("-contLx",     "--continueLx",      "continue solution in streamwise width Lx");
  const bool Lzcontinue = args.getflag("-contLz",     "--continueLz",      "continue solution in spanwise width Lz");
  const bool aspectcont = args.getflag("-contAsp",    "--continueAspect",  "continue solution in aspect ratio Lx/Lz");
  const bool diagcont   = args.getflag("-contDiag",   "--continueDiagonal","continue solution along diagonal with const aspect ratio Lx/Lz");
  const bool Ltargcont  = args.getflag("-contLtarg", "--continueLtarget",  "continue solution towards a target Lx,Lz");
  const bool thetacont   = args.getflag("-conttheta",  "--continuetheta",  "continue solution in orientation of relative wall velocity");

  const bool upwards    = args.getflag("-up",  "--upwards",        "for non-restart searches, search in dir of increasing free parameter");
  /***/ Real Lxtarg     = args.getreal("-Lxtarg",  "--LxTarget",   2*pi, "with -contLtarg, aim for this value of Lx");
  /***/ Real Lztarg     = args.getreal("-Lztarg",  "--LzTarget",   pi, "with -contLtarg, aim for this value of Lz");
  const Real T0         = args.getreal("-T",  "--maptime",  20.0,  "initial guess for orbit period or time of eqb/reqb map f^T(u)");
  /***/ Real Re0        = args.getreal("-R", "--Reynolds", 400.0,  "pseudo-Reynolds number 1/nu");
  const Real nu0        = args.getreal("-nu", "--nu",       0,     "kinematic viscosity (takes precedence over Reynolds, if nonzero)");
  const Real dPdx0      = args.getreal("-dPdx",  "--dPdx",   0.0,  "imposed mean pressure gradient");
  const Real Ubulk0     = args.getreal("-Ubulk", "--Ubulk",  0.0,  "imposed bulk velocity");
  const Real Uwall0     = args.getreal("-Uwall", "--Uwall",  1.0,  "imposed wall velocity");
  const Real theta0     = args.getreal("-theta", "--theta",  0.0,  "orientation of relative wall motion to x axis");

  const string sigmastr = args.getstr ("-sigma", "--sigma", "",    "file containing symmetry of relative solution (default == identity)");
  const string symmstr  = args.getstr ("-symms", "--symmetries", "", "file containing generators of isotropy group for symmetry-constrained search");
  const string initstr  = args.getstr ("-is", "--initstepping", "smrk2", "timestepping algorithm for initializing multistep algorithms, one of [befe1 cnrk2 smrk2 sbdf1]");
  const string stepstr  = args.getstr ("-ts", "--timestepping", "sbdf3", "timestepping algorithm, one of [befe1 cnab2 smrk2 sbdf1 sbdf2 sbdf3 sbdf4]");
  const string nonlstr  = args.getstr ("-nl", "--nonlinearity", "rot", "method of calculating nonlinearity, one of [rot conv div skew alt]");
  const string meanstr  = args.getstr ("-mc", "--meanconstraint", "gradp", "gradp|bulkv : hold one fixed");

  const Real s0         = args.getreal("-s0", "--s0" ,        0.0,   "start value for arclength (arbitrary)");
  const Real ds0        = args.getreal("-ds0", "--ss0" ,      0.0,   "initial increment for arclength continuation");
  /***/ Real dmu        = args.getreal("-dmu", "--dmu" ,    1e-04,   "initial relative increment for quadratic extrapolation in continuation parameter");
  /***/ bool adjdt      = args.getflag("-adt","--adjustDt",   "adjust dt between continuation steps to keep CFL in bounds");

  const Real dsmin      = args.getreal("-dsmin", "--dsmin",   1e-06, "minimum arclength increment (in normalized D,Re space)");
  const Real dsmax      = args.getreal("-dsmax", "--dsmax",   0.10,  "maximum arclength increment (in normalized D,Re space)");
  const Real guesserrmin= args.getreal("-errmin", "--errmin", 1e-05, "minimum error for extrapolated guesses");
  const Real guesserrmax= args.getreal("-errmax", "--errmax", 2e-05, "maximum error for extrapolated guesses");


  const bool xphasehack = args.getflag("-xphasehack", "--xphasehack",  "fix x phase so that u(x,0,Lz/2,0) - mean = 0 at x=Lx/2 (HACK for u UNSYMM in x)");
  const bool zphasehack = args.getflag("-zphasehack", "--zphasehack",  "fix z phase so that     d/dz <u>_{xy}(z) = 0 at z=Lz/2 (HACK for u UNSYMM in z)");
  const bool uUbasehack = args.getflag("-uUbasehack", "--uUbasehack",  "channelflow only: fix u/Ubase decomposition so that <du/dy> == 0 at walls.");

  const int    phasehackcoord  = 0; 
  const parity phasehackparity = Odd; 
  const Real   phasehackguess  = 0.0; 

  const bool vardt     = args.getflag("-vdt","--variableDt", "vary dt during time integration to keep CFLmin<=CFL<CFLmax");
  //const bool autodt    = args.getflag("-sdt","--scaleDt",    "scale dt when Lx changes to keep dt/Lx const ");
  const Real dtarg     = args.getreal("-dt","--timestep",  0.03125, "(initial) integration timestep");
  const Real dtmin     = args.getreal("-dtmin", "--dtmin", 0.001,  "minimum timestep");
  const Real dtmax     = args.getreal("-dtmax", "--dtmax", dtarg,  "maximum timestep");
  const Real dTCFL     = args.getreal("-dTCFL", "--dTCFL", 1.00,  "check CFL # at this interval");
  const Real CFLmin    = args.getreal("-CFLmin", "--CFLmin", 0.55, "minimum CFL number");
  const Real CFLmax    = args.getreal("-CFLmax", "--CFLmax", 0.65, "maximum CFL number");

  //const Real epsMax     = args.getreal("-emax","--epsMax",      1e-10, "minimum acceptable residual for GMRESHookstep");
  srchflags.epsSearch = args.getreal("-es",  "--epsSearch",   1e-10, "stop search if L2Norm(s f^T(u) - u) < epsSearch");
  srchflags.epsKrylov = args.getreal("-ek",  "--epsKrylov",   1e-13, "min. condition # of Krylov vectors");
  srchflags.epsDu     = args.getreal("-edu", "--epsDuLinear", 1e-7,  "relative size of du to u in linearization");
  srchflags.epsDt     = args.getreal("-edt", "--epsDtLinear", 1e-7,  "size of dT in linearization of f^T about T");
  srchflags.epsGMRES  = args.getreal("-eg",  "--epsGMRES",    1e-3,  "stop GMRES iteration when Ax=b residual is < this");
  srchflags.epsGMRESf = args.getreal("-egf",  "--epsGMRESfinal",  0.05,  "accept final GMRES iterate if residual is < this");
  srchflags.centdiff  = args.getflag("-cd", "--centerdiff", "centered differencing to estimate differentials");

  const int  Nsteps   = args.getint("-Ns",  "--Nsteps",   100,    "max number of continuation steps");
  srchflags.Nnewton   = args.getint( "-Nn", "--Nnewton",   20,    "max number of Newton steps ");
  srchflags.Ngmres    = args.getint( "-Ng", "--Ngmres",   120,    "max number of GMRES iterations per restart");
  srchflags.Nhook     = args.getint( "-Nh", "--Nhook",     20,    "max number of hookstep iterations per Newton");

  srchflags.delta     = args.getreal("-d",    "--delta",      0.01,  "initial radius of trust region");
  srchflags.deltaMin  = args.getreal("-dmin", "--deltaMin",   0.009, "stop if radius of trust region gets this small");
  srchflags.deltaMax  = args.getreal("-dmax", "--deltaMax",   0.1,   "maximum radius of trust region");
  srchflags.deltaFuzz = args.getreal("-df",   "--deltaFuzz",  1e-06, "accept hooksteps within (1+/-deltaFuzz)*delta");
  srchflags.lambdaMin = args.getreal("-lmin",   "--lambdaMin", 0.2,  "minimum delta shrink rate");
  srchflags.lambdaMax = args.getreal("-lmax",   "--lambdaMax", 1.5,  "maximum delta expansion rate");
  srchflags.lambdaRequiredReduction = 0.5; // when reducing delta, reduce by at least this factor.
  srchflags.improvReq = args.getreal("-irq",  "--improveReq", 1e-3,  "reduce delta and recompute hookstep if improvement "
				      " is worse than this fraction of what we'd expect from gradient");
  srchflags.improvOk  = args.getreal("-iok",  "--improveOk",   0.10, "accept step and keep same delta if improvement "
				      "is better than this fraction of quadratic model");
  srchflags.improvGood= args.getreal("-igd",  "--improveGood", 0.75, "accept step and increase delta if improvement "
				      "is better than this fraction of quadratic model");
  srchflags.improvAcc = args.getreal("-iac",  "--improveAcc",  0.10, "recompute hookstep with larger trust region if "
				      "improvement is within this fraction quadratic prediction.");

  srchflags.TscaleRel = args.getreal("-Tsc", "--Tscale",     20,    "scale time in hookstep: |T| = Ts*|u|/L2Norm(du/dt)");
  srchflags.dudtortho = args.getbool("-tc",  "--dudtConstr", true,  "require orthogonality of step to dudt");
  srchflags.orthoscale= args.getreal("-os",  "--orthoScale", 1,     "rescale orthogonality constraint");
  srchflags.dudxortho = args.getbool("-xc", "--dudxConstraint", true,"require orthogonality of step to dudx and dudz");
  srchflags.Rscale    = args.getreal("-rs", "--relativeScale", 1, "scale relative-search variables by this factor");
  srchflags.saveall   = args.getflag("-sa", "--saveall", "save each newton step");

  const string outdir   = args.getpath("-o",  "--outdir", "./", "output directory");
  const string logfile  = args.getstr ("-log","--logfile", "findsoln.log",  "output log (filename or \"stdout\")");
  const int digits      = args.getint("-dg", "--digits", 8, "number of digits for ReD.asc");

  string uname;
  string solndir[3];
  //Real ds0 = 0.01;

  const Real EPSILON = 1e-13;

  PoincareCondition* hpoincare     =  (srchflags.poinconst || srchflags.poincsrch) ? new DragDissipation() : 0;
  PoincareCondition* hpoincare_dns =  (srchflags.poinconst) ? hpoincare : 0;

  TimeStep timestep(dtarg, dtmin, dtmax, dTCFL, CFLmin, CFLmax, vardt);

  if (!restart) {
    //ds0 = args.getreal("-ds0", "--ds0" ,  0.01, "arclength increment for perturbations of initial solution");
    //up  = args.getflag("-up", "--upwards",      "continue upwards in continuation param (Re, Lx, or Lz)");
    uname = args.getstr(1, "<flowfield>",       "initial solution from which to start continuation");
  }
  else {
    solndir[2]  = args.getpath(3, "<solndir0>", "directory containing previous solution data");
    solndir[1]  = args.getpath(2, "<solndir1>", "ditto");
    solndir[0]  = args.getpath(1, "<solndir2>", "ditto");
  }
  args.check();

  if (!(eqb^orb)) {
    cerr << "Please choose either -eqb or -orb option to search for (relative) equilibrium or (relative) periodic orbit" << endl;
    exit(1);
  }

  if (int(Recontinue) + int(Pcontinue) + int(RePcontinue) + int(Lxcontinue) + int(Ubcontinue) + int(Uwcontinue) +
      int(Lzcontinue) + int(aspectcont) + int(diagcont) + int(Ltargcont) + int(thetacont) != 1) {
    cerr << "Please choose one of -contRe, -contdPdx, -contRedPdx, -contUbulk, -contUwall, -contLx, -contLz, -contAsp -contDiag, -conttheta, -contLtarg options for continuation in Re, Lx, Lz, Lx/Lz, Lx*Lz, or dPdx, or target(Lx,Lz)" << endl;
    exit(1);
  }
  args.save("./");
  srchflags.solntype = orb ? PeriodicOrbit : Equilibrium;
  bool relative = srchflags.xrelative || srchflags.zrelative;
  bool Tnormalize = eqb;
  bool Unormalize = srchflags.unormalize;

  Symm sigma0;
  if (sigmastr.length() != 0)
    sigma0 = Symm(sigmastr);

  if (nu0 != 0)
    Re0 = 1.0/nu0;


  int W = 24;
  cout << setprecision(8);
  cout << "Working directory == " << pwd() << endl;
  cout << "Command-line args == ";

  //VelocityScale vscale = s2velocityscale(velstr);
  // Construct time-stepping algorithm
  DNSFlags dnsflags;
  dnsflags.dealiasing   = DealiasXZ;
  dnsflags.initstepping = s2stepmethod(initstr);
  dnsflags.timestepping = s2stepmethod(stepstr);
  dnsflags.nonlinearity = s2nonlmethod(nonlstr);
  dnsflags.constraint   = s2constraint(meanstr);
  dnsflags.baseflow     = LaminarBase;
  dnsflags.ulowerwall   = 0.0;   // will be reset later
  dnsflags.uupperwall   = 0.0;   //
  dnsflags.wlowerwall   = 0.0;   // will be reset later
  dnsflags.wupperwall   = 0.0;   //
  dnsflags.nu           = 0.0;   // will be reset later
  dnsflags.dPdx         = 0.0;   // 
  dnsflags.Ubulk        = 0.0; 
  dnsflags.Wbulk        = 0.0; 
  dnsflags.dt           = Real(timestep); // will be reset later
  dnsflags.t0           = 0.0;
  dnsflags.verbosity    = Silent;

  if (symmstr.length() > 0) {
    SymmetryList symms(symmstr);
    cout << "Restricting flow to invariant subspace generated by symmetries" << endl;
    cout << symms << endl;
    dnsflags.symmetries = symms;
  }

  for (int i=0; i<argc; ++i) cout << argv[i] << ' ';
  cout << endl;

  cout << "Re    == " << Re0 << endl;
  cout << "sigma == " << sigma0 << endl;
  cout << "T     == " << T0 << endl;
  cout << "dPdx  == " << dnsflags.dPdx << endl;
  cout << "Ubulk == " << Ubulk0 << endl;
  cout << "Uwall == " << Uwall0 << endl;
  cout << "dt    == " << timestep.dt() << endl;
  cout << "DNSFlags == " << dnsflags << endl << endl;

  channelflow::array<FlowField> u(3); // solutions
  channelflow::array<Real>      T(3); // period
  channelflow::array<Real>      R(3); // pseudo-Reynolds number == 1/nu
  channelflow::array<Real>      P(3); // pressure gradient, shorthand for dPdx
  channelflow::array<Real>     Ub(3); // bulk velocity 
  channelflow::array<Real>     Uw(3); // wall velocity
  channelflow::array<Real>  theta(3); // orientation of wall motion to x axis
  channelflow::array<Real>      D(3); // dissipation (avg for orbits)
  channelflow::array<Real>     mu(3); // continuation parameter
  channelflow::array<Real>    obs(3); // observable
  channelflow::array<Real>    res(3); // residual
  channelflow::array<Real>      s(3); // (mu,obs) arclengths
  channelflow::array<Symm>  sigma(3); // symmetry
  
  Real CFL = 0.0; // CFL = c dt/dx;
  int fcount = 0;

  string muname = "mu";
  if (Recontinue)       muname = "mu=Re";
  else if (Pcontinue)   muname = "mu=dPdx";
  else if (RePcontinue) muname = "mu=Re";
  else if (Ubcontinue)  muname = "mu=Ubulk";
  else if (Uwcontinue)  muname = "mu=Uwall";
  else if (thetacont)   muname = "mu=theta";
  else if (Lxcontinue)  muname = "mu=Lx";
  else if (Lzcontinue)  muname = "mu=Lz";
  else if (aspectcont)  muname = "mu=Lx/Lz";
  else if (diagcont)    muname = "mu=sqrt(Lx^2+Lz^2)";
  else if (Ltargcont)   muname = "mu=Ltarget";


  // Relations for aspect and diagonal continuation: 
  // Lx = diagonal * cos alpha, Lz = diagonal * sin alpha,
  Real aspect   = 0.0; // Lx/Lz
  Real diagonal = 0.0; // sqrt(Lx^2 + Lz^2)
  Real alpha    = 0.0; // arccot(Lx/Lz) = arctan(Lz/Lx) = arctan(1/aspect);
  //Real h        = 1.0; // half wall separation;

  // Relations for target Lx,Lz continuation
  Real Ltarget = 0.0; // sqrt((Lxtarg-Lx0)^2 + (Lztarg-Lz0)^2), length to target
  Real phi = 0.0;     // arccot((Lxtarg-Lx0)/(Lztarg-Lz0)) 
  Real dt_Lx = 0.0;

  // Relation for Re*P == const continuation
  Real ReP = 0.0;

  // Compute initial data points for extrapolation from perturbations of given solution
  if (!restart) {
    u[1] = FlowField(uname);
    u[2] = u[1];
    u[0] = u[1];

    R[2] = Re0;
    R[1] = Re0;
    R[0] = Re0;

    T[2] = T0;
    T[1] = T0;
    T[0] = T0;

    P[2] = dPdx0;
    P[1] = dPdx0;
    P[0] = dPdx0;

    Ub[2] = Ubulk0;
    Ub[1] = Ubulk0;
    Ub[0] = Ubulk0;

    Uw[2] = Uwall0;
    Uw[1] = Uwall0;
    Uw[0] = Uwall0;

    theta[2] = theta0;
    theta[1] = theta0;
    theta[0] = theta0;

    //h        = 0.5*fabs(u[1].b() - u[1].a());
    aspect   = u[1].Lx()/u[1].Lz();
    diagonal = pythag(u[1].Lx(), u[1].Lz());
    alpha    = atan(1.0/aspect);

    dt_Lx = dtarg/u[1].Lx();

    ReP = Re0*dPdx0;
    
    if (Ltargcont) {
      phi = atan((Lztarg-u[1].Lz())/(Lxtarg-u[1].Lx()));
      Ltarget = spythag(u[1].Lx(), Lxtarg, u[1].Lz(), Lztarg, phi);
    }
    else {
      Lxtarg = u[1].Lx();
      Lztarg = u[1].Lz();
      phi = 0.0;
      Ltarget  = 0.0;
    }

    cout << "Some geometrical parameters. Phi and Ltarg are used for continuation along line in 2d (Lx,Lz) plane." << endl;
    cout << "  phi == " << phi << endl;
    cout << "Ltarg == " << Ltarget << endl;
    cout << "distance  == " << pythag(Lxtarg-u[1].Lx(), Lztarg-u[1].Lz()) << endl;
    cout << "u[1].Lx() == " << u[1].Lx() << endl;
    cout << "   Lxtarg == " << Lxtarg << endl;
    cout << "u[1].Lz() == " << u[1].Lz() << endl;
    cout << "   Lztarg == " << Lztarg << endl;

    cout << "Lxtarg - Ltarg cos(phi) == " << Lxtarg - Ltarget*cos(phi) << endl;
    cout << "Lztarg - Ltarg sin(phi) == " << Lztarg - Ltarget*sin(phi) << endl;
    cout << "dt_Lx == " << dt_Lx << endl;

    const int musign = upwards ? 1 : -1; // Go up or down in continuation parameter?

    // Revise above as necessary
    if (Recontinue) {
      mu[2] = R[2] = Re0*(1-musign*dmu);
      mu[1] = R[1] = Re0;
      mu[0] = R[0] = Re0*(1+musign*dmu);
    }
    else if (Pcontinue) {
      if (dPdx0 != 0.0) {
	mu[2] = P[2] = dPdx0*(1-musign*dmu);
	mu[1] = P[1] = dPdx0;
	mu[0] = P[0] = dPdx0*(1+musign*dmu);
      }
      else {
	// Use pressure gradient for Poiseuille flow as normalization for dPdx
	Real dPdxNorm = 1.0/R[1];
	mu[2] = P[2] = -musign*dmu*dPdxNorm;
	mu[1] = P[1] =  0.0;
	mu[0] = P[0] =  musign*dmu*dPdxNorm;
      }
    }
    
    else if (RePcontinue) {
      // Continuation parameter is Re, ...
      mu[2] = R[2] = Re0*(1-musign*dmu);
      mu[1] = R[1] = Re0;
      mu[0] = R[0] = Re0*(1+musign*dmu);
      
      // reset dPdx's so dPdx*Re == const
      if (abs(P[2]*R[2]-ReP) > 1e-13 ||
	  abs(P[0]*R[0]-ReP) > 1e-13) {
	cout << "Warning : Re*dPdx is not constant in input data. Resetting dPdx to make it so." << endl;
	P[2] = ReP/R[2];
	P[1] = dPdx0;
	P[0] = ReP/R[0];
      }
    }
    else if (Ubcontinue) {
      if (Ubulk0 != 0.0) {
	mu[2] = Ub[2] = Ubulk0*(1-musign*dmu);
	mu[1] = Ub[1] = Ubulk0;
	mu[0] = Ub[0] = Ubulk0*(1+musign*dmu);
      }
      else {
	// Use bulk velocity of Poiseuille flow as normalization for Ubulk
	Real Ubnorm = 1.0;
	mu[2] = Ub[2] = -musign*dmu*Ubnorm;
	mu[1] = Ub[1] =  0.0;
	mu[0] = Ub[0] =  musign*dmu*Ubnorm;
      }
    }
    else if (Uwcontinue) {
      if (Uwall0 != 0.0) {
	mu[2] = Uw[2] = Uwall0*(1-musign*dmu);
	mu[1] = Uw[1] = Uwall0;
	mu[0] = Uw[0] = Uwall0*(1+musign*dmu);
      }
      else {
	// Use wall velocity of Plane Couette flow as normalization for Uwall
	Real Uwnorm = 1.0;
	mu[2] = Uw[2] = -musign*dmu*Uwnorm;
	mu[1] = Uw[1] =  0.0;
	mu[0] = Uw[0] =  musign*dmu*Uwnorm;
      }
    }
    else if (thetacont) {
      mu[2] = theta[2] = theta0 - musign*dmu*2*pi;
      mu[1] = theta[1] = theta0;
      mu[0] = theta[0] = theta0 + musign*dmu*2*pi;
    }
    else if (Lxcontinue) {
      mu[2] = u[2].Lx() * (1-musign*dmu);
      mu[1] = u[1].Lx();
      mu[0] = u[0].Lx() * (1+musign*dmu);

      u[2].rescale(mu[2], u[2].Lz());
      u[0].rescale(mu[0], u[0].Lz());
    }
    else if (Lzcontinue) {
      mu[2] = u[2].Lz() * (1-musign*dmu);
      mu[1] = u[1].Lz();
      mu[0] = u[0].Lz() * (1+musign*dmu);

      u[2].rescale(u[2].Lx(), mu[2]);
      u[0].rescale(u[0].Lx(), mu[0]);
    }
    else if (aspectcont) {

      // Continuation parameter mu == Lx/Lz, keeping diagonal = sqrt(Lx^2 + Lz^2) fixed.
      // Let Lx' = diag cos alpha'
      //     Lz' = diag sin alpha'
      // Then diagonal is constant and aspect ratio is
      //     Lx'/Lz' = cot alpha'
      //         mu' = cot alpha'
      //      alpha' = arccot mu' = arctan(1/mu') = arctan(Lz'/Lx')
      mu[2] = aspect*(1-musign*dmu);
      mu[1] = aspect;
      mu[0] = aspect*(1+musign*dmu);

      Real alpha0 = atan(1/mu[0]);
      Real alpha2 = atan(1/mu[2]);

      u[2].rescale(diagonal*cos(alpha2), diagonal*sin(alpha2));
      u[0].rescale(diagonal*cos(alpha0), diagonal*sin(alpha0));

    }
    else if (diagcont) {

      // Continuation parameter mu == sqrt(Lx^2 + Lz^2), keeping aspect ratio Lx/Lz constant
      // Let Lx' = k' Lx
      //     Lz' = k' Lz
      // Then aspect ratio is constant and
      //     mu' = sqrt(Lx'^2 + Lz'^2) = k' mu
      //      k' = mu'/mu;
      mu[2] = diagonal*(1-musign*dmu);
      mu[1] = diagonal;
      mu[0] = diagonal*(1+musign*dmu);

      u[2].rescale(mu[2]/mu[1]*u[1].Lx(), mu[2]/mu[1]*u[1].Lz());
      u[0].rescale(mu[0]/mu[1]*u[1].Lx(), mu[0]/mu[1]*u[1].Lz());
    }
    else if (Ltargcont) {

      // Continuation parameter mu = Ltarget = length to target Lx,Lz = pythag(Lxtarg-Lx, Lztarg-Lz)
      // Continuation is along l ine 
      // (Lx,Lz) = (Lxtarg - mu cos phi, Lztarg - mu sin phi)
      mu[2] = Ltarget*(1-musign*dmu);
      mu[1] = Ltarget;
      mu[0] = Ltarget*(1+musign*dmu);

      u[2].rescale(Lxtarg - mu[2]*cos(phi), Lztarg - mu[2]*sin(phi));
      u[1].rescale(Lxtarg - mu[1]*cos(phi), Lztarg - mu[1]*sin(phi));
      u[0].rescale(Lxtarg - mu[0]*cos(phi), Lztarg - mu[0]*sin(phi));
      
    }
    else {
      cerr << "Error in specification of continuation parameter! The control flow needs fixing (A)" << endl;
      exit(1);
    }
    
    sigma[2] = sigma0;
    sigma[1] = sigma0;
    sigma[0] = sigma0;

    cout << "set up the following initial data..." << endl;
    cout << setw(4) << "i" << setw(W) << "T" << setw(W) << "Reynolds" << setw(W) << "dPdx" 
	 << setw(W) << "Ubulk" << setw(W) << "Uwall" 
	 << setw(W) << "L2Norm(u)" << setw(W) << "sigma" <<setw(W) << "theta" <<endl;
    for (int i=2; i>=0; --i)
      cout << setw(4) << i << setw(W) << T[i] << setw(W) << R[i] << setw(W) << P[i] 
	   << setw(W) << Ub[i] << setw(W) << Uw[i] 
	   << setw(W) << L2Norm(u[i]) << setw(W) << sigma[i] <<setw(W) << theta[i] <<endl;
  }

  // Read in some solutions from previous computations
  else {

    for (int i=0; i<3; ++i) {
      u[i] = FlowField(solndir[i] + "ubest");
      load(T[i],  solndir[i] + "Tbest");
      load(R[i],  solndir[i] + "Reynolds");
      load(Ub[i], solndir[i] + "Ubulk");
      load(Uw[i], solndir[i] + "Uwall");
      load(P[i],  solndir[i] + "dPdx");
      sigma[i] = Symm(solndir[i] + "sigmabest");
      load(theta[i], solndir[i] + "theta");
    }

    if (!relative && (sigma[0] != sigma[1] || sigma[1] != sigma[2]))
       cferror("continuesoln error : initial symmetries should be equal for non-relative searchs");
     
    sigma0 = sigma[1];
    
    //h        = (u[1].b() - u[1].a())/2;
    aspect   = u[1].Lx()/u[1].Lz();
    diagonal = pythag(u[1].Lx(), u[1].Lz());
    alpha    = atan(1.0/aspect);
    Ltarget = spythag(u[1].Lx(), Lxtarg, u[1].Lz(), Lztarg, phi);
    ReP = R[1]*P[1];

    if (Recontinue) {
      mu[2] = R[2];
      mu[1] = R[1];
      mu[0] = R[0];
    }
    else if (Pcontinue) {
      mu[2] = P[2];
      mu[1] = P[1];
      mu[0] = P[0];
    }
    else if (Ubcontinue) {
      mu[2] = Ub[2];
      mu[1] = Ub[1];
      mu[0] = Ub[0];
    }
    else if (Uwcontinue) {
      mu[2] = Uw[2];
      mu[1] = Uw[1];
      mu[0] = Uw[0];
    }
    else if (RePcontinue) {
      mu[2] = R[2];
      mu[1] = R[1];
      mu[0] = R[0];

      // reset dPdx's so dPdx*Re == const
      if (abs(P[2]*R[2]-ReP) > 1e-13 ||
	  abs(P[0]*R[0]-ReP) > 1e-13) {
	cout << "Warning : Re*dPdx is not constant in input data. Resetting dPdx to make it so." << endl;
	P[2] = ReP/R[2];
	P[0] = ReP/R[0];
      }
    }
    else if (thetacont) {
      mu[2] = theta[2];
      mu[1] = theta[1];
      mu[0] = theta[0];
    }
    else if (Lxcontinue) {
      mu[2] = u[2].Lx();
      mu[1] = u[1].Lx();
      mu[0] = u[0].Lx();
    }
    else if (Lzcontinue) {
      mu[2] = u[2].Lz();
      mu[1] = u[1].Lz();
      mu[0] = u[0].Lz();
    }
    else if (aspectcont) {
      mu[2] = u[2].Lx()/u[2].Lz();
      mu[1] = u[1].Lx()/u[1].Lz();
      mu[0] = u[0].Lx()/u[0].Lz();

      // Check that diagonal of u[2]'s is same as u[0]. Rescale Lx,Lz keeping aspect ratio
      // constant if diagonals are different. Normally reloaded aspect-continued data will
      // have constant diagonals, but do check and fix in order to restart data with some
      // small fuzz in diagonal.
      for (int n=2; n>=0; n -=2) {
	if (abs(diagonal - pythag(u[n].Lx(), u[n].Lz())) >= EPSILON) {
	  cout << "Diagonal of u[" << n << "] != diagonal of u[1]. Rescaling Lx,Lz...." << endl;
	  Real alpha_n = atan(1/mu[n]);
	  u[n].rescale(diagonal*cos(alpha_n), diagonal*sin(alpha_n));
	}
      }
    }
    else if (diagcont) {
      mu[2] = pythag(u[2].Lx(), u[2].Lz());
      mu[1] = pythag(u[1].Lx(), u[1].Lz());
      mu[0] = pythag(u[0].Lx(), u[0].Lz());

      // Check that aspect ratio of u[2]'s is same u[0]. Rescale Lx,Lz keeping diagonal
      // constant if aspect ratios differ. Normally reloaded diagonl-continued data will
      // have constant aspect ratio, but do check and fix in order to restart data with some
      // small fuzz in aspect ratio.
      for (int n=2; n>=0; n -=2) {
	if (abs(aspect - u[n].Lx()/u[n].Lz()) >= EPSILON) {
	  cout << "Aspect ratio of u[" << n << "] != aspect ratio of u[1]. Rescaling Lx,Lz...." << endl;
	  Real diagonal_n =  pythag(u[n].Lx(), u[n].Lz());
	  u[n].rescale(diagonal_n*cos(alpha), diagonal_n*sin(alpha));
	}
      }
    }
    else if (Ltargcont) {
      mu[2] = spythag(u[2].Lx(), Lxtarg, u[2].Lz(), Lztarg, phi);
      mu[1] = spythag(u[1].Lx(), Lxtarg, u[1].Lz(), Lztarg, phi);
      mu[0] = spythag(u[0].Lx(), Lxtarg, u[0].Lz(), Lztarg, phi);

      // Check that all solutions are colinear with (u[1].Lx, u[1].Lz) and (Lxtarg,Lztarg).
      // If not, rescale u[2] and u[0] Lx,Lz so that they are
      //for (int i=2; i>=0; i -=2) {
      //if (abs(atan((Lztarg - u[i].Lz())/(Lxtarg - u[i].Lx()))- phi) >= EPSILON) {
      //cout << "u[" << i << "] is not colinear with (u[1].Lx, u[1].Lz) and (Lxtarg,Lztarg). Rescaling to fix." << endl;
      //u[i].rescale(Lxtarg - mu[i]*cos(phi), Lztarg - mu[i]*sin(phi));
      //}
      //}
    }
    else {
      cerr << "Error in specification of continuation parameter! The control flow needs fixing (B)" << endl;
      exit(1);
    }

    dt_Lx = dtarg/u[1].Lx();

    cout << "loaded the following data..." << endl;
    cout << setw(4) << "i" << setw(W) << "T" << setw(W) << "Reynolds" << setw(W) << "dPdx" 
	 << setw(W) << "Ubulk" << setw(W) << "Uwall" 
	 << setw(W) << "L2Norm(u)" << setw(W) << "sigma" <<setw(W) << "theta" <<endl;
    for (int i=2; i>=0; --i)
      cout << setw(4) << i << setw(W) << T[i] << setw(W) << R[i] << setw(W) << P[i] 
	   << setw(W) << Ub[i] << setw(W) << Uw[i] 
	   << setw(W) << L2Norm(u[i]) << setw(W) << sigma[i] <<setw(W) << theta[i] <<endl;

  }

  if (aspectcont || diagcont) {
    cout << "aspect ratio || diagonal continuation : " << endl;
    cout << setprecision(15);
    cout.setf(ios::left);
    cout << setw(4) << "n" << setw(20) << muname << setw(20) << "aspect" << setw(20) << "diagonal" << endl;
    for (int n=2; n>=0; --n)
      cout << setw(4) << n << setw(20) << mu[n] << setw(20) << u[n].Lx()/u[n].Lz() << setw(20) << pythag(u[n].Lx(), u[n].Lz()) << endl;
    cout.unsetf(ios::left);
  }

  if (Ltargcont) {
    cout << "Lx,Lz target continuation : " << endl;
    cout << setprecision(15);
    cout.setf(ios::left);
    cout << setw(4) << "n" << setw(20) << muname << setw(20) << "Lx" << setw(20) << "Lz" << endl;
    for (int i=2; i>=0; --i)
      cout << setw(4) << i << setw(20) << mu[i] << setw(20) << u[i].Lx() << setw(20) << u[i].Lz() << endl;
    cout.unsetf(ios::left);
  }

  ofstream dos((outdir + "ReD.asc").c_str());
  dos << setw(W) << (string("%")+muname) << setw(W) << "D" << setw(W) << "s" << setw(W) << "guesserr" << setw(W) << "error" << " % directory" << endl;
  dos << setprecision(digits);

  // Now find solutions for initial data.
  for (int i=2; i>=0; --i) {

    // Allow for more newton steps if we start with blind perturbation of Re.
    GMRESHookstepFlags srchflags0 = srchflags;
    if (!restart)
      srchflags0.Nnewton = 20;

    cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    string searchdir = outdir + "restart-" + i2s(i) + "/";
    mkdir(searchdir);
    ofstream searchlog((searchdir + "findsoln.log").c_str());
    srchflags0.logstream = &searchlog;
    srchflags0.outdir    = searchdir;

    dnsflags.dPdx        = P[i];
    dnsflags.Ubulk       = Ub[i];
    dnsflags.ulowerwall  = -Uw[i]*cos(theta[i]);
    dnsflags.uupperwall  =  Uw[i]*cos(theta[i]);
    dnsflags.wlowerwall  = -Uw[i]*sin(theta[i]);
    dnsflags.wupperwall  =  Uw[i]*sin(theta[i]);
    dnsflags.nu          =  1.0/R[i];

    dnsflags.save(searchdir + "dnsflags");
    save(dnsflags.nu,  searchdir + "nu");
    save(R[i],  searchdir + "Reynolds");
    save(Uw[i], searchdir + "Uwall");
    save(P[i],  searchdir + "dPdx");
    save(Ub[i], searchdir + "Ubulk");
    save(theta[i],  searchdir + "theta");

    string label = "initial field u[" + i2s(i) + "]";
    fixdivnoslip(u[i]);
    project(dnsflags.symmetries, u[i], label, cout);

    cout << "computing solution for " << muname << " == " << mu[i] << " in dir " << searchdir << "..." << endl;
    //cout << "dt    == " << timestep.dt() << endl;
    //cout << "dnsflags == " << dnsflags << endl;
    //cout << "searchflags == " << srchflags0 << endl;

    // Just use constant initial value of dt to start the continuation. Adjustment is for long term drift in CFL,dt
    res[i] = GMRESHookstep(u[i], T[i], sigma[i], hpoincare, srchflags0, dnsflags, timestep, CFL);
    D[i]   = meandissipation(u[i], T[i], dnsflags, timestep, srchflags0.solntype);
    obs[i] = D[i];

    Real ubulk = Re(u[i].profile(0,0,0)).mean();
    //Real wbulk = Re(u.profile(0,0,2)).mean();
    if (abs(ubulk) < 1e-15) ubulk = 0.0;
    //if (abs(wbulk) < 1e-15) wbulk = 0.0;
  
    //Ubase.save(searchdir + "Ubase");

    if (zphasehack) {
      Symm tau = zfixphasehack(u[i], phasehackguess, phasehackcoord, phasehackparity);
      cout << "fixing z phase of solution with phase shift tau == " << tau << endl;
      u[i] *= tau;
    }
    if (xphasehack) {
      Symm tau = xfixphasehack(u[i], phasehackguess, phasehackcoord, phasehackparity);
      cout << "fixing x phase of solution with phase shift tau == " << tau << endl;
      u[i] *= tau;
    }
    if (uUbasehack) {
      cout << "fixing u+Ubase decomposition so that <du/dy> = 0 at walls (i.e. Ubase balances mean pressure gradient))" << endl;
      ChebyCoeff Ubase =  laminarProfile(dnsflags.nu, dnsflags.constraint, dnsflags.dPdx, dnsflags.Ubulk-ubulk, 
					 u[i].a(), u[i].b(), dnsflags.ulowerwall, dnsflags.uupperwall, u[i].Ny());
      fixuUbasehack(u[i], Ubase);
      //Ubase.save(searchdir + "Ubase");      
    }
    //if (xphasehack || zphasehack || uUbasehack) {
    //cout << "resaving phase and/or uUbase-fixed solution" << endl;
    //u[i].save(searchdir + "ubest");
    //}
      
    cout << muname << " == " << mu[i] << "   obs == " << obs[i] << "   D == " << D[i] << "   searchresidual == " << res[i] << endl;
    sigma[i].save(searchdir + "sigmabest");
    save(D[i],  searchdir + "D");

    //save(CFL,  searchdir + "CFL");
    //save(timestep.dt(),   searchdir + "dt");

    dos << setw(W) << mu[i] << setw(W) << D[i] << setw(W) << 0.0 << setw(W) << 0.0 << setw(W) << res[i] << " % restart-"  << i << endl;
    cout << "CFL == " << CFL << endl;    
    cout << " dt == " << timestep.dt() << endl;

  }

  if (adjdt) {
    timestep.adjustToMiddle(CFL);
    cout << "Readjusted dt" << endl;
    cout << " dt == " << timestep.dt() << endl;
  }    


  Real obsnorm = abs(obs[1]);
  Real munorm  = abs(mu[1]);
  if (Pcontinue) {
    // Poiseuille flow has U(y) = h^2/(2nu) dPdx (1 - (y/h)^2)
    // Use this as a simple normalization for dPdx : mu = 2nu/h^2 
    munorm = 2*dnsflags.nu/square(0.5*(u[1].b()-u[1].a()));
  }
  if (Ltargcont)
    munorm = pythag(Lxtarg,Lztarg); 
  if (Ubcontinue)
    munorm = 1;
  if (thetacont)
    munorm = 0.01; // pi/2 might be more natural, but tilting continuation is pretty stiff in theta

  cout << "     s0 == " << s0 << endl;
  cout << " munorm == " << munorm << endl;
  for (int i=2; i>=0; --i) 
    cout << " mu[" << i << "] == " << mu[i]  << endl;
  cout << "obsnorm == " << obsnorm << endl;
  for (int i=2; i>=0; --i) 
    cout << "obs[" << i << "] == " << obs[i]  << endl;

  s[2] = s0 - pythag((obs[2]-obs[1])/obsnorm, (mu[2]-mu[1])/munorm);
  s[1] = s0;
  s[0] = s0 + pythag((obs[0]-obs[1])/obsnorm, (mu[0]-mu[1])/munorm);

  for (int i=2; i>=0; --i) 
    cout << "s[" << i << "] == " << s[i]  << endl;
  
  Real ds = (ds0 != 0) ? ds0 : pythag((obs[0]-obs[1])/obsnorm, (mu[0]-mu[1])/munorm);
  cout << "ds == " << ds << endl;

  const Real guesserrtarget = sqrt(guesserrmin*guesserrmax); // aim between error bounds
  const int Ndsadjust = 6;

  bool prev_search_failed = false; // did previous search fail?

  Real guesserr_prev_search = guesserrtarget;
  Real guesserr = 0;

  for (int n=0; n<Nsteps; ++n) {

    cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    cout << "Continuing previous solutions by pseudo-arclength quadratic extrapolation in s = (mu,obs)" << endl;
    cout << setw(8) << "i" << setw(W) << "s" << setw(W) << muname << setw(W) << "T" 
	 << setw(W) << "dPdx" << setw(W) << "Ubulk" << setw(W) << "Uwall" << setw(W) << "obs" << endl;
    for (int i=2; i>=0; --i)
      cout << setw(8) << i << setw(W) << s[i] << setw(W) << mu[i] << setw(W) << T[i] 
	   << setw(W) << P[i] << setw(W) << Ub[i] << setw(W) << Uw[i] << setw(W) << obs[i] << endl;

    bool ds_reached_bounds = false;
    bool no_more_adjustments = false;

    Real munew;
    Real snew;
    Real Tnew;
    Real Rnew;
    Real Pnew;
    Real Ubnew;
    Real Uwnew;
    Real thetanew;
    Symm sigmanew;
    FlowField unew;

    // Find a guess with error within error bounds, or under it if previous Newton search has failed
    for (int m=0; m<Ndsadjust; ++m) {

      cout << "------------------------------------------------------" << endl;
      cout << "Look for decent initial guess, step == " << m << endl;

      snew = s[0] + ds;
      Tnew      = quadraticInterpolate(T, s, snew);
      Rnew      = quadraticInterpolate(R, s, snew);
      Pnew      = quadraticInterpolate(P, s, snew);
      Ubnew     = quadraticInterpolate(Ub, s, snew);
      Uwnew     = quadraticInterpolate(Uw, s, snew);
      thetanew  = quadraticInterpolate(theta, s, snew);
      sigmanew  = quadraticInterpolate(sigma, s, snew);
      unew      = quadraticInterpolate(u, s, snew);

      if (RePcontinue) 
	Pnew = P[1]*R[1]/Rnew; // rescale Pnew so that PR = const 
      else if (aspectcont) {
	// FlowField quadratic interpolation interpolates Lx and Lz independently.
	// Aspect and diagonal continuation and need to stay on a particular (Lx,Lz)
	// curve, so revise them.
	Real alpha_new = atan(unew.Lz()/unew.Lx());
	unew.rescale(diagonal*cos(alpha_new), diagonal*sin(alpha_new));
      }
      else if (diagcont) {
	Real diagonal_new = pythag(unew.Lx(), unew.Lz());
	unew.rescale(diagonal_new*cos(alpha), diagonal_new*sin(alpha));
      }
      fixdivnoslip(unew);
      //project(dnsflags.symmetries, unew, "quadratic extrapolant guess unew", cout);

      if (Recontinue)        munew = Rnew;
      else if (Pcontinue)   {munew = Pnew; dnsflags.dPdx = Pnew;}
      else if (RePcontinue) {munew = Rnew; dnsflags.dPdx = Pnew;}
      else if (Ubcontinue)  {munew = Ubnew; dnsflags.Ubulk = Ubnew;}
      else if (Uwcontinue) {
	munew = Uwnew; 
	dnsflags.ulowerwall  = -Uwnew*cos(thetanew);
	dnsflags.uupperwall  =  Uwnew*cos(thetanew);
	dnsflags.wlowerwall  = -Uwnew*sin(thetanew);
	dnsflags.wupperwall  =  Uwnew*sin(thetanew);
      }
      else if (thetacont) {
	munew = thetanew; 
	dnsflags.ulowerwall  = -Uwnew*cos(thetanew);
	dnsflags.uupperwall  =  Uwnew*cos(thetanew);
	dnsflags.wlowerwall  = -Uwnew*sin(thetanew);
	dnsflags.wupperwall  =  Uwnew*sin(thetanew);
      }
      else if (Lxcontinue)   munew = unew.Lx();
      else if (Lzcontinue)   munew = unew.Lz();
      else if (aspectcont)   munew = unew.Lx()/unew.Lz();
      else if (diagcont)     munew = pythag(unew.Lx(), unew.Lz());
      else if (Ltargcont)    munew = spythag(unew.Lx(), Lxtarg, unew.Lz(), Lztarg, phi);
      else {
	cerr << "Error in specification of continuation parameter! The control flow needs fixing (B)" << endl;
	exit(1);
      }

      //dnsflags.nu = viscosity(Rnew, dnsflags, unew.a(), unew.b());
      dnsflags.nu  =  1/Rnew;
      cout << setw(8) << "i" << setw(W) << "s" << setw(W) << muname << setw(W) << "T" 
	   << setw(W) << "dPdx" << setw(W) << "Ubulk" << setw(W) << "Uwall" << endl;
      cout << setw(8) << "guess" << setw(W) << snew << setw(W) << munew << setw(W) << Tnew 
	   << setw(W) << Pnew << setw(W) << Ubnew << setw(W) << Uwnew << endl;

      cout << "calculating error of extrapolated guess" << flush;
      //cout << endl;
      //cout << "L2Norm(u)  == " << L2Norm(unew) << endl;
      //cout << "Tnew       == " << Tnew << endl;
      //cout << "dns poinc  == " << (hpoincare_dns ? 1 : 0) << endl;
      //cout << "sigmanew   == " << sigmanew << endl;
      //cout << "dnsflags   == " << dnsflags << endl;
      //cout << "timestep   == " << timestep << endl;
      //cout << "Tnormalize == " << Tnormalize << endl;
      //cout << "Unormalize == " << Unormalize << endl;


      FlowField Guguess;
      Gp(unew, Tnew, hpoincare_dns, sigmanew, Guguess, dnsflags, timestep, Tnormalize, Unormalize, fcount, CFL, cout);

      guesserr = L2Norm(Guguess);

      cout << "dt    == " << timestep.dt() << endl;
      cout << "CFL   == " << CFL << endl;
      cout << "dsmin == " << dsmin << endl;
      cout << "ds    == " << ds << endl;
      cout << "dsmax == " << dsmax << endl;
      cout << "guesserrmin == " << guesserrmin << endl;
      cout << "guesserr    == " << guesserr << endl;
      cout << "guesserrmax == " << guesserrmax << endl;
      cout << "guesserr previous  == " << guesserr_prev_search << endl;
      cout << "prev_search_failed == " << prev_search_failed << endl;
      cout << "ds_reached_bounds  == " << ds_reached_bounds << endl;
      cout << "no_more_adjustment == " << no_more_adjustments << endl;


      if (adjdt) {
	timestep.adjustToMiddle(CFL);
	cout << "Readjusted dt" << endl;
	cout << " dt == " << timestep.dt() << endl;
      }          

      // Decide whether to continue adjusting the guess, search on the guess, or give up.
      if (prev_search_failed && ds < dsmin) {
	cout << "Stopping continuation because continuing would require ds <= dsmin" << endl;
	exit(1);
      }
      else if (prev_search_failed && ds >= dsmin) {
	cout << "------------------------------------------------------" << endl;
	cout << "Stopping guess adjustments because previous search failed," << endl;
	cout << "so we're allowing guesserr <= guesserrmin, as long as dsmin <= ds" << endl;
	break;
      }
      // Previous search succeeded.
      if ((guesserr >= guesserrmin && guesserr <= guesserrmax)) {
	cout << "Stopping guess adjustments because guess error is in bounds: errmin <= guesserr <= errmax" << endl;
	break;
      }
      else if (ds_reached_bounds) {
	cout << "Stopping guess adjustments because ds has reached its bounds:" << endl;
	break;
      }
      else if (no_more_adjustments) {
	cout << "Stopping guess adjustments because a recent search failed and we're being cautious." << endl;
	break;
      }
      else if (guesserr_prev_search <= guesserrmin) {
	cout << "Previous search succeeded with guesserr <= guesserrmin. " << endl;
	cout << "Let's try increasing ds, to lesser of 2*ds and value suggested by guesserr = O(ds^3)" << endl;
	cout << "but no more than max allowed value dsmax. And no more guess adjustments after this." << endl;
	ds *= lesser(2.0, pow(guesserrmin/guesserr, 0.33));
	if (ds > dsmax) {
	  ds = dsmax;
	  ds_reached_bounds = true;
	}
	else
	  no_more_adjustments = true;
	continue;
      }
      else {
	cout << "Guess not yet within bounds. Try new guess based on model guesserr = O(ds^3)" << endl;
	// Adjust ds on model err = k O(ds^3), ds = 1/k err^1/3
	cout << "guesserrtarget == " << guesserrtarget << endl;
	cout << "guesserr       == " << guesserr << endl;
	cout << "(err/targ)^1/3 == " << pow(guesserrtarget/guesserr, 0.33) << endl;
	ds *= pow(guesserrtarget/guesserr, 0.33);
	if (ds < dsmin) {
	  ds = dsmin;
	  ds_reached_bounds = true;
	}
	else if (ds > dsmax) {
	  ds = dsmax;
	  ds_reached_bounds = true;
	}
	continue;
      }
    }

    // Done looking for a good guess. Now search on the guess.

    string searchdir = outdir + "search-" + i2s(n) + "/";
    mkdir(searchdir);
    save(Rnew, searchdir + "Reynolds");
    save(dnsflags.nu, searchdir + "nu");
    save(Pnew, searchdir + "dPdx");
    save(Ubnew, searchdir + "Ubulk");
    save(Uwnew, searchdir + "Uwall");
    save(thetanew, searchdir + "theta");

    //save(snew, searchdir + "arclength");
    //save(munew, searchdir + "mu");
    //save(timestep.dt(),   searchdir + "dt");
    ofstream searchlog((searchdir + "findsoln.log").c_str());

    //dnsflags.verbosity  = Silent;
    srchflags.logstream = &searchlog;
    srchflags.outdir    = searchdir;

    cout << "Computing new solution in directory " << searchdir << "  ..." << endl;
    Real resnew = GMRESHookstep(unew, Tnew, sigmanew, hpoincare, srchflags, dnsflags, timestep, CFL);
    Real Dnew = meandissipation(unew, Tnew, dnsflags, timestep, srchflags.solntype);
    Real obsnew = Dnew;

    cout << "dt    == " << timestep.dt() << endl;
    cout << "CFL   == " << CFL << endl;

    if (zphasehack) {
      Symm tau = zfixphasehack(unew, phasehackguess, phasehackcoord, phasehackparity);
      cout << "fixing z phase of potential solution with phase shift tau == " << tau << endl;
      unew *= tau;
    }    
    if (xphasehack) {
      Symm tau = xfixphasehack(unew, phasehackguess, phasehackcoord, phasehackparity);
      cout << "fixing x phase of potential solution with phase shift tau == " << tau << endl;
      unew *= tau;
    }
    if (uUbasehack) {
      cout << "fixing u+Ubase decomposition so that <du/dy> = 0 at walls (i.e. Ubase balances mean pressure gradient))" << endl;
      Real ubulk = Re(unew.profile(0,0,0)).mean();
      if (abs(ubulk) < 1e-15) ubulk = 0.0;

      ChebyCoeff Ubase =  laminarProfile(dnsflags.nu, dnsflags.constraint, dnsflags.dPdx, dnsflags.Ubulk-ubulk, 
					 unew.a(), unew.b(), dnsflags.ulowerwall, dnsflags.uupperwall, unew.Ny());   

      fixuUbasehack(unew, Ubase);
      Ubase.save(searchdir + "Ubase");      
    }    
    if (xphasehack || zphasehack || uUbasehack) {
      cout << "resaving phase and/or uUbase-fixed of solution " << endl;
      unew.save(searchdir + "ubest");
    }

    //save(CFL,  searchdir + "CFL");
    save(Dnew,  searchdir + "D");
    //save(Tnew,  searchdir + "Tbest");
    //sigmanew.save(searchdir + "sigmabest");

    if (resnew < srchflags.epsSearch) {
      cout << "Found new solution." << endl;

      ds = pythag((obsnew-obs[0])/obsnorm, (munew-mu[0])/munorm);
      snew = s[0]+ds;

      push(unew, u);
      push(Tnew, T);
      push(Rnew, R);
      push(Pnew, P);
      push(Ubnew, Ub);
      push(Uwnew, Uw);
      push(thetanew, theta);
      push(Dnew, D);
      push(munew, mu);
      push(obsnew, obs);
      push(resnew, res);
      push(snew, s);
      push(sigmanew, sigma);

      if (adjdt) {
	timestep.adjustToMiddle(CFL);
	cout << "Readjusted dt" << endl;
	cout << " dt == " << timestep.dt() << endl;
      }
      cout << setw(8) << "solution" << setw(W) << snew << setw(W) << munew << setw(W) << Tnew << "   residual == " << resnew << endl;

      dos << setw(W) << munew << setw(W) << Dnew << setw(W) << snew << setw(W) << guesserr << setw(W) << resnew << "  % " << searchdir << endl;

      prev_search_failed = false;
      guesserr_prev_search = guesserr;

      
    }
    else {
      cout << "Failed to find new solution. Halving ds and trying again" << endl;
      cout << setw(8) << "failure" << setw(W) << snew << setw(W) << munew << setw(W) << Tnew << "   residual == " << resnew << endl;
      ds *= 0.5;
      prev_search_failed = true;
      guesserr_prev_search = guesserr;

      string failuredir(outdir + "failures");
      mkdir(failuredir);
      rename(searchdir, failuredir + "/search-" + i2s(n));
    }
  }
}

template <class T> void push(const T& t, channelflow::array<T>& a) {
  for (int n=a.length()-1; n>0; --n)
    a[n] = a[n-1];
  a[0] = t;
}


//void temporalRegularize(FlowField& unew, Real Tnew, const FieldSymmetry& sigmanew, Real Rnew,
//			const DNSFlags& dnsflags, SolutionType solntype) {
//}

Real meandissipation(const FlowField& uarg, Real T, DNSFlags dnsflags,
		     const TimeStep& dtarg, SolutionType solntype) {

  cout << "computing mean dissipation " << flush;
  FlowField u(uarg);

  ChebyCoeff Ubase(u.Ny(), u.a(), u.b());
  
  if (dnsflags.baseflow == LinearBase) 
    Ubase[1] = 1;
  else if (dnsflags.baseflow == ParabolicBase) {
    Ubase[0] = 0.5;
    Ubase[2] = -0.5;
  }
  else if (dnsflags.baseflow == LaminarBase) {
   Real ubulk = Re(uarg.profile(0,0,0)).mean();
   if (abs(ubulk) < 1e-15) 
     ubulk = 0.0;
   Ubase =  laminarProfile(dnsflags.nu, dnsflags.constraint, dnsflags.dPdx, dnsflags.Ubulk-ubulk, 
			   uarg.a(), uarg.b(), dnsflags.ulowerwall, dnsflags.uupperwall, uarg.Ny());   
  }

  if (solntype == Equilibrium) {
    cout << " of EQB/TW..." << flush;
    u += Ubase;
    cout << "done" << endl;
    return dissipation(u);
  }
  else {
    cout << " of periodic orbit..." << flush;
    //DNSFlags dnsflags = dnsflagsarg;
    //dnsflags.verbosity = PrintTicks;

    int N = iround(T);
    const Real dT = T/N;
    TimeStep dt(dtarg.dt(), dtarg.dtmin(), dtarg.dtmax(), dT, dtarg.CFLmin(), dtarg.CFLmax(), dtarg.variable());
    //u.optimizeFFTW(FFTW_PATIENT);
    dnsflags.dt = Real(dt);


    const int Nx = u.Nx();
    const int Ny = u.Ny();
    const int Nz = u.Nz();
    const Real Lx = u.Lx();
    const Real Lz = u.Lz();
    const Real a = u.a();
    const Real b = u.b();

    FlowField q(Nx,Ny,Nz,1,Lx,Lz,a,b);

    DNS dns(u, dnsflags);

    int count = 0;
    Real D = 0.0;

    // Don't close the orbit so as not to count last point twice
    for (Real t=0; t<=T-dT/2; t += dT) {

      u += Ubase;
      D += dissipation(u);
      u -= Ubase;
      ++count;

      dns.advance(u, q, dt.n());

      if (dt.adjust(dns.CFL()))
	dns.reset_dt(dt);
    }
    cout << "done" << endl;
    return D/count;
  }
}

Real observable(FlowField& u) {
  ChebyCoeff U(u.Ny(),u.a(),u.b(),Spectral);
  U[1] = 1;
  u += U;
  Real obs = dissipation(u);
  u -= U;
  return obs;
}

// distance with a sign
Real spythag(Real Lx, Real Lxtarg, Real Lz, Real Lztarg, Real phi) {

  Real ex = cos(phi);
  Real ez = sin(phi);

  Real dLx = Lxtarg-Lx;
  Real dLz = Lztarg-Lz;
  Real dist = pythag(dLx, dLz);
  Real projection = ex*dLx + ez*dLz;
  int sign = 0;
  if (projection > 0)
    sign = 1;
  else if (projection < 0)
    sign = -1;

  return sign*dist;
};

void fixuUbasehack(FlowField& u, ChebyCoeff U) {
  // Construct Udiff = 1 - (y/h)^2
  ChebyCoeff u00 = Re(u.profile(0,0,0));
  ChebyCoeff u00y = diff(u00);
  Real ubulk = u00.mean();
  Real uya = u00y.eval_a();
  Real uyb = u00y.eval_b();
  //Real h =  (u.b() - u.a())/2;

  ChebyCoeff Uy = diff(U);
  Real Ubulk = U.mean();
  Real Uya = Uy.eval_a();
  Real Uyb = Uy.eval_b();
  
  ChebyCoeff utot = u00;
  utot += U;
  ChebyCoeff utoty = diff(utot);
  Real utotbulk = utot.mean();
  Real utotya = utoty.eval_a();
  Real utotyb = utoty.eval_b();

  cout << "fixUbasehack input: " << endl;
  cout << "    ubulk == " << ubulk << endl;
  cout << "    Ubulk == " << Ubulk << endl;
  cout << " utotbulk == " << utotbulk << endl;
  cout << "  uya-uyb == " << uya-uyb << endl;
  cout << "  Uya-Uya == " << Uyb-Uya << endl;
  cout << "utya-utyb == " << utotyb-utotya << endl;

  
  // Construct Udiff = c*(1 - (y/h)^2) where c is set so that Uffyb-Udiffya == -(uyb-uya)
  ChebyCoeff Udiff(u.Ny(), u.a(), u.b(), Spectral);
  Udiff[0] =  0.5;
  Udiff[2] = -0.5;
  ChebyCoeff Udiffy = diff(Udiff);
  Udiff *= -(uyb - uya)/(Udiffy.eval_b() - Udiffy.eval_a());

  // Move Udiff from u to U
  u += Udiff;
  U -= Udiff;


  u00 = Re(u.profile(0,0,0));
  u00y = diff(u00);
  ubulk = u00.mean();
  uya = u00y.eval_a();
  uyb = u00y.eval_b();

  Uy = diff(U);
  Ubulk = U.mean();
  Uya = Uy.eval_a();
  Uyb = Uy.eval_b();
  
  utot = u00;
  utot += U;
  utoty = diff(utot);
  utotbulk = utot.mean();
  utotya = utoty.eval_a();
  utotyb = utoty.eval_b();

  cout << "fixUbasehack output: " << endl;
  cout << "    ubulk == " << ubulk << endl;
  cout << "    Ubulk == " << Ubulk << endl;
  cout << " utotbulk == " << utotbulk << endl;
  cout << "  uya-uyb == " << uya-uyb << endl;
  cout << "  Uya-Uya == " << Uyb-Uya << endl;
  cout << "utya-utyb == " << utotyb-utotya << endl;

}


/* continuesoln.cpp, channelflow-1.3, www.channelflow.org
 *
 * Copyright (C) 2001-2009  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu
 * Center for Nonlinear Sciences, School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
