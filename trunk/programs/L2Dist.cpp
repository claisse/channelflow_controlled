#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <sys/stat.h>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

void geomprint(const FlowField& u, ostream& os);

int main(int argc, char* argv[]) {

  string purpose("compute the L2 Distance of two fields");

  ArgList args(argc, argv, purpose);

  const bool   sx      = args.getflag("-sx", "--shiftx", "shift by Lx/2");
  const bool   sz      = args.getflag("-sz", "--shiftz", "shift by Lz/2");
  const bool   normal  = args.getflag("-n", "--normalize", "return |u-v|/sqrt(|u||v|)");
  const string u0name  = args.getstr (2, "<flowfield>", "input field #1");
  const string u1name  = args.getstr (1, "<flowfield>", "input field #2");

  args.check();

  FlowField u0(u0name);
  FlowField u1(u1name);

  u0.makeSpectral();
  u1.makeSpectral();
  if (!u0.congruent(u1)) {
    cout << setprecision(16);
    cout << u0name << " : ";
    geomprint(u0, cout);
    cout << endl;
    cout << u1name << " : ";
    geomprint(u1, cout);
    cout << endl;
  }

  if (sx && sz)
    u1 *= FieldSymmetry(1,1,1, 0.5, 0.5);
  else if (sx)
    u1 *= FieldSymmetry(1,1,1, 0.5, 0.0);
  else if (sz)
    u1 *= FieldSymmetry(1,1,1, 0.0, 0.5);

  Real nrm = normal ? 1.0/sqrt(L2Norm(u0)*L2Norm(u1)) : 1;
  cout << nrm*L2Dist(u0,u1) << endl;
}

void geomprint(const FlowField& u, ostream& os) {
  cout << u.Nx() << " x " << u.Ny() << " x "
       << u.Nz() << " x " << u.Nd() << ",  "
       << "[0, " << u.Lx() << "] x "
       << "[" << u.a() << ", " << u.b() << "] x "
       << "[0, " << u.Lz() << "]";
}
