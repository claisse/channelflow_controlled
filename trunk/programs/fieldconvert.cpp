// License declaration at end of file

// Program for writing HDF5 files from flowfield data, essentially a
// modification of field2ascii. Some of the code and the accopmanying
// comments are taken from the HDF group website (www.hdfgroup.org).

#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("fieldconvert : convert between FlowField .ff and .h5 (HDF5) files\n"
		 "usage: fieldconvert u.ff u.h5     (converts u.ff to u.h5)\n"
		 "       fieldconvert u.h5 u.ff     (converts u.h5 to u.ff)");

  ArgList args(argc, argv, purpose);
  const string iname = args.getstr(2, "<filename>",   "input field");
  const string oname = args.getstr(1, "<filename>",   "output field");

  args.check();

  FlowField u(iname);
  u.save(oname);
}
