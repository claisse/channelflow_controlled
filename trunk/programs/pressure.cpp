// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/poissonsolver.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("compute pressure field of a given velocity field");

  ArgList args(argc, argv, purpose);

  const string nonlstr= args.getstr ("-nl", "--nonlinearity", "conv", "method of calculating nonlinearity, "
				     "one of [rot conv div skew alt]");

  //const string ifileutdir = args.getpath("-o", "--outdir", "data/", "output directory");
  //const string label  = args.getstr ("-l", "--label", "p", "output field prefix");

  /***/ Real nu       = args.getreal("-nu", "--viscosity", 0, "kinematic viscosity (takes precedence over Re if nonzero)");
  const Real Reynolds = args.getreal("-R", "--Reynolds", 400, "Reynolds number");
  const string Uname  = args.getstr (3, "<chebycoeff>", "input baseflow file or none|linear|parabolic");
  const string uname  = args.getstr (2, "<flowfield>",  "input velocity field (deviation from laminar)");
  const string pname  = args.getstr (1, "<flowfield>",  "output pressure field (not including const pressure gradient)");

  //const bool bulkvel  = args.getflag("-b", "--bulkvelocity","hold bulk velocity fixed, not pressure gradient");
  //const Real dPdx     = args.getreal("-P", "--dPdx",   0.0, "value for fixed pressure gradient");
  //const Real Ubulk    = args.getreal("-U", "--Ubulk",  0.0, "value for fixed bulk velocity");

  args.check();
  args.save("./");
  
  if (nu == 0.0) 
    nu = 1.0/Reynolds;

  FlowField u(uname);
  ChebyCoeff Ubase;
  if (Uname == "parabolic") {
    Ubase = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
    Ubase[0] =  0.5;
    Ubase[2] = -0.5;
  }
  else if (Uname == "linear") {
    Ubase = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
    Ubase[1] = 1.0;
  }
  else if (Uname == "none") {
    Ubase = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
  }
  else {
    Ubase = ChebyCoeff(Uname);
  }
  ChebyCoeff Wbase(u.Ny(), u.a(), u.b(), Spectral);

  Ubase.makeSpectral();
  u.makeSpectral();

  PressureSolver poisson(u, Ubase, Wbase, nu, s2nonlmethod(nonlstr)); 

  FlowField q = poisson.solve(u);

  q.save(pname);
}

