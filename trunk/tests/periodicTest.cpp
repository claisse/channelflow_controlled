#include <iostream>
#include <iomanip>
#include "channelflow/periodicfunc.h"
#include "channelflow/chebyshev.h"

using namespace std;
using namespace channelflow;

bool modecheck(const PeriodicFunc& f, int k, Complex fk);
bool normcheck(const PeriodicFunc& f, int k, Real c);

int main() {

  const uint Ntests = 100;
  const uint Nmax = 128;
  const Real Lmax = 10;
  const Real magn = 10;

  const uint P = 4;  // print precision
  const uint W = 12; // print width
  uint failures = 0;

  cerr << "helmholtzTest: " << flush;
  if (verbose) {
    cout << "\n====================================================" << endl;
    cout << "helmholtzTest\n\n";
  }
  
  for (int test=0; test<Ntests; ++test) {

    // Produce a random but sensible PeriodicFunc
    const uint N = 2*(rand() % (Nmax/2));
    const Real L = randomReal(0, Lmax);
    const uint k = rand() % (N/2);
    const bool s = rand() % 2;
    const Real c = randomReal(-magn, magn);
    const Real a = 2*pi/L;
    
    PeriodicFunc f(N,L,Physical);
    PeriodicFunc fx(N,L,Physical);
    Vector x = periodicpoints(N, L);

    Complex fk;
    if (s) {
      for (uint n=0; n<N; ++n) {
	f(n)  =   c*sin(a*x(n));
	fx(n) = a*c*cos(a*x(n));
      }
      fk = Complex(0,c);
    }
    else {
      for (uint n=0; n<N; ++n) {
	f(n)  =    c*cos(a*x(n));
	fx(n) = -a*c*sin(a*x(n));
      }
      fk = Complex(c,0);
    }
    f.makeSpectral();
    
    if (!modecheck(f,k,fk)) 
      ++failures;
    if (!normcheck(f,k,c))
      ++failures;
  }   
  cout << "failures == " << failures << endl;

  return 0;
}

const Real eps = 1e-14;

bool modecheck(const PeriodicFunc& f, int k, Complex fk) {
  assert(k<=f.kmax());
  const Real eps = 1e-14;
  for (int j=0; j<=f.kmax(); ++j)
    if ((j==k && abs(f.cmplx(k) - fk) > eps) ||
	(j!=k && abs(f.cmplx(k)) > eps))
      return false;
  return true;
}      


bool normcheck(const PeriodicFunc& f, int k, Real c) {
  Real norm = L2Norm(f);
  if ((k==0 && abs(norm - c) > eps) || (k!=0 && (abs(norm - c/2) > eps)))
    return false;
  else 
    return true;
}
