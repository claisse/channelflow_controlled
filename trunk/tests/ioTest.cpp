#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"

using namespace std;
using namespace channelflow;

int main() {

  bool verbose = true;
  cerr << "ioTest: " << flush;
  if (verbose) {
    cout << "\n====================================================" << endl;
    cout << "ioTest\n\n";
  }

  // Define gridsize
  const int Nx=32;
  const int Ny=33;
  const int Nz=24;

  // Define box size
  const Real Lx=1.75*pi;
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Lz=1.2*pi;


  // Define size and smoothness of initial disturbance
  const Real decay = 0.5;
  const Real magn  = 0.1;
  //const int kxmax = 4;
  //const int kzmax = 4;
  const Real maxerr = 1e-14;
  Real toterr = 0.0;

  FlowField u(Nx,Ny,Nz,3,Lx,Lz,a,b);

  // Test IO on nonpadded field
  u.setPadded(false);
  u.perturb(magn, decay);
  u *= magn/L2Norm(u);
  u.save("ufull");
  FlowField v("ufull");


  Real err = L2Dist(u,v);
  cout << "L2Dist(u,v) == " << err << " (nonpadded IO test)" << endl;

  // Test IO on padded field
  u.zeroPaddedModes();
  u.save("upad");
  FlowField w("upad");

  err = L2Dist(u,w);
  toterr += err;
  cout << "L2Dist(u,w) == " << err << " (padded IO test)" << endl;

  if (toterr < maxerr) {
    cerr << "\t   pass   " << endl;
    cout << "\t   pass   " << endl;
    return 0;
  }
  else {
    cerr << "\t** FAIL **" << endl;
    cout << "\t** FAIL **" << endl;
    cout << "   err == " << err << endl;
    cout << "maxerr == " << maxerr << endl;
    return 1;
  }
}
