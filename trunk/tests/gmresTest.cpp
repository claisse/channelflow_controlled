#include <iostream>
//#include <stdlib>
#include "channelflow/matrixutils.h"

using namespace std;
using namespace channelflow;
using Eigen::JacobiSVD;
using Eigen::ComputeFullU;
using Eigen::ComputeFullV;

int main() {

  //bool save = false;
  bool verbose = true;
  bool pass = true;

  Real maxerr = 1e-12;
  Real    err = 1.0;
  
  
  cerr << "gmresTest: " << flush;
  if (verbose) {
    cout << "\n====================================================" << endl;
    cout << "gmresTest\n\n";
  }

  {
    cout << "----------------------------------------------------" << endl;
    cout << "Test 1: Solve a small (10x10) random Ax=b problem." << endl;
    cout << "Final Ax-b residual and x-xtrue should both be small" << endl << endl;
    
    const int N=10;   
    MatrixXd A = MatrixXd::Random(N,N);
    ColumnVector xtrue = ColumnVector::Random(N);
    ColumnVector b = A*xtrue;

    GMRES gmres(b, N);

    Real bnorm = L2Norm(b);
    Real xnorm = L2Norm(xtrue);

    for (int n=1; n<=N; ++n) {
      cout << "test 1, " << n << "th Krylov iteration... " << endl;

      ColumnVector q  = gmres.testVector();
      ColumnVector Aq = A*q;
      gmres.iterate(Aq);

      ColumnVector x = gmres.solution(); // current estimate of soln
      ColumnVector tmp = A*x-b;
      Real Axberr = L2Norm(tmp)/bnorm;
      tmp = x - xtrue;
      Real   xerr = L2Norm(tmp)/xnorm;
      
      cout << "   gmres residual == " << gmres.residual() << endl;
      cout << "       |Ax-b|/|b| == " << Axberr << endl;
      cout << "|x-xtrue|/|xtrue| == " << xerr << endl << endl;

      err = Axberr + xerr;
    }
    if (err > maxerr) {
      cout << "test 1 failed" << endl;
      pass = false;
    }
  }
    
  {
    cout << "----------------------------------------------------" << endl;
    cout << "Test 2: Solve a larger (40x40) Ax=b problem with exponentially\n";
    cout << "decaying singular values. Ax-b residual should decay rapidly." << endl;
    cout << "x-xtrue error will probably always be large since A is ill-conditioned" << endl << endl;

    const int N=40;
    MatrixXd A = MatrixXd::Random(N,N);

    // Compute SVD then replace diagonal with 2^-n, to get exponentially
    // graded singular values and a poorly conditioned A

    JacobiSVD<MatrixXd> svd(A, ComputeFullU | ComputeFullV);
    MatrixXd U  = svd.matrixU();
    MatrixXd Vt  = svd.matrixV().transpose();
    MatrixXd D(N,N);
    D(0,0) = 1.0;
    for (int i=1; i<N; ++i)
      D(i,i) = 0.5*D(i-1,i-1);
      
    A = U*D*Vt;

    ColumnVector xtrue = ColumnVector::Random(N);
    ColumnVector b = A*xtrue;

    GMRES gmres(b, N);

    Real bnorm = L2Norm(b);
    Real xnorm = L2Norm(xtrue);
    
    Real c = 100; // constant for Ax-b error convergence, err ~ c 1/2^n

    for (int n=1; n<=N; ++n) {
      cout << "test 2, " << n << "th Krylov iteration... " << endl;

      ColumnVector q  = gmres.testVector();
      ColumnVector Aq = A*q;
      gmres.iterate(Aq);

      ColumnVector x = gmres.solution(); // current estimate of soln
      ColumnVector tmp = A*x-b;
      Real Axberr = L2Norm(tmp)/bnorm;
      tmp = x - xtrue;
      Real   xerr = L2Norm(tmp)/xnorm;

      cout << "   gmres residual == " << gmres.residual() << endl;
      cout << "       |Ax-b|/|b| == " << Axberr << endl;
      cout << "|x-xtrue|/|xtrue| == " << xerr << endl << endl;

      if (Axberr > c*pow(0.5, n-1)) {
	pass = false;
	cout << "failure: Ax-b convergence is slower than expected (" << c*pow(0.5,n-1) << endl << endl;
      }
      err = Axberr;
    }
    if (err > maxerr) {
      cout << "test 2 failed" << endl;
      pass = false;
    }
  }

  if (pass) {
    cerr << "\t   pass   " << endl;
    cout << "\t   pass   " << endl;
    return 0;
  }
  else {
    cerr << "\t** FAIL **" << endl;
    cout << "\t** FAIL **" << endl;
    return 1;
  }
}
  
