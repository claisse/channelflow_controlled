2007-09-30

For channelflow-2.0

  change FlowField data layout to i,x,z,y
  change FlowField declaration to number of Kx,Ky,Kz (?)
  switch to 3/2 dealiasing (?)
  simplify FlowField binary IO format --always spectral
  implement Tecplot output
  implement (switch to?) velocity-vorticity
  implement (switch to?) integral solution of Helmholtz eqns
  multithread using OpenMP (Intel Thread Building Blocks reviewed, declined)
  python wrapper for interactive use
  add body force
  add Boussinesq approx
  better config and install system, automate detection of
    FFTW
x   octave
x	  write channelflow tutorial
  replace error-and-exits w exceptions
  simplify/unify DNS parameter setting: timestepping, algorithm, flow

  Name changes
    ChebyCoeff -> ChebyExpan
    findorbit  -> findsolution
    couette    -> integrate
    Parabola   -> Parabolic
    PlaneCouette -> Linear

2007-03-06

Ideas from Homology workshop/projects for others.
  E/M/H == easy/hard, LP/MP/HP == low/medium/high priority.

x Move PCF-utils to channelflow svn. (E)
x Move base flow specification to DNSflags. (E)
x Allow ChebyCoeff U(y), PlaneCouette, and Poiseuille specifications. (E)
x Write function to newton-search FlowField translation for max symmetry.(M,LP)
x Implement DNS linearization about (relative) equilibrium. (M,HP)
x Implement symmetry restrictions within DNS (H,LP)
x Implement PCF/Poisseuille symmetry group and operation on fields
x Consolidate Arnoldi iteration and GMRES in class to simplify PCF-utils codes
    and eliminate repeated code.
x Write new basis-less vector2field based on Fourier-Chebyshev coeffs. (M,HP)
x Handle Octave/Octave-free compilation in autoconf or switch to CMake
X Spiff up website. (M,LP)
X Decide if PCF-utils should go in channelflow distribution tarballs. (E,MP)

  Add symmetry restrictions to makeBasis, vector2field, field2vector. (M,HP)
  Implement velocity-vorticity. (M,LP)
  Implement DNS linearization about (relative) periodic orbit. (H,MP)
  Implement symmetry restrictions within DNS (H,LP)
  Write basic channelflow tutorial. (E,LP)
  Write Matlab interface to makebasis and projectseries. (?,MP)
  Change all assert(main_algorithm_) in DNS to if (main_algorithm_ == 0) err
  Move nonlinearity calculations into classes to consolidate their state info.
  Enable DealiasY.
  Exceptions

To-do list for channelflow-1.0


2006-11-30

  Check for a!=-1 b!=1 errors in ChebyCoeff/FlowField
x Implement DNS linearization about initial FlowField

2005-09-30

x Upgrade to FFTW-3.0
x Improve config and make system
x Automate tests.

x Check IO funcs for file "filebase" if "filebase.ext" fails
x Remove "using namespace std" from chflow headers.
x Put channelflow in chflow namespace.
- Make sure example programs have good integration params.
x Change to big-endian binary IO format, add version number to FlowField IO
x Move transform guts to ChebyCoeff and ComplexChebyCoeff, from ChebyTransform
- Update documentation, particularly for new DNS class
x Make setting of dPdx and Ubulk more straightforward

  Track down pressure phase error in RK schemes.
  Review d/dx behavior of last kx mode, fix if necess, ditto for d/dz.
  Review FlowField, DNS assumptions on even/oddness of Nx,Ny,Nz.
  Regularize dottimes functions: move to diffops and fix internal state trans.
  Regularize silent Spec<->Phy conversions
  Design, implement monitoring scheme for silent S<->P, ChebyTrans constrctn.
  Regularize norms and innerproducts: check vol-norm, default to true, S<->P.
x Make BasisFunc d-dimensional.
x L2InnerProduct for FlowField

  Spruce up website with quanta, include validation codes and graphics
  Implement corrected-euler timestepping
  Switch ComplexChebyExpan to Complex array, from 2 Real arrays (Re, Im parts)
  Grep for inefficient, fix
x Change lib names to chflow
x Rearrange chebyshev header: classes first, then functions
  Switch dealiasing from 2/3 to 3/2 style.
  Name changes: Coeff -> Expan, Complex -> Cmplx, BasisFunc -> FourierProfile
  Add quick-and-dirty FFT for non-repeated use.
  Try implementing FlowField y-transforms in place with strided FFT
  Tensorize FlowField

  Change default install directory to $(top_srcdir)
x Improve optimize compile flags: -O2 -fomit-frame-pointer -march=pentium4/athlon/... -mtune=ditto
  Install data (OS eigenfuncs, etc, into appropriate place)

  configure option for octave?
  configure option for nana?
