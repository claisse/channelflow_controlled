#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"


using namespace std;
using namespace channelflow;


// utot == up + Up == ub + uB  =>  ub == up + Up - Ub
int main(int argc, char* argv[]) {


  string purpose("produce and save laminar flow profile for either imposed dPdx or imposed Ubulk");

  ArgList args(argc, argv, purpose);
  
  const Real Ubulk    = args.getreal("-Ubulk", "--Ubulk",  0.0, "imposed bulk velocity, if nonzero");
  const Real dPdx     = args.getreal("-dPdx",  "--dPdx",   0.0, "imposed pressure grad, if nonzero");
  const Real Uwall    = args.getreal("-Uwall", "--Uwall",  1.0, "imposed wall velocity U=+/-Uwall at y=+/-h");

  const Real Reynolds = args.getreal("-R", "--Reynolds", 400.0, "pseudo-Reynolds number 1/nu (needed only for imposed dPdx)");
  /***/ Real nu       = args.getreal("-nu", "--nu",       0,  "kinematic viscosity (takes precedence over Reynolds, if nonzero)");

  const int Ny = args.getint ("-Ny", "--Ny", 49, "input  field up");
  const Real a = args.getreal("-a", "--a",  -1.0, "lower wall position");
  const Real b = args.getreal("-b", "--b",   1.0, "upper wall position");

  const string Uname  = args.getstr ("-o",  "--o", "Ubase", "filename stub for output profile U(y)");
  args.check(); 
  if (Ubulk != 0.0 && dPdx != 0.0) {
    cerr << "Please specify a nonzero value for Ubulk *or* dPdx, not both" << endl;
    exit(1);
  }
  args.save("./");

  if (nu == 0) 
    nu = 1/Reynolds;

  MeanConstraint mc = (dPdx != 0.0) ? PressureGradient : BulkVelocity;

  ChebyCoeff U = laminarProfile(nu, mc, dPdx, Ubulk, a, b, -Uwall, Uwall, Ny);
  U.save(Uname);
}
  
