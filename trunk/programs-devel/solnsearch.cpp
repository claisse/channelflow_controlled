#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <time.h>
#include <octave/oct.h>
#include <octave/CColVector.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/EIG.h>
#include <octave/dbleSVD.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/periodicfunc.h"
#include "octutils.h"


using namespace std;
using namespace channelflow;

typedef FieldSymmetry Symmetry;

// Integrate until you hit the Poincare section, or die trying.
FlowField f(const FlowField& u, Real nu, TimeStep dt, DNSFlags flags, Real& T, Real& CFL);

FlowField perturbation(const FlowField& u, const array<FlowField>& ef, Real eps, Real smoothness);

Real L2DistS(const FlowField& u, const FlowField& v, const array<Symmetry>& s, Symmetry& sigma);

int main(int argc, char* argv[]) {

  string purpose("Autonomous engine for finding periodic orbits:\n"
		 "Integrate flow until a close recurrence occurs.\n"
		 "Then apply Newton-Krylov to find periodic orbit.\n"
		 "Repeat. ALPHA VERSION with unstated assumptions\n");

  // assumptions are 
  //  solution type is periodic orbit
  //  no relative orbits
  //  hard-coded values in checks on unorm
  //  poincare section search

  ArgList args(argc, argv, purpose);

  DNSFlags dnsflags;
  GMRESHookstepFlags srchflags;

  //const bool eqb        = args.getflag("-eqb",  "--equilibrium",   "search for equilibrium or relative equilibrium (trav wave)");
  //const bool orb        = args.getflag("-orb",  "--periodicorbit", "search for periodic orbit or relative periodic orbit");
  //const bool poincsrch  = args.getflag("-poinc",  "--poincare",    "(relative) periodic orbit search constrained to I-D=0 Poincare section");
  //srchflags.xrelative   = args.getflag("-xrel",  "--xrelative",    "search over x phase shift for relative orbit or eqb");
  //srchflags.zrelative   = args.getflag("-zrel",  "--zrelative",    "search over z phase shift for relative orbit or eqb");

  const Real guessradius = args.getreal("-gr", "--guessradius",  0.04,  "max radius for close recurrence");
  const Real nbrhdradius = args.getreal("-nr", "--neighborhoodradius",  0.10,  "get at least this far away from initial point before looking for close recurrences");
  const bool chainsrch  = args.getflag("-ch",  "--chainsearch",   "start new guess searches from most recent solution");
  const string sigstr   = args.getstr ("-sigmas", "--sigmas", "",    "file containing possible symmetries sigma for preperiodic orbit sigma f^T(u) - u == 0");
  const string symmstr  = args.getstr ("-symms", "--symmetries", "", "file containing generators of isotropy group for symmetry-constrained search");
  const int  seed       = args.getint ("-sd",  "--seed",       1, "seed for random number generator");
  const Real smooth     = args.getreal("-sm",  "--smoothness", 0.5, "smoothness of random erturbation, betwn 0 and 1");
  const int  Nef        = args.getint ("-Nef",  "--Neigenfuncs",       0, "number of eigenfunctions to use in perturbatiosns");
  const string efdir    = args.getpath("-efd",  "--eigenfuncdir", "./", "directory containing eigenfunctions");
  const Real eps        = args.getreal("-ep", "--epsPerturb",  0.01,  "u = u0 + eps sum <random> ef[n]");
  /***/ Real T          = args.getreal("-T", "--maptime",  20.0,  "initial guess for orbit period or time of eqb/reqb map f^T(u)");
  const int Nsearch     = args.getint("-Ns", "--Nsearch",  1000,  "stop after this many orbit searches");
  const Real Reynolds   = args.getreal("-R", "--Reynolds", 400.0, "Reynolds number");

  dnsflags.baseflow     = PlaneCouette;
  dnsflags.timestepping = s2stepmethod(args.getstr("-ts", "--timestepping", "SBDF3", "time step algorithm: sbdf[1234]|cnab2|cnrk2|smrk2"));
  dnsflags.dealiasing   = DealiasXZ;
  dnsflags.nonlinearity = s2nonlmethod(args.getstr("-nl", "--nonlinearity", "Rotational", "nonlinearity method: rot|conv|skew|div|alt"));
  dnsflags.constraint   = PressureGradient;
  dnsflags.dPdx         = 0.0;
  dnsflags.verbosity    = Silent;

  const bool vardt     = args.getflag("-vdt","--variableDt", "adjust dt to keep CFLmin<=CFL<CFLmax");
  const Real dtarg     = args.getreal("-dt","--timestep",  0.03125, "(initial) integration timestep");
  const Real dtmin     = args.getreal("-dtmin", "--dtmin", 0.001, "minimum timestep");
  const Real dtmax     = args.getreal("-dtmax", "--dtmax", dtarg,  "maximum timestep");
  const Real dTCFL     = args.getreal("-dTCFL", "--dTCFL", 1.00,  "check CFL # at this interval");
  const Real CFLmin    = args.getreal("-CFLmin", "--CFLmin", 0.50, "minimum CFL number");
  const Real CFLmax    = args.getreal("-CFLmax", "--CFLmax", 0.65, "maximum CFL number");

  srchflags.epsSearch = args.getreal("-es",  "--epsSearch",   1e-11, "stop search if L2Norm(s f^T(u) - u) < epsEQB");
  srchflags.epsKrylov = args.getreal("-ek",  "--epsKrylov",   1e-14, "min. condition # of Krylov vectors");
  srchflags.epsDu     = args.getreal("-edu", "--epsDuLinear", 1e-7,  "relative size of du to u in linearization");
  srchflags.epsDt     = args.getreal("-edt", "--epsDtLinear", 1e-5,  "size of dT in linearization of f^T about T");
  srchflags.epsGMRES  = args.getreal("-eg",  "--epsGMRES",    1e-3,  "stop GMRES iteration when Ax=b residual is < this");
  srchflags.epsGMRESf = args.getreal("-egf",  "--epsGMRESfinal",  0.05,  "accept final GMRES iterate if residual is < this");
  srchflags.centdiff  = args.getflag("-cd", "--centerdiff", "centered differencing to estimate differentials");

  srchflags.Nnewton   = args.getint( "-Nn", "--Nnewton",   20,    "max number of Newton steps ");
  srchflags.Ngmres    = args.getint( "-Ng", "--Ngmres",   100,    "max number of GMRES iterations per restart");
  srchflags.Nhook     = args.getint( "-Nh", "--Nhook",     20,    "max number of hookstep iterations per Newton");

  srchflags.delta     = args.getreal("-d",    "--delta",      0.01,  "initial radius of trust region");
  srchflags.deltaMin  = args.getreal("-dmin", "--deltaMin",   1e-12, "stop if radius of trust region gets this small");
  srchflags.deltaMax  = args.getreal("-dmax", "--deltaMax",   0.1,   "maximum radius of trust region");
  srchflags.deltaFuzz = args.getreal("-df",   "--deltaFuzz",  0.01,  "accept steps within (1+/-deltaFuzz)*delta");
  srchflags.lambdaMin = args.getreal("-lmin",   "--lambdaMin", 0.2,  "minimum delta shrink rate");
  srchflags.lambdaMax = args.getreal("-lmax",   "--lambdaMax", 1.5,  "maximum delta expansion rate");
  srchflags.lambdaRequiredReduction = 0.5; // when reducing delta, reduce by at least this factor.
  srchflags.improvReq = args.getreal("-irq",  "--improveReq", 1e-3,  "reduce delta and recompute hookstep if improvement "
				      " is worse than this fraction of what we'd expect from gradient");
  srchflags.improvOk  = args.getreal("-iok",  "--improveOk",   0.10, "accept step and keep same delta if improvement "
				      "is better than this fraction of quadratic model");
  srchflags.improvGood= args.getreal("-igd",  "--improveGood", 0.75, "accept step and increase delta if improvement "
				      "is better than this fraction of quadratic model");
  srchflags.improvAcc = args.getreal("-iac",  "--improveAcc",  0.10, "recompute hookstep with larger trust region if "
				      "improvement is within this fraction quadratic prediction.");

  srchflags.TscaleRel = args.getreal("-Tsc", "--Tscale",     20,    "scale time in hookstep: |T| = Ts*|u|/L2Norm(du/dt)");
  srchflags.dudtortho = args.getbool("-tc",  "--dudtConstr", true,  "require orthogonality of step to dudt");
  srchflags.orthoscale= args.getreal("-os",  "--orthoScale", 1,     "rescale orthogonality constraint");
  srchflags.dudxortho = args.getbool("-xc", "--dudxConstraint", true,"require orthogonality of step to dudx and dudz");
  srchflags.Rscale    = args.getreal("-rs", "--relativeScale", 1, "scale relative-search variables by this factor");
  srchflags.outdir    = args.getpath("-o",  "--outdir", "./", "output directory");
  srchflags.autostop  = true;
  srchflags.stopfactor= args.getreal("-sf", "--stopfactor", 0.98, "stop search if residual doesn't decrease this much");
  
  const string logfile  = args.getstr ("-log","--logfile", "stdout",  "output log (filename or \"stdout\")");
  const string uname    = args.getstr(1, "<flowfield>", "initial guess for Newton search");

  //if (eqb && !orb)
  //srchflags.solntype = Equilibrium;
  //else if (orb && !eqb)
  srchflags.solntype = PeriodicOrbit;
  //else {
  //cerr << "Please choose either -eqb or -orb option to search for (relative) equilibrium or (relative) periodic orbit" << endl;
  //exit(1);
  //}
  args.check();
  args.save("./");

  cout << "Working directory == " << pwd() << endl;
  cout << "Command-line args == ";
  for (int i=0; i<argc; ++i) cout << argv[i] << ' '; cout << endl;

  const Real unormmin = 0.10;
  const Real unormmax = 1.00;
  const Real nu = 1/Reynolds;
  //const Real secperclock = 1.0/Real(CLOCKS_PER_SEC);
  const Real hrsperclock = 1.0/Real(3600*CLOCKS_PER_SEC);

  srand48(seed);

  SymmetryList sigmas(1);
  if (sigstr.length() != 0) 
    sigmas = SymmetryList(sigstr);

  SymmetryList symms(symmstr);
  if (symmstr.length() > 0) {
    cout << "Restricting flow to invariant subspace generated by symmetries" << endl;
    symms = SymmetryList(symmstr);
    cout << symms << endl;
    dnsflags.symmetries = symms;
  }

  // Construct random perturbation of u along eigenfunctions
  FlowField u0(uname);
  array<FlowField> ef(Nef);
  for (int n=0; n<Nef; ++n) 
    ef[n] = FlowField(efdir + "ef" + i2s(n+1));
  
  FlowField u = perturbation(u0, ef, eps, smooth);
  
  TimeStep dt(dtarg, dtmin, dtmax, dTCFL, CFLmin, CFLmax, vardt);

  bool startup = true;
  int nsolution = 0;
  int nfailure = 0;
  clock_t guess_time = 0; // cpu time spent looking for good guesses
  clock_t soln_time = 0;  // cpu time spent looking for good guesses

  for (int nsearch=0; nsearch<Nsearch; ++nsearch) {

    cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    cout << "starting search #" << nsearch << endl;

    //Real Tguess = 0;
    Real CFL = 0;
    FlowField fu;
    Symmetry sigma; 

    // Find a decent initial guess
    while (true) {

      cout << "Integrating u to next Poincare crossing..." << endl;
      clock_t tick = clock();
      fu = f(u, nu, dt, dnsflags, T, CFL);
      Real funorm = L2Norm(fu);
      clock_t tock = clock();
      guess_time += tock-tick;

      cout << "So far we have consumed " << guess_time*hrsperclock << "\t hours   looking for guesses" << endl;
      cout << "                    and " << soln_time *hrsperclock << "\t hours   solving the equations" << endl;
      cout << "for a ratio of " << Real(guess_time)/Real(soln_time) << " == guess/solution times " << endl;

      // If fu has blown up or decayed to laminar, start over with a new perturbation
      if (funorm < unormmin || funorm > unormmax) {
	cout << "bother! u has decayed or blown up: L2Norm(fu) == " << funorm << endl;
	cout << "starting over with another perturbation..." << endl;
	u = perturbation(u0, ef, eps, smooth);
	startup = true;
	continue;
      }
      
      Symmetry sigma0;
      Real d0 = L2DistS(fu, u0, sigmas, sigma0); 
      Real d  = L2DistS(fu, u,  sigmas, sigma); 
      cout << "nsearch == " << nsearch 
	   << ", L2Dist(fu,u0) == " << d0 
	   << ", L2Dist(fu,u) == " << d 
	   << ", T  == " << T
	   << ", sigma == " << sigma 
	   << ", CFL == " << CFL << endl; 
	

      // Don't be fooled by near-recurrences of the initial perturbation 
      // Integrate until we get at least one bad guess before searching on a good one
      if (startup && d0<nbrhdradius) {
	cout << "startup == " << startup << endl;
	cout << "d0      == " << d0 << endl;
	cout << "nbrrad  == " << nbrhdradius << endl;
	cout << "we are still in the neighborhood of the initial orbit" << endl;
	cout << "keep integrating until we move away" << endl;
	u = fu;
	continue;
      }
      else if (startup && d0>=nbrhdradius) {
	cout << "excellent! we have finally left the neighborhood of the initial orbit" << endl;
	startup = false;
	u = fu;
	continue;
      }
      else if (d>=guessradius) {
	cout << "that is not a close recurrence" << endl;
	u = fu;
	continue;
      }
      else {
	cout << "excellent! we have found a close recurrence. Time to start searching" << endl;
	break;
      }
    }
     
    string searchdir = "guess-" + i2s(nsearch) + "/";

    // At this point we have a close recurrence! Search on it.
    cout << "-------------------------------------------------" << endl;
    cout << "Time to search! THe initial guess is " << endl;
    cout << "T == " << T << endl;
    cout << "sigma == " << sigma << endl; 
    cout << "L2Dist(sigma f^T u, u) == " << L2Dist(sigma(fu), u) << endl;
    cout << "searchdir == " << searchdir << endl;

    mkdir(searchdir);
    ofstream logstream((searchdir + "findsoln.log").c_str());
    dnsflags.logstream = &logstream;
    srchflags.logstream = &logstream;
    srchflags.outdir    = searchdir;


    FlowField usoln(u);
    PoincareCondition* hpoincare = new DragDissipation();

    clock_t tick = clock();
    Real residual = GMRESHookstep(usoln, T, sigma, Reynolds, hpoincare, srchflags, dnsflags, dt, CFL);
    clock_t tock = clock();
    soln_time += tock-tick;

    cout << "Done searching for orbit. The answer from Newton-Krylov-hoosktep refinement is " << endl;
    cout << "                    CFL == " << CFL << endl;
    cout << "                      T == " << endl;
    cout << "                  sigma == " << sigma << endl;
    cout << "              L2Norm(u) == " << L2Norm(usoln) << endl;
    cout << "L2Norm(sigma f^T u - u) == " << residual << endl;

    if (residual < srchflags.epsSearch) {
      cout << "Great news! We found a periodic orbit solution! :-)" << endl;
      cout << "Whew! No time to rest on our laurels! Get right back to work and look for another!" << endl;
      rename(searchdir, "solution-" + i2s(nsearch));
      
      if (chainsrch) {
	cout << "Look for new guesses starting from the new solution u " << endl;
	u0 = usoln;
	u = perturbation(u0, ef, eps, smooth);	
	startup = true;
      }
      else {
	cout << "Look for new guesses from f(u) of last failed guess" << endl;
	u = fu;
	startup = false;
      }
	
    }
    else {
      cout << "Bummer! We did not find a periodic orbit. :-(" << endl;
      cout << "Time to put all that hard work into the circular filing cabinet and try again" << endl;
      cout << "Look for new guesses from f(u) of last failed guess" << endl;

      mkdir("failures");
      rename(searchdir, "failures/guess-" + i2s(nsearch));

      u = fu;
      startup = false;

    }

    cout << "So far we have consumed " << guess_time*hrsperclock << "\t hours   looking for guesses" << endl;
    cout << "                    and " << soln_time *hrsperclock << "\t hours   solving the equations" << endl;
    cout << "for a ratio of " << Real(guess_time)/Real(soln_time) << " == guess/solution times " << endl;

    //u.save(srchflags.outdir + "usoln");
    //sigma.save(srchflags.outdir + "sigmasoln");
    //save(T, srchflags.outdir + "Tsoln");
    //save(Reynolds, srchflags.outdir + "Resoln");
  }
}


FlowField f(const FlowField& u, Real nu, TimeStep dt, DNSFlags flags, Real& T, Real& CFL) {
  
  FlowField fu(u);
  FlowField q(u.Nx(), u.Ny(), u.Nz(), u.Nd(), u.Lx(), u.Lz(), u.a(), u.b());
  DragDissipation h;

  T = 0;
  DNSPoincare dns(fu, &h, nu, dt, flags, T);
  
  dns.advance(fu, q, dt.n()); // advance at least dT before checknig for crossings.
  
  int crosssign = -1;
  cout << "f(u)" << flush;
  while (true) {
    
    cout << "." << flush;
    bool crossed = dns.advanceToSection(fu, q, dt.n(), crosssign);
    
    int it = iround(dns.time());
    if (it != 0 && it % 10 == 0) 
      cout << it << flush;

    if (crossed) {
      fu = dns.ucrossing();
      T  = dns.tcrossing();
      CFL = dns.CFL();
      cout << endl;
      return fu;
    }
    Real norm = chebyNorm(u);
    if (norm < 0.10) {
      fu *= 0;
      T = dns.time();
      CFL = dns.CFL();
      cout << endl;
      return fu;
    }
    else if (norm > 1.0) {
      cout << "f(u) error: chebNorm(u) > 1. Resetting to laminar and returning" << endl;
      fu *= 0;
      T = dns.time();
      CFL = dns.CFL();
      cout << endl;
      return fu;
    }
  }
}

FlowField perturbation(const FlowField& u0, const array<FlowField>& ef, Real eps, Real smooth) {

  cout << "perturbation..." << endl;
  FlowField u(u0);
  cout << "entering L2Norm(u) == " << L2Norm(u) << endl;
  for (int n=0; n<ef.length(); ++n) {
    FlowField tmp(ef[n]);
    tmp *= eps*randomReal(-1, 1);
    u += tmp;
  }
  if (ef.length() == 0) {
    FlowField tmp(u.Nx(), u.Ny(), u.Nz(), u.Nd(), u.Lx(), u.Lz(), u.a(), u.b());
    tmp.addPerturbations(1.0, 1.0-smooth);
    tmp *= eps/L2Norm(tmp);
    u += tmp;
  }
  cout << "L2Dist(u,u0) == " << L2Dist(u,u0) << endl;
  return u;
}

Real L2DistS(const FlowField& u, const FlowField& v, const array<Symmetry>& s, Symmetry& sigma) {

  //cout << "L2DistS..." << endl;
  Symmetry identity;
  Real dmin = L2Dist(u,v);
  sigma = identity;

  //cout << "d    == " << dmin << ",\t     s == " << identity << endl;
  
  for (int n=0; n<s.length(); ++n) {
    if (s[n] == identity) 
      continue;
    FlowField su = s[n](u);
    Real d = L2Dist(su, v);
    //cout << "d    == " << d << ",\t     s == " << s[n] << endl;
    //cout << "d == " << dmin << ", s == " << s[n] << endl;
    if (d < dmin) {
      dmin = d;
      sigma = s[n];
    }
  }
  //cout << "dmin == " << dmin << ",\t sigma == " << sigma << endl;
  return dmin;
}

/* findorbit.cpp, channelflow-1.3, www.channelflow.org
 *
 * Copyright (C) 2001-2009  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu
 * Center for Nonlinear Sciences, School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
