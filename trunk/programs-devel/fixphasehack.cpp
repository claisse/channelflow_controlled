#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"
#include "channelflow/utilfuncs.h"
#include "channelflow/periodicfunc.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("fix x and/or z phase of unsymmetric solutions by fairly arbitrary conditions");
  ArgList args(argc, argv, purpose);

  const bool fx       = args.getflag("-x",  "--xphase", "fix x phase so that f(x) is locally even/odd about x=Lx/2");
  const bool fz       = args.getflag("-z",  "--zphase", "fix z phase so that g(z) is locally even/odd about z=Lz/2");
  const bool ex       = args.getflag("-xe", "--xeven",  "even parity in x (default odd)");
  const bool ez       = args.getflag("-ze", "--zeven",  "even parity in z (default odd)");
  const int  ix       = args.getint ("-ix", "--ixcomponent", 0, "i for f(x) = u(x,0,Lz/2,i) - <u(x,0,Lz/2,i)>_x");
  const int  iz       = args.getint ("-iz", "--izcomponent", 0, "i for g(z) = <u_i>_{xz}(z)");
  const Real ax       = args.getreal("-ax", "--ax", 0.0, "initial guess for x phase shift to zero f(Lx/2), units of ax = delta x/Lx");
  const Real az       = args.getreal("-az", "--az", 0.0, "initial guess for z phase shift to zero g(Lz/2), units of az = delta z/Lz");
  //const bool save     = args.getflag("-s", "--savedata",   "save initial f(x), g(z) into f.asc, g.asc");
  const string iname  = args.getstr(2, "<flowfield>", "input field");
  const string oname  = args.getstr(1, "<flowfield>", "output field");


  args.check();
  args.save();

  FlowField u(iname);
  u.makeSpectral();

  if (fz) {
    FieldSymmetry tauz = zfixphasehack(u, az, iz, ez ? Even : Odd);
    cout << "z phase shift == " << tauz << endl;
    u *= tauz;
  }
  if (fx) {
    FieldSymmetry taux = xfixphasehack(u, ax, ix, ex ? Even : Odd);
    cout << "x phase shift == " << taux << endl;
    u *= taux;
  }
  u.save(oname);
}

