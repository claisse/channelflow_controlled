#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/utilfuncs.h"
#include "octutils.h"

using namespace std;
using namespace channelflow;

Real zpadding(FlowField& u, Real eps=1e-3); 

int main(int argc, char* argv[]) {

  string purpose("determine fraction of z-domain occupied by z-localized solution");
  ArgList args(argc, argv, purpose);

  const Real eps = args.getreal("-e", "--eps",   1e-04, "define edges of solution by first occurrences of sqrt(1/2 u{xy}>(z) > eps");
  const string uname = args.getstr(1, "<flowfield>",    "input field");

  args.check();

  FlowField u(uname);
  
  cout << setprecision(2) << zpadding(u, eps) 
       << " == fraction of box occupied by laminar flow, as measure by\n"
       << "sqrt(<energy>_{xy}(z)) > " << eps << endl;
}


Real zpadding(FlowField& u, Real eps) {
  u.makeSpectral();

  int  Nz  = u.Nz();
  int  Nz2 = Nz/2;

  // Compute the xy-average of streamwise velocity u
  //FlowField uxyavg(4, 1, u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
  //for (int mz=0; mz<u.Mz(); ++mz)
  //uxyavg.cmplx(0,0,mz,0) = u.cmplx(0,0,mz,0);
  FlowField e = energy(u);
  e.makeSpectral();

  PeriodicFunc exyavg(e.Nz(), e.Lz(), Spectral);
  for (int mz=0; mz<e.Mz(); ++mz)
    exyavg.cmplx(mz) = e.cmplx(0,0,mz,0);

  // Find first place from left where uxyzvg is above small threshold
  Real leftedge = 0;
  exyavg.makePhysical();
  ofstream os("g.asc");

  for (int nz = 0; nz<Nz; ++nz) 
    os << e.z(nz) << " " << exyavg(nz) << "\n";
  os << e.Lz() << " " << exyavg(0) << "\n";

  Real eps2 = square(eps);
  //cout << "eps == " << eps << endl;
  for (int nz = 0; nz<=Nz2; ++nz) {
    //cout << e.z(nz) << " " << abs(exyavg(nz)) << endl;
    if (exyavg(nz) < eps2) 
      leftedge = e.z(nz);      
    else
      break;
  }

  //cout << "Find zero to right of center" << endl;    
  Real rightedge = e.Lz();
  exyavg.makePhysical();
  //coet << "Nz   == " << u.Nz() << endl;
  //cout << "Nz/2 == " << u.Nz()/2 << endl;
  for (int nz = u.Nz()-1; nz>=u.Nz()/2; --nz) {
    //cout << e.z(nz) << " " << abs(exyavg(nz)) << endl;
    if (exyavg(nz) < eps2) 
      rightedge = e.z(nz);        
    else
      break;
  }
  
  //cout << leftedge << endl;
  //cout << rightedge << endl;
  return 1 - (rightedge-leftedge)/e.Lz();
}


