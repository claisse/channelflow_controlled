#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

#include "octutils.h"

using namespace std;
using namespace channelflow;

// This program verifies with DNS the eigenvalues and eigenfunctions of PCF 
// equilibria obtained from Arnolid iteration. It (sequentially) reads 
// in an ew,ef pair and outputs ten quantities as a function of integration 
// time:
// dnsnorm : L2Norm(udns)
// eignorm : L2Norm(ueig)
// errnorm : L2Dist(udns,ueig)
// magnerr : abs(L2Norm(udns) - L2Norm(ueig))/L2Norm(ueig)
// phaserr : L2IP(udns,ueig)/(L2Norm(udns)*L2Norm(ueig))
// aligerr : L2Dist(udns, ad0 e0 + ad1 e1)/L2Norm(udns)
//     ad0 : L2IP(udns,e0)
//     ad1 : L2IP(udns,e1) for complex ew, t for real ew
//     ae0 : L2IP(ueig,e0) 
//     ae1 : L2IP(ueig,e1) for complex ew, t for real ew

// Note that udns really means udns-u0, the displacement from the equilibrium.
// Note that the verification algorithm varies considerably between real-valued
// and complex-valued eigenvalues


// Returned matrices have cols (dnsnorm eignorm errnorm magnerr phaseerr);
// rows are at intervals dt*Nsteps
Matrix verify_real(const FlowField& u0, Real lambda, const FlowField& ueig, 
		   Real ueps, Real nu, TimeStep& dt, const DNSFlags& flags);

Matrix verify_cmplx(const FlowField& u0, Complex lambda,
		    const FlowField& ueigA, const FlowField& ueigB, 
		    Real ueps, Real nu, TimeStep& dt, const DNSFlags& flags);



int main(int argc, char* argv[]) {

  string purpose("verify results from Arnoldi by integrating eigenfunctions");
  ArgList args(argc, argv, purpose);

  const string odir = args.getpath("-o", "--outdir", "./", "output directory");
  const string idir = args.getpath("-i", "--indir",  "./", "input directory");
  const int Neig    = args.getint ("-N", "--Neig", 20, "# eigenvalues to verify");
  const int n0      = args.getint ("-n0", "--n0",   1, "index of first eigenvalues (0 or 1)");
  const bool IPmat  = args.getflag("-IP", "--IPmatrix", "output inner product matrix");
  const Real epsLin = args.getreal("-e", "--eps", 1e-8, "epsilon for linearation");
  const Real T      = args.getreal("-T",  "--T", 30,  "integration time");
  const Real dT     = args.getreal("-dT", "--dT", 1.0, "save interval");
  const Real dtarg  = args.getreal("-dt", "--timestep",  0.03125, "(initial) integration timestep");
  const Real dtmin  = args.getreal("-dtmin", "--dtmin", 0.001, "minimum timestep");
  const Real dtmax  = args.getreal("-dtmax", "--dtmax", dtarg,  "maximum timestep");
  const Real CFLmin = args.getreal("-CFLmin", "--CFLmin", 0.50, "minimum CFL number");
  const Real CFLmax = args.getreal("-CFLmax", "--CFLmax", 0.70, "maximum CFL number");
  const Real Reynolds = args.getreal("-R", "--Reynolds", 400, "Reynolds number");
  const bool vardt    = args.getflag("-vdt","--variableDt", "adjust dt to keep CFLmin<=CFL<CFLmax");
  const string nonlstr= args.getstr ("-nl", "--nonl", "rotational", "method for u dot grad u calculation. See README for options.");
  const string stepstr= args.getstr ("-ts", "--timestepping", "sbdf3", "timestepping algorithm. See README for options.");
  const string u0name = args.getstr(1, "<eqbfield>", "equilibrium field");

  args.check();
  args.save(odir);
  const Real nu = 1.0/Reynolds;
  const Real EPS_DOUBLE = 2e-16;

  DNSFlags flags; 
  flags.baseflow     = PlaneCouette;
  flags.timestepping = s2stepmethod(stepstr);
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = s2nonlmethod(nonlstr);
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;
  flags.verbosity    = Silent;
  cout << "flags == " << flags << endl;

  // Used to communicate timestep parameters to integration functions.
  cout << "dt     == " << dtarg << endl;
  cout << "dtmin  == " << dtmin << endl;
  cout << "dtmax  == " << dtmax << endl;
  cout << "CFLmin == " << CFLmin << endl;
  cout << "CFLmax == " << CFLmax << endl;
  TimeStep dt(dtarg, dtmin, dtmax, dT, CFLmin, CFLmax, vardt);
  dt.adjust_for_T(T, true);
  const int Nsteps = dt.N();

  ofstream os0((odir + "dnsnorm.asc").c_str());
  ofstream os1((odir + "eignorm.asc").c_str());
  ofstream os2((odir + "errnorm.asc").c_str());
  ofstream os3((odir + "magnerr.asc").c_str());
  ofstream os4((odir + "phaserr.asc").c_str());
  ofstream os5((odir + "alignerr.asc").c_str());
  ofstream os6((odir + "ad0.asc").c_str());
  ofstream os7((odir + "ad1.asc").c_str());
  ofstream os8((odir + "ae0.asc").c_str());
  ofstream os9((odir + "ae1.asc").c_str());
  ofstream os10((odir + "lambda.asc").c_str());
  ofstream os11((odir + "efIP.asc").c_str());

  if (IPmat) {
    cout << "Computing inner product matrix ..." << endl;
    Matrix IP(Neig,Neig);
    FlowField ef[Neig];
    Vector efnorm(Neig); 
    for (int n=n0; n<n0+Neig; ++n) {
      ef[n] = FlowField(idir + "ef" + i2s(n));
      efnorm(n) = L2Norm(ef[n]);
    }

    for (int m=0; m<Neig; ++m) {
      cout << m << ' ' << flush;
      for (int n=0; n<m; ++n) {
	IP(m,n) = L2IP(ef[m], ef[n])/(efnorm(m)*efnorm(n));
	IP(n,m) = IP(m,n);
      }
      IP(m,m) = 1.0;
    }
    save(IP, odir + "IP");
  }
 
  FlowField ustar(u0name);

  // Define gridsize
  //const int Ny = ustar.Ny();
  //const Real ya = ustar.a(); 
  //const Real yb = ustar.b();
  //const int Nx = ustar.Nx();
  //const int Nz = ustar.Nz();
  //const Real Lx = ustar.Lx(); 
  //const Real Lz = ustar.Lz();
  const char s = ' ';

  //for (int n=0; n<Neig; ++n) {
  for (int n=n0; n<n0+Neig; ++n) {
    cout << "Verifying eigenvalue " << n << endl;

    Complex lambda;
    load(lambda, idir + "ew"+i2s(n));
    bool real_eig = (Im(lambda) == 0.0);
    bool cmplx_eig = (!real_eig && n<Neig); // Need a cmplx eig *pair*

    // How many times to write results to output. 1 for real, 2 for cmplx pair,
    // so that output row numbers match eigval indices
    int R = 0; 
    Matrix results;
    
    // Real eigenvalue
    if (real_eig) {
      R = 1;
      cout << "Real-valued eigenvalue lambda == " << lambda << endl;
      FlowField ueig(idir + "ef" + i2s(n));
      results = verify_real(ustar, Re(lambda), ueig, epsLin, nu, dt, flags);

      os10 << Re(lambda) << s << Im(lambda) << endl;
    }
    // Complex eigenvalue
    else if (cmplx_eig) {
      R = 2;
      cout << "Complex-valued eigenvalue lambda == " << lambda << endl;
      Real period = 2*pi/abs(Im(lambda));
      Real multip = exp(Re(lambda)*period);
      cout << "period == " << period << endl;
      cout << "multip == " << multip << endl;

      Complex lambda_conj;
      load(lambda_conj, idir + "ew" + i2s(n+1));
      cout << "     conjugate eigenvalue lambda == " << lambda_conj << endl;
      if (abs(lambda - conj(lambda_conj)) > EPS_DOUBLE) {
	cerr << "ew" << n << " and ew" << n+1 << " are not conjugates" << endl;
	exit(1);
      }

      FlowField eigA(idir + "ef" + i2s(n));
      FlowField eigB(idir + "ef" + i2s(n+1));
      cout << "L2Norm(eigA) == " << L2Norm(eigA) << endl;
      cout << "L2Norm(eigB) == " << L2Norm(eigB) << endl;
      results = verify_cmplx(ustar, lambda, eigA, eigB, epsLin, nu, dt, flags);

      os10 << Re(lambda) << s <<  Im(lambda) << endl;
      os10 << Re(lambda) << s << -Im(lambda) << endl;
      ++n;
    }

    cout << "saving results for n==" << n << ", R==" << R << endl;
    for (int r=0; r<R; ++r) {
      for (int n=0; n<Nsteps; ++n) {
	os0 << results(n,0) << s;
	os1 << results(n,1) << s;
	os2 << results(n,2) << s;
	os3 << results(n,3) << s;
	os4 << results(n,4) << s;
	os5 << results(n,5) << s;
	os6 << results(n,6) << s;
	os7 << results(n,7) << s;
	os8 << results(n,8) << s;
	os9 << results(n,9) << s;
      }
      os0 << endl;
      os1 << endl;
      os2 << endl;
      os3 << endl;
      os4 << endl;
      os5 << endl;
      os6 << endl;
      os7 << endl;
      os8 << endl;
      os9 << endl;
    }
  }
}

Matrix verify_real(const FlowField& ustar, Real lambda, const FlowField& eig, 
		   Real eps, Real nu, TimeStep& dt, const DNSFlags& flags) {
  
  FlowField p(ustar.Nx(), ustar.Ny(), ustar.Nz(), 1, 
	      ustar.Lx(), ustar.Lz(), ustar.a(), ustar.b());
  FlowField ueig(eig); 
  FlowField udns(eig); 
  FlowField utmp(eig); 
  Real scale = eps/L2Norm(ueig);
  udns *= scale;
  udns += ustar;
  utmp = udns;

  const Real dT = dt.dT();
  //const Real T = dt.T();
  const int N = dt.N();
  
  cout << "dt == " << dt << endl;
  
  //DNS dns(utmp, nu, dt, flags);
  //if (dt.variable()) {
  //dns.advance(utmp, p, 1);
  //dt.adjust(dns.CFL(), false);
  //dns.reset_dt(dt);
  //}
  
  DNS dns(udns, nu, dt, flags);
  
  Matrix results(N, 10);

  FlowField e0 = eig;
  e0 *= 1.0/L2Norm(e0);

  const int wt = 6;
  const int wr = 14;

  cout << setiosflags(ios::left)
       <<setw(wt)<< "t"
       <<setw(wr)<< "L2Norm(udns)"
       <<setw(wr)<< "L2Norm(ueig)"
       <<setw(wr)<< "rel err"
       <<setw(wr)<< "magn err"
       <<setw(wr)<< "phase err"
       <<setw(wr)<< "align err" 
       <<setw(wr)<< "ad0" 
       <<setw(wr)<< "ae0" 
       << endl;

  for (int n=0; n<N; ++n) {

    Real t = n*dT;

    udns -= ustar;
    ueig  = eig;
    ueig *= scale*exp(Re(lambda)*t);

    Real dnsnorm = L2Norm(udns);
    Real eignorm = L2Norm(ueig);
    Real relaerr = L2Dist(udns, ueig)/eignorm;
    Real magnerr = abs(dnsnorm - eignorm)/eignorm;
    Real phaserr = 1.0 - L2IP(udns, ueig)/(dnsnorm*eignorm);
    Real ad0 = L2IP(udns, e0);
    Real ae0 = L2IP(ueig, e0);

    utmp  = e0;
    utmp *= ad0;
    Real alignerr = L2Dist(udns, utmp)/dnsnorm;
    
    cout << setiosflags(ios::left);
    cout <<setw(wt)<< t 
	 <<setw(wr)<< dnsnorm 
	 <<setw(wr)<< eignorm 
	 <<setw(wr)<< relaerr 
	 <<setw(wr)<< magnerr 
	 <<setw(wr)<< phaserr 
	 <<setw(wr)<< alignerr
	 <<setw(wr)<< ad0
	 <<setw(wr)<< ae0
	 << endl;
   
    results(n, 0) = dnsnorm;
    results(n, 1) = eignorm;
    results(n, 2) = relaerr;
    results(n, 3) = magnerr;
    results(n, 4) = phaserr;
    results(n, 5) = alignerr;
    results(n, 6) = ad0;
    results(n, 7) = t; 
    results(n, 8) = ae0;
    results(n, 9) = t;

    udns += ustar;
    dns.advance(udns, p, dt.n());
  }
  cout << "CFL == " << dns.CFL() << endl;
  return results;
}

Matrix verify_cmplx(const FlowField& ustar, Complex lambda,
		    const FlowField& eigA, const FlowField& eigB, 
		    Real eps, Real nu, TimeStep& dt, const DNSFlags& flags) {
  
  FlowField utmp;
  FlowField utmp0;
  FlowField utmp1;

  FlowField p(ustar.Nx(), ustar.Ny(), ustar.Nz(), 1, 
	      ustar.Lx(), ustar.Lz(), ustar.a(), ustar.b());
  FlowField ueigA(eigA); 
  FlowField ueigB(eigB); 

  const Real scale = eps; // /sqrt(L2Norm2(eigA) + L2Norm2(eigB));
  ueigA *= scale;
  ueigB *= scale;
  cout << "scale  == " <<  scale << endl;
  cout << "L2Norm(ueigA) == " << L2Norm(ueigA) << endl;
  cout << "L2Norm(ueigB) == " << L2Norm(ueigB) << endl;
  cout << "L2Norm(eigA)  == " << L2Norm(eigA) << endl;
  cout << "L2Norm(eigB)  == " << L2Norm(eigB) << endl;
  FlowField udns(ueigA); // integrated velocity field
  udns += ustar;
  utmp = udns;
  FlowField ueig;

  //const Real T = dt.T();
  const Real dT = dt.dT();
  const int  N  = dt.N();
  
  //DNS dns(utmp, nu, dt, flags);
  //if (dt.variable()) {
  //dns.advance(utmp, p, 1);
  //dt.adjust(dns.CFL(), false);
  //dns.reset_dt(dt);
  //}
  DNS dns(udns, nu, dt, flags);
  
  Matrix results(N, 10);
  array<FlowField> e(2);
  e[0]  = eigA;
  e[1]  = eigB;
  e[0] *= 1.0/L2Norm(e[0]);
  e[1] *= 1.0/L2Norm(e[1]);
  orthonormalize(e);

  const int wt = 6;
  const int wr = 14;

  cout << setiosflags(ios::left);
  cout <<setw(wt)<< "t"
       <<setw(wr)<< "L2Norm(udns)"
       <<setw(wr)<< "L2Norm(ueig)"
       <<setw(wr)<< "relative err"
       <<setw(wr)<< "phase err"
       <<setw(wr)<< "magn err"
       <<setw(wr)<< "align err"
       <<setw(wr)<< "ad0"
       <<setw(wr)<< "ad1"
       <<setw(wr)<< "ae0"
       <<setw(wr)<< "ae1"
       << endl;

  for (int n=0; n<N; ++n) {
    
    Real t = n*dT;
    
    udns -= ustar;

    ueigA  = eigA;
    ueigA *= scale*cos(Im(lambda)*t)*exp(Re(lambda)*t);

    ueigB  = eigB;
    ueigB *= scale*sin(Im(lambda)*t)*exp(Re(lambda)*t);

    ueig  = ueigA;
    ueig -= ueigB;

    Real ad0 = L2IP(udns, e[0]);
    Real ad1 = L2IP(udns, e[1]);
    Real ae0 = L2IP(ueig, e[0]);
    Real ae1 = L2IP(ueig, e[1]);

    utmp0 = e[0];
    utmp1 = e[1];
    utmp0 *= ad0;
    utmp1 *= ad1;
    utmp  = utmp0;
    utmp += utmp1;

    Real dnsnorm = L2Norm(udns);
    Real eignorm = L2Norm(ueig);
    Real relaerr = L2Dist(udns, ueig)/eignorm;
    Real magnerr = abs(dnsnorm - eignorm)/eignorm;
    Real phaserr = 1.0-L2IP(udns, ueig)/(dnsnorm*eignorm);
    Real aligerr = L2Dist(udns, utmp)/dnsnorm;
    
    cout << setiosflags(ios::left);
    cout <<setw(wt) << t 
	 <<setw(wr) << dnsnorm 
	 <<setw(wr) << eignorm 
	 <<setw(wr) << relaerr
	 <<setw(wr) << phaserr
	 <<setw(wr) << magnerr
	 <<setw(wr) << aligerr
	 <<setw(wr) << ad0
	 <<setw(wr) << ad1
	 <<setw(wr) << ae0
	 <<setw(wr) << ae1
	 << endl;
   
    results(n, 0) = dnsnorm;
    results(n, 1) = eignorm;
    results(n, 2) = relaerr;
    results(n, 3) = magnerr;
    results(n, 4) = phaserr;
    results(n, 5) = aligerr;
    results(n, 6) = ad0;
    results(n, 7) = ad1;
    results(n, 8) = ae0;
    results(n, 9) = ae1;

    udns += ustar;
    dns.advance(udns, p, dt.n());
  }
  cout << "CFL == " << dns.CFL() << endl;
  return results;
}

    
