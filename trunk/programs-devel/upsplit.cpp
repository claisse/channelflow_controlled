// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/poissonsolver.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("convert 3d velocity field u and Reynolds number to 4d (u,p) field");

  ArgList args(argc, argv, purpose);

  const string upname = args.getstr (3, "<flowfield>", "input  4d velocity,pressure field");
  const string uname  = args.getstr (2, "<flowfield>", "output 3d velocity field");
  const string pname  = args.getstr (1, "<flowfield>", "output 1d pressure field");

  args.check();
  args.save("./");

  FlowField up(upname);

  array<int> i(3); 
  i[0] = 0; i[1] = 1; i[2] = 2;
  FlowField u = up[i];
  FlowField p = up[3];
  
  u.save(uname);
  p.save(pname);
}

