#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"
#include "channelflow/periodicfunc.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("convolute a velocity field with a windowing function in z\n"
		 " w(z') = 1                   for 0 < |z'| < a\n"
		 "       = cos(pi/2 (z'-a)/b)  for a < |z'| < a+b\n"
		 "       = 0                   for a+b < |z'|\n"
		 " where z'= z-Lz/2");


  ArgList args(argc, argv, purpose);

  const Real a   = args.getreal("-a", "--a", 0, "halfwidth of w(z') = 1 weighting section");
  const Real b   = args.getreal("-b", "--b", 0, "width of w(z') transition section");
  const bool fixdiv   = args.getbool("-dv", "--divergence", true, "fix divergence and Dirichlet BCs");
  const bool padded   = args.getbool("-p",  "--padded", true, "zero padded fourier modes for dealiasing");
  const string iname = args.getstr (2, "<field>", "input field");
  const string oname = args.getstr (1, "<field>", "output field");
  args.check();
  args.save("./");

  FlowField u(iname);

  int Nx = u.Nx();
  int Ny = u.Ny();
  int Nz = u.Nz();
  int Nd = u.Nd();
  //Real Lx = u.Lx();
  Real Lz = u.Lz();
  //Real a  = u.a();
  //Real b  = u.b();

  u.makePhysical();

  Vector z = periodicpoints(Nz, Lz);
  Real Lz2 = Lz/2;
  Real pi2b = 0.5*pi/b;

  for (int i=0; i<Nd; ++i)
    for (int ny=0; ny<Ny; ++ny)
      for (int nx=0; nx<Nx; ++nx)
	for (int nz=0; nz<Nz; ++nz) {
	  Real zn = z(nz);
	  Real zp = zn - Lz2;
	  Real azp = abs(zp);
	    
	  if (a < azp && azp < a+b) 
	    u(nx,ny,nz,i) *= cos(pi2b*(azp-a));
	  if (a+b < azp)
	    u(nx,ny,nz,i) = 0;
	}

  u.makeSpectral();

  cout << setprecision(16);
  cout << "L2Norm(u)  == " << L2Norm(u) << endl;
  cout << "bcNorm(u)  == " << bcNorm(u) << endl;
  if (Nd == 3) 
    cout << "divNorm(u) == " << divNorm(u) << endl;

  if (u.padded() || padded)
    u.zeroPaddedModes();

  if (fixdiv) {
    cout <<"fixing divergence..." << endl;
    Vector v;
    field2vector(u,v);
    vector2field(v,u);
    cout << "L2Norm(u)  == " << L2Norm(u) << endl;
    cout << "divNorm(u) == " << divNorm(u) << endl;
    cout << "bcNorm(u)  == " << bcNorm(u) << endl;
  }
  u.save(oname);
}
