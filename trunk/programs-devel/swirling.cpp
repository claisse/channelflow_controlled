// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/poissonsolver.h"
#include "channelflow/utilfuncs.h"
#include "matrixutils.h"

using namespace std;
using namespace channelflow;

FlowField swirling(FlowField& u, Real ymax, bool absval);
ColumnVector cross(const ColumnVector& u, const ColumnVector& v);

int main(int argc, char* argv[]) {

  string purpose("compute swirling strength of a velocity field");

  ArgList args(argc, argv, purpose);

  //const bool   half   = args.getflag("-half", "--half",   "set swirl to zero for y>0");
  const Real   ymax   = args.getreal("-ymax", "--ymax", 1.0, "show swirl for -1 <= y <= ymax");
  const bool  absval  = args.getflag("-abs", "--absolute",   "give swirl magnitude, no sign");
  const string Uname  = args.getstr (3, "<chebycoeff>", "base flow profile filename or zero|linear|parabolic");
  const string uname  = args.getstr (2, "<flowfield>", "input velocity field");
  const string sname  = args.getstr (1, "<flowfield>", "output swirling strength");

  args.check();

  FlowField u(uname);

  ChebyCoeff U;
  if (Uname == "parabolic") {
    U = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
    U[0] =  0.5;
    U[2] = -0.5;
  }
  else if (Uname == "linear") {
    U = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
    U[1] = 1.0;
  }
  else if (Uname == "zero") 
    U = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
  else
    U = ChebyCoeff(Uname);

  U.makeSpectral();
  u.makeSpectral();
  u += U;
  
  FlowField s = swirling(u, ymax, absval);
  s.zeroPaddedModes();
  s.save(sname);
}


FlowField swirling(FlowField& u, Real ymax, bool absval) {

  u.makeSpectral();  
  FlowField gradu = grad(u);
  gradu.makePhysical();  
  Matrix3d G;

  FlowField curlu = curl(u);
  curlu.makePhysical();

  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();

  FlowField swirl(Nx,Ny,Nz,1,u.Lx(),u.Lz(),u.a(),u.b(),Physical,Physical);
  Vector y = u.ygridpts();

  for (int ny=0; ny<Ny; ++ny) {
    
    if (y(ny) > ymax)
      continue;
    
    for (int nx=0; nx<Nx; ++nx)
      for (int nz=0; nz<Nz; ++nz) {
	
	for (int i=0; i<3; ++i)
	  for (int j=0; j<3; ++j)
	    G(i,j) = gradu(nx,ny,nz,i3j(i,j));
	
	EigenSolver<Matrix3d> eigG(G);
	ComplexColumnVector lambda = eigG.eigenvalues();
	//ComplexMatrix v = eigG.eigenvectors();

	Real swrl = 0;
	for (int i=0; i<3; ++i) { 
	  Real omega = imag(lambda(i));

	  if (omega > swrl) { 
	    int sgn=1;

	    /********************************
	    if (!absval) {
	      // compute sign of swirl w.r.t x axis
	      //ComplexColumnVector v = v.column(i);
	      //ColumnVector vr = real(v);
	      //ColumnVector vi = imag(v);
	      //ColumnVector vr_vi = cross(vr,vi);
	      //Real x_projection = -omega*vr_vi(0);
	      //if (x_projection < 0)
	      //sgn = -1;
	      Real vr1 = Re(v(1,i));
	      Real vr2 = Re(v(2,i));
	      Real vi1 = Im(v(1,i));
	      Real vi2 = Im(v(2,i));

	      Real x_projection = -omega*(vr1*vi2 - vr2*vi1);
	      if (x_projection < 0)
		sgn = -1;
	      
	    }
	    *********************************/
	    if (!absval) {
	      if (curlu(nx,ny,nz,0) < 0)
		sgn = -1;
	    }
	    
	    swirl(nx,ny,nz,0) = sgn*omega;
	  }
	}
      }
  }
  swirl.makeSpectral();
  swirl.zeroPaddedModes();
  return swirl;
}
  

  
ColumnVector cross(const ColumnVector& u, const ColumnVector& v) {
  ColumnVector w(3);

  w(0) = u(1)*v(2) - u(2)*v(1);
  w(1) = u(2)*v(0) - u(0)*v(2);
  w(2) = u(0)*v(1) - u(1)*v(0);

  return w;
}
