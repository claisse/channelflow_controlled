#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/quicksort.h"
#include "octutils.h"

using namespace std;
using namespace channelflow;

//ColumnVector f(const ColumnVector& x, int label);
//ColumnVector gmodel(const ColumnVector& x, int label);



ColumnVector gtoy(const ColumnVector& x, int label, bool cacheable);
ColumnVector gEQ4toEQ1(const ColumnVector& x, int label, bool cacheable);
ColumnVector gEQ4toEQ1model(const ColumnVector& x, int label, bool cacheable);

int main() {

  cout << setprecision(17);

  ColumnVector x0(2);
  x0(0) = 2; // T, integration time
  x0(1) = 0.001; // theta

  ColumnVector xscale(2);
  xscale(0) = 1;
  xscale(1) = 1e-2;

  HookstepParams params;
  params.xscale = xscale;
  //params.deltaMax = 10.0;
  params.Nnewton = 40;
  params.pausing = true;
  ColumnVector x = hookstepSearch(&gtoy, x0, params);

  cout << x << endl;
}

ColumnVector gtoy(const ColumnVector& x, int label, bool cacheable) {

  // Model approach to EQ1 based on local eigenvalues
  // y0(t) = a(0) exp(lambda0 t)
  // y1(t) = a(1) exp(lambda1 t)
  Real T   = x(0);
  Real a0  = x(1);
  Real a1  = 0.001;

  //Real lambda0 =  0.050120783987456764;;
  //Real lambda1 = -0.0020048004455956408;
  Real lambda0 =  0.05;
  Real lambda1 = -0.002;

  ofstream os;
  if (label != -1) {
    string filename("a" + i2s(label) + ".asc");
    os.open(filename.c_str());
    os << setprecision(17);
  }

  // =================================================================
  // Produce plotting data
  // want N*dt = T, dt approx 0.10
  // N = int(T/0.10);
  // dt = T/N;
  int   N = int(T/0.10);
  Real dt = T/N;
  Real t = 0;
  for (int n=0; n<=N; ++n) {
    t += dt;
    Real f0 = a0*exp(lambda0*t);
    Real f1 = a1*exp(lambda1*t);

    if (label != -1)
      os << f0 << ' ' << f1 << ' ' << sqrt(f0*f0 + f1*f1) << endl;
  }
  // =================================================================

  ColumnVector gx(2);
  gx(0) = a0*exp(lambda0*T);
  gx(1) = a1*exp(lambda1*T);
  return gx;
}





// g(x) = function to be minimized
ColumnVector gEQ4toEQ1model(const ColumnVector& x, int label, bool cacheable) {

  Real T   = x(0);
  Real a0  = x(1);
  Real eps = 0.001;

  FlowField eq1("eq1");
  FlowField ef0("ef0");
  FlowField ef1("ef1");

  FlowField e0("e0");
  FlowField e1("e1");

  // Set u = eq1 + x(0) ef0 + eps ef1
  FlowField u(eq1);
  FlowField tmp;
  tmp = ef1;
  tmp *= eps;
  u += tmp;
  tmp = ef0;
  tmp *= a0;
  u += tmp;

  DNSFlags flags;
  flags.baseflow     = PlaneCouette;
  flags.timestepping = SBDF3;
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = Rotational;
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;
  flags.verbosity    = Silent;
  Real dtarg  = 0.03125;
  Real dtmin  = 0.02;
  Real dtmax  = 0.04;
  Real CFLmin = 0.5;
  Real CFLmax = 0.7;
  Real dT     = 0.1;
  bool vardt  = false;
  Real Reynolds = 400.0;
  Real nu     = 1/Reynolds;


  TimeStep dt(dtarg, dtmin, dtmax, dT, CFLmin, CFLmax, vardt);
  dt.adjust_for_T(T, false); // adjust dt so it divides T-Tc
  cout << "dt    == " << dt << endl;
  cout << "dt.dT == " << dt.dT() << endl;
  cout << "dt.T  == " << dt.T() << endl;

  FlowField p;
  FlowField du;
  DNS dns(u, nu, dt, flags);

  ofstream os;
  if (label != -1) {
    string filename("a" + i2s(label) + ".asc");
    os.open(filename.c_str());
    os << setprecision(17);
    du = u;
    du -= eq1;
    os << L2IP(du, e0) << ' ' << L2IP(du, e1) << ' ' << L2Dist(du, eq1) << endl;
  }

  cout << "f : 0" << flush;
  for (int s=1; s<=dt.N(); ++s) {
    dns.advance(u, p, dt.n());

    Real t = s*dt.dT();
    if (s % 10 == 0)      cout << iround(t) << flush;
    else if (s % 2 == 0)  cout << '.' << flush;

    Real CFL = dns.CFL();
    if (CFL < dt.CFLmin())      cout << '<' << flush;
    else if (CFL > dt.CFLmax()) cout << '>' << flush;


    if (label != -1) {
      du = u;
      du -= eq1;
      os << L2IP(du, e0) << ' ' << L2IP(du, e1) << ' ' << L2Dist(du, eq1) << endl;
    }
  }
  cout << endl;

  u.binarySave("ufinal");

  du = u;
  du -= eq1;

  a0 = L2IP(du, e0);     // projection of (u(t) - eq0) onto eq0 eigfunc
  e0 *= a0;
  du -= e0;              // remove the eq0_e0 portion of du
  Real p0 = L2Norm(du);  // orthog distance of du from e0 line.

  ColumnVector gx(2);
  gx(0) = p0;
  gx(1) = a0;
  return gx;
}

ColumnVector gEQ4toEQ1(const ColumnVector& x, int label, bool cacheable) {

  Real T     = x(0);
  Real theta = x(1);
  Real eps   = 0.000317166;

  FlowField eq4("EQ4");
  FlowField eq4_e0("EQ4_e0");
  FlowField eq4_e1("EQ4_e1");

  FlowField u(eq4);
  eq4_e0 *= eps*sin(theta);
  eq4_e1 *= eps*cos(theta);
  u += eq4_e0;
  u += eq4_e1;

  DNSFlags flags;
  flags.baseflow     = PlaneCouette;
  flags.timestepping = SBDF3;
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = Rotational;
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;
  flags.verbosity    = Silent;
  Real dtarg  = 0.03125;
  Real dtmin  = 0.02;
  Real dtmax  = 0.04;
  Real CFLmin = 0.5;
  Real CFLmax = 0.7;
  Real dT     = 0.1;
  bool vardt  = false;
  Real Reynolds = 400.0;
  Real nu     = 1/Reynolds;

  TimeStep dt(dtarg, dtmin, dtmax, dT, CFLmin, CFLmax, vardt);
  dt.adjust_for_T(T, false); // adjust dt slightly so it divides T evenly
  cout << "f : 0" << flush;

  // Look in cache for previous integration of u.




  FlowField p;
  DNS dns(u, nu, dt, flags);
  for (int s=1; s<=dt.N(); ++s) {
    dns.advance(u, p, dt.n());

    Real t = s*dt.dT();
    if (s % 10 == 0)
      cout << iround(t) << flush;
    else if (s % 2 == 0)
      cout << '.' << flush;

    Real CFL = dns.CFL();
    if (CFL < dt.CFLmin())
      cout << '<' << flush;
    else if (CFL > dt.CFLmax())
      cout << '>' << flush;

  }
  cout << endl;

  u.binarySave("ufinal");

  FlowField eq1("EQ1");
  FlowField eq1_e0("EQ1_e0");

  FlowField du(u);
  du -= eq1;

  Real a0 = L2IP(du, eq1_e0); // projection of (u(t) - eq0) onto eq0 eigfunc
  eq1_e0 *= a0;
  du -= eq1_e0;               // remove the eq0_e0 portion of du
  Real p0 = L2Norm(du);       // orthog distance of du from e0 line.

  ColumnVector gx(2);
  gx(0) = a0;
  gx(1) = p0;
  return gx;
}
