#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("extract the kx,kz Fourier profile of a field and save to disk");
  ArgList args(argc, argv, purpose);


  const int kx  = args.getint("-kx", "--kx", 0, "extract (kx,kz) Fourier mode");
  const int kz  = args.getint("-kz", "--kz", 0, "extract (kx,kz) Fourier mode");
  const string Uname = args.getstr("-U", "--Ubase", "", "base flow profile filename or zero|linear|parabolica");
  const string outdir = args.getpath("-o", "--outdir", ".", "output directory");
  /***/ string label  = args.getstr ("-l", "--label", "",   "output file stub, defaults to input field stub");
  const string uname = args.getstr(1, "<field>", "input field");

  args.check();
  args.save("./");

  FlowField u(uname);

  ChebyCoeff U;
  if (Uname == "parabolic") {
    U = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
    U[0] =  0.5;
    U[2] = -0.5;
  }
  else if (Uname == "linear") {
    U = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
    U[1] = 1.0;
  }
  else if (Uname == "zero" || Uname.length() == 0) 
    U = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
  else
    U = ChebyCoeff(Uname);

  U.makeSpectral();
  u.makeSpectral();
  u += U;

  u.makeState(Spectral, Physical);

  if (label.length() == 0) {
    label = stub(uname, ".ff");
    label = stub(label, ".h5");
  }

  const int mx = u.mx(kx);
  const int mz = u.mz(kz);

  
  BasisFunc profile = u.profile(mx,mz);
  profile.save(outdir + label + "_kx" + i2s(kx) + "_kz" + i2s(kz));
  
  Vector y = chebypoints(u.Ny(), u.a(), u.b());
  y.save(outdir + "y");

}
