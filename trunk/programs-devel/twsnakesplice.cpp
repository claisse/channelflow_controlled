#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <sys/stat.h>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("try to splice in an additional wavelength of interior structure in a localized traveling wave");

  ArgList args(argc, argv, purpose);

  const Real ax       = args.getreal("-ax",  "--ax", 0.0,  "phase shift in x at z splice points (vary this to minimize divergence)");
  const bool plot     = args.getflag("-p",  "--plot",  "plot f(z) = <u>_{xy}(z), filename f.asc");
  const int digits    = args.getint("-dg",  "--digits", 8, "number of digits in output");
  const string uname  = args.getstr (2, "<flowfield>",      "input field");
  const string oname  = args.getstr (1, "<flowfield>",      "output field");

  args.check();
  args.save();

  cout << setprecision(digits);

  FlowField u(uname);
  u.makeSpectral();

  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();
  const int Nz2 = Nz/2;
  const int Nd  = u.Nd();

  const Real Lz  = u.Lz();
  const Real Lz2 = Lz/2;

  // Compute the f(z) = xy-average of streamwise velocity u
  PeriodicFunc f(Nz, Lz, Spectral);
  
  for (int mz=0; mz<u.Mz(); ++mz)
    f.cmplx(mz) = u.cmplx(0,0,mz,0);
  
  //PeriodicFunc dfdz = diff(f);

  if (plot) {
    Vector z = periodicpoints(Nz, Lz);
    z.save("z");
    f.save("f");
    //dfdz.save("f");
  }

  // Find zeros of f(z) on either side of z=Lz/2
  cout << "Find zero to left of center" << endl;
  Real leftzero = Lz2;
  Real rghtzero = Lz2;
  //dfdz.makePhysical();
  f.makePhysical();
  for (int nz = Nz2-1; nz>0; --nz) 
    if (f(nz)*f(nz-1) <= 0) { // sign change
      leftzero = u.z(nz);          // set initial guess to nearby value
      break;
    }
  for (int nz = Nz2+1; nz<Nz-1; ++nz) 
    if (f(nz)*f(nz+1) < 0) { // sign change
      rghtzero = u.z(nz);              // set initial guess to nearby value
      break;
    }
  f.makeSpectral();
  //dfdz.makeSpectral();
  cout << "leftzero guess == " << leftzero << endl;
  leftzero = newtonSearch(f, leftzero);
  cout << "leftzero exact == " << leftzero << endl;

  cout << "rghtzero guess == " << rghtzero << endl;
  rghtzero = newtonSearch(f, rghtzero);    
  cout << "rghtzero exact == " << rghtzero << endl;

  Real lz = rghtzero-leftzero; // half wavelength of interior structure
  cout << "estimate of wavelength from zeros == " << rghtzero-leftzero << endl;

  // Shift
  FieldSymmetry  leftshift(1,1,1,ax,  lz/(Lz));
  FieldSymmetry  rghtshift(1,1,1,-ax, -lz/(Lz));
  FieldSymmetry  flop(-1,-1,1);
    
  FlowField uleft  =  (flop*leftshift)(u);
  FlowField urght  =  (flop*rghtshift)(u);

  //uleft.save("uleft");
  //uright.save("uright");
  u.makePhysical();
  uleft.makePhysical();
  urght.makePhysical();
  //uright.makePhysical();

  cout << "finding splice points" << endl;

  // The splice points for the three fields are at z=Lz/2-lz and z=Lz/2+lz. Find the
  // gridpoints that these slicepoints fall between by marching through the z values 
  // z(nzleft-1)  <  Lz/2-lz <= z(nzleft)
  // z(nzright-1) <  Lz/2+lz <= z(nzright)
  int nzleft  = Nz2; 
  int nzrght = Nz2;
  
  // find the first nz-1 below Nz/2 for which z(nz-1) < Lz-lz
  for (int nz=Nz2; nz>=1; --nz) {
    if (u.z(nz-1) < Lz2 - lz/2) {
      nzleft = nz; 
      break;
    }
  }

  // find the first nz above Nz/2 for which Lz+lz <= z(nz)
  for (int nz=Nz2; nz<Nz; ++nz) {
    if (Lz2 + lz/2 <= u.z(nz)) {
      nzrght = nz; 
      break;
    }
  }

  cout << "nzleft == " << nzleft << endl;
  cout << "nzrght == " << nzrght << endl;
  cout << "building spliced field" << endl;

  FlowField unew(u);
  //cout << "Nx == " << Nx << endl;
  //cout << "Ny == " << Ny << endl;
  //cout << "Nz == " << Nz << endl;
  //cout << "Nd == " << Nd << endl;

  cout << "splicing in data" << endl;
  //       0 <= nz <  nzleft  : uleft
  // nzleft  <= nz <  nzright : u
  // nzright <= nz < Nz       : uright
  for (int i=0; i<Nd; ++i) 
    for (int ny=0; ny<Ny; ++ny) 
      for (int nx=0; nx<Nx; ++nx) {
	for (int nz=0; nz<nzleft; ++nz)
	  unew(nx,ny,nz,i) = uleft(nx,ny,nz,i);
	for (int nz=nzleft; nz<nzrght; ++nz)	       
	  unew(nx,ny,nz,i) = u(nx,ny,nz,i);
	for (int nz=nzrght; nz<Nz; ++nz)	       
	  unew(nx,ny,nz,i) = urght(nx,ny,nz,i);
      }

  unew.makeSpectral();

  if (u.padded())
    unew.zeroPaddedModes();

  cout << "L2Norm(u)  == " << L2Norm(unew) << endl;
  cout << "divNorm(u) == " << divNorm(unew) << endl;
  cout << "bcNorm(u)  == " << bcNorm(unew) << endl << endl;

  cout <<"fixing divergence..." << endl;
  Vector v;
  field2vector(unew,v);
  vector2field(v,unew);

  cout << "L2Norm(unew)  == " << L2Norm(unew) << endl;
  cout << "divNorm(unew) == " << divNorm(unew) << endl;
  cout << "bcNorm(unew)  == " << bcNorm(unew) << endl;


  //FieldSymmetry recenter(1,1,1,0, -lz/(2*Lz));
  //unew *= recenter;

  unew.save(oname);
}

