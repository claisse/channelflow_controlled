#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

Real project(const FlowField& u, const FieldSymmetry& s, int sign);

int main(int argc, char* argv[]) {
  string purpose("force a set of fields to be s1,s2,s3 symmetric or antisymmetric by projection\n"
		 "\tusage: forcesymmetry u0 u1 u2 ...");

  ArgList args(argc, argv, purpose);

  const Real eps    = args.getreal("-e", "--eps", 1e-02, "project if fraction energy (a)symmetry is < eps");
  const string odir = args.getpath("-o", "--outdir", "", "output directory");

  FieldSymmetry s[2];
  s[0] = FieldSymmetry( 1, 1,-1, 0.5, 0);
  s[1] = FieldSymmetry(-1,-1, 1, 0.5, 0.5);

  FlowField su;
  FlowField uanti;
  FlowField usymm;

  const int Nf = args.remaining();

  for (int n=1; n<=Nf; ++n) {
    string uname = args.getstr(n, "<field>", "field to symmetrize");
    if (uname == argv[0])
      break;
    cout << "symmetrizing " << uname << ": " << flush;
    FlowField u(uname);
    Real unorm2 = L2Norm2(u);
    if (unorm2 < 1e-12)
      unorm2 = 1e-12;

    for (int n=0; n<2; ++n) {
      su = s[n](u);
      uanti = u;
      uanti -= su;
      uanti *= 0.5;
      usymm = u;
      usymm += su;
      usymm *= 0.5;
      if (L2Norm2(uanti)/unorm2 < eps) {
	u = usymm;
	cout << "S ";
      }
      else if (L2Norm2(usymm)/unorm2 < eps) {
	u = uanti;
	cout << "A ";
      }
      else
	cout << "- ";
      u.binarySave(odir + uname);
    }
    cout << endl;
  }
}
