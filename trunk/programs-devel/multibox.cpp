#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("embed a field periodic in Lx, Lz in a box of size Mx*Lx, Mz*Lz");
  ArgList args(argc, argv, purpose);


  const int Mx  = args.getint("-Mx", "--Mx", 1, "Mx copies in x");
  const int Mz  = args.getint("-Mz", "--Mz", 1, "Mz copies in z");
  const Real Lx  = args.getint("-Lx", "--Lx", 0, "resize Lx");
  const Real Lz  = args.getint("-Lz", "--Lz", 0, "resize Lz");
  const string inname = args.getstr (2, "<field>", "input field");
  const string otname = args.getstr (1, "<field>", "output field");
  args.check();
  args.save("./");

  if (Mx < 1 || Mz < 1) {
    cerr << "error : can't have negative or zero box lengths" << endl;
    exit(1);
  }

  FlowField u(inname);
  u.makeSpectral();

  int Ny = u.Ny();
  int Nd = u.Nd();

  FlowField u2(Mx*u.Nx(), Ny, Mz*u.Nz(), Nd, Mx*u.Lx(),Mz*u.Lz(), u.a(),u.b());

  for (int i=0; i<Nd; ++i)
    for (int ky=0; ky<Ny; ++ky)
      for (int kx = u.kxmin(); kx<=u.kxmax(); ++kx) {
	int mx   = u.mx(kx);
	int mx2  = u2.mx(Mx*kx);

	for (int kz = 0; kz<=u.kzmax(); ++kz) {
	  int mz   = u.mz(kz);
	  int mz2  = u2.mz(Mz*kz);

	  u2.cmplx(mx2,ky,mz2,i) = u.cmplx(mx,ky,mz,i);
	}
      }

  cout << setprecision(16);
  cout << "L2Norm(u1)  == " << L2Norm(u) << endl;
  cout << "L2Norm(u2)  == " << L2Norm(u2) << endl;
  cout << "bcNorm(u1)  == " << bcNorm(u) << endl;
  cout << "bcNorm(u2)  == " << bcNorm(u2) << endl;
  if (Nd == 3) {
    cout << "divNorm(u1) == " <<  divNorm(u) << endl;
    cout << "divNorm(u2) == " << divNorm(u2) << endl;
  }
  if (u.padded())
    u2.zeroPaddedModes();

  if (Lx != 0.0 && Lz != 0.0)
    u2.rescale(Lx,Lz);
  else if (Lx != 0.0 && Lz == 0.0)
    u2.rescale(Lx, u2.Lz());
  else if (Lx == 0.0 && Lz != 0.0)
    u2.rescale(u2.Lx(), Lz);

  u2.save(otname);
}
