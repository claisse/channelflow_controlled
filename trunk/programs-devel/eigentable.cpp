#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <sys/stat.h>
#include "octutils.h"
#include <octave/CColVector.h>

#include "channelflow/flowfield.h"

using namespace std;
using namespace channelflow;

char symmetry_char(Real puf, Real eps);

enum Stability {UNSTABLE=1, MARGINAL=0, STABLE=-1, TRANSLATIONAL=2};
ostream& operator<<(ostream& os, Stability s);
bool fileexists(const string& filename);

int main(int argc, char* argv[]) {

  string purpose("read eigenvalues from disk and produce a table of eigenvalues and eigenfunction symmetries\n\n"
		 "Note: floating-pt measures of translation and s[n]-symmetry are as follows\n"
		 "For a symmetry s, a given velocity field can be decomposed into \n"
		 "symmetric and antisymmetric components u = u_s + u_a, where u_s = (u+s(u))/2\n"
		 "and u_a = (u-s(u))/2, and u_s and u_a will satisfy |u_s|^2 + |u_a|^2 = |u^2|, \n"
		 "where | | is the L2 norm. The floating pt number reported in the s[n] column\n"
		 "is |u_s|/|u|, which is 1 for a purely symmetric field and 0 for a purely \n"
		 "antisymmetric field. For the translation-symmetry column, the idea is \n"
		 "similar, but instead we take u_s to be the projection onto the 2d subspace \n"
		 "spanned by the translation symmetries du/dx and du/dz, so that 1 indicates \n"
		 "a purely translational eigenfunctional, and 0 an eigenfunction with no \n"
		 "component in the translational directions.\n"

  

		 );
  ArgList args(argc, argv, purpose);

  const bool verbose = !args.getflag("-q", "--quiet", "suppress output to screen");
  const int  Nsdepth  = args.getint ("-Ns",  "--Nstable",  5,  "number of stable eigenvals to list in table");
  const int  n0       = args.getint ("-n0", "--n0", 1, "index of first eigenvalue");
  const int  digits   = args.getint ("-d", "--digits", 8, "number of digits");
  const Real traneps  = args.getreal("-te", "--traneps", 5e-2, "acceptable rel error in translation symmetry");
  const Real symmeps  = args.getreal("-se", "--symmeps", 1e-2, "acceptable rel error in discrete symmetry");
  const Real margeps  = args.getreal("-me", "--marginaleps", 1e-5, "take |Re(lambda)| < margeps to be marginal");
  const bool showconj = args.getflag("-s", "--showconj", "list conjugates seperately");
  const bool orb      = args.getflag("-orb", "--orbit",  "eigenvalues are for orbit, not eqb");
  const string symmstr = args.getstr("-symms", "--symmetries",  "symmetry group of solution");
  const string idir   = args.getpath("-i", "--indir",  "./", "input directory");
  const string odir   = args.getpath("-o", "--outdir", "./", "output directory");
  const string uname  = args.getstr(1, "<flowfield>", "filename for EQB, TW, or PO solution");
  args.check();
  args.save();

  const int w = digits + 7;
  const bool flow = !orb;

  SymmetryList s;
  if (symmstr.length() > 0) 
    s = SymmetryList(symmstr);
  const int Nsymms = s.length();

  FlowField u(uname);

  // Make ON basis for tangent space of translation symms
  FlowField edudx = xdiff(u);
  FlowField edudz = zdiff(u);
  edudx *= 1.0/L2Norm(edudx);
  edudz *= 1.0/L2Norm(edudz);
  //cout << "L2IP(edudx, edudz) == " << L2IP(edudx, edudz) << endl;
  FlowField tmp = edudx;
  tmp *= L2IP(edudx, edudz);
  edudz -= tmp;
  edudz *= 1.0/L2Norm(edudz);
  //cout << "L2IP(edudx, edudz) == " << L2IP(edudx, edudz) << endl;
  //cout << "L2Norm(edudx) == " << L2Norm(edudx) << endl;
  //cout << "L2Norm(edudz) == " << L2Norm(edudz) << endl;

  string ofile = odir + "eigentable.asc";
  ofstream os(ofile.c_str());
  os << setprecision(digits);

  if (verbose) cout << "n = " << flush;
  os.setf(ios::left);

  if (flow) {
    os << setw(10) << "% n"
       << setw(w+1) << "Re(lambda)"
       << setw(w+1) << "Im(lambda)";
  }
  else {
    os << setw(10) << "% n"
       << setw(w+1) << "abs(Lambda)"
       << setw(w+1) << "arg(Lambda)/pi";
  }

  os << setw(14) << "translation";  
  for (int n=0; n<Nsymms; ++n) 
    os << setw(10) << ("s" + i2s(n) + "-symmetry");
  os << endl;

  
  ComplexColumnVector lambdas; // eigvals of flow 1/dt sigma f^dt

  if (flow) 
    load(lambdas, idir + "lambda");
  else
    load(lambdas, idir + "Lambda");

  const int Nlambda = lambdas.length();
  array<Stability> stability(Nlambda); // {1,0,-1,2} == unstable, marginal, stable, tangent

  int Nstable   = 0;
  int Nmarginal = 0;
  int Ntranslat = 0;
  int Nunstable = 0;
  bool all_translations_are_marginal = true;

  for (int n=0; n<Nlambda; ++n) {

    
    if (!fileexists(idir + "ef" + i2s(n+n0) + ".h5") &&
	!fileexists(idir + "ef" + i2s(n+n0) + ".ff")) {
      cout << "stopping because " << (idir + "ef" + i2s(n+n0)) << " is not available " << endl;
      break;
    }
	
    if (verbose) cout << n << ' ' << flush;
    os << setprecision(digits);
    os.unsetf(ios::scientific);


    Complex lambda = lambdas(n);
    Complex lambdaNext = lambdas(n);
    bool complex_pair = (conj(lambdaNext) == lambda && Im(lambda) != 0.0) ? true : false;
  
    if (!showconj && complex_pair) 
      os << n+n0 << ',' << n+n0+1 << "\t  ";
    else
      os << n+n0 << "\t  ";

    if (flow) {
      os << setw(w) << Re(lambda) << " ";
      if (showconj)
	os << setw(w) << Im(lambda) << " ";
      else
	os << setw(w) << abs(Im(lambda)) << " ";

      if (Re(lambda) > 0)
	stability[n] = UNSTABLE;
      else if (Re(lambda) < 0)
	stability[n] = STABLE;
      else
	stability[n] = MARGINAL;

    }
    else {
      if (showconj) {
	os << setw(w) << abs(lambda) << " ";
	os << setw(w) << arg(lambda)/pi << " ";
      }
      else {
	os << setw(w) << abs(lambda) << " ";
	os << setw(w) << abs(arg(lambda))/pi << " ";
      }

      if (abs(lambda) > 1)
	stability[n] = UNSTABLE;
      else if (abs(lambda) < 1)
	stability[n] = STABLE;
      else
	stability[n] = MARGINAL;
    }
    
    FlowField ef(idir + "ef"+i2s(n+n0));

    // Test to see if ef is in tangent space of span dudx, dudz
    //cout << endl;

    FlowField pef = ef;
    pef.setToZero();

    tmp = edudx;
    tmp *= L2IP(edudx, ef);
    pef += tmp;
    tmp = edudz;
    tmp *=  L2IP(edudz, ef);
    pef += tmp;

    FlowField oef = ef;
    oef -= pef;

    Real offtangency = L2Dist(pef, ef)/L2Norm(ef);
    Real tangency    = L2Dist(oef, ef)/L2Norm(ef);
    if (verbose) {
      cout << "\nt^2 + o^2 == " << square(tangency) + square(offtangency) << endl;
      cout << "t == " << tangency << endl;
      cout << "o == " << offtangency << endl;
    }

    bool tangent = abs(offtangency) < traneps ? true : false;

    // If eigenfunction is within du/dx, du/dz plane, revise its stability label
    if (tangent) {
      stability[n] = TRANSLATIONAL;
      
      // Check that the translational eigenfunctions have approx marginal eigenvalues
      if (flow) {
	if (abs(Re(lambda)) > margeps)
	  all_translations_are_marginal = false;
      }
      else {
	if (abs(1 - abs(lambda)) > margeps)
	  all_translations_are_marginal = false;
      }
    }

    os.unsetf(ios::fixed);
    os.setf(ios::scientific);
    os.precision(2);
    
    os << (tangent ? "T  " : "-  ");
    os << setw(11) << tangency;

    array<Real> puf(Nsymms);
    for (int i=0; i<Nsymms; ++i)
      puf[i] = sqrt(PuFraction(ef,s[i],1));
    
    for (int i=0; i<Nsymms; ++i) {
      os << symmetry_char(puf[i], symmeps);
      os << "  " << setw(7) << puf[i];
    }

    os << endl;

    // Flip various switches and increment counts in assessing set of eigvals
    int nlump = (!showconj && complex_pair && n+1 < Nlambda) ? 2 : 1;  // 2 if lumping complex conjs together

    switch(stability[n]) {
    case UNSTABLE: 
      Nunstable += nlump; break;
    case MARGINAL:
      Nmarginal += nlump; break;
    case TRANSLATIONAL:
      Ntranslat += nlump; break;
    case STABLE:
      Nstable += nlump; break;
    }

    // If lumping eigenvalue with current, skip it in the containing for loop.
    if (nlump ==2) {
      stability[n+1] = stability[n];
      ++n;           
    }

    // Stop if we have seen enough stable eigenvalues, or if we're out of recorded eigenfunctions
    if (Nstable >= Nsdepth)
      break;

  }

  const int Ntotal = Nunstable + Nmarginal + Ntranslat + Nstable;

  if (verbose) {
    cout << "\n eigenvalue stabilities (U,M,T,S == unstable, marginal-non-trans, translational, stable)" << endl;
    for (int n=0; n<Ntotal; ++n) 
      cout << stability[n];
    cout << endl;
    cout << Nunstable << " unstable eigenvalues" << endl;
    cout << Ntranslat << " translational eigenvalues" << endl;
  }

  save(Nunstable, odir + "Nunstable");
  
  if (Ntranslat != 2 || !all_translations_are_marginal || Nstable == 0) {
    ofstream wos((odir + "WARNING").c_str());
    
    if (Ntranslat != 2)
      wos << "Number of translational eigenfunctions != 2" << endl;
    if (!all_translations_are_marginal) 
      wos << "Some translational eigenfunctions have nonmarginal stability, outside tolerance " << margeps << endl;      
    if (Nstable == 0)
      wos << "Did not reach the stable eigenvalues, |Re(lambda)| < " << margeps << endl;            
  }
}


ostream& operator<<(ostream& os, Stability s) {
  switch (s) {
  case UNSTABLE: 
    os << 'U';
    break;
  case MARGINAL:
    os << 'M';
    break;
  case TRANSLATIONAL:
    os << 'T';
    break;
  case STABLE:
    os << 'S';
    break;
  }
  return os;
}

// Should be UUUUMTTSSSS
bool okordering(const array<Stability>& s) {
  //for (int n=1; n<s.length(); ++n) {
  //if (
  return false;
}
      

char symmetry_char(Real puf, Real eps) {
  char c = '-';
  if (abs(puf - 1) < eps)
    c = 'S';
  else if (abs(puf) < eps)
    c = 'A';
  return c;
}
bool fileexists(const string& filename) {
  struct stat st;
  return (stat(filename.c_str(), &st) == 0) ? true : false;
}
