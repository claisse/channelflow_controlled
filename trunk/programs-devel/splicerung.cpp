#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("splice together halves of two fields\n");

  ArgList args(argc, argv, purpose);
  const string odir = args.getpath("-o", "--outdir", "./", "output directory");
  const bool fixdiv   = args.getbool("-dv", "--divergence", true, "fix divergence and Dirichlet BCs");
  string solndir[2];
  solndir[0] = args.getpath(2, "<solndirL>", "directory containing uLEFT");
  solndir[1] = args.getpath(1, "<solndirR>", "directory containing uRIGHT");

  args.check();
  args.save("./");

  // should do some error checking here...

  FlowField spliced;

  array<FlowField> u(2); //solutions
  array<Real>      T(2); //period
  array<Real>      R(2); //Reynolds
  array<FieldSymmetry> s(2); //sigma
  
  for (int i=0; i<2; i++) {
    u[i] = FlowField(solndir[i] + "ubest");
    load(T[i], solndir[i] + "Tbest");
    load(R[i], solndir[i] + "Reynolds");
    s[i] = FieldSymmetry(solndir[i] + "sigmabest");
  }

  cout << "congruence == " << u[0].geomCongruent(u[1]) << endl;

  if (!u[0].geomCongruent(u[1])) 
    cferror("input fields are not geometrically congruent. exiting");


  int W = 10;
  cout << "Loaded data..." << endl;
  cout.setf(ios::left);
  cout << setw(W) << "Re" << setw(W) << "Lx" << setw(W) << "Lz" 
       << setw(W) << "L2Norm(u)" << setw(W) << "T"  << setw(W) << "sigma" << endl;
  for (int i=0; i<2; ++i) {
    cout.setf(ios::left);
    cout << setw(W) << R[i] << setw(W) << u[i].Lx() << setw(W) << u[i].Lz() 
	 << setw(W) << L2Norm(u[i]) << setw(W) << T[i] << setw(W) << s[i] << endl;
  }


  Real Tnew = (T[0]+T[1])/2;
  Real Rnew = (R[0]+R[1])/2;

  FieldSymmetry snew(s[0].sx(), s[0].sy(), s[0].sz(), (s[0].ax()+s[1].ax())/2, (s[0].az()+s[1].az())/2, s[0].s());
  FlowField unew = u[1];
  
  u[0].makePhysical();
  u[1].makePhysical();
  unew.makePhysical();
  
  for (int i=0; i<unew.Nd(); ++i)
    for (int nx=0; nx<unew.Nx(); ++nx)
      for (int ny=0; ny<unew.Ny(); ++ny)
        for (int nz=0; nz<(unew.Nz())/2; ++nz)
          unew(nx, ny, nz, i) = u[0](nx,ny,nz,i);

  u[0].makeSpectral();
  u[1].makeSpectral();
  unew.makeSpectral();
  
  cout << "L2Norm(unew)  == " << L2Norm(unew) << endl;
  cout << "divNorm(unew) == " << divNorm(unew) << endl;
  cout << "bcNorm(unew)  == " << bcNorm(unew) << endl;
    
   if (fixdiv) {
      cout <<"fixing divergence..." << endl;
      Vector v;
      FlowField foo(unew);
      field2vector(unew,v);
      vector2field(v,foo);
      unew = foo;
      cout << "L2Norm(unew)  == " << L2Norm(unew) << endl;
      cout << "divNorm(unew) == " << divNorm(unew) << endl;
      cout << "bcNorm(unew)  == " << bcNorm(unew) << endl;
    }
    
  mkdir(odir);
  save(Tnew, odir+"T");
  save(Rnew, odir+"Reynolds");
  snew.save(odir+"sigma");
  unew.save(odir+"uguess");
}
