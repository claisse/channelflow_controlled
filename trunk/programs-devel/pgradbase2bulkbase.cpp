#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"


using namespace std;
using namespace channelflow;


// utot == up + Up == ub + uB  =>  ub == up + Up - Ub
int main(int argc, char* argv[]) {


  string purpose("change a diff-from-laminar velocity field from const pressure gradient baseflow\n"
		 "to const bulk vel baseflow. We have utot == up + Up == ub + Ub, therefore\n"
		 "ub == up + Up - Ub, where Up(y) and Ub(y) are the laminar flow profiles for\n"
		 "the given const pressure gradient.");


  ArgList args(argc, argv, purpose);
  
  const Real Reynolds = args.getreal("-R", "--Reylnolds", 400,  "pseudo-Reynolds number");
  /***/ Real nu       = args.getreal("-nu", "--nu",          0, "kinematic viscosity (takes precedence over Reynolds, if nonzero)");
  const Real dPdx     = args.getreal("-dPdx", "--dPdx",    0.0, "imposed pressure gradient, determines Up");
  const Real Ubulk    = args.getreal("-Ubulk", "--Ubulk",  0.0, "imposed bulk velocity, determines Ub");

  const string upname  = args.getstr (2, "<flowfield>", "input  field up");
  const string ubname  = args.getstr (1, "<flowfield>", "output field ub == ub + (Up - Ub)");
  args.check(); 
  args.save("./");

  FlowField u(upname);
  u.makeSpectral();

  const Real a = u.a();
  const Real b = u.b();
  const Real ua = 0.0;
  const Real ub = 0.0;
  const int  Ny = u.Ny();
  
  if (nu == 0) 
    nu = 1/Reynolds;
  
  ChebyCoeff Up = laminarProfile(nu, PressureGradient, dPdx,  0.0, a, b, ua, ub, Ny);
  ChebyCoeff Ub = laminarProfile(nu, BulkVelocity,     0.0, Ubulk, a, b, ua, ub, Ny);

  u += Up;
  u -= Ub;
  
  u.save(ubname);
}
  
