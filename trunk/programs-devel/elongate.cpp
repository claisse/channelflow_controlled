#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("elongate the physical domain of a localized solution by padding new\n"
		 "boxes full of zeroes on the ends given of the field");
  ArgList args(argc, argv, purpose);

  const int Mx  = args.getint("-Mx", "--Mx", 1, "Mx/Rx copies in x");
  const int Mz  = args.getint("-Mz", "--Mz", 1, "Mz/Rz copies in z");
  const int Rx  = args.getint("-Rx", "--Rx", 1, "Mx/Rx copies in x");
  const int Rz  = args.getint("-Rz", "--Rz", 1, "Mz/Rz copies in z");
  const bool center   = args.getbool("-c", "--center", true, "shift to keep center of box same");
  const bool padded   = args.getbool("-p", "--padded", true, "set padding modes to zero");
  const bool fixdiv   = args.getbool("-dv", "--divergence", true, "fix divergence and Dirichlet BCs");
  const string inname = args.getstr (2, "<field>", "input field");
  const string otname = args.getstr (1, "<field>", "output field");
  args.check();
  args.save("./");

  if (Mx < 1 || Mz < 1) {
    cerr << "error : can't have negative or zero box lengths" << endl;
    exit(1);
  }

  FlowField u1(inname);

  int Nx = u1.Nx();
  int Ny = u1.Ny();
  int Nz = u1.Nz();
  int Nd = u1.Nd();
  Real Lx = u1.Lx();
  Real Lz = u1.Lz();
  Real a  = u1.a();
  Real b  = u1.b();

  cout << "Mx,Mz == " << Mx << ", " << Mz << endl;
  FlowField u2(Mx*(Nx/Rx), Ny, Mz*(Nz/Rz), Nd, Mx*(Lx/Rx), Mz*(Lz/Rz), a, b, Physical, Physical);

  if (center && (u1.Lz() > u2.Lz() || u1.Lx() > u2.Lx())) {
    // Old center is at L1/2; New center should be L2/2. 
    // So shift old data by (L1-L2)/2 before copying into new 
    // Normalized by L1, that is a = 1/2 (1 - L2/L1) = 1/2 (1 - M/R) = (R-M)/(2R) 
    Real ax = Real(Rx-Mx)/(2.0*Rx);
    Real az = Real(Rz-Mz)/(2.0*Rz);
    FieldSymmetry shift(1,1,1, ax, az);
    cout << "shift == " << shift << endl;
    u1 *= shift;
  }

  u1.makePhysical();
  u2.makePhysical();

  if (u2.Lx() > u1.Lx()) {
    cout << "copying x edge..." << endl;

    // First copy the x=0 edge of the input field into the whole of the new field
    for (int i=0; i<Nd; ++i)
      for (int ny=0; ny<Ny; ++ny) 
	for (int nx=0; nx<u2.Nx(); ++nx) {
	  for (int nz=0; nz<u1.Nz(); ++nz)
	    u2(nx,ny,nz,i) = u1(0,ny,nz,i);

	  for (int nz=u1.Nz(); nz<u2.Nz(); ++nz)
	    u2(nx,ny,nz,i) = u1(0,ny,u1.Nz()-1,i);
	}
  }

  if (u2.Lz() > u1.Lz()) {
    cout << "copying z edge..." << endl;
    // Copy the z=0 edge of the input field into the whole of the new field
    for (int i=0; i<Nd; ++i)
      for (int ny=0; ny<Ny; ++ny) 
	for (int nx=0; nx<u1.Nx(); ++nx)
	  for (int nz=0; nz<u2.Nz(); ++nz)
	    u2(nx,ny,nz,i) = u1(nx,ny,0,i);
  }

  // Fill the new box with a single copy of the old box
  int Nxm = lesser(u1.Nx(), u2.Nx());
  int Nzm = lesser(u1.Nz(), u2.Nz());

  cout << "copying center..." << endl;
  for (int i=0; i<Nd; ++i)
    for (int ny=0; ny<Ny; ++ny)
      for (int nx=0; nx<Nxm; ++nx)
	for (int nz=0; nz<Nzm; ++nz)
	  u2(nx,ny,nz,i) = u1(nx,ny,nz,i);

  u1.makeSpectral();
  u2.makeSpectral();


  if (center && (u2.Lz() > u1.Lz() || u2.Lx() > u1.Lx())) {
    // Old center is at L1/2; New center should be L2/2. 
    // So shift new data by (L1-L2)/2.
    // Normalized by L2, that is a = 1/2 (L1/L2 - 1) = 1/2 (R/M - 1) = (R-M)/(2M) 
    Real ax = Real(Rx-Mx)/(2.0*Mx);
    Real az = Real(Rz-Mz)/(2.0*Mz);
    FieldSymmetry shift(1,1,1, ax, az);
    cout << "shift == " << shift << endl;
    u2 *= shift;
  }

  cout << setprecision(16);
  cout << "L2Norm(u1)  == " << L2Norm(u1) << endl;
  cout << "L2Norm(u2)  == " << L2Norm(u2) << endl;
  cout << "bcNorm(u1)  == " << bcNorm(u1) << endl;
  cout << "bcNorm(u2)  == " << bcNorm(u2) << endl;
  if (Nd == 3) {
    cout << "divNorm(u1) == " <<  divNorm(u1) << endl;
    cout << "divNorm(u2) == " << divNorm(u2) << endl;
  }
  if (u1.padded() || padded)
    u2.zeroPaddedModes();

  /*********************
  if (Lx != 0.0 && Lz != 0.0)
    u2.rescale(Lx,Lz);
  else if (Lx != 0.0 && Lz == 0.0)
    u2.rescale(Lx, u2.Lz());
  else if (Lx == 0.0 && Lz != 0.0)
    u2.rescale(u2.Lx(), u.Lz());
  *******************/

   if (fixdiv) {
      cout <<"fixing divergence..." << endl;
      Vector v;
      FlowField foo(u2);
      field2vector(u2,v);
      vector2field(v,foo);
      u2 = foo;
      cout << "L2Norm(u2)  == " << L2Norm(u2) << endl;
      cout << "divNorm(u2) == " << divNorm(u2) << endl;
      cout << "bcNorm(u2)  == " << bcNorm(u2) << endl;
    }
   u2.save(otname);
}
