// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/poissonsolver.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("convert 3d velocity field u and Reynolds number to 4d (u,p) field");

  ArgList args(argc, argv, purpose);

  const string nonlstr= args.getstr ("-nl", "--nonlinearity", "conv", "method of calculating nonlinearity, "
				     "one of [rot conv div skew alt]");

  const Real Reynolds = args.getreal("-R", "--Reynolds", 400, "Reynolds number");
  const bool channel  = args.getflag("-c", "--channel", "channelflow instead of plane Couette");
  //const bool bulkvel  = args.getflag("-b", "--bulkvelocity","hold bulk velocity fixed, not pressure gradient");
  //const Real dPdx     = args.getreal("-P", "--dPdx",   0.0, "value for fixed pressure gradient");
  //const Real Ubulk    = args.getreal("-U", "--Ubulk",  0.0, "value for fixed bulk velocity");
  
  const string uname  = args.getstr (2, "<flowfield>", "input 3d velocity field (deviation from laminar)");
  const string upname = args.getstr (1, "<flowfield>", "output 4d velocity,pressure field (not including const pressure gradient)");

  args.check();
  args.save("./");
  Real nu = 1.0/Reynolds;

  FlowField u(uname);
  ChebyCoeff Ubase(u.Ny(), u.a(), u.b(), Spectral);
  if (channel) {
    Ubase[0] =  0.5;  // Ubase = 1 - y^2;
    Ubase[2] = -0.5;
  }
  else
    Ubase[1] = 1;     // Ubase = y;

  PressureSolver poisson(u, Ubase, nu, s2nonlmethod(nonlstr)); 
  FlowField p = poisson.solve(u);
  FlowField up(u.Nx(), u.Ny(), u.Nz(), 4, u.Lx(), u.Lz(), u.a(), u.b());

  u.makeSpectral();
  p.makeSpectral();
  
  for (int i=0; i<3; ++i) 
    for (int my=0; my<u.My(); ++my)
      for (int mx=0; mx<u.Mx(); ++mx)
	for (int mz=0; mz<u.Mz(); ++mz)
	  up.cmplx(mx,my,mz,i) = u.cmplx(mx,my,mz,i);
   
  for (int my=0; my<u.My(); ++my)
     for (int mx=0; mx<u.Mx(); ++mx)
       for (int mz=0; mz<u.Mz(); ++mz)
	 up.cmplx(mx,my,mz,3) = p.cmplx(mx,my,mz,0);
  
  up.save(upname);
}

