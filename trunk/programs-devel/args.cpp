#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

Real project(const FlowField& u, const FieldSymmetry& s, int sign);

int main(int argc, char* argv[]) {
  string purpose("force a set of fields to be s1,s2,s3 symmetric or antisymmetric by projection\n"
		 "\tusage: forcesymmetry u0 u1 u2 ...");

  ArgList args(argc, argv, purpose);

  cout << "pre  args.remaining() == " << args.remaining() << endl;
  const Real eps    = args.getreal("-e", "--eps", 1e-02, "project if L2Norm of (a)symmetry is < eps L2Norm(u)");
  const string odir = args.getpath("-o", "--outdir", "", "output directory");
  cout << "post  args.remaining() == " << args.remaining() << endl;

  // n0 is the argument index of the 0th (1st) field for basis construction
  cout << "args.remaining() == " << args.remaining() << endl;
  const int Nf = args.remaining();

  for (int n=1; n<=Nf; ++n) {
    cout << "n == " << n << flush;
    string s = args.getstr(n, "<field>", "field to symmetrize");
    cout << " " << s << endl;
  }
}
