#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

Real project(const FlowField& u, const FieldSymmetry& s, int sign);

int main(int argc, char* argv[]) {
  string purpose("translate a field to maximize its x,y,z -> -x,-y,-z symmetry");

  ArgList args(argc, argv, purpose);

  const Real axguess = args.getreal("-ax",  "--ax", 0.0, "guess for x shift");
  const Real azguess = args.getreal("-az",  "--az", 0.0, "guess for z shift");
  const bool force   = args.getflag("-f",  "--force", "force symmetries by projection after optimization");
  const int  Nsteps  = args.getint ("-N",  "--Nsteps", 10, "max # Newton steps");
  const Real eps     = args.getreal("-e",  "--eps", 1e-14, "stop Newton search when err<eps");
  const Real minimpr = args.getreal("-m",  "--minimprove", 0.9, "if improvement doesn't beat this, stop");
  const Real damp    = args.getreal("-d",  "--damp", 1.0, "if improvement doesn't beat this, stop");

  const bool verbose = args.getflag("-v",  "--verbose", "print results during Newton search");
  const string uname = args.getstr(2, "<infield>", "input field");
  const string oname = args.getstr(1, "<outfield>", "output field");

  args.check();
  args.save("./");

  FieldSymmetry s(-1,-1,-1,0,0);
  FieldSymmetry tau(1,1,1,axguess,azguess);

  FlowField u(uname);
  FlowField tu, stu, ux, uz, tux, stux, tuz, stuz;

  Real Lx = u.Lx();
  Real Lz = u.Lz();

  // Notes 2009-07-16
  // Find minimum of function
  //    f(tau) = 1/2 |tau u - s tau u|^2 where |u| = (u,u) = L2 inner product over domain of u
  // s is inversion in (x,y,z), tau is translation parameterized by 2 1d variables ax,az
  //
  // Equivalently, find zero of gradient of f
  //
  //    g = (g0) = (df/d(ax)) = (s tau u_x, tau u) - (s tau u, tau u_xx)
  //        (g1)   (df/d(az)) = (s tau u_z, tau u) - (s tau u, tau u_zz)
  //
  // Use Newton iteration
  //   tau' = tau + dtau
  //   Dg dtau = -g
  //
  //   Dg = (dg0/d(ax)  dg0/d(az))
  //        (dg1/d(ax)  dg1/d(az))
  //
  //   dtau = (dx)
  //          (dz)
  //
  // A little effort gives
  //   dg0/d(ax) = 2 (stux, tux) - 2 (stu, tuxx)
  //   dg0/d(az) = 2 (stux, tuz) - 2 (stu, tuxz) = dg1/d(ax)
  //   dg1/d(az) = 2 (stuz, tuz) - 2 (stu, tuzz)
  //
  // using shorthand stux = s tau du/dx, etc.

  tu = tau(u);
  stu = s(tu);

  Real prev_residual = L2Dist(tu,stu);

  cout << "L2Dist(tu,stu)  == " << L2Dist(tu,stu) << endl;
  //cout << "|u|^2 - (tu,stu) == " << L2Norm2(u) - L2IP(tu,stu) << endl;


  // The Newton-step loop. (dx,dz) is the translational step in x,z being solved for
  // to minimize the residual
  for (int n=0; n<Nsteps; ++n) {
    cout << "=========================================" << endl;
    cout << "Newton step " << n  << endl;

    ux    = xdiff(u);
    uz    = zdiff(u);
    tu    = tau(u);
    tux   = tau(ux);
    tuz   = tau(uz);
    stu   = s(tu);
    stux  = s(tux);
    stuz  = s(tuz);

    // Now need to solve a 2x2 linear algebra problem to find the Newton step
    // in the translation, (dx, dz). The 2x2 problem above is
    //    [a  b] (dx) = (d)
    //    [b  c] (dz) = (e)
    Real a = 4*L2IP(stux, tux);
    Real b = 4*L2IP(stux, tuz);
    Real c = 4*L2IP(stuz, tuz);

    Real d = -2*L2IP(stux, tu);
    Real e = -2*L2IP(stuz, tu);
    Real b2_ac = b*b-a*c;

    /***********************
    cout << "a == " << a << endl;
    cout << "b == " << b << endl;
    cout << "c == " << c << endl;
    cout << "d == " << d << endl;
    cout << "e == " << e << endl;
    cout << "b^2 - ac == " << b2_ac << endl;
    ****************************/

    if (sqrt(abs(b2_ac)) < 1e-13) {
      cerr << "Newton step equation is singular. Exiting" << endl;
      exit(1);
    }
    Real dx = (b*e-c*d)/b2_ac;
    Real dz = (b*d-a*e)/b2_ac;
    cout << "dx == " << dx << endl;
    cout << "dz == " << dz << endl;

    /*********************************************
    cout << "a dx + b dz == " << a*dx + b*dz << endl;
    cout << "          d == " << d << endl;
    cout << "b dx + c dz == " << b*dx + c*dz << endl;
    cout << "          e == " << e << endl;

    cout << "a dx + b dz - d == " << a*dx + b*dz -d << endl;
    cout << "b dx + c dz - e == " << b*dx + c*dz -e << endl;
    ********************************************/

    cout << "dax == " << dx/Lx << endl;
    cout << "daz == " << dz/Lz << endl;
    FieldSymmetry dtau(1,1,1,damp*dx/Lx,damp*dz/Lz);

    tau *= dtau;

    tu = tau(u);
    stu = s(tu);

    cout << "L2Dist(tu,stu)  == " << L2Dist(tu,stu) << endl;
    Real residual = L2Dist(tu,stu);

    if (L2Dist(tu,stu) < eps || residual > minimpr*prev_residual)
      break;

    prev_residual = residual;

  }

  u = tu;

  Real residual = L2Dist(u,s(u));
  save(residual, "symmerr");

  if (force) {
    cout << "\nForcing symmetry by projection." << endl;
    (u += s(u)) *= 0.5;
    cout << "L2Dist(u, u' = 1/2 (u + su)) == " << L2Dist(u,tu) << endl;
  }

  residual = L2Dist(u,s(u));
  cout << "L2Dist(u',su')  == " << residual << endl;

  u.save(oname);
}


  /*****************************************
    tau = FieldSymmetry(x[0]/u.Lx(), x[2]/u.Lz());

    tu  = tau(u);
    stu = s(tu);
    tux   = diff(tu, i, 1);
    stux  = diff(stu, i, 1);
    tuxx  = diff(tux, i, 1);
    stuxx = diff(stux, i, 1);

    // g(x) is df/dx where f == (L2Norm(stu - tu)) and t = tau(x/Lx,0)
    Real g    =  L2IP(stux, tu) - L2IP(stu, tux);
    Real dgdx = 2*L2IP(stux, tux) - L2IP(stuxx, tu) - L2IP(stu, tuxx);

      if (verbose) {
	Real f = L2Dist(stu, tu);
	cout << endl;
	cout << "Newton step n  == " << n << endl;
	cout << "tau.ax         == " << tau.ax() << endl;
	cout << "tau.az         == " << tau.az() << endl;
	cout << "zeroable g     == " << g << endl;
	cout << "L2Norm(stu-tu) == " << f << endl;
      }
      if (abs(g) < residual && verbose) {
	cout << "\noptimizePhase: Newton search converged." << endl;
	break;
      }

      Real dx = -g/dgdx;

      if (abs(damp*dx)/L > 0.5) {
	cout << "\nerror in optimizePhase(FlowField, FieldSymmetry) :" << endl;
	cout << "Newton search went beyond bounds. " << endl;
	cout << "     g == " << g << endl;
	cout << " dg/dx == " << dgdx << endl;
	cout << "    dx == " << damp*dx << endl;
	cout << "  L/2  == " << 0.5*L  << endl;
	cout << "Jostle by L/10 and try again." << endl;
	x[i] += L/10;
      }
      else
	x[i] += damp*dx;

    }

  FieldSymmetry sz( 1,  1, -1, 0, 0);
  FieldSymmetry sx(-1, -1,  1, 0, 0);
  FieldSymmetry sxz(-1,-1, -1, 0, 0);


  FieldSymmetry txbest;
  FieldSymmetry tzbest;

  Real unorm2 = L2Norm2(u);

  cout << endl;
  cout.setf(ios::left);
  cout << "Initial energy decomposition:" << endl;
  cout << "sxz symm: " << setw(12) << project(u, sxz,  1)/unorm2;
  cout << "    anti: " << setw(12) << project(u, sxz, -1)/unorm2 << endl;
  cout << endl;
  cout.unsetf(ios::left);

  cout << "Rough symmetrization in z..." << endl;
  Real asymmbest = project(u, sxz, -1)/unorm2;
  for (Real az=0.005; az<=1.0; az += 0.005) {
    FieldSymmetry tz(1,1,1,0.0,az);
    FlowField tzu = tz(u);
    Real asymm = project(tzu, sxz, -1)/unorm2;
    cout << az << "\t " << asymm;
    if (asymm < asymmbest) {
      tzbest = tz;
      asymmbest = asymm;
      cout << "\t best!" << endl;
    }
    else
      cout << endl;
  }

  u *= tzbest;

  cout << "Rough symmetrization in x..." << endl;
  asymmbest = project(u, sxz, -1)/unorm2;
  for (Real ax=0.1; ax<=1.0; ax += 0.1) {
    FieldSymmetry tx(1,1,1,ax,0.0);
    FlowField txu = tx(u);
    Real asymm = project(txu, sxz, -1)/unorm2;
    cout << ax << "\t " << asymm;
    if (asymm < asymmbest) {
      txbest = tx;
      asymmbest = asymm;
      cout << "\t best!" << endl;
    }
    else
      cout << endl;
  }

  u *= txbest;

  cout.setf(ios::left);
  cout << "After rough symmetrization:" << endl;
  cout << "sxz symm: " << setw(12) << project(u, sxz,  1)/unorm2;
  cout << "    anti: " << setw(12) << project(u, sxz, -1)/unorm2 << endl;
  cout << endl;
  cout.unsetf(ios::left);

  FieldSymmetry taubest = txbest*tzbest;
  cout << "taubest == " << taubest << endl;
  exit(1);

  FieldSymmetry tx;
  tx = optimizePhase(u, sxz, Nsteps, eps, 1.0, verbose);
  u = tx(u);

  FieldSymmetry tz;
  tz = optimizePhase(u, sz, Nsteps, eps, 1.0, verbose);
  u = tz(u);

  u.save(oname);

  cout << endl;



  cout.setf(ios::left);
  cout << "Optimized energy decomposition:" << endl;
  cout << "sx symm: " << setw(12) << project(u, sx, 1)/unorm2;
  cout << "   anti: " << setw(12) << project(u, sx,-1)/unorm2 << endl;
  cout << "sx symm: " << setw(12) << project(u, sx, 1)/unorm2;
  cout << "   anti: " << setw(12) << project(u, sx,-1)/unorm2 << endl;
  cout << endl;
  cout.unsetf(ios::left);
  cout << "Post-translation sx     symm err == " << L2Dist(u,sx(u)) << endl;

  if (force) {
    cout << "\nForcing sxz symmetry by projection." << endl;
    (u += sxz(u)) *= 0.5;
  }
  unorm2 = L2Norm2(u);
  cout << endl;
  cout << "Forced energy decomposition:" << endl;
  cout.setf(ios::left);
  cout << "sx symm: " << setw(12) << project(u, sx, 1)/unorm2;
  cout << "   anti: " << setw(12) << project(u, sx,-1)/unorm2 << endl;

  cout << "\nL2Dist(u,u0)      == " << L2Dist(u,u0) << endl;
  cout << setprecision(16);
  cout << "optimal translation == " << tx << endl;
  u.save(oname);
}

Real project(const FlowField& u, const FieldSymmetry& s, int sign) {
  FlowField Pu(u);
  if (sign<0)
    Pu -= s(u);
  else
    Pu += s(u);
  Pu *= 0.5;
  return L2Norm2(Pu);
}

// Return a translation that maximizes the s-symmetry of u.
// (i.e. return tau // that minimizes L2Norm(s(tau(u)) - tau(u))).
FieldSymmetry optimizePhase(const FlowField& u, int Nsteps, Real residual,
			    bool verbose, Real x0, Real z0) {

  FieldSymmetry tau;
  FieldSymmetry sxz(-1,-1,-1,0,0);


  // Newton search for x-translation that maximizes shift-rotate symmetry
  if (verbose)
    cout << "\noptimizePhase: Newton search on translation" <<endl;

    int i   = (s.sx() == -1) ? 0 : 2; // are we optimizing on x or z?
    Real L  = (s.sx() == -1) ? u.Lx() : u.Lz();
    array<Real> x(3);
    x[0] = x0;
    x[2] = z0;

    FlowField tu, stu, tux, stux, tuxx, stuxx, tuz, stuz, ;
    FlowField tu, stu, tux, stux, tuxx, stuxx;

    for (int n=0; n<Nsteps; ++n) {
      tau = FieldSymmetry(x[0]/u.Lx(), x[2]/u.Lz());

      tu  = tau(u);
      stu = s(tu);
      tux   = diff(tu, i, 1);
      stux  = diff(stu, i, 1);
      tuxx  = diff(tux, i, 1);
      stuxx = diff(stux, i, 1);

      // g(x) is df/dx where f == (L2Norm(stu - tu)) and t = tau(x/Lx,0)
      Real g    =  L2IP(stux, tu) - L2IP(stu, tux);
      Real dgdx = 2*L2IP(stux, tux) - L2IP(stuxx, tu) - L2IP(stu, tuxx);

      if (verbose) {
	Real f = L2Dist(stu, tu);
	cout << endl;
	cout << "Newton step n  == " << n << endl;
	cout << "tau.ax         == " << tau.ax() << endl;
	cout << "tau.az         == " << tau.az() << endl;
	cout << "zeroable g     == " << g << endl;
	cout << "L2Norm(stu-tu) == " << f << endl;
      }
      if (abs(g) < residual && verbose) {
	cout << "\noptimizePhase: Newton search converged." << endl;
	break;
      }

      Real dx = -g/dgdx;

      if (abs(damp*dx)/L > 0.5) {
	cout << "\nerror in optimizePhase(FlowField, FieldSymmetry) :" << endl;
	cout << "Newton search went beyond bounds. " << endl;
	cout << "     g == " << g << endl;
	cout << " dg/dx == " << dgdx << endl;
	cout << "    dx == " << damp*dx << endl;
	cout << "  L/2  == " << 0.5*L  << endl;
	cout << "Jostle by L/10 and try again." << endl;
	x[i] += L/10;
      }
      else
	x[i] += damp*dx;

    }
  }

  if (verbose) {
    FlowField tu = tau(u);
    FlowField stu = s(tu);
    cout << "\noptimal translation == " << tau << endl;
    cout << "Post-translation symmetry error == " << L2Dist(stu, tu) << endl;
  }
  return tau;
}
  ********************************************************/
