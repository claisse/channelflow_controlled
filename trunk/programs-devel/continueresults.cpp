// Produce data files from results of continuesoln.cpp

// Re
// Lx, Ly, Lz
// D,E (normalized as required)
// internal wavelength
// residual
// guess error
// directory
// dPdx
// wavespeed
// T


#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"
#include "channelflow/diffops.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/poissonsolver.h"
#include "channelflow/matrixutils.h"

using namespace std;
using namespace channelflow;

bool fileexists(const string& filename);
void load(Real& x, const string& filebase, Real default_value);

Real zwavelength(FlowField& u);       // rtn[0] = dist btwn interior zeroes, rtn[1] = mean total shear over one internal wavelength

Real zpadding(FlowField& u, Real eps=1e-3);  // crude measure of fraction of z width which is laminar padding

Real zlocalization(FlowField& u);     // sqrt ratio of energy at z=0 and maximum energy (energy avg over x,y)

Real LrmsNorm(FlowField& u);              // max_y rms_z u(y,z) 



Real wallforce(FlowField& u, int i, int wall); // wall = +/-1 indicate top or bottom wall
Real circulation(FlowField& u, int i);
Real skewing(FlowField& u, Real Reynolds);
Real bending(FlowField& u, Real Reynolds);

ColumnVector pextrememum(FlowField& p, Real xguess, Real zguess);

int main(int argc, char* argv[]) {

  string purpose("produce data file of solution properties from stored results of continuesoln.cpp"
		 "WARNING: alpha version, -lz option makes assumptions about symmetry");
  ArgList args(argc, argv, purpose);

  const bool norestart = args.getflag("-nr", "--norestart", "don't look for restart-[012] dirs");
  const int digits = args.getint("-d",  "--digits", 6,   "number of digits in output");
  const bool outRe = args.getflag("-Re", "--Reynolds",  "output Reynolds number");
  const bool outnu = args.getflag("-nu", "--nu",        "output kiniematic viscosity");
  const bool outUb = args.getflag("-Ub", "--Ubulk",     "output bulk velocity (total)");
  const bool outdP = args.getflag("-dP", "--dPdx",      "output pressure gradient");
  const bool outUw = args.getflag("-Uw", "--Uwall",     "output wall speed");
  const bool outth = args.getflag("-th", "--theta",     "output theta, angle of wall speed to x axis");
  const bool outLx = args.getflag("-Lx", "--Lx",        "output streamwise box length Lx");
  const bool outLy = args.getflag("-Ly", "--Ly",        "output wall-normal distance");
  const bool outLz = args.getflag("-Lz", "--Lz",        "output spanwise box length Lz");
  const bool outI0 = args.getflag("-I0",  "--Ivolnorm", "output wallshear(u+U) normalized by Lx*Ly*Lz");
  const bool outI1 = args.getflag("-I1",  "--ILxnorm",  "output wallshear(u) normalized by Lx");
  const bool outI2 = args.getflag("-I2",  "--Iunnorm",  "output wallshear(u) unnormalized");
  const bool outI3 = args.getflag("-I3",  "--I0Lxnorm", "output wallshear(u+U) normalized by Lx");
  const bool bothw = args.getflag("-bw",  "--bothwalls","output wallshear at both walls");
  const bool outD0 = args.getflag("-D0",  "--Dvolnorm", "output dissipation(u+U) normalized by Lx*Ly*Lz");
  const bool outD1 = args.getflag("-D1",  "--DLxnorm",  "output dissipation(u) normalized by Lx");
  const bool outD2 = args.getflag("-D2",  "--Dunnorm",  "output dissipation(u) unnormalized");
  const bool outD3 = args.getflag("-D3",  "--D0Lxnorm", "output dissipation(u+U) normalized by Lx");
  const bool outE0 = args.getflag("-E0",  "--Evolnorm", "output energy(u+U) normalized by Lx*Ly*Lz");
  const bool outE1 = args.getflag("-E1",  "--ELxnorm",  "output energy(u) normalized by Lx");
  const bool outE2 = args.getflag("-E2",  "--Eunnorm",  "output energy(u) unnormalized");
  const bool outE3 = args.getflag("-E3",  "--E0Lxnorm", "output energy(u+U) normalized by Lx");
  const bool outlz = args.getflag("-lz", "--lz",        "output internal wavelength lz");  
  const bool outcx = args.getflag("-cx", "--cx",        "output x wavespeed");  
  const bool outcz = args.getflag("-cz", "--cz",        "output z wavespeed");  
  const bool outxp = args.getflag("-xp", "--xpumping",  "output x pumping = 1/Lx integral u dxdydz");
  const bool outzp = args.getflag("-zp", "--zpumping",  "output z pumping = 1/Lx integral w dxdydz");
  const bool outxc = args.getflag("-xc", "--xcirculation",  "output x circ = 1/Lx int_{xz} int_0^1 u(x,y,z)-u(u,-y,z) dy dx dz");
  const bool outzc = args.getflag("-zc", "--zcirculation",  "output z circ = 1/Lx int_{xz} int_0^1 w(x,y,z)-w(u,-y,z) dy dx dz");
  const bool outxf = args.getflag("-xf", "--xforce",    "output (+/-1)/Lx integral_0^Lz/2 integral_0^Lx du/dy(x,+/-1,z) dx dz");
  const bool outzf = args.getflag("-zf", "--zforce",    "output (+/-1)/Lx integral_0^Lz/2 integral_0^Lx dw/dy(x,+/-1,z) dx dz");
  const bool outsk = args.getflag("-sk",  "--skewing",  "output skewing of eqb soln");
  const bool outbd = args.getflag("-bd",  "--bending",  "output bending of tw soln");
  const bool outsc = args.getflag("-cm", "--compmagn",  "output magnitude of roll-streak components");
  const bool outT  = args.getflag("-T",  "--T",         "output period T");
  const bool outtr = args.getflag("-tr", "--truncation", "output x,y,z truncation");  
  const bool outev = args.getflag("-ev", "--eigenvals",  "output number of unstable eigenvalues ");
  const bool outev2 = args.getflag("-ev2", "--eigenvalss",  "output number of unstable eigenvalues, in arnoldi-sxyz and arnoldi-axyz ");
  const bool outrs = args.getflag("-res", "--residual",  "output residual of solution, L2Norm(sigma f^T(u) - u)");
  const bool outzl = args.getflag("-zl",  "--zlocal",    "output sqrt(min(e(z))/max(e(z)))");

  const string comment = args.getstr("-c", "--comment", "%", "comment marker");
  //const bool restart  = args.getflag("-r", "--restart", "data from restart directories, too");

  //const bool outNx = args.getflag("-Nx", "--Nx",       "print # x gridpoints in comment");
  //const bool outNy = args.getflag("-Ny", "--Ny",       "print # y gridpoints");
  //const bool outNz = args.getflag("-Nz", "--Nz",       "print # z gridpoints");
  //const bool outEx = args.getflag("-xt", "--xtrunc",   "print x truncation error");
  //const bool outEy = args.getflag("-yt", "--ytrunc",   "print y truncation error");
  //const bool outEz = args.getflag("-zt", "--ztrunc",   "print z truncation error");

  const int Nmax        = args.getint("-N", "--Nsearchdirs",  100,   "if nonzero, stop after looking for search-N instead of cueing of failures");
  const string stubdir  = args.getstr("-st", "--stub",   "search", "stub for directory name, search of search-0, search-1, ...");
  const string outfile  = args.getstr("-o", "--output",  "stdout", "output file");
  const bool singledir  = args.getflag("-sd", "singledir", "extract results from a single search directory");
  const string contdir  = args.getpath(1, "<contdir>",   "directory containing continuation results");

  args.check();

  // Load solution field from disk only when necessary, since it's expensive
  //bool needfield = (outLx || outLy || outLz || outD0 || outD1 || outD2 || outE0 || outE1 || outE2 || outlz || uavg || wavg);

  const int Nbuff = 1024;
  char buff[Nbuff];
  gethostname(buff, Nbuff);

  string hostname(buff);
  string workdir;
  char buf[Nbuff];
  getcwd(buf, Nbuff);
  workdir = string(buf);
  

  //cout << "hostname == " << hostname << endl;
  //cout << "contdir  == " << contdir  << endl;
  //cout << "workdir  == " << workdir  << endl;

  // Oh the shenanigans one has to go through to get iostreams to act like objects...
  ostream* osptr;
  ofstream ofs;
  if (outfile == "stdout" || outfile == "cout") 
    osptr = &cout;
  else {
    ofs.open(outfile.c_str());
    osptr = &ofs;
  }
  ostream& os = *osptr;

  FlowField u;    // a sample value of u from which to get Nx,Ny,Nz etc.
  if (singledir)
    u = FlowField(contdir + "ubest");
  else {
    // look for first occurrence of restart- or search- directories
    for (int ndir=-3; ndir<Nmax; ++ndir) {
       string subdir = (ndir < 0) ? ("restart-" + i2s(abs(ndir+1)) + "/") : ("search-" + i2s(ndir) + "/");
       string solndir = contdir + subdir;
       if (fileexists(solndir)) {
	 u = FlowField(solndir + "ubest");
	 break;
       }
    }
  }
	   

  u.makeSpectral();

  os << comment << " " << hostname << ":" << workdir << "/" << contdir << endl;
  os << comment << setprecision(4)
     << " box == " << u.Lx()/pi << "pi x " << u.Lz()/pi << "pi, "
     << " grid  == " << u.Nx() << " x " << u.Ny() << " x " << u.Nz() << endl;
  os << setprecision(digits);  

  //array<Real> trunc = truncerr(u);  
  //os << setprecision(0) << scientific;
  //os << comment << " trunc == " << trunc[0] << " x " << trunc[1] << " x " << trunc[2] << " at startup" << endl;

  const int cw = digits+10; 
  os << "%-";
  if (outRe) os << left<<setw(cw-2) << "pseudo-Re";
  if (outnu) os << left<<setw(cw) << "nu";
  if (outUw) os << left<<setw(cw) << "Uwall";
  if (outUb) os << left<<setw(cw) << "Ubulk";
  if (outdP) os << left<<setw(cw) << "dPdx";
  if (outth) os << left<<setw(cw) << "theta";
  if (outLx) os << left<<setw(cw) << "Lx";
  if (outLy) os << left<<setw(cw) << "Ly";
  if (outLz) os << left<<setw(cw) << "Lz";
  if (outI0) os << left<<setw(cw) << "shear(u+U)/V";
  if (outI1) os << left<<setw(cw) << "shear(u)/Lx";
  if (outI2) os << left<<setw(cw) << "shear(u)";
  if (outI3) os << left<<setw(cw) << "shear(u+U)/Lx";
  if (bothw) os << left<<setw(cw) << "lowerwall";
  if (bothw) os << left<<setw(cw) << "upperwall";
  if (outD0) os << left<<setw(cw) << "dissip(u+U)/V";
  if (outD1) os << left<<setw(cw) << "dissip(u)/Lx";
  if (outD2) os << left<<setw(cw) << "dissip(u)";
  if (outD3) os << left<<setw(cw) << "dissip(u+U)/Lx";
  if (outE0) os << left<<setw(cw) << "energy(u+U)/V";
  if (outE1) os << left<<setw(cw) << "energy(u)/Lx";
  if (outE2) os << left<<setw(cw) << "energy(u)";
  if (outE3) os << left<<setw(cw) << "energy(u+U)/Lx";
  if (outlz) os << left<<setw(cw) << "lz";
  if (outcx) os << left<<setw(cw) << "cx";
  if (outcz) os << left<<setw(cw) << "cz";
  if (outxp) os << left<<setw(cw) << "xpumping";
  if (outzp) os << left<<setw(cw) << "zpumping";
  if (outxc) os << left<<setw(cw) << "xcirculation";
  if (outzc) os << left<<setw(cw) << "zcirculation";
  if (outxf) os << left<<setw(cw) << "xforce-top";
  if (outxf) os << left<<setw(cw) << "xforce-bot";
  if (outsk) os << left<<setw(cw) << "skewing";
  if (outbd) os << left<<setw(cw) << "bending";
  if (outsc) os << left<<setw(cw) << "Linf(mean)";
  if (outsc) os << left<<setw(cw) << "Linf(streak)";
  if (outsc) os << left<<setw(cw) << "Linf(roll)";
  if (outsc) os << left<<setw(cw) << "Linf(wave)";
  if (outsc) os << left<<setw(cw) << "Linf(harm1)";
  if (outsc) os << left<<setw(cw) << "Linf(harm2)";
  if (outsc) os << left<<setw(cw) << "Linf(harm3)";
  //if (outsc) os << left<<setw(cw) << "Linf(harm4)";
  if (outT)  os << left<<setw(cw) << "T";
  if (outtr) os << "xtrunc   ytrunc   ztrunc   ";
  if (outev) os << left<<setw(10) << "Nunstab";
  if (outev2) os << left<<setw(10) << "Nsxyz";
  if (outev2) os << left<<setw(10) << "Naxyz";
  if (outev2) os << left<<setw(10) << "Nunstab";
  if (outrs) os << left<<setw(10) << "residual";
  if (outzl) os << left<<setw(6) << "zlocal";
  
  os << comment  <<  " directory" << endl;
  
  string space = "";
  
  const int n0 = norestart ? 0 : -3;
  for (int ndir = n0;  ; ++ndir) {
    
    // will produce sequence restart-2/, restart-1/, restart-0/, search-0/, search-1/, ...
    string subdir  = (ndir < 0) ? ("restart-" + i2s(abs(ndir+1)) + "/") : (stubdir + string("-") + i2s(ndir) + string("/"));
    string solndir = contdir + subdir;

    // hacky modification for processing a single solution directory
    if (singledir) {
      solndir  = contdir;
      subdir   = contdir;
    }
    //cout << "soldir == " << solndir << endl;
    
    
    // If the contdir/subdir exists, process it
    if (fileexists(solndir)) {
      
      // If D.asc was not written, then this search didn't finish. Break, next pass will not exist and exit
      if (ndir >= 0 && !fileexists(solndir + "D.asc")) {
	//cout << (solndir + "D.asc") << " doesn't exist, indicating end of continuation. Exiting" << endl;
	break;
      }
      
      //if (needfield)
      u = FlowField(solndir + "ubest");
      ChebyCoeff Ubase;
      if (fileexists(solndir + "Ubase.asc")) {
	Ubase = ChebyCoeff(solndir + "Ubase.asc");
	Ubase.makeSpectral();
      }
      else {
	Ubase = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
	Ubase[1] = 1;
      }
      ChebyCoeff Wbase;
      if (fileexists(solndir + "Wbase.asc")) {
	Wbase = ChebyCoeff(solndir + "Wbase.asc");
	Wbase.makeSpectral();
      }
      else {
	Wbase = ChebyCoeff(u.Ny(), u.a(), u.b(), Spectral);
	Wbase[1] = 1;
      }

      os << scientific;
      if (outRe || outnu) {
	Real Reynolds;
	load(Reynolds, solndir + "Reynolds");
	if (outRe)
	  os << fixed << left<<setw(cw) << Reynolds << scientific << space ;
	if (outnu) 
	  os << left<<setw(cw) << 1/Reynolds << space ;
      }
      if (outUw) {
	Real Uwall;
	if (fileexists(solndir + "Uwall.asc"))
	  load(Uwall, solndir + "Uwall");
	else 
	  Uwall = pythag(Ubase.eval_b() - Ubase.eval_a(), Wbase.eval_b() - Wbase.eval_a());
	os << left<<setw(cw) << Uwall << space ;
      }
      if (outUb) {
	Real Ubulk;
	load(Ubulk, solndir + "Ubulk", 0);
	if (fileexists(solndir + "Ubulk.asc"))
	  load(Ubulk, solndir + "Ubulk");
	else 
	  Ubulk = Ubase.mean() + Re(u.profile(0,0,0)).mean();
	os << left<<setw(cw) << Ubulk << space ;
      }
      if (outdP) {
	Real dPdx;
	load(dPdx, solndir + "dPdx");
	os << left<<setw(cw) << dPdx << space ;
      }
      if (outth) {
	Real theta;
	load(theta, solndir + "theta");
	os << left<<setw(cw) << theta << space ;
      }

      if (outLx) os << left<<setw(cw) << u.Lx() << space;
      if (outLy) os << left<<setw(cw) << u.Ly() << space;
      if (outLz) os << left<<setw(cw) << u.Lz() << space;

      if (outI0) {
	u += Ubase;
	os << left<<setw(cw) << wallshear(u) << space;
	if (bothw) {
	  os << left<<setw(cw) << wallshearLower(u) << space;
	  os << left<<setw(cw) << wallshearUpper(u) << space;
	}
	u -= Ubase;
      }

      if (outI1) {
	Real I = wallshear(u, false);
	//cout << endl;
	//cout << "I       == " << I << endl;
	//cout << "I/Lx    == " << I/u.Lx() << endl;
	//cout << "I/LxLz  == " << I/(u.Lx()*u.Lz()) << endl;
	//cout << "I/2LxLz == " << I/(2*u.Lx()*u.Lz()) << endl;
	os << left<<setw(cw) << I/u.Lx() << space;
	if (bothw) {
	  os << left<<setw(cw) << wallshearLower(u,false)/u.Lx() << space;
	  os << left<<setw(cw) << wallshearUpper(u,false)/u.Lx() << space;
	}
      }

      if (outI2) {
	Real I = wallshear(u, false);
	os << left<<setw(cw) << I << space;
	if (bothw) {
	  os << left<<setw(cw) << wallshearLower(u,false) << space;
	  os << left<<setw(cw) << wallshearUpper(u,false) << space;
	}
      }

      if (outI3) {
	u += Ubase;
	Real I = wallshear(u, false);
	os << left<<setw(cw) << I/u.Lx() << space;
	if (bothw) {
	  os << left<<setw(cw) << wallshearLower(u,false)/u.Lx() << space;
	  os << left<<setw(cw) << wallshearUpper(u,false)/u.Lx() << space;
	}
	u -= Ubase;
      }

      if (outD0) {
	u += Ubase;
	os << left<<setw(cw) << dissipation(u) << space;
	u -= Ubase;
      }

      if (outD1 || outD2) {
	Real D = dissipation(u, false);
	if (outD1) os << left<<setw(cw) << D/u.Lx() << space;
	if (outD2) os << left<<setw(cw) << D << space;
      }


      if (outD3) {
	u += Ubase;
	os << left<<setw(cw) << dissipation(u,false)/u.Lx() << space;
	u -= Ubase;
      }
      if (outE0) {
	u += Ubase;
	os << left<<setw(cw) << 0.5*L2Norm2(u) << space;
	u -= Ubase;
      }
	
      if (outE1 || outE2) {
	//Real E = 0.5*L2Norm2(u);
	//if (outE1) os << left<<setw(cw) << E*u.Ly()*u.Lz() << space;
	//if (outE2) os << left<<setw(cw) << E*u.Lx()*u.Ly()*u.Lz() << space;
	Real E = 0.5*L2Norm2(u, false);
	if (outE1) os << left<<setw(cw) << E/u.Lx() << space;
	if (outE2) os << left<<setw(cw) << E << space;
      }
      if (outE3) {
	u += Ubase;
	os << left<<setw(cw) <<  0.5*L2Norm2(u, false)/u.Lx() << space;
	u -= Ubase;
      }

      if (outlz) os << left<<setw(cw) << zwavelength(u) << space;

      if (outcx || outcz) {
	FieldSymmetry sigma(solndir + "sigmabest.asc");
	Real T;
	load(T, solndir + "Tbest.asc");
	if (outcx) os << left<<setw(cw) << sigma.ax()*u.Lx()/T << space;
	if (outcz) os << left<<setw(cw) << sigma.az()*u.Lz()/T << space;
      }
      
      if (outxp) os << left<<setw(cw) << u.Lz()*u.Ly()*Re(u.profile(0,0,0)).mean() << space;
      if (outzp) os << left<<setw(cw) << u.Lz()*u.Ly()*Re(u.profile(0,0,2)).mean() << space;

      if (outxc) os << left<<setw(cw) << circulation(u,0) << space;
      if (outzc) os << left<<setw(cw) << circulation(u,2) << space;

      if (outxf) os << left<<setw(cw) << wallforce(u,0,-1) << space;
      if (outxf) os << left<<setw(cw) << wallforce(u,0, 1) << space;
      if (outzf) os << left<<setw(cw) << wallforce(u,2,-1) << space;
      if (outzf) os << left<<setw(cw) << wallforce(u,2, 1) << space;

      if (outsk) {
	Real Reynolds;
	load(Reynolds, solndir + "Reynolds");
	os << left<<setw(cw) << skewing(u, Reynolds) << space;
      }

      if (outbd) {
	Real Reynolds;
	load(Reynolds, solndir + "Reynolds");
	os << left<<setw(cw) << bending(u, Reynolds) << space;
      }

      if (outsc) {

	// For definitions of these quantities, see Wang et al PRL 98, 204501 (2007)
	// In uk(y,z), k is the kth x Fourier mode. Computations of norms using 3D 
	// FlowFields instead of some 2D slice are a little wasteful, but hey.

	//const int Mx = u.Mx();
	const int My = u.My();
	const int Mz = u.Mz();
	FlowField uk(8, u.Ny(), u.Nz(), 3, u.Lx(), u.Lz(), u.a(), u.b());
	
	u.makeSpectral();
	//cout << endl;
	//cout << "Mx == " << Mx << endl;
	//cout << "My == " << My << endl;
	//cout << "Mz == " << Mz << endl;
	//cout << "L2Norm(u)  == " << L2Norm(u) << endl;

	// Compute magnitude of mean flow <u> = U(y)
	for (int my=0; my<My; ++my)
	  uk.cmplx(0,my,0,0) = u.cmplx(0,my,0,0);
	const Real meanmag = LinfNorm(uk);

	os << left<<setw(cw) << meanmag << space;	

	// Compute magnitude of streaks (u0, 0, 0)(y,z) - <u>
	uk.setToZero();
	for (int my=0; my<My; ++my)
	  for (int mz=1; mz<Mz; ++mz)
	    uk.cmplx(0,my,mz,0) = u.cmplx(0,my,mz,0);
	const Real streakmag = LinfNorm(uk);

	os << left<<setw(cw) << streakmag << space;	

	// Compute magnitude of rolls (0, v0, w0)(y,z)
	uk.setToZero();
	for (int i=1; i<3; ++i)
	  for (int my=0; my<My; ++my)
	    for (int mz=0; mz<Mz; ++mz) 
	      uk.cmplx(0,my,mz,i) = u.cmplx(0,my,mz,i);
	const Real rollmag = LinfNorm(uk);

	os << left<<setw(cw) << rollmag << space;	

	// Compute magnitude of fundamental wave (0, 0, w1)(y,z)
	uk.setToZero();
	for (int my=0; my<My; ++my)
	  for (int mz=0; mz<Mz; ++mz) 
	    uk.cmplx(1,my,mz,2) = u.cmplx(1,my,mz,2);
	const Real wavemag = LinfNorm(uk);

	os << left<<setw(cw) << wavemag << space;	

	// Compute magnitude of kth x-harmonic (uk, vk, wk)(y,z)
	for (int kx=1; kx<=3; ++kx) {
	  
	  Real harmmag = 0.0;
	  if (kx <= u.kxmaxDealiased()) {
	    uk.setToZero();
	    for (int i=0; i<3; ++i)
	      for (int my=0; my<My; ++my)
		for (int mz=0; mz<Mz; ++mz) 
		  uk.cmplx(kx,my,mz,i) = u.cmplx(kx,my,mz,i);
	    harmmag = LinfNorm(uk);
	  }
	  os << left<<setw(cw) << harmmag << space;	
	}

      }

      if (outT) {
	Real T;
	load(T, solndir + "Tbest.asc");
	os << left<<setw(cw) << T << space;
      }

      if (outtr) {
	os << setprecision(0);
	array<Real> trunc = truncerr(u); 
	os << trunc[0] << "    " << trunc[1] << "    " << trunc[2] << "    ";
	os << setprecision(digits);
      }

      if (outev) {
	int Nunstab = -1;
	ifstream is((solndir + "arnoldi-0/lambda.asc").c_str());
	if (is.good()) {
	  char s;
	  int N;
	  Real mu, omega;
	  Real eps = 1e-04;
	  Nunstab = 0;
	  is >> s >> N;

	  for (int n=0; n<N; ++n) {
	    is >> mu >> omega;
	    if (mu > eps)
	      ++Nunstab;
	    else
	      break;
	  }
	}
	os << left<<setw(10) << Nunstab;
      }

      if (outev2) {
	char s;
	int N;
	Real mu, omega;
	Real eps = 1e-06;
	ifstream is;

	int Nsxyz = -1;
	is.open((solndir + "arnoldi-sxyz/lambda.asc").c_str());
	if (is.good()) {
	  Nsxyz = 0;
	  is >> s >> N;

	  for (int n=0; n<N; ++n) {
	    is >> mu >> omega;
	    if (mu > eps)
	      ++Nsxyz;
	    else
	      break;
	  }
	}
	is.close();

	int Naxyz = -1;
	is.open((solndir + "arnoldi-axyz/lambda.asc").c_str());
	if (is.good()) {
	  Naxyz = 0;
	  is >> s >> N;

	  for (int n=0; n<N; ++n) {
	    is >> mu >> omega;
	    if (mu > eps)
	      ++Naxyz;
	    else
	      break;
	  }
	}
	is.close();

	int Nunstab = (Nsxyz == -1 || Naxyz == -1) ? -1 : Naxyz + Nsxyz ;

	os << left<<setw(10) << Nsxyz;
	os << left<<setw(10) << Naxyz;
	os << left<<setw(10) << Nunstab;
      }

      if (outrs) {

	Real residual = 0;
	const int N=512;
	char buff[N];
	
	// read residual in from convergence.asc
	ifstream is((solndir + "convergence.asc").c_str());	
	while (is.good()) {
	  is.getline(buff, N); // eat first line, or remainder of previous line
	  is >> residual;
	}
	os << setprecision(2) << setw(10) << residual << setprecision(digits);	  
	  
      }

      if (outzl) 
	os << setprecision(2) << setw(10) << zlocalization(u) << setprecision(digits);

      string eigenwarning = (outev && fileexists(solndir + "arnoldi-0/WARNING")) ? " WARNING" : "";
      os << comment << " " << subdir << eigenwarning << endl;
      
      if (singledir) {
	//cout << "exiting!" << endl;
	exit(0);
      }
    }
    /******************************************
    // If subdir was put into failures contdir/failures/subdir, skip and continue processing next ndir
    else if ((Nmax != 0 && ndir < Nmax) || fileexists(contdir + "failures/" + subdir)) {
      //cout << "skipping" << endl;
      //cout << (contdir + "failures/" + subdir) << " : skipping" << endl;
      continue;
    }
    else {
      //cout << "exiting" << endl;
      //cout << subdir << " is neither in contdir nor failures. Exiting." << endl;
      break;
    }
    ********************************************/
  }

  os << comment << " " << hostname << ":" << workdir << "/" << contdir << "\n\n\n";
  //cout << endl;
}

bool fileexists(const string& filename) {
  struct stat st;
  return (stat(filename.c_str(), &st) == 0) ? true : false;
}


// Cases,
Real zwavelength(FlowField& u) {
  u.makeSpectral();

  int  Nz  = u.Nz();
  int  Nz2 = Nz/2;

  Real Lz  = u.Lz();
  Real Lz2 = Lz/2;

  // Compute the xy-average of streamwise velocity u
  //FlowField uxyavg(4, 1, u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
  //for (int mz=0; mz<u.Mz(); ++mz)
  //uxyavg.cmplx(0,0,mz,0) = u.cmplx(0,0,mz,0);

  // Let f(z) = <u>_{xy}(z) - <u>_{xyz}
  PeriodicFunc f(u.Nz(), u.Lz(), Spectral);
  for (int mz=1; mz<u.Mz(); ++mz)
    f.cmplx(mz) = u.cmplx(0,0,mz,0);
  
  PeriodicFunc dfdz = diff(f);

  // f(z) falls into three classes for computing internal wavelength
  // "odd"  parity: f  changes sign about f(Lz/2) i.e. nz=Nz/2
  // "even" parity: f' changes sign about f(Lz/2) 
  // neither

  // Determine which parity we have
  f.makePhysical();
  dfdz.makePhysical();

  //ofstream os("f.asc");
  //for (int nz=0; nz<Nz; ++nz)
  //os << u.z(nz) << " " << f(nz) << " " << dfdz(nz) << endl;
  //os << Lz << " " << f(0) << " " << dfdz(0) << endl;

  bool fodd  = (f(Nz2-1)* f(Nz2+1) < 0)        ? true : false;
  bool feven = (dfdz(Nz2-1) * dfdz(Nz2+1) < 0) ? true : false;

  // Change f so that it has odd parity.
  // If f is even, replace f with f' and compute wavelength with same algorithm,
  // namely finding the next zeros on either side of Lz/2.
  if (feven) {
    f = dfdz;
    f.makeSpectral();
    dfdz = diff(f);
    fodd = true;
    feven = false;
  }
  // If is is neither even nor odd, find zero closest to z=Lz/2 and shift f
  // so that it is odd
  else if (!fodd) {

    Real leftzero = Lz2;
    for (int nz = Nz2-1; nz>0; --nz) 
    if (f(nz)*f(nz-1) < 0) { // sign change
      leftzero = (u.z(nz-1) + u.z(nz))/2;    // set initial guess to midpoint
      break;
    }
    Real rghtzero = Lz2;
    for (int nz = Nz2+1; nz<Nz-1; ++nz) 
      if (f(nz)*f(nz+1) < 0) { // sign change
	rghtzero = (u.z(nz) + u.z(nz+1))/2;  // set initial guess to nearby value
	break;
    }

    // Catastrophic failure: f never changes sign. Return 0.
    if (leftzero == Lz2 && rghtzero == Lz2)
      return 0.0;

    // Approximate center of odd symmetry
    Real center = (abs(leftzero-Lz2) < abs(rghtzero-Lz2)) ? leftzero : rghtzero;
    //cout << "center   == " << center << endl;
    f.makeSpectral();
    center = newtonSearch(f, center);
    //cout << "center   == " << center << endl;

    Real shift  = center - Lz2;
    //cout << "shift    == " << shift << endl;
    Complex c(0.0, 2*pi*shift/Lz);
    for (int mz=0; mz<u.Mz(); ++mz)
      f.cmplx(mz) *= exp(Real(mz)*c);

    dfdz = diff(f);    

    fodd = true;
    feven = false;
  }
  
  f.makePhysical();
  dfdz.makePhysical();
       
  //os.close();
  //os.open("g.asc");
  //for (int nz=0; nz<Nz; ++nz)
  //os << u.z(nz) << " " << f(nz) << " " << dfdz(nz) << endl;
  //os << Lz << " " << f(0) << " " << dfdz(0) << endl;

  // Now we are guaranteed that f(z) is odd about z=Lz/2.
    
  // Find a wavelength by the distance between zeros on either side of z = Lz/2
  //cout << "Find zero to left of center" << endl;
  Real leftzero = Lz2;
  for (int nz = Nz2-1; nz>0; --nz) 
    if (f(nz)*f(nz-1) < 0) { // sign change
      leftzero = (u.z(nz-1) + u.z(nz))/2;    // set initial guess to midpoint
      break;
    }
  Real rghtzero = Lz/2;
  for (int nz = Nz2+1; nz<Nz-1; ++nz) 
    if (f(nz)*f(nz+1) < 0) { // sign change
      rghtzero = (u.z(nz) + u.z(nz+1))/2;  // set initial guess to nearby value
      break;
    }
  f.makeSpectral();
  leftzero = newtonSearch(f, leftzero);
  rghtzero = newtonSearch(f, rghtzero);

  return rghtzero-leftzero;
}



Real zpadding(FlowField& u, Real eps) {
  u.makeSpectral();

  int  Nz  = u.Nz();
  int  Nz2 = Nz/2;

  // Compute the xy-average of streamwise velocity u
  //FlowField uxyavg(4, 1, u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
  //for (int mz=0; mz<u.Mz(); ++mz)
  //uxyavg.cmplx(0,0,mz,0) = u.cmplx(0,0,mz,0);
  u.makeSpectral();
  PeriodicFunc uxyavg(u.Nz(), u.Lz(), Spectral);
  for (int mz=0; mz<u.Mz(); ++mz)
    uxyavg.cmplx(mz) = u.cmplx(0,0,mz,0);

  // Find first place from left where uxyzvg is above small threshold
  Real leftedge = 0;
  uxyavg.makePhysical();
  ofstream os("g.asc");

  for (int nz = 0; nz<Nz; ++nz) 
    os << u.z(nz) << " " << uxyavg(nz) << "\n";
  os << u.Lz() << " " << uxyavg(0) << "\n";

  for (int nz = 0; nz<=Nz2; ++nz) 
    if (abs(uxyavg(nz)) < eps) 
      leftedge = u.z(nz);      
    else
      break;

  //cout << "Find zero to right of center" << endl;    
  Real rightedge = u.Lz();
  uxyavg.makePhysical();
  for (int nz = Nz-1; nz>=Nz/2; --nz) 
    if (abs(uxyavg(nz)) < eps) 
      rightedge = u.z(nz);        
    else
      break;

  return 1 - (rightedge-leftedge)/u.Lz();
}

Real zlocalization(FlowField& u) {     // sqrt ratio of energy at z=0 and maximum energy (energy avg over x,y)
  FlowField e = energy(u);
  e.makeSpectral();

  PeriodicFunc exyavg(e.Nz(), e.Lz(), Spectral);
  for (int mz=0; mz<e.Mz(); ++mz)
    exyavg.cmplx(mz) = e.cmplx(0,0,mz,0);
  exyavg.makePhysical();

  Real emin = exyavg(0);
  Real emax = emin;

  for (int nz=1; nz<e.Nz(); ++nz) {
    emin = lesser(emin, exyavg(nz));
    emax = Greater(emax, exyavg(nz));
  }
  return sqrt(emin/emax);
}

// return slope dz/dx of line interpolating extrema of pressure in y=0 midplane
Real skewing(FlowField& u, Real Reynolds) {

  // Compute pressure field. Assumptions:
  //    u is perturbation on plane couette base flow, 
  //    Reynolds = 1/nu
  //    u has sxyz symmetry about center of domain Lx/2, Lz/2
  // 
  ChebyCoeff Ubase(u.Ny(), u.a(), u.b(), Spectral);
  ChebyCoeff Wbase(u.Ny(), u.a(), u.b(), Spectral);
  Ubase[1] = 1;     // Ubase = y;

  FlowField p;
  {
    PressureSolver poisson(u, Ubase, Wbase, 1/Reynolds, Convection); 
    FlowField p0 = poisson.solve(u);

    // Extract p(x,z) at y=0 midplane
    p0.makePhysical();
    p = FlowField(p0.Nx(), 3, p0.Nz(), 1, p0.Lx(), p0.Lz(), p0.a(), p0.b(), Physical, Physical);
    const int ny2 = (p0.Ny()-1)/2;
    
    for (int ny=0; ny<3; ++ny)
      for (int nx=0; nx<p.Nx(); ++nx)
	for (int nz=0; nz<p.Nz(); ++nz)
	  p(nx,ny,nz,0) = p0(nx,ny2,nz,0);
    p.makeSpectral();
  }

  
  Real xguess = u.Lx()/2;
  Real zguess = u.Lz()/2;
  
  ColumnVector ex0 = pextrememum(p, xguess, zguess);

  Real lz = zwavelength(u);

  xguess = ex0(0);
  zguess = ex0(1) + lz/2;
  
  ColumnVector ex1 = pextrememum(p, xguess, zguess);
  
  //cout << endl;
  //Real Lx2= u.Lx()/2;
  //Real Lz2= u.Lz()/2;
  //cout << endl;
  //cout << "x,z == " << u.Lx()/2 << '\t' << u.Lz()/2 << endl;
  //cout << "ex0 == " << ex0(0) << '\t' << ex0(1) << endl;
  //cout << "x,z == " << ex0(0) << '\t' << ex0(1) + lz/2 << endl;
  //cout << "ex1 == " << ex1(0) << '\t' << ex1(1) << endl;

  Real dxdz = (ex1(0)-ex0(0))/(ex1(1)-ex0(1));
  //cout << "dx/dz == " << dxdz << endl;
  return dxdz;
}

// return curvature d2z/dx2 of line interpolating extrema of pressure in y=0 midplane
Real bending(FlowField& u, Real Reynolds) {

  // Compute pressure field. Assumptions:
  //    u is perturbation on plane couette base flow, 
  //    Reynolds = 1/nu
  //    u has sxyz symmetry about center of domain Lx/2, Lz/2
  // 
  ChebyCoeff Ubase(u.Ny(), u.a(), u.b(), Spectral);
  ChebyCoeff Wbase(u.Ny(), u.a(), u.b(), Spectral);
  Ubase[1] = 1;     // Ubase = y;

  FlowField p;
  {
    PressureSolver poisson(u, Ubase, Wbase, 1/Reynolds, Convection); 
    FlowField p0 = poisson.solve(u);

    // Extract p(x,z) at y=0 midplane
    p0.makePhysical();
    p = FlowField(p0.Nx(), 3, p0.Nz(), 1, p0.Lx(), p0.Lz(), p0.a(), p0.b(), Physical, Physical);
    const int ny2 = (p0.Ny()-1)/2;
    
    for (int ny=0; ny<3; ++ny)
      for (int nx=0; nx<p.Nx(); ++nx)
	for (int nz=0; nz<p.Nz(); ++nz) 
	  p(nx,ny,nz,0) = p0(nx,ny2,nz,0);
    p.makeSpectral();
  }

  // Bending will be computed from four pressure extrema straddling the center
  // z=Lz/2, namely ex0 ex1 | ex2 ex3, where | represents z=Lz/2
  // The z locations of these points will be roughly
  //    ex0    ex1   |  ex2    ex3
  //  -3lz/4  -lz/4  |  lz/4  3lz/4,  relative to the z=Lz/2 center
  // where lz is the spanwise wavelength of the internal structure
  
  // Start by trying to find ex2
  // The x phase of a TW is arbitrary, so we have to search for a pressure 
  // extremum along x at z = Lz/2 + lz/4 to get a decent initial guess
  // for the newton search. Arbitrarily choose minimum for extremum.
  Real lz = zwavelength(u);
  Real Lz = u.Lz();
  Real xguess = u.x(0);
  Real zguess = Lz/2+lz/4;
  Real pmin = p.eval(xguess, 0, zguess, 0);

  for (int nx=0; nx<u.Nx(); ++nx) {
    Real peval = p.eval(u.x(nx), 0, zguess, 0);
    //cout << "x, p(x) == " << u.x(nx) << '\t' << peval << endl;
    if (peval < pmin) {
      xguess = u.x(nx);
      pmin = peval;
    }
  }

  // Find extremum ex2
  ColumnVector ex2 = pextrememum(p, xguess, zguess);

  // Find extremum ex3
  xguess = ex2(0);          // same x value
  zguess = ex2(1) + lz/2;   // half wavelength in z to right
  ColumnVector ex3 = pextrememum(p, xguess, zguess);
  
  // Find extremum ex1
  xguess = ex2(0);          // same x value as ex2
  zguess = Lz - ex2(1);     // symmetric opposite of ex2(1) about z=Lz/2
  ColumnVector ex1 = pextrememum(p, xguess, zguess);
  
  // Find extremum ex0
  xguess = ex3(0);          // same x value as ex3
  zguess = Lz - ex3(1);     // symmetric opposite of ex3(1) about z=Lz/2
  ColumnVector ex0 = pextrememum(p, xguess, zguess);

  // Approximate dx/dz halfway between ex2, ex3
  Real dxdz_23 = (ex3(0)-ex2(0))/(ex3(1)-ex2(1));
  Real z_23 = (ex2(1)+ex3(1))/2;

  // Approximate dx/dz halfway between ex0, ex1
  Real dxdz_01 = (ex1(0)-ex0(0))/(ex1(1)-ex0(1));
  Real z_01 = (ex0(1)+ex1(1))/2;

  // Now approximate d^2x/dz^2 at z=Lz/2
  Real d2zdx2 = (dxdz_23 - dxdz_01)/(z_23 - z_01);

  return d2zdx2;
}

Real eval(FlowField& f, ColumnVector& xi) {
  return f.eval(xi(0), 0.0, xi(1), 0);
}



// Find nearest extremum of pressure gradient using Newton search
ColumnVector pextrememum(FlowField& p, Real xguess, Real zguess) {
  FlowField px = xdiff(p);
  FlowField pz = zdiff(p);

  FlowField pxx = xdiff(px);
  FlowField pxz = zdiff(px);
  FlowField pzz = zdiff(pz);

  // Do newton search for f(xi) = 0 where f = (px,pz) and xi = (x,z)
  ColumnVector xi(2);
  ColumnVector f_xi(2); 
  xi(0) = xguess;
  xi(1) = zguess;
  
  for (int n=0; n<10; ++n) {
    
    f_xi(0) = eval(px,xi);
    f_xi(1) = eval(pz,xi);

    if (pythag(f_xi(0),f_xi(1)) < 1e-13)
      break;

    MatrixXd Df(2,2);
    Df(0,0) = eval(pxx, xi);
    Df(0,1) = eval(pxz, xi);
    Df(1,0) = Df(0,1); // eval(pxz, xi);
    Df(1,1) = eval(pzz, xi);    

    //Real peval = p.eval(xi(0),0,xi(1),0);

    //f_xi *= -1.0;
    ColumnVector dxi = Df.householderQr().solve(f_xi);
    //f_xi *= -1.0;
    ColumnVector Dfdxi = Df*dxi;

    xi -= dxi;

  }
  return xi;
}

// Compute shear force on wall, integrated over x and HALF OF Z DOMAIN, from z=0 to Lz/2
// i.e., (+/-1)/Lx integral_0^Lz/2 integral_0^Lx du/dy(x,+/-1,z) dx dz
Real wallforce(FlowField& u, int i, int wall) {

  int  Nz  = u.Nz();
  int  Nz2 = Nz/2;

  u.makeSpectral();

  // Compute <ui>_x(y,z), the x-average of ith component of u
  FlowField uxavg(4, u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
  for (int my=0; my<u.My(); ++my)
    for (int mz=0; mz<u.Mz(); ++mz)
      uxavg.cmplx(0,my,mz,0) = u.cmplx(0,my,mz,i);

  // Compute d/dy <ui>_x(y,z)
  FlowField dudy_xavg = ydiff(uxavg);
  dudy_xavg.makeState(Physical, Physical);
  
  // Evaluate d/dy <ui>_x(y,z) at wall y = +/-1
  int ny = (wall == 1) ? 0 : u.Ny()-1; 
  cout << endl << "evaluating at y = " << u.y(ny) << endl;
  PeriodicFunc dudy_xavg_wall(u.Nz(), u.Lz(), Physical);

  for (int nz=0; nz<u.Nz(); ++nz)
    dudy_xavg_wall(nz) = dudy_xavg(0,ny,nz,0);

  string comp;
  if (i==0)
    comp = "u";
  else if (i==1)
    comp = "v";
  else if (i==2)
    comp = "w";
  if (wall == 1)
    dudy_xavg_wall.save("top_wall_d" + comp + "dy");
  else
    dudy_xavg_wall.save("bot_wall_d" + comp + "dy");
  dudy_xavg_wall.makeSpectral();

  // Integrate d/dy <ui>_x(wall,z) from center z=Lz/2 to box edge z = Lz = 0.
  // Note that the integral of a periodic function with nonzero mean is not itself
  // periodic. The mean term c in the original function has to be integrated
  // separately to cz.
  PeriodicFunc integrated_shear = integrate(dudy_xavg_wall);
  Real mean_shear = dudy_xavg_wall.mean();
  integrated_shear.makePhysical();
  
  if (wall == 1) {
    integrated_shear.save("int_top_wall_d" + comp + "dy");
    save(mean_shear, "mean_top_wall_d" + comp + "dy");
  }
  else {
    integrated_shear.save("int_bot_wall_d" + comp + "dy");
    save(mean_shear, "mean_bot_wall_d" + comp + "dy");
  }
  // multiply by wall = +-1 because shear force == normal * du/dy
  return wall*(integrated_shear(Nz-1) - integrated_shear(Nz2) + mean_shear*u.Lz()/2);
}

// return (integral_0^1 <u>(y) dy - integral_-1^0 <u>(y) dy)*Lz, where <u> is xz-mean flow
Real circulation(FlowField& u, int i) {
  u.makeSpectral();
  ChebyCoeff umean = Re(u.profile(0,0,i)); // get xz-mean flow
  ChebyCoeff f = integrate(umean);         // integrate in y
  f.makePhysical();

  int nytop = 0;
  int nybot = u.Ny()-1;

  return (f(nytop) - f(nybot))*u.Lz(); // multiply by Lz to undo z-normalization of mean
}
void load(Real& x, const string& filebase, Real default_value) {
  ifstream is((filebase + ".asc").c_str());
  if (is.good())
    is >> x;
  else
    x = default_value;
}

Real LrmsNorm(FlowField& u) {

  FlowField e = energy(u);        // compute pointwise |u|^2
  e.makeSpectral();

  ChebyCoeff e00 = Re(e.profile(0,0,0)); // get <|u|^2>_{xz}(y)
  e00.makePhysical();             

  Real max = LinfNorm(e00);           // find max_y <|u|^2>_{xz}(y)

  return sqrt(max);               // return max_y sqrt(<|u|^2>_{xz}(y))
}
  
