//
// controller.cpp
//
// Created by:
//
// Geoffroy Claisse
// University of Southampton, 08/2018
//
// Peter Heins on 27/02/2012.
// Copyright 2012 University of Sheffield. All rights reserved.
//
#include <iostream>
#include <sstream>
#include <string.h>
#include <complex>
#include <Eigen/Dense>

#include <channelflow/utilfuncs.h>

#include "config.h"
#include "channelflow/controller.h"

#ifdef HDF5_FOUND // set by cmake in config.h
#include "H5Cpp.h"
#define HAVE_HDF5 1
#else
#define HAVE_HDF5 0
#endif

using namespace std;

namespace channelflow
{
    //############################################################
    // Created by Geoffroy Claisse
    // University of Southampton, 08/2018
    //############################################################

    #ifdef HAVE_HDF5
    // read the matrix dataset 'mat' in 'h5file' and store it to 'matrix'
    int hdf5read(
            double** matrix,
            size_t N0,
            size_t N1,
            H5::H5File& h5file);
    int hdf5read_real(
            Eigen::MatrixXcd& matrix,
            H5::H5File& h5file);
    int hdf5read_real(
            Eigen::VectorXcd& vector,
            H5::H5File& h5file);
    int hdf5read_complex(
            Eigen::MatrixXcd& matrix,
            H5::H5File& h5file);
    int hdf5read_complex(
            Eigen::VectorXcd& vector,
            H5::H5File& h5file);

    int hdf5write_real(
            Eigen::MatrixXcd& matrix,
            H5::H5File* h5file);
    int hdf5write_real(
            Eigen::VectorXcd& vector,
            H5::H5File* h5file);
    int hdf5write_complex(
            Eigen::MatrixXcd& matrix,
            H5::H5File* h5file);
    int hdf5write_complex(
            Eigen::VectorXcd& vector,
            H5::H5File* h5file);
    #endif // HAVE_HDF5

    int MatIn_hdf5(
            double **matrix,
            const size_t N0,
            const size_t N1,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read(matrix, N0, N1, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }

    int MatIn_hdf5_real(
            Eigen::MatrixXcd& matrix,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read_real(matrix, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }

    int MatIn_hdf5_real(
            Eigen::VectorXcd& vector,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read_real(vector, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }

    int MatIn_hdf5_complex(
            Eigen::MatrixXcd& matrix,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read_complex(matrix, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }

    int MatIn_hdf5_complex(
            Eigen::VectorXcd& vector,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specifield HDF5 file
            H5::H5File h5file(filename, H5F_ACC_RDONLY);

            // Read the new specified file to get the matrix
            hdf5read_complex(vector, h5file);
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatIn_hdf5. HDF5 not defined.");
        }
    }

    int MatOut_hdf5_real(
            Eigen::MatrixXcd& matrix,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specified HDF5 file
            auto* h5file = new H5::H5File(filename, H5F_ACC_TRUNC);

            // Write the vector into the new file
            hdf5write_real(matrix, h5file);

            delete h5file;
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatOut_hdf5. HDF5 not defined.");
        }
    }

    int MatOut_hdf5_real(
            Eigen::VectorXcd& vector,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specified HDF5 file
            auto* h5file = new H5::H5File(filename, H5F_ACC_TRUNC);

            // Write the vector into the new file
            hdf5write_real(vector, h5file);

            delete h5file;
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatOut_hdf5. HDF5 not defined.");
        }
    }

    int MatOut_hdf5_complex(
            Eigen::MatrixXcd& matrix,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specified HDF5 file
            auto* h5file = new H5::H5File(filename, H5F_ACC_TRUNC);

            // Write the vector into the new file
            hdf5write_complex(matrix, h5file);

            delete h5file;
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatOut_hdf5. HDF5 not defined.");
        }
    }

    int MatOut_hdf5_complex(
            Eigen::VectorXcd& vector,
            const std::string& filename)
    {
        if (HAVE_HDF5) {
            using namespace H5;
            // Open the specified HDF5 file
            auto* h5file = new H5::H5File(filename, H5F_ACC_TRUNC);

            // Write the vector into the new file
            hdf5write_complex(vector, h5file);

            delete h5file;
            return 0;
        }
        else {
            channelflow::cferror("Error in controller.cpp MatOut_hdf5. HDF5 not defined.");
        }
    }

    //############################################################
    // Created by Peter Heins on 27/02/2012.
    // Copyright 2012 University of Sheffield. All rights reserved.
    //############################################################

    // Reads in matrix from file
    double** MatIn(const char* FileName) {
        double* memblock;
        int size;

        ifstream Matfile(FileName, ios_base::in|ios_base::binary|ios::ate);
        if (Matfile.is_open()) {

            size = (int) Matfile.tellg();
            memblock = new double[size];
            Matfile.seekg(0,ios::beg);
            Matfile.read((char*)memblock,size);
            Matfile.close();

            // read in matrices' dimensions
            int num_row = iround(memblock[0]);
            int num_col = iround(memblock[1]);


            double **Mat;
            Mat = new double*[num_row];
            for (int i=0;i<num_row;++i){
                Mat[i] = new double[num_col];
            }

            int Vecsize = num_row*num_col;
            double Vec[Vecsize];

            for (int i=0; i<Vecsize; ++i) { // vector
                Vec[i] = memblock[i+2];
            }

            int count =0;
            for (int row=0; row<num_row; ++row) {
                for (int col=0; col<num_col; ++col) { // matrix
                    Mat[row][col] = Vec[count];
                    ++count;
                }
            }

            delete[] memblock;
            return Mat;
            //delete[] Mat;

        }
        else {
            cout << "Unable to open file" << endl;
            return nullptr;
        }
    }

    double** MatIn_asc(const char* FileName) {
        fstream Matfile(FileName, ios_base::in|ios_base::out);
        Matfile.seekg(0);

        // read in matrices' dimensions
        int num_row;
        int num_col;
        Matfile >> num_row >> num_col;

        double **Mat;
        Mat = new double*[num_row];
        for (int i=0;i<num_row;++i){
            Mat[i] = new double[num_col];
        }

        int Vecsize = num_row*num_col;
        double Vec[Vecsize];

        for (int i=0; i<Vecsize; ++i) { // vector
            Matfile >> Vec[i];
        }

        int count =0;
        for (int row=0; row<num_row; ++row) {
            for (int col=0; col<num_col; ++col) { // matrix
                Mat[row][col] = Vec[count];
                ++count;
            }
        }
        return Mat;
    }

    // Reads in number of rows of matrix in file
    int NumRow(const char* FileName) {
        double* memblock;
        int size;

        ifstream Matfile(FileName, ios_base::in|ios_base::binary|ios::ate);
        size = (int) Matfile.tellg();
        memblock = new double[size];
        Matfile.seekg(0,ios::beg);
        Matfile.read((char*)memblock,size);
        Matfile.close();

        // read in matrices' dimensions
        int num_row = iround(memblock[0]);

        delete[] memblock;

        return num_row;

    }
    // Reads in number of columnss of matrix in file
    int NumCol(const char* FileName) {
        double* memblock;
        int size;

        ifstream Matfile(FileName, ios_base::in|ios_base::binary|ios::ate);
        size = (int) Matfile.tellg();
        memblock = new double[size];
        Matfile.seekg(0,ios::beg);
        Matfile.read((char*)memblock,size);
        Matfile.close();

        // read in matrices' dimensions
        int num_col = iround(memblock[1]);


        delete[] memblock;

        return num_col;

    }
    // Multiplies matrix with vector
    double* MatVec(double** Mat, const double Vec[], int Matrow, int Matcol) {
        double *Result;
        Result = new double[Matrow];

        for (int i=0;i<Matrow;++i){ // Initialise result vector
            Result[i] = 0.0;
        }

        double resultmat[Matrow][Matcol];
        for (int j=0;j<Matcol;++j) {
            for (int i=0;i<Matrow;++i) {
                resultmat[i][j] = Mat[i][j]*Vec[j];
            }
        }

        for (int i=0;i<Matrow;++i) {
            for (int j=0;j<Matcol;++j) {
                Result[i] += resultmat[i][j] ;
            }
        }
        return Result;
        //delete[] Result;
    }

    // Solves Ax=B for x
    void gauss(double** A, double* B, int N){
        float* alpha;
        alpha = new float[N*N];
        float* beta;
        beta = new float[N];

        int cnt = 0;

        for (int i=0;i<N;++i){
            beta[i] = B[i];
            for (int j=0;j<N;++j){
                alpha[cnt] = A[j][i];
                ++cnt;
            }
        }

        int *IPIV = new int[N+1];

        int INFO;

        int NRHS = 1;


        INFO = clapack_sgetrf(CblasColMajor,N,N,alpha,N,IPIV); 

        if (INFO==0){
            INFO = clapack_sgetrs(CblasColMajor,CblasNoTrans,N,NRHS,alpha,N,IPIV,beta,N);

            if (INFO==0){

                for (int i=0;i<N;++i){
                    B[i] = beta[i];
                }
            }
            else {
                cerr << "Error: Gauss LU" << endl;
            }
        }
        else {
            cerr << "Error: Gauss" << endl;
        }

        delete[] IPIV;
        delete[] alpha;
        delete[] beta;
    }

    // returns square identity matrix dimension numCstates
    double** eyeF(int numCstates) {
        // Identity matrix, size of Ak
        double** eyeA;
        eyeA = new double*[numCstates];
        for (int i=0;i<numCstates;++i){
            eyeA[i] = new double[numCstates];
        }


        for (int i=0;i<numCstates;++i){
            for (int j=0;j<numCstates;++j){
                if (i-j==0) {
                    eyeA[i][j] = 1.0;
                }
                else {
                    eyeA[i][j] = 0.0;
                }
            }
        }

        return eyeA;
        //delete[] eyeA;
    }


    //############################################################
    // CONTROLLER MOTHER CLASS
    // Created by Geoffroy Claisse
    // University of Southampton, 08/2018
    //############################################################

    // Only use for compiling and polymorphism.
    // Functions are not implemented here, but overridden in child classes.

    // NOT IMPLEMENTED
    // Peter Heins do call the method Controller.ConStates
    // and then use it as parameter of dns.advance,
    // which called itself the method Controller.advance_Con
    // I do not understand why Constates is not used
    // privately in the class Controller as a class member ?
    double*** Controller::ConStates() {
        cout << "controller.cpp: Controller::ConStates" << endl;
        throw NotImplementedException();
    }

    // NO IMPLEMENTED
    // returns array containing number of controller states
    // at each kx,kz pair
    double** Controller::CStateInfo() {
        cout << "controller.cpp: Controller::CStateInfo" << endl;
        throw NotImplementedException();
    }

    // NOT IMPLEMENTED
    // Returns kx wavenumber based on controlled mx_c mode number
    // (which differs from sim mx mode number)
    int Controller::kx_c(int mx_c) {
        cout << "controller.cpp: Controller::kx_c" << endl;
        throw NotImplementedException();
    }

    // NOT IMPLEMENTED
    void Controller::advance_Con(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO)
    {
        cout << "controller.cpp: Controller::advance_Con" << endl;
        throw NotImplementedException();
    }

    // NOT IMPLEMENTED
    void Controller::advance_Con_CON(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO)
    {
        cout << "controller.cpp: Controller::advance_Con_CON" << endl;
        throw NotImplementedException();
    }


    //############################################################
    // CONTROLLER_HEINS CLASS
    // Created by Peter Heins on 27/02/2012.
    // Copyright 2012 University of Sheffield. All rights reserved.
    //############################################################

    Controller_Heins::Controller_Heins()
        :
            Ny_(0),
            minKx_(0),
            maxKx_(0),
            minKz_(0),
            maxKz_(0),
            uvw_(0),
            StateInfoFile_("Mult_Control_Mat/StateInfo.bin"),
            NumConInputs_(0),
            tau_(0.0),
            Spectral_states_(false)
    {}

    Controller_Heins::Controller_Heins(
            int Ny,
            int minKx,
            int maxKx,
            int minKz,
            int maxKz,
            int uvw,
            const char* StateInfoFile,
            int NumConInputs,
            double tau,
            bool Spectral_states)
        :
            Ny_(Ny),
            minKx_(minKx),
            maxKx_(maxKx),
            minKz_(minKz),
            maxKz_(maxKz),
            uvw_(uvw),
            StateInfoFile_(StateInfoFile),
            NumConInputs_(NumConInputs),
            tau_(tau),
            Spectral_states_(Spectral_states)
    {}

    // returns array containing number of controller states
    // at each kx,kz pair
    double** Controller_Heins::CStateInfo() {
        double** InfoMat;
        int minKx = minKx_;
        int maxKx = maxKx_;
        int minKz = minKz_;
        int maxKz = maxKz_;

        int maxMx = (maxKx-minKx)+1;
        int maxMz = (maxKz-minKz)+1;

        InfoMat = new double*[maxMx];
        for (int mx=0;mx<maxMx;++mx){
            InfoMat[mx] = new double[maxMz];
        }

        double** InMat = MatIn(StateInfoFile_);
        int cnt=0;

        for (int mx=0;mx<maxMx;++mx){
            for (int mz=0;mz<maxMz;++mz){
                InfoMat[mx][mz] = InMat[cnt][2];
                cnt += 1;
            }
        }

        for (int i=0;i<maxMx*maxMz;++i){
            delete[] InMat[i];
        }

        delete[] InMat;
        return InfoMat;
        //delete[] InfoMat;
    }

    // returns 3D array which is used to store controller states
    // at each kx,kz pair
    double*** Controller_Heins::ConStates() {
        int minKx = minKx_;
        int maxKx = maxKx_;
        int minKz = minKz_;
        int maxKz = maxKz_;

        int maxMx = (maxKx-minKx)+1;
        int maxMz = (maxKz-minKz)+1;

        double** StateInfo = CStateInfo();
        int cnt=0;
        int tmpNS;
        double*** StateMat;
        StateMat = new double**[maxMx];
        for (int mx=0;mx<maxMx;++mx) {
            StateMat[mx] = new double*[maxMz];

            for (int mz=0;mz<maxMz;++mz){
                tmpNS = int(StateInfo[mx][mz]);
                StateMat[mx][mz] = new double[tmpNS];
                cnt +=1;
            }
        }

        for (int mx=0;mx<maxMx;++mx){
            for (int mz=0;mz<maxMz;++mz) {
                int tmp = int(StateInfo[mx][mz]);
                for (int i=0;i<tmp;++i){
                    StateMat[mx][mz][i] = 0.0;
                }
            }
        }

        for (int i=0;i<maxMx;++i){
            delete[] StateInfo[i];
        }

        delete[] StateInfo;
        return StateMat;

        //delete[] StateMat;
    }

    // Returns kx wavenumber based on controlled mx_c mode number
    // (which differs from sim mx mode number)
    int Controller_Heins::kx_c(int mx_c) {
        int minKx = minKx_;
        int maxKx = maxKx_;
        int maxMx_c = (maxKx-minKx)+1;
        int kx_c;

        if (mx_c >= 0 && mx_c <= maxKx) {
            kx_c = mx_c;
        }
        else {
            kx_c = mx_c-maxMx_c;
        }

        return kx_c;
    }

    // integrates controller forward in time by dT
    void Controller_Heins::advance_Con(
            FlowField& u,
            FlowField& q,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        int IOcnt = 0;
        int minKx = minKx_;
        int maxKx = maxKx_;
        int minKz = minKz_;
        int maxKz = maxKz_;
        int Ny = Ny_;
        int uvw = uvw_;

        int maxMx_c = (maxKx-minKx)+1;
        int maxMz_c = (maxKz-minKz)+1;

        double** StateInfo =  CStateInfo();

        for (int mx_c=0;mx_c<maxMx_c;++mx_c){
            int kx = kx_c(mx_c);
            for(int mz_c=0;mz_c<maxMz_c;++mz_c){
                int kz = mz_c + minKz;

                if (kx!=0 || kz!=0)
                {
                    cout << "(" << kx << "," << kz << ") " ;

                    int mx = u.mx(kx); // Simulation mode numbers
                    int mz = u.mz(kz);

                    // Get number of controller states, simulation states and control inputs
                    int numCStates = int(StateInfo[mx_c][mz_c]); // Controller states
                    int numSStates = 4*Ny; // Sim states
                    int numInputs = NumConInputs_; // Controller inputs
                    int numOutputs = 4; // Controller outputs - cannot change from 4

                    // Extract number of states from state array
                    double CStates[numCStates];

                    for (int i=0;i<numCStates;++i){
                        // Controller state matrix
                        CStates[i] = CStateMat[mx_c][mz_c][i];
                    }

                    // Read in A,B,C,D and Cs matrices for correct kx,kz pair
                    string KaF = "Mult_Control_Mat/Ka_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KbF = "Mult_Control_Mat/Kb_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KcF = "Mult_Control_Mat/Kc_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KdF = "Mult_Control_Mat/Kd_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string CsF = "Mult_Control_Mat/Cs_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    const char* KaFile = KaF.c_str();
                    const char* KbFile = KbF.c_str();
                    const char* KcFile = KcF.c_str();
                    const char* KdFile = KdF.c_str();
                    const char* CsFile = CsF.c_str();
                    double** Ka = MatIn(KaFile);
                    double** Kb = MatIn(KbFile);
                    double** Kc = MatIn(KcFile);
                    double** Kd = MatIn(KdFile);
                    double** Cs = MatIn(CsFile);

                    // Create sim state vector and find controller input
                    //int Nys = u.Ny(); // num of Cheb Ny points
                    double RSimStates[numSStates]; // real part of states
                    double ISimStates[numSStates]; // imag part of states
                    u.makeState(Spectral, Physical); // Make physical in y
                    q.makeState(Spectral, Physical);

                    for (int i=0;i<Ny;++i)
                    {
                        RSimStates[i] =      real(u.cmplx(mx,i,mz,0));
                        RSimStates[i+Ny] =   real(u.cmplx(mx,i,mz,1));
                        RSimStates[i+2*Ny] = real(u.cmplx(mx,i,mz,2));
                        RSimStates[i+3*Ny] = real(q.cmplx(mx,i,mz,0));
                        ISimStates[i] =      imag(u.cmplx(mx,i,mz,0));
                        ISimStates[i+Ny] =   imag(u.cmplx(mx,i,mz,1));
                        ISimStates[i+2*Ny] = imag(u.cmplx(mx,i,mz,2));
                        ISimStates[i+3*Ny] = imag(q.cmplx(mx,i,mz,0));
                    }

                    u.makeState(Spectral, Spectral);
                    q.makeState(Spectral, Spectral);

                    // Collect real and imaginary sim states in one vector
                    double SimStates[2*numSStates];
                    for (int i=0;i<numSStates;++i){
                        SimStates[i] = RSimStates[i];
                    }
                    for (int i=numSStates;i<2*numSStates;++i){
                        SimStates[i] = ISimStates[i-numSStates];
                    }

                    // Multiply state vector by Cs to get sim output/controller input
                    double* uk; // Controller input vector
                    double* yk; // Controller output vector
                    yk = new double[4];
                    uk = new double[numInputs];

                    uk = MatVec(Cs,SimStates,numInputs,2*numSStates);

                    // Integrate Discrete Controller forward in time
                    double* AkXk = MatVec(Ka,CStates,numCStates,numCStates);
                    double* BkUk = MatVec(Kb,uk,numCStates,numInputs);

                    // Update controller states
                    for (int i=0;i<numCStates;++i){
                        CStates[i] = AkXk[i] + BkUk[i];
                    }

                    // Calculate controller output
                    double* CkXk;
                    double* DkUk;

                    CkXk = MatVec(Kc,CStates,numOutputs,numCStates);
                    DkUk = MatVec(Kd,uk,numOutputs,numOutputs);
                    for (int i=0; i<numOutputs;++i){
                        // Calculate controller output
                        yk[i] = CkXk[i] + DkUk[i];
                    }

                    // Assign controller output to BCs flowfield
                    double Ad = exp((-1/tau_)*dT);
                    double Bd = -( exp((-1/tau_)*dT) -1);

                    // Simple low-pass filter
                    BC.cmplx(mx,1,mz,uvw) = (Ad*BC.cmplx(mx,1,mz,uvw)) + (Bd*(yk[0] + yk[2]*I));
                    BC.cmplx(mx,0,mz,uvw) = (Ad*BC.cmplx(mx,0,mz,uvw)) + (Bd*(yk[1] + yk[3]*I));


                    // Store updated states in state array
                    for (int i=0;i<numCStates;++i){
                        CStateMat[mx_c][mz_c][i] = CStates[i];
                    }

                    if (numInputs == 8){
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[4]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[5]; // Imag Str lower
                        IO[0][7][IOcnt]    = uk[2]; // Real Spa upper
                        IO[0][8][IOcnt]    = uk[6]; // Imag Spa upper
                        IO[0][9][IOcnt]    = uk[3]; // Real Spa lower
                        IO[0][10][IOcnt]   = uk[7]; // Imag Spa lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                        IO[1][7][IOcnt]    = 0.0;
                        IO[1][8][IOcnt]    = 0.0;
                        IO[1][9][IOcnt]    = 0.0;
                        IO[1][10][IOcnt]    = 0.0;
                    }
                    else {
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[2]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[3]; // Imag Str lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                    }

                    IOcnt += 1;

                    for (int i=0;i<numCStates;++i){
                        delete[] Ka[i];
                        //delete[] eyeA[i];
                        //delete[] alpha[i];
                        //delete[] beta[i];
                        delete[] Kb[i];
                    }

                    for (int i=0;i<4;++i){
                        delete[] Kc[i];
                        delete[] Kd[i];
                    }

                    for (int i=0;i<numInputs;++i){
                        delete[] Cs[i];

                    }

                    delete[] Ka;
                    delete[] Kb;
                    delete[] Kc;
                    delete[] Kd;
                    delete[] Cs;
                    //delete[] alpha;
                    //delete[] beta;
                    //delete[] bXk;
                    delete[] BkUk;
                    //delete[] Gam;
                    //delete[] eyeA;
                    delete[] CkXk;
                    delete[] DkUk;
                    delete[] uk;
                    delete[] yk;
                }
                else
                {
                    IO[0][0][IOcnt]    = t;
                    IO[0][1][IOcnt]    = kx;
                    IO[0][2][IOcnt]    = kz;
                    IO[0][3][IOcnt]    = 0; // Real upper
                    IO[0][4][IOcnt]    = 0; // Imag upper
                    IO[0][5][IOcnt]    = 0; // Real lower
                    IO[0][6][IOcnt]    = 0; // Imag lower

                    IO[1][0][IOcnt]    = t;
                    IO[1][1][IOcnt]    = kx;
                    IO[1][2][IOcnt]    = kz;
                    IO[1][3][IOcnt]    = 0; // Real upper
                    IO[1][4][IOcnt]    = 0; // Imag upper
                    IO[1][5][IOcnt]    = 0; // Real lower
                    IO[1][6][IOcnt]    = 0; // Imag lower

                    IOcnt += 1;
                }
            }

        }
        cout << " " << endl;
        cout << "Control applied to WNs kx="+i2s(minKx)+":"+i2s(maxKx)+", kz="+i2s(minKz)+":"+i2s(maxKz) << endl;

        for (int i=0;i<maxMx_c;++i){
            delete[] StateInfo[i];
        }

        delete[] StateInfo;
    }

    // integrates controller forward in time by dT
    void Controller_Heins::advance_Con_CON(
            FlowField& u,
            FlowField& q,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        int IOcnt = 0;
        int minKx = minKx_;
        int maxKx = maxKx_;
        int minKz = minKz_;
        int maxKz = maxKz_;
        int Ny = Ny_;
        int uvw = uvw_;

        int maxMx_c = (maxKx-minKx)+1;
        int maxMz_c = (maxKz-minKz)+1;

        bool Spectral_states = Spectral_states_;

        double** StateInfo = CStateInfo();

        for (int mx_c = 0; mx_c < maxMx_c; ++mx_c)
        {
            int kx = kx_c(mx_c);
            for(int mz_c = 0; mz_c < maxMz_c; ++mz_c)
            {
                int kz = mz_c + minKz;
                if (kx != 0 || kz != 0)
                {
                    //cout << "(" << kx << "," << kz << ") " ;
                    int mx = u.mx(kx); // Simulation mode numbers
                    int mz = u.mz(kz);

                    // Get number of controller states, simulation states and control inputs
                    int numCStates = int(StateInfo[mx_c][mz_c]); // Controller states
                    int numSStates = 4*Ny; // Sim states
                    int numInputs = NumConInputs_; // Controller inputs
                    int numOutputs = 4; // Controller outputs - cannot change from 4

                    // Extract number of states from state array
                    double CStates[numCStates];

                    for (int i = 0; i < numCStates; ++i)
                    {
                        CStates[i] = CStateMat[mx_c][mz_c][i]; // Controller state matrix
                    }

                    // Read in A,B,C,D and Cs matrices for correct kx,kz pair
                    string KaF = "Mult_Control_Mat/Ka_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KbF = "Mult_Control_Mat/Kb_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KcF = "Mult_Control_Mat/Kc_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string KdF = "Mult_Control_Mat/Kd_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    string CsF = "Mult_Control_Mat/Cs_mat_kx"+i2s(kx)+"kz"+i2s(kz)+".bin";
                    const char* KaFile = KaF.c_str();
                    const char* KbFile = KbF.c_str();
                    const char* KcFile = KcF.c_str();
                    const char* KdFile = KdF.c_str();
                    const char* CsFile = CsF.c_str();
                    double** Ka = MatIn(KaFile);
                    double** Kb = MatIn(KbFile);
                    double** Kc = MatIn(KcFile);
                    double** Kd = MatIn(KdFile);
                    double** Cs = MatIn(CsFile);

                    // Create sim state vector and find controller input
                    //int Nys = u.Ny(); // num of Cheb Ny points
                    double RSimStates[numSStates]; // real part of states
                    double ISimStates[numSStates]; // imag part of states

                    if (Spectral_states)
                    {
                        u.makeState(Spectral, Spectral); // Make spectral in y
                        q.makeState(Spectral, Spectral);

                        for (int i=0;i<Ny;++i){
                            RSimStates[i]       = real(u.cmplx(mx,i,mz,0));
                            RSimStates[i+Ny]    = real(u.cmplx(mx,i,mz,1));
                            RSimStates[i+2*Ny]  = real(u.cmplx(mx,i,mz,2));
                            RSimStates[i+3*Ny]  = real(q.cmplx(mx,i,mz,0));
                            ISimStates[i]       = imag(u.cmplx(mx,i,mz,0));
                            ISimStates[i+Ny]    = imag(u.cmplx(mx,i,mz,1));
                            ISimStates[i+2*Ny]  = imag(u.cmplx(mx,i,mz,2));
                            ISimStates[i+3*Ny]  = imag(q.cmplx(mx,i,mz,0));
                        }
                    } // fi (Spectral_states)
                    else
                    {
                        u.makeState(Spectral, Physical); // Make physical in y
                        q.makeState(Spectral, Physical);

                        for (int i=0;i<Ny;++i){
                            RSimStates[i]       = real(u.cmplx(mx,i,mz,0));
                            RSimStates[i+Ny]    = real(u.cmplx(mx,i,mz,1));
                            RSimStates[i+2*Ny]  = real(u.cmplx(mx,i,mz,2));
                            RSimStates[i+3*Ny]  = real(q.cmplx(mx,i,mz,0));
                            ISimStates[i]       = imag(u.cmplx(mx,i,mz,0));
                            ISimStates[i+Ny]    = imag(u.cmplx(mx,i,mz,1));
                            ISimStates[i+2*Ny]  = imag(u.cmplx(mx,i,mz,2));
                            ISimStates[i+3*Ny]  = imag(q.cmplx(mx,i,mz,0));
                        }

                        u.makeState(Spectral, Spectral);
                        q.makeState(Spectral, Spectral);
                    }

                    // Collect real and imaginary sim states in one vector
                    double SimStates[2 * numSStates];
                    for (int i = 0; i < numSStates; ++i)
                    {
                        SimStates[i] = RSimStates[i];
                    }
                    for (int i = numSStates; i < 2 * numSStates; ++i)
                    {
                        SimStates[i] = ISimStates[i - numSStates];
                    }

                    // Multiply state vector by Cs to get sim output/controller input
                    double* uk;   // Controller input vector
                    double* yk;   // Controller output vector
                    yk = new double[4];
                    uk = new double[numInputs];

                    uk = MatVec(Cs, SimStates, numInputs, 2 * numSStates);

                    ////////////////////////////////////////////////////////////////
                    // Integrate controller forward in time USING CRANK-NICOLSON

                    // Calculate alpha and beta
                    // alpha = I-delT/2*A, beta = I+delT/2*A
                    double** alpha; //alpha matrix
                    alpha = new double*[numCStates];
                    for (int i = 0; i < numCStates; ++i)
                    {
                        alpha[i] = new double[numCStates];
                    }

                    double** beta; // beta matrix
                    beta = new double*[numCStates];
                    for (int i = 0; i < numCStates; ++i){
                        beta[i] = new double[numCStates];
                    }

                    double** eyeA = eyeF(numCStates); // Identity matrix

                    for (int i = 0; i < numCStates; ++i){
                        for (int j = 0; j < numCStates; ++j){
                            alpha[i][j] = eyeA[i][j] - ((dT / 2) * Ka[i][j]);
                            beta[i][j] = eyeA[i][j] + ((dT / 2) * Ka[i][j]);
                        }
                    }

                    // Calculate beta*Xk, Bk*uk and Gam
                    double* bXk; // beta*Xk
                    double* Gam; // bXk + dt*Bkuk
                    double* BkUk; // Bk*uk

                    Gam = new double[numCStates];

                    bXk = MatVec(beta,CStates,numCStates,numCStates);
                    BkUk = MatVec(Kb,uk,numCStates,numInputs);

                    // Calculate gamma
                    for (int i = 0; i < numCStates; ++i){
                        Gam[i] = bXk[i] + (dT * BkUk[i]);
                    }

                    // Calculate new state vector Xk
                    gauss(alpha, Gam, numCStates); // Solves alpha*Xk=Gam
                    // Gam now equal to Xk

                    // Update controller states
                    for (int i = 0; i < numCStates; ++i){
                        CStates[i] = Gam[i];
                    }
                    ////////////////////////////////////////////////////////////////

                    // Calculate controller output
                    double* CkXk;
                    double* DkUk;

                    CkXk = MatVec(Kc,CStates,numOutputs,numCStates);
                    DkUk = MatVec(Kd,uk,numOutputs,numOutputs);
                    for (int i = 0; i < numOutputs; ++i){
                        yk[i] = CkXk[i] + DkUk[i]; // Calculate controller output
                    }

                    // Crank-Nicolson low-pass filter integration
                    double alpha_lpf = 1 + (dT / (2 * tau_));
                    double beta_lpf =  1 - (dT / (2 * tau_));

                    BC.cmplx(mx, 1, mz, uvw) =
                        (1 / alpha_lpf) * (  (beta_lpf * BC.cmplx(mx, 1, mz, uvw)) + ((dT / tau_) * (yk[0] + yk[2] * I)) );
                    BC.cmplx(mx, 0, mz, uvw) =
                        (1 / alpha_lpf) * (  (beta_lpf * BC.cmplx(mx, 0, mz, uvw)) + ((dT / tau_) * (yk[1] + yk[3] * I)) );

                    // Store updated states in state array
                    for (int i=0;i<numCStates;++i){
                        CStateMat[mx_c][mz_c][i] = CStates[i];
                    }

                    if (numInputs == 12){
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[6]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[7]; // Imag Str lower
                        IO[0][7][IOcnt]    = uk[2]; // Real Spa upper
                        IO[0][8][IOcnt]    = uk[8]; // Imag Spa upper
                        IO[0][9][IOcnt]    = uk[3]; // Real Spa lower
                        IO[0][10][IOcnt]   = uk[9]; // Imag Spa lower
                        IO[0][11][IOcnt]   = uk[4]; // Real P upper
                        IO[0][12][IOcnt]   = uk[10]; // Imag P upper
                        IO[0][13][IOcnt]   = uk[5]; // Real P lower
                        IO[0][14][IOcnt]   = uk[11]; // Imag P lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                        IO[1][7][IOcnt]    = 0.0;
                        IO[1][8][IOcnt]    = 0.0;
                        IO[1][9][IOcnt]    = 0.0;
                        IO[1][10][IOcnt]    = 0.0;
                    }
                    else if (numInputs == 8){
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[4]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[5]; // Imag Str lower
                        IO[0][7][IOcnt]    = uk[2]; // Real Spa upper
                        IO[0][8][IOcnt]    = uk[6]; // Imag Spa upper
                        IO[0][9][IOcnt]    = uk[3]; // Real Spa lower
                        IO[0][10][IOcnt]    = uk[7]; // Imag Spa lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                        IO[1][7][IOcnt]    = 0.0;
                        IO[1][8][IOcnt]    = 0.0;
                        IO[1][9][IOcnt]    = 0.0;
                        IO[1][10][IOcnt]    = 0.0;
                    }
                    else if (numInputs == 4) {
                        // Sim Output matrix
                        IO[0][0][IOcnt]    = t;
                        IO[0][1][IOcnt]    = kx;
                        IO[0][2][IOcnt]    = kz;
                        IO[0][3][IOcnt]    = uk[0]; // Real Str upper
                        IO[0][4][IOcnt]    = uk[2]; // Imag Str upper
                        IO[0][5][IOcnt]    = uk[1]; // Real Str lower
                        IO[0][6][IOcnt]    = uk[3]; // Imag Str lower

                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                    }
                    else {
                        // Sim Input matrix
                        IO[1][0][IOcnt]    = t;
                        IO[1][1][IOcnt]    = kx;
                        IO[1][2][IOcnt]    = kz;
                        IO[1][3][IOcnt]    = real(BC.cmplx(mx,1,mz,uvw));//yk[0]; // Real upper
                        IO[1][4][IOcnt]    = imag(BC.cmplx(mx,1,mz,uvw));//yk[2]; // Imag upper
                        IO[1][5][IOcnt]    = real(BC.cmplx(mx,0,mz,uvw));//yk[1]; // Real lower
                        IO[1][6][IOcnt]    = imag(BC.cmplx(mx,0,mz,uvw));//yk[3]; // Imag lower
                    }

                    IOcnt += 1;
                    for (int i=0;i<numCStates;++i){
                        delete[] Ka[i];
                        delete[] eyeA[i];
                        delete[] alpha[i];
                        delete[] beta[i];
                        delete[] Kb[i];
                    }
                    for (int i=0;i<4;++i){
                        delete[] Kc[i];
                        delete[] Kd[i];
                    }
                    for (int i=0;i<numInputs;++i){
                        delete[] Cs[i];
                    }
                    delete[] Ka;
                    delete[] Kb;
                    delete[] Kc;
                    delete[] Kd;
                    delete[] Cs;
                    delete[] alpha;
                    delete[] beta;
                    delete[] bXk;
                    delete[] BkUk;
                    delete[] Gam;
                    delete[] eyeA;
                    delete[] CkXk;
                    delete[] DkUk;
                    delete[] uk;
                    delete[] yk;
                } //fi (kx != 0 || kz != 0)
                else {
                    IO[0][0][IOcnt]    = t;
                    IO[0][1][IOcnt]    = kx;
                    IO[0][2][IOcnt]    = kz;
                    IO[0][3][IOcnt]    = 0; // Real upper
                    IO[0][4][IOcnt]    = 0; // Imag upper
                    IO[0][5][IOcnt]    = 0; // Real lower
                    IO[0][6][IOcnt]    = 0; // Imag lower

                    IO[1][0][IOcnt]    = t;
                    IO[1][1][IOcnt]    = kx;
                    IO[1][2][IOcnt]    = kz;
                    IO[1][3][IOcnt]    = 0; // Real upper
                    IO[1][4][IOcnt]    = 0; // Imag upper
                    IO[1][5][IOcnt]    = 0; // Real lower
                    IO[1][6][IOcnt]    = 0; // Imag lower

                    IOcnt += 1;
                }
            }
        }
        //cout << " " << endl;
        //cout << "Control applied to WNs kx="+i2s(minKx)+":"+i2s(maxKx)+", kz="+i2s(minKz)+":"+i2s(maxKz) << endl;
        cout << "C";

        for (int i = 0; i < maxMx_c; ++i)
        {
            delete[] StateInfo[i];
        }
        delete[] StateInfo;
    }

    //############################################################
    // CONTROLLER K CLASS
    // Created by Geoffroy Claisse
    // University of Southampton, 08/2018
    //############################################################

    // add controller_K constructor loading the matrices K and Cs
    // add function parameter K in the initialization field

    Controller_K::Controller_K()
        :
            _ff(*(FlowField*)nullptr),
            _Nd_C(3),
            _Nx_C(0),
            _Ny_C(0),
            _Nz_C(0),
            _Mx_C(0),
            _My_C(0),
            _Mz_C(0),
            _M_C_in(_Nd_C * _Nx_C * _Ny_C * _Nz_C),
            _M_C_in_rosse(2 * _Nd_C * _Nx_C * _Ny_C * _Mz_C),
            _M_C_out(1 * _Mx_C * 2 * _Mz_C),
            _M_C_osse( (_Nx_C * _Nz_C + _Nx_C * _Nz_C - 1 + 2) * _Ny_C),
            _M_C_rosse((_Nx_C * _Mz_C + _Mx_C * _Mz_C - 1 + 2) * _Ny_C),
            _matrix_filename(""),
            _target_baseflow_filename(""),
            _tau(0)
    {
    }

    Controller_K::Controller_K(
            FlowField& ff,
            int Nx_C,
            int Ny_C,
            int Nz_C,
            std::string matrix_filename,
            std::string target_baseflow_filename,
            double tau)
        :
            _ff(ff),
            _Nd_C(3),
            _Nx_C(Nx_C),
            _Ny_C(Ny_C),
            _Nz_C(Nz_C),
            _Mx_C(_Nx_C),
            _My_C(_Ny_C),
            _Mz_C(_Nz_C/2+1),
            _M_C_in(_Nd_C * _Nx_C * _Ny_C * _Nz_C),
            _M_C_in_rosse(2 * _Nd_C * _Nx_C * _Ny_C * _Mz_C),
            _M_C_out(1 * _Mx_C * 2 * _Mz_C),
            _M_C_osse( (_Nx_C * _Nz_C + _Nx_C * _Nz_C - 1 + 2) * _Ny_C),
            _M_C_rosse(2 * (_Nx_C * _Mz_C + _Mx_C * _Mz_C - 1 + 2) * _Ny_C),
            _matrix_filename(matrix_filename),
            _target_baseflow_filename(target_baseflow_filename),
            _tau(tau)
    {
        cout << endl << "------------------------------------" << endl;
        cout << "CONSTRUCTOR Controller_K::Controller_K " << endl;

        // LOAD REFERENCE BASEFLOW OF CONTROLLER
        //FlowField target_temp;
        if (_target_baseflow_filename != "None")
        {
            cout << "Loading target baseflow for controller" << endl;
            _target_baseflow_flowfield = FlowField(_target_baseflow_filename);
            //target_temp = FlowField(_target_baseflow_filename);
            //target_temp.makeState(Spectral, Physical);
        }
        else
        {
            //target_temp = FlowField(
            cout << "Creating null target baseflow for controller" << endl;
            _target_baseflow_flowfield = FlowField(
                _ff.Nx(), _ff.Ny(), _ff.Nz(), _ff.Nd(),
                _ff.Lx(), _ff.Lz(),
                _ff.a(), _ff.b());
            //target_temp.makeState(Spectral, Physical);
            // set to zero
            for (int i = 0; i < _target_baseflow_flowfield.Nd(); ++i) {
                for (int mx = 0; mx < _target_baseflow_flowfield.Nx(); ++mx) {
                    for (int my = 0; my < _target_baseflow_flowfield.Ny(); ++my) {
                        for (int mz = 0; mz < _target_baseflow_flowfield.Nz(); ++mz) {
                            _target_baseflow_flowfield(mx, my, mz, i) = 0;
                            //target_temp.cmplx(mx, my, mz, i) = 0;
                        }
                    }
                }
            }
        }

        // check if _target_temp dimension is same as _ff
        //if (target_temp.Lx() != _ff.Lx()
        //    || target_temp.Lz() != _ff.Lz()
        //    || target_temp.a() != _ff.a()
        //    || target_temp.b() != _ff.b())
        if (_target_baseflow_flowfield.Lx() != _ff.Lx()
            || _target_baseflow_flowfield.Lz() != _ff.Lz()
            || _target_baseflow_flowfield.a() != _ff.a()
            || _target_baseflow_flowfield.b() != _ff.b())
        {
            channelflow::cferror("In controller.Controller_K(): dimensions of target_baseflow and flowfields _ff differs.");
        }

        // interpolate _target_baseflow_flowfield to _ff dimensions.
        //_target_baseflow_flowfield = FlowField(
        //        _ff.Nx(), _ff.Ny(), _ff.Nz(), _ff.Nd(),
        //        target_temp.Lx(), target_temp.Lz(),
        //        target_temp.a(), target_temp.b());
        //_target_baseflow_flowfield.interpolate(target_temp);
        //FlowField _target_baseflow_flowfield = FlowField(target_temp);

        forcing = FlowField(_Nx_C, 2, _Nz_C, 1,
                        _target_baseflow_flowfield.Lx(),
                        _target_baseflow_flowfield.Lz(),
                        _target_baseflow_flowfield.a(),
                        _target_baseflow_flowfield.b(), Spectral, Physical);

        cout << "--------------------" << endl;
        cout << "TARGET BASEFLOW : " << endl;
        cout << "Nx     : " << _target_baseflow_flowfield.Nx() << endl;
        cout << "Ny     : " << _target_baseflow_flowfield.Ny() << endl;
        cout << "Nz     : " << _target_baseflow_flowfield.Nz() << endl;
        cout << "Nd     : " << _target_baseflow_flowfield.Nd() << endl;
        cout << "Mx     : " << _target_baseflow_flowfield.Mx() << endl;
        cout << "My     : " << _target_baseflow_flowfield.My() << endl;
        cout << "Mz     : " << _target_baseflow_flowfield.Mz() << endl;
        cout << "Lx     : " << _target_baseflow_flowfield.Lx() << endl;
        cout << "Lz     : " << _target_baseflow_flowfield.Lz() << endl;
        cout << "a      : " << _target_baseflow_flowfield.a() << endl;
        cout << "b      : " << _target_baseflow_flowfield.b() << endl;
        cout << "L2Norm : " << L2Norm(_target_baseflow_flowfield) << endl;
        cout << "_M_C_in    : " << _M_C_in << endl;
        cout << "_M_C_out   : " << _M_C_out << endl;
        cout << "_M_C_osse  : " << _M_C_osse << endl;
        cout << "_M_C_rosse : " << _M_C_rosse << endl;
        cout << "--------------------" << endl << endl;

        //target_temp.makeState(Spectral, Physical);
        _target_baseflow_flowfield.makeState(Spectral, Spectral);

        // interpolated target baseflow is the same as original target baseflow: CHECKED with L2Dist
        // cout << L2Dist(_target_baseflow_flowfield, target_temp);
    }

    // NOT IMPLEMENTED
    void Controller_K::advance_Con(
            FlowField& u,
            FlowField& p,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        cout << "controller.cpp: Controller_K::advance_Con" << endl;
        throw NotImplementedException();
    }

    // NOT IMPLEMENTED
    void Controller_K::advance_Con_CON(
            FlowField& u,
            FlowField& p,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        cout << "controller.cpp: Controller_K::advance_Con_CON" << endl;
        throw NotImplementedException();
    }

    void Controller_K::FlowField_to_ff_eigenVector(
            FlowField& ff,
            Eigen::VectorXcd& ff_eigenVector,
            bool mirror)
    {
        // convert a flowfield uff with only positive kz mode,
        // as stored in channelflow, of size
        //      (_Nd_C * _Mx_C * _My_C * _Mz_C),
        //
        // into...
        //
        // if mirror = true:
        // ...an eigenvector corresponding to a flowfield
        // with all kz mode, as stored in OSSE project, of size
        //      (_Nd_C * _Mx_C * _My_C * _Nz_C),
        //
        // if mirror = false:
        // ...an eigenvector corresponding to a flowfield
        // with only positive kz modes
        // as stored in channelflow, of size
        //      (_Nd_C * _Mx_C * _My_C * _Mz_C),

        // save state
        fieldstate ff_xzstate = ff.xzstate();
        fieldstate ff_ystate = ff.ystate();

        // insure that ff is spectral
        ff.makeState(Spectral, Physical);

        int Mx = ff.Mx();
        //int My = ff.My();
        int Mz = ff.Mz();
        int Nd = ff.Nd();


        int Nx = ff.Nx();
        int Ny = ff.Ny();
        int Nz = ff.Nz();
        //cout << "Ny : " << Ny << endl;

        int size;
        if (mirror) {
            size = Mx * Ny * Nz * Nd;
        } else {
            size = Mx * Ny * Mz * Nd;
        }

        // Check if dimensions fit
        if (size != ff_eigenVector.size())
        {
            cout << "Controller_K::FlowField_to_ff_eigenVector" << endl;
            cout << "MODAL DIMENSIONS DIFFERS" << endl;
            cout << " Expected : " << size << " / M_eigenvector : " << ff_eigenVector.size() << endl;
        }

        //TODO: would it be easier/faster to concatenate all the columns of the flowfield instead of using for-loops

        int kx = 0;
        int kz = 0;
        std::vector<int> freq_X = Controller_K::numpy_fftfreq(Nx, 1/Nx);
        std::vector<int> freq_Z = Controller_K::numpy_fftfreq(Nz, 1/Nz);

        if (mirror) {
            for (int i = 0; i < Nd; ++i) {
                for (int i_kx = 0; i_kx < Nx; ++i_kx) {
                    kx = freq_X[i_kx];
                    for (int i_kz = 0; i_kz < Nz; ++i_kz) {
                        kz = freq_Z[i_kz];
                        for (int my = 0; my < Ny; ++my) {
                            if (kx < 0 && kz < 0) {
                                    ff_eigenVector[i * (Mx*Ny*Nz) + i_kx * (Ny*Nz) + i_kz * Ny + my]
                                        = std::conj(ff.cmplx(-kx, my, -kz, i));
                            } else if (kx > 0 && kz < 0) {
                                    ff_eigenVector[i * (Mx*Ny*Nz) + i_kx * (Ny*Nz) + i_kz * Ny + my]
                                        = std::conj(ff.cmplx(Nx-kx, my, -kz, i));
                                        // it seems that ff.cmplx does not like taking -kx and -kz
                                        // for this case ? I dont understand why, but it shifts
                                        // the data of an index...
                            } else if (kx == 0 && kz < 0) {
                                    ff_eigenVector[i * (Mx*Ny*Nz) + i_kx * (Ny*Nz) + i_kz * Ny + my]
                                        = std::conj(ff.cmplx(kx, my, -kz, i));
                            } else {
                                ff_eigenVector[i * (Mx*Ny*Nz) + i_kx * (Ny*Nz) + i_kz * Ny + my]
                                    = ff.cmplx(i_kx, my, kz, i);
                            }
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < Nd; ++i) {
                for (int mx = 0; mx < Mx; ++mx) {
                    for (int my = 0; my < Ny; ++my) {
                        for (int mz = 0; mz < Mz; ++mz) {
                            //int mx = ff.mx(kx);
                            //int mz = ff.mz(kz);
                            ff_eigenVector[i * (Mx*Ny*Mz) + mx * (Ny*Mz) + mz * Ny + my] = ff.cmplx(mx, my, mz, i);
                        }
                    }
                }
            }
        }

        ff.makeState(ff_xzstate, ff_ystate);
    }

    // mirror if the eigenVector is mirrored
    // the flowfield ff can never be mirrored
    void Controller_K::ff_eigenVector_to_FlowField(
            Eigen::VectorXcd& ff_eigenVector,
            FlowField& ff,
            bool mirror)
    {
        // save state
        fieldstate ff_xzstate = ff.xzstate();
        fieldstate ff_ystate = ff.ystate();

        // insure that ff is spectral
        ff.makeState(Spectral, Physical);

        int Mx = ff.Mx();
        int My = ff.My();
        int Mz = ff.Mz();
        int Nd = ff.Nd();

        int Nz = ff.Nz();

        int size;
        if (mirror) {
            size = Mx * My * Nz * Nd;
        } else {
            size = Mx * My * Mz * Nd;
        }

        // Check if dimensions fit
        if (size != ff_eigenVector.size())
        {
            cout << "Controller_K::ff_eigenVector_to_FlowField" << endl;
            cout << "MODAL DIMENSIONS DIFFERS" << endl;
            cout << " M : " << size << " / M_eigenvector : " << ff_eigenVector.size() << endl;
        }

        // TODO Check if the passing order is right
        for (int i = 0; i < Nd; ++i) {
            for (int mx = 0; mx < Mx; ++mx) {
                for (int mz = 0; mz < Mz; ++mz) {
                    // TODO DEFINE KZ HERE
                    for (int my = 0; my < My; ++my) {
                        if (mirror) {
                            ff.cmplx(mx, my, mz, i) = ff_eigenVector[i * (Mx*My*Nz) + mx * (My*Nz) + mz * My + my];
                        } else {
                            ff.cmplx(mx, my, mz, i) = ff_eigenVector[i * (Mx*My*Mz) + mx * (My*Mz) + mz * My + my];
                        }
                    }
                    // the other half part of the ff_eigenVector is not taken
                    // as it corresponds to the kz negative
                    // equal to complex conjugate of the kz positive
                }
            }
        }

        ff.makeState(ff_xzstate, ff_ystate);
    }

    void Controller_K::state_OSSE_to_state_ROSSE(
            Eigen::VectorXcd& state_OSSE,
            Eigen::VectorXd& state_ROSSE)
    {
        // transform a state vector as in the OSSE model
        // [v, eta, u00, w00]
        // into a vector as in the ROSSE model
        // [v.real, eta.real, u00.real, w00.real,
        //      v.imag, eta.imag, u00.imag, w00.imag]
        //
        // validate in Controller_ROSSE::test0

        int M_half = _M_C_rosse / 2;  // half dimension of ROSSE state vector

        // Check if dimensions fit
        if (_M_C_osse != state_OSSE.size())
        {
            cout << "MODAL DIMENSIONS DIFFERS FOR STATE_OSSE" << endl;
            cout << " Expected : " << _M_C_osse << " / M_state_OSSE : " << state_OSSE.size() << endl;
        }
        if (_M_C_rosse != state_ROSSE.size())
        {
            cout << "MODAL DIMENSIONS DIFFERS FOR STATE_ROSSE" << endl;
            cout << " Expected : " << _M_C_rosse << " / M_state_ROSSE : " << state_ROSSE.size() << endl;
        }

        // for v and eta
        for (int mx = 0; mx < _Mx_C; ++mx) {
            for (int my = 0; my < _Ny_C; ++my) {
                for (int mz = 0; mz < _Mz_C; ++mz) {
                    // for OSSE -> Nz
                    // for ROSSE -> Mz

                    // u
                    state_ROSSE[(mx * _Mz_C + mz) * _Ny_C + my]
                        = state_OSSE[(mx * _Nz_C + mz) * _Ny_C + my].real();
                    state_ROSSE[M_half + (mx * _Mz_C + mz) * _Ny_C + my]
                        = state_OSSE[(mx * _Nz_C + mz) * _Ny_C + my].imag();

                    // eta
                    if (!(mx == 0 && mz == 0)) {
                        state_ROSSE[(_Mx_C * _Mz_C + mx * _Mz_C + mz - 1) * _Ny_C + my]
                            = state_OSSE[(_Mx_C * _Nz_C + mx * _Nz_C + mz - 1) * _Ny_C + my].real();
                        state_ROSSE[M_half + (_Mx_C * _Mz_C + mx * _Mz_C + mz - 1) * _Ny_C + my]
                            = state_OSSE[(_Mx_C * _Nz_C + mx * _Nz_C + mz - 1) * _Ny_C + my].imag();
                    }
                }
            }
        }

        // for u00 and w00
        for (int my = 0; my < _Ny_C; ++my) {
            // u00
            state_ROSSE[(_Mx_C * _Mz_C + _Mx_C * _Mz_C - 1) * _Ny_C + my]
                = state_OSSE[(_Mx_C * _Nz_C + _Mx_C * _Nz_C - 1) * _Ny_C + my].real();
            state_ROSSE[M_half + (_Mx_C * _Mz_C + _Mx_C * _Mz_C - 1) * _Ny_C + my]
                = state_OSSE[(_Mx_C * _Nz_C + _Mx_C * _Nz_C - 1) * _Ny_C + my].imag();

            // w00
            state_ROSSE[(_Mx_C * _Mz_C + _Mx_C * _Mz_C - 1 + 1) * _Ny_C + my]
                = state_OSSE[(_Mx_C * _Nz_C + _Mx_C * _Nz_C - 1 + 1) * _Ny_C + my].real();
            state_ROSSE[M_half + (_Mx_C * _Mz_C + _Mx_C * _Mz_C - 1 + 1) * _Ny_C + my]
                = state_OSSE[(_Mx_C * _Nz_C + _Mx_C * _Nz_C - 1 + 1) * _Ny_C + my].imag();
        }
    }

    void Controller_K::state_ROSSE_to_state_OSSE(
            Eigen::VectorXd& state_ROSSE,
            Eigen::VectorXcd& state_OSSE)
    {
        // transform a state vector as in the ROSSE model
        // [v.real, eta.real, u00.real, w00.real,
        //      v.imag, eta.imag, u00.imag, w00.imag]
        // into a vector as in the OSSE model
        // [v, eta, u00, w00]
        //
        // validate in Controller_ROSSE::test0

        int M_half = _M_C_rosse / 2;  // half dimension of ROSSE state vector

        // Check if dimensions fit
        if (_M_C_rosse != state_ROSSE.size())
        {
            cout << "MODAL DIMENSIONS DIFFERS FOR STATE_ROSSE" << endl;
            cout << " Expected : " << _M_C_rosse << " / M_state_ROSSE : " << state_ROSSE.size() << endl;
        }
        if (_M_C_osse != state_OSSE.size())
        {
            cout << "MODAL DIMENSIONS DIFFERS FOR STATE_OSSE" << endl;
            cout << " Expected : " << _M_C_osse << " / M_state_OSSE : " << state_OSSE.size() << endl;
        }

        int kx, kz;
        int kx3, kz3;
        int i_kx3, i_kz3;
        double sign;
        int i_osse, i_rosse;
        std::vector<int> freq_X = Controller_K::numpy_fftfreq(_Nx_C, 1/_Nx_C);
        std::vector<int> freq_Z = Controller_K::numpy_fftfreq(_Nz_C, 1/_Nz_C);

        // for v and eta
        for (int i_kx = 0; i_kx < _Nx_C; ++i_kx) {
            kx = freq_X[i_kx];
            for (int i_kz = 0; i_kz < _Nz_C; ++i_kz) {
                kz = freq_Z[i_kz];

                // For precision on these indices, see
                // OSSE project, file operators.py,
                // method Rosse_nonlaminar_baseflow()
                // and read where comment about the for-loops
                // of kx and kz.
                // This loop actually apply the complex-conjugacy between modes.
                //
                // note on math:
                // this is a long and less robust way to do :
                //i_kx3 = (kx3 % _Nx_C + _Nx_C) % _Nx_C;
                //i_kz3 = (kz3 % _Nz_C + _Nz_C) % _Nz_C;
                // but it doesnt work as I expect it to do
                if (kz >= 0) {
                    if (kx == 0) {
                        kx3 = 0;
                        kz3 = kz;
                        i_kx3 = 0;
                        i_kz3 = kz3;
                    } else if (kx > 0) {
                        kx3 = + kx;
                        kz3 = + kz;
                        i_kx3 = kx;
                        i_kz3 = kz;
                    } else {
                        kx3 = + kx;
                        kz3 = + kz;
                        i_kx3 = i_kx;
                        i_kz3 = i_kz;
                    }
                    sign = +1;
                } else {
                    if (kx == 0 ) {
                        kx3 = 0;
                        kz3 = - kz;
                        i_kx3 = 0;
                        i_kz3 = kz3;
                    } else if (kx > 0) {
                        kx3 = - kx;
                        kz3 = - kz;
                        i_kx3 = kx3 + _Nx_C;
                        i_kz3 = kz3;
                    } else {
                        kx3 = - kx;
                        kz3 = - kz;
                        i_kx3 = kx3;
                        i_kz3 = kz3;
                    }
                    sign = -1;
                }

                // for OSSE -> Nz
                // for ROSSE -> Mz
                i_osse = i_kx * _Nz_C + i_kz;
                i_rosse = i_kx3 * _Mz_C + i_kz3;

                /*
                if (i_kz == 0) { cout << endl;}
                cout << "----------" << endl;
                cout << "i_kx  = " << i_kx  << " / kx  = " << kx << endl;
                cout << "i_kz  = " << i_kz  << " / kz  = " << kz << endl;
                cout << "i_kx3 = " << i_kx3 << " / kx3 = " << kx3 << endl;
                cout << "i_kz3 = " << i_kz3 << " / kz3 = " << kz3 << endl;
                cout << "i_osse = " << i_osse << endl;
                cout << "i_rosse = " << i_rosse << endl;
                //*/

                for (int my = 0; my < _Ny_C; ++my) {
                    state_OSSE[i_osse * _Ny_C + my] =
                        state_ROSSE[i_rosse * _Ny_C + my]
                        + sign * 1i * state_ROSSE[M_half + i_rosse * _Ny_C + my];

                    // eta
                    if (!(kx == 0 && kz == 0)) {
                        state_OSSE[(_Mx_C * _Nz_C + i_osse - 1) * _Ny_C + my] =
                            state_ROSSE[(_Mx_C * _Mz_C + i_rosse - 1) * _Ny_C + my]
                            + sign * 1i * state_ROSSE[M_half + (_Mx_C * _Mz_C + i_rosse - 1) * _Ny_C + my];
                    }
                }
            }
        }

        // for u00 and w00
        for (int my = 0; my < _Ny_C; ++my) {
            // u00
            state_OSSE[(_Mx_C * _Nz_C + _Mx_C * _Nz_C - 1) * _Ny_C + my] =
                state_ROSSE[(_Mx_C * _Mz_C + _Mx_C * _Mz_C - 1) * _Ny_C + my]
                + 1i * state_ROSSE[M_half + (_Mx_C * _Mz_C + _Mx_C * _Mz_C - 1) * _Ny_C + my];

            // w00
            state_OSSE[(_Mx_C * _Nz_C + _Mx_C * _Nz_C - 1 + 1) * _Ny_C + my] =
                state_ROSSE[(_Mx_C * _Mz_C + _Mx_C * _Mz_C - 1 + 1) * _Ny_C + my]
                + 1i * state_ROSSE[M_half + (_Mx_C * _Mz_C + _Mx_C * _Mz_C - 1 + 1) * _Ny_C + my];
        }
    }

    void Controller_K::substract_ff(FlowField& ff, FlowField& ff_to_substract)
    {
        // This method firstly interpolate ff_to_substract
        // into the dimensions of ff to make the difference.
        // This interpolation is not necessary and the difference
        // could be made directly.
        // However, I prefered doing so as in this way I am sure
        // there is no problem of indexing of chebytransform,
        // as interplate cares about that.
        fieldstate ffxzstate = ff.xzstate();
        fieldstate ffystate = ff.ystate();
        fieldstate ffsubxzstate = ff_to_substract.xzstate();
        fieldstate ffsubystate = ff_to_substract.ystate();

        ff.makeState(Spectral, Physical);
        ff_to_substract.makeState(Spectral, Physical);

        if (ff.Nd() != ff_to_substract.Nd())
        {
            channelflow::cferror("In controller.Controller_K.substract_ff: dimensions Nd of flowfields differs.");
        }

        FlowField ff_to_substract_fitted;

        if (ff.Nx() != ff_to_substract.Nx()
                || ff.Ny() != ff_to_substract.Ny()
                || ff.Nz() != ff_to_substract.Nz())
        {
            ff_to_substract_fitted = FlowField(
                    ff.Nx(), ff.Ny(), ff.Nz(), ff.Nd(),
                    ff_to_substract.Lx(), ff_to_substract.Lz(),
                    ff_to_substract.a(), ff_to_substract.b(), Spectral, Spectral);

            // method interpolate() check if Lx, Lz, a and b are equal
            ff_to_substract_fitted.interpolate(ff_to_substract);
        }
        else {
            ff_to_substract_fitted = FlowField(ff_to_substract);
            ff_to_substract_fitted.makeState(Spectral, Physical);
        }

        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mx = 0; mx < ff.Mx(); ++mx) {
                for (int my = 0; my < ff.My(); ++my) {
                    for (int mz = 0; mz < ff.Mz(); ++mz) {
                        ff.cmplx(mx, my, mz, i) -= ff_to_substract_fitted.cmplx(mx, my, mz, i);
                    }
                }
            }
        }

        ff.makeState(ffxzstate, ffystate);
        ff_to_substract.makeState(ffsubxzstate, ffsubystate);
    }

    void Controller_K::substract_ff2(FlowField& ff, FlowField& ff_to_substract)
    {
        // This method firstly interpolate ff_to_substract
        // into the dimensions of ff to make the difference.
        // This interpolation is not necessary and the difference
        // could be made directly.
        // However, I prefered doing so as in this way I am sure
        // there is no problem of indexing of chebytransform,
        // as interplate cares about that.
        fieldstate ffxzstate = ff.xzstate();
        fieldstate ffystate = ff.ystate();
        fieldstate ffsubxzstate = ff_to_substract.xzstate();
        fieldstate ffsubystate = ff_to_substract.ystate();

        ff.makeState(Physical, Physical);
        ff_to_substract.makeState(Physical, Physical);

        if (ff.Nd() != ff_to_substract.Nd())
        {
            channelflow::cferror("In controller.Controller_K.substract_ff: dimensions Nd of flowfields differs.");
        }

        FlowField ff_to_substract_fitted;

        if (ff.Nx() != ff_to_substract.Nx()
                || ff.Ny() != ff_to_substract.Ny()
                || ff.Nz() != ff_to_substract.Nz())
        {
            cout << "INTERPOLATION" << endl;
            ff_to_substract_fitted = FlowField(
                    ff.Nx(), ff.Ny(), ff.Nz(), ff.Nd(),
                    ff_to_substract.Lx(), ff_to_substract.Lz(),
                    ff_to_substract.a(), ff_to_substract.b(), Spectral, Spectral);

            // method interpolate() check if Lx, Lz, a and b are equal
            ff_to_substract_fitted.interpolate(ff_to_substract);
        }
        else {
            ff_to_substract_fitted = FlowField(ff_to_substract);
            ff_to_substract_fitted.makeState(Physical, Physical);
        }

        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mx = 0; mx < ff.Nx(); ++mx) {
                for (int my = 0; my < ff.Ny(); ++my) {
                    for (int mz = 0; mz < ff.Nz(); ++mz) {
                        ff(mx, my, mz, i) -= ff_to_substract_fitted(mx, my, mz, i);
                    }
                }
            }
        }

        ff.makeState(ffxzstate, ffystate);
        ff_to_substract.makeState(ffsubxzstate, ffsubystate);
    }

    void Controller_K::add_ff(FlowField& ff, FlowField& ff_to_add)
    {
        // This method firstly interpolate ff_to_add
        // into the dimensions of ff to make the addition.
        // This interpolation is not necessary and the difference
        // could be made directly.
        // However, I prefered doing so as in this way I am sure
        // there is no problem of indexing of chebytransform,
        // as interplate cares about that.
        fieldstate ffxzstate = ff.xzstate();
        fieldstate ffystate = ff.ystate();
        fieldstate ffsubxzstate = ff_to_add.xzstate();
        fieldstate ffsubystate = ff_to_add.ystate();

        ff.makeState(Spectral, Physical);
        ff_to_add.makeState(Spectral, Physical);

        if (ff.Nd() != ff_to_add.Nd())
        {
            channelflow::cferror("In controller.Controller_K.add_ff: dimensions Nd of flowfields differs.");
        }

        FlowField ff_to_add_fitted;

        if (ff.Nx() != ff_to_add.Nx()
                || ff.Ny() != ff_to_add.Ny()
                || ff.Nz() != ff_to_add.Nz())
        {
            ff_to_add_fitted = FlowField(
                    ff.Nx(), ff.Ny(), ff.Nz(), ff.Nd(),
                    ff_to_add.Lx(), ff_to_add.Lz(),
                    ff_to_add.a(), ff_to_add.b(), Spectral, Spectral);

            // method interpolate() check if Lx, Lz, a and b are equal
            ff_to_add_fitted.interpolate(ff_to_add);
        }
        else {
            ff_to_add_fitted = FlowField(ff_to_add);
            ff_to_add_fitted.makeState(Spectral, Physical);
        }

        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mx = 0; mx < ff.Mx(); ++mx) {
                for (int my = 0; my < ff.My(); ++my) {
                    for (int mz = 0; mz < ff.Mz(); ++mz) {
                        ff.cmplx(mx, my, mz, i) += ff_to_add_fitted.cmplx(mx, my, mz, i);
                    }
                }
            }
        }

        ff.makeState(ffxzstate, ffystate);
        ff_to_add.makeState(ffsubxzstate, ffsubystate);
    }

    void Controller_K::K_changegrid(FlowField& u1, FlowField& u0)
    {
        /*
         * taken from /channelflow/trunk/programs/changegrid.cpp
         * interpolate u0 into u1
         * does not padded
         */
        u1.interpolate(u0);

        // fixing divergenc is TRUE for changegrid program,
        // but what does it mean ?
        // to get same result at with changegrid, it need to be true
        // also, it is necessary to get same controller output
        // for different resolutions (tested with 17 and 32)
        //
        if (true)
        {
            //cout <<"fixing divergence..." << endl;
            //cout << "divnorm u0 : " << divNorm(u0) << endl;
            //cout << "divnorm u1 : " << divNorm(u1) << endl;

            Vector v;
            FlowField foo(u1.Nx(), u1.Ny(), u1.Nz(), u0.Nd(), u0.Lx(), u0.Lz(), u0.a(), u0.b(), Spectral, Spectral);
            field2vector(u1, v);
            vector2field(v, foo);
            //cout << "L2Dist(u1,2)== " << L2Dist(u1,foo) << endl;
            u1 = foo;
            //cout << "L2Norm(u2)  == " << L2Norm(u1) << endl;
            //cout << "divNorm(u2) == " << divNorm(u1) << endl;
            //cout << "bcNorm(u2)  == " << bcNorm(u1) << endl;

            //cout << "divnorm u0 : " << divNorm(u0) << endl;
            //cout << "divnorm u1 : " << divNorm(u1) << endl << endl;
        }
    }

    std::vector<int> Controller_K::numpy_fftfreq(int n, double d)
    {
        // from numpy.fft.fftfreq source code
        // on github: numpy/numpy/fft/helper.py/fftfreq

        //double val = 1.0 / (n*d);
        //I consider val = 1

        int N = (n-1)/2 + 1;

        std::vector<int> p1;
        for(int i = 0; i < N; ++i){
            p1.push_back(i);
        }
        for(int i = - n/2; i < 0; ++i) {
            p1.push_back(i);
        }
        return p1;
    }

    /*
    void Controller_K::upsize_xz(FlowField& u_in, FlowField& u_out)
    {
        //u_in.Mx() and u_in.Mz() are smaller than u_out

        // save state
        fieldstate u_in_xzstate = u_in.xzstate();
        fieldstate u_in_ystate = u_in.ystate();
        fieldstate u_out_xzstate = u_out.xzstate();
        fieldstate u_out_ystate = u_out.ystate();

        // insure that u_in and u_out are spectral
        u_in.makeState(Spectral, Physical);
        u_out.makeState(Spectral, Physical);

        int Mx = u_in.Mx();
        int My = u_in.My();
        int Mz = u_in.Mz();
        int Nd = u_in.Nd();
        int M = Mx * My * Mz * Nd;

        int Nd_out = u_out.Nd();
        int My_out = u_out.My();
         // Check if dimensions Nd and Ny fit
        if (My != My_out || Nd != Nd_out)
        {
            cout << "DIMENSIONS DIFFERS" << endl;
            cout << " My : " << My << " / My_out : " << My_out << endl;
            cout << " Nd : " << Nd << " / Nd_out : " << Nd_out << endl;
        }

        int mx_inloop;
        for (int i = 0; i < Nd; ++i) {
            for (int mx = 0; mx < Mx; ++mx) {
                mx_inloop = u_out.mx(u_in.kx(mx));
                //cout << "mx : " << mx << " / u_in.kx : " << u_in.kx(mx) << " / mx_inloop " << mx_inloop << endl;
                for (int my = 0; my < My; ++my) {
                    for (int mz = 0; mz < Mz; ++mz) {
                        u_out.cmplx(mx_inloop, my, mz, i) = u_in.cmplx(mx, my, mz, i);
                    }
                }
            }
        }

        u_in.makeState(u_in_xzstate, u_in_ystate);
        u_out.makeState(u_out_xzstate, u_out_ystate);
    }

    void Controller_K::downsize_xz(FlowField& u_in, FlowField& u_out)
    {
        //u_in.Mx() and u_in.Mz() are bigger than u_out

        // save state
        fieldstate u_in_xzstate = u_in.xzstate();
        fieldstate u_in_ystate = u_in.ystate();
        fieldstate u_out_xzstate = u_out.xzstate();
        fieldstate u_out_ystate = u_out.ystate();

        // insure that u_in and u_out are spectral
        u_in.makeState(Spectral, Physical);
        u_out.makeState(Spectral, Physical);

        int Mx = u_out.Mx();
        int My = u_out.My();
        int Mz = u_out.Mz();
        int Nd = u_out.Nd();
        int M = Mx * My * Mz * Nd;

        int Nd_in = u_in.Nd();
        int My_in = u_in.My();
         // Check if dimensions Nd and Ny fit
        if (My != My_in || Nd != Nd_in)
        {
            cout << "DIMENSIONS DIFFERS" << endl;
            cout << " My : " << My << " / My_in : " << My_in << endl;
            cout << " Nd : " << Nd << " / Nd_in : " << Nd_in << endl;
        }

        int mx_inloop;
        for (int i = 0; i < Nd; ++i) {
            for (int mx = 0; mx < Mx; ++mx) {
                mx_inloop = u_in.mx(u_out.kx(mx));
                //cout << "mx : " << mx << " / u_out.kx : " << u_out.kx(mx) << " / mx_inloop " << mx_inloop << endl;
                for (int my = 0; my < My; ++my) {
                    for (int mz = 0; mz < Mz; ++mz) {
                        u_out.cmplx(mx, my, mz, i) = u_in.cmplx(mx_inloop, my, mz, i);
                    }
                }
            }
        }

        u_in.makeState(u_in_xzstate, u_in_ystate);
        u_out.makeState(u_out_xzstate, u_out_ystate);
    }

    void Controller_K::downsize_xyz(FlowField& u_in, FlowField& u_out)
    {
        //u_in.Mx() and u_in.Mz() are bigger than u_out
        //u_in.My() is bigger than u_out.My()

        // save state
        fieldstate u_in_xzstate = u_in.xzstate();
        fieldstate u_in_ystate = u_in.ystate();
        fieldstate u_out_xzstate = u_out.xzstate();
        fieldstate u_out_ystate = u_out.ystate();

        // insure that u_in and u_out are spectral
        u_in.makeState(Spectral, Physical);
        u_out.makeState(Spectral, Physical);

        int Mx = u_out.Mx();
        int My = u_out.My();
        int Mz = u_out.Mz();
        int Nd = u_out.Nd();
        int M = Mx * My * Mz * Nd;

        int My_in = u_in.My();
        int Nd_in = u_in.Nd();
         // Check if dimension Nd fits
        if (Nd != Nd_in)
        {
            cout << "DIMENSIONS DIFFERS" << endl;
            cout << " Nd : " << Nd << " / Nd_in : " << Nd_in << endl;
        }

        FlowField temp = FlowField(u_out.Nx(), u_in.Ny(), u_out.Nz(), u_out.Nd(), u_out.Lx(), u_out.Lz(), u_out.a(), u_out.b(), Spectral, Spectral);

        int mx_inloop;
        for (int i = 0; i < Nd; ++i) {
            for (int mx = 0; mx < Mx; ++mx) {
                mx_inloop = u_in.mx(temp.kx(mx));
                //cout << "mx : " << mx << " / u_out.kx : " << temp.kx(mx) << " / mx_inloop " << mx_inloop << endl;
                for (int my = 0; my < My_in; ++my) {
                    for (int mz = 0; mz < Mz; ++mz) {
                        temp.cmplx(mx, my, mz, i) = u_in.cmplx(mx_inloop, my, mz, i);
                    }
                }
            }
        }
        // change Ny from temp to u_out
        u_out.interpolate(temp);

        u_in.makeState(u_in_xzstate, u_in_ystate);
        u_out.makeState(u_out_xzstate, u_out_ystate);
    }


    void Controller_K::changegrid(FlowField& u_in, FlowField& u_out, bool padded)
    {
        // from channelflow/trunk/program/changegrid.cpp

        const int Nx = u_out.Nx();
        const int Ny = u_out.Ny();
        const int Nz = u_out.Nz();

        // interpolate cares about state xz and y transformation
        u_out.interpolate(u_in);

        if (u_out.Lx() != u_in.Lx() || u_out.Lz() != u_in.Lz())
            u_out.rescale(u_in.Lx(), u_in.Lz());

        if (u_in.Nd() == 3) {
            if (padded || (Nx >= (3*u_in.Nx())/2 && Nx >= (3*u_in.Nz())/2))
                u_out.zeroPaddedModes();
        }
    }
    */

    //############################################################
    // CONTROLLER ROSSE CLASS
    // Created by Geoffroy Claisse
    // University of Southampton, 06/2019
    //############################################################

    Controller_ROSSE::Controller_ROSSE()
        :
            Controller_K()
    {
    }

    // TODO UPGRADE WITH NEW K_CHFL
    // it means :
    // 1. do not load CTinv anymore (already in new K_CHFL = K_ROSSE Tinv Cinv)
    // 2. define method to get x_CHFL in vector with real and imag separated, i.e. as x_rosse
    //          -> FlowField_to_eigenVector_ROSSE
    //    and reverse method to validate the previous one
    //          -> eigenVector_ROSSE_to_FlowField
    // 3. update advance_Con_CON method with appropriate method (above) and new dot product
    Controller_ROSSE::Controller_ROSSE(
            FlowField& ff,
            int Nx_C,
            int Ny_C,
            int Nz_C,
            std::string matrix_filename,
            std::string target_baseflow_filename,
            double tau)
        :
            Controller_K(ff,
                Nx_C,
                Ny_C,
                Nz_C,
                matrix_filename,
                target_baseflow_filename,
                tau)
    {
        cout << "------------------------------------" << endl;
        cout <<  "CONSTRUCTOR Controller_ROSSE::Controller_ROSSE " << endl;

        // LOAD MATRIX K for [u,v,w]
        if (_matrix_filename == "None")
            K = Eigen::MatrixXcd(_M_C_out, _M_C_in_rosse);
        else
        {
            cout << "Load matrix K: " << endl;
            cout << _matrix_filename << endl;
            K = Eigen::MatrixXcd(_M_C_out, _M_C_in_rosse);
            cout << _M_C_out << " x " <<_M_C_rosse << endl;
            MatIn_hdf5_complex(K, _matrix_filename);
        }
    }

    // TODO upgrade with new K_CHFL
    void Controller_ROSSE::advance_Con_CON(
            FlowField& u,
            FlowField& p,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        // TODO
        // mofidy K_CHFL in ROSSE by defining Tinv_ROSSE and Cinv_ROSSE,
        // and define new K_CHFL, such that it saves time at each CHFL iteration
        // (see rosse_riccati.py in osse project)
        //
        // TODO
        // interpolate the target baseflow at creation
        // instead of at each controller call
        //
        // TODO
        // memory and perf profiling
        //
        // TODO
        // pass u_reduced into the constructor so it is not allocated at every iteration
        //
        // TODO
        //TEST 2:
        //from a null flowfield, but input just on one mode, and change K such that it does identity
        //check that q is actually the right mode actuated
        //
        //TEST 1:
        // consider q_vector = zeros and replace only one mode,
        // check the effect on the boundary condition
        // check if complex value
        // try to impose a null flowfield with imposed q_vector on one mode

        // double*** CStateMat, Real t, double*** IO are not used here
        // they are used to export/import the state of the field
        // so might be useful, but not required

        cout << "R";
        //cout << "Controller_ROSSE::advance_Con_CON" << endl;

        fieldstate target_xzstate = _target_baseflow_flowfield.xzstate();
        fieldstate target_ystate = _target_baseflow_flowfield.ystate();
        //TODO : marke target_baseflow into (Spectral, Physical) ?
        _target_baseflow_flowfield.makeState(Spectral, Spectral);

        fieldstate BC_xzstate = BC.xzstate();
        fieldstate BC_ystate = BC.ystate();
        BC.makeState(Spectral, Physical);

        fieldstate u_xzstate = u.xzstate();
        fieldstate u_ystate = u.ystate();
        u.makeState(Spectral, Spectral);

        //#############################################################################################################################################
        // Extract the flowfield u and transform it into a state-vector
        //#############################################################################################################################################

        FlowField delta_u = FlowField(u);
        //cout << "L2Norm(u) in K: " << L2Norm(u) << endl;
        //cout << "State of u     : " << u.xzstate() << " / " << u.ystate() << endl;
        //cout << "State of delta : " << delta_u.xzstate() << " / " << delta_u.ystate() << endl;
        delta_u.makeState(Spectral, Spectral);
        //cout << "State of u     : " << u.xzstate() << " / " << u.ystate() << endl;
        //cout << "State of delta : " << delta_u.xzstate() << " / " << delta_u.ystate() << endl;

        // remove the baseflow form the channelflow state
        // to deal with the perturbation only
        // CHFL considers the flowfield as u_EQ + u_pert
        // while OSSE consider only the flowfield as u_pert
        // so the controller from OSSE is controlling the perturbations only,
        // meaning the distance from a given _target_baseflow_flowfield only
        Controller_K::substract_ff(delta_u, _target_baseflow_flowfield);
        //cout << "L2Norm(delta_u) in K: " << L2Norm(delta_u) << endl;

        // SUBSTRACTION VALIDATED (test0): with NoDealiasing and with DealiasXZ
        // -> NoDealiasing gives MUCH better precision if target and flowfield have same resolution

        // interpolate the channeflow velocity field u to controller dimension
        // NOTE: u and _target_baseflow_flowfield are both padded.
        // However, it is not influential as we interpolate u
        // into u_reduced of lower dimension, implying that the padding disappears.
        FlowField delta_reduced = FlowField(_Nx_C, _Ny_C, _Nz_C, u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
        Controller_K::K_changegrid(delta_reduced, delta_u);
        //delta_reduced.interpolate(delta_u);
        //cout << endl << "L2Norm of the flowfield u: " << L2Norm2(u) << endl;
        //cout << endl << "L2Norm of the flowfield target: " << L2Norm2(_target_baseflow_flowfield) << endl;
        //cout << endl << "L2Norm of the perturbation u: " << L2Norm2(u) << endl;
        //cout << endl << "L2Norm of the perturbation u reduced: " << L2Norm2(u_reduced) << endl;

        //std::cin.ignore();

        // REDUCTION AND INTERPOLATION VALIDATED (test1 and test2):
        // difference between u and u_reduced : CHECKED with L2Dist
        // difference between u_reduced and u.interpolate(u_reduced) : CHECKED with L2Dist

        // return the state of simulation u_reduced as an Eigen::VectorXcd
        // NOTE: The u_reduced_vector includes the entire set
        // of spanwise modes as "mirror = true".
        // This is necessary as OSSE spans all the spanwise modes
        // but CHFL only the positive spanwise modes, hence the "mirror".
        delta_reduced.makeState(Spectral, Physical);
        Eigen::VectorXd delta_reduced_vector_rosse = Eigen::VectorXd::Zero(_M_C_in_rosse);
        //cout << "FlowField_to_state_ROSSE_via_OSSE" << endl;
        Controller_ROSSE::FlowField_to_ff_eigenVector_ROSSE(delta_reduced, delta_reduced_vector_rosse);

        // FlowField_to_ff_eigenVector VALIDATED (test3)
        // ff_eigenVector_to_FlowField VALIDATED (test4)
        // -> get an eigenvector from a flowfield CHECKED
        // -> back transform and compare flowfield CHECKED

        // operator ff_eigenVector_to_FlowField again and verify that I get the same flowfield -> CHECKED
        /*
        FlowField u_reduced2 = FlowField(_Nx_C, _Ny_C, _Nz_C, u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Physical);
        this->ff_eigenVector_to_FlowField(
                u_reduced_vector,
                u_reduced2,
                true);
        cout << "L2Dist u_reduced, u_reduced2 : " << L2Dist(u_reduced, u_reduced2) << endl;

        Eigen::VectorXcd u_reduced_vector2 = Eigen::VectorXcd::Zero(_M_C_in);
        this->FlowField_to_ff_eigenVector(
                u_reduced2,
                u_reduced_vector2,
                true);

        Eigen::VectorXcd x_diff = Eigen::VectorXcd::Zero(_M_C_in);
        x_diff = u_reduced_vector2 - u_reduced_vector;
        cout << "u_reduced_vector2 - u_reduced_vector : " << x_diff.norm() << endl;

        // save u_reduced
        u_reduced.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced.h5");
        u_reduced2.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced2.h5");
        // save u_reduced_vector
        MatOut_hdf5_complex(u_reduced_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced_vector.h5");
        MatOut_hdf5_complex(u_reduced_vector2, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced_vector2.h5");
        std::cin.ignore();
        */

        // HERE I SHOULD GET THE EXACT SAME STATE AS IN OSSE IF THE SIMULATION IS THE SAME
        // -> CHECKED

        //#############################################################################################################################################
        // Determine forcing q [yk in Heins controller class]
        //#############################################################################################################################################

        Eigen::VectorXcd q_vector = Eigen::VectorXcd::Zero(_M_C_out);
        //cout << "    M_C_in  : " << _M_C_in << endl;
        //cout << "    M_C_out : " << _M_C_out << endl;
        //cout << "    K : " << K.rows() << " / " << K.cols() << endl;

        // the vector q_vector does only contain the positive modes in kz
        // and not the negative ones as they are not useful for later
        // calculation in channelflow
        // just bear in mind for the different dimensions, here use _Mz_C, as in _M_C_out
        // Limitating the actuation on positive mode is made in the OSSE with the method
        // operator.op_reduce_actuation_modes, so that the resulting matrix K of the
        // OSSE model is limited to actuation on the positive kz already.

        q_vector = K * delta_reduced_vector_rosse;
        // CHECKED (test5): Verify that for a given state-vector given from OSSE, I get the same q in OSSE and in CHFL

        // CHECKED: FlowField q corresponds to the actuation at the boundaries,
        // with the same _Nx_C and _Nz_C as the controller
        // but for the wall-normal velocity at the upper and lower wall, so Nd = 1 and Ny = 2

        FlowField q = FlowField(_Nx_C, 2, _Nz_C, 1, BC.Lx(), BC.Lz(), BC.a(), BC.b(), Spectral, Physical);
        //cout << "ff_eigenVector_to_FlowField" << endl;
        Controller_K::ff_eigenVector_to_FlowField(q_vector, q, false);
        //MatOut_hdf5_complex(q_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/q_vector.h5");
        //q.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/q.h5");

        // Operator FlowField_to_ff_eigenVector and verify that q is equal to q_vector: CHECKED
        // however it is hard to know if the indexation is ok if mirror = false,
        // on the transformation in and back, it worked, so i think it is validated.
        // but bear in mind that q is only for positive kz mode
        //

        //cout << "L2Norm(q) : " << L2Norm(q) << endl;
        forcing = q;
        //cout << "L2Norm(f) : " << L2Norm(forcing) << endl;

        //#############################################################################################################################################
        // UPDATE THE FLOWFIELD BC
        //#############################################################################################################################################

        if (true)
        {
            // Crank-Nicholson low-pass filter integration
            double alpha_lpf = 1 + (dT / (2 * _tau));
            double beta_lpf =  1 - (dT / (2 * _tau));

            // This for-loop should fit the BCs dimensions. However, as only the modes of the controller
            // are actually actuated, I restrict the loop to _Mx_C and _Mz_C instead of BC.Mx() and BC.Mz().
            // All the high-dimension modes are not actuated.
            int mx_inloop;
            for (int mx = 0; mx < _Mx_C; ++mx) {
                // as q is of smaller dimension than BC, we use mx_inloop to fit the right modes pairs together.
                // not a problem for mz as all the kz are positive.
                // here mx corresponds to a mode of q while mx_inloop corresponds to the same corresonding mode for BC
                mx_inloop = BC.mx(q.kx(mx));
                //cout << "mx : " << mx << " / q.kx : " << q.kx(mx) << " / mx_inloop " << mx_inloop << endl;
                for (int mz = 0; mz < _Mz_C; ++mz) {
                    //lower wall, for u, v and w, already 0 for i=0 (u) and i=2 (w)
                    BC.cmplx(mx_inloop, 0, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC.cmplx(mx_inloop, 0, mz, 1)) + ((dT / _tau) * q.cmplx(mx, 0, mz, 0)) );
                    //upper  wall, for u, v and w, alreadu 0 for i=0 (u) and i=2 (w)
                    BC.cmplx(mx_inloop, 1, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC.cmplx(mx_inloop, 1, mz, 1)) + ((dT / _tau) * q.cmplx(mx, 1, mz, 0)) );
                }
            }

            // low-pass filter integration VALIDATED (test6)

            /*
            cout << setprecision(3) << endl;
            for (int mx = 0; mx < Mx; mx++) {
                kx = BC.kx(mx);
                cout << mx << "|" << kx << "    : ";
                for (int mz = 0; mz < Mz; mz++) {
                    //mz = BC.mz(kz);
                    cout << round(real(BC.cmplx(mx, 0, mz, 1))*100)/100 << "+(" << round(imag(BC.cmplx(mx, 0, mz, 1))*100)/100 << ")i | ";
                }
                cout << endl;
            }
            cout << "END" << endl;
            exit(1);
            */
        }
        else if (false)
        {
            int T_perturbed = 10;
            double forcing_mag = 0.0005;
            int kx = +1;
            int kz = +1;
            int mx = BC.mx(kx);
            int mz = BC.mz(kz);
            //cout << 1 * sin(2*pi*(t-T_perturbed)/10) * forcing_mag << endl;
            BC.cmplx(mx, 0, mz, 1) = 1 * sin(2*pi*(t-T_perturbed)/10) * forcing_mag;
            BC.cmplx(mx, 1, mz, 1) = 1 * sin(2*pi*(t-T_perturbed)/10) * forcing_mag;
        }

        _target_baseflow_flowfield.makeState(target_xzstate, target_ystate);
        BC.makeState(BC_xzstate, BC_ystate);
        u.makeState(u_xzstate, u_ystate);
    }

    void Controller_ROSSE::FlowField_to_ff_eigenVector_ROSSE(
            FlowField& ff,
            Eigen::VectorXd& ff_eigenVector_ROSSE)
    {
        // convert a flowfield ff with only positive kz mode,
        // as stored in channelflow, of size
        //      (_Nd_C * _Mx_C * _My_C * _Mz_C),
        //
        // into an eigenvector corresponding to a flowfield
        // with only positive kz modes
        // as stored in channelflow, of size
        //      (_Nd_C * _Mx_C * _My_C * _Mz_C),
        // with real and imaginary part separated:
        // ff_eigenVector_ROSSE
        //  Eigen::VectorXd&
        //  real [u.real, v.real, w.real, u.imag, v.imag, w.imag]
        //  for kz > 0 only
        //  size 2 * _M_C_in

        // save state
        fieldstate ff_xzstate = ff.xzstate();
        fieldstate ff_ystate = ff.ystate();

        // insure that ff is spectral
        ff.makeState(Spectral, Physical);

        int Mx = ff.Mx();
        int My = ff.My();
        int Mz = ff.Mz();
        int Nd = ff.Nd();

        int Nx = ff.Nx();
        int Nz = ff.Nz();

        int M_half = Mx * My * Mz * Nd;
        int size = 2 * M_half;

        // Check if dimensions fit
        if (size != ff_eigenVector_ROSSE.size())
        {
            cout << "Controller_K::FlowField_to_ff_eigenVector_ROSSE" << endl;
            cout << "MODAL DIMENSIONS DIFFERS" << endl;
            cout << " Expected : " << size << " / M_eigenvector : " << ff_eigenVector_ROSSE.size() << endl;
        }

        //TODO: would it be easier/faster to concatenate all the columns of the flowfield instead of using for-loops

        std::vector<int> freq_X = Controller_K::numpy_fftfreq(Nx, 1/Nx);
        std::vector<int> freq_Z = Controller_K::numpy_fftfreq(Nz, 1/Nz);

        for (int i = 0; i < Nd; ++i) {
            for (int mx = 0; mx < Mx; ++mx) {
                for (int my = 0; my < My; ++my) {
                    for (int mz = 0; mz < Mz; ++mz) {
                        //int mx = ff.mx(kx);
                        //int mz = ff.mz(kz);
                        ff_eigenVector_ROSSE[i * (Mx*My*Mz) + mx * (My*Mz) + mz * My + my] = ff.cmplx(mx, my, mz, i).real();
                        ff_eigenVector_ROSSE[M_half + i * (Mx*My*Mz) + mx * (My*Mz) + mz * My + my] = ff.cmplx(mx, my, mz, i).imag();
                    }
                }
            }
        }

        ff.makeState(ff_xzstate, ff_ystate);
    }

    void Controller_ROSSE::ff_eigenVector_ROSSE_to_FlowField(
            Eigen::VectorXd& ff_eigenVector_ROSSE,
            FlowField& ff)
    {
        // save state
        fieldstate ff_xzstate = ff.xzstate();
        fieldstate ff_ystate = ff.ystate();

        // insure that ff is spectral
        ff.makeState(Spectral, Physical);

        int Mx = ff.Mx();
        int My = ff.My();
        int Mz = ff.Mz();
        int Nd = ff.Nd();

        int M_half = Mx * My * Mz * Nd;
        int size = 2 * M_half;

        // Check if dimensions fit
        if (size != ff_eigenVector_ROSSE.size())
        {
            cout << "Controller_K::ff_eigenVector_ROSSE_to_FlowField" << endl;
            cout << "MODAL DIMENSIONS DIFFERS" << endl;
            cout << " M : " << size << " / M_eigenvector : " << ff_eigenVector_ROSSE.size() << endl;
        }

        for (int i = 0; i < Nd; ++i) {
            for (int mx = 0; mx < Mx; ++mx) {
                for (int mz = 0; mz < Mz; ++mz) {
                    for (int my = 0; my < My; ++my) {
                        ff.cmplx(mx, my, mz, i) =
                                ff_eigenVector_ROSSE[i * (Mx*My*Mz) + mx * (My*Mz) + mz * My + my]
                                + 1i * ff_eigenVector_ROSSE[M_half + i * (Mx*My*Mz) + mx * (My*Mz) + mz * My + my];
                    }
                }
            }
        }

        ff.makeState(ff_xzstate, ff_ystate);
    }

    void Controller_ROSSE::test0()
    {
        cout << endl;
        cout << "-------------------------" << endl;
        cout << "Controller_ROSSE::test0" <<endl;
        cout << "Tests of methods Controller_ROSSE::ff_eigenVector_ROSSE_to_FlowField" << endl;
        cout << "and Controller_ROSSE::FlowField_to_ff_eigenVector_ROSSE" << endl;
        cout << "-------------------------" << endl;

        // create a random ROSSE state and apply method many time
        string home = string(getenv("HOME")) + "/";

        FlowField ff_temp = FlowField(_Nx_C, _Ny_C, _Nz_C, _Nd_C, 2*pi, 2*pi, -1, 1, Spectral, Physical);
        FlowField ff_temp_2 = FlowField(_Nx_C, _Ny_C, _Nz_C, _Nd_C, 2*pi, 2*pi, -1, 1, Spectral, Physical);

        int M = 2 * ff_temp.Mx() * ff_temp.Mz() * ff_temp.Mz() * ff_temp.Nd();

        Eigen::VectorXd ff_eigenVector_ROSSE = Eigen::VectorXd::Random(M);
        Eigen::VectorXd ff_eigenVector_ROSSE_2 = Eigen::VectorXd::Random(M);

        cout << "ff_eigenVector_ROSSE to FlowField" << endl;
        Controller_ROSSE::ff_eigenVector_ROSSE_to_FlowField(ff_eigenVector_ROSSE, ff_temp);
        cout << "FlowField to ff_eigenVector_ROSSE" << endl;
        Controller_ROSSE::FlowField_to_ff_eigenVector_ROSSE(ff_temp, ff_eigenVector_ROSSE_2);
        cout << "ff_eigenVector_ROSSE to FlowField" << endl;
        Controller_ROSSE::ff_eigenVector_ROSSE_to_FlowField(ff_eigenVector_ROSSE_2, ff_temp_2);

        Eigen::VectorXd delta_ROSSE = ff_eigenVector_ROSSE - ff_eigenVector_ROSSE_2;
        auto delta_ff = L2Dist(ff_temp, ff_temp_2);
        cout << "Delta_ROSSE.norm() : " << delta_ROSSE.norm() << endl; // 0
        cout << "L2Dist(ff_temp, ff_temp2) : " << delta_ff << endl; // 0
        cout << "-------------------------" << endl;
    }

    //############################################################
    // CONTROLLER ROSSE_via_OSSE CLASS
    // Created by Geoffroy Claisse
    // University of Southampton, 05/2019
    //############################################################

    Controller_ROSSE_via_OSSE::Controller_ROSSE_via_OSSE()
        :
            Controller_K(),
            _CT_filename(""),
            _CTinv_filename("")
    {
    }

    Controller_ROSSE_via_OSSE::Controller_ROSSE_via_OSSE(
            FlowField& ff,
            int Nx_C,
            int Ny_C,
            int Nz_C,
            std::string matrix_filename,
            std::string target_baseflow_filename,
            std::string CT_filename,
            std::string CTinv_filename,
            double tau)
        :
            Controller_K(ff,
                Nx_C,
                Ny_C,
                Nz_C,
                matrix_filename,
                target_baseflow_filename,
                tau),
            _CT_filename(CT_filename),
            _CTinv_filename(CTinv_filename)
    {
        cout << "------------------------------------" << endl;
        cout <<  "CONSTRUCTOR Controller_ROSSE_via_OSSE::Controller_ROSSE_via_OSSE " << endl;

        // LOAD MATRIX K for [u,v,w]
        if (_matrix_filename == "None")
            K = Eigen::MatrixXcd(_M_C_out, _M_C_rosse);
        else
        {
            cout << "Load matrix K: " << endl;
            cout << _matrix_filename << endl;
            K = Eigen::MatrixXcd(_M_C_out, _M_C_rosse);
            cout << _M_C_out << " x " <<_M_C_rosse << endl;
            MatIn_hdf5_complex(K, _matrix_filename);
        }

        // LOAD MATRIX _CT
        if (_CT_filename == "None")
            _CT = Eigen::MatrixXcd(_M_C_in, _M_C_osse);
        else
        {
            cout << "Load matrix _CT: " << endl;
            cout << _CT_filename << endl;
            _CT = Eigen::MatrixXcd(_M_C_in, _M_C_osse);
            cout << _M_C_in << " x " <<_M_C_osse << endl;
            MatIn_hdf5_complex(_CT, _CT_filename);
        }

        // LOAD MATRIX _CTinv
        if (_CTinv_filename == "None")
            _CTinv = Eigen::MatrixXcd(_M_C_osse, _M_C_in);
        else
        {
            cout << "Load matrix _CTinv: " << endl;
            cout << _CTinv_filename << endl;
            _CTinv = Eigen::MatrixXcd(_M_C_osse, _M_C_in);
            cout << _M_C_osse << " x " <<_M_C_in << endl;
            MatIn_hdf5_complex(_CTinv, _CTinv_filename);
        }
        cout << "------------------------------------" << endl << endl;
    }

    void Controller_ROSSE_via_OSSE::advance_Con_CON(
            FlowField& u,
            FlowField& p,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        // TODO
        // interpolate the target baseflow at creation
        // instead of at each controller call
        //
        // TODO
        // memory and perf profiling
        //
        // TODO
        // pass u_reduced into the constructor so it is not allocated at every iteration
        //
        // TODO
        //TEST 2:
        //from a null flowfield, but input just on one mode, and change K such that it does identity
        //check that q is actually the right mode actuated
        //
        //TEST 1:
        // consider q_vector = zeros and replace only one mode,
        // check the effect on the boundary condition
        // check if complex value
        // try to impose a null flowfield with imposed q_vector on one mode

        // double*** CStateMat, Real t, double*** IO are not used here
        // they are used to export/import the state of the field
        // so might be useful, but not required

        cout << "RO";
        //cout << "Controller_ROSSE::advance_Con_CON" << endl;

        fieldstate target_xzstate = _target_baseflow_flowfield.xzstate();
        fieldstate target_ystate = _target_baseflow_flowfield.ystate();
        //TODO : marke target_baseflow into (Spectral, Physical) ?
        _target_baseflow_flowfield.makeState(Spectral, Spectral);

        fieldstate BC_xzstate = BC.xzstate();
        fieldstate BC_ystate = BC.ystate();
        BC.makeState(Spectral, Physical);

        fieldstate u_xzstate = u.xzstate();
        fieldstate u_ystate = u.ystate();
        u.makeState(Spectral, Spectral);

        //#############################################################################################################################################
        // Extract the flowfield u and transform it into a state-vector
        //#############################################################################################################################################

        FlowField delta_u = FlowField(u);
        //cout << "L2Norm(u) in K: " << L2Norm(u) << endl;
        //cout << "State of u     : " << u.xzstate() << " / " << u.ystate() << endl;
        //cout << "State of delta : " << delta_u.xzstate() << " / " << delta_u.ystate() << endl;
        delta_u.makeState(Spectral, Spectral);
        //cout << "State of u     : " << u.xzstate() << " / " << u.ystate() << endl;
        //cout << "State of delta : " << delta_u.xzstate() << " / " << delta_u.ystate() << endl;

        // remove the baseflow form the channelflow state
        // to deal with the perturbation only
        // CHFL considers the flowfield as u_EQ + u_pert
        // while OSSE consider only the flowfield as u_pert
        // so the controller from OSSE is controlling the perturbations only,
        // meaning the distance from a given _target_baseflow_flowfield only
        Controller_K::substract_ff(delta_u, _target_baseflow_flowfield);
        //cout << "L2Norm(delta_u) in K: " << L2Norm(delta_u) << endl;

        // SUBSTRACTION VALIDATED (test0): with NoDealiasing and with DealiasXZ
        // -> NoDealiasing gives MUCH better precision if target and flowfield have same resolution

        // interpolate the channeflow velocity field u to controller dimension
        // NOTE: u and _target_baseflow_flowfield are both padded.
        // However, it is not influential as we interpolate u
        // into u_reduced of lower dimension, implying that the padding disappears.
        FlowField delta_reduced = FlowField(_Nx_C, _Ny_C, _Nz_C, u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
        Controller_K::K_changegrid(delta_reduced, delta_u);
        //delta_reduced.interpolate(delta_u);
        //cout << endl << "L2Norm of the flowfield u: " << L2Norm2(u) << endl;
        //cout << endl << "L2Norm of the flowfield target: " << L2Norm2(_target_baseflow_flowfield) << endl;
        //cout << endl << "L2Norm of the perturbation u: " << L2Norm2(u) << endl;
        //cout << endl << "L2Norm of the perturbation u reduced: " << L2Norm2(u_reduced) << endl;

        //std::cin.ignore();

        // REDUCTION AND INTERPOLATION VALIDATED (test1 and test2):
        // difference between u and u_reduced : CHECKED with L2Dist
        // difference between u_reduced and u.interpolate(u_reduced) : CHECKED with L2Dist

        // return the state of simulation u_reduced as an Eigen::VectorXcd
        // NOTE: The u_reduced_vector includes the entire set
        // of spanwise modes as "mirror = true".
        // This is necessary as OSSE spans all the spanwise modes
        // but CHFL only the positive spanwise modes, hence the "mirror".
        delta_reduced.makeState(Spectral, Physical);
        Eigen::VectorXd delta_reduced_vector_rosse = Eigen::VectorXd::Zero(_M_C_rosse);
        //cout << "FlowField_to_state_ROSSE_via_OSSE" << endl;
        Controller_ROSSE_via_OSSE::FlowField_to_state_ROSSE_via_OSSE(delta_reduced, delta_reduced_vector_rosse);

        // FlowField_to_ff_eigenVector VALIDATED (test3)
        // ff_eigenVector_to_FlowField VALIDATED (test4)
        // -> get an eigenvector from a flowfield CHECKED
        // -> back transform and compare flowfield CHECKED

        // operator ff_eigenVector_to_FlowField again and verify that I get the same flowfield -> CHECKED
        /*
        FlowField u_reduced2 = FlowField(_Nx_C, _Ny_C, _Nz_C, u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Physical);
        this->ff_eigenVector_to_FlowField(
                u_reduced_vector,
                u_reduced2,
                true);
        cout << "L2Dist u_reduced, u_reduced2 : " << L2Dist(u_reduced, u_reduced2) << endl;

        Eigen::VectorXcd u_reduced_vector2 = Eigen::VectorXcd::Zero(_M_C_in);
        this->FlowField_to_ff_eigenVector(
                u_reduced2,
                u_reduced_vector2,
                true);

        Eigen::VectorXcd x_diff = Eigen::VectorXcd::Zero(_M_C_in);
        x_diff = u_reduced_vector2 - u_reduced_vector;
        cout << "u_reduced_vector2 - u_reduced_vector : " << x_diff.norm() << endl;

        // save u_reduced
        u_reduced.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced.h5");
        u_reduced2.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced2.h5");
        // save u_reduced_vector
        MatOut_hdf5_complex(u_reduced_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced_vector.h5");
        MatOut_hdf5_complex(u_reduced_vector2, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced_vector2.h5");
        std::cin.ignore();
        */

        // HERE I SHOULD GET THE EXACT SAME STATE AS IN OSSE IF THE SIMULATION IS THE SAME
        // -> CHECKED

        //#############################################################################################################################################
        // Determine forcing q [yk in Heins controller class]
        //#############################################################################################################################################

        Eigen::VectorXcd q_vector = Eigen::VectorXcd::Zero(_M_C_out);
        //cout << "    M_C_in  : " << _M_C_in << endl;
        //cout << "    M_C_out : " << _M_C_out << endl;
        //cout << "    K : " << K.rows() << " / " << K.cols() << endl;

        // the vector q_vector does only contain the positive modes in kz
        // and not the negative ones as they are not useful for later
        // calculation in channelflow
        // just bear in mind for the different dimensions, here use _Mz_C, as in _M_C_out
        // Limitating the actuation on positive mode is made in the OSSE with the method
        // operator.op_reduce_actuation_modes, so that the resulting matrix K of the
        // OSSE model is limited to actuation on the positive kz already.

        q_vector = K * delta_reduced_vector_rosse;
        // CHECKED (test5): Verify that for a given state-vector given from OSSE, I get the same q in OSSE and in CHFL

        // CHECKED: FlowField q corresponds to the actuation at the boundaries,
        // with the same _Nx_C and _Nz_C as the controller
        // but for the wall-normal velocity at the upper and lower wall, so Nd = 1 and Ny = 2

        FlowField q = FlowField(_Nx_C, 2, _Nz_C, 1, BC.Lx(), BC.Lz(), BC.a(), BC.b(), Spectral, Physical);
        //cout << "ff_eigenVector_to_FlowField" << endl;
        Controller_K::ff_eigenVector_to_FlowField(q_vector, q, false);
        //MatOut_hdf5_complex(q_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/q_vector.h5");
        //q.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/q.h5");

        // Operator FlowField_to_ff_eigenVector and verify that q is equal to q_vector: CHECKED
        // however it is hard to know if the indexation is ok if mirror = false,
        // on the transformation in and back, it worked, so i think it is validated.
        // but bear in mind that q is only for positive kz mode
        //

        //cout << "L2Norm(q) : " << L2Norm(q) << endl;
        forcing = q;
        //cout << "L2Norm(f) : " << L2Norm(forcing) << endl;

        //#############################################################################################################################################
        // UPDATE THE FLOWFIELD BC
        //#############################################################################################################################################

        if (true)
        {
            // Crank-Nicholson low-pass filter integration
            double alpha_lpf = 1 + (dT / (2 * _tau));
            double beta_lpf =  1 - (dT / (2 * _tau));

            // This for-loop should fit the BCs dimensions. However, as only the modes of the controller
            // are actually actuated, I restrict the loop to _Mx_C and _Mz_C instead of BC.Mx() and BC.Mz().
            // All the high-dimension modes are not actuated.
            int mx_inloop;
            for (int mx = 0; mx < _Mx_C; ++mx) {
                // as q is of smaller dimension than BC, we use mx_inloop to fit the right modes pairs together.
                // not a problem for mz as all the kz are positive.
                // here mx corresponds to a mode of q while mx_inloop corresponds to the same corresonding mode for BC
                mx_inloop = BC.mx(q.kx(mx));
                //cout << "mx : " << mx << " / q.kx : " << q.kx(mx) << " / mx_inloop " << mx_inloop << endl;
                for (int mz = 0; mz < _Mz_C; ++mz) {
                    //lower wall, for u, v and w, already 0 for i=0 (u) and i=2 (w)
                    BC.cmplx(mx_inloop, 0, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC.cmplx(mx_inloop, 0, mz, 1)) + ((dT / _tau) * q.cmplx(mx, 0, mz, 0)) );
                    //upper  wall, for u, v and w, alreadu 0 for i=0 (u) and i=2 (w)
                    BC.cmplx(mx_inloop, 1, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC.cmplx(mx_inloop, 1, mz, 1)) + ((dT / _tau) * q.cmplx(mx, 1, mz, 0)) );
                }
            }

            // low-pass filter integration VALIDATED (test6)

            /*
            cout << setprecision(3) << endl;
            for (int mx = 0; mx < Mx; mx++) {
                kx = BC.kx(mx);
                cout << mx << "|" << kx << "    : ";
                for (int mz = 0; mz < Mz; mz++) {
                    //mz = BC.mz(kz);
                    cout << round(real(BC.cmplx(mx, 0, mz, 1))*100)/100 << "+(" << round(imag(BC.cmplx(mx, 0, mz, 1))*100)/100 << ")i | ";
                }
                cout << endl;
            }
            cout << "END" << endl;
            exit(1);
            */
        }
        else if (false)
        {
            int T_perturbed = 10;
            double forcing_mag = 0.0005;
            int kx = +1;
            int kz = +1;
            int mx = BC.mx(kx);
            int mz = BC.mz(kz);
            //cout << 1 * sin(2*pi*(t-T_perturbed)/10) * forcing_mag << endl;
            BC.cmplx(mx, 0, mz, 1) = 1 * sin(2*pi*(t-T_perturbed)/10) * forcing_mag;
            BC.cmplx(mx, 1, mz, 1) = 1 * sin(2*pi*(t-T_perturbed)/10) * forcing_mag;
        }

        _target_baseflow_flowfield.makeState(target_xzstate, target_ystate);
        BC.makeState(BC_xzstate, BC_ystate);
        u.makeState(u_xzstate, u_ystate);
    }

    void Controller_ROSSE_via_OSSE::FlowField_to_state_ROSSE_via_OSSE(
            FlowField& ff,
            Eigen::VectorXd& state_ROSSE)
    {
        // transform a flowfield [u,v,w]
        // into a vector as in the ROSSE model
        // [v.real, eta.real, u00.real, w00.real,
        //      v.imag, eta.imag, u00.imag, w00.imag]

        // TODO allocate memory in constructor instead
        Eigen::VectorXcd ff_vector = Eigen::VectorXcd::Zero(_M_C_in);
        Eigen::VectorXcd state_OSSE = Eigen::VectorXcd::Zero(_M_C_osse);

        // 1. transform flowfield array [u,v,w] with only kz > 0
        //    to vector [u,v,w] with all kz :
        //cout << "FlowField_to_eigenVector" << endl;
        Controller_K::FlowField_to_ff_eigenVector(ff, ff_vector, true);

        // 2. multiply vector by _Tinv and _Cinv,
        //    to get state as in OSSE model: state_OSSE
        //    for all kz
        //    [v+, v0, v-, eta+, eta0, eta-, u00+, u00, u00-, w00+, w00, w00-]
        //    NOTE: we need all kz because _Tinv and _Cinv are defined for all kz
        //cout << "_CTinv * ff_vector" << endl;
        state_OSSE = _CTinv * ff_vector;
        //cout << "_Tinv * _Cinv * ff_vector" << endl;
        //state_OSSE = _Tinv * _Cinv * ff_vector;

        // 3. remove positive kz modes and
        //    separate real and imaginary to get state_ROSSE
        //cout << "state_OSSE_to_state_ROSSE" << endl;
        Controller_K::state_OSSE_to_state_ROSSE(state_OSSE, state_ROSSE);
    }

    void Controller_ROSSE_via_OSSE::state_ROSSE_to_FlowField_via_OSSE(
            Eigen::VectorXd& state_ROSSE,
            FlowField& ff)
    {
        // transform a vector of the form of the ROSSE model
        // [v.real, eta.real, u00.real, w00.real,
        //      v.imag, eta.imag, u00.imag, w00.imag]
        // into a flowfield [u,v,w]

        // TODO allocate memory in constructor instead
        Eigen::VectorXcd ff_vector = Eigen::VectorXcd::Zero(_M_C_in);
        Eigen::VectorXcd state_OSSE = Eigen::VectorXcd::Zero(_M_C_osse);

        // 1. get state OSSE from state OSSE:
        // add real and imaginary part together
        // and rebuild the kz < 0 part.
        Controller_K::state_ROSSE_to_state_OSSE(state_ROSSE, state_OSSE);

        // 2. multiply by vector _C and _T
        // to get state as an eigenVector Flowfield,
        // i.e. with [u,v,w] form and with actuation effecting:
        // state_OSSE = [v+, v0, v-, eta+, eta0, eta-]
        // ff_vector = [u+, u, u-, v+, v, v-, w+, w, w-]
        // where v0 stands for homogeneous field (without actuation affecting)
        // and v stands for inhomogeneous field (with actuation affecting)
        // (see OSSE project, operators.py, methods osse_mapping and osse_change_x0_to_x
        // for more details_
        ff_vector = _CT * state_OSSE;

        // 3. transfpr, ff_vector (with all kz modes, so mirror == true)
        // back to a flowfield
        Controller_K::ff_eigenVector_to_FlowField(ff_vector, ff, true);
    }

    void Controller_ROSSE_via_OSSE::test0()
    {
        cout << endl;
        cout << "-------------------------" << endl;
        cout << "Controller_ROSSE_via_OSSE::test0" << endl;
        cout << "test of methods Controller_ROSSE_via_OSSE::state_ROSSE_to_OSSE" << endl;
        cout << "and Controller_ROSSE_via_OSSE::state_OSSE_to_ROSSE" << endl;
        cout << "-------------------------" << endl;

        // I simply create a ramdon state ROSSE and apply these method
        // many time, to obtain the same state ROSSE.

        // create state ROSSE
        Eigen::VectorXd state_ROSSE = Eigen::VectorXd::Random(_M_C_rosse);

        Eigen::VectorXcd state_OSSE = Eigen::VectorXcd::Zero(_M_C_osse);
        cout << "ROSSE to OSSE" << endl;
        Controller_K::state_ROSSE_to_state_OSSE(state_ROSSE, state_OSSE);

        Eigen::VectorXd state_ROSSE_2 = Eigen::VectorXd::Zero(_M_C_rosse);
        cout << "OSSE to ROSSE" << endl;
        Controller_K::state_OSSE_to_state_ROSSE(state_OSSE, state_ROSSE_2);

        Eigen::VectorXcd state_OSSE_2 = Eigen::VectorXcd::Zero(_M_C_osse);
        cout << "ROSSE to OSSE" << endl;
        Controller_K::state_ROSSE_to_state_OSSE(state_ROSSE_2, state_OSSE_2);

        Eigen::VectorXd delta_ROSSE = state_ROSSE - state_ROSSE_2;
        Eigen::VectorXcd delta_OSSE = state_OSSE - state_OSSE_2;

        /*
        int index;
        cout << "----------- V REAL ---------------" << endl;
        for (int i_kx = 0; i_kx < _Nx_C; i_kx++) {
            cout << "--------------- i_kx = " << i_kx << " ----------------" << endl;
            for (int i_kz = 0; i_kz < _Mz_C; i_kz++) {
                cout << "--------------- i_kz = " << i_kz << " ----------------" << endl;
                for (int i = 0; i < _Ny_C; i++) {
                    index = (i_kx * _Mz_C + i_kz) * _Ny_C;
                    cout << state_ROSSE[index + i] << " - " << state_ROSSE_2[index + i] << " = " << delta_ROSSE[index + i] << endl;
                }
            }
            cout << endl;
        }
        cout << endl;

        cout << "----------- ETA REAL ---------------" << endl;
        for (int i_kx = 0; i_kx < _Nx_C; i_kx++) {
            cout << "--------------- i_kx = " << i_kx << " ----------------" << endl;
            for (int i_kz = 0; i_kz < _Mz_C; i_kz++) {
                if (!(i_kx == 0 and i_kz == 0)){
                    cout << "--------------- i_kz = " << i_kz << " ----------------" << endl;
                    for (int i = 0; i < _Ny_C; i++) {
                        index = (_Nx_C * _Mz_C + i_kx * _Mz_C + i_kz - 1) * _Ny_C;
                        cout << state_ROSSE[index + i] << " - " << state_ROSSE_2[index + i] << " = " << delta_ROSSE[index + i] << endl;
                    }
                }
            }
            cout << endl;
        }
        cout << endl;

        int M_half = _M_C_rosse / 2;
        cout << "----------- V IMAG ---------------" << endl;
        for (int i_kx = 0; i_kx < _Nx_C; i_kx++) {
            cout << "--------------- i_kx = " << i_kx << " ----------------" << endl;
            for (int i_kz = 0; i_kz < _Mz_C; i_kz++) {
                cout << "--------------- i_kz = " << i_kz << " ----------------" << endl;
                for (int i = 0; i < _Ny_C; i++) {
                    index = M_half + (i_kx * _Mz_C + i_kz) * _Ny_C;
                    cout << state_ROSSE[index + i] << " - " << state_ROSSE_2[index + i] << " = " << delta_ROSSE[index + i] << endl;
                }
            }
            cout << endl;
        }
        cout << endl;
        cout << "----------- ETA IMAG ---------------" << endl;
        for (int i_kx = 0; i_kx < _Nx_C; i_kx++) {
            cout << "--------------- i_kx = " << i_kx << " ----------------" << endl;
            for (int i_kz = 0; i_kz < _Mz_C; i_kz++) {
                if (!(i_kx == 0 and i_kz == 0)){
                    cout << "--------------- i_kz = " << i_kz << " ----------------" << endl;
                    for (int i = 0; i < _Ny_C; i++) {
                        index = M_half + (_Nx_C * _Mz_C + i_kx * _Mz_C + i_kz - 1) * _Ny_C;
                        cout << state_ROSSE[index + i] << " - " << state_ROSSE_2[index + i] << " = " << delta_ROSSE[index + i] << endl;
                    }
                }
            }
            cout << endl;
        }
        cout << endl;


        cout << "-----------" << endl;
        cout << "--- OSSE ---" << endl;
        cout << "-----------" << endl;

        cout << "----------- V REAL ---------------" << endl;
        for (int i_kx = 0; i_kx < _Nx_C; i_kx++) {
            cout << "--------------- i_kx = " << i_kx << " ----------------" << endl;
            for (int i_kz = 0; i_kz < _Nz_C; i_kz++) {
                cout << "--------------- i_kz = " << i_kz << " ----------------" << endl;
                for (int i = 0; i < _Ny_C; i++) {
                    index = (i_kx * _Nz_C + i_kz) * _Ny_C;
                    cout << state_OSSE[index + i] << " - " << state_OSSE_2[index + i]<< " = " << delta_OSSE[index + i]<< endl;
                }
            }
            cout << endl;
        }
        cout << endl;
        cout << "----------- ETA REAL ---------------" << endl;
        for (int i_kx = 0; i_kx < _Nx_C; i_kx++) {
            cout << "--------------- i_kx = " << i_kx << " ----------------" << endl;
            for (int i_kz = 0; i_kz < _Nz_C; i_kz++) {
                if (!(i_kx == 0 and i_kz == 0)){
                    cout << "--------------- i_kz = " << i_kz << " ----------------" << endl;
                    for (int i = 0; i < _Ny_C; i++) {
                        index = (_Nx_C * _Nz_C + i_kx * _Nz_C + i_kz - 1) * _Ny_C;
                        cout << state_OSSE[index + i]<< " - " << state_OSSE_2[index + i]<< " = " << delta_OSSE[index + i]<< endl;
                    }
                }
            }
            cout << endl;
        }
        cout << endl;
        //cout << delta_OSSE << endl;
        //*/

        cout << "Delta_ROSSE.norm() : " << delta_ROSSE.norm() << endl; // 0
        cout << "Delta_OSSE.norm() : " << delta_OSSE.norm() << endl; // 0
        cout << "-------------------------" << endl;
    }

    void Controller_ROSSE_via_OSSE::test1()
    {
        cout << endl;
        cout << "-------------------------" << endl;
        cout << "Controller_ROSSE_via_OSSE::test1" <<endl;
        cout << "Tests of methods Controller_ROSSE_via_OSSE::state_ROSSE_to_FlowField_via_OSSE" << endl;
        cout << "and Controller_ROSSE_via_OSSE::FlowField_to_state_ROSSE_via_OSSE" << endl;
        cout << "-------------------------" << endl;

        // create a random ROSSE state and apply method many time
        string home = string(getenv("HOME")) + "/";

        Eigen::VectorXd state_ROSSE = Eigen::VectorXd::Random(_M_C_rosse);
        Eigen::VectorXd state_ROSSE_2 = Eigen::VectorXd::Random(_M_C_rosse);

        // could use following lines, but needs to consider Nopad dimension in this case:
        //FlowField ff_temp = FlowField(home + "osse/database/eq/eq1_"+i2s(_Nx_C)+"x"+i2s(_Ny_C)+"x"+i2s(_Nz_C)+"_findsoln.h5");
        //FlowField ff_temp_3 = FlowField(home + "osse/database/eq/eq1_"+i2s(_Nx_C)+"x"+i2s(_Ny_C)+"x"+i2s(_Nz_C)+"_findsoln.h5");
        // otherwise :
        FlowField ff_temp = FlowField(_Nx_C, _Ny_C, _Nz_C, _Nd_C, 2*pi, 2*pi, -1, 1, Spectral, Physical);
        FlowField ff_temp_2 = FlowField(_Nx_C, _Ny_C, _Nz_C, _Nd_C, 2*pi, 2*pi, -1, 1, Spectral, Physical);

        cout << "State ROSSE to FlowField via OSSE" << endl;
        Controller_ROSSE_via_OSSE::state_ROSSE_to_FlowField_via_OSSE(state_ROSSE, ff_temp);
        cout << "FlowField to State ROSSE via OSSE" << endl;
        Controller_ROSSE_via_OSSE::FlowField_to_state_ROSSE_via_OSSE(ff_temp, state_ROSSE_2);
        cout << "State ROSSE to FlowField via OSSE" << endl;
        Controller_ROSSE_via_OSSE::state_ROSSE_to_FlowField_via_OSSE(state_ROSSE_2, ff_temp_2);

        Eigen::VectorXd delta_ROSSE = state_ROSSE - state_ROSSE_2;
        auto delta_ff = L2Dist(ff_temp, ff_temp_2);
        cout << "Delta_ROSSE.norm() : " << delta_ROSSE.norm() << endl; // 0
        cout << "L2Dist(ff_temp, ff_temp2) : " << delta_ff << endl; // 0
        cout << "-------------------------" << endl;
    }

    //############################################################
    // CONTROLLER OSSE CLASS
    // Created by Geoffroy Claisse
    // University of Southampton, 05/2019
    //############################################################

    Controller_OSSE::Controller_OSSE()
        :
            Controller_K()
    {
    }

    Controller_OSSE::Controller_OSSE(
            FlowField& ff,
            int Nx_C,
            int Ny_C,
            int Nz_C,
            std::string matrix_filename,
            std::string target_baseflow_filename,
            double tau)
        :
            Controller_K(ff,
                Nx_C,
                Ny_C,
                Nz_C,
                matrix_filename,
                target_baseflow_filename,
                tau)
    {
        cout << "------------------------------------" << endl;
        cout << "CONSTRUCTOR Controller_OSSE::Controller_OSSE " << endl;

        // LOAD MATRIX K for [u,v,w]
        if (_matrix_filename == "None")
            K = Eigen::MatrixXcd(_M_C_out, _M_C_in);
        else
        {
            cout << "Load matrix K: " << endl;
            cout << _matrix_filename << endl;
            K = Eigen::MatrixXcd(_M_C_out, _M_C_in);
            cout << _M_C_out << endl;
            cout << _M_C_in << endl;
            MatIn_hdf5_complex(K, _matrix_filename);
        }
        cout << "------------------------------------" << endl << endl;
    }

    void Controller_OSSE::advance_Con_CON(
            FlowField& u,
            FlowField& p,
            FlowField& BC,
            double*** CStateMat,
            Real dT,
            Real t,
            double*** IO)
    {
        // TODO
        // interpolate the target baseflow at creation
        // instead of at each controller call
        //
        // TODO
        // memory and perf profiling
        //
        // TODO
        // pass u_reduced into the constructor so it is not allocated at every iteration
        //
        // TODO
        //TEST 2:
        //from a null flowfield, but input just on one mode, and change K such that it does identity
        //check that q is actually the right mode actuated
        //
        //TEST 1:
        // consider q_vector = zeros and replace only one mode,
        // check the effect on the boundary condition
        // check if complex value
        // try to impose a null flowfield with imposed q_vector on one mode

        // double*** CStateMat, Real t, double*** IO are not used here
        // they are used to export/import the state of the field
        // so might be useful, but not required

        cout << "O";
        //cout << "Controller_ROSSE::advance_Con_CON" << endl;

        fieldstate target_xzstate = _target_baseflow_flowfield.xzstate();
        fieldstate target_ystate = _target_baseflow_flowfield.ystate();
        //TODO : marke target_baseflow into (Spectral, Physical) ?
        _target_baseflow_flowfield.makeState(Spectral, Spectral);

        fieldstate BC_xzstate = BC.xzstate();
        fieldstate BC_ystate = BC.ystate();
        BC.makeState(Spectral, Physical);

        fieldstate u_xzstate = u.xzstate();
        fieldstate u_ystate = u.ystate();
        u.makeState(Spectral, Spectral);

        //#############################################################################################################################################
        // Extract the flowfield u and transform it into a state-vector
        //#############################################################################################################################################

        FlowField delta_u = FlowField(u);
        //cout << "L2Norm(u) in K: " << L2Norm(u) << endl;
        //cout << "State of u     : " << u.xzstate() << " / " << u.ystate() << endl;
        //cout << "State of delta : " << delta_u.xzstate() << " / " << delta_u.ystate() << endl;
        delta_u.makeState(Spectral, Spectral);
        //cout << "State of u     : " << u.xzstate() << " / " << u.ystate() << endl;
        //cout << "State of delta : " << delta_u.xzstate() << " / " << delta_u.ystate() << endl;

        // remove the baseflow form the channelflow state
        // to deal with the perturbation only
        // CHFL considers the flowfield as u_EQ + u_pert
        // while OSSE consider only the flowfield as u_pert
        // so the controller from OSSE is controlling the perturbations only,
        // meaning the distance from a given _target_baseflow_flowfield only
        Controller_K::substract_ff(delta_u, _target_baseflow_flowfield);
        //cout << "L2Norm(delta_u) in K: " << L2Norm(delta_u) << endl;

        //TODO does save make a change ?

        // SUBSTRACTION VALIDATED (test0): with NoDealiasing and with DealiasXZ
        // -> NoDealiasing gives MUCH better precision if target and flowfield have same resolution

        // interpolate the channeflow velocity field u to controller dimension
        // NOTE: u and _target_baseflow_flowfield are both padded.
        // However, it is not influential as we interpolate u
        // into u_reduced of lower dimension, implying that the padding disappears.
        FlowField delta_reduced = FlowField(_Nx_C, _Ny_C, _Nz_C, u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
        Controller_K::K_changegrid(delta_reduced, delta_u);
        //delta_reduced.interpolate(delta_u);
        //cout << endl << "L2Norm of the flowfield u: " << L2Norm2(u) << endl;
        //cout << endl << "L2Norm of the flowfield target: " << L2Norm2(_target_baseflow_flowfield) << endl;
        //cout << endl << "L2Norm of the perturbation u: " << L2Norm2(u) << endl;
        //cout << endl << "L2Norm of the perturbation u reduced: " << L2Norm2(u_reduced) << endl;

        //TODO does save make a change ?

        //std::cin.ignore();

        // REDUCTION AND INTERPOLATION VALIDATED (test1 and test2):
        // difference between u and u_reduced : CHECKED with L2Dist
        // difference between u_reduced and u.interpolate(u_reduced) : CHECKED with L2Dist

        // return the state of simulation u_reduced as an Eigen::VectorXcd
        // NOTE: The u_reduced_vector includes the entire set
        // of spanwise modes as "mirror = true".
        // This is necessary as OSSE spans all the spanwise modes
        // but CHFL only the positive spanwise modes, hence the "mirror".
        delta_reduced.makeState(Spectral, Physical);
        Eigen::VectorXcd delta_reduced_vector = Eigen::VectorXcd::Zero(_M_C_in);
        this->FlowField_to_ff_eigenVector(
                delta_reduced,
                delta_reduced_vector,
                true);

        // FlowField_to_ff_eigenVector VALIDATED (test3)
        // ff_eigenVector_to_FlowField VALIDATED (test4)
        // -> get an eigenvector from a flowfield CHECKED
        // -> back transform and compare flowfield CHECKED

        // operator ff_eigenVector_to_FlowField again and verify that I get the same flowfield -> CHECKED
        /*
        FlowField u_reduced2 = FlowField(_Nx_C, _Ny_C, _Nz_C, u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Physical);
        this->ff_eigenVector_to_FlowField(
                u_reduced_vector,
                u_reduced2,
                true);
        cout << "L2Dist u_reduced, u_reduced2 : " << L2Dist(u_reduced, u_reduced2) << endl;

        Eigen::VectorXcd u_reduced_vector2 = Eigen::VectorXcd::Zero(_M_C_in);
        this->FlowField_to_ff_eigenVector(
                u_reduced2,
                u_reduced_vector2,
                true);

        Eigen::VectorXcd x_diff = Eigen::VectorXcd::Zero(_M_C_in);
        x_diff = u_reduced_vector2 - u_reduced_vector;
        cout << "u_reduced_vector2 - u_reduced_vector : " << x_diff.norm() << endl;

        // save u_reduced
        u_reduced.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced.h5");
        u_reduced2.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced2.h5");
        // save u_reduced_vector
        MatOut_hdf5_complex(u_reduced_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced_vector.h5");
        MatOut_hdf5_complex(u_reduced_vector2, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced_vector2.h5");
        std::cin.ignore();
        */

        // HERE I SHOULD GET THE EXACT SAME STATE AS IN OSSE IF THE SIMULATION IS THE SAME
        // -> CHECKED

        //#############################################################################################################################################
        // Determine forcing q [yk in Heins controller class]
        //#############################################################################################################################################

        Eigen::VectorXcd q_vector = Eigen::VectorXcd::Zero(_M_C_out);
        //cout << "    M_C_in  : " << _M_C_in << endl;
        //cout << "    M_C_out : " << _M_C_out << endl;
        //cout << "    K : " << K.rows() << " / " << K.cols() << endl;

        // the vector q_vector does only contain the positive modes in kz
        // and not the negative ones as they are not useful for later
        // calculation in channelflow
        // just bear in mind for the different dimensions, here use _Mz_C, as in _M_C_out
        // Limitating the actuation on positive mode is made in the OSSE with the method
        // operator.op_reduce_actuation_modes, so that the resulting matrix K of the
        // OSSE model is limited to actuation on the positive kz already.

        q_vector = K * delta_reduced_vector;

        // CHECKED (test5): Verify that for a given state-vector given from OSSE, I get the same q in OSSE and in CHFL

        // CHECKED: FlowField q corresponds to the actuation at the boundaries,
        // with the same _Nx_C and _Nz_C as the controller
        // but for the wall-normal velocity at the upper and lower wall, so Nd = 1 and Ny = 2

        FlowField q = FlowField(_Nx_C, 2, _Nz_C, 1, BC.Lx(), BC.Lz(), BC.a(), BC.b(), Spectral, Physical);
        this->ff_eigenVector_to_FlowField(q_vector, q, false);
        //MatOut_hdf5_complex(q_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/q_vector.h5");
        //q.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/q.h5");

        // Operator FlowField_to_ff_eigenVector and verify that q is equal to q_vector: CHECKED
        // however it is hard to know if the indexation is ok if mirror = false,
        // on the transformation in and back, it worked, so i think it is validated.
        // but bear in mind that q is only for positive kz mode
        //

        //cout << "L2Norm(q) : " << L2Norm(q) << endl;
        forcing = q;
        //cout << "L2Norm(f) : " << L2Norm(forcing) << endl;

        //#############################################################################################################################################
        // UPDATE THE FLOWFIELD BC
        //#############################################################################################################################################

        if (true)
        {
            // Crank-Nicholson low-pass filter integration
            double alpha_lpf = 1 + (dT / (2 * _tau));
            double beta_lpf =  1 - (dT / (2 * _tau));

            // This for-loop should fit the BCs dimensions. However, as only the modes of the controller
            // are actually actuated, I restrict the loop to _Mx_C and _Mz_C instead of BC.Mx() and BC.Mz().
            // All the high-dimension modes are not actuated.
            int mx_inloop;
            for (int mx = 0; mx < _Mx_C; ++mx) {
                // as q is of smaller dimension than BC, we use mx_inloop to fit the right modes pairs together.
                // not a problem for mz as all the kz are positive.
                // here mx corresponds to a mode of q while mx_inloop corresponds to the same corresonding mode for BC
                mx_inloop = BC.mx(q.kx(mx));
                //cout << "mx : " << mx << " / q.kx : " << q.kx(mx) << " / mx_inloop " << mx_inloop << endl;
                for (int mz = 0; mz < _Mz_C; ++mz) {
                    //lower wall, for u, v and w, already 0 for i=0 (u) and i=2 (w)
                    BC.cmplx(mx_inloop, 0, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC.cmplx(mx_inloop, 0, mz, 1)) + ((dT / _tau) * q.cmplx(mx, 0, mz, 0)) );
                    //upper  wall, for u, v and w, alreadu 0 for i=0 (u) and i=2 (w)
                    BC.cmplx(mx_inloop, 1, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC.cmplx(mx_inloop, 1, mz, 1)) + ((dT / _tau) * q.cmplx(mx, 1, mz, 0)) );
                }
            }

            // low-pass filter integration VALIDATED (test6)

            /*
            cout << setprecision(3) << endl;
            for (int mx = 0; mx < Mx; mx++) {
                kx = BC.kx(mx);
                cout << mx << "|" << kx << "    : ";
                for (int mz = 0; mz < Mz; mz++) {
                    //mz = BC.mz(kz);
                    cout << round(real(BC.cmplx(mx, 0, mz, 1))*100)/100 << "+(" << round(imag(BC.cmplx(mx, 0, mz, 1))*100)/100 << ")i | ";
                }
                cout << endl;
            }
            cout << "END" << endl;
            exit(1);
            */
        }
        else if (false)
        {
            int T_perturbed = 10;
            double forcing_mag = 0.0005;
            int kx = +1;
            int kz = +1;
            int mx = BC.mx(kx);
            int mz = BC.mz(kz);
            //cout << 1 * sin(2*pi*(t-T_perturbed)/10) * forcing_mag << endl;
            BC.cmplx(mx, 0, mz, 1) = 1 * sin(2*pi*(t-T_perturbed)/10) * forcing_mag;
            BC.cmplx(mx, 1, mz, 1) = 1 * sin(2*pi*(t-T_perturbed)/10) * forcing_mag;
        }

        _target_baseflow_flowfield.makeState(target_xzstate, target_ystate);
        BC.makeState(BC_xzstate, BC_ystate);
        u.makeState(u_xzstate, u_ystate);
    }

    void Controller_OSSE::test8(FlowField& BC)
    {
        // run a simulation without any control
        // with channelflow and save the state
        // after that, compute the q with advance_Con_CON
        // method and compare to OSSE
        //
        // about PADDING:
        // Nx_nopad = 32
        // CHFL simulation with nodealising produces results
        // with Nx = 48 for dataset but padded
        // if use changegrid --Nx 48 --padded true
        // I get Nx = 32 for dataset but padded, so 48 within chfl
        // if use changegrid --Nx 32 --padded false
        // I get Nx = 32 for dataset but not padded,
        // so 32 within chfl
        //
        // so for now I aim for simulation from NoDealiasing:
        //
        // take the original_48x65x32 as input for flowfield
        
        
        //for (int t = 0; t < 50; ++t) {
        //}
    }

    void Controller_OSSE::test7bis(FlowField& BC)
    {
        // TODO
        // Write distance and norm method

        cout << "test 7 bis" << endl << "---------------" << endl;
        //Real dT = 1;

        fieldstate target_xzstate = _target_baseflow_flowfield.xzstate();
        fieldstate target_ystate = _target_baseflow_flowfield.ystate();
        _target_baseflow_flowfield.makeState(Spectral, Physical);

        fieldstate BC_xzstate = BC.xzstate();
        fieldstate BC_ystate = BC.ystate();
        BC.makeState(Spectral, Physical);

        // from changegrid
        FlowField eq1_17_npd("/home/gcpc1m15/osse/database/eq/eq1_17x21x17_unpadded.h5");
        eq1_17_npd.makeState(Spectral, Physical);
        // original
        FlowField eq1_32_npd("/home/gcpc1m15/osse/database/eq/eq1_32x65x32_unpadded.h5");
        eq1_32_npd.makeState(Spectral, Physical);
        // to compare
        FlowField eq1_17_npd_bis = FlowField(eq1_32_npd);
        eq1_17_npd_bis.interpolate(eq1_17_npd);

        FlowField tmp;

        //
        // which state for xz and y do I need to use L2Norm ?!?!
        // L2Norm needs .makeSpectral()
        //
        cout << "-----------------------" << endl;
        cout << "TEST ACCURACY INTERPOLATION FROM 32 TO 17: eq1" << endl;
        cout << "-----------------------" << endl;
        eq1_17_npd.makeState(Spectral, Spectral);
        eq1_17_npd_bis.makeState(Spectral, Spectral);
        eq1_32_npd.makeState(Spectral, Spectral);
        cout << "L2Norm(eq1_17_npd)                    : " << L2Norm2(eq1_17_npd) << endl;
        cout << eq1_17_npd.Nx() << " / " << eq1_17_npd.Ny() << " / " << eq1_17_npd.Nz() << endl;
        cout << "L2Norm(eq1_17_npd_bis)                : " << L2Norm2(eq1_17_npd_bis) << endl;
        cout << eq1_17_npd_bis.Nx() << " / " << eq1_17_npd_bis.Ny() << " / " << eq1_17_npd_bis.Nz() << endl;
        cout << "L2Norm(eq1_32_npd)                    : " << L2Norm2(eq1_32_npd) << endl;
        cout << eq1_32_npd.Nx() << " / " << eq1_32_npd.Ny() << " / " << eq1_32_npd.Nz() << endl;
        //cout << "Distance between eq1_17 and eq1_32    : " <<
        //    L2Dist(eq1_17_npd, eq1_32_npd) << endl;
        //cout << "Distance between eq1_32 and eq1_17    : " <<
        //    L2Dist(eq1_32_npd, eq1_17_npd) << endl;

        tmp = FlowField(eq1_32_npd);
        tmp -= eq1_17_npd_bis;
        cout << "L2Dist(eq1_32, eq1_17_bis)            : " <<
            L2Dist(eq1_32_npd, eq1_17_npd_bis) << endl;
        cout << "L2Norm2(eq1_32 - eq1_npd_bis)         : " <<
            L2Norm2(tmp) << endl;

        tmp = FlowField(eq1_17_npd_bis);
        tmp -= eq1_32_npd;
        cout << "L2Dist(eq1_17_bis, eq1_32)            : " <<
            L2Dist(eq1_17_npd_bis, eq1_32_npd) << endl;
        cout << "L2Norm2(eq1_17_bis - eq1_32)          : " <<
            L2Norm2(tmp) << endl;
        eq1_17_npd.makeState(Spectral, Physical);
        eq1_17_npd_bis.makeState(Spectral, Physical);
        eq1_32_npd.makeState(Spectral, Physical);

        FlowField eq1_32_npd_bis("/home/gcpc1m15/osse/database/eq/eq1_32x65x32_unpadded.h5");
        eq1_32_npd_bis.makeState(Spectral, Physical);
        Controller_K::substract_ff(eq1_32_npd_bis, eq1_17_npd_bis);
        eq1_32_npd_bis.makeState(Spectral, Spectral);
        cout << "L2Norm2(substract(eq1_32, eq1_17_bis) : " << L2Norm2(eq1_32_npd_bis) << endl;

        // CONCLUSION ABOUT L2Norm2, L2Dist, and substract:
        // 1. always use (Spectral, Spectral) when using L2Norm2, L2Dist
        //    always use (Spectral, Physical) when using substract_ff
        // 2. to use L2Dist, it requires the same resolution for both fields
        // 3. L2Dist does not actually the distance I do expect
        //    CORRECTION: L2Norm2 = (L2Dist)**2
        // 4. but substract does give the distance I expect,
        //    so use substract to compare results and validity

        int t = 10;

        //FlowField u_17_npd("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/u/17x21x17/unpadded/u"+i2s(t)+".h5");
        //u_17_npd.makeState(Spectral, Physical);
        FlowField u_32_npd("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/u/32x65x32/unpadded/u"+i2s(t)+".h5");
        //FlowField u_32_npd("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/test_delta/random2_32_unpadded.h5");
        u_32_npd.makeState(Spectral, Physical);
        FlowField u_32_npd_bis(u_32_npd);
        u_32_npd_bis.makeState(Spectral, Physical);
        FlowField u_32_npd_tri(u_32_npd);
        u_32_npd_tri.makeState(Spectral, Physical);
        FlowField u_32_npd_four(u_32_npd);
        u_32_npd_four.makeState(Spectral, Physical);

        cout << endl << "-----------------------" << endl;
        cout << " I. REDUCE THEN SUBSTRACT" << endl;
        cout << "-----------------------" << endl;
        cout << " I.1. Reduce from u 32 to 17" << endl;
        cout << "-----------------------" << endl;
        // SMAL TEST
        // 1. take u32 and interpolate it to u17
        // calculate delta_interp = u32 - u17
        FlowField u_reduced_32_npd = FlowField(_Nx_C, _Ny_C, _Nz_C, u_32_npd.Nd(), u_32_npd.Lx(), u_32_npd.Lz(), u_32_npd.a(), u_32_npd.b(), Spectral, Physical);
        u_reduced_32_npd.makeState(Spectral, Spectral);
        Controller_K::K_changegrid(u_reduced_32_npd, u_32_npd);
        //u_reduced_32_npd.interpolate(u_32_npd);

        FlowField u_reduced_32_npd_bis = FlowField(u_32_npd);
        Controller_K::K_changegrid(u_reduced_32_npd_bis, u_32_npd);
        //u_reduced_32_npd_bis.interpolate(u_reduced_32_npd);

        u_32_npd.makeState(Spectral, Spectral);
        u_32_npd_bis.makeState(Spectral, Spectral);
        u_reduced_32_npd.makeState(Spectral, Spectral);
        u_reduced_32_npd_bis.makeState(Spectral, Spectral);
        //cout << "L2Dist(u, u_reduced) from 32_npd    : " << L2Dist(u_reduced_32_npd, u_32_npd) << endl;
        //cout << "L2Dist(u_reduced, u) from 32_npd    : " << L2Dist(u_32_npd, u_reduced_32_npd) << endl;
        //cout << "L2Dist(u, u_reduced_bis) from 32_npd: " << L2Dist(u_reduced_32_npd_bis, u_32_npd) << endl;
        //cout << "L2Dist(u_reduced_bis, u) from 32_npd: " << L2Dist(u_32_npd, u_reduced_32_npd_bis) << endl;
        cout << "L2Norm2(u_32_npd)                   : " << L2Norm2(u_32_npd) << endl;
        cout << u_32_npd.Nx() << " / " << u_32_npd.Ny() << " / " << u_32_npd.Nz() << endl;
        cout << "L2Norm2(u_32_npd_bis)               : " << L2Norm2(u_32_npd_bis) << endl;
        cout << "L2Norm2(u_reduced_32_npd)           : " << L2Norm2(u_reduced_32_npd) << endl;
        cout << "L2Norm2(u_reduced_32_npd_bis)       : " << L2Norm2(u_reduced_32_npd_bis) << endl;
        //u_32_npd.makeState(Spectral, Physical);
        //u_reduced_32_npd.makeState(Spectral, Physical);

        cout << endl;
        u_32_npd_bis.makeState(Spectral, Physical);
        u_reduced_32_npd_bis.makeState(Spectral, Physical);
        Controller_K::substract_ff(u_32_npd_bis, u_reduced_32_npd_bis);
        u_32_npd_bis.makeState(Spectral, Spectral);
        cout << "L2Norm2(substract(u_32,u_reduced_32_bis): " << L2Norm2(u_32_npd_bis) << endl;

        u_32_npd.makeState(Spectral, Spectral);
        u_reduced_32_npd_bis.makeState(Spectral, Spectral);
        cout << "L2Dist(u_32, u_reduced_32_bis)          : " << L2Dist(u_32_npd, u_reduced_32_npd_bis) << endl;
        tmp = FlowField(u_32_npd);
        tmp -= u_reduced_32_npd_bis;
        cout << "L2Norm2(u_32 - u_reduced_32_bis)        : " << L2Norm2(tmp) << endl;

        cout << "-----------------------" << endl;
        cout << " I.2. Substract delta_17 = u_17 - eq1_17" << endl;
        cout << "-----------------------" << endl;

        // the reduced ff is "u_reduced_32_npd" in 17 and "u_reduced_32_npd_bis" in 32
        eq1_17_npd.makeState(Spectral, Spectral);
        u_reduced_32_npd.makeState(Spectral, Spectral);

        FlowField delta_17 = FlowField(u_reduced_32_npd);
        delta_17 -= eq1_17_npd;

        FlowField delta_17_bis = FlowField(u_reduced_32_npd);
        Controller_K::substract_ff(delta_17_bis, eq1_17_npd);

        cout << "L2Norm2(eq1_17_npd)             : " << L2Norm2(eq1_17_npd) << endl;
        cout << "L2Norm2(u_reduced_32_npd)       : " << L2Norm2(u_reduced_32_npd) << endl;
        cout << "L2Norm2(u_reduced - eq1_17)     : " << L2Norm2(delta_17) << endl;
        cout << "L2Norm2(SUB(u_reduced, eq1_17)) : " << L2Norm2(delta_17_bis) <<
            " <<<<<<<<<<<<<<<<<<< " << endl;

        cout << endl;
        FlowField delta_17_python("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/test_delta/delta_17_"+i2s(t)+".h5");
        delta_17_python.makeState(Spectral, Spectral);
        cout << "L2Norm(delta_17_python)         : " << L2Norm2(delta_17_python) << endl;

        cout << endl;
        cout << "-----------------------" << endl;
        cout << " II. SUBSTRACT THEN REDUCE (what I do)" << endl;

        cout << "-----------------------" << endl;
        cout << " II.1. Substract delta_32 = u_32 - eq1_32" << endl;
        cout << "-----------------------" << endl;

        eq1_32_npd.makeState(Spectral, Spectral);
        u_32_npd.makeState(Spectral, Spectral);
        u_32_npd_tri.makeState(Spectral, Spectral);
        u_32_npd_four.makeState(Spectral, Spectral);
        cout << "L2Norm of eq1_32_npd            : " << L2Norm2(eq1_32_npd) << endl;
        cout << "L2Norm of u_32_npd              : " << L2Norm2(u_32_npd) << endl;
        cout << "L2Norm of u_32_npd_tri          : " << L2Norm2(u_32_npd_tri) << endl;
        cout << "L2Norm of u_32_npd_four         : " << L2Norm2(u_32_npd_four) << endl;

        cout << endl << "SUBSTRACTION u32 - eq1_32" << endl;
        u_32_npd_four.makeState(Spectral, Spectral);
        eq1_32_npd.makeState(Spectral, Spectral);
        u_32_npd_four -= eq1_32_npd;

        u_32_npd_tri.makeState(Spectral, Spectral);
        eq1_32_npd.makeState(Spectral, Spectral);
        Controller_K::substract_ff(u_32_npd_tri, eq1_32_npd);

        eq1_32_npd.makeState(Spectral, Spectral);
        u_32_npd.makeState(Spectral, Spectral);
        u_32_npd_tri.makeState(Spectral, Spectral);
        u_32_npd_four.makeState(Spectral, Spectral);
        cout << "L2Norm of eq1_32_npd            : " << L2Norm2(eq1_32_npd) << endl;
        cout << "L2Norm of u_32_npd              : " << L2Norm2(u_32_npd) << endl;
        cout << "L2Dist(u_32, eq1_32)            : " << L2Dist(u_32_npd, eq1_32_npd) << endl;
        cout << "L2Norm2(subst(u_32, eq1_32)     : " << L2Norm2(u_32_npd_tri) << endl;
        cout << "L2Norm2(u_32 - eq1_32)          : " << L2Norm2(u_32_npd_four) << endl;

        cout << endl;
        FlowField delta_32_python("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/test_delta/delta_32_"+i2s(t)+".h5");
        delta_32_python.makeState(Spectral, Spectral);
        cout << "L2Norm(delta_32_python)         : " << L2Norm2(delta_32_python) << endl;

        // TODO: why is the perturbation so small ?

        cout << endl << "ADDITION delta32 + eq1_32" << endl;

        u_32_npd_four += eq1_32_npd;
        cout << "L2Norm of u_32_npd_four         : " << L2Norm2(u_32_npd_four) << endl;

        cout << "-----------------------" << endl;
        cout << " II.2. Reduce delta_32 to 17" << endl;
        cout << "-----------------------" << endl;

        eq1_17_npd.makeState(Spectral, Spectral);
        FlowField delta_17_tri = FlowField(eq1_17_npd);
        Controller_K::K_changegrid(delta_17_tri, u_32_npd_tri);
        //delta_17_tri.interpolate(u_32_npd_tri);

        cout << "L2Norm2(eq1_17_npd)             : " << L2Norm2(eq1_17_npd) << endl;
        cout << "L2Norm2(u_32 - eq1_32)          : " << L2Norm2(u_32_npd_tri) << endl;
        cout << "L2Norm2((u_32 - eq1_32)_reduced): " << L2Norm2(delta_17_tri) << 
            " <<<<<<<<<<<<<<<<<<< " << endl;

        // just put delta_17_tri back to 32 to calculate error during interpolation
        FlowField delta_17_tri_32 = FlowField(eq1_32_npd);
        Controller_K::K_changegrid(delta_17_tri_32, delta_17_tri);
        //delta_17_tri_32.interpolate(delta_17_tri);
        delta_17_tri_32 -= u_32_npd_tri;
        cout << "L2Norm2(delta_17 - delta_32) error interpolate of delta: " << endl;
        cout << "                                : " << L2Norm2(delta_17_tri_32) << endl;

        // -> delta_interp should be way smaller than delta_substract
        //
        // interpolate to delta17
        //
        //
        //
        // 3. take u17 and substract eq1_17
        // calculate delta_substract = u17 - eq1_17
        // <<<<<<<<<<<<<<<<<<

        // >>>>>>>>>>>>>>>>>>
        // OTHER TEST
        // case 1 :
        // 1. load u32
        // 2. reduce u32 to u17
        // 3. substract eq1_17
        // 4. get q
        //
        // case 2:
        // 1. load u32
        // 2. substract eq1_32
        // 3. reduce delta_32 to delta_17
        // 4. get q
        //
        // -> WHAT IS THE DISTANCE between eq1_17 and eq1_32 ???
        // because in case 1 and case 2 I substract 2 different things
        //
        // shall the case 1 and the case 2 gives the exact same q ?
        // if no, why ?
        // <<<<<<<<<<<<<<<<<<

        _target_baseflow_flowfield.makeState(target_xzstate, target_ystate);
        BC.makeState(BC_xzstate, BC_ystate);
    }

    void Controller_OSSE::test7(FlowField& BC)
    {
        // get a simulation from channelflow with 32x35x32
        //
        // in test7 folder:
        // get the changegrid of the simulaiton
        // for 17x21x17 for OSSE
        // import each timestep in OSSE
        // import matrix K
        // get q for each timestep
        //
        // import each timestep in channelflow within this method
        // and get the correspond q for each timestep
        //
        // compare q from CHFL and OSSE

        // double*** CStateMat, Real t, double*** IO are not used here
        // they are used to export/import the state of the field
        // so might be useful, but not required

        cout << "K";
        Real dT = 1;

        fieldstate target_xzstate = _target_baseflow_flowfield.xzstate();
        fieldstate target_ystate = _target_baseflow_flowfield.ystate();
        _target_baseflow_flowfield.makeState(Spectral, Physical);

        fieldstate BC_xzstate = BC.xzstate();
        fieldstate BC_ystate = BC.ystate();

        FlowField BC_I_pad  = FlowField(BC);
        FlowField BC_I_48   = FlowField(BC);
        FlowField BC_II_pad = FlowField(BC);
        FlowField BC_II_48  = FlowField(BC);

        BC_I_pad.makeState(Spectral, Physical);
        cout << endl;
        cout << BC_I_pad.Nd() << endl;
        cout << BC_I_pad.Nx() << endl;
        cout << BC_I_pad.Ny() << endl;
        cout << BC_I_pad.Nz() << endl;
        cout << BC_I_pad.Mx() << endl;
        cout << BC_I_pad.Mz() << endl;
        cout << L2Norm2(BC_I_pad) << endl;

        //#############################################################################################################################################
        // Extract the flowfield u and transform it into a state-vector
        //#############################################################################################################################################

        // It seems that by taking a target flowfield such that :
        // - the target flowfield and the controller has the same dimensions
        // - the target flowfield has no padding
        // the interpolation is not necessary and the resulting forcing q
        // is the same (or the closest) between OSSE and CHFL

        FlowField eq1_17_npd("/home/gcpc1m15/osse/database/eq/eq1_17x21x17_unpadded.h5");
        eq1_17_npd.makeState(Spectral, Spectral);
        FlowField eq1_17_pad("/home/gcpc1m15/osse/database/eq/eq1_17x21x17.h5");
        eq1_17_pad.makeState(Spectral, Spectral);
        FlowField eq1_32_npd("/home/gcpc1m15/osse/database/eq/eq1_32x65x32_unpadded.h5");
        eq1_32_npd.makeState(Spectral, Spectral);
        FlowField eq1_32_pad("/home/gcpc1m15/osse/database/eq/eq1_32x65x32.h5");
        eq1_32_pad.makeState(Spectral, Spectral);

        cout << "eq1_17_npd : " << endl;
        cout << eq1_17_npd.Nx() << " / " << eq1_17_npd.Ny() << " / " << eq1_17_npd.Nz() << endl;
        cout << "eq1_17_pad : " << endl;
        cout << eq1_17_pad.Nx() << " / " << eq1_17_pad.Ny() << " / " << eq1_17_pad.Nz() << endl;
        cout << "eq1_32_npd : " << endl;
        cout << eq1_32_npd.Nx() << " / " << eq1_32_npd.Ny() << " / " << eq1_32_npd.Nz() << endl;
        cout << "eq1_32_pad : " << endl;
        cout << eq1_32_pad.Nx() << " / " << eq1_32_pad.Ny() << " / " << eq1_32_pad.Nz() << endl;

        // here I do either two things:
        // REDUCE AND SUBSTRACT with u_17_from_32
        // SUBSTRACT AND REDUCE with u_32 and u_reduced_32

        for (int t = 0; t < 50; ++t) {
            // For the controller state vector, I NEED SPECTRAL PHYSICAL
            cout << "------------ time : " << i2s(t) << "-----------" << endl;

            //-----------------------
            // LOAD FLOWFIELDS
            // Note: changegrid was operated outside this method beforehand for 17x21x17
            //-----------------------
            FlowField u_17_npd("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/u/17x21x17/unpadded/u"+i2s(t)+".h5");
            u_17_npd.makeState(Spectral, Spectral);
            FlowField u_17_pad("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/u/17x21x17/padded/u"+i2s(t)+".h5");
            u_17_pad.makeState(Spectral, Spectral);

            FlowField u_32_npd("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/u/32x65x32/unpadded/u"+i2s(t)+".h5");
            u_32_npd.makeState(Spectral, Spectral);
            FlowField u_32_pad("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/u/32x65x32/padded/u"+i2s(t)+".h5");
            u_32_pad.makeState(Spectral, Spectral);

            FlowField u_48("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/u/32x65x32/NoDealiasing_48x65x48/u"+i2s(t)+".h5");
            u_48.makeState(Spectral, Spectral);

            //-----------------------
            // delta_17 = (u_17 - eq1_17)
            //-----------------------
            FlowField delta_17_npd = FlowField(u_17_npd);
            Controller_K::substract_ff(delta_17_npd, eq1_17_npd);
            cout << endl << "L2Norm2(delta_17_npd): " << L2Norm2(delta_17_npd) << endl;

            FlowField delta_17_pad = FlowField(u_17_pad);
            Controller_K::substract_ff(delta_17_pad, eq1_17_pad);
            cout << endl << "L2Norm2(delta_17_pad): " << L2Norm2(delta_17_pad) << endl;

            //delta_17_npd.makeState(Physical, Physical);
            //delta_17_pad.makeState(Physical, Physical);
            delta_17_npd.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/17x21x17/0/unpadded/delta_u"+i2s(t)+".h5");
            delta_17_pad.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/17x21x17/0/padded/delta_u"+i2s(t)+".h5");
            //delta_17_npd.makeState(Spectral, Spectral);
            //delta_17_pad.makeState(Spectral, Spectral);

            //-----------------------
            // I. REDUCE THEN SUBSTRACT
            //-----------------------

            //-----------------------
            // I.1. Reduce from u_32 (32x65x32) to u_I (17x21x21)
            //-----------------------
            FlowField u_I_npd = FlowField(_Nx_C, _Ny_C, _Nz_C, u_32_npd.Nd(), u_32_npd.Lx(), u_32_npd.Lz(), u_32_npd.a(), u_32_npd.b(), Spectral, Spectral);
            Controller_K::K_changegrid(u_I_npd, u_32_npd);
            //u_32_npd.makeState(Spectral, Spectral);
            //u_I_npd.makeState(Spectral, Spectral);
            cout << "L2Dist(u_17_npd, u_I_npd): " << L2Dist(u_17_npd, u_I_npd) << endl;
            cout << "L2Norm(u_I_npd): " << L2Norm2(u_I_npd) << endl;

            FlowField u_I_pad = FlowField(_Nx_C, _Ny_C, _Nz_C, u_32_pad.Nd(), u_32_pad.Lx(), u_32_pad.Lz(), u_32_pad.a(), u_32_pad.b(), Spectral, Spectral);
            Controller_K::K_changegrid(u_I_pad, u_32_pad);
            //u_32_pad.makeState(Spectral, Spectral);
            //u_I_pad.makeState(Spectral, Spectral);
            // here difference relative to u_17_npd to fit dimensions
            // how to calculate distance to eq1_17_pad (26x21x26) ?
            cout << "L2Dist(u_17_pad, u_I_pad): " << L2Dist(u_17_npd, u_I_pad) << endl;
            cout << "L2Norm(u_I_pad): " << L2Norm2(u_I_pad) << endl;

            FlowField u_I_48 = FlowField(_Nx_C, _Ny_C, _Nz_C, u_48.Nd(), u_48.Lx(), u_48.Lz(), u_48.a(), u_48.b(), Spectral, Spectral);
            Controller_K::K_changegrid(u_I_48, u_48);
            //u_48.makeState(Spectral, Spectral);
            //u_I_48.makeState(Spectral, Spectral);
            // here difference relative to u_17_npd to fit dimensions
            // how to calculate distance to eq1_17_pad (26x21x26) ?
            cout << "L2Dist(u_17_pad, u_I_48): " << L2Dist(u_17_npd, u_I_48) << endl;
            cout << "L2Norm(u_I_48): " << L2Norm2(u_I_48) << endl;

            /*
            cout << "u_I_npd : " << endl;
            cout << u_I_npd.Nx() << " / " << u_I_npd.Ny() << " / " << u_I_npd.Nz() << endl;
            cout << "L2Norm(u) : " << L2Norm2(u_I_npd) << endl;
            cout << "u_I_pad : " << endl;
            cout << u_I_pad.Nx() << " / " << u_I_pad.Ny() << " / " << u_I_pad.Nz() << endl;
            cout << "L2Norm(u) : " << L2Norm2(u_I_pad) << endl;
            cout << "u_32_npd : " << endl;
            cout << u_32_npd.Nx() << " / " << u_32_npd.Ny() << " / " << u_32_npd.Nz() << endl;
            cout << "L2Norm(u) : " << L2Norm2(u_32_npd) << endl;
            cout << "u_32_pad : " << endl;
            cout << u_32_pad.Nx() << " / " << u_32_pad.Ny() << " / " << u_32_pad.Nz() << endl;
            cout << "L2Norm(u) : " << L2Norm2(u_32_pad) << endl;
            */

            //-----------------------
            // I.2. Substract delta_17 = u_17 - eq1_17
            //-----------------------
            // remove the baseflow form the channelflow state
            // to deal with the perturbation only
            // CHFL considers the flowfield as u_EQ + u_pert
            // while OSSE consider only the flowfield as u_pert
            // so the controller from OSSE is controlling the perturbations
            // from a given _target_baseflow_flowfield only
            //-----------------------
            FlowField delta_I_npd = FlowField(u_I_npd);
            Controller_K::substract_ff(delta_I_npd, eq1_17_npd);
            cout << endl << "L2Norm2(delta_I_npd): " << L2Norm2(delta_I_npd) << endl;

            // here difference relative to u_17_npd to fit dimensions
            // TODO : how to calculate distance to eq1_17_pad (26x21x26) ?
            FlowField delta_I_pad = FlowField(u_I_pad);
            Controller_K::substract_ff(delta_I_pad, eq1_17_npd);
            cout << endl << "L2Norm2(delta_I_pad): " << L2Norm2(delta_I_pad) << endl;

            // here difference relative to u_17_npd to fit dimensions
            // TODO : how to calculate distance to eq1_17_pad (26x21x26) ?
            FlowField delta_I_48 = FlowField(u_I_48);
            Controller_K::substract_ff(delta_I_48, eq1_17_npd);
            cout << endl << "L2Norm2(delta_I_48): " << L2Norm2(delta_I_48) << endl;

            //delta_I_npd.makeState(Physical, Physical);
            //delta_I_pad.makeState(Physical, Physical);
            //delta_I_48.makeState(Physical, Physical);
            delta_I_npd.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/32x65x32/I/unpadded/delta_u"+i2s(t)+".h5");
            delta_I_pad.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/32x65x32/I/padded/delta_u"+i2s(t)+".h5");
            delta_I_48.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/32x65x32/I/nodealiasing/delta_u"+i2s(t)+".h5");
            //delta_I_npd.makeState(Spectral, Spectral);
            //delta_I_pad.makeState(Spectral, Spectral);
            //delta_I_48.makeState(Spectral, Spectral);

            //-----------------------
            // II. SUBSTRACT THEN REDUCE (what I do)

            //-----------------------
            // II.1. Substract delta_32 = u_32 - eq1_32
            //-----------------------
            FlowField delta_32_npd = FlowField(u_32_npd);
            Controller_K::substract_ff(delta_32_npd, eq1_32_npd);
            cout << endl << "L2Norm2(delta_32_npd) / eq1_32_npd: " << L2Norm2(delta_32_npd) << endl;
            FlowField delta_32_pad = FlowField(u_32_pad);
            Controller_K::substract_ff(delta_32_pad, eq1_32_pad);
            cout << endl << "L2Norm2(delta_32_pad) / eq1_32_pad: " << L2Norm2(delta_32_pad) << endl;
            FlowField delta_48 = FlowField(u_48);
            Controller_K::substract_ff(delta_48, eq1_32_pad);
            cout << endl << "L2Norm2(delta_48) / eq1_32_pad: " << L2Norm2(delta_48) << endl;

            //delta_32_npd.makeState(Physical, Physical);
            //delta_32_pad.makeState(Physical, Physical);
            //delta_48.makeState(Physical, Physical);
            delta_32_npd.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/32x65x32/0/unpadded/delta_u"+i2s(t)+".h5");
            delta_32_pad.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/32x65x32/0/padded/delta_u"+i2s(t)+".h5");
            delta_48.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/32x65x32/0/nodealiasing/delta_u"+i2s(t)+".h5");
            //delta_32_npd.makeState(Spectral, Spectral);
            //delta_32_pad.makeState(Spectral, Spectral);
            //delta_48.makeState(Spectral, Spectral);

            //-----------------------
            // II.2. Reduce delta_32 to 17
            //-----------------------
            FlowField delta_II_npd = FlowField(_Nx_C, _Ny_C, _Nz_C, u_32_npd.Nd(), u_32_npd.Lx(), u_32_npd.Lz(), u_32_npd.a(), u_32_npd.b(), Spectral, Spectral);
            Controller_K::K_changegrid(delta_II_npd, delta_32_npd);
            //delta_32_npd.makeState(Spectral, Spectral);
            //delta_II_npd.makeState(Spectral, Spectral);
            cout << "L2Dist(delta, delta_reduced) from 32_npd: " << L2Dist(delta_32_npd, delta_II_npd) << endl;
            cout << "L2Norm of the REDUCED to 17 perturbation from 32_npd: " << L2Norm2(delta_II_npd) << endl;

            FlowField delta_II_pad = FlowField(_Nx_C, _Ny_C, _Nz_C, u_32_pad.Nd(), u_32_pad.Lx(), u_32_pad.Lz(), u_32_pad.a(), u_32_pad.b(), Spectral, Spectral);
            Controller_K::K_changegrid(delta_II_pad, delta_32_pad);
            //delta_32_pad.makeState(Spectral, Spectral);
            //delta_II_pad.makeState(Spectral, Spectral);
            cout << "L2Dist(delta, delta_reduced) from 32_pad: " << L2Dist(delta_32_pad, delta_II_pad) << endl;
            cout << "L2Norm of the REDUCED to 17 perturbation from 32_pad: " << L2Norm2(delta_II_pad) << endl;

            FlowField delta_II_48 = FlowField(_Nx_C, _Ny_C, _Nz_C, u_48.Nd(), u_48.Lx(), u_48.Lz(), u_48.a(), u_48.b(), Spectral, Spectral);
            Controller_K::K_changegrid(delta_II_48, delta_48);
            //delta_48.makeState(Spectral, Spectral);
            //delta_II_48.makeState(Spectral, Spectral);
            cout << "L2Dist(delta, delta_reduced) from 48: " << L2Dist(delta_48, delta_II_48) << endl;
            cout << "L2Norm of the REDUCED to 17 perturbation from 48: " << L2Norm2(delta_II_48) << endl;

            cout << "Dist between delta_I_npd and delta_II_npd : " << L2Dist(delta_I_npd, delta_II_npd) << endl;
            cout << "Dist between delta_I_pad and delta_II_pad : " << L2Dist(delta_I_pad, delta_II_pad) << endl;
            cout << "Dist between delta_I_48 and delta_II_48 : " << L2Dist(delta_I_48, delta_II_48) << endl;

            //delta_II_npd.makeState(Physical, Physical);
            //delta_II_pad.makeState(Physical, Physical);
            //delta_II_48.makeState(Physical, Physical);
            delta_II_npd.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/32x65x32/II/unpadded/delta_u"+i2s(t)+".h5");
            delta_II_pad.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/32x65x32/II/padded/delta_u"+i2s(t)+".h5");
            delta_II_48.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u/32x65x32/II/nodealiasing/delta_u"+i2s(t)+".h5");

            //-----------------------
            // transform to Eigen::VectorXcd
            // Note: delta states needs to be (Spectral, Physical)
            //-----------------------
            delta_17_npd.makeState(Spectral, Physical);
            delta_17_pad.makeState(Spectral, Physical);
            delta_I_npd.makeState(Spectral, Physical);
            delta_I_pad.makeState(Spectral, Physical);
            delta_I_48.makeState(Spectral, Physical);
            delta_II_npd.makeState(Spectral, Physical);
            delta_II_pad.makeState(Spectral, Physical);
            delta_II_48.makeState(Spectral, Physical);

            // return the state of simulation delta_X_X_vector as an Eigen::VectorXcd
            // NOTE: The delta_X_X_vector includes the entire set
            // of spanwise modes as "mirror = true".
            // This is necessary as OSSE spans all the spanwise modes
            // but CHFL only the positive spanwise modes, hence the "mirror".
            Eigen::VectorXcd delta_17_npd_vector = Eigen::VectorXcd::Zero(_M_C_in);
            this->FlowField_to_ff_eigenVector(
                    delta_17_npd,
                    delta_17_npd_vector,
                    true);

            // delta_17_pad cant be used as dimensions differs
            // so I put delta_17_npd at the moment
            Eigen::VectorXcd delta_17_pad_vector = Eigen::VectorXcd::Zero(_M_C_in);
            this->FlowField_to_ff_eigenVector(
                    delta_17_npd,
                    delta_17_pad_vector,
                    true);

            Eigen::VectorXcd delta_I_npd_vector = Eigen::VectorXcd::Zero(_M_C_in);
            this->FlowField_to_ff_eigenVector(
                    delta_I_npd,
                    delta_I_npd_vector,
                    true);
            Eigen::VectorXcd delta_I_pad_vector = Eigen::VectorXcd::Zero(_M_C_in);
            this->FlowField_to_ff_eigenVector(
                    delta_I_pad,
                    delta_I_pad_vector,
                    true);
            Eigen::VectorXcd delta_I_48_vector = Eigen::VectorXcd::Zero(_M_C_in);
            this->FlowField_to_ff_eigenVector(
                    delta_I_48,
                    delta_I_48_vector,
                    true);

            Eigen::VectorXcd delta_II_npd_vector = Eigen::VectorXcd::Zero(_M_C_in);
            this->FlowField_to_ff_eigenVector(
                    delta_II_npd,
                    delta_II_npd_vector,
                    true);
            Eigen::VectorXcd delta_II_pad_vector = Eigen::VectorXcd::Zero(_M_C_in);
            this->FlowField_to_ff_eigenVector(
                    delta_II_pad,
                    delta_II_pad_vector,
                    true);
            Eigen::VectorXcd delta_II_48_vector = Eigen::VectorXcd::Zero(_M_C_in);
            this->FlowField_to_ff_eigenVector(
                    delta_II_48,
                    delta_II_48_vector,
                    true);

            MatOut_hdf5_complex(delta_17_npd_vector,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u_vector/17x21x17/0/unpadded/delta_u_vector"+i2s(t)+".h5");
            MatOut_hdf5_complex(delta_17_pad_vector,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u_vector/17x21x17/0/padded/delta_u_vector"+i2s(t)+".h5");
            MatOut_hdf5_complex(delta_I_npd_vector,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u_vector/32x65x32/I/unpadded/delta_u_vector"+i2s(t)+".h5");
            MatOut_hdf5_complex(delta_I_pad_vector,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u_vector/32x65x32/I/padded/delta_u_vector"+i2s(t)+".h5");
            MatOut_hdf5_complex(delta_I_48_vector,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u_vector/32x65x32/I/nodealiasing/delta_u_vector"+i2s(t)+".h5");
            MatOut_hdf5_complex(delta_II_npd_vector,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u_vector/32x65x32/II/unpadded/delta_u_vector"+i2s(t)+".h5");
            MatOut_hdf5_complex(delta_II_pad_vector,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u_vector/32x65x32/II/padded/delta_u_vector"+i2s(t)+".h5");
            MatOut_hdf5_complex(delta_II_48_vector,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/delta_u_vector/32x65x32/II/nodealiasing/delta_u_vector"+i2s(t)+".h5");

            // operator ff_eigenVector_to_FlowField again and verify that I get the same flowfield -> CHECKED
            /*
            FlowField u_reduced2 = FlowField(_Nx_C, _Ny_C, _Nz_C, u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Physical);
            this->ff_eigenVector_to_FlowField(
                    u_reduced_vector,
                    u_reduced2,
                    true);
            cout << "L2Dist u_reduced, u_reduced2 : " << L2Dist(u_reduced, u_reduced2) << endl;

            Eigen::VectorXcd u_reduced_vector2 = Eigen::VectorXcd::Zero(_M_C_in);
            this->FlowField_to_ff_eigenVector(
                    u_reduced2,
                    u_reduced_vector2,
                    true);

            Eigen::VectorXcd x_diff = Eigen::VectorXcd::Zero(_M_C_in);
            x_diff = u_reduced_vector2 - u_reduced_vector;
            cout << "u_reduced_vector2 - u_reduced_vector : " << x_diff.norm() << endl;

            // save u_reduced
            u_reduced.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced.h5");
            u_reduced2.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced2.h5");
            // save u_reduced_vector
            MatOut_hdf5_complex(u_reduced_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced_vector.h5");
            MatOut_hdf5_complex(u_reduced_vector2, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced_vector2.h5");
            std::cin.ignore();
            */

            //#############################################################################################################################################
            // Determine forcing q [yk in Heins controller class]
            //#############################################################################################################################################

            Eigen::VectorXcd q_vector_17_npd = Eigen::VectorXcd::Zero(_M_C_out);
            Eigen::VectorXcd q_vector_17_pad = Eigen::VectorXcd::Zero(_M_C_out);
            Eigen::VectorXcd q_vector_I_npd  = Eigen::VectorXcd::Zero(_M_C_out);
            Eigen::VectorXcd q_vector_I_pad  = Eigen::VectorXcd::Zero(_M_C_out);
            Eigen::VectorXcd q_vector_I_48   = Eigen::VectorXcd::Zero(_M_C_out);
            Eigen::VectorXcd q_vector_II_npd = Eigen::VectorXcd::Zero(_M_C_out);
            Eigen::VectorXcd q_vector_II_pad = Eigen::VectorXcd::Zero(_M_C_out);
            Eigen::VectorXcd q_vector_II_48 = Eigen::VectorXcd::Zero(_M_C_out);

            //cout << "    M_C_in  : " << _M_C_in << endl;
            //cout << "    M_C_out : " << _M_C_out << endl;
            //cout << "    K : " << K.rows() << " / " << K.cols() << endl;

            // the vector q_vector does only contain the positive modes in kz
            // and not the negative ones as they are not useful for later
            // calculation in channelflow
            // just bear in mind for the different dimensions, here use _Mz_C, as in _M_C_out
            // Limitating the actuation on positive mode is made in the OSSE with the method
            // operator.op_reduce_actuation_modes, so that the resulting matrix K of the
            // OSSE model is limited to actuation on the positive kz already.

            q_vector_17_npd = K * delta_17_npd_vector;
            q_vector_17_pad = K * delta_17_pad_vector;
            q_vector_I_npd  = K * delta_I_npd_vector;
            q_vector_I_pad  = K * delta_I_pad_vector;
            q_vector_I_48   = K * delta_I_48_vector;
            q_vector_II_npd = K * delta_II_npd_vector;
            q_vector_II_pad = K * delta_II_pad_vector;
            q_vector_II_48  = K * delta_II_48_vector;

            MatOut_hdf5_complex(q_vector_17_npd,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl/17x21x17/0/unpadded/q"+i2s(t)+".h5");
            MatOut_hdf5_complex(q_vector_17_pad,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl/17x21x17/0/padded/q"+i2s(t)+".h5");
            MatOut_hdf5_complex(q_vector_I_npd,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl/32x65x32/I/unpadded/q"+i2s(t)+".h5");
            MatOut_hdf5_complex(q_vector_I_pad,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl/32x65x32/I/padded/q"+i2s(t)+".h5");
            MatOut_hdf5_complex(q_vector_I_48,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl/32x65x32/I/nodealiasing/q"+i2s(t)+".h5");
            MatOut_hdf5_complex(q_vector_II_npd,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl/32x65x32/II/unpadded/q"+i2s(t)+".h5");
            MatOut_hdf5_complex(q_vector_I_pad,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl/32x65x32/II/padded/q"+i2s(t)+".h5");
            MatOut_hdf5_complex(q_vector_I_48,
                    "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl/32x65x32/II/nodealiasing/q"+i2s(t)+".h5");

            // CHECKED (test5): Verify that for a given state-vector given from OSSE, I get the same q in OSSE and in CHFL

            FlowField q_I_pad  = FlowField(_Nx_C, 2, _Nz_C, 1, BC_I_pad.Lx(), BC_I_pad.Lz(), BC_I_pad.a(), BC_I_pad.b(), Spectral, Physical);
            FlowField q_I_48   = FlowField(_Nx_C, 2, _Nz_C, 1, BC_I_pad.Lx(), BC_I_pad.Lz(), BC_I_pad.a(), BC_I_pad.b(), Spectral, Physical);
            FlowField q_II_pad = FlowField(_Nx_C, 2, _Nz_C, 1, BC_I_pad.Lx(), BC_I_pad.Lz(), BC_I_pad.a(), BC_I_pad.b(), Spectral, Physical);
            FlowField q_II_48  = FlowField(_Nx_C, 2, _Nz_C, 1, BC_I_pad.Lx(), BC_I_pad.Lz(), BC_I_pad.a(), BC_I_pad.b(), Spectral, Physical);

            this->ff_eigenVector_to_FlowField(q_vector_I_pad , q_I_pad , false);
            this->ff_eigenVector_to_FlowField(q_vector_I_48  , q_I_48  , false);
            this->ff_eigenVector_to_FlowField(q_vector_II_pad, q_II_pad, false);
            this->ff_eigenVector_to_FlowField(q_vector_II_48 , q_II_48 , false);

            //q_I_48.makeState(Physical, Physical);
            //q_II_pad.makeState(Physical, Physical);
            //q_II_48.makeState(Physical, Physical);
            //q_I_pad.makeState(Physical, Physical);
            q_I_pad.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl_ff/I/padded/q"+i2s(t)+".h5");
            q_I_48.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl_ff/I/nodealiasing/q"+i2s(t)+".h5");
            q_II_pad.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl_ff/II/padded/q"+i2s(t)+".h5");
            q_II_48.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/q_chfl_ff/II/nodealiasing/q"+i2s(t)+".h5");
            //q_I_pad.makeState(Spectral, Physical);
            //q_I_48.makeState(Spectral, Physical);
            //q_II_pad.makeState(Spectral, Physical);

            //q_II_48.makeState(Spectral, Physical);
            //continue;

            //FlowField q  = FlowField(_Nx_C, 2, _Nz_C, 1, BC_I_pad.Lx(), BC_I_pad.Lz(), BC_I_pad.a(), BC_I_pad.b(), Spectral, Physical);

            // Operator FlowField_to_ff_eigenVector and verify that q is equal to q_vector: CHECKED
            // however it is hard to know if the indexation is ok if mirror = false,
            // on the transformation in and back, it worked, so i think it is validated.
            // but bear in mind that q is only for positive kz mode

            //#############################################################################################################################################
            // UPDATE THE FLOWFIELD BC
            //#############################################################################################################################################

            // Crank-Nicholson low-pass filter integration
            int tau_test7 = 1;
            double alpha_lpf = 1 + (dT / (2 * tau_test7));
            double beta_lpf =  1 - (dT / (2 * tau_test7));

            // This for-loop should fit the BCs dimensions. However, as only the modes of the controller
            // are actually actuated, I restrict the loop to _Mx_C and _Mz_C instead of BC.Mx() and BC.Mz().
            // All the high-dimension modes are not actuated.
            int mx_inloop_pad;
            int mx_inloop_48;
            for (int mx = 0; mx < _Mx_C; ++mx) {
                // as q is of smaller dimension than BC, we use mx_inloop to fit the right modes pairs together.
                // not a problem for mz as all the kz are positive.
                // here mx corresponds to a mode of q while mx_inloop corresponds to the same corresonding mode for BC
                mx_inloop_pad = BC_I_pad.mx(q_I_pad.kx(mx));
                mx_inloop_48  = BC_I_48.mx(q_I_48.kx(mx));
                //cout << "mx : " << mx << " / q.kx : " << q.kx(mx) << " / mx_inloop " << mx_inloop << endl;
                for (int mz = 0; mz < _Mz_C; ++mz) {
                    //lower wall, for u, v and w, already 0 for i=0 (u) and i=2 (w)
                    BC_I_pad.cmplx(mx_inloop_pad, 0, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC_I_pad.cmplx(mx_inloop_pad, 0, mz, 1)) + ((dT / tau_test7) * q_I_pad.cmplx(mx, 0, mz, 0)) );
                    BC_I_48.cmplx(mx_inloop_48, 0, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC_I_48.cmplx(mx_inloop_48, 0, mz, 1)) + ((dT / tau_test7) * q_I_48.cmplx(mx, 0, mz, 0)) );
                    BC_II_pad.cmplx(mx_inloop_pad, 0, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC_II_pad.cmplx(mx_inloop_pad, 0, mz, 1)) + ((dT / tau_test7) * q_II_pad.cmplx(mx, 0, mz, 0)) );
                    BC_II_48.cmplx(mx_inloop_48, 0, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC_II_48.cmplx(mx_inloop_48, 0, mz, 1)) + ((dT / tau_test7) * q_II_48.cmplx(mx, 0, mz, 0)) );
                    //upper  wall, for u, v and w, alreadu 0 for i=0 (u) and i=2 (w)
                    BC_I_pad.cmplx(mx_inloop_pad, 1, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC_I_pad.cmplx(mx_inloop_pad, 1, mz, 1)) + ((dT / tau_test7) * q_I_pad.cmplx(mx, 1, mz, 0)) );
                    BC_I_48.cmplx(mx_inloop_48, 1, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC_I_48.cmplx(mx_inloop_48, 1, mz, 1)) + ((dT / tau_test7) * q_I_48.cmplx(mx, 1, mz, 0)) );
                    BC_II_pad.cmplx(mx_inloop_pad, 1, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC_II_pad.cmplx(mx_inloop_pad, 1, mz, 1)) + ((dT / tau_test7) * q_II_pad.cmplx(mx, 1, mz, 0)) );
                    BC_II_48.cmplx(mx_inloop_48, 1, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * BC_II_48.cmplx(mx_inloop_48, 1, mz, 1)) + ((dT / tau_test7) * q_II_48.cmplx(mx, 1, mz, 0)) );
                }
            }

            BC_I_pad.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/BC/I/padded/BC"+i2s(t)+".h5");
            BC_I_48.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/BC/I/nodealiasing/BC"+i2s(t)+".h5");
            BC_II_pad.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/BC/II/padded/BC"+i2s(t)+".h5");
            BC_II_48.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test7/BC/II/nodealiasing/BC"+i2s(t)+".h5");

            // low-pass filter integration VALIDATED (test6)

            /*
            cout << setprecision(3) << endl;
            for (int mx = 0; mx < Mx; mx++) {
                kx = BC.kx(mx);
                cout << mx << "|" << kx << "    : ";
                for (int mz = 0; mz < Mz; mz++) {
                    //mz = BC.mz(kz);
                    cout << round(real(BC.cmplx(mx, 0, mz, 1))*100)/100 << "+(" << round(imag(BC.cmplx(mx, 0, mz, 1))*100)/100 << ")i | ";
                }
                cout << endl;
            }
            cout << "END" << endl;
            exit(1);
            */
        }

        _target_baseflow_flowfield.makeState(target_xzstate, target_ystate);
        BC_I_pad.makeState(BC_xzstate, BC_ystate);
        BC_I_48.makeState(BC_xzstate, BC_ystate);
        BC_II_pad.makeState(BC_xzstate, BC_ystate);
        BC_II_48.makeState(BC_xzstate, BC_ystate);
    }

    void Controller_OSSE::test6()
    {
        // this method validated the low pass filter integration
        // i actuate the flowfield BC and compare the time evolution
        // of BC and q along time

        FlowField _BC = FlowField(32, 2, 32, 3, 6, 4, 1, -1);
        for (int ix = 0; ix < 32; ++ix) {
            for (int iz = 0; iz < 32; ++iz) {
                for (int ii = 0; ii < 3; ++ii) {
                    _BC(ix, 0, iz, ii) = 0.0;
                    _BC(ix, 1, iz, ii) = 0.0;
                }
            }
        }
        _BC.makeState(Spectral, Physical);

        int Nx_C = 11;
        int Nz_C = 11;
        FlowField q = FlowField(Nx_C, 2, Nz_C, 1, _BC.Lx(), _BC.Lz(), _BC.a(), _BC.b(), Spectral, Physical);
        Real dT = 1.0;
        double tau = 2;
        int Tmax = 100;

        int Mx_C = q.Mx();
        int Mz_C = q.Mz();

        // Crank-Nicholson low-pass filter integration
        int mx_inloop;
        double alpha_lpf = 1 + (dT / (2 * tau));
        double beta_lpf =  1 - (dT / (2 * tau));

        for (double t = 0; t < Tmax; t += dT)
        {
            cout << " t = " << i2s(t) << endl;
            if (t < 10) {
                for (int ix = 0; ix < Mx_C; ++ix) {
                    for (int iz = 0; iz < Mz_C; ++iz) {
                        q.cmplx(ix, 0, iz, 0) = Complex(-40.0, -40.0);
                        q.cmplx(ix, 1, iz, 0) = Complex(-40.0, -40.0);
                    }
                }
            } else if (t >= 10 && t < 20) {
                for (int ix = 0; ix < Mx_C; ++ix) {
                    for (int iz = 0; iz < Mz_C; ++iz) {
                        q.cmplx(ix, 0, iz, 0) = Complex(-2.0, -2.0);
                        q.cmplx(ix, 1, iz, 0) = Complex(-2.0, -2.0);
                    }
                }
            } else if (t >= 20) {
                for (int ix = 0; ix < Mx_C; ++ix) {
                    for (int iz = 0; iz < Mz_C; ++iz) {
                        q.cmplx(ix, 0, iz, 0) = Complex(-20.0, -10.0);
                        q.cmplx(ix, 1, iz, 0) = Complex(-20.0, -10.0);
                    }
                }
            }

            // This for-loop should fit the BCs dimensions. However, as only the modes of the controller
            // are actually actuated, I restrict the loop to _Mx_C and _Mz_C instead of BC.Mx() and BC.Mz().
            // All the high-dimension modes are not actuated.
            for (int mx = 0; mx < Mx_C; ++mx) {
                // as q is of smaller dimension than BC, we use mx_inloop to fit the right modes pairs together.
                // not a problem for mz as all the kz are positive.
                // here mx corresponds to a mode of q while mx_inloop corresponds to the same corresonding mode for BC
                mx_inloop = _BC.mx(q.kx(mx));
                //cout << " / mx_inloop = " << i2s(mx_inloop) << endl;
                //cout << "mx : " << mx << " / q.kx : " << q.kx(mx) << " / mx_inloop " << mx_inloop << endl;
                for (int mz = 0; mz < Mz_C; ++mz) {
                    //lower wall, for u, v and w, already 0 for i=0 (u) and i=2 (w)
                    _BC.cmplx(mx_inloop, 0, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * _BC.cmplx(mx_inloop, 0, mz, 1)) + ((dT / tau) * q.cmplx(mx, 0, mz, 0)) );
                    //* (yk[0] + yk[2] * I)) );
                    //q.cmplx(mx, 0, mz, 0);
                    //upper  wall, for u, v and w, alreadu 0 for i=0 (u) and i=2 (w)
                    _BC.cmplx(mx_inloop, 1, mz, 1) =
                        (1 / alpha_lpf) * ( (beta_lpf * _BC.cmplx(mx_inloop, 1, mz, 1)) + ((dT / tau) * q.cmplx(mx, 1, mz, 0)) );
                    //* (yk[0] + yk[2] * I)) );
                    // q.cmplx(mx, 1, mz, 0);
                }
            }

            cout << q.cmplx(0,0,0,0) << endl;
            cout << _BC.cmplx(0,1,0,1) << endl;
            // save BC

            // IMPORTANT NOTE:
            // it seems that if I actuated with an imaganiry part
            // when using makeState(Physical, Physical)
            // and then makeState(Spectral, Physcal).
            // The imaginary part disappears.
            // Perhaps it is normal because i am not balancing the imaginary part ?

            //q.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/low-pass-int/q_t="+i2s(t)+".h5");
            //_BC.save("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/low-pass-int/BC_t="+i2s(t)+".h5");
            //_BC.makeState(Spectral, Physical);

        }

        std::cin.ignore();
    }

    void Controller_OSSE::test5()
    {
        // Verify that for a given state-vector given from OSSE, I get the same q in OSSE and in CHFL -> CHECKED

        Eigen::VectorXcd u_reduced_vector = Eigen::VectorXcd::Zero(_M_C_in);
        Eigen::VectorXcd q_vector = Eigen::VectorXcd::Zero(_M_C_out);
        MatIn_hdf5_complex(u_reduced_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u150_17x21x17_vector.h5");
        q_vector = K * u_reduced_vector;
        cout << "saving u_reduced_vector" << endl;
        MatOut_hdf5_complex(u_reduced_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u_reduced_vector.h5");
        cout << "saving q_vector" << endl;
        MatOut_hdf5_complex(q_vector, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/q_vector.h5");
        std::cin.ignore();
    }

    void Controller_OSSE::test4()
    {
        // import a flowfield (ff0) and convert it to eigenVector (x1)
        // compare the result with the one obtained with the OSSE model (x0)
        //
        // then transform back the eigenVector (x1) into a flowfield (ff1)
        // and compare if ff1 = ff0
        //
        // and then do it again and compare the difference
        //
        // in OSSE, I did :
        // ff = FlowField(...)
        // ff.make_spectral('xz')
        // ope = operators(ff)
        // x = ope.transform_spectral_uvw_array_to_vector(ff, ff.u, truncation=False)
        // OP.save_mat_cmplx_hdf5(x, foldername, filename)
        //
        // x2 = ope.load_vector_hdf5(filename) # where x2 is the vector produced from ff within Channelflow (x1)
        // numpy.linalg.norm(x2 - x) # null

        FlowField ff0 = FlowField("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u150_17x21x17_unpadded.h5");
        Eigen::VectorXcd x0 = Eigen::VectorXcd::Zero(_M_C_in);
        MatIn_hdf5_complex(x0, "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u150_17x21x17_vector.h5");

        FlowField ff1 = FlowField(_Nx_C, _Ny_C, _Nz_C, ff0.Nd(), ff0.Lx(), ff0.Lz(), ff0.a(), ff0.b(), Spectral, Physical);
        FlowField ff2 = FlowField(_Nx_C, _Ny_C, _Nz_C, ff0.Nd(), ff0.Lx(), ff0.Lz(), ff0.a(), ff0.b(), Spectral, Physical);
        Eigen::VectorXcd x1 = Eigen::VectorXcd::Zero(_M_C_in);
        Eigen::VectorXcd x2 = Eigen::VectorXcd::Zero(_M_C_in);
        Eigen::VectorXcd x_diff = Eigen::VectorXcd::Zero(_M_C_in);
        Eigen::VectorXcd x_diff1 = Eigen::VectorXcd::Zero(_M_C_in);
        Eigen::VectorXcd x_diff2 = Eigen::VectorXcd::Zero(_M_C_in);

        ff0.makeState(Spectral, Physical);
        ff1.makeState(Spectral, Physical);
        ff2.makeState(Spectral, Physical);

        this->FlowField_to_ff_eigenVector(
                ff0,
                x1,
                true);
        // I should get x1 = x0
        cout << "x0 : " << x0.norm() << endl;
        cout << "x1 : " << x1.norm() << endl;
        x_diff = x1 - x0;
        cout << "x1 - x0 : " << x_diff.norm() << endl;

        MatOut_hdf5_complex(
            x1,
            "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u150_unpadded_vector.h5");

        this->ff_eigenVector_to_FlowField(
                x1,
                ff1,
                true);
        // I should get ff1 = ff0
        cout << "L2Dist ff1, ff0 : " << L2Dist(ff1, ff0) << endl;

        this->FlowField_to_ff_eigenVector(
                ff1,
                x2,
                true);
        x_diff1 = x2 - x0;
        x_diff2 = x2 - x1;
        cout << "x2 - x0 : " << x_diff1.norm() << endl;
        cout << "x2 - x1 : " << x_diff2.norm() << endl;

        this->ff_eigenVector_to_FlowField(
                x2,
                ff2,
                true);
        cout << "L2Dist ff2, ff0 : " << L2Dist(ff2, ff0) << endl;
        cout << "L2Dist ff2, ff1 : " << L2Dist(ff2, ff1) << endl;

        //int kx0 = 1;
        //int kz0 = 1; //u_reduced.mz(kz);
        //int kx1 = -1;
        //int kz1 = -1;
        //int nd = 0;

        //cout << " ----------------- " << endl;
        //for (int ii = 0; ii < 21; ++ii) {
        //    cout << x_vector(nd * (_Nx_C*_Ny_C*_Nz_C) + kx0 * (_Ny_C*_Nz_C) + kz0 * _Ny_C + ii) << " | "
        //        << x_ff.cmplx(kx0,ii,kz0,nd) << " | "
        //        << x_ff.cmplx(kx1,ii,kz1,nd) << " | " << endl;
        //}
        //cout << " ----------------- " << endl;
        //for (int ii = 0; ii < 21; ++ii) {
        //    cout << x_vector(nd * (_Nx_C*_Ny_C*_Nz_C) + mx * (_Ny_C*_Nz_C) + kz1 * _Ny_C + ii) << " | "
        //        << x_ff.cmplx(mx,ii,mz,nd) << " | "
        //        << x_vector2(nd * (_Nx_C*_Ny_C*_Nz_C) + mx * (_Ny_C*_Nz_C) + kz1 * _Ny_C + ii) << " | "
        //        << x_ff2.cmplx(mx,ii,mz,nd) << endl;
        //}

        std::cin.ignore();
    }

    void Controller_OSSE::test3()
    {
        /*
        This method verify that the method FlowField_to_eigenVector
        is transforming the field into the right order
        and as in the OSSE model
        */
        int Nxx = 17;
        int Nyy = 21;
        int Nzz = 17;
        int M_in = 3 * Nxx * Nyy * Nzz;
        FlowField u_ff = FlowField("/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/u150_17x21x17_unpadded.h5");
        //FlowField u_ff_red = FlowField(_Nx_C, _Ny_C, _Nz_C, u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Physical);
        //u_ff_red.interpolate(u_ff);
        FlowField u_ff_red = FlowField(u_ff);
        u_ff_red.makeState(Spectral, Physical);

        int kx0 = 1;
        int kz0 = 1; //u_reduced.mz(kz);
        int kx1 = Nxx - kx0;
        int kz1 = Nzz - kz0;
        int kx2 = kx0;
        int kz2 = Nzz - kz0;
        int kx3 = Nxx - kx0;
        int kz3 = kz0;
        int nd = 2;

        std::vector<int> freq_Z = Controller_K::numpy_fftfreq(Nzz, 1/Nzz);
        for (int iz = 0; iz < Nzz; ++iz) {
            cout << i2s(freq_Z[iz]) << " | " ;
        } cout << endl;
        cout << "Mz : " << u_ff.Mz() << endl;
        cout << "nd : " << nd << endl;

        Eigen::VectorXcd u_v = Eigen::VectorXcd::Zero(M_in);
        this->FlowField_to_ff_eigenVector(
                u_ff_red,
                u_v,
                true);

        MatOut_hdf5_complex(
            u_v,
            "/home/gcpc1m15/osse/database/DEBUG/CHFL_export_debug_control/test_unpadded_vector.h5");
        cout << u_ff_red.Mx() << endl;
        cout << u_ff_red.My() << endl;
        cout << u_ff_red.Mz() << endl;

        // Compare the vector u_v for each different conjugate mode

        cout << " ----------------- " << endl;
        cout << "kx0 : " << kx0 << endl;
        cout << "kz0 : " << kz0 << endl;
        for (int ii = 0; ii < Nyy; ++ii) {
            cout << i2s(ii) << " | "
                 << i2s(nd * (Nxx*Nyy*Nzz) + kx0 * (Nyy*Nzz) + kz0 * Nyy + ii) << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx0 * (Nyy*Nzz) + kz0 * Nyy + ii).real() << endl;
        }
        cout << " ----------------- " << endl;
        cout << "kx1 : " << kx1 << endl;
        cout << "kz1 : " << kz1 << endl;
        for (int ii = 0; ii < Nyy; ++ii) {
            cout << i2s(ii) << " | "
                 << i2s(nd * (Nxx*Nyy*Nzz) + kx1 * (Nyy*Nzz) + kz1 * Nyy + ii) << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx1 * (Nyy*Nzz) + kz1 * Nyy + ii).real() << endl;
        }
        cout << " ----------------- " << endl;
        cout << "kx2 : " << kx2 << endl;
        cout << "kz2 : " << kz2 << endl;
        for (int ii = 0; ii < Nyy; ++ii) {
            cout << i2s(ii) << " | "
                 << i2s(nd * (Nxx*Nyy*Nzz) + kx2 * (Nyy*Nzz) + kz2 * Nyy + ii) << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx2 * (Nyy*Nzz) + kz2 * Nyy + ii).real() << endl;
        }
        cout << " ----------------- " << endl;
        cout << "kx3 : " << kx3 << endl;
        cout << "kz3 : " << kz3 << endl;
        for (int ii = 0; ii < Nyy; ++ii) {
            cout << i2s(ii) << " | "
                 << i2s(nd * (Nxx*Nyy*Nzz) + kx3 * (Nyy*Nzz) + kz3 * Nyy + ii) << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx3 * (Nyy*Nzz) + kz3 * Nyy + ii).real() << endl;
        }

        //std::cin.ignore();

        cout << " -------REAL---------- " << endl;
        for (int ii = 0; ii < Nyy; ++ii) {
            cout << i2s(ii) << " | ";
            cout << u_ff_red.cmplx(kx0,ii,kz0,nd).real() << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx0 * (Nyy*Nzz) + kz0 * Nyy + ii).real() << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx1 * (Nyy*Nzz) + kz1 * Nyy + ii).real() << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx2 * (Nyy*Nzz) + kz2 * Nyy + ii).real() << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx3 * (Nyy*Nzz) + kz3 * Nyy + ii).real() << " | "
                 << u_ff_red.cmplx(kx3,ii,kz3,nd).real() << endl;
        }
        cout << " -------IMAG---------- " << endl;
        for (int ii = 0; ii < Nyy; ++ii) {
            cout << i2s(ii) << " | ";
            cout << u_ff_red.cmplx(kx0,ii,kz0,nd).imag() << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx0 * (Nyy*Nzz) + kz0 * Nyy + ii).imag() << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx1 * (Nyy*Nzz) + kz1 * Nyy + ii).imag() << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx2 * (Nyy*Nzz) + kz2 * Nyy + ii).imag() << " | "
                 << u_v(nd * (Nxx*Nyy*Nzz) + kx3 * (Nyy*Nzz) + kz3 * Nyy + ii).imag() << " | "
                 << u_ff_red.cmplx(kx3,ii,kz3,nd).imag() << endl;
        }
        cout << " ----------------- " << endl;

        //for (int ii = 0; ii < 21; ++ii) {

        std::cin.ignore();
    }

    void Controller_OSSE::test2()
    {
        // CHECK THE INTERPOLATION

        cout << setprecision(3);
        cout << "------------------------------------" << endl;
        cout << "Dimensions of _ff : " << endl;
        cout << "Nx : " << _ff.Nx() << endl;
        cout << "Ny : " << _ff.Ny() << endl;
        cout << "Nz: " << _ff.Nz() << endl;
        cout << "Mx : " << _ff.Mx() << endl;
        cout << "My : " << _ff.My() << endl;
        cout << "Mz : " << _ff.Mz() << endl;
        cout << "_ff" << endl;
        int kx;
        for (int mx = 0; mx < _ff.Mx(); mx++) {
            kx = _ff.kx(mx);
            cout << mx << "|" << kx << "    : ";
            for (int mz = 0; mz < _ff.Mz(); mz++) {
                //mz = BC.mz(kz);
                cout << real(_ff.cmplx(mx, 2, mz, 1))*100/100 << "+(" << imag(_ff.cmplx(mx, 2, mz, 1))*100/100 << ")i | ";
            }
            cout << endl;
        }


        FlowField ff_reduced = FlowField(_Nx_C, _Ny_C, _Nz_C, _ff.Nd(), _ff.Lx(), _ff.Lz(), _ff.a(), _ff.b(), Spectral, Spectral);
        ff_reduced.interpolate(_ff);

        cout << "------------------------------------" << endl;
        cout << "Dimensions of ff_reduced : " << endl;
        cout << "Nx : " << ff_reduced.Nx() << endl;
        cout << "Ny : " << ff_reduced.Ny() << endl;
        cout << "Nz: " << ff_reduced.Nz() << endl;
        cout << "Mx : " << ff_reduced.Mx() << endl;
        cout << "My : " << ff_reduced.My() << endl;
        cout << "Mz : " << ff_reduced.Mz() << endl;
        cout << "ff_reduced interpolated" << endl;
        for (int mx = 0; mx < ff_reduced.Mx(); mx++) {
            kx = ff_reduced.kx(mx);
            cout << mx << "|" << kx << "    : ";
            for (int mz = 0; mz < ff_reduced.Mz(); mz++) {
                //mz = BC.mz(kz);
                cout << real(ff_reduced.cmplx(mx, 15, mz, 1))*100/100 << "+(" << imag(ff_reduced.cmplx(mx, 15, mz, 1))*100/100 << ")i | ";
            }
            cout << endl;
        }
    }

    void Controller_OSSE::test1()
    {
        // CHECK THE PADDING

        cout << setprecision(3);
        cout << "------------------------------------" << endl;
        cout << "test 1 " << endl;
        cout << "padded : " << _target_baseflow_flowfield.padded() << endl;
        const int target_Nx = _target_baseflow_flowfield.Nx();
        const int target_Ny = _target_baseflow_flowfield.Ny();
        const int target_Nz = _target_baseflow_flowfield.Nz();
        //const int target_Nd = _target_baseflow_flowfield.Nd();
        const int target_Mx = _target_baseflow_flowfield.Mx();
        const int target_My = _target_baseflow_flowfield.My();
        const int target_Mz = _target_baseflow_flowfield.Mz();
        cout << "Nx : " << target_Nx << endl;
        cout << "Ny : " << target_Ny << endl;
        cout << "Nz: " << target_Nz << endl;
        cout << "Mx : " << target_Mx << endl;
        cout << "My : " << target_My << endl;
        cout << "Mz : " << target_Mz << endl;

        cout << "------------------------------------" << endl;
        cout << "padded flowfield" << endl;
        int kx;
        for (int mx = 0; mx < _target_baseflow_flowfield.Mx(); mx++) {
            kx = _target_baseflow_flowfield.kx(mx);
            cout << mx << "|" << kx << "    : ";
            for (int mz = 0; mz < _target_baseflow_flowfield.Mz(); mz++) {
                //mz = BC.mz(kz);
                cout << real(_target_baseflow_flowfield.cmplx(mx, 2, mz, 1))*100/100
                    << "+(" << imag(_target_baseflow_flowfield.cmplx(mx, 2, mz, 1))*100/100 << ")i | ";
            }
            cout << " << " <<  endl;
        }
    }

    void Controller_OSSE::test0()
    {
        // CHECK SUBTRACT FLOWFIELD

        cout << setprecision(3);
        cout << " test 0 " << endl;
        double Lx = _target_baseflow_flowfield.Lx();
        double Lz = _target_baseflow_flowfield.Lz();
        double a = _target_baseflow_flowfield.a();
        double b = _target_baseflow_flowfield.b();

        cout << "--------------------"<< endl;
        cout << "Essai 1 : " << endl;
        FlowField essai1 = FlowField(12, 35 , 10, 3, Lx, Lz, a, b);
        cout << essai1.Nx() << endl;
        cout << essai1.Ny() << endl;
        cout << essai1.Nz() << endl;
        //essai1.makeState(Spectral, Physical);
        essai1.makeSpectral_xz();
        for (int mx = 0; mx < essai1.Mx(); ++mx) {
            for (int my = 0; my < essai1.My(); ++my) {
                for (int mz = 0; mz < essai1.Mz(); ++mz) {
                    essai1.cmplx(mx, my, mz, 0) = 0; //1+1*1il;
                }
            }
        }

        cout << "--------------------"<< endl;
        cout << "Essai 2 : " << endl;
        FlowField essai2 = FlowField(10, 25 , 8, 3, Lx, Lz, a, b);
        cout << essai2.Nx() << endl;
        cout << essai2.Ny() << endl;
        cout << essai2.Nz() << endl;
        //essai2.makeState(Spectral, Physical);
        essai2.makeSpectral_xz();
        for (int mx = 0; mx < essai2.Mx(); ++mx) {
            for (int my = 0; my < essai2.My(); ++my) {
                for (int mz = 0; mz < essai2.Mz(); ++mz) {
                    essai2.cmplx(mx, my, mz, 0) = 1.0 + 2.0 * 1i;
                }
            }
        }

        for (int mx = 0; mx < essai1.Mx(); ++mx) {
            for (int mz = 0; mz < essai1.Mz(); ++mz) {
                cout << essai1.cmplx(mx, 0, mz, 0) << " | ";
            }
            cout << " < " << endl;
        }
        cout << "END" << endl;

        // 2. remove the baseflow form the channelflow state
        //cout << "    SUBSTRACT FF" << endl;
        Controller_K::substract_ff(essai1, essai2); // return essai1 - essai2

        for (int mx = 0; mx < essai1.Mx(); ++mx) {
            for (int mz = 0; mz < essai1.Mz(); ++mz) {
                cout << essai1.cmplx(mx, 0, mz, 0) << " | ";
            }
            cout << " < " << endl;
        }
        cout << "END" << endl;
    }

    void Controller_OSSE::test()
    {
        cout << setprecision(3);
        cout << "TEST" << endl;
        int Nd = 3;
        int Nx = 3;
        int Ny = 1;
        int Nz = 4;
        //int N = Nx * Ny * Nz * Nd;

        FlowField ff = FlowField(Nx, Ny, Nz, Nd, 2, 2, 1, -1);
        cout << "DIMENSION : " << ff.Nx() << " / " << ff.Ny() << " / " << ff.Nz() << " / " << ff.Nd() << endl;
        cout << "MODES : " << ff.Mx() << " / " << ff.My() << " / " << ff.Mz() << " / " << ff.Nd() << endl;
        int Mx = ff.Mx();
        int My = ff.My();
        int Mz = ff.Mz();
        int M = ff.Mx() * ff.My() * ff.Mz() * ff.Nd();
        cout << "total modes : " << M << endl;

        /*
        cout << " ----------- " << endl;
        cout << ff.kzmin() << endl;
        cout << ff.kzmax() << endl;
        cout << ff.Mz() << endl;
        cout << " ----------- " << endl;
        cout << ff.mz(0) << endl;
        cout << ff.mz(1) << endl;
        cout << ff.mz(2) << endl;
        cout << ff.mz(3) << endl;
        cout << ff.mz(-2) << endl;
        cout << ff.mz(-1) << endl;
        cout << " ----------- " << endl;
        */

        Eigen::VectorXcd vect = Eigen::VectorXcd(M);
        for (int i = 0; i< M; ++i)
            vect(i) = 0;

        int mx1 = 1;
        int mz1 = 0;
        int i1 = 0;
        int M1 = i1 * (Mx*My*Mz) + mx1 * (My*Mz) + mz1 * My  + 0;
        cout << " M1 : " << M1 << endl;

        int mx2 = 0;
        int mz2 = 1;
        int i2 = 0;
        int M2 = i2 * (Mx*My*Mz) + mx2 * (My*Mz) + mz2 * My  + 0;
        cout << " M2 : " << M2 << endl;

        int mx3 = 2;
        int mz3 = 2;
        int i3 = 2;
        int M3 = i3 * (Mx*My*Mz) + mx3 * (My*Mz) + mz3 * My  + 0;
        cout << " M3 : " << M3 << endl;

        cout << endl;
        cout << "VECT" << endl;
        for (int my = 0; my < My; ++my)
        {
            vect[i1 * (Mx*My*Mz) + mx1 * (My*Mz) + mz1 * My  + my] = 310;
            vect[i2 * (Mx*My*Mz) + mx2 * (My*Mz) + mz2 * My  + my] = 101;
            vect[i3 * (Mx*My*Mz) + mx3 * (My*Mz) + mz3 * My  + my] = 411;
        }
        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mx = 0; mx < ff.Mx(); ++mx) {
                for (int mz = 0; mz < ff.Mz(); ++mz) {
                    cout << i << "/" << mx << "/" << mz
                         << " =  " << i * (Mx*My*Mz) + mx * (My*Mz) + mz * My  + 0
                         << " : "  << vect[i * (Mx*My*Mz) + mx * (My*Mz) + mz * My  + 0]
                         << endl;
                }
            }
            cout << "----" << endl;
        }

        cout << endl;
        cout << "ff" << endl;
        this->ff_eigenVector_to_FlowField(vect, ff, false);
        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mz = 0; mz < ff.Mz(); ++mz) {
                for (int mx = 0; mx < ff.Mx(); ++mx) {
                    cout << ff.cmplx(mx, 0, mz, i) << " ";
                }
                cout << endl;
            }
            cout << " ----------- " << endl;
        }

        cout << endl;
        cout << "VECT" << endl;
        this->FlowField_to_ff_eigenVector(ff, vect, false);
        for (int i = 0; i < ff.Nd(); ++i) {
            for (int mx = 0; mx < ff.Mx(); ++mx) {
                for (int mz = 0; mz < ff.Mz(); ++mz) {
                    cout << i << "/" << mx << "/" << mz
                         << " =  " << i * (Mx*My*Mz) + mx * (My*Mz) + mz * My  + 0
                         << " : "  << vect[i * (Mx*My*Mz) + mx * (My*Mz) + mz * My  + 0]
                         << endl;
                }
            }
            cout << "----" << endl;
        }
    }


    //############################################################
    // HDF5 reading functions
    // Created by Geoffroy Claisse
    // University of Southampton, 08/2018
    //############################################################

    #ifdef HAVE_HDF5
    int hdf5read(double **matrix, const size_t N0, const size_t N1, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the matrix
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank > 2) {
                channelflow::cferror("Matrix rank should be 1 or 2.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, nullptr);
            int N = dims_out[0] * dims_out[1];
            /*
               cout << rank << " / Dimensions : "
               << (unsigned long) (dims_out[0]) << " x "
               << (unsigned long) (dims_out[1]) << endl;
            //*/
            if (dims_out[0] != N0 && dims_out[1] != N1)
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(matrix, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset matrix size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into matrix
            auto* buff = new double[N];
            //cout << typeid(buff).name() << endl;

            dataset.read(buff, H5::PredType::IEEE_F64BE, memspace, dataspace);
            H5Tconvert(H5T_IEEE_F64BE, H5T_NATIVE_DOUBLE, N, buff, nullptr, H5P_DEFAULT);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                for (size_t jj = 0; jj < dims_out[1]; jj++) {
                    matrix[ii][jj] = buff[ii * dims_out[0] + jj];
                }
            }

            dataset.close();
            delete[] buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException &error ) {
            cout << "Error hdf5read_real FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException &error ) {
            cout << "Error hdf5read_real DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException &error ) {
            cout << "Error hdf5read_real DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException &error ) {
            cout << "Error hdf5read_real DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5read_real(Eigen::MatrixXcd& matrix, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the matrix
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank != 2) {
                channelflow::cferror("Matrix rank should be 2.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, nullptr);
            //dataspace.getSimpleExtentDims(dims_out, nullptr);

            int N = dims_out[0] * dims_out[1];

            /*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " x "
                << (unsigned long) (dims_out[1]) << " / "
                << N << endl;
            //*/

            if ((unsigned) dims_out[0] != matrix.rows()
                    || (unsigned) dims_out[1] != matrix.cols())
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(matrix, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset matrix size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into matrix
            auto* buff = new double[N];
            dataset.read(buff, H5::PredType::IEEE_F64BE, memspace, dataspace);
            H5Tconvert(H5T_IEEE_F64BE,
                    H5T_NATIVE_DOUBLE,
                    N, buff, nullptr, H5P_DEFAULT);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                for (size_t jj = 0; jj < dims_out[1]; jj++) {
                    matrix(ii, jj) = buff[ii * dims_out[1] + jj];
                }
            }

            dataset.close();
            delete[] buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException &error ) {
            cout << "Error hdf5read_real FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException &error ) {
            cout << "Error hdf5read_real DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException &error ) {
            cout << "Error hdf5read_real DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException &error ) {
            cout << "Error hdf5read_real DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5read_real(Eigen::VectorXcd& vector, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the vector
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank != 1) {
                channelflow::cferror("Vector rank should be 1.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, nullptr);

            int N = dims_out[0];

            /*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " / "
                << N << endl;
            //*/

            if ((unsigned) dims_out[0] != vector.rows())
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(vector, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset vector size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into vector
            auto* buff = new double[N];
            dataset.read(buff, H5::PredType::IEEE_F64BE, memspace, dataspace);
            H5Tconvert(H5T_IEEE_F64BE,
                    H5T_NATIVE_DOUBLE,
                    N, buff, nullptr, H5P_DEFAULT);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                vector(ii) = buff[ii];
            }

            dataset.close();
            delete[] buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException &error ) {
            cout << "Error hdf5read_real FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException &error ) {
            cout << "Error hdf5read_real DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException &error ) {
            cout << "Error hdf5read_real DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException &error ) {
            cout << "Error hdf5read_real DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5read_complex(Eigen::MatrixXcd& matrix, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            typedef struct complex_type{
                double r;
                double i;
            } complex_type;

            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the matrix
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank != 2) {
                channelflow::cferror("Matrix rank should be 2.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, nullptr);

            int N = dims_out[0] * dims_out[1];

            /*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " x "
                << (unsigned long) (dims_out[1]) << " / "
                << N << endl;
            //*/
            if ((unsigned) dims_out[0] != matrix.rows()
                    || (unsigned) dims_out[1] != matrix.cols())
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(matrix, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset matrix size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into matrix
            H5::CompType complex_compound( sizeof(complex_type) );
            complex_compound.insertMember("r", HOFFSET(complex_type, r), H5::PredType::NATIVE_DOUBLE);
            complex_compound.insertMember("i", HOFFSET(complex_type, i), H5::PredType::NATIVE_DOUBLE);

            auto* buff = new complex_type[N];
            dataset.read(buff, complex_compound, memspace, dataspace);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                for (size_t jj = 0; jj < dims_out[1]; jj++) {
                    matrix(ii, jj) = buff[ii * dims_out[1] + jj].r + 1i * buff[ii * dims_out[1] + jj].i;
                }
            }

            dataset.close();
            delete[] buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException &error ) {
            cout << "Error hdf5read_complex FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException &error ) {
            cout << "Error hdf5read_complex DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException &error ) {
            cout << "Error hdf5read_complex DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException &error ) {
            cout << "Error hdf5read_complex DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5read_complex(Eigen::VectorXcd& vector, H5::H5File& h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            typedef struct complex_type{
                double r;
                double i;
            } complex_type;

            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Open the dataset in h5file corresponding to the vector
            H5::DataSet dataset = h5file.openDataSet("mat");

            // Get the dataspace of this dataset
            H5::DataSpace dataspace = dataset.getSpace();

            // Get the dimension given by the dataspace, and display them
            int rank = dataspace.getSimpleExtentNdims();
            if (rank != 1) {
                channelflow::cferror("Vector rank should be 1.");
            }
            hsize_t dims_out[rank];
            dataspace.getSimpleExtentDims(dims_out, nullptr);

            int N = dims_out[0];

            /*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " x "
                << (unsigned long) (dims_out[1]) << " / "
                << N << endl;
            //*/

            if ((unsigned) dims_out[0] != vector.rows())
            {
                channelflow::cferror("Expected dimensions given differ from array dimension \
                        in hdf5read(vector, ...)");
            }

            // Define the memory dataspace
            // corresponding to the dataset vector size
            H5::DataSpace memspace(rank, dims_out);

            // Read dataspace into vector
            H5::CompType complex_compound( sizeof(complex_type) );
            complex_compound.insertMember("r", HOFFSET(complex_type, r), H5::PredType::NATIVE_DOUBLE);
            complex_compound.insertMember("i", HOFFSET(complex_type, i), H5::PredType::NATIVE_DOUBLE);

            auto* buff = new complex_type[N];
            dataset.read(buff, complex_compound, memspace, dataspace);

            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                vector(ii) = buff[ii].r + 1i * buff[ii].i;
            }

            dataset.close();
            delete[] buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException &error ) {
            cout << "Error hdf5read_complex FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException &error ) {
            cout << "Error hdf5read_complex DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException &error ) {
            cout << "Error hdf5read_complex DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException &error ) {
            cout << "Error hdf5read_complex DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5write_real(Eigen::MatrixXcd& matrix, H5::H5File* h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Define memory space
            int rank = 2; // rank = 2 for vector
            hsize_t dims_out[rank];
            dims_out[0] = matrix.rows();
            dims_out[1] = matrix.cols();
            H5::DataSpace dataspace(rank, dims_out);

            int N = dims_out[0] * dims_out[1];

            /*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " x "
                << (unsigned long) (dims_out[1]) << " / "
                << N << endl;
            //*/

            // Define the memory dataspace
            // corresponding to the dataset vector size
            H5::DataSpace memspace(rank, dims_out);

            // create the dataset
            H5::DataSet* dataset;
            dataset = new H5::DataSet(h5file->createDataSet("mat", H5T_NATIVE_DOUBLE, dataspace));

            // write data to dataset
            auto* buff = new double[N];
            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                for (size_t jj = 0; jj < dims_out[1]; jj++) {
                    buff[ii * dims_out[1] + jj] = matrix(ii, jj).real();
                }
            }
            dataset->write(buff, H5T_NATIVE_DOUBLE);

            // delete resources
            delete dataset;
            delete[] buff;
        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException &error ) {
            cout << "Error hdf5read_complex FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException &error ) {
            cout << "Error hdf5read_complex DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException &error ) {
            cout << "Error hdf5read_complex DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException &error ) {
            cout << "Error hdf5read_complex DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5write_real(Eigen::VectorXcd& vector, H5::H5File* h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Define memory space
            int rank = 1; // rank = 1 for vector
            hsize_t dims_out[rank];
            dims_out[0] = vector.size();
            H5::DataSpace dataspace(rank, dims_out);

            int N = dims_out[0];

            /*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " : "
                << vector.size() << endl;
            //*/

            // Define the memory dataspace
            // corresponding to the dataset vector size
            H5::DataSpace memspace(rank, dims_out);

            // create the dataset
            H5::DataSet* dataset;
            dataset = new H5::DataSet(h5file->createDataSet("mat", H5T_NATIVE_DOUBLE, dataspace));

            // write data to dataset
            auto* buff = new double[N];
            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                buff[ii] = vector(ii).real();
            }
            dataset->write(buff, H5T_NATIVE_DOUBLE);

            // delete resources
            delete dataset;
            delete[] buff;

        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException &error ) {
            cout << "Error hdf5read_complex FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException &error ) {
            cout << "Error hdf5read_complex DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException &error ) {
            cout << "Error hdf5read_complex DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException &error ) {
            cout << "Error hdf5read_complex DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5write_complex(Eigen::MatrixXcd& matrix, H5::H5File* h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            typedef struct complex_type{
                double r;
                double i;
            } complex_type;

            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Define memory space
            int rank = 2; // rank = 2 for vector
            hsize_t dims_out[rank];
            dims_out[0] = matrix.rows();
            dims_out[1] = matrix.cols();
            H5::DataSpace dataspace(rank, dims_out);

            int N = dims_out[0] * dims_out[1];

            /*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " x "
                << (unsigned long) (dims_out[1]) << " / "
                << N << endl;
            //*/

            // Define the memory dataspace
            // corresponding to the dataset vector size
            H5::DataSpace memspace(rank, dims_out);

            // Define Complex Type
            H5::CompType complex_compound( sizeof(complex_type) );
            complex_compound.insertMember("r", HOFFSET(complex_type, r), H5::PredType::NATIVE_DOUBLE);
            complex_compound.insertMember("i", HOFFSET(complex_type, i), H5::PredType::NATIVE_DOUBLE);

            // create the dataset
            H5::DataSet* dataset;
            dataset = new H5::DataSet(h5file->createDataSet("mat", complex_compound, dataspace));

            // write data to dataset
            auto* buff = new complex_type[N];
            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                for (size_t jj = 0; jj < dims_out[1]; jj++) {
                    buff[ii * dims_out[1] + jj].r = matrix(ii, jj).real();
                    buff[ii * dims_out[1] + jj].i = matrix(ii, jj).imag();
                    //buff[ii, jj].r = matrix(ii, jj).real();
                    //buff[ii, jj].i = matrix(ii, jj).imag();
                }
            }
            dataset->write(buff, complex_compound);

            // delete resources
            delete dataset;
            delete[] buff;

        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException &error ) {
            cout << "Error hdf5read_complex FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException &error ) {
            cout << "Error hdf5read_complex DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException &error ) {
            cout << "Error hdf5read_complex DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException &error ) {
            cout << "Error hdf5read_complex DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

    #ifdef HAVE_HDF5
    int hdf5write_complex(Eigen::VectorXcd& vector, H5::H5File* h5file)
    {
        // try block to detect raised exceptions.
        try
        {
            typedef struct complex_type{
                double r;
                double i;
            } complex_type;

            // Turn-off the printing of exception so they are catched later.
            H5::Exception::dontPrint();

            // Define memory space
            int rank = 1; // rank = 1 for vector
            hsize_t dims_out[rank];
            dims_out[0] = vector.size();
            H5::DataSpace dataspace(rank, dims_out);

            int N = dims_out[0];

            /*
            cout << rank << " / Dimensions : "
                << (unsigned long) (dims_out[0]) << " : "
                << vector.size() << endl;
            //*/

            // Define the memory dataspace
            // corresponding to the dataset vector size
            H5::DataSpace memspace(rank, dims_out);

            // Define Complex Type
            H5::CompType complex_compound( sizeof(complex_type) );
            complex_compound.insertMember("r", HOFFSET(complex_type, r), H5::PredType::NATIVE_DOUBLE);
            complex_compound.insertMember("i", HOFFSET(complex_type, i), H5::PredType::NATIVE_DOUBLE);

            // create the dataset
            H5::DataSet* dataset;
            dataset = new H5::DataSet(h5file->createDataSet("mat", complex_compound, dataspace));

            // write data to dataset
            auto* buff = new complex_type[N];
            for (size_t ii = 0; ii < dims_out[0]; ii++) {
                buff[ii].r = vector(ii).real();
                buff[ii].i = vector(ii).imag();
            }
            dataset->write(buff, complex_compound);

            // delete resources
            delete dataset;
            delete[] buff;

        } // end of try block

        // catch failure caused by H5::H5File operations
        catch( H5::FileIException &error ) {
            cout << "Error hdf5read_complex FileIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSet operations
        catch( H5::DataSetIException &error ) {
            cout << "Error hdf5read_complex DataSetIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataSpace operations
        catch( H5::DataSpaceIException &error ) {
            cout << "Error hdf5read_complex DataSpaceIEception" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // catch failure caused by H5::DataType operations
        catch( H5::DataTypeIException &error ) {
            cout << "Error hdf5read_complex DataTypeIException" << endl;
            //error.printError();
            cout << error.getDetailMsg() << endl;
            cout << error.getFuncName() << endl;
            return -1;
        }
        // successfully executed
        return 0;
    }
    #endif // HAVE_HDF5

} // namespace channelfow
