//
// controller.h
//
// Created by:
//
// Geoffroy Claisse
// University of Southampton, 08/2018
//
// Peter Heins on 27/02/2012.
// Copyright 2012 University of Sheffield. All rights reserved.
//
#ifndef CHANNELFLOW_CONTROLLER_H
#define CHANNELFLOW_CONTROLLER_H

#include <iostream>
#include <Eigen/Dense>
#include "channelflow/flowfield.h"
//#include "channelflow/utilfuncs.h"

// asus
extern "C" {
#include <clapack.h>
#include <cblas.h>
}
// kepler
/*
extern "C" {
#include <atlas/clapack.h>
#include <atlas/cblas.h>
}
*/

namespace channelflow {

typedef double Real;

//############################################################
// Created by Geoffroy Claisse
// University of Southampton, 08/2018
//############################################################

// NotImplemented exception handling class
class NotImplementedException : public std::logic_error {
    public:
        NotImplementedException() : logic_error("Function not implemented yet") {};
};

// Reads matrices in hdf5 format
int MatIn_hdf5(
        double** matrix,
        size_t N1,
        size_t N2,
        const std::string& filename);

// Reads real (and real only) matrices/vectors in hdf5 format
int MatIn_hdf5_real(
        Eigen::MatrixXcd& matrix,
        const std::string& filename);
int MatIn_hdf5_real(
        Eigen::VectorXcd& vector,
        const std::string& filename);

// Reads complex (and complex only) matrices/vectors in hdf5 format
int MatIn_hdf5_complex(
        Eigen::MatrixXcd& matrix,
        const std::string& filename);
int MatIn_hdf5_complex(
        Eigen::VectorXcd& vector,
        const std::string& filename);

//############################################################
// Created by Peter Heins on 27/02/2012.
// Copyright 2012 University of Sheffield. All rights reserved.
//############################################################

// General functions
double** MatIn(const char* FileName);           // Reads matrices in binary format
double** MatIn_asc(const char* FileName);       // Reads matrices in ascii format

int NumRow(const char* FileName);               // Outputs no. of rows of matrix
int NumCol(const char* FileName);               // Outputs no. of columns of matrix
double* MatVec(
        double** Mat,
        const double Vec[],
        int Matrow,
        int Matcol);         // Performs matrix-vector multiplication
void gauss(double** A, double* B, int N);                                   // Performs Gauss-Seidel iteration
double** eyeF(int numCstates);                                              // Outputs identity matrix

//############################################################
// CONTROLLER MOTHER CLASS
// Created by Geoffroy Claisse
// University of Southampton, 08/2018
//############################################################

class Controller {
    public:
        virtual double** CStateInfo();      // 2D Array for no. of controller states for controlled wavenumber pairs
        virtual double*** ConStates();      // 3D Array containing controller states for controlled wavenumber pairs
        virtual int kx_c(int mx_c);

        // EMPTY // Advances discrete-time controller forward in time with fixed time-step dT
        virtual void advance_Con(
                FlowField& u,
                FlowField& q,
                FlowField& BC,
                double*** CStateMat,
                Real dT,
                Real t,
                double*** IO);

        // EMPTY // Advances continuous-time controller forward in time with variable time-step dT
        virtual void advance_Con_CON(
                FlowField& u,
                FlowField& q,
                FlowField& BC,
                double*** CStateMat,
                Real dT,
                Real t,
                double*** IO);
};

//############################################################
// CONTROLLER_HEINS CLASS inherited from Controller class
// Created by Peter Heins on 27/02/2012.
// Copyright 2012 University of Sheffield. All rights reserved.
//############################################################

class Controller_Heins : public Controller {
    public:
        // Creates controller object
        Controller_Heins();
        Controller_Heins(int Ny, int minKx, int maxKx, int minKz, int maxKz, int uvw, const char* StateInfoFile, int NumConInputs, double tau, bool Spectral_states);

        double** CStateInfo() override;      // 2D Array for no. of controller states for controlled wavenumber pairs
        double*** ConStates() override;      // 3D Array containing controller states for controlled wavenumber pairs
        int kx_c(int mx_c) override;

        // Advances discrete-time controller forward in time with fixed time-step dT
        void advance_Con(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO) override;

        // Advances continuous-time controller forward in time with variable time-step dT
        void advance_Con_CON(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO) override;

    protected:
        int Ny_;       // No. of simulation wall-normal gridpoints
        int minKx_;    // Minimum kx to control
        int maxKx_;    // Maximum kx to control
        int minKz_;    // Minimum kz to control
        int maxKz_;    // Maximum kz to control
        int uvw_;      // Actuation via u, v or w
        const char* StateInfoFile_;     // Filename for file containing information of no. of controller states for each controlled wavenumber pair
        int NumConInputs_;              // No. of inputs INTO the controller, i.e. flow measurements
        double tau_;                    // Actuator time-constant
        bool Spectral_states_;          // States physical or Chebyshev spectral state
};

//############################################################
// CONTROLLER K CLASS inherited from Controller class
// Controller with constant K gain
// Created by Geoffroy Claisse
// University of Southampton, 08/2018
//
//############################################################
//
// Note:
// These functions are thought to be used with the OSSE project.
// This OSSE project considers a Fourier-Chebyshev discretisation,
// as in Channelflow.
// However, the z-direction Fourier transform in Channelfow
// is a real-transform, while in OSSE it is a common one.
// This implies Channelflow having only positive z-modes,
// while OSSE considers positive and negative z-modes.
// It needs to be considered when dealing with vectors or
// matrices imported from the OSSE project.
// Particularly,for the flowfield u.
// Furthermore, the actuation in the OSSE is limited to
// the positive z-modes, so it reduces headache of size-conversion
// when dealing with the forcing q.
// Moreover, even when dealing with normal fft, the span of frequencies
// between OSSE and Channelflow is different, for example with N = 6:
// freq(OSSE) = [0, +1, +2, -3, -2, -1]
// freq(CHFL) = [0, +1, +2, +3, -2, -1]
// For that reason (and other), only use an impair number for OSSE and ROSSE model.
// It is not possible to implement RFFT within the OSSE model (cf my thesis).
// However, the ROSSE model is an other way to use this symmetry.
// With the ROSSE, we can remove all the negative spanwise (kz) modes
// without any loss of generality.
//
// in OSSE, REDUCE THE ACTUATION TO:
// for v, streamwise modes : all modes
// for v, spanwise modes : all positive modes (kz >= 0)
// for eta, streamwise : 0, DO NOT CONTROL
// for eta, spanwse : 0, DO NOT CONTROL
// for uw : 0, DO NOT CONTROL
// Control on eta / (u, w) was not validated and not considered in this study.
// It should require a small update, as Heins think its actuation should work
// with u and w, but I did not consider that at all.
//
// in ROSSE:
// negative spanwise modes (i.e. kz < 0) are already removed.
//
// NAME NOMENCLATURE:
//
// FlowField:
//  FlowField
//  3D array complex [u, v, w] for kz > 0 only
//
// ff_eigenVector :
//  if  mirror :
//      Eigen::VectorXcd&
//      complex [u, v, w] for all kz
//      size _M_C_osse
//  if !mirror :
//      Eigen::VectorXcd&
//      complex [u, v, w] for kz > 0 only
//      size _M_C_in
//  note: actually the size change to _M_C_out in field is q
//
// ff_eigenVector_ROSSE :
//  Eigen::VectorXd&
//  real [u.real, v.real, w.real, u.imag, v.imag, w.imag]
//  for kz > 0 only
//  size 2 * _M_C_in (for real and imag part)
//
// state_OSSE :
//  Eigen::VectorXcd&
//  complex [v, eta, u00, w00]
//  for all kz
//  size _M_C_osse
//
// state_ROSSE :
//  Eigen::VectorXd&
//  real [v.real, eta.real, u00.real, w00.real, v.imag, eta.imag, u00.imag, w00.imag]
//  for kz > 0 only
//  size _M_C_rosse
//
// Everything is SPECTRAL
// ONLY COMPARE AND WORK WITH SPECTRAL STATE IN OSSE AND IN CHANNELFLOW
//
//############################################################

class Controller_K : public Controller {
    protected:
        // parameters to set at construction
        FlowField& _ff;
        // here we care about the number of modes as we want them to be equal
        // for the present ChannelFlow Controller_K and in the matrix K.
        int _Nd_C;
        int _Nx_C;
        int _Ny_C;
        int _Nz_C;
        int _Mx_C; // streamwise modes
        int _My_C; // wall-normal modes
        int _Mz_C; // spanwise modes = _Nz_C / 2 + 1

        // DIMENSION OF THE CONTROL MATRIX K:
        // (control q = dot(K, state x)
        //
        // 1. _M_C_in: nb of controller input state (FlowField state x)
        //      _M_C_in = Nd * Mx * Nz * Ny
        //      it does corresponds to an input vector used in the OSSE project,
        //      under the form [u, v, w],
        //      meaning that Mz = Nz with the normal fft (OSSE),
        //      and not to an input vector as channelfow,
        //      where Mz = Nz/2 + 1 with the real-fft (CHFL).
        int _M_C_in;

        // 1bis. _M_C_in_rosse: nb of controller input state (FlowField state x)
        //      in ROSSE format
        //      _M_C_in = 2 * Nd * Mx * Nz * Ny
        //      it does corresponds to an input vector used in the ROSSE project,
        //      under the form [u, v, w], with real/imag separated
        //      meaning that Mz = int((Nz+1)/2) with rfft (ROSSE),
        int _M_C_in_rosse;

        // 2. _M_C_out: number of controller output state (FlowField or vector q),
        //      correspond to the actuation at the BCs
        //      ATTENTION:
        //      IT DOES ONLY CONSIDER WALL-NORMAL VELOCITY ACTUATION
        //          ON >>ALL<< THE MODES (abs(kx), abs(kz)) < (_Mx_C, _Mz_C) (no reduction)
        //      AND
        //          kz >= 0 (as channelflow only define kz>0)
        int _M_C_out;

        // 3. _M_C_osse: dimension of the state vector used in the OSSE project
        //      under the former [v, eta] with FFT,
        //      hence with positive and negative spanwise modes
        //      _M_C_osse = (Mx * Nz + Mx * Nz - 1 + 2) * Ny
        //      - Mx * Nz for v
        //      - Mx * Nz - 1 for eta
        //      - +2 for u00 and w00
        int _M_C_osse;

        // 4. _M_C_rosse: dimension of the state vector used in the ROSSE project
        //      under the former [v, eta] using the symmetry to remove kz < 0
        //      hence with positive and negative spanwise modes
        //      _M_C_osse = 2 * (Mx * Mz + Mx * Mz - 1 + 2) * Ny
        //      - 2 * for real and imaginary parts
        //      - Mx * Mz for v
        //      - Mx * Mz - 1 for eta
        //      - +2 for u00 and w00
        int _M_C_rosse;

        // In brief :
        // _M_C_in : dimension of input flowfield [u,v,w] in channelflow (only kz > 0)
        // _M_C_osse : dimension of OSSE state vector [v, eta]
        // _M_C_rosse : dimension of ROSSE state vector [v, eta] (only kz > 0)
        // _M_C_out : dimension of output flowfield [q_v] in channelflow (only kz > 0)

        // folder containing K and Cs
        std::string _matrix_filename;
        // file containing the baseflow targeted by the controller
        // used to removed from the FlowField u of channelflow
        // before applying control of the perturbation only
        std::string _target_baseflow_filename;
        double _tau;
        Eigen::MatrixXcd K;
        //Eigen::MatrixXcd Cs;
        FlowField _target_baseflow_flowfield;

    public:
        FlowField forcing;

        Controller_K();
        Controller_K(
                FlowField& ff,
                int Mx_C,
                int My_C,
                int Nz_C,
                std::string matrix_filename,
                std::string target_baseflow_filename,
                double tau);

        // Advances discrete-time controller forward in time with fixed time-step dT
        void advance_Con(
                FlowField& u,
                FlowField& p,
                FlowField& BC,
                double*** CStateMat,
                Real dT,
                Real t,
                double*** IO) override;

        // Advances continuous-time controller forward in time with variable time-step dT
        void advance_Con_CON(
                FlowField& u,
                FlowField& p,
                FlowField& BC,
                double*** CStateMat,
                Real dT,
                Real t,
                double*** IO) override;

        // Advances continuous-time controller forward in time with variable time-step dT
        //void advance_Con_CN(FlowField& u, FlowField& q, FlowField& BC, double*** CStateMat, Real dT, Real t, double*** IO);

        // Note: EigenVector is not a linear algebra eigenvectors but for Eigen::Vector
        // if mirror = true, ff_eigenVector = ff_eigenVector_OSSE = [u, v, w] in vector form with all kz modes.
        virtual void FlowField_to_ff_eigenVector(
                FlowField& ff,
                Eigen::VectorXcd& ff_eigenVector, bool mirror);
        virtual void ff_eigenVector_to_FlowField(
                Eigen::VectorXcd& ff_eigenVector,
                FlowField& ff, bool mirror);

        // Note: EigenVector is not a linear algebra eigenvectors but for Eigen::Vector
        void state_OSSE_to_state_ROSSE(
                Eigen::VectorXcd& state_OSSE,
                Eigen::VectorXd& state_ROSSE);
        void state_ROSSE_to_state_OSSE(
                Eigen::VectorXd& state_ROSSE,
                Eigen::VectorXcd& state_OSSE);

        static void substract_ff(
                FlowField& u,
                FlowField& ff_to_substract);
        static void substract_ff2(
                FlowField& u,
                FlowField& ff_to_substract);
        static void add_ff(
                FlowField& u,
                FlowField& ff_to_add);

        static void K_changegrid(FlowField& u1, FlowField& u0);

        static std::vector<int> numpy_fftfreq(int n, double d);

        // DEPRECATED:
        //void upsize_xz(FlowField& u_in, FlowField& u_out);
        //void downsize_xz(FlowField& u_in, FlowField& u_out);
        //void downsize_xyz(FlowField& u_in, FlowField& u_out);
        // only use changegrid for Ny dimension change
        //void changegrid(FlowField& u_in, FlowField& u_out, bool padded=true);
};

class Controller_OSSE : public Controller_K {
    public:
        Controller_OSSE();
        Controller_OSSE(
                FlowField& ff,
                int Mx_C,
                int My_C,
                int Nz_C,
                std::string matrix_filename,
                std::string target_baseflow_filename,
                double tau);

        // Advances continuous-time controller forward in time with variable time-step dT
        void advance_Con_CON(
                FlowField& u,
                FlowField& p,
                FlowField& BC,
                double*** CStateMat,
                Real dT,
                Real t,
                double*** IO) override ;

        void test();
        void test0();
        void test1();
        void test2();
        void test3();
        void test4();
        void test5();
        void test6();
        void test7(FlowField& BC);
        void test7bis(FlowField& BC);
        void test8(FlowField& BC);
};

class Controller_ROSSE : public Controller_K {
    protected:

    public:
        Controller_ROSSE();
        Controller_ROSSE(
                FlowField& ff,
                int Mx_C,
                int My_C,
                int Nz_C,
                std::string matrix_filename,
                std::string target_baseflow_filename,
                double tau);

        // Advances continuous-time controller forward in time with variable time-step dT
        void advance_Con_CON(
                FlowField& u,
                FlowField& p,
                FlowField& BC,
                double*** CStateMat,
                Real dT,
                Real t,
                double*** IO) override;

        // This method is to use only with Controller_ROSSE, but it is placed here
        // here it is not dependent of private variable of Controller_ROSSE object
        static void FlowField_to_ff_eigenVector_ROSSE(
                FlowField& ff,
                Eigen::VectorXd& ff_eigenVector);
        static void ff_eigenVector_ROSSE_to_FlowField(
                Eigen::VectorXd& ff_eigenVector,
                FlowField& ff);

        void test0();
};

class Controller_ROSSE_via_OSSE : public Controller_K {
    protected:
        std::string _CT_filename;
        std::string _CTinv_filename;
        Eigen::MatrixXcd _CT;
        Eigen::MatrixXcd _CTinv;

    public:
        Controller_ROSSE_via_OSSE();
        Controller_ROSSE_via_OSSE(
                FlowField& ff,
                int Mx_C,
                int My_C,
                int Nz_C,
                std::string matrix_filename,
                std::string target_baseflow_filename,
                std::string CT_filename,
                std::string CTinv_filename,
                double tau);

        // Advances continuous-time controller forward in time with variable time-step dT
        void advance_Con_CON(
                FlowField& u,
                FlowField& p,
                FlowField& BC,
                double*** CStateMat,
                Real dT,
                Real t,
                double*** IO) override;

        // Note: EigenVector is not a linear algebra eigenvectors but for Eigen::Vector
        void FlowField_to_state_ROSSE_via_OSSE(
                FlowField& ff,
                Eigen::VectorXd& state_ROSSE);
        void state_ROSSE_to_FlowField_via_OSSE(
                Eigen::VectorXd& state_ROSSE,
                FlowField& ff);

        void test0();
        void test1();
};



}


#endif
