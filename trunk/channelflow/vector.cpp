/* vector.cpp: simple vector class for use with BandedTridiag
 *
 * channelflow-1.3, www.channelflow.org
 *
 * Copyright (C) 2001-2009  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu
 * Center for Nonlinear Sciences, School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */

#include <fstream>
#include <iomanip>
#include <fftw3.h>

#include "channelflow/vector.h"

#define FFTW_MALLOC 1

using namespace std;

namespace channelflow {

Vector::Vector(int N) :
#ifdef FFTW_MALLOC
  data_((Real*) fftw_malloc(N*sizeof(Real))),
#else
  data_(new Real[N]),
#endif
  N_(N)
{
  //cerr << "Vector ctor " << long(data_) << endl;
  assert(data_ != 0);
  Real* dptr = data_;
  for (int i=0; i<N_; ++i)
    *dptr++ = 0.0;
}

Vector::Vector(const Vector& a) :
#ifdef FFTW_MALLOC
  data_((Real*) fftw_malloc(a.N_*sizeof(Real))),
#else
  data_(new Real[a.N_]),
#endif
  N_(a.N_)
{
  //cerr << "Vector ctor " << long(data_) << endl;
  assert(data_ != 0);

  Real* dptr = data_;
  Real* aptr = a.data_;
  for (int i=0; i<N_; ++i)
    *dptr++ = *aptr++;
}


Vector::Vector(const string& filebase) :
  data_(0),
  N_(0)
{
  ifstream is;
  string filename = ifstreamOpen(is, filebase, ".asc");
  if (!is) {
    cerr << "Vector::Vector(filebase) : can't open file "
	 << filebase << " or " << (filebase+".asc") << endl;
    exit(1);
  }

  // Read in header. Form is "%5 4" for a 5x4 matrix
  char c;
  int M,N;
  is >> c;
  if (c != '%') {
    string message("Vector(filebase): bad header in file ");
    message += filename;
    cferror(message);
  }
  is >> M >> N;

  assert(M == 1 || N == 1);
  N_ = (M > N) ? M : N;

#ifdef FFTW_MALLOC
  data_ = (Real*) fftw_malloc(N_*sizeof(Real));
#else
  data_ = new Real[N_];
#endif

  //char BUFFER[256];
  //is.getline(BUFFER, 256); // Finsh off the first line
  //is.getline(BUFFER, 256); // Get the comment string.

  Real* dptr = data_;
  Real* end = data_ + N_;
  while (dptr < end)
    is >> *dptr++;
  is.close();
}

void Vector::resize(int newN) {
  if (newN != N_) {
#ifdef FFTW_MALLOC
    Real* newdata = (Real*) fftw_malloc(newN*sizeof(Real));
#else
    Real* newdata = new Real[newN];
#endif
    //cerr << "Vector resize " << long(data_) << " -> " << long(newdata) << endl;
    assert(newdata != 0);
    int shortN = lesser(N_, newN);
    for (int i=0; i<shortN; ++i)
      newdata[i] = data_[i];
    for (int i=shortN; i<newN; ++i)
      newdata[i] = 0.0;
#ifdef FFTW_MALLOC
    fftw_free(data_);
#else
    delete[] data_;
#endif
    data_ = newdata;
    N_ = newN;
  }
}


void Vector::randomize() {
  for (int n=0; n<N_; ++n)
    data_[n] = Re(randomComplex()); // normal distribution
}

void Vector::setToZero() {
  for (int i=0; i<N_; ++i)
    data_[i] = 0.0;
}


Vector::~Vector() {
  //cerr << "Vector dtor " << long(data_) << flush;
#ifdef FFTW_MALLOC
    fftw_free(data_);
#else
    delete[] data_;
#endif
  //cerr << "done" << endl;
  data_=0;
}

/*********
Vector& Vector::operator-() {
  for (int i=0; i<N_; ++i)
    data_[i] = -data_[i];
  return *this;
}
**********/
Vector& Vector::operator*=(Real c) {
  for (int i=0; i<N_; ++i)
    data_[i] *= c;
  return *this;
}

Vector& Vector::operator/=(Real c) {
  Real cinv = 1.0/c;
  for (int i=0; i<N_; ++i)
    data_[i] *= cinv;
  return *this;
}

Vector& Vector::operator+=(Real c) {
  for (int i=0; i<N_; ++i)
    data_[i] += c;
  return *this;
}

Vector& Vector::operator-=(Real c) {
  for (int i=0; i<N_; ++i)
    data_[i] -= c;
  return *this;
}

Vector& Vector::operator+=(const Vector& a) {
  assert(a.N_ == N_);
  for (int i=0; i<N_; ++i)
    data_[i] += a.data_[i];
  return *this;
}

Vector& Vector::operator-=(const Vector& a) {
  assert(a.N_ == N_);
  for (int i=0; i<N_; ++i)
    data_[i] -= a.data_[i];
  return *this;
}

Vector& Vector::dottimes(const Vector& a) {
  assert(a.N_ == N_);
  for (int i=0; i<N_; ++i)
    data_[i] *= a.data_[i];
  return *this;
}

Vector& Vector::dotdivide(const Vector& a) {
  assert(a.N_ == N_);
  for (int i=0; i<N_; ++i)
    data_[i] /= a.data_[i];
  return *this;
}

Vector& Vector::abs() {
  for (int i=0; i<N_; ++i)
    data_[i] = fabs(data_[i]);
  return *this;
}

Vector Vector::subvector(int offset, int N) const {
  Vector subvec(N);
  assert(N+offset <= N_);
  for (int i=0; i<N; ++i)
    subvec[i] = data_[i+offset];
  return subvec;
}

Vector Vector::modularSubvector(int offset, int N) const {
  Vector subvec(N);
  for (int i=0; i<N; ++i)
    subvec[i] = data_[(i+offset) % N_];
  return subvec;
}

Vector& Vector::operator=(const Vector& a) {
  if (data_ != a.data_) {
    if (N_ != a.N_) {
      //cout << "delete in operator= on " << long(data_) << endl;
#ifdef FFTW_MALLOC
      fftw_free(data_);
      data_ = (Real*) fftw_malloc(a.N_*sizeof(Real));
#else
      delete[] data_;
      data_ = new Real[a.N_];
#endif
      N_ = a.N_;
      assert(data_ != 0);
    }
    Real* dptr = data_;
    Real* aptr = a.data_;
    for (int i=0; i<N_; ++i)
      *dptr++ = *aptr++;
  }
  return *this;
}

void Vector::save(const string& filebase) const {
  string filename(filebase);
  filename += string(".asc");
  ofstream os(filename.c_str());

  os << scientific << setprecision(REAL_DIGITS);
  os << "% " << N_ << " 1\n";
  for (int i=0; i<N_; ++i)
    os << setw(REAL_IOWIDTH) << data_[i] << '\n';
  os.close();
}

void swap(Vector& f, Vector& g) {
  int itmp = f.N_;
  f.N_ = g.N_;
  g.N_ = itmp;

  Real* tmp = f.data_;
  f.data_ = g.data_;
  g.data_ = tmp;
}

void assign(Vector& u, int uistart, int uistride, int uiend,
	    Vector& v, int vistart, int vistride, int viend) {
  assert(((uistart - uiend)/uistride == (vistart - viend)/vistride));
  int ui, vi;
  for (ui=uistart, vi=vistart; ui<uiend; ui+=uistride, vi+=vistride)
      u[ui] = v[vi];
}

Vector operator*(Real c, const Vector& v) {
  Vector u(v.length());
  int N=u.length();
  for (int i=0; i<N; ++i)
    u[i] = c*v[i];
  return u;
}

Vector operator+(const Vector& u, const Vector& v) {
  Vector w(v.length());
  int N=u.length();
  assert(v.length() == N);
  for (int i=0; i<N; ++i)
    w[i] = u[i] + v[i];
  return w;
}

Vector operator-(const Vector& u, const Vector& v) {
  Vector w(v.length());
  int N=u.length();
  assert(v.length() == N);
  for (int i=0; i<N; ++i)
    w[i] = u[i] - v[i];
  return w;
}

Vector dottimes(const Vector& u, const Vector& v) {
  Vector w(v.length());
  int N=u.length();
  assert(v.length() == N);
  for (int i=0; i<N; ++i)
    w[i] = u[i] * v[i];
  return w;
}

Vector dotdivide(const Vector& u, const Vector& v) {
  Vector w(v.length());
  int N=u.length();
  assert(v.length() == N);
  for (int i=0; i<N; ++i)
    w[i] = u[i]/v[i];
  return w;
}

Real operator*(const Vector& u, const Vector& v) {
  Real sum = 0.0;
  int N=u.length();
  assert(v.length() == N);
  for (int i=0; i<N; ++i)
    sum += u[i]*v[i];
  return sum;
}

bool operator==(const Vector& u, const Vector& v) {
  int N=u.length();
  if (v.length() != N)
    return false;
  for (int i=0; i<N; ++i)
    if (u[i] != v[i])
      return false;

  return true;
}

Vector vabs(const Vector& v) {
  Vector rtn(v);
  rtn.abs();
  return rtn;
}

Real L1Norm(const Vector& v) {
  Real sum = 0.0;
  int N=v.length();
  for (int i=0; i<N; ++i)
    sum += fabs(v[i]);
  return sum;
}

Real L2Norm(const Vector& v) {
  Real sum = 0.0;
  int N=v.length();
  for (int i=0; i<N; ++i)
    sum += square(v[i]);
  return sqrt(sum);
}

Real L2Norm2(const Vector& v) {
  Real sum = 0.0;
  int N=v.length();
  for (int i=0; i<N; ++i)
    sum += square(v[i]);
  return sum;
}

Real LinfNorm(const Vector& v) {
  Real max = 0.0;
  int N=v.length();
  for (int i=0; i<N; ++i)
    max = Greater(fabs(v[i]), max);
  return max;
}

Real L1Dist(const Vector& u, const Vector& v) {
  assert(u.length() == v.length());
  Real sum = 0.0;
  int N=v.length();
  for (int i=0; i<N; ++i)
    sum += fabs(u[i] - v[i]);
  return sum;
}


Real L2Dist(const Vector& u, const Vector& v) {
  assert(u.length() == v.length());
  Real sum = 0.0;
  int N=v.length();
  for (int i=0; i<N; ++i)
    sum += square(v[i]-u[i]);
  return sqrt(sum);
}

Real L2Dist2(const Vector& u, const Vector& v) {
  assert(u.length() == v.length());
  Real sum = 0.0;
  int N=v.length();
  for (int i=0; i<N; ++i)
    sum += square(u[i]-v[i]);
  return sum;
}

Real LinfDist(const Vector& u, const Vector& v) {
  assert(u.length() == v.length());
  Real max = 0.0;
  int N=v.length();
  for (int i=0; i<N; ++i)
    max = Greater(fabs(v[i]-u[i]), max);
  return max;
}

Real mean(const Vector& v) {
  Real sum = 0.0;
  int N = v.length();
  for (int i=0; i<N; ++i)
    sum += v[i];
  return sum/N;
}

int maxElemIndex(const Vector& v) {
  Real max = 0.0;
  int index = 0;
  int N=v.length();
  for (int i=0; i<N; ++i)
    if (fabs(v[i]) > max) {
      max = fabs(v[i]);
      index = i;
    }
  return index;
}

ostream& operator<<(ostream& os, const Vector& a) {
  int N = a.length();
  char seperator=(N<10) ? ' ' : '\n';
  for (int i=0; i<N; ++i) {
    os << a[i];
    os << seperator;
    //os << a[i] << seperator;
  }
  return os;
}

} //namespace channelflow
