/* mathdefs.cpp: some small mathematical conveniences.
 *
 * channelflow-1.3, www.channelflow.org
 *
 * Copyright (C) 2001-2009  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu
 * Center for Nonlinear Sciences, School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */

#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <cstring>
#include <iomanip>
#include <arpa/inet.h>

#include "channelflow/mathdefs.h"

#include "config.h"

#ifndef HAVE_DRAND48
#define drand48() rand()/(double(RAND_MAX));
#endif

#ifdef __CYGWIN__
#define drand48() rand()/(double(RAND_MAX));
#endif

using namespace std;

namespace channelflow {

string i2s(int n) {
  const int Nbuf = 32;
  char buff[Nbuf];
  sprintf(buff, "%d", n);
  return string(buff);
}

string r2s(Real r) {
  const int Nbuf = 32;
  char buff[Nbuf];
  sprintf(buff, "%g", r);
  return string(buff);
}

void cferror(const string& message) {
  cerr << message << endl;
  exit(-1);
}

void cfpause() {
  cout << "cfpause..." << flush;
  char s[10];
  cin.getline(s,10);
}

Real pythag(Real a, Real b) {
  Real absa = fabs(a);
  Real absb = fabs(b);

  if (absa > absb)
    return absa * (sqrt(1.0 + square(absb/absa)));
  else if (absb > absa)
    return absb * (sqrt(1.0 + square(absa/absb)));
  else
    return 0.0;
}

bool isPowerOfTwo(int N) {
  int onbits = 0;
  for (unsigned int i = 0; i<int(8*sizeof(int)); ++i) {
    onbits += N % 2;
    N = N >> 1;
    if (onbits > 1)
      return false;
  }
  return true;
}

// int roundReal(Real x) {
//   int ix = int(x);
//   Real dm = abs(x-(ix-1)); // m==minus:  distance x to (xcast minus 1)
//   Real dc = abs(x-ix);     // c==center: distance x to (xcast)
//   Real dp = abs(x-(ix+1)); // p==plus:   distance x to (xcast plus 1)

//   return ((dm<dp) ? ((dm<dc) ? ix-1 : ix) : ((dc<dp) ? ix : ix+1));
// }

int iround(Real x) {
  return int(x > 0.0 ? x + 0.5 : x - 0.5);
}

int intpow(int x, int n) {
  if (n < 0)
    cferror("int pow(int, int) : can't do negative exponents, use Real pow(Real,int)");
  switch(n) {
  case 0:
    return 1;
  case 1:
    return x;
  case 2:
    return x*x;
  case 3:
    return x*x*x;
  case 4: {
    int xx = x*x;
    return xx*xx;
  }
  default: {
    // a*x^n is invariant and n is decreasing in this loop
    int a = 1;
    while (n>0) {
      if (n%2 == 0) {
	x *= x; //     x -> x^2
	n /= 2; //     n -> n/2
      }         // a x^n -> a (x^2)^(n/2) = a x^n
      else {
	a *= x; //     a -> ax
	--n;    //     n -> n-1
      }         // a x^n -> (a x) x^(n-1) = a x^n
    }
    return a;
  }
  }
}

/*****************************
Real fmod(Real x, Real modulus) {
  if (modulus < 0) {
    cerr << "error in fmod(x,m) : m should be greater or equal to 0." << endl;
    exit(1);
  }
  if (modulus == 0)
    return 0.0;

  if (x<0)
    x += modulus*(int(fabs(x)/modulus)+1);
  if (x>0)
    x -= modulus*int(x/modulus);

  // Just in case previous algorithm slips up.
  while (x<0)
    x += modulus;
  while (x>=modulus)
    x -= modulus;
  return x;
}
************************************/

string ifstreamOpen(ifstream& is, const string& filebase, const string& ext,
		    ios_base::openmode mode) {

  mode = mode | ios::in;

  // Try to open <filebase>
  string filename = filebase;

  is.open(filename.c_str(), mode);

  if (is)
    return filename;

  is.close();
  is.clear();
  filename += ext;
  is.open(filename.c_str(), mode);

  if (!is)
    is.close();

  return filename;
}

void save(Complex c, const string& filebase) {
  string filename = appendSuffix(filebase, ".asc");
  ofstream os(filename.c_str());
  if (!os.good())
    cferror("save(Complex, filebase) :  can't open file " + filename);
  os << setprecision(17);
  os << Re(c) << ' ' << Im(c) << '\n';	// format can be read by matlab.
  os.close();
}

void load(Complex& c, const string& filebase) {
  string filename = appendSuffix(filebase, ".asc");
  ifstream is(filename.c_str());
  if (!is.good())
    cferror("load(Complex, filebase) :  can't open file " + filename);
  Real a, b;
  is >> a >> b;
  c = Complex(a,b);
}

void save(Real c, const string& filebase) {
  string filename = appendSuffix(filebase, ".asc");
  ofstream os(filename.c_str());
  if (!os.good())
    cferror("save(Real, filebase) :  can't open file " + filename);
  os << setprecision(17);
  os << c << '\n';	// format can be read by matlab.
  os.close();
}

void load(Real& c, const string& filebase) {
  string filename = appendSuffix(filebase, ".asc");
  ifstream is(filename.c_str());
  if (!is.good())
    cferror("load(Real, filebase) :  can't open file " + filename);
  Real r;
  is >> r;
  c = r;
}

// ====================================================================
// Endian-independent binary IO code (i.e. this code is indpt of endianness)
void write (ostream& os, bool b) {
  char c = (b) ? '1' : '0';
  os.write(&c, 1);  // sizeof(char) == 1
}

void read(istream& is, bool& b) {
  char c;
  is.read(&c, 1);
  b = (c=='0') ? false : true;
}

void write(ostream& os, fieldstate s) {
  char c = (s == Spectral) ? 'S' : 'P';
  os.write(&c, 1);
}

void read(istream& is, fieldstate& s) {
  char c;
  is.read(&c, 1);
  s = (c=='S') ? Spectral : Physical;
}

void write(ostream& os, Complex z) {
  Real x = Re(z);
  write(os, x);
  x = Im(z);
  write(os, x);
}

void read(istream& is, Complex& z) {
  Real a, b;
  read(is, a);
  read(is, b);
  z = Complex(a, b);
}

// ====================================================================
// Binary IO: native saved to big-endian format on disk.
//
// Cross-endian IO assumes 32-bit int.
void write(ostream& os, int n) {

  if (sizeof(int) != 4) {
    cerr << "write(ostream& os, int n) :  channelflow binary IO assumes 32-bit int\n";
    cerr << "The int on this platform is " << 8*sizeof(int) << " bits" << endl;
    exit(1);
  }

  n = htonl(n);
  os.write((char*) &n, sizeof(int));
}

void read(istream& is, int& n) {

  if (sizeof(int) != 4) {
    cerr << "read(istream& is, int n) error: channelflow binary IO assumes 32-bit int.\n";
    cerr << "The int on this platform is " << 8*sizeof(int) << " bits" << endl;
    exit(1);
  }

  is.read((char*)(&n), sizeof(int));
  n = ntohl(n);
}

// Linux has a bswap_64 function, but the following is more portable.
void write(ostream& os, Real x) {

  if (sizeof(double) != 8) {
    cerr << "write(ostream& os, Real x) error: channelflow binary IO assumes 64-bit double-precision floating point.\n";
    cerr << "The double on this platform is " << 8*sizeof(Real) << " bits" << endl;
    exit(1);
  }

  if (sizeof(int) != 4) {
    cerr << "write(ostream& os, Real x) error: channelflow binary IO assumes 64-bit double-precision floating point and 32-bit ints.\n";
    cerr << "The int on this platform is " << 8*sizeof(int) << " bits" << endl;
    exit(1);
  }

#if __BYTE_ORDER == __LITTLE_ENDIAN
  int i[2];

  // Copy 64 bit x into two consecutive 32 bit ints
  memcpy(i, &x, sizeof(uint64_t));

  // Byteswap each 32 bit int and reverse their order.
  int tmp = i[1];
  i[1] = htonl(i[0]);
  i[0] = htonl(tmp);

  // Write out the two reversed and byteswapped 32bit ints
  os.write((char*) i, sizeof(double));
#else
  os.write((char*) &x, sizeof(double));
#endif
}

void read(istream& is, Real& x) {

  if (sizeof(double) != 8) {
    cerr << "read(istream& is, Real x) error: channelflow binary IO assumes 64-bit double-precision floating point.\n";
    cerr << "The double on this platform is " << 8*sizeof(Real) << " bits" << endl;
    exit(1);
  }

  if (sizeof(int) != 4) {
    cerr << "write(ostream& os, Real x) error: channelflow binary IO assumes 64-bit double-precision floating point and 32-bit ints.\n";
    cerr << "The int on this platform is " << 8*sizeof(int) << " bits" << endl;
    exit(1);
  }

#if __BYTE_ORDER == __LITTLE_ENDIAN
  int i[2];

  // Read in two reversed and byteswapped 32bit ints
  is.read((char*)(i), sizeof(double));

  // Byteswap each 32 bit int and reverse their order
  int tmp = i[1];
  i[1] = ntohl(i[0]);
  i[0] = ntohl(tmp);

  // Copy the two consecutive 32bit ints into the Real
  memcpy(&x, i, sizeof(double));
#else
  is.read((char*) &x, sizeof(double));
#endif
}

/**************************************************************************
// =========================================================================
// Binary IO: Read little-endian data from disk. For backwards compatibility
// with old binary IO formats.

void readLittleEndian(istream& is, int& n) {
#if __BYTE_ORDER == __BIG_ENDIAN
  uint32_t i;
  is.read((char*)(&i), sizeof(uint32_t));
  i = ntohl(i);
  memcpy(&n, &j, sizeof(uint32_t));
#else
  is.read((char*)(&n), sizeof(uint32_t));
#endif
}

void readLittleEndian(istream& is, Real& x) {
#if __BYTE_ORDER == __BIG_ENDIAN
  uint32_t i[2];

  // Read in two reversed and byteswapped 32bit ints
  is.read((char*)(i), sizeof(uint64_t));

  // Byteswap each 32 bit int and reverse their order
  uint32_t tmp = i[1];
  i[1] = ntohl(i[0]);
  i[0] = ntohl(tmp);

  // Copy the two consecutive 32bit ints into the Real
  memcpy(&x, i, sizeof(uint64_t));#else
  is.read((char*)(&x), sizeof(uint64_t));
#endif
}
**************************************************************************/


Real randomReal(Real a, Real b) {
  return a+(b-a)*drand48();
}

Complex randomComplex() {
  Real a;
  Real b;
  Real r2;
  do {
    a = randomReal(-1, 1);
    b = randomReal(-1, 1);
    r2 = a * a + b * b;
  } while (r2 >= 1.0 || r2 == 0.0);
  return sqrt(-log(r2) / r2) * Complex(a,b);
}


ostream& operator <<(ostream& os, Complex z) {
  os << '(' << Re(z) << ", " << Im(z) << ')';
  return os;
}

ostream& operator <<(ostream& os, fieldstate s) {
  os << ((s==Spectral) ? 'S' : 'P');
  return os;
}

istream& operator >>(istream& is, fieldstate& s) {
  char c = ' ';
  while (c == ' ')
    is >> c;
  switch(c) {
  case 'P':
    s = Physical;
    break;
  case 'S':
    s = Spectral;
    break;
  default:
    cerr << "read fieldstate error: unknown fieldstate " << c << endl;
    s = Spectral;
    assert(false);
  }
  return is;
}

// Return filebase.extension unless filebase already includes .extension
string appendSuffix(const string& filebase, const string& extension) {
  int Lbase = filebase.length();
  int Lext  = extension.length();
  string filename = filebase;
  if (Lbase < Lext  || filebase.substr(Lbase-Lext, Lext) != extension)
    filename += extension;
  return filename;
}

// If filename == foo.ext, return foo, else return filename
string removeSuffix(const string& filename, const string& extension) {
  int extpos = filename.find(extension);
  if (extpos == -1)
    extpos = filename.length();
  return string(filename, 0, extpos);
}

bool hasSuffix(const string& filename, const string& extension) {
  int extpos = filename.find(extension);
  if (extpos == -1)
    return false;
  else
    return true;
}

bool isReadable(const std::string& filename) {
  ifstream is(filename.c_str(), ios::in | ios::binary);
  bool rtn = (is) ? true : false;
  is.close();
  return rtn;
}


// Extract the version numbers from the PACKAGE_STRING provided by autoconf
void channelflowVersion(int& major, int& minor, int& update) {
  const char delimiters[] = ".";
  char* cp = strdup(PACKAGE_VERSION);  // Make writable copy of version str
  assert(cp != NULL);
  major  = atoi(strtok(cp, delimiters));
  minor  = atoi(strtok(NULL, delimiters));
  update = atoi(strtok(NULL, delimiters));
  free(cp);
}


} //namespace channelflow
