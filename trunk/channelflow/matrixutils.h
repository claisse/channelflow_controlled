#ifndef CHANNELFLOW_OCTUTILS_H
#define CHANNELFLOW_OCTUTILS_H

#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <vector>

#include <Eigen/Eigen>
#include <Eigen/Dense>
typedef std::complex<double> Complex;

#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/quicksort.h"
#include "channelflow/utilfuncs.h"

using Eigen::MatrixXd;
using Eigen::MatrixXcd;
using Eigen::Matrix3d;
using Eigen::EigenSolver;
using Eigen::VectorXcd;
using Eigen::VectorXd;
using Eigen::RowVectorXd;
//using Eigen::DiagonalMatrixXd;

//typedef MatrixXd  Matrix;
typedef MatrixXcd   ComplexMatrix;
typedef VectorXd    ColumnVector;
typedef VectorXcd   ComplexColumnVector;
typedef RowVectorXd RowVector;
//typedef DiagonalMatrixXd DiagMatrix;


namespace channelflow {

// ====================================================================
// All Octave-dependent code goes within this ifdef

void print(const MatrixXd& x);
void print(const ColumnVector& x);
void print(const ComplexColumnVector& x);
void save(const MatrixXd& A, const std::string& filebase);
void save(const ColumnVector& x, const  std::string& filebase);
void save(const ComplexColumnVector& x, const std::string& filebase);
void load(MatrixXd& A, const std::string& filebase);
void load(ColumnVector& x, const  std::string& filebase);
void load(ComplexColumnVector& x, const std::string& filebase);
void setToZero(MatrixXd& A);
void setToZero(ColumnVector& x);
void setToZero(ComplexColumnVector& x);

Real L2Norm(const ColumnVector& x);
Real L2Dist(const ColumnVector& x,  const ColumnVector& y);
Real L2Norm2(const ColumnVector& x);
Real L2Dist2(const ColumnVector& x,  const ColumnVector& y);
Real L2IP(const ColumnVector& u, const ColumnVector& v);

//Comment this out for Octave 3.4.0 and later
//void operator*=(ColumnVector& x, Real c);

Real L2Norm(const ComplexColumnVector& x);
Real L2Dist(const ComplexColumnVector& x,  const ComplexColumnVector& y);
Real L2Norm2(const ComplexColumnVector& x);
Real L2Dist2(const ComplexColumnVector& x,  const ComplexColumnVector& y);
void operator*=(ComplexColumnVector& x, Complex c);

enum BasisType {L2Basis, AdHocBasis};

Real V2Norm2(const FlowField& u); // L2Norm2(field2vector(u));
Real V2Norm(const FlowField& u);
Real V2IP(const FlowField& u, const FlowField& v);

Real Norm2(const FlowField& u, BasisType norm);
Real Norm(const FlowField& u, BasisType norm);
Real IP(const FlowField& u, const FlowField& v, BasisType norm);

FlowField swirling(const FlowField& u);
void swirling(const FlowField& u, FlowField& s);

void quicksort(ComplexColumnVector& z); // sort by norm, decreasing
void quicksort(ComplexColumnVector& Lambda, ComplexMatrix& V); // ditto

// The field2vector and vector2field functions assume zero divergece and no-slip BCs.
void field2vector(const FlowField& u, ColumnVector& v,
		  const std::vector<RealProfileNG>& basis);
void vector2field(const ColumnVector& v, FlowField& u,
		  const std::vector<RealProfileNG>& basis);

void field2vector(const std::vector<BasisFunc>& basis,
		  const FlowField& u, ComplexColumnVector& v);
void vector2field(const std::vector<BasisFunc>& basis,
		  const ComplexColumnVector& v, FlowField& u);

void field2vector(const FlowField& u, ColumnVector& v);

void vector2field(const ColumnVector& v, FlowField& u);

void fixdivnoslip(FlowField& u);

void leastsquares(const MatrixXd& A, ColumnVector& b, ColumnVector& x,
		  Real& residual);

//void solve(const MatrixXd& Ut, const DiagMatrix& D, const Matrix& C,
//	   const ColumnVector& b, ColumnVector& x);

// f^T(u) = time-T DNS integration of u
// f(u, T, fu, flags, timestep, fcount, CFL, os)
void f(const FlowField& u, Real T, FlowField& f_u, const DNSFlags& flags, 
       const TimeStep& dt, int& fcount, Real& CFL, std::ostream& os=std::cout);

// f^(n dt)(u) == time-(n dt) DNS integration of u
void f(const FlowField& u, int n, Real dt, FlowField& f_u, const DNSFlags& flags, 
       std::ostream& os=std::cout);

// G(u,sigma,T) = (sigma f^T(u) - u)   for orbits and
//                (sigma f^T(u) - u)/T for eqbs, tws
void G(const FlowField& u, Real T, const FieldSymmetry& sigma, FlowField& G_u,
       const DNSFlags& flags, const TimeStep& dt, bool Tnormalize, Real Unormalize,
       int& fcount, Real& CFL, std::ostream& os=std::cout);

void DG(const FlowField& u, const FlowField& du, Real T, Real dT,
	const FieldSymmetry& sigma,  const FieldSymmetry& dsigma,
	const FlowField& Gx, FlowField& DG_dx, const DNSFlags& flags,
	const TimeStep& dt, bool Tnormalize, Real Unormalize, Real eps, bool centerdiff, int& fcount,
	Real& CFL, std::ostream& os=std::cout);


// Versions of f, G, DG that handle Poincare section calculations, additionally.
void fp(const FlowField& u, Real& T, PoincareCondition* h,
	FlowField& fu, const DNSFlags& flags, const TimeStep& dt, int& fcount, Real& CFL,
	std::ostream& os=std::cout);

// sf(u,sigma,T) = sigma f^T(u)
void sfp(const FlowField& u, Real T, PoincareCondition* h,
	 const FieldSymmetry& sigma, FlowField& sfu, const DNSFlags& flags, 
	 const TimeStep& dt, int& fcount, Real& CFL, std::ostream& os=std::cout);

// Dsf(u,sigma,T,du) = sigma f^T(u+du) - sigma f^T(u)
void Dsfp(const FlowField& u, Real T, PoincareCondition* h,
	  const FieldSymmetry& sigma, const FlowField& sfu, const FlowField& du, 
	  FlowField& Dsf_du, const DNSFlags& flags, const TimeStep& dt, Real eps, 
	  bool centerdiff, int& fcount, Real& CFL, std::ostream& os=std::cout);

void Gp(const FlowField& u, Real& T, PoincareCondition* h,
	const FieldSymmetry& sigma, FlowField& Gu, const DNSFlags& flags, const TimeStep& dt,
	bool Tnormalize, Real Unormalize, int& fcount, Real& CFL, std::ostream& os=std::cout) ;

void DGp(const FlowField& u, const FlowField& du, Real& T, Real dT, 
	 PoincareCondition* h, const FieldSymmetry& sigma, const FieldSymmetry& dsigma,
	 const FlowField& Gu, FlowField& DG_dx, const DNSFlags& flags, const TimeStep& dt,
	 bool Tnormalize, Real Unormalize, Real epsDu, bool centdiff, int& fcount, Real& CFL, std::ostream& os=std::cout);







// Arnoldi iteration to estimate eigenvalues of matrix A. Usage:
// Arnoldi arnoldi(N, b);
// for (int n=0; n<N; ++n) {
//   ColumnVector q  = arnoldi.testVector();
//   ColumnVector Aq = A*q; // or however else you calculate A*q
//   arnoldi.iterate(Aq);
//   ComplexColumnVector ew = arnoldi.ew(); // current estimate of eigenvalues
// }

class Arnoldi {

public:
  Arnoldi();
  Arnoldi(const ColumnVector& b, int Niterations, Real minCondition=1e-13);

  const ColumnVector& testVector() const; // get test vector q
  void iterate(const ColumnVector& Aq);   // tell Arnoldi the value of Aq
  
  void orthocheck(); // save Q' Q into file QtQ.asc, should be I.

  int n() const;     // current iteration number
  int Niter() const; // total number iterations

  const ComplexColumnVector& ew(); // current estimate of eigenvals
  const ComplexMatrix& ev();       // current estimate of eigenvecs

private:
  int M_;     // dimension of linear operator (A is M x M)
  int Niter_; // number of Arnoldi iterations
  int n_;     // current iteration number
  Real condition_;

  MatrixXd H_;
  MatrixXd Q_;
  MatrixXd Vn_;
  ColumnVector qn_;
  ColumnVector v_;

  ComplexColumnVector ew_;
  ComplexMatrix ev_;

  void eigencalc();
};


// Iterative GMRES solution to Ax=b. 
// Usage for classic iterative GMRES solution of Ax=b:
//
// GMRES gmres(b, N);
// for (int n=0; n<N; ++n) {
//   ColumnVector q  = gmres.testVector();
//   ColumnVector Aq = A*q; // or however else you calculate A*q
//   gmres.iterate(Aq);
//   ColumnVector x = gmres.solution(); // current estimate of soln
//   cout << "krylov residual == " << gmres.residual() << endl;
// }
//
// Additional functionality: 
// Find approx solution x' of Ax'=b' projected into current Krylov subspace.
// Real residual;
// Vector xprime = grmes.solve(bprime, residual);


class GMRES {
public:
  GMRES();
  GMRES(const ColumnVector& b, int Niterations, Real minCondition=1e-13);

  const ColumnVector& testVector() const;
  void iterate(const ColumnVector& A_testvec);

  const ColumnVector& solution() const; // current best approx to soln x of Ax=b
  const ColumnVector& guess() const;
  Real residual() const;  // |Ax-b|/|b|

  int n() const;
  int Niter() const;

  MatrixXd Hn() const;  // Hn  = (n+1) x n submatrix of H
  MatrixXd Qn() const;  // Qn  = M x n     submatrix of Q
  MatrixXd Qn1() const; // Qn1 = M x (n+1) submatrix of Q

  const MatrixXd& Q() const;  // Q is large, so avoid copying it

  //ColumnVector solve(const ColumnVector& bprime, Real& residual);
  
private:
  int M_;     // dimension of vector space
  int Niter_; // max number of iterations
  int n_;     // current iteration number
  Real condition_;

  MatrixXd H_;
  MatrixXd Q_;
  MatrixXd Vn_;
  ColumnVector x0_; // guess;
  ColumnVector v_;
  ColumnVector qn_;
  ColumnVector xn_;
  Real bnorm_;
  Real residual_;
};

// Iterative solution to Ax=b with initial guess x0.
// Internally, let A (x0+y) = b and solve A y = b - A x0 with GMRES
// Residual is |Ax-b|/|b|, not |Ax-b|/|b-Ax0|
class GMRESguess {
public:
  GMRESguess(int Niterations, const ColumnVector& b, Real minCondition=1e-10);
  GMRESguess(int Niterations, const ColumnVector& b, const ColumnVector& x0, const ColumnVector& Ax0,
	     Real minCondition=1e-13);

  const ColumnVector& testVector() const;
  void iterate(const ColumnVector& A_testvec);

  ColumnVector solution() const;
  Real krylovResidual() const;

  int n() const;
  int Niter() const;

private:
  GMRES gmres_;
  ColumnVector x0_;
  Real residualScale_; // |b-Ax0|/|b|
};



class HookstepParams {
public:
  HookstepParams(Real epsSearch = 1e-13, 
		 Real epsDx     = 1e-7,
		 bool centdiff  = false,
		 int  Nnewton   = 20,
		 int  Nhook     = 20,
		 Real deltaInit = 0.01,
		 Real deltaMin  = 1e-12,
		 Real deltaMax  = 1e+12,
		 Real deltaFuzz = 0.01,
		 Real lambdaMin = 0.10,
		 Real lambdaMax = 2.0,
		 Real lambdaRequiredReduction = 0.5,
		 Real improvReq = 1e-03,
		 Real improvOk   = 0.10,
		 Real improvGood = 0.75,
		 Real improvAcc  = 0.10,
		 std::ostream* logstream=&std::cout);

  Real epsSearch; // stop search if |f(x)| < epsSearch
  Real epsDx;     // scale for perturbs in finite-diff estimate of Df
  bool centdiff;  // used centered differences in estimating Df
  int  Nnewton;   // max number of Newton step
  int  Nhook;     // max number of hookstep iterations per Newton
  Real deltaInit; // initial trust region radius
  Real deltaMin;  // stop if radius of trust region gets this small
  Real deltaMax;  // don't let trust region radius get bigger than this
  Real deltaFuzz; // accept steps within (1+/-deltaFuzz)*delta
  Real lambdaMin; // minimum delta shrink rate
  Real lambdaMax; // maximum delta expansion rate
  Real lambdaRequiredReduction; // when reducing delta, reduce by at least this factor.
  Real improvReq; // reduce delta and recompute hookstep if residual is worse than this fraction of what we'd expect from gradient
  Real improvOk;  // accept step and keep same delta if improvement is better than this fraction of quadratic mode
  Real improvGood;// accept step and increase delta if improvement is better than this fraction of quadratic model
  Real improvAcc; // recompute hookstep with larger trust region if improvement is within this fraction quadratic prediction.
  ColumnVector xscale;     // rescale x during hookstep search, x -> x./xscale. Default == don't
  ColumnVector fscale;     // rescale f during hookstep search, f -> f./fscale. Default == don't
  std::ostream* logstream; // write search log here
  //string logfile;
};
std::ostream& operator<<(std::ostream& os, const HookstepParams& p);

enum HookstepPhase {ConstantDelta, ReducingDelta, IncreasingDelta, Finished};
enum ResidualImprovement {Unacceptable, Poor, Ok, Good, Accurate, NegaCurve};

std::ostream& operator<<(std::ostream& os, HookstepPhase p);
std::ostream& operator<<(std::ostream& os, ResidualImprovement i);

Real adjustLambda(Real lambda, Real lambdaMin, Real lambdaMax, std::ostream& os=std::cout);
Real adjustDelta(Real delta, Real rescale, Real deltaMin, Real deltaMax, std::ostream& os=std::cout);


// This enum is used for labeling diagnostic output data
enum fEvalType {fEval, DfEval, HookstepEval};

typedef ColumnVector (*Rn2Rnfunc)(const ColumnVector& x, int label, bool cacheable);

void rescale(ColumnVector& x, const ColumnVector& xscale); // x -> x./xscale
void unscale(ColumnVector& x, const ColumnVector& xscale); // x -> x.*xscale

ColumnVector hookstepSearch(Rn2Rnfunc f, const ColumnVector& x0);
ColumnVector hookstepSearch(Rn2Rnfunc f, const ColumnVector& x0, const HookstepParams& p, int& nsteps);

// returns u dot v/(|u||v|) or zero if either has zero norm
Real align(const ColumnVector& u, const ColumnVector& v);

typedef Real (*R2Rfunc)(Real x);

//Real bisectSearch(R2Rfunc f, Real a, Real b, Real feps=1e-14, int maxsteps=50);

//enum HookstepPhase {ConstantDelta, ReducingDelta, IncreasingDelta, Finished};
//std::ostream& operator<<(std::ostream& os, HookstepPhase p);

//enum ResidualImprovement {Unacceptable, Poor, Ok, Good, Accurate, NegaCurve};
//std::ostream& operator<<(std::ostream& os, ResidualImprovement i);

//Real adjustLambda(Real lambda, Real lambdaMin, Real lambdaMax);
//Real adjustDelta(Real delta, Real rescale, Real deltaMin, Real deltaMax);

enum SolutionType {Equilibrium, PeriodicOrbit};
std::ostream& operator<<(std::ostream& os, SolutionType solntype);


class GMRESHookstepFlags {
public:
  SolutionType solntype;
  bool xrelative; // search over x phase shift for relative EQB or TW
  bool zrelative; // search over z phase shift for relative EQB or TW
  Real epsSearch; // stop search if L2Norm(s f^T(u) - u) < epsEQB
  Real epsKrylov; // min. condition # of Krylov vectors
  Real epsDu;     // relative size of du to u in linearization
  Real epsDt;     // size of dT in linearization of f^T about T;
  Real epsGMRES;  // stop GMRES iteration when Ax=b residual is < this;
  Real epsGMRESf; // accept final GMRES iterate if residual is < this);
  bool centdiff;  // centered differencing to estimate differentials

  int  Nnewton;   // max number of Newton steps
  int  Ngmres;    // max number of GMRES iterations per restart;
  int  Nhook;     // max number of hookstep iterations per Newton

  Real delta;      // initial radius of trust region
  Real deltaMin;   // stop if radius of trust region gets this small
  Real deltaMax;   // maximum radius of trust region
  Real deltaFuzz;  // accept steps within (1+/-deltaFuzz)*delta
  Real lambdaMin;  // minimum delta shrink rate
  Real lambdaMax;  // maximum delta expansion rate
  Real lambdaRequiredReduction; // when reducing delta, reduce by at least this factor.
  Real improvReq;  // reduce delta and recompute hookstep if improvement is worse than this fraction of what we'd expect from gradient");
  Real improvOk ;  // accept step and keep same delta if improvement is better than this fraction of quadratic model
  Real improvGood; // accept step and increase delta if improvement is better than this fraction of quadratic model
  Real improvAcc;  // recompute hookstep with larger trust region if improvement is within this fraction quadratic prediction.

  bool dudtortho;  // add orthogonality constraint  L2IP(du,dudt) == 0 to search equations
  bool dudxortho;  // add orthogonality constraints L2IP(du,dudx) == L2IP(du,dudz) == 0 to search equations
  bool poincsrch;  // add h(u) == 0 to search equations
  bool poinconst;  // perform orbit search starting and stopping time integration on h(u) == 0 poincare section
  Real orthoscale; // rescale orthogonality constraints
  Real TscaleRel;  // scale time in hookstep: |T| = Ts*|u|/L2Norm(du/dt)
  Real Rscale;     // scale relative-search variables by this factor;
  Real unormalize; // normalize residual (sigma f^T(u) - u) by L2Norm3d(u/unormalize), to keep away from u=0 laminar
  bool autostop;   // give up if residual doesn't improve by stopfactor
  Real stopfactor;
  bool saveall;    // save fields at every newton step
  std::string outdir;  // directory for storing solutions
  std::ostream* logstream; // output stream for logging solution algorithm

  GMRESHookstepFlags(SolutionType solntype = Equilibrium,
		     bool xrelative = false,
		     bool zrelative = false,
		     Real epsSearch = 1e-13,
		     Real epsKrylov = 1e-14,
		     Real epsDu = 1e-07,
		     Real epsDt = 1e-07,
		     Real epsGMRES = 1e-03,
		     Real epsGMRESf = 0.05,
		     bool centdiff = false,
		     int  Nnewton = 30,
		     int  Ngmres = 50,
		     int  Nhook = 20,
		     Real delta = 1e-02,
		     Real deltaMin = 1e-08,
		     Real deltaMax = 1e-01,
		     Real deltaFuzz = 1e-02,
		     Real lambdaMin = 0.2,
		     Real lambdaMax = 1.5,
		     Real lambdaRequiredReduction = 0.5,
		     Real improvReq = 1e-03,
		     Real improvOk  = 1e-01,
		     Real improvGood = 0.75,
		     Real improvAcc = 1e-01,
		     bool dudtortho = true,
		     bool dudxortho = true,
		     bool poincsrch = false,
		     bool poinconst = false,
		     Real orthoscale = 1,
		     Real TscaleRel = 20,
		     Real Rscale = 1.0,
		     bool unormalize = false,
		     bool autostop   = false,
		     Real stopfactor = 0.98,
		     bool saveall = false,
		     std::string outdir = "./",
		     std::ostream* logstream = &std::cout);

  //GMRESHookstepFlags(ArgList& args);

  //bool relative() const;
  //bool orbit() const;
};
std::ostream& operator<<(std::ostream& os, const GMRESHookstepFlags& f);

// Find solution of equation sigma f^T(u) - u == 0. Inputs are initial
// guess and return as solution values. Real return value is residual
// L2Norm(sigma f^T(u) - u)
Real GMRESHookstep(FlowField& u, Real& T, FieldSymmetry& sigma, 
		   PoincareCondition* h, const GMRESHookstepFlags& searchflags,
		   const DNSFlags& dnsflags, const TimeStep& dt, Real& CFL);

} // namespace channelflow

#endif // OCTAVE_UTILITIES_H
