#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include "matrixutils.h"
#include "channelflow/utilfuncs.h"
#include "channelflow/mathdefs.h"
#include "channelflow/periodicfunc.h"

using namespace std;
//using namespace channelflow;

using Eigen::JacobiSVD;
using Eigen::HouseholderQR;
using Eigen::EigenSolver;
using Eigen::ComputeFullU;
using Eigen::ComputeFullV;

typedef JacobiSVD<MatrixXd> SVD;
typedef HouseholderQR<MatrixXd> QR;

namespace channelflow {

// ======================================================================
// All Octave-dependent code goes within this ifdef
// #ifdef HAVE_OCTAVE

void setToZero(ColumnVector& x) {
  for (int i=0; i<x.size(); ++i)
    x(i) = 0.0;
}

void setToZero(MatrixXd& A) {
  for (int i=0; i<A.rows(); ++i)
    for (int j=0; j<A.cols(); ++j)
      A(i,j) = 0.0;
}

void print(const ColumnVector& x) {
  for (int i=0; i<x.size(); ++i)
    cout << x(i) << '\n';
}
void print(const MatrixXd& A) {
  for (int i=0; i<A.rows(); ++i) {
    for (int j=0; j<A.cols(); ++j)
      cout << A(i,j) << ' ';
    cout << '\n';
  }
}


void save(const ColumnVector& x, const string& filebase) {
  string filename = appendSuffix(filebase, ".asc");
  ofstream os(filename.c_str());
  os << setprecision(17);
  os << "% " << x.size() << '\n';
  for (int i=0; i<x.size(); ++i)
    os << x(i) << '\n';
}

void save(const ComplexColumnVector& x, const string& filebase) {
  string filename = appendSuffix(filebase, ".asc");
  ofstream os(filename.c_str());
  os << setprecision(17);
  os << "% " << x.size() << '\n';
  for (int i=0; i<x.size(); ++i)
    os << Re(x(i)) << ' ' << Im(x(i)) << '\n';
}

void save(const MatrixXd& A, const string& filebase) {
  string filename = appendSuffix(filebase, ".asc");
  ofstream os(filename.c_str());
  os << setprecision(17);
  os << "% " << A.rows() << ' ' << A.cols() << '\n';
  for (int i=0; i<A.rows(); ++i) {
    for (int j=0; j<A.cols(); ++j)
      os << A(i,j) << ' ';
    os << '\n';
  }
}


void load(ColumnVector& x, const string& filebase) {

  string filename = appendSuffix(filebase, ".asc");
  ifstream is(filename.c_str());
  int N;
  char c;
  is >> c;
  if (c != '%') {
    string message("load(ColumnVector&, filebase): bad header in file ");
    message += filename;
    cferror(message);
  }
  is >> N;  
  x.resize(N);
  for (int i=0; i<x.size(); ++i)
    is >> x(i);
}

void load(ComplexColumnVector& x, const string& filebase) {

  string filename = appendSuffix(filebase, ".asc");
  ifstream is(filename.c_str());
  int N;
  char c;
  is >> c;
  if (c != '%') {
    string message("load(ComplexColumnVector&, filebase): bad header in file ");
    message += filename;
    cferror(message);
  }
  is >> N;  
  x.resize(N);
  Real xr, xi;
  for (int i=0; i<x.size(); ++i) {
    is >> xr >> xi;
    x(i) = Complex(xr, xi);
  }
}

void load(MatrixXd& A, const string& filebase) {
  string filename = appendSuffix(filebase, ".asc");
  ifstream is(filename.c_str());
  int M, N;
  char c;
  is >> c;
  if (c != '%') {
    string message("load(MatrixXd&, filebase): bad header in file ");
    message += filename;
    cferror(message);
  }
  is >> M >> N;  
  A.resize(M, N);
  for (int i=0; i<M; ++i) 
    for (int j=0; j<N; ++j)
      is >> A(i,j);
}



Real L2Norm2(const ColumnVector& x) {
  Real sum=0.0;
  for (int i=0; i<x.size(); ++i)
    sum += x(i)*x(i);
  return sum;
}

Real L2Dist2(const ColumnVector& x,  const ColumnVector& y) {
  assert(x.size() == y.size());
  Real sum=0.0;
  for (int i=0; i<x.size(); ++i)
    sum += square(x(i)-y(i));
  return sum;
}
Real L2Norm(const ColumnVector& x) {
  return sqrt(L2Norm2(x));
}

Real L2Dist(const ColumnVector& x,  const ColumnVector& y) {
  return sqrt(L2Dist2(x,y));
}

Real V2Norm2(const FlowField& u) {
  ColumnVector v;
  field2vector(u,v);
  return L2Norm2(v);
}

Real V2Norm(const FlowField& u) {
  return sqrt(V2Norm2(u));
}

Real V2IP(const FlowField& u, const FlowField& v) {
  ColumnVector uV;
  ColumnVector vV;
  field2vector(u,uV);
  field2vector(v,vV);
  return (uV.transpose())*vV;
}

Real Norm2(const FlowField& u, BasisType norm) {
  if (norm == L2Basis)
    return L2Norm2(u);
  else
    return V2Norm2(u);
}

Real Norm(const FlowField& u, BasisType norm) {
  if (norm == L2Basis)
    return L2Norm(u);
  else
    return V2Norm(u);
}

Real IP(const FlowField& u, const FlowField& v, BasisType norm) {
  if (norm == L2Basis)
    return L2IP(u,v);
  else
    return V2IP(u,v);
}

/***********************
void operator*=(ColumnVector& x, Real c) {
  for (int i=0; i<x.size(); ++i)
    x(i) *= c;
}
************************/

Real L2Norm2(const ComplexColumnVector& x) {
  Real sum=0.0;
  for (int i=0; i<x.size(); ++i)
    sum += abs2(x(i));
  return sum;
}

Real L2Dist2(const ComplexColumnVector& x,  const ComplexColumnVector& y) {
  assert(x.size() == y.size());
  Real sum=0.0;
  for (int i=0; i<x.size(); ++i)
    sum += abs2(x(i)-y(i));
  return sum;
}
Real L2Norm(const ComplexColumnVector& x) {
  return sqrt(L2Norm2(x));
}

Real L2Dist(const ComplexColumnVector& x,  const ComplexColumnVector& y) {
  return sqrt(L2Dist2(x,y));
}

void operator*=(ComplexColumnVector& x, Complex c) {
  for (int i=0; i<x.size(); ++i)
    x(i) *= c;
}

void swirling(const FlowField& u, FlowField& s) {
  s = swirling(u);
}

FlowField swirling(const FlowField& u_) {

  FlowField& u = (FlowField&) u_;
  fieldstate xzstate = u.xzstate();
  fieldstate ystate  = u.ystate();
  u.makeSpectral();  

  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();

  //u.cmplx(0,1,0,0) += 1.0; // add laminar base flow

  FlowField gradu = grad(u);
  gradu.makePhysical();

  FlowField swirl(Nx,Ny,Nz,1,u.Lx(),u.Lz(),u.a(),u.b(),Physical,Physical);
  MatrixXd G(3,3);

  for (int ny=0; ny<Ny; ++ny)
    for (int nx=0; nx<Nx; ++nx)
      for (int nz=0; nz<Nz; ++nz) {
	
	for (int i=0; i<3; ++i)
	  for (int j=0; j<3; ++j)
	    G(i,j) = gradu(nx,ny,nz,i3j(i,j));
	
	EigenSolver<MatrixXd> eigG(G);
	ComplexColumnVector lambda = eigG.eigenvalues();

	Real s = 0;
	for (int i=0; i<3; ++i)
	  s = Greater(s, imag(lambda(i)));
	
	swirl(nx,ny,nz,0) = s;
      }
  u.makeState(xzstate, ystate);
  swirl.makeSpectral();
  return swirl;
}
  
/**************
void fixDiri(ChebyCoeff& f) {
  Real fa = f.eval_a();
  Real fb = f.eval_b();
  Real mean = 0.5*(fb+fa);
  Real slop = 0.5*(fb-fa);
  f[0] -= mean;
  f[1] -= slop;
}

void fixDiriMean(ChebyCoeff& f) {
  Real fa = f.eval_a();
  Real fb = f.eval_b();
  Real fm = f.mean();
  f[0] -= 0.125*(fa+fb) + 0.75*fm;
  f[1] -= 0.5*(fb-fa);
  f[2] -= 0.375*(fa+fb) - 0.75*fm;
}

void fixDiriNeum(ChebyCoeff& f)  {

  Real ya = f.a();
  Real yb = f.b();
  f.setBounds(-1,1);
  Real a = f.eval_a();
  Real b = f.eval_b();
  Real c = f.slope_a();
  Real d = f.slope_b();

  // The numercial coeffs are inverse of the matrix (values found with Maple)
  // T0(-1)  T1(-1)  T2(-1)  T3(-1)     s0      a
  // T0(1)   T1(1)   T2(1)   T3(1)      s1      b
  // T0'(-1) T1'(-1) T2'(-1) T3'(-1)    s2  ==  c
  // T0'(1)  T1'(1)  T2'(1)  T3'(1)     s3      d

  Real s0 = 0.5*(a + b) + 0.125*(c - d);
  Real s1 = 0.5625*(b - a) - 0.0625*(c + d);
  Real s2 = 0.125*(d - c);
  Real s3 = 0.0625*(a - b + c + d);

  f[0] -= s0;
  f[1] -= s1;
  f[2] -= s2;
  f[3] -= s3;
  f.setBounds(ya,yb);
}

void fixDiri(ComplexChebyCoeff& f) {
  fixDiri(f.re);
  fixDiri(f.im);
}
void fixDiriMean(ComplexChebyCoeff& f) {
  fixDiriMean(f.re);
  fixDiriMean(f.im);
}

void fixDiriNeum(ComplexChebyCoeff& f) {
  fixDiriNeum(f.re);
  fixDiriNeum(f.im);
}
**********************/

void field2vector(const FlowField& u, ColumnVector& a) {

  assert(u.xzstate() == Spectral && u.ystate() == Spectral);
  int Kx = u.kxmaxDealiased();
  int Kz = u.kzmaxDealiased();
  int Ny = u.Ny();

  int N = 4*(Kx+2*Kx*Kz+Kz)*(Ny-3)+2*(Ny-2);

  /*****************
  if (a.size() == 0)
   a.resize_and_fill(N,0); // octave-2.x and octave-3.0 syntax
  else
    setToZero(a);
  ******************/
  if (a.size() == 0)
    a.resize(N, true); // octave-3.2 syntax
  setToZero(a);

  assert(a.size() >= N);


  int n=0;

  // These loops follow Gibson, Halcrow, Cvitanovic table 1.
  for (int ny=2; ny<Ny; ++ny)
    a(n++) = Re(u.cmplx(0,ny,0,0)); // Ny-2 modes (line 1 of table)

  for (int ny=2; ny<Ny; ++ny)
    a(n++) = Re(u.cmplx(0,ny,0,2)); // Ny-2 modes (line 2)

  // Some coefficients from the FlowField are linearly dependent due
  // to BCs and divergence constraint. Omit these from a(n), and reconstruct
  // them in vector2field(v,u) using the constraint equations. In what follows,
  // which coefficients are omitted is determined by the changing ny loop index
  // e.g. (ny=3; n<Ny-1; ++ny) omits ny=0,1 and Ny-1, and the constraint eqns
  // that allow their reconstruction are mentioned in comments to the right
  // of the loop. 2007-06-07 jfg.
  for (int kx=1; kx<=Kx; ++kx) {
    int mx = u.mx(kx);
    for (int ny=2; ny<Ny; ++ny) {      // w BCs => ny=0,1 coefficients
      a(n++) = Re(u.cmplx(mx,ny,0,2)); // J(Ny-2) modes (line 3a)
      a(n++) = Im(u.cmplx(mx,ny,0,2)); // J(Ny-2) modes (line 3b)
    }
    for (int ny=3; ny<Ny-1; ++ny) {    // u BCs => 0,1; v BC => 2; div => Ny-1
      a(n++) = Re(u.cmplx(mx,ny,0,0)); // J(Ny-4) modes (line 4a)
      a(n++) = Im(u.cmplx(mx,ny,0,0)); // J(Ny-4) modes (line 4b)
    }
  }
  for (int kz=1; kz<=Kz; ++kz) {
    int mz = u.mz(kz);
    for (int ny=2; ny<Ny; ++ny) {      // u BCs => 0,1; v BC => 2; div => Ny-1
      a(n++) = Re(u.cmplx(0,ny,mz,0)); // K(Ny-2)  (line 5a)
      a(n++) = Im(u.cmplx(0,ny,mz,0)); // K(Ny-2)  (line 5b)
    }
    for (int ny=3; ny<Ny-1; ++ny) {    // w BCs => 0,1; v BC => 2; div => Ny-1
      a(n++) = Re(u.cmplx(0,ny,mz,2)); // K(Ny-2)  (line 6a)
      a(n++) = Im(u.cmplx(0,ny,mz,2)); // K(Ny-2)  (line 6b)
    }
  }
  for (int kx=-Kx; kx<=Kx; ++kx) {
    if (kx==0)
      continue;

    int mx = u.mx(kx);
    for (int kz=1; kz<=Kz; ++kz) {
      int mz = u.mz(kz);
      for (int ny=2; ny<Ny; ++ny) {     // u BCs => 0,1;
	a(n++) = Re(u.cmplx(mx,ny,mz,0)); // JK(Ny-2)  (line 7a)
	a(n++) = Im(u.cmplx(mx,ny,mz,0)); // JK(Ny-2)  (line 7b)
      }
      for (int ny=3; ny<Ny-1; ++ny) { // w BCs => 0,1; v BC => 2; div => Ny-1
	a(n++) = Re(u.cmplx(mx,ny,mz,2)); // JK(Ny-4)  (line 8a)
	a(n++) = Im(u.cmplx(mx,ny,mz,2)); // JK(Ny-4)  (line 8b)
      }
    }
  }
}

void vector2field(const ColumnVector& a, FlowField& u) {

  u.setState(Spectral, Spectral);
  u.setToZero();

  int Kx=u.kxmaxDealiased();
  int Kz=u.kzmaxDealiased();
  int Ny=u.Ny(); // max polynomial order == number y gridpoints - 1
  Real ya = u.a();
  Real yb = u.b();
  Real Lx = u.Lx();
  Real Lz = u.Lz();

  assert(a.size() >= 4*(Kx+2*Kx*Kz+Kz)*(Ny-3)+2*(Ny-2));

  Complex zero(0.0, 0.0);
  ComplexChebyCoeff f0(Ny, ya, yb, Spectral);
  ComplexChebyCoeff f1(Ny, ya, yb, Spectral);
  ComplexChebyCoeff f2(Ny, ya, yb, Spectral);

  // These loops follow Gibson, Halcrow, Cvitanovic table 1.
  // The vector contains the linearly independent real numbers in a
  // zero-div no-slip FlowField. The idea is to extract these from the
  // vector and reconstruct the others from the BCs and div constraint
  // See corresponding comments in field2vector(u,v);

  int n=0;

  // =========================================================
  // (0,0) Fourier mode
  for (int ny=2; ny<Ny; ++ny)
    f0.re[ny] = a(n++);
  fixDiri(f0.re);
  for (int ny=0; ny<Ny; ++ny)
    u.cmplx(0,ny,0,0) = f0[ny];

  for (int ny=2; ny<Ny; ++ny)
    f2.re[ny] = a(n++);
  fixDiri(f2.re);
  for (int ny=0; ny<Ny; ++ny)
    u.cmplx(0,ny,0,2) = f2[ny];

  // =========================================================
  // (kx,0) Fourier modes, 0<kx
  for (int kx=1; kx<=Kx; ++kx) {
    int mx = u.mx(kx);

    // read in w(y) profile, except for ny=0,1 coeffs 
    for (int ny=2; ny<Ny; ++ny) {
      f2.re[ny] = a(n++);
      f2.im[ny] = a(n++);
    }
    fixDiri(f2); // determine ny=0,1 coeffs via dirichlet BCs

    // read in u(y) component, except for ny=0,1,2 and Ny-1 coeffs 
    for (int ny=3; ny<Ny-1; ++ny) {
      f0.re[ny] = a(n++);
      f0.im[ny] = a(n++);
    }
    f0.re[Ny-1] = 0.0; // ny=Ny-1 mode is 0 (why?)
    f0.im[Ny-1] = 0.0; // ditto
    fixDiriMean(f0);   // determine ny=0,1,2 coeffs via dirichlet BCs & zero mean

    // determine v(y) from incompressibility. v BCs satisfied automatically
    integrate(f0, f1); 
    f1.sub(0, 0.5*(f1.eval_a() + f1.eval_b()));
    f1 *= Complex(0.0, -(2*pi*kx)/Lx);

    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mx,ny,0,0) = f0[ny];
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mx,ny,0,1) = f1[ny];
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mx,ny,0,2) = f2[ny];

    // ------------------------------------------------------
    // Now copy conjugates of u(kx,ny,0,i) to u(-kx,ny,0,i). These are
    // redundant modes stored only for the convenience of FFTW.
    int mxm = u.mx(-kx);
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mxm,ny,0,0) = conj(f0[ny]);
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mxm,ny,0,1) = conj(f1[ny]);
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mxm,ny,0,2) = conj(f2[ny]);

  }


  // =========================================================
  // (0,kz) Fourier modes, 0<kz.
  for (int kz=1; kz<=Kz; ++kz) {
    int mz = u.mz(kz);
    
    for (int ny=2; ny<Ny; ++ny) {
      f0.re[ny] = a(n++);
      f0.im[ny] = a(n++);
    }
    fixDiri(f0);
    for (int ny=3; ny<Ny-1; ++ny) {
      f2.re[ny] = a(n++);
      f2.im[ny] = a(n++);
    }
    f2.re[Ny-1] = 0.0;
    f2.im[Ny-1] = 0.0;
    fixDiriMean(f2);

    integrate(f2, f1);
    f1.sub(0, 0.5*(f1.eval_a() + f1.eval_b()));
    f1 *= Complex(0.0, -(2*pi*kz)/Lz);

    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(0,ny,mz,0) = f0[ny];
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(0,ny,mz,1) = f1[ny];
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(0,ny,mz,2) = f2[ny];
  }

  // =========================================================
  for (int kx=-Kx; kx<=Kx; ++kx) {
    if (kx==0)
      continue;

    int mx = u.mx(kx);
    for (int kz=1; kz<=Kz; ++kz) {
      int mz = u.mz(kz);

      for (int ny=2; ny<Ny; ++ny) {
	f0.re[ny] = a(n++);
	f0.im[ny] = a(n++);
      }
      fixDiri(f0);

      // f0 is complete. Load it into u.
      for (int ny=0; ny<Ny; ++ny)
	u.cmplx(mx,ny,mz,0) = f0[ny];

      for (int ny=3; ny<Ny-1; ++ny) {
	f2.re[ny] = a(n++);
	f2.im[ny] = a(n++);
      }
      f2.re[Ny-1] = -f0.re[Ny-1]*(kx*Lz)/(kz*Lx);
      f2.im[Ny-1] = -f0.im[Ny-1]*(kx*Lz)/(kz*Lx);

      // Adjust 0,1,2 coeffs of f2 so that f2(+/-1) = 0 and
      // kz/Lz mean(f2) + kx/Lx mean(f0) == 0 (so that  v(1)==v(-1))
      Complex f2a = f2.eval_a();
      Complex f2b = f2.eval_b();
      Complex f0m = f0.mean();
      Complex f2m = f2.mean() + (kx*Lz)/(kz*Lx)*f0m;

      f2.sub(0, 0.125*(f2a+f2b) + 0.75*f2m);
      f2.sub(1, 0.5*(f2b-f2a));
      f2.sub(2, 0.375*(f2a+f2b) - 0.75*f2m);

      for (int ny=0; ny<Ny; ++ny)
	u.cmplx(mx,ny,mz,2) = f2[ny];

      f0 *= Complex(0, -2*pi*kx/Lx);
      f2 *= Complex(0, -2*pi*kz/Lz);
      f0 += f2;

      integrate(f0, f1);
      f1.sub(0, 0.5*(f1.eval_a() + f1.eval_b()));

      for (int ny=0; ny<Ny; ++ny)
	u.cmplx(mx,ny,mz,1) = f1[ny];
    }
  }
  u.setPadded(true);
}

void fixdivnoslip(FlowField& u) {
  ColumnVector v;
  field2vector(u,v);
  vector2field(v,u);
}

void field2vector(const FlowField& u, ColumnVector& v, const std::vector<RealProfileNG>& basis) {
  if (basis.size() == 0)
    field2vector(u,v);
  else {

    /*************************
    if (v.size() == 0)
      v.resize_and_fill(basis.size(), 0);
    else
      setToZero(v);
    ****************************/
    if (v.size() == 0)
      v.resize(basis.size(), true);
    setToZero(v);
    for (uint i=0; i<basis.size(); ++i)
      v(i) = L2InnerProduct(u, basis[i]);
  }
  return;
}


void vector2field(const ColumnVector& v, FlowField& u, const std::vector<RealProfileNG>& basis) {
  if (basis.size() == 0) {
    vector2field(v,u);
  }
  else {
    RealProfileNG prof;
    const Real EPSILON = 1e-16;
    u.setToZero();

    for (uint i=0; i<basis.size(); ++i) {
      if (abs(v(i)) < EPSILON)
	continue;
      prof = basis[i];
      prof *= v(i);
      u += prof;
    }
  }
}

void vector2field(const vector<RealProfile>& basis, const ColumnVector& a,
		  FlowField& u) {
  int N = basis.size();
  assert(a.size() >= N);
  u.setToZero();
  RealProfile tmp;

  const Real EPSILON = 1e-16;
  for (int n=0; n<N; ++n) {
    Real an = a(n);
    if (abs(an) < EPSILON)
      continue;

    tmp = basis[n];
    tmp *= an;

    assert(tmp.kx() <= u.kxmax() && tmp.kx() >= u.kxmin() &&
	   tmp.kz() <= u.kzmax() && tmp.kz() >= u.kzmin());

    u += tmp;
  }
}




void field2vector(const vector<RealProfile>& basis, const FlowField& u, ColumnVector& a) {

  int N = basis.size();
  /**********************
  if (a.size() == 0)
    a.resize_no_fill(N);
  ************************/

  if (a.size() == 0)
    a.resize(N);
  assert(a.size() >= N);

  for (int n=0; n<N; ++n)
    a(n) = L2InnerProduct(u, basis[n]);
}



void vector2field(const vector<BasisFunc>& basis, const ComplexColumnVector& a,
		  FlowField& u) {
  int N = basis.size();
  assert(a.size() >= N);
  u.setToZero();
  BasisFunc tmp;

  const Real EPSILON = 1e-16;
  for (int n=0; n<N; ++n) {
    Complex an = a(n);
    if (abs(an) < EPSILON)
      continue;

    tmp = basis[n];
    tmp *= an;

    assert(tmp.kx() <= u.kxmax() && tmp.kx() >= u.kxmin() &&
	   tmp.kz() <= u.kzmax() && tmp.kz() >= u.kzmin());

    u += tmp;
  }
}

void quicksort(ComplexColumnVector& z) {
  int N = z.size();
  array<Real> x(N);
  for (int n=0; n<N; ++n)
    x[n] = abs(z(n));
  array<int> i = quicksort(x);
  ComplexColumnVector w(N);
  for (int n=0; n<N; ++n)
    w(n) = z(i[N-n-1]);
  z = w;
}

// Does extra copying
void quicksort(ComplexColumnVector& Lambda, ComplexMatrix& V) {
  int M = V.rows();
  int N = V.cols();
  assert(Lambda.size() == N);

  array<Real> magn(N);
  for (int n=0; n<N; ++n)
    magn[n] = abs(Lambda(n));
  array<int> i = quicksort(magn);

  ComplexColumnVector Lambda_sorted(N);
  ComplexMatrix V_sorted(M,N);

  for (int n=0; n<N; ++n) {
    Lambda_sorted(n) = Lambda(i[N-n-1]);
    // V_sorted.insert(V.col(i[N-n-1]),0,n); // octave code
    V_sorted.col(n) = V.col(i[N-n-1]);
  }
  Lambda = Lambda_sorted;
  V = V_sorted;
}

/**********************************************************************
// Solve Hn x = bk via leastsquares
//      Q R x = bk
//        R x = Q^t bk
void leastsquares(const Matrix& Hn, ColumnVector& bk, ColumnVector& y,
		  Real& residual) {
  //cout << "entering leastsquares..." << endl;

  // Hn y = bk, Hn = QR
  // QQR y = bk
  //  R y = Q' bk;

  QR qr(Hn, QR::economy);
  Matrix Q = qr.Q();
  Matrix R = qr.R();
  int N = R.rows();
  RowVector c = bk.transpose()*Q;

  // Backsolve R y = Q* b
  for (int i=N-1; i>=0; --i) {
    Real sum = c(i);
    for (int j=i+1; j<N; ++j)
      sum -= R(i,j)*y(j);
    y(i) = sum/R(i,i);
  }

  residual = L2Norm(Hn*y-bk)/L2Norm(bk);
  //cout << "subspace residual = " << L2Norm(R*y-c)/L2Norm(c) << endl;
  //cout << "leastsqs residual = " << residual << endl;
  //cout << "L2Norm(bk)        == " << L2Norm(bk) << endl;
  //cout << "L2Norm(Hn*y-bk)   == " << L2Norm(Hn*y-bk) << endl;
  //cout << "L2Norm(Q'*bk)     == " << L2Norm(c) << endl;
  //cout << "L2Norm(R*y-Q'*bk) == " << L2Norm(R*y-c) << endl;
}
********************************************************/


Arnoldi::Arnoldi()
  :
  M_(0),
  Niter_(0),
  n_(0),
  condition_(0)
  //H_(0,0),
  //Q_(0,0),
  //Vn_(0,0),
  //qn_(0),
  //v_(0),
  //ew_(0),
  //ev_(0,0)
{}

Arnoldi::Arnoldi(const ColumnVector& b, int Niterations, Real minCondition)
  :
  M_(b.size()),
  Niter_(Niterations),
  n_(0),
  condition_(minCondition),
  H_(Niter_+1, Niter_),
  Q_(M_, Niter_+1),
  Vn_(0,0),
  qn_(b),
  v_(M_)
  //ew_(0,0),
  //ev_(0,0)
{
  setToZero(H_);
  setToZero(Q_);
  qn_ *= 1.0/L2Norm(qn_);
  qn_ *= 1.0/L2Norm(qn_);
  // Q_.insert(qn_,0,0); octave code
  Q_.col(0) = qn_; 
}

void Arnoldi::orthocheck() {

  ofstream os("QtQ.asc"); 
  
  for (int i=0; i<n_; ++i) {
    RowVector Qit = Q_.col(i).transpose();
    for (int j=0; j<n_; ++j) 
      os << Qit * Q_.col(j) << ' ';
    os << endl;
  }
}
    
    
    
  

void Arnoldi::iterate(const ColumnVector& Aq) {
  if (n_ == Niter_) {
    cerr << "warning : Arnoldi::iterate(Aq) : \n"
	 << "reached maximum number of iterations. doing nothing.\n";
  }
  v_ = Aq;

  // Orthogonalize v and insert it in Q matrix
  //cout << "calculating arnoldi iterate..." << endl;

  for (int j=0; j<=n_; ++j) {
    //cout << j << ' ' << flush;
    ColumnVector Qj = Q_.col(j);
    H_(j,n_) = Qj.transpose() * v_;
    v_ -= H_(j,n_)*Qj;
  }
  //cout << endl;
  Real vnorm = L2Norm(v_);

  if (abs(vnorm) < condition_) {
    H_(n_+1, n_) = 0.0;
    v_ *= 0.0;
    cerr << "Arnoldi breakdown. Exiting\n";
    exit(1);
  }

  else {
    H_(n_+1, n_) = vnorm;
    v_ *= 1.0/vnorm;
  }
  // Q_.insert(v_,0,n_+1); octave code
  Q_.col(n_+1) = v_;
  qn_ = Q_.col(n_+1);
  ++n_;
}

const ColumnVector& Arnoldi::testVector() const {
  return qn_;
}

int Arnoldi::n() const {return n_;}
int Arnoldi::Niter() const {return Niter_;}

const ComplexColumnVector& Arnoldi::ew() {
  if (ew_.size() != n_)
    eigencalc();
  return ew_;
}

void Arnoldi::eigencalc() {
  // do eigenvalue calculation inside a scope to kill off 
  //MatrixXd Hn = H_.extract(0,0,n_-1,n_-1);   // n x n octave code
  MatrixXd Hn = H_.block(0,0,n_,n_);   // n x n
  //cout << "Q == \n" << Q_ << endl;
  //cout << "H == \n" << H_ << endl;
  //cout << "Hn == \n" << Hn << endl;
  //  eignHn_ = EIG(Hn);
  EigenSolver<MatrixXd> eignHn(Hn);
  ew_ = eignHn.eigenvalues();
  ComplexMatrix W = eignHn.eigenvectors(); // n x n
  quicksort(ew_, W);

  // unroll this mult to avoid taking submatrix
  //MatrixXd Qn  = Q_.extract(0,0,M_-1,n_-1); // M x n
  //ev_ = Qn*W;                             // M x n times n x n == M x n

  //ev(i,j) = Qn(i,k) * W(k,j);
  ev_ = ComplexMatrix(M_,n_);
  for (int i=0; i<M_; ++i) 
    for (int j=0; j<n_; ++j) {
      Complex sum = 0.0;
      for (int k=0; k<n_; ++k) 
	sum += Q_(i,k)*W(k,j);
      ev_(i,j) = sum;
    }
};

const ComplexMatrix& Arnoldi::ev() {
  if (ev_.size() != n_)
    eigencalc();
  return ev_;
}


GMRES::GMRES()
  :
  M_(0),
  Niter_(0),
  n_(0),
  condition_(0),
  //H_(0,0),
  //Q_(0,0),
  //Vn_(0,0),
  //x0_(0),
  //v_(0),
  //qn_(0),
  //xn_(0),
  bnorm_(0),
  residual_(0)
{}

GMRES::GMRES(const ColumnVector& b, int Niterations, Real minCondition)
  :
  M_(b.size()),
  Niter_(Niterations),
  n_(0),
  condition_(minCondition),
  H_(Niter_+1, Niter_),
  Q_(M_, Niter_+1),
  //Vn_(0,0),
  //x0_(0),
  //v_(0),
  qn_(b),
  xn_(M_),
  bnorm_(L2Norm(b)),
  residual_(bnorm_)
{
  if (Niter_ > M_) {
    cerr << "GMRES::GMRES : Niterations can't exceed dimension of b. Resetting Niterations = dim(b)" << endl;
    Niter_ = M_;
    H_ = MatrixXd(M_, M_);
    Q_ = MatrixXd(M_, M_);
  }
  
  setToZero(H_);
  setToZero(Q_);
  setToZero(xn_);
  qn_ *= 1.0/L2Norm(qn_);
  qn_ *= 1.0/L2Norm(qn_);
  //Q_.insert(qn_,0,0); octave code
  Q_.col(0) = qn_;
}

/**********************************
GMRES::GMRES(const ColumnVector& guess, const ColumnVector& b, int Niterations, Real minCondition)
  :
  M_(b.size()),
  Niter_(Niterations),
  n_(0),
  condition_(minCondition),
  H_(Niter_+1, Niter_),
  Q_(M_, Niter_+1),
  Vn_(0,0),
  x0_(guess),
  v_(0),
  qn_(b),
  bnorm_(L2Norm(b))
{
  if (guess.size() != b.size()) {
    cerr << "error in : GMRES::GMRES(guess,b,Niterations,minCondition)" << endl;
    cerr << "guess.size() != b.size()). Exiting." << endl;
  }
  setToZero(H_);
  setToZero(Q_);
  qn_ *= 1.0/L2Norm(qn_);
  qn_ *= 1.0/L2Norm(qn_);
  Q_.insert(qn_,0,0);
}
************************************/


// Refer to Trefethen & Bau chapter 35 for algorithm and notation
void GMRES::iterate(const ColumnVector& Aq) {

  // Increment iteration count at start so that n=1 during first iteration,
  // (where we're orthogonalizing Aq w.r.t q0 = b/||b||) and notation matches 
  // Trefethen & Bau (except for 0-based matrix indices).

  // A   is m x m, 
  // Qn  is m x n, 
  // Qn1 is m x n+1
  // Hn  is n+1 x n   (Hn tilde)

  // Don't do any more iterations if we've reached Niter (otherwise we'd
  // reach beyond matrices allocated with Niters). 
  if (n_ == Niter_) {
    cerr << "warning : GMRES::iterate(Ab) : \n"
	 << "reached maximum number of iterations. doing nothing\n";
    return;
  }

  ++n_; 

  v_ = Aq;

  // Expand v=Aq in basis q0...q_{n-1}, storing coeffs in H. Gramm-Schmidt orthog, essentially.
  for (int j=0; j<n_; ++j) {
    ColumnVector Qj = Q_.col(j);
    H_(j,n_-1) = Qj.transpose() * v_;  // zero-based form of Trefethen's H(j,n)
    v_ -= H_(j,n_-1)*Qj;
  }
  Real vnorm = L2Norm(v_);


  ColumnVector y;

  // The normal Arnoldi/GMRES iteration, where {q0, ... q_{n-1}, Aq} produces 
  // {q0, ... q_{n-1}, q_n}. 
  if (n_ < M_) {
    
    // Normally the above Gramm-Schmidt orthogonalization of v_ = Aq leaves some
    // nonzero component orthog to all previous q's, that provides the next Krylov 
    // basis vector q_n (or in zero-based indices, q_n+1). But if v is within subspace 
    // previous q's, just replace it with a random vector (known as benign breakdown). 
    // There's a measure 0 chance that any particular random vector will also
    // spanned by Q, so allow for a limited number of retries.
    int maxretries = 10;
    int retries = 0;
    while (abs(vnorm) < condition_ && retries++ < maxretries) {
      cerr << "GMRES benign breakdown: \n"
	   << "  Using random vector instead of linearly dependent Krylov vector\n.";

      for (int i=0; i<v_.size(); ++i)
	v_(i) = randomReal();

      // This is an exact copy of the orthogonalization loop about twenty lines up.
      for (int j=0; j<n_; ++j) {
	ColumnVector Qj = Q_.col(j);
	H_(j,n_-1) = Qj.transpose() * v_;  // zero-based form of Trefethen's H(j,n)
	v_ -= H_(j,n_-1)*Qj;
      }
      vnorm = L2Norm(v_);
    }

    H_(n_, n_-1) = vnorm; // zero-based form of Trefethen's H(n+1,n)
    v_ *= 1.0/vnorm;

    qn_ = v_;              
    Q_.col(n_) = qn_;     // zero-based form of Trefethen's q_{n+1}, (n+1) th col of Q
  
    // The following code is wasteful of memory, due to extraction & storage of Hn and Qn
    // and wasteful of cpu time, due to repeated QR factorizations of Hn within leastsquares 
    // (each iteration adds one more column to Hn, so we're repeating the same QR decomp 
    // on columns 1,2,3,... of Hn each time). But Hn is small. 
    MatrixXd Hn = H_.block(0,0,n_+1,n_);      // get n+1 x n principal submatrix
    ColumnVector bk = ColumnVector(n_+1);     // allocate b in R^{n+1)           

    setToZero(bk);
    bk(0) = bnorm_;

    y = Hn.householderQr().solve(bk); 
    ColumnVector tmp = Hn*y-bk;
    residual_ = L2Norm(tmp)/L2Norm(bk);

  }
  // The last Arnoldi/GMRES iteration does not produce a new q, and Hn is square 
  // instead of oblong. 
  else if (n_ == M_) { 
    MatrixXd Hn = H_.block(0,0,n_,n_);      // get n x n principal submatrix
    ColumnVector bk(n_);                    // allocate b in R^(n==M)
    setToZero(bk);
    bk(0) = bnorm_;

    y = Hn.householderQr().solve(bk); 
    ColumnVector tmp = Hn*y-bk;
    residual_ = L2Norm(tmp)/L2Norm(bk);
  }      
  else {
    cferror("GMRES::iterate(...) error: iteration number n_ exceeds dimension of space M_. This should not be possible");
  }

  // Unroll this matrix mult to eliminate need for submatrix extraction & copy
  // xn_ = Qn*y; // M x n times n x 1 == M x 1
  
  //MatrixXd Qn = Q_.block(0,0, M_, n_);
  //xn_ = Qn*y;
  
  for (int i=0; i<M_; ++i) {
    Real sum = 0.0;
    for (int j=0; j<n_; ++j) 
      sum += Q_(i,j)*y(j);
    xn_(i) = sum;
  }
}

// GMRES provides a partial Hessenberg factorization Qn* A Qn = Hn. 
// So Ax=b projected into nth Krylov subspace is Qn* (Ax-b) = 0.
// Restrict x to nth Krylov subspace x = Qn y, giving 
// Qn* A Qn y = Qn* b
// Hn y = Qn* b
// Solve that last one w least squares. Assuming A is full rank, the residual 
// of Ax-b will be given by the magnitiude of the part of b that sticks out 
// of the nth Krylov subspace: r = |b - Q (Q*b)|/|b|

/*************************************************************************
ColumnVector GMRES::solve(const ColumnVector& bprime, Real& resid) {
  assert(bprime.size() == M_);

  //MatrixXd Hn = H_.extract(0,0,n_-1,n_-1); // n x n principal submatrix octave code
  MatrixXd Hn = H_.block(0,0,n_,n_);     // n x n principal submatrix
  //MatrixXd Qn = Q_.extract(0,0,M_-1,n_-1); // M x n principal submatrix

  // unroll
  //ColumnVector Qnb = (bprime.transpose()*Qn).transpose(); // avoid transposing Qn
  //
  // Qntb = (bprime' Qn)' = Qn' b has shape n x M times M x 1 == n x 1

  ColumnVector Qntb(n_);
  for (int i=0; i<n_; ++i) {
    Real sum = 0.0;
    for (int j=0; j<M_; ++j)
      sum += Q_(j,i)*bprime(j);
    Qntb(i) = sum;
  }
  
  // 2015-04-30 replacing octave code for solving Hn y = Qtnb
  //ColumnVector y(n_);
  //leastsquares(Hn, Qntb, y, resid);
  // with Eigen code
  ColumnVector y = Hn.householderQr().solve(Qntb);
  ColumnVector tmp = Hn*y-Qntb;
  resid = L2Norm(tmp)/L2Norm(Qntb);

  // unroll
  //ColumnVector error = Qn * Qntb; // M x n times n x 1 == M x 1
  ColumnVector error(M_);
  for (int i=0; i<M_; ++i) {
    Real sum = 0.0;
    for (int j=0; j<n_; ++j)
      sum += Q_(i,j)*Qntb(j);
    error(i) = sum;
  }  
  error -= bprime;
  resid = L2Norm(error)/L2Norm(bprime);

  // unroll
  //ColumnVector xprime = Qn*y; // M x n times n x 1 == M x 1
  ColumnVector xprime(M_);
  for (int i=0; i<M_; ++i) {
    Real sum = 0.0;
    for (int j=0; j<n_; ++j)
      sum += Q_(i,j)*y(j);
    xprime(i) = sum;
  }  
  return xprime;
}
***************************************************************************/

int GMRES::n() const {return n_;}
int GMRES::Niter() const {return Niter_;}

const ColumnVector& GMRES::testVector() const {
  return qn_;
}
const ColumnVector& GMRES::solution() const{
  return xn_;
}
Real GMRES::residual() const{
  return residual_;
}
MatrixXd GMRES::Hn() const {
  //return H_.extract(0,0,n_,n_-1);
  if (n_ < M_)
    return H_.block(0,0,n_+1,n_);
  else
    return H_;
}

MatrixXd GMRES::Qn() const {
  //return Q_.extract(0,0,M_-1,n_-1);
  return Q_.block(0,0,M_,n_);
}
MatrixXd GMRES::Qn1() const {
  //return Q_.extract(0,0,M_-1,n_);
  return Q_.block(0,0,M_,n_+1);
}
const MatrixXd& GMRES::Q() const {
  return Q_;
}

/******************************************
HookStepSearch::HookStepSearch()
  :
  newtonStep_(0),
  stuck_(false),
  residual_(0),
  delta_(0),
  x_(0),
  fx_(0),
{}

HookStepSearch::HookStepSearch(Rn2Rnfunc, const ColumnVector& x0);
  :
  newtonStep_(0),
  residual_(0),
  delta_(0),
  stuck_(false),
  Nunk_(0),
  x_(0),
  fx_(0),
{}


HookStepSearch::HookStepSearch(Rn2Rnfunc, const ColumnVector& x0);const HookstepParams& p) : params_(p) {
  newtonStep_(0),
  stuck_(false),
  residual_(0),
  Nunk_(0),
  delta_(0),
  x_(0),
  fx_(0),
}
****************************************************/

ColumnVector hookstepSearch(Rn2Rnfunc f, const ColumnVector& x0) {
  HookstepParams p;
  int foo;
  return hookstepSearch(f, x0, p, foo);
}

HookstepParams::HookstepParams(Real epsSearch_,
			       Real epsDx_,
			       bool centdiff_,
			       int  Nnewton_,
			       int  Nhook_,
			       Real deltaInit_,
			       Real deltaMin_,
			       Real deltaMax_,
			       Real deltaFuzz_,
			       Real lambdaMin_,
			       Real lambdaMax_,
			       Real lambdaRequiredReduction_,
			       Real improvReq_,
			       Real improvOk_,
			       Real improvGood_,
			       Real improvAcc_,
			       ostream* logstream_)
  :
  epsSearch(epsSearch_),
  epsDx(epsDx_),
  centdiff(centdiff_),
  Nnewton(Nnewton_),
  Nhook(Nhook_),
  deltaInit(deltaInit_),
  deltaMin(deltaMin_),
  deltaMax(deltaMax_),
  deltaFuzz(deltaFuzz_),
  lambdaMin(lambdaMin_),
  lambdaMax(lambdaMax_),
  lambdaRequiredReduction(lambdaRequiredReduction_),
  improvReq(improvReq_),
  improvOk(improvOk_),
  improvGood(improvGood_),
  improvAcc(improvAcc_),
  //xscale(0),
  //fscale(0),
  logstream(logstream_)
{}

std::ostream& operator<<(std::ostream& os, const HookstepParams& p) {
  os << "{\n";
  os << "epsSearch == " << p.epsSearch << '\n';
  os << "epsDx     == " << p.epsDx     << '\n';
  os << "centdiff  == " << p.centdiff  << '\n';
  os << "Nnewton   == " << p.Nnewton   << '\n';
  os << "Nhook     == " << p.Nhook     << '\n';
  os << "deltaInit == " << p.deltaInit << '\n';
  os << "deltaMin  == " << p.deltaMin  << '\n';
  os << "deltaMax  == " << p.deltaMax  << '\n';
  os << "deltaFuzz == " << p.deltaFuzz << '\n';
  os << "lambdaMin == " << p.lambdaMin << '\n';
  os << "lambdaMax == " << p.lambdaMax << '\n';
  os << "lambdaRequiredReduction == " << p.lambdaRequiredReduction << '\n';
  os << "improvReq == " << p.improvReq << '\n';
  os << "improvOk  == " << p.improvOk  << '\n';
  os << "improvGood== " << p.improvGood<< '\n';
  os << "improvAcc == " << p.improvAcc << '\n';
  os << "xscale == " << p.xscale << '\n';
  os << "fscale == " << p.fscale << '\n';
  os << "}\n";
  return os;
}


ColumnVector hookstepSearch(Rn2Rnfunc f, const ColumnVector& x0, const HookstepParams& p, int& nsteps) {

  Real epsSearch = p.epsSearch; // stop search if |f(x)| < epsSearch
  Real epsDx     = p.epsDx;     // scale for perturbs in finite-diff estimate of Df
  //bool centdiff  = p.centdiff;// used centered differences in estimating Df
  int  Nnewton   = p.Nnewton;   // max number of Newton step
  int  Nhook     = p.Nhook;     // max number of hookstep iterations per Newton
  Real deltaInit = p.deltaInit; // initial trust region radius
  Real deltaMin  = p.deltaMin;  // stop if radius of trust region gets this small
  Real deltaMax  = p.deltaMax;  // don't let trust region radius get bigger than this
  Real deltaFuzz = p.deltaFuzz; // accept steps within (1+/-deltaFuzz)*delta
  Real lambdaMin = p.lambdaMin; // minimum delta shrink rate
  Real lambdaMax = p.lambdaMax; // maximum delta expansion rate
  Real lambdaRequiredReduction = p.lambdaRequiredReduction;
  Real improvReq = p.improvReq;
  Real improvOk   = p.improvOk;
  Real improvGood = p.improvGood;
  Real improvAcc  = p.improvAcc;
  ColumnVector xscale = p.xscale;
  ColumnVector fscale = p.fscale;
  ostream* os = p.logstream;

  ofstream osnull;
  if (os == 0) {
    osnull.open("/dev/null");
    os = &osnull;
  }

  string outdir = "./";

  Real delta = deltaInit;
  int fcount_newton = 0;
  int fcount_hookstep = 0;

  const int Nunk = x0.size();
  const int Neqn = Nunk;
  int label;
  bool cacheable;

  ofstream xnewtos("xnewt.asc");
  ofstream dnewtos("dxnewt.asc");
  ofstream fnewtos("fnewt.asc");
  ofstream osconv("convergence.asc");
  osconv.setf(ios::left);
  osconv << setw(14) << "% residual" << setw(14) << "delta"
	 << setw(14) << "L2Norm(dx)" << setw(14) << "L2Norm(sN)"
	 << setw(14) << "L2Norm(sH)" << setw(10) << "ftotal"
	 << setw(10) << "fnewt" << setw(10) << "fhook" << endl;

  // ==============================================================
  // Initialize Newton iteration
  *os << "Computing f(x)" << endl;
  ColumnVector       x(x0);
  ColumnVector      fx = (*f)(x, label=0, cacheable=true); // 0 ==> current best guess
  ++fcount_newton;
  // From here on, scale/unscale x when interfacing with the external world
  rescale(x,xscale);

  ColumnVector      dx(Nunk); // temporary for increments in unknowns
  ColumnVector   Df_dx(Nunk); // "Df*dx" = 1/eps (f(x + eps dx)
  ColumnVector      xS(Nunk); // stores xN and xH, the Newton and Hookstep unknowns
  ColumnVector      fS(Nunk); // stores fN and fH, fS = f(xS);
  ColumnVector      sN(Nunk); // the Newton step

  setToZero(dx);
  //for (i=0; i<Nunk; ++i)
  //dx(i) = 0.0;

  Real rx = 0.0;        // Dennis & Schnabel residual r(x) = 1/2 ||f(x)||^2
  Real init_rx = 0.0;
  Real prev_rx = 0.0;
  Real snorm   = 0.0;   // norm of hookstep
  Real sNnorm  = 0.0;   // norm of newton step
  Real dxnorm  = 0.0;
  Real xnorm   = 0.0;
  bool stuck = false;

  for (int newtonStep=0; newtonStep<Nnewton; ++newtonStep) {

    nsteps = newtonStep;

    *os << "========================================================" << endl;
    *os << "Newton iteration number " << newtonStep << endl;

    // Compute quantities that will be used multiple times
    // fx     = f(x) - x
    dxnorm = L2Norm(dx);
    xnorm = L2Norm(x);
    rx = 0.5*L2Norm2(fx);

    *os << "Current state of Newton iteration:" << endl;
    *os << "fcount_newton  == " << fcount_newton << endl;
    *os << "fcount_hookstep== " << fcount_hookstep << endl;
    *os << "    L2Norm(x)  == " << xnorm << endl;
    *os << "   L2Norm(dx)  == " << dxnorm <<endl;
    *os << "   L2Norm(sH)  == " << snorm <<endl;
    *os << "L2Dist(x,x0)   == " << L2Dist(x, x0) << endl;
    *os << "rx == 1/2 Norm2(f(x)) : " << endl;
    *os << "   initial  rx == " << init_rx << endl;
    *os << "   previous rx == " << prev_rx << endl;
    *os << "   current  rx == " << rx << endl;
    *os << "             x == ";
    for (int i=0; i<Nunk; ++i) *os << x(i) << ' ';
    *os << endl;
    *os << "            dx == ";
    for (int i=0; i<Nunk; ++i) *os << dx(i) << ' ';
    *os << endl;
    *os << "            fx == ";
    for (int i=0; i<Nunk; ++i) *os << fx(i) << ' ';
    *os << endl;
    *os << "        delta  == " << delta << endl;

    if (newtonStep == 0) {
      init_rx = rx;
      prev_rx = rx;
    }

    osconv.setf(ios::left);
    osconv.setf(ios::left); osconv << setw(14) << rx;
    osconv.setf(ios::left); osconv << setw(14) << delta;
    osconv.setf(ios::left); osconv << setw(14) << dxnorm;
    osconv.setf(ios::left); osconv << setw(14) << sNnorm;
    osconv.setf(ios::left); osconv << setw(14) << snorm;
    osconv.setf(ios::left); osconv << setw(10) << fcount_newton + fcount_hookstep;
    osconv.setf(ios::left); osconv << setw(10) << fcount_newton;
    osconv.setf(ios::left); osconv << setw(10) << fcount_hookstep;
    osconv << endl;

    unscale( x,xscale);
    unscale(dx,xscale);
    save(x, outdir + "xnewt" + i2s(newtonStep));
    save(x, outdir + "xbest");
    for (int i=0; i<Nunk; ++i) {
      xnewtos << x(i) << ' ';
      dnewtos << dx(i) << ' ';
      fnewtos << fx(i) << ' ';
    }
    xnewtos << endl;
    dnewtos << endl;
    fnewtos << endl;
    rescale( x,xscale);
    rescale(dx,xscale);

    if (rx < epsSearch) {
      *os << "Newton search converged. Breaking." << endl;
      *os << "1/2 L2Norm(f(x)) == " << rx << " < "
	   << epsSearch << " == " << epsSearch << endl;
      break;
    }
    else if (stuck) {
      *os << "Newton search is stuck. Breaking." << endl;
      break;
    }
    else if (newtonStep == Nnewton) {
      *os << "Reached maximum number of Newton steps. Breaking." << endl;
      break;
    }

    prev_rx = rx;

    // Set up RHS vector b = -f(x)
    ColumnVector b(Neqn);
    setToZero(b);
    b = fx; // field2vector(fu, b, basis);
    b *= -1.0;

    // Estimate f_ij = df_i/dx_j with finite differences
    //               = 1/eps (f_i(x + eps e_j) - f_i(x))
    MatrixXd Df(Nunk, Nunk);

    for (int j=0; j<Nunk; ++j) {
      ColumnVector f_x_dxj(Nunk);
      ColumnVector x_dxj = x;
      x_dxj(j) += epsDx;
      unscale(x_dxj, xscale);
      f_x_dxj = (*f)(x_dxj, label=-1, cacheable=false); // -1 ==> evaluating Df

      for (int i=0; i<Nunk; ++i)
	Df(i,j) = (f_x_dxj(i)- fx(i))/epsDx;
    }
    fcount_newton += Nunk*Nunk;

    //save(Df, "Df");
    /*********************************
    // Octave SVD solve
    SVD svd(Df, SVD::std);
    MatrixXd V = svd.right_singular_matrix();
    MatrixXd Ut = svd.left_singular_matrix().transpose();
    DiagMatrix D = svd.singular_values();
    *os << "D == ";
    for (int i=0; i<D.rows(); ++i)
      *os << D(i,i) << ' ';
    *os << endl;
    //cfpause();

    solve(Ut, D, V, b, sN);
    *************************************/
    
    JacobiSVD<MatrixXd> svd(Df, ComputeFullU | ComputeFullV);
    sN = svd.solve(b);
    //sN = Df.jacobiSvd().solve(b);
    sNnorm = L2Norm(sN);
    *os << "newton step sN == " << sN(0) << ' ' << sN(1) << endl;

    MatrixXd Ut = svd.matrixU().transpose();
    MatrixXd V  = svd.matrixV();
    ColumnVector D = svd.singularValues(); 
    *os << "D == ";
    for (int i=0; i<D.size(); ++i)
      *os << D(i) << ' ';
    *os << endl;
    // ==================================================================
    // Hookstep algorithm

    *os << "------------------------------------------------" << endl;
    *os << "Beginning hookstep calculations." << endl;

    int Nk = Nunk;

    ColumnVector bh = Ut*b; // b hat

    int hookcount = 0;
    bool have_backup  = false;
    Real backup_snorm = 0.0;
    Real backup_delta = 0.0;
    Real backup_rS    = 0.0;

    ColumnVector backup_dx;
    ColumnVector backup_fS;

    ColumnVector sH;

    //bool deltahas_decreased = false;
    Real deltaMaxLocal = deltaMax;

    // Find a good trust region and the optimal hookstep in it.
    // Quality of trust region
    while (true) {

      *os << "-------------------------------------------" << endl;
      *os << "Newton, hookstep number " << newtonStep << ", "
	   << hookcount++ << endl;
      *os << "delta == " << delta << endl;

      bool hookstep_equals_newtonstep;

      if (L2Norm(sN) <= delta) {
	*os << "Newton step is within trust region: " << endl;
	*os << "L2Norm(sN) == " << L2Norm(sN) << " <= " << delta << " == delta" << endl;
	hookstep_equals_newtonstep = true;
	sH = sN;
      }
      else {
	*os << "Newton step is outside trust region: " << endl;
	*os << "L2Norm(sN) == " << L2Norm(sN) << " > " << delta << " == delta" << endl;
	*os << "Calculate hookstep sH with radius L2Norm(sH) == delta" << endl;

	hookstep_equals_newtonstep = false;

	// This for-loop determines the hookstep. Search for value of mu that
	// produces ||shmu|| == delta. That provides optimal reduction of
	// quadratic model of residual in trust region norm(shmu) <= delta.
	Real mu = 0.0;
	Real nshmu = 0;
	ColumnVector shmu(Nk); // (s hat)(mu)]

	for (int hookSearch=0; hookSearch<Nhook; ++hookSearch) {
	  for (int i=0; i<Nk; ++i)
	    shmu(i) = bh(i)/(D(i) + mu); // research notes
	  nshmu = L2Norm(shmu);
	  Real Phi = nshmu*nshmu - delta*delta;
	  *os << "mu, L2Norm(sH(mu)) == " << mu << ", " << nshmu << endl;

	  // Found satisfactory value of mu and thus shmu and sH s.t. |sH| < delta
	  if (nshmu < delta || (nshmu > (1-deltaFuzz)*delta && nshmu < (1+deltaFuzz)*delta))
	    break;

	  // Update value of mu and try again. Update rule is a Newton search for
	  // Phi(mu)==0 based on a model of form a/(b+mu) for norm(sh(mu)). See
	  // Dennis & Schnabel.
	  else if (hookSearch<Nhook-1) {
	    Real PhiPrime = 0.0;
	    for (int i=0; i<Nk; ++i) {
	      Real di_mu = D(i) + mu;
	      Real bi = bh(i);
	      PhiPrime -= 2*bi*bi/(di_mu*di_mu*di_mu);
	    }
	    mu -= (nshmu/delta) * (Phi/PhiPrime);
	  }
	  else {
	    *os << "Couldn't find solution of hookstep optimization eqn Phi(mu)==0" << endl;
	    *os << "This shouldn't happen. It indicates an error in the algorithm." << endl;
	    *os << "Exiting.\n";
	    exit(1);
	  }
	} // search over mu for norm(s) == delta

	*os << "Found hookstep of proper radius" << endl;
	*os << "nshmu == " << L2Norm(shmu) << " ~= " << delta << " == delta " << endl;
	sH = V*shmu;
      } // else {Newton step outside trust region, so

      // Compute
      // (1) actual residual from evaluation of
      //     rS == r(s+ds) act  == 1/2 (f(x+dx),f(x+dx))
      //
      // (2) predicted residual from quadratic model of r(s)
      //     rP == r(s+ds) pred  == 1/2 (f(x),f(x)) + (f(x), Df dx)
      //
      // (3) slope of r(s)
      //     dr/ds == (r(s + ds) - r(s))/|ds|
      //           == (f(x,T), Df dx /|ds|
      // where ( , ) is Vector inner product V2IP, which equals L2 norm of vector rep

      // (1) Compute actual residual of step, rS
      *os << "Computing residual of hookstep sH" << endl;
      dx = sH;
      dxnorm = L2Norm(dx);
      xS  = x;
      xS += dx;

      *os << "L2Norm(dx) == " << L2Norm(dx) << endl;
      unscale(xS, xscale);
      fS = (*f)(xS, newtonStep+1, cacheable=true);
      ++fcount_hookstep;
      rescale(xS, xscale);

      *os << endl;

      Real rS = 0.5*L2Norm2(fS);       // actual residual of hookstep
      Real Delta_rS = rS - rx;         // improvement in residual
      //*os << "r(s), r(s+ds) == " << rx << ", " << rS << endl;

      // (2) and (3) Compute quadratic model and slope
      *os << "Computing local quadratic model of residual" << endl;
      ColumnVector Df_dx = Df*dx;
      *os << endl;
      // Local quadratic and linear models of residual, based on Taylor exp at current position
      // Quadratic model of r(s+ds): rQ == 1/2 |f(x) + Df dx|^2
      // Linear  model   of r(s+ds): rL == 1/2 (f(x),f(x)) + (f(x), Df dx)

      Real Delta_rL = L2IP(fx,Df_dx); // rL  - 1/2 (f(x),f(x)) == (f(x), Df dx)
      Real rL = rx + Delta_rL;        // rL == 1/2 (f(x),f(x))  + (f(x), Df dx)

      if (rL >= rx) {
	*os << "error : local linear model of residual is increasing, indicating\n"
		    << "        that the solution to the Newton equations is inaccurate\n"
		    << "returning best answer so far" << endl;
	return x;
      }

      Df_dx += fx;
      Real rQ = 0.5*L2Norm2(Df_dx);  // rQ == 1/2 |f(x) + Df dx|^2
      Real Delta_rQ = rQ - rx;       // rQ == 1/2 |(f(x)|^2 - 1/2 |f(x) + Df dx|^2

      snorm = L2Norm(sH);

      // ===========================================================================
      *os << "Determining what to do with current Newton/hookstep and trust region" << endl;

      // Coefficients for non-local quadratic model of residual, based on r(s), r'(s) and r(d+ds)
      // dr/ds == (f(x), Df dx)/|ds|
      Real drds  = Delta_rL/snorm;

      // Try to minimize a quadratic model of the residual
      // r(s+ds) = r(s) + r' |ds| + 1/2 r'' |ds|^2
      Real lambda = -0.5*drds*snorm/(rS - rx - drds*snorm);

      // Compare the actual reduction in residual to the quadratic model of residual.
      // How well the model matches the actual reduction will determine, later on, how and
      // when the radius of the trust region should be changed.
      Real Delta_rS_req  = improvReq*drds*snorm;    // the minimum acceptable change in residual
      Real Delta_rS_ok   = improvOk*Delta_rQ;       // acceptable, but reduce trust region for next newton step
      Real Delta_rS_good = improvGood*Delta_rQ;     // acceptable, keep same trust region in next newton step
      Real Delta_rQ_acc  = abs(improvAcc*Delta_rQ); // for accurate models, increase trust region and recompute hookstep
      Real Delta_rS_accP = Delta_rQ + Delta_rQ_acc; //   upper bound for accurate predictions
      Real Delta_rS_accM = Delta_rQ - Delta_rQ_acc; //   lower bound for accurate predictions

      // Characterise change in residual Delta_rS
      ResidualImprovement improvement;  // fine-grained characterization

      // Place improvement in contiguous spectrum: Unacceptable > Poor > Ok > Good.
      if (Delta_rS > Delta_rS_req)
	improvement = Unacceptable;     // not even a tiny fraction of linear rediction
      else if (Delta_rS > Delta_rS_ok)
	improvement = Poor;             // worse than small fraction of quadratic prediction
      else if (Delta_rS < Delta_rS_ok && Delta_rS > Delta_rS_good)
	improvement = Ok;
      else {
	improvement = Good;             // not much worse or better than large fraction of prediction
	if (Delta_rS_accM <= Delta_rS &&  Delta_rS <= Delta_rS_accP)
	  improvement = Accurate;         // close to quadratic prediction
	else if (Delta_rS < Delta_rL)
	  improvement = NegaCurve;        // negative curvature in r(|s|) => try bigger step
      }
      int w = 14;

      *os << " rx      == " << setw(w) << rx            << " residual at current position" << endl;
      *os << " rS      == " << setw(w) << rS            << " residual of newton/hookstep" << endl;
      *os << "Delta_rS == " << setw(w) << Delta_rS      << " actual improvement in residual from newton/hookstep" << endl;
      *os << endl;

      if (improvement==Unacceptable)
      *os << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Unacceptable <------ " << endl;
      *os << "            " << setw(w) << Delta_rS_req  << " lower bound for acceptable improvement" << endl;
      if (improvement==Poor)
      *os << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Poor <------" << endl;
      *os << "            " << setw(w) << Delta_rS_ok   << " upper bound for ok improvement." << endl;
      if (improvement==Ok)
      *os << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Ok <------" << endl;
      *os << "            " << setw(w) << Delta_rS_good << " upper bound for good improvement." << endl;
      if (improvement==Good)
      *os << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Good <------" << endl;
      *os << "            " << setw(w) << Delta_rS_accP << " upper bound for accurate prediction." << endl;
      if (improvement==Accurate)
      *os << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Accurate <------" << endl;
      *os << "            " << setw(w) << Delta_rS_accM << " lower bound for accurate prediction." << endl;
      *os << "            " << setw(w) << Delta_rL      << " local linear model of improvement" << endl;
      if (improvement==NegaCurve)
      *os << "Delta_rS == " << setw(w) << Delta_rS      << " ------> NegativeCurvature <------" << endl;

      bool recompute_hookstep = false;

      *os << "lambda       == " << lambda << " is the reduction/increase factor for delta suggested by quadratic model" << endl;
      *os << "lambda*delta == " << lambda*delta << " is the delta suggested by quadratic model" << endl;

      // See comments at end for outline of this control structure

      // Following Dennis and Schnable, if the Newton step lies within the trust region,
      // reset trust region radius to the length of the Newton step, and then adjust it
      // further according to the quality of the residual.
      //if (hookstep_equals_newtonstep)
      //delta = snorm;

      // CASE 1: UNACCEPTABLE IMPROVEMENT
      // Improvement is so bad (relative to gradient at current position) that we'll
      // in all likelihood get better results in a smaller trust region.
      if (improvement == Unacceptable) {
	*os << "Improvement is unacceptable." << endl;

	if (have_backup) {
	  *os << "But we have a backup step that was acceptable." << endl;
	  *os << "Revert to backup step and backup delta, take the step, and go to next Newton iteration." << endl;
	  dx = backup_dx;
	  fS = backup_fS;
	  rS = backup_rS;

	  snorm = backup_snorm;
	  delta = backup_delta;

	  recompute_hookstep = false;
	}
	else {
	  *os << "No backup step is available." << endl;
	  *os << "Reduce trust region by minimizing local quadratic model and recompute hookstep." << endl;
	  deltaMaxLocal = delta;
	  lambda = adjustLambda(lambda, lambdaMin, lambdaRequiredReduction, *os);
	  delta  = adjustDelta(delta, lambda, deltaMin, deltaMax, *os);

	  if (delta > snorm) {
	    *os << "That delta is still bigger than the Newton step." << endl;
	    //*os << "This shouldn't happen. Control structure needs review." << endl;
	    *os << "Reducing delta to half the length of the Newton step." << endl;
	    *os << "  old delta == " << delta << endl;
	    delta = 0.5*snorm;
	    *os << "  new delta == " << delta << endl;
	  }
	  else if (delta < deltaMin) {
	    *os << "    delta == " << delta << endl;
	    *os << "delta min == " << deltaMin << endl;
	    *os << "Sorry, can't go below deltaMin. Returning current best solution" << endl;
	    return x;
	  }

	  //delta_has_decreased = true;
	  recompute_hookstep = true;
	}
      }

      // CASE 2: EXCELLENT IMPROVEMENT AND ROOM TO GROW
      // Improvement == Accurate or Negacurve means we're likely to get a significant improvement
      // by increasing trust region. Increase trust region by a fixed factor (rather than quadratic
      // model) so that trust-region search is monotonic.
      // Exceptions are
      //   have_backup && backup_rS < rS -- residual is getting wrose as we increase delta
      //   hookstep==newtonstep          -- increasing trust region won't change answer
      //   delta>=deltamax               -- we're already at the largest allowable trust region
      else if ((improvement == NegaCurve || improvement == Accurate)
	       && !(have_backup && backup_rS < rS)
	       && !hookstep_equals_newtonstep
	       && !(delta >= deltaMax)) {

	*os << "Improvement is " << improvement << endl;

	//lambda = adjustLambda(lambda, lambdaMin, lambdaMax);
	Real new_delta = adjustDelta(delta, lambdaMax, deltaMin, deltaMax, *os);

	if (new_delta < deltaMaxLocal) {
	  *os << "Continue adjusting trust region radius delta because" << endl;
	  *os << "Suggested delta has room: new_delta < deltaMaxLocal " << new_delta << " < " << deltaMaxLocal << endl;
	  if (have_backup)
	    *os << "And residual is improving:     rS < backup_sS     " << rS << " < " << backup_rS << endl;

	  *os << "Increase delta and recompute hookstep." << endl;
	  have_backup = true;
	  backup_dx = dx;
	  backup_fS = fS;
	  backup_rS = rS;
	  backup_snorm = snorm;
	  backup_delta = delta;

	  recompute_hookstep = true;
	  *os << " old delta == " << delta << endl;
	  delta = new_delta;
	  *os << " new delta == " << delta << endl;
	}
	else {
	  *os << "Stop adjusting trust region radius delta and take step because the new delta" << endl;
	  *os << "reached a local limit:  new_delta >= deltaMaxLocal " << new_delta << " >= " << deltaMaxLocal << endl;
	  *os << "Reset delta to local limit and go to next Newton iteration" << endl;
	  delta = deltaMaxLocal;
	  *os << "  delta == " << delta << endl;
	  recompute_hookstep = false;
	}
      }

      // CASE 3: MODERATE IMPROVEMENT, NO ROOM TO GROW, OR BACKUP IS BETTER
      // Remaining cases: Improvement is acceptable: either Poor, Ok, Good, or
      // {Accurate/NegaCurve and (backup is superior || Hookstep==NewtonStep || delta>=deltaMaxLocal)}.
      // In all these cases take the current step or backup if better.
      // Adjust delta according to accuracy of quadratic prediction of residual (Poor, Ok, etc).
      else {
	*os << "Improvement is " << improvement <<  " (some form of acceptable)." << endl;
	*os << "Stop adjusting trust region and take a step because" << endl;
	*os << "Improvement is merely Poor, Ok, or Good : " << (improvement == Poor || improvement == Ok || improvement == Good) << endl;
	*os << "Newton step is within trust region      : " << hookstep_equals_newtonstep << endl;
	*os << "Backup step is better                   : " << (have_backup && backup_rS < rS) << endl;
	*os << "Delta has reached local limit           : " << (delta >= deltaMax) << endl;

	recompute_hookstep = false;

	// Backup step is better. Take it instead of current step.
	if (have_backup && backup_rS < rS) {
	  *os << "Take backup step and set delta to backup value." << endl;
	  dx = backup_dx;
	  fS = backup_fS;
	  rS = backup_rS;

	  *os << "  old delta == " << delta << endl;
	  snorm = backup_snorm;
	  delta = backup_delta;
	  *os << "  new delta == " << delta << endl;
	}

	// Current step is best available. Take it. Adjust delta according to Poor, Ok, etc.
	// Will start new monotonic trust-region search in Newton-hookstep, so be more
	// flexible about adjusting delta and use quadratic model.
	else {
	  if (have_backup) {
	    *os << "Take current step and keep current delta, since it's produced best result." << endl;
	    *os << "delta == " << delta << endl;
	    // Keep current delta because current step was arrived at through a sequence of
	    // adjustments in delta, and this value yielded best results.
	  }
	  else if (improvement == Poor || improvement == Ok) {
	    *os << "Take current step and reduce delta, because improvement is poor or Ok " << endl;
	    lambda = adjustLambda(lambda, lambdaMin, 1.0, *os);
	    delta  = adjustDelta(delta, lambda, deltaMin, deltaMax, *os);
	    if (delta < deltaMin) {
	      *os << "    delta == " << delta << endl;
	      *os << "delta min == " << deltaMin << endl;
	      *os << "Sorry, can't go below deltaMin. Returning current best solution" << endl;
	      return x;
	    }
	  }
	  else if (hookstep_equals_newtonstep || (delta >= deltaMax)) {
	    *os << "Take current step and leave delta unchanged, for the following reasons:" << endl;
	    *os << "improvement            : " << improvement << endl;
	    *os << "|newtonstep| <= delta  : " << (hookstep_equals_newtonstep ? "true" : "false") << endl;
	    *os << "delta >= deltaMax      : " << (delta >= deltaMax    ? "true" : "false") << endl;
	    //*os << "delta has decreased    : " << (delta_has_decreased ? "true" : "false") << endl;
	    *os << "delta == " << delta << endl;
	  }
	  else {  // improvement == Good, Accurate, or NegaCurve and no restriction on increasing apply
	    *os << "Take step and increase delta, if there's room." << endl;
	    lambda = adjustLambda(lambda, 1.0, lambdaMax, *os);
	    delta  = adjustDelta(delta, lambda, deltaMin, deltaMax, *os);
	  }
	}
      }
      //*os << "Recompute hookstep ? " << (recompute_hookstep ? "yes" : "no") << endl;

      if (recompute_hookstep)
	continue;
      else
	break;
    }

    *os << "Taking best step and continuing Newton iteration." <<endl;
    x += dx;
    dxnorm = L2Norm(dx);
    xnorm  = L2Norm(x);

    fx = fS;

    *os << "L2Norm(f(x)) == " << L2Norm(fx) << endl;
    *os << "rx           == " << rx << endl;
    *os << "L2Norm(sN)   == " << L2Norm(sN) << endl;
    *os << "L2Norm(sH)   == " << L2Norm(sH) << endl;
    *os << "L2Norm(dx)   == " << dxnorm << endl;
    //*os << "x ==\n " << x << endl;

  }
  unscale(x,xscale);
  return x;
}

ostream& operator<<(ostream& os, HookstepPhase p) {
  if      (p == ReducingDelta)   os << "ReducingDelta";
  else if (p == IncreasingDelta) os << "IncreasingDelta";
  else                           os << "Finished";
  return os;
}
ostream& operator<<(ostream& os, ResidualImprovement i) {
  if      (i == Unacceptable) os << "Unacceptable";
  else if (i == Poor)         os << "Poor";
  else if (i == Ok)           os << "Ok";
  else if (i == Good)         os << "Good";
  else if (i == Accurate)     os << "Accurate";
  else                        os << "NegativeCurvature";
  return os;
 }

void rescale(ColumnVector& x, const ColumnVector& xscale) {
  if (xscale.size()  != 0)
    for (int i=0; i<x.size(); ++i)
      x(i) /= xscale(i);
}
void unscale(ColumnVector& x, const ColumnVector& xscale) {
  if (xscale.size()  != 0)
    for (int i=0; i<x.size(); ++i)
      x(i) *= xscale(i);
}

Real adjustDelta(Real delta, Real deltaRate, Real deltaMin, Real deltaMax, ostream& os) {
  os  << "  old delta == " << delta << endl;

  // A special case: if deltaRate would take us from above to below deltaMin,
  // set it at exactly deltaMin and give it one last try. If next time throgh
  // we try to go even lower, go ahead and set it lower, and the calling function
  // will check and return best current answer

  if (delta == deltaMin && delta*deltaRate < deltaMin) {
    os << "delta bottoming out at deltaMin, try one more search" << endl;
    return deltaMin;
  }

  delta *= deltaRate;
  if (delta <= deltaMin) {
    delta = 0.5*deltaMin;
    os << "delta bottomed out at deltaMin" << endl;
  }
  if (delta > deltaMax) {
    delta = deltaMax;
    os << "delta topped out at deltaMax" << endl;
  }
  os << "  new delta == " << delta << endl;
  return delta;
}

Real adjustLambda(Real lambda, Real lambdaMin, Real lambdaMax, ostream& os) {
  if (lambda < lambdaMin) {
    os << "lambda == " << lambda << " is too small. Resetting to " << endl;
    lambda = lambdaMin;
    os << "lambda == " << lambda << endl;
  }
  else if (lambda < lambdaMin) {
    os << "lambda == " << lambda << " is too large. Resetting to " << endl;
    lambda = lambdaMax;
    os << "lambda == " << lambda << endl;
  }
  return lambda;
}

// solve Ax=b via SVD of A
//   U D V^t x = b
//           x = V D^-1 U^t b
/**************************
void solve(const Matrix& Ut, const DiagMatrix& D, const Matrix& V, const ColumnVector& b,
	   ColumnVector& x) {

  ColumnVector bh = Ut*b;
  const Real eps = 1e-12;
  for (int i=0; i<D.rows(); ++i) {
    if (abs(D(i,i)) > eps)
      bh(i) *= 1.0/D(i,i);
    else
      bh(i)  = 0.0;
  }
  x = V*bh;
}
*****************************/
Real L2IP(const ColumnVector& u, const ColumnVector& v) {
  if (u.size() != v.size()) {
    cerr << "error in L2IP(ColumnVector, ColumnVector) : vector length mismatch" << endl;
    exit(1);
  }
  Real sum = 0.0;
  for (int i=0; i<u.size(); ++i)
    sum += u(i) * v(i);
  return sum;
}

// returns u dot v/(|u||v|) or zero if either has zero norm
Real align(const ColumnVector& u, const ColumnVector& v) {
  Real norm = L2Norm(u)*L2Norm(v);
  if (norm == 0.0)
    return 0.0;
  else
    return L2IP(u,v)/norm;
}

Real bisectSearch(R2Rfunc f, Real a, Real b, Real feps, int maxsteps) {
  Real fa = (*f)(a);
  Real fb = (*f)(b);
  if (fa*fb > 0)
    cferror("bisectSearch(a,b,fn,xn) : a and b don't bracket a zero");

  Real c = 0.5*(a+b)/2;
  Real fc;

  for (int n=0; n<maxsteps; ++n) {
    c = 0.5*(a+b);
    fc = (*f)(c);
    if (abs(fc) < feps)
      break;
    if (fc*fa > 0) {
      a = c;
      fa = fc;
    }
    else {
      b = c;
      fb = fc;
    }
  }
  return c;
}

/************************************************************************
Real computehookstep(const GMRESHookstepFlags& searchflags,
		     const ColumnVector& dxN,
		     const DiagMatrix& D,
		     const Matrix& V,
		     const Matrix& Ut,
		     const Matrix& Qn,
		     const Matrix& Qn1,
		     const ColumnVector& bh,
		     Real delta,
		     ColumnVector& dxH,
		     bool& hookstep_equals_newtonstep,
		     ostream* os) 
{
  
  if (L2Norm(dxN) <= delta) {
    *os << "Newton step is within trust region: " << endl;
    *os << "L2Norm(dxN) == " << L2Norm(dxN) << " <= " << delta << " == delta" << endl;
    hookstep_equals_newtonstep = true;
    dxH = dxN;
  }
  
  else {
    *os << "Newton step is outside trust region: " << endl;
    *os << "L2Norm(dxN) == " << L2Norm(dxN) << " > " << delta << " == delta" << endl;
    *os << "Calculate hookstep dxH(mu) with radius L2Norm(dxH(mu)) == delta" << endl;

    hookstep_equals_newtonstep = false;

    // This for-loop determines the hookstep. Search for value of mu that
    // produces ||dxHmu|| == delta. That provides optimal reduction of
    // quadratic model of residual in trust region norm(dxHmu) <= delta.
    ColumnVector& dxHmu(dxH); // dxH(mu) : Hookstep dxH as a function of parameter
    Real ndxHmu = 0;          // norm of dxHmu
    Real mu = 0;              // a parameter we search over to find dxH with radius delta

    // See Dennis and Schnabel for this search-over-mu algorithm
    for (int hookSearch=0; hookSearch<searchflags.Nhook; ++hookSearch) {
      for (int i=0; i<Nk; ++i)
	dxHmu(i) = bh(i)/(D(i,i) + mu); // research notes
      ndxHmu = L2Norm(dxHmu);
      Real Phi = ndxHmu*ndxHmu - delta*delta;
      *os << "mu, L2Norm(dxH(mu)) == " << mu << ", " << ndxHmu << endl;

	  // Found satisfactory value of mu and thus dxHmu and dxH s.t. |dxH| < delta
	  if (ndxHmu < delta || (ndxHmu > (1-searchflags.deltaFuzz)*delta && ndxHmu < (1+searchflags.deltaFuzz)*delta))
	    break;

	  // Update value of mu and try again. Update rule is a Newton search for
	  // Phi(mu)==0 based on a model of form a/(b+mu) for norm(sh(mu)). See
	  // Dennis & Schnabel.
	  else if (hookSearch<searchflags.Nhook-1) {
	    Real PhiPrime = 0.0;
	    for (int i=0; i<Nk; ++i) {
	      Real di_mu = D(i,i) + mu;
	      Real bi = bh(i);
	      PhiPrime -= 2*bi*bi/(di_mu*di_mu*di_mu);
	    }
	    mu -= (ndxHmu/delta) * (Phi/PhiPrime);
	  }
	  else {
	    *os << "Couldn't find solution of hookstep optimization eqn Phi(mu)==0" << endl;
	    *os << "This shouldn't happen. It indicates an error in the algorithm." << endl;
	    *os << "Returning best answer so far\n";
	    return gx;
	  }
	} // search over mu for norm(s) == delta

	*os << "Found hookstep of proper radius" << endl;
	*os << "ndxHmu == " << L2Norm(dxHmu) << " ~= " << delta << " == delta " << endl;
	dxH = Qn*(V*dxHmu);
      } // end else clause for Newton step outside trust region


}
**************************************************/		     

  
Real GMRESHookstep(FlowField& u, Real& T, FieldSymmetry& sigma, 
		   PoincareCondition* hpoincare, const GMRESHookstepFlags& searchflags,
		   const DNSFlags& dnsflags, const TimeStep& dtarg, Real& CFL) {

  // get short names for the following heavily used searchflags fields
  ostream* os = searchflags.logstream; // a short name for ease of use

  Real delta  = searchflags.delta;

  //const BasisType nrm = l2basis ? L2Basis : AdHocBasis;
  const BasisType nrm = AdHocBasis;
  //const int w = coutprec + 7;
  *os << "GMRESHookstep search for solution (u,sigma,T) of u - sigma f^T(u) == 0" << endl;
  *os << "function inputs: " << endl;
  *os << setprecision(16);
  *os << "L2Norm(u)   == " << L2Norm(u) << endl;
  *os << "T           == " << T << endl;
  *os << "sigma       == " << sigma << endl;
  *os << "hpoincare   == " << (hpoincare == 0 ? "zero" : "nonzero") << endl;
  *os << "searchflags == " << searchflags << endl;
  *os << "dnsflags    == " << dnsflags << endl;
  *os << "dtarg       == " << dtarg << endl;
  *os << "CFL         == " << CFL << endl << endl;

  //*os << "searchflags.xrelative == " << searchflags.xrelative << endl;
  //*os << "searchflags.zrelative == " << searchflags.zrelative << endl;
  //*os << "L2Norm(u) == " << L2Norm(u) << endl;
  //*os << "       T  == " << T << endl;
  //*os << "   sigma  == " << sigma << endl;
  //*os << "dnsflags  == " << dnsflags << endl;
  //*os << "   dtarg  == " << dtarg << endl;

  // 2014-10-02 new treatment Poincare section, distinguishing btwn P-constrained DNS and P-section srch eqn.
  // const bool Tsearch    = (searchflags.solntype == PeriodicOrbit && hpoincare ==0) ? true : false;
  const bool Tsearch    = (searchflags.solntype == PeriodicOrbit && !searchflags.poinconst) ? true : false;
  const bool Rxsearch   = searchflags.xrelative;
  const bool Rzsearch   = searchflags.zrelative;
  const bool Tnormalize = searchflags.solntype == PeriodicOrbit ? false : true;
  const Real Unormalize = searchflags.unormalize;
  const bool autostop   = searchflags.autostop;
  const Real stopfactor = searchflags.stopfactor;
  
  // 2014-10-02 new treatment Poincare section, distinguishing btwn P-constrained DNS and P-section srch eqn.
  PoincareCondition* hpoincare_dns = searchflags.poinconst ? hpoincare : 0;

  TimeStep dt(dtarg);
  int fcount_newton = 0;
  int fcount_hookstep = 0;

  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();
  const Real Lx = u.Lx();
  const Real ya = u.a();
  const Real yb = u.b();
  const Real Lz = u.Lz();

  //const int kxmax = u.kxmaxDealiased();
  //const int kzmax = u.kzmaxDealiased();

  vector<RealProfileNG> basis;
  /**********************
  if (l2basis || s1 || s2 || s3) {
    *os << "constructing basis set (takes a long time)..." << endl;
    basis = realBasisNG( Ny, kxmax, kzmax, Lx, Lz, ya, yb);

    vector<FieldSymmetry> symms;
    if (s1) symms.push_back(FieldSymmetry( 1, 1,-1, 0.5, 0.0));
    if (s2) symms.push_back(FieldSymmetry(-1,-1, 1, 0.5, 0.5));
    if (s3) symms.push_back(FieldSymmetry(-1,-1,-1, 0.0, 0.5));
    if (s1 || s2 || s3)
      selectSymmetries(basis, symms);
    orthonormalize(basis);
  }
  **************************/

  project(dnsflags.symmetries, u, "initial guess u", *os);
  fixdivnoslip(u);

  ColumnVector dxN;   // "dx Newton" : the Newton step, computed w GMRES
  field2vector(u, dxN, basis);

  // Neqn == number of equations (total)
  // Nunk == number of unknowns

  const int uunk = dxN.size();                                // # of variables for u unknonwn
  const int Tunk = Tsearch  ?  uunk : -1;                       // index for T unknown
  const int xunk = Rxsearch ? (uunk + Tsearch) : -1;            // index for x-shift unknown
  const int zunk = Rzsearch ? (uunk + Tsearch + Rxsearch) : -1; // index for z-shift unknown
  const int Nunk = uunk + Tsearch + Rxsearch + Rzsearch;
  const int xeqn = xunk;
  const int zeqn = zunk;
  const int Teqn = Tunk;
  const int Neqn = Nunk;
  const int Ngmres2 = (Nunk < searchflags.Ngmres) ? Nunk : searchflags.Ngmres;

  *os << Neqn << " equations" << endl;
  *os << Nunk << " unknowns" << endl;
  if (Tsearch) {
    *os << "Teqn  == " << Teqn << endl;
    *os << "Tunk  == " << Tunk << endl;
  }
  if (Rxsearch) {
    *os << "xeqn  == " << xeqn << endl;
    *os << "xunk  == " << xunk << endl;
  }
  if (Rzsearch) {
    *os << "zeqn  == " << zeqn << endl;
    *os << "zunk  == " << zunk << endl;
  }
  if (searchflags.poincsrch && !hpoincare)
    cferror("GMRESHookstep: poincare-section search requires the poincare section to be defined");

  dxN = ColumnVector(Nunk);

  // These are purely diagnostic variables. I want to see how these directions change per iteration
  //ColumnVector prev_dxG(Nunk); // previous gradient direction
  ColumnVector prev_dxH(Nunk); // previous hookstep
  ColumnVector prev_dxN(Nunk); // previous newton

  // ==============================================================
  // Initialize Newton iteration
  // Notation:
  //    x = set of free variables: (u), (u,T), (u,sigma), or (u,sigma,T)
  // N means Newton, H means Hookstep, e.g.
  //   dxN is the Newton step
  //   dxH is the Hookstep

  FlowField      du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // temporary for increments in orbit velocity field
  FlowField      Gx(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // G(x) = G(u,sigma,T) = sigma f^T(u) - u
  FlowField   DG_dx(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // "DG*dx" = 1/e [G(u+e.du, sigma+e.dsigma, T+e.dT) - G(u,sigma,T)]
  FlowField       p(Nx, Ny, Nz, 1, Lx, Lz, ya, yb); // pressure field for time integration
  FlowField      uH(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // stores Newton uN = u + duN and Hookstep uH = u + duH fields
  FlowField      GH(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // stores GN = G(dx+dxH) and GH = G(dx + dxH)

  Real tH  = 0.0;  // stores tN = T + dTN and tH = T + dTH
  Real dT  = 0.0;  // Newton step for orbit period T
  Real dax = 0.0;  // Newton step for x phase shift
  Real daz = 0.0;  // Newton step for z phase shift

  //FieldSymmetry sigma(sx,sy,sz,ax,az,anti);
  //if (symname.size() != 0)
  //load(symname, T, sigma);
  *os << "Initial integration time T == " << T << endl;
  *os << "Initial symmetry sigma     == " << sigma << endl;

  FieldSymmetry dsigma; // identity, for now
  FieldSymmetry sigmaH(sigma);


  Real rx = 0.0;        // Dennis & Schnabel residual r(u,T) = 1/2 ||G(x)||^2 == 1/2 VNorm2(G(x))
  Real gx = 0.0;        // g(u,T) = L2Norm(G(x))
  Real init_rx = 0.0;
  Real init_gx = 0.0;
  Real prev_rx = 0.0;
  Real prev_gx = 0.0;
  Real gmres_residual = 0.0;
  Real dxHnorm  = 0.0;   // norm of hookstep
  Real dxNnorm  = 0.0;   // norm of newton step
  Real dxNalign = 0.0;   // alignment of current Newton step to previous (normalized inner product)
  Real dunorm  = 0.0;
  Real unorm   = 0.0;
  
  //Real Reynolds = abs(0.25*(dnsflags.uupperwall-dnsflags.ulowerwall)*(u.b()-u.a())/dnsflags.nu);
  //save(Reynolds, searchflags.outdir + "Reynolds");
  save(dnsflags.nu, searchflags.outdir + "nu");

  ofstream osconv((searchflags.outdir + "convergence.asc").c_str());
  osconv.setf(ios::left);
  osconv << setw(14) << "%-L2Norm(G)" << setw(14) << "r" << setw(14) << "delta";
  if (Tsearch)
    osconv << setw(14) << "dT";
  if (Rxsearch)
    osconv << setw(14) << "dax";
  if (Rzsearch)
    osconv << setw(14) << "daz";

  osconv << setw(14) << "L2Norm(du)" << setw(14) << "L2Norm(u)" << setw(14) << "L2Norm(dxN)" << setw(14) << "dxNalign"
	 << setw(14) << "L2Norm(dxH)"<< setw(14) << "GMRESerr"  << setw(10) << "ftotal" 
	 << setw(10) << "fnewt"	 << setw(10) << "fhook" << setw(10) << "CFL" << endl;

  // Determine which of four half-cell translations sigma minimizes 1/2 || sigma f^T(u) - u ||^2
  //*os << "Determining which of four half-cell translations sigma minimizes residual" << endl;
  *os << "Search for solutions of sigma f^T(u) - u == 0 with " << endl;
  *os << "    sigma == " << sigma << endl;
  *os << "     1/nu == " << 1.0/dnsflags.nu << endl;
  *os << setprecision(6);
  *os << "Computing G(x) = ";
  if (Tnormalize) 
    *os << "1/T ";
  if (Unormalize != 0.0) 
    //*os << "1/L2Norm3d(f(u)) ";    
    //*os << "1/tanh(L2Norm3d(f(u)/unnormalize)) ";    
    //*os << "1/sqrt(abs(funorm3d*(0.035-funorm3d))) ";
    //*os << "1/(funorm3d-0.025) ";
    *os << "1/(funorm3d-Unormalize) ";
  *os << " (sigma f(u,T) - u)" << endl;

  //G(u, T, sigma, Gx, Reynolds, dnsflags, dt, Tnormalize, fcount_hookstep, CFL, os);

  // 2014-10-02 new treatment Poincare section as enforced eqn to be solved for,
  // rather than a stopping condition for time-integration. 
  //cout << setprecision(16);
  //cout << " L2Norm(u) == " << L2Norm(u) << endl;
  //cout << "         T == " << T << endl;
  //cout << "         h == " << (hpoincare_dns ? 1 : 0) << endl;
  //cout << "     sigma == " << sigma << endl;
  //cout << "  dnsflags == " << dnsflags << endl;
  //cout << "        dt == " << dt << endl;
  //cout << "Tnormalize == " << Tnormalize << endl;
  //cout << "Unormalize == " << Unormalize << endl;
  //cout << "    fcount == " << fcount_hookstep << endl;
  //cout << "       CFL == " << CFL << endl;


  Gp(u, T, hpoincare_dns, sigma, Gx, dnsflags, dt, Tnormalize, Unormalize, fcount_hookstep, CFL, *os);
  *os << endl;

  for (int newtonStep = 0; newtonStep<=searchflags.Nnewton; ++newtonStep) {

    *os << "========================================================" << endl;
    *os << "Newton iteration number " << newtonStep << endl;
    *os << "Computing normalized dudt, edudx, edudz" << endl;

    // Compute quantities that will be used multiple times in GMRES loop
    // Gx     = (sigma f^T(u) - u)  or  1/T (sigma f^T(u) - u)
    // dfxdT  = 1/(dt) (f^(T+dt)(u) - f^(T)(u))
    // dudt   = 1/(dt) (f^(dt)(u) - u)
    //        = tangent vector to flow
    // edudx  = tangent vector of x translation
    // edudz  = tangent vector of z translation

    dunorm = L2Norm(du);
    unorm = L2Norm(u);
    rx = 0.5*Norm2(Gx, nrm);
    gx = L2Norm(Gx);

    if (newtonStep == 0) {
      init_gx = gx;
      prev_gx = gx;
      init_rx = rx;
      prev_rx = rx;
    }
    Real uwall_relative = dnsflags.uupperwall - dnsflags.ulowerwall; 
    Real wwall_relative = dnsflags.wupperwall - dnsflags.wlowerwall; 
    Real Uwall = pythag(uwall_relative/2, wwall_relative/2);
    Real theta = atan2(wwall_relative, uwall_relative);

    *os << "Current state of Newton iteration:" << endl;
    *os << "    L2Norm(u)  == " << unorm << endl;
    *os << "   L2Norm(du)  == " << dunorm <<endl;
    *os << "  L2Norm(dxH)  == " << dxHnorm <<endl;
    //*os << "          Re   == " << Reynolds << endl;
    *os << "       Uwall   == " << 0.5*(dnsflags.uupperwall-dnsflags.ulowerwall) << endl;
    *os << "       Ubulk   == " << dnsflags.Ubulk << endl;
    *os << "  L2Norm3d(u)  == " << L2Norm3d(u) << endl;
    *os << "        1/nu   == " << 1.0/dnsflags.nu << endl;
    *os << "        dPdx   == " << dnsflags.dPdx << endl;
    *os << "       Ubulk   == " << dnsflags.Ubulk << endl;
    *os << "       Uwall   == " << Uwall << endl;
    *os << "       theta   == " << theta << endl;
    *os << "           T   == " << T << endl;
    *os << "          dT   == " << dT << endl;
    *os << "          dt   == " << dt << endl;
    *os << "         sigma == " << sigma << endl;
    *os << "        dsigma == " << dsigma << endl;
    //*os << "L2Dist(u,u0) == " << L2Dist(u, FlowField(uname)) << endl;
    *os << "gx == L2Norm(G(x)) : " << endl;
    *os << "   initial  gx == " << init_gx << endl;
    *os << "   previous gx == " << prev_gx << endl;
    *os << "   current  gx == " << gx << endl;
    *os << "rx == 1/2 Norm2(G(x)) : " << endl;
    *os << "   initial  rx == " << init_rx << endl;
    *os << "   previous rx == " << prev_rx << endl;
    *os << "   current  rx == " << rx << endl;
    *os << "1/2 L2Norm2(G,u,T) : " << endl;
    *os << "               == " << 0.5*square(gx) << endl;
    if (searchflags.poincsrch) 
      *os << "          h(u) == " << (*hpoincare)(u) << endl;      

    *os << "CFL == " << CFL << endl;
    *os << "dsigma == " << dsigma << endl;

    osconv.setf(ios::left); osconv << setw(14) <<  gx;
    osconv.setf(ios::left); osconv << setw(14) << rx;
    osconv.setf(ios::left); osconv << setw(14) << delta;
    if (Tsearch) {
      osconv.setf(ios::left); osconv << setw(14) << dT;
    }
    if (Rxsearch) {
      osconv.setf(ios::left); osconv << setw(14) << dsigma.ax();
    }
    if (Rzsearch) {
      osconv.setf(ios::left); osconv << setw(14) << dsigma.az();
    }
    osconv.setf(ios::left); osconv << setw(14) << dunorm;
    osconv.setf(ios::left); osconv << setw(14) << unorm;
    osconv.setf(ios::left); osconv << setw(14) << dxNnorm;
    osconv.setf(ios::left); osconv << setw(14) << dxNalign;
    osconv.setf(ios::left); osconv << setw(14) << dxHnorm;
    osconv.setf(ios::left); osconv << setw(14) << gmres_residual;
    osconv.setf(ios::left); osconv << setw(10) << fcount_newton + fcount_hookstep;
    osconv.setf(ios::left); osconv << setw(10) << fcount_newton;
    osconv.setf(ios::left); osconv << setw(10) << fcount_hookstep;
    osconv.setf(ios::left); osconv << setw(10) << CFL;
    osconv << endl;

    string ns = i2s(newtonStep);
    //Real nu = viscosity(Reynolds, dnsflags, u.a(), u.b());
    //Real nu = 0.25*(ub-ua)*(u.b()-u.a())/Reynolds;
    u.setnu(dnsflags.nu);

    Real ubulk = Re(u.profile(0,0,0)).mean();
    //Real wbulk = Re(u.profile(0,0,2)).mean();
    if (abs(ubulk) < 1e-15) ubulk = 0.0;
    //if (abs(wbulk) < 1e-15) wbulk = 0.0;

    ChebyCoeff U = laminarProfile(dnsflags.nu, dnsflags.constraint, dnsflags.dPdx, dnsflags.Ubulk-ubulk, 
				  u.a(), u.b(), dnsflags.ulowerwall, dnsflags.uupperwall, u.Ny());

    U.save(searchflags.outdir + "Ubase");
    u.save(searchflags.outdir + "ubest");
    sigma.save(searchflags.outdir + "sigmabest");
    dnsflags.save(searchflags.outdir + "dnsflags");
    save(T, searchflags.outdir + "Tbest");
    save(1/dnsflags.nu, searchflags.outdir + "Reynolds");
    //save(dnsflags.nu, searchflags.outdir + "nu");
    save(dnsflags.dPdx, searchflags.outdir + "dPdx");
    save(Uwall,  searchflags.outdir + "Uwall");
    save(theta, searchflags.outdir + "theta");
    save(dnsflags.Ubulk, searchflags.outdir + "Ubulk");
    save(gx, searchflags.outdir + "residual");

    if (searchflags.saveall) {
      u.save(searchflags.outdir + "unewt" + ns);
      if (searchflags.xrelative || searchflags.zrelative)
	sigma.save(searchflags.outdir + "sigmanewt" + ns);
      if (searchflags.solntype == PeriodicOrbit)      
	save(T, searchflags.outdir + "Tnewt" + ns);
      //save(dnsflags.dPdx, searchflags.outdir + "dPdxnewt" + ns);
    }

    if (gx < searchflags.epsSearch) {
      *os << "Newton search converged. Returning residual" << endl
		 << "L2Norm(G(x))) == " << gx << " < "
		 << searchflags.epsSearch << " == " << searchflags.epsSearch << endl;
      return gx;
    }
    else if (autostop && newtonStep > 0 && sqrt(rx/prev_rx) > stopfactor) {
      *os << "Newton search is stuck. Returning best answer so far." << endl
		 << "L2Norm(G(x))) == " << gx << endl;
      return gx;
    }
    else if (newtonStep == searchflags.Nnewton) {
      *os << "Reached maximum number of Newton steps. Returning best answer so far." << endl
		 << "L2Norm(G(x))) == " << gx << endl;
      return gx;
    }

    prev_gx = gx;
    prev_rx = rx;

    // The following flowfields are used in calculating the additional constraint eqns
    // Don't initialize them unless they're used to save space
    FlowField edudx, edudz, edudt, u_du;
    Real hu = 0;
    if (searchflags.dudxortho) {
      edudx = xdiff(u);
      edudz = zdiff(u);
      edudx *= 1.0/L2Norm(edudx);
      edudz *= 1.0/L2Norm(edudz);
    }
    if (searchflags.dudtortho) {    
      edudt = u;
      f(u, 1, searchflags.epsDt, edudt, dnsflags, *os);   // using dt=epsDt timestep
      edudt -= u;
      //edudt *= 1.0/searchflags.epsDt; // to get dudt = (u(t+dt)-u(t))/dt, but undone by normalization
      edudt *= 1.0/L2Norm(edudt);
    }
    if (searchflags.poincsrch) 
      hu = (*hpoincare)(u);


    // Set Tscale so that |dT|*Tscale = TscaleRel |du|
    // Tscale = TscaleRel |du|/|dt|
    // O(t)*Tscale = O(u) => Tscale = u/t
    //const Real Tscale = TscaleRel*V2Norm(edudt);
    const Real Tscale = searchflags.TscaleRel;

    *os << "delta  == " << delta << endl;
    *os << "Tscale == " << Tscale << endl;

    // For GMRES solution of eqn L dx = b, set up RHS vector b = (-field2vector(G(x)), 0, 0, 0); 
    // (0, 0, 0) is the RHS of these three eqns (or some subset of them)
    // L2IP(du,edudt) == 0  for Tsearch 
    // L2IP(du,edudx) == 0  for Rxsearch
    // L2IP(du,edudz) == 0  for Rzsearch
    // In the case that we are the temporal invaraince is broken by poincare constraint h(u) == 0,
    // the t eqn is instead a newton-step eqn Dh du = -h(u)
    ColumnVector b(Neqn);
    setToZero(b);
    field2vector(Gx, b, basis);
    if (Tsearch) {
      if (searchflags.poincsrch) 
	b(Teqn) = hu;
      else
	b(Teqn) = 0;
    }
    if (Rxsearch)
      b(xeqn) = 0;
    if (Rzsearch)
      b(zeqn) = 0;
    b *= -1.0;

    GMRES gmres(b, Ngmres2, searchflags.epsKrylov);

    // ===============================================================
    // GMRES iteration to solve DG(x) dx = -G(x) for dx = (du, dsigma, dT)
    for (int n=0; n<Ngmres2; ++n) {

      *os << "Newt,GMRES == " << newtonStep << ',' << n << ", " << flush;

      // Compute v = Lq in Arnoldi iteration terms, where q is Q.col(n)
      // In Navier-Stokes terms, the main quantity to compute is
      // DG dx = 1/e (G(u + e du, sigma + e dsigma, T + e dT) - G(u,sigma,T)) for e << 1

      ColumnVector q = gmres.testVector();
      vector2field(q, du, basis);
      dT  = Tsearch  ? q(Tunk)*Tscale : 0.0;
      dax = Rxsearch ? q(xunk)*searchflags.Rscale : 0.0;
      daz = Rzsearch ? q(zunk)*searchflags.Rscale : 0.0;
      dsigma = FieldSymmetry(1,1,1,dax,daz);

      // Compute DG dx = 1/e (G(u+e.du, sigma+e.dsigma, T+e.dT) - G(u,sigma,T))
      //DG(u, du, T, dT, sigma, dsigma, Gx, DG_dx, dnsflags, dt, Tnormalize,
      //searchflags.epsDu,  searchflags.centdiff, fcount_newton, CFL, os);

      //DGp(u, du, T, dT, hpoincare, sigma, dsigma, Gx, DG_dx, dnsflags, dt, Tnormalize, Unormalize,
      //searchflags.epsDu,  searchflags.centdiff, fcount_newton, CFL, *os);

      // 2014-10-02 new treatment Poincare section 
      DGp(u, du, T, dT, hpoincare_dns, sigma, dsigma, Gx, DG_dx, dnsflags, dt, Tnormalize, Unormalize,
	  searchflags.epsDu,  searchflags.centdiff, fcount_newton, CFL, *os);

      // Assemble v = Lq from computed time integration DG_dx and constraint eqns
      // Note that there are options to ignore the constraint eqns by setting those
      // components of Lq to zero
      ColumnVector Lq(Neqn);
      field2vector(DG_dx, Lq, basis);

      // Three possible temporal constraint eqns du in T searches
      if (Tsearch) {
	
	// compute LHS of Dh du == -h(u) temporal constraint eqn
	if (searchflags.poincsrch) {  	  
	  // compute Dh du via approx Dh du = (h(u + eps du) - h(u))/eps
	  u_du = du;
	  u_du *= searchflags.epsDu;
	  u_du += u;
	  Real Dh_du = ((*hpoincare)(u_du) - hu)/searchflags.epsDu;
	  //*os << '\n';
	  //*os << "     Dh du == " << Dh_du << endl;
	  Lq(Teqn) = Dh_du;
	}
	else if (searchflags.dudtortho)   	// compute LHS of L2IP(du,edudt) == 0 temporal constraint eqn
	  Lq(Teqn) = searchflags.orthoscale*L2InnerProduct(du, edudt); 
	else
	  Lq(Teqn) = 0.0;                	// compute LHS of 0 == 0 temporal constraint eqn 

      }
      if (Rxsearch)
	Lq(xeqn) = searchflags.dudxortho ? searchflags.orthoscale*L2InnerProduct(du, edudx) : 0.0;
      if (Rzsearch)
	Lq(zeqn) = searchflags.dudxortho ? searchflags.orthoscale*L2InnerProduct(du, edudz) : 0.0;

      gmres.iterate(Lq);
      gmres_residual = gmres.residual();

      *os << " res == " << gmres_residual << endl;

      if (gmres_residual < searchflags.epsGMRES) {
        *os << "GMRES converged. Breaking." <<endl;
	dxN = gmres.solution();
	break;
      }
      else if (n==searchflags.Ngmres-1 && gmres_residual < searchflags.epsGMRESf ) {
        *os << "GMRES has not converged, but the final iterate is acceptable. Breaking." <<endl;
	dxN = gmres.solution();
      }
      else if (n==searchflags.Ngmres-1) {
	*os << "GMRES failed to converge. Returning best answer so far." << endl;
	return gx;
      }
    } // end GMRES iteration

    dxNnorm = L2Norm(dxN);
    dxNalign = align(dxN, prev_dxN);

    // Diagnostic
    //vector2field(dxN, du, basis);
    //du.save("dxN" + i2s(newtonStep));

    // ==================================================================
    // Hookstep algorithm

    *os << "------------------------------------------------" << endl;
    *os << "Beginning hookstep calculations." << endl;

    int Nk = gmres.n();     // Krylov dimension

    // The following submatrices of Q would be convenient, but they are large,
    // So use a reference to Q and do the multiplications manually
    //MatrixXd Qn = gmres.Qn();     // Large: Nunk x Nk
    //Matrix Qn1 = gmres.Qn1();     // Large: Nunk x Nk+1

    const MatrixXd& Q = gmres.Q();  // Qn is large: Nunk x ... (bigger than Nk+1)
    MatrixXd Hn = gmres.Hn();       // Hn is small: Nk+1 x Nk

    // Compute spectrum of DG, for diagnostic purposes 2017-11-21. This is a temporary addition
    // and should either be removed, commented out, or turned off by default with a switch
    /**************************
    {
        MatrixXd H = Hn.block(0,0,Nk,Nk);   // n x n
	EigenSolver<MatrixXd> eignH(H);
	ComplexColumnVector ew = eignH.eigenvalues();
	//ComplexMatrix W = eignHn.eigenvectors(); // n x n
	quicksort(ew);
	ew.save("DGLambda" + n2s(n));
    }
    *******************************/

    
    JacobiSVD<MatrixXd> svd(Hn, ComputeFullU | ComputeFullV);

    MatrixXd Um = svd.matrixU();  // Nk+1 x Nk  (named Um since U is already taken)
    MatrixXd V  = svd.matrixV();  //   Nk x Nk
    ColumnVector D = svd.singularValues();    //   Nk x Nk
    
    //Manual multiplication of 
    //ColumnVector btmp = Qn1.transpose() * b; // Nk+1 x Nunk  times  Nunk x 1 == Nk+1 x 1
    ColumnVector btmp(Nk+1);
    for (int i=0; i<Nk+1; ++i) {
      Real sum = 0.0;
      for (int j=0; j<Nunk; ++j)
	sum += Q(j,i)*b(j);
      btmp(i) = sum;
    }
    
    //Manual multiplication of 
    //ColumnVector bh = U.transpose()*btmp;  // Nk x Nk+1 times Nk+1 x 1 == Nk x 1
    ColumnVector bh(Nk);
    for (int i=0; i<Nk; ++i) {
      Real sum = 0.0;
      for (int j=0; j<Nk+1; ++j)
	sum += Um(j,i)*btmp(j);
      bh(i) = sum;
    }
    
    int hookcount = 0;

    // Hookstep algorithm requires iterated tweaking. Remember the best hookstep
    // with these variables, so we can revert to it if necessary.
    bool have_backup  = false;
    Real backup_dxHnorm = 0.0;
    Real backup_delta = 0.0;
    Real backup_rH    = 0.0;
    Real backup_tH    = 0.0;
    Real backup_dT    = 0;
    FlowField backup_du;
    FlowField backup_GH;
    FieldSymmetry backup_dsigma;

    ColumnVector dxH(Nunk);  // The hookstep
    //ColumnVector dxG;  // The gradient direction

    //bool delta_has_decreased = false;
    Real deltaMaxLocal = searchflags.deltaMax;

    // Find a good trust region and the optimal hookstep in it.
    while (true) {

      *os << "-------------------------------------------" << endl;
      *os << "Newton, hookstep number " << newtonStep << ", "
	   << hookcount++ << endl;
      *os << "delta == " << delta << endl;

      bool hookstep_equals_newtonstep;

      if (L2Norm(dxN) <= delta) {
	*os << "Newton step is within trust region: " << endl;
	*os << "L2Norm(dxN) == " << L2Norm(dxN) << " <= " << delta << " == delta" << endl;
	hookstep_equals_newtonstep = true;
	dxH = dxN;
      }
      else {
	*os << "Newton step is outside trust region: " << endl;
	*os << "L2Norm(dxN) == " << L2Norm(dxN) << " > " << delta << " == delta" << endl;
	*os << "Calculate hookstep dxH(mu) with radius L2Norm(dxH(mu)) == delta" << endl;

	hookstep_equals_newtonstep = false;

	// This for-loop determines the hookstep. Search for value of mu that
	// produces ||dxHmu|| == delta. That provides optimal reduction of
	// quadratic model of residual in trust region norm(dxHmu) <= delta.
	ColumnVector dxHmu(Nk); // dxH(mu) : Hookstep dxH as a function of parameter
	Real ndxHmu = 0;        // norm of dxHmu
	Real mu = 0;            // a parameter we search over to find dxH with radius delta

	// See Dennis and Schnabel for this search-over-mu algorithm
	for (int hookSearch=0; hookSearch<searchflags.Nhook; ++hookSearch) {

	  for (int i=0; i<Nk; ++i)
	    dxHmu(i) = bh(i)/(D(i) + mu); // research notes

	  ndxHmu = L2Norm(dxHmu);
	  Real Phi = ndxHmu*ndxHmu - delta*delta;
	  *os << "mu, L2Norm(dxH(mu)) == " << mu << ", " << ndxHmu << endl;

	  // Found satisfactory value of mu and thus dxHmu and dxH s.t. |dxH| < delta
	  if (ndxHmu < delta || 
	      (ndxHmu > (1-searchflags.deltaFuzz)*delta && ndxHmu < (1+searchflags.deltaFuzz)*delta)) {
	    *os << "Found hookstep of acceptable radius" << endl;
	    break;
	  }
	  // Update value of mu and try again. Update rule is a Newton search for
	  // Phi(mu)==0 based on a model of form a/(b+mu) for norm(sh(mu)). See
	  // Dennis & Schnabel.
	  else if (hookSearch<searchflags.Nhook-1) {
	    Real PhiPrime = 0.0;
	    for (int i=0; i<Nk; ++i) {
	      Real di_mu = D(i) + mu;
	      Real bi = bh(i);
	      PhiPrime -= 2*bi*bi/(di_mu*di_mu*di_mu);
	    }
	    mu -= (ndxHmu/delta) * (Phi/PhiPrime);
	  }
	  else {
	    *os << "Couldn't find solution of hookstep optimization eqn Phi(mu)==0" << endl;
	    *os << "This shouldn't happen. It indicates an error in the algorithm." << endl;
	    *os << "Returning best answer so far\n";
	    return gx;
	  }
	} // search over mu for norm(s) == delta

	*os << "L2Norm(dxHmu) == " << ndxHmu << " ~= " << delta << " == delta " << endl;
	*os << "L2Norm(dxHmu)-delta == " << ndxHmu - delta << endl;

	// Spell out this multiplication
	//dxH = Qn*(V*dxHmu); Nunk x Nk times  Nk x Nk times Nk x 1 == Nunk x 1
	
	ColumnVector vtmp = V*dxHmu; // Nk x Nk times Nk x 1 == Nk x 1

	// dxH = Qn*vtmp;  Nunk x Nk times Nk x 1 == Nunk x 1
	for (int i=0; i<Nunk; ++i) {
	  Real sum = 0.0;
	  for (int j=0; j<Nk; ++j)
	    sum += Q(i,j)*vtmp(j);
	  dxH(i) = sum;
	}	
      } // end else clause for Newton step outside trust region


      // Compute
      // (1) actual residual from evaluation of
      //     rH == r(x+dxH) act  == 1/2 (G(x+dxH), G(x+dxH))
      //
      // (2) predicted residual from quadratic model of r(x)
      //     rP == r(x+dx) pred  == 1/2 (G(x), G(x)) + (G(x), DG dx)
      //
      // (3) slope of r(x)
      //     dr/dx == (r(x + dx) - r(x))/|dx|
      //           == (G(x), DG dx)/|dx|
      // where ( , ) is FlowField inner product V2IP, which equals L2 norm of vector rep

      // (1) Compute actual residual of step, rH
      *os << "Computing residual of hookstep dxH" << endl;
      vector2field(dxH, du, basis);
      project(dnsflags.symmetries, du, "Newton step du", *os);
      fixdivnoslip(du);

      dunorm = L2Norm(du);
      uH  = u;
      uH += du;
      dT  = Tsearch  ? dxH(Tunk)*Tscale : 0;
      dax = Rxsearch ? dxH(xunk)*searchflags.Rscale : 0.0;
      daz = Rzsearch ? dxH(zunk)*searchflags.Rscale : 0.0;
      dsigma = FieldSymmetry(1,1,1,dax,daz);

      *os << "L2Norm(du) == " << L2Norm(du) << endl;
      *os << "       dT  == " << dT << endl;
      *os << "   dsigma  == " << dsigma << endl;
      tH  = T + dT;
      sigmaH = dsigma*sigma;

      //G(uH, tH, sigmaH, GH, dnsflags, dt, Tnormalize, fcount_hookstep, CFL, os);
      //Gp(uH, tH, hpoincare, sigmaH, GH,dnsflags, dt, Tnormalize, Unormalize, fcount_hookstep, CFL, *os);

      // 2014-10-02 new treatment Poincare section
      Gp(uH, tH, hpoincare_dns, sigmaH, GH,dnsflags, dt, Tnormalize, Unormalize, fcount_hookstep, CFL, *os);
      *os << endl;

      Real rH = 0.5*Norm2(GH, nrm); // actual residual of hookstep
      Real Delta_rH = rH - rx;     // improvement in residual
      *os << "r(x), r(x+dxH) == " << rx << ", " << rH << endl;

      // (2) and (3) Compute quadratic model and slope
      *os << "Computing local quadratic model of residual" << endl;
      //DG(u, du, T, dT, sigma, dsigma, Gx, DG_dx, dnsflags, dt, Tnormalize,
      //searchflags.epsDu,  searchflags.centdiff, fcount_newton, CFL, os);

      //DGp(u, du, T, dT, hpoincare, sigma, dsigma, Gx, DG_dx, dnsflags, dt, Tnormalize, Unormalize,
      //searchflags.epsDu,  searchflags.centdiff, fcount_newton, CFL, *os);

      // 2014-10-02 new treatment Poincare section
      DGp(u, du, T, dT, hpoincare_dns, sigma, dsigma, Gx, DG_dx, dnsflags, dt, Tnormalize, Unormalize,
	  searchflags.epsDu,  searchflags.centdiff, fcount_newton, CFL, *os);
      *os << endl;

      // Local quadratic and linear models of residual, based on Taylor exp at current position
      // Quadratic model of r(x+dx): rQ == 1/2 |G(x) + DG dx|^2
      // Linear  model   of r(x+dx): rL == 1/2 (G(x), G(x)) + (G(x), DG dx)

      Real Delta_rL = IP(Gx,DG_dx,nrm); // rL - 1/2 (G(x), G(x)) == (G(x), DG dx)
      Real rL = rx + Delta_rL;          // rL == 1/2 (G(x), G(x)) + (G(x), DG dx)

      if (rL >= rx) {
	*os << "error : local linear model of residual is increasing, indicating\n"
	     << "        that the solution to the Newton equations is inaccurate\n";

	if (searchflags.centdiff == true) {
	  *os << "Returning best answer so far." << endl;
	  return gx;
	}
	else {
	  *os << "Trying local linear model again, using centered finite differencing" << endl;
	  //DG(u, du, T, dT, sigma, dsigma, Gx, DG_dx, dnsflags, dt, Tnormalize,
	  //searchflags.epsDu,  true, fcount_newton, CFL, os);

	  //DGp(u, du, T, dT, hpoincare, sigma, dsigma, Gx, DG_dx, dnsflags, dt, Tnormalize, Unormalize,
	  //searchflags.epsDu,  true, fcount_newton, CFL, *os);

	  // 2014-10-02 new treatment Poincare section as enforced eqn to be solved for,
	  // rather than a stopping condition for time-integration. 
	  DGp(u, du, T, dT, hpoincare_dns, sigma, dsigma, Gx, DG_dx, dnsflags, dt, Tnormalize, Unormalize,
	      searchflags.epsDu,  true, fcount_newton, CFL, *os);

	  Delta_rL = IP(Gx,DG_dx,nrm); // rL - 1/2 (G(x),G(x)) == (G(x), DG dx)
	  rL = rx + Delta_rL;          // rL == 1/2 (G(x),G(x)) + (G(x), DG dx)

	  if (rL >= rx) {
	    *os << "error : centered differencing didn't help\n"
		 << "Returning best answer so far." << endl;
	    return gx;
	  }
	  // if we get here, centered differencing does help, and we can continue
	}
      }

      DG_dx += Gx;
      Real rQ = 0.5*Norm2(DG_dx,nrm);   // rQ == 1/2 |G(x) + DG dx|^2
      Real Delta_rQ = rQ - rx;          // rQ == 1/2 (G(x),G(x)) - 1/2 |G(x) + DG dx|^2

      dxHnorm = L2Norm(dxH);

      // ===========================================================================
      *os << "Determining what to do with current Newton/hookstep and trust region" << endl;

      // Coefficients for non-local quadratic model of residual, based on r(x), r'(x) and r(x+dx)
      // dr/dx == (G(x), DG dx)/|dx|
      Real drdx  = Delta_rL/dxHnorm;

      // Try to minimize a quadratic model of the residual
      // r(x+dx) = r(x) + r' |dx| + 1/2 r'' |dx|^2
      Real lambda = -0.5*drdx*dxHnorm/(rH - rx - drdx*dxHnorm);

      // Compare the actual reduction in residual to the quadratic model of residual.
      // How well the model matches the actual reduction will determine, later on, how and
      // when the radius of the trust region should be changed.
      Real Delta_rH_req  = searchflags.improvReq*drdx*dxHnorm;  // the minimum acceptable change in residual
      Real Delta_rH_ok   = searchflags.improvOk*Delta_rQ;       // acceptable, but reduce trust region for next newton step
      Real Delta_rH_good = searchflags.improvGood*Delta_rQ;     // acceptable, keep same trust region in next newton step
      Real Delta_rQ_acc  = abs(searchflags.improvAcc*Delta_rQ); // for accurate models, increase trust region and recompute hookstep
      Real Delta_rH_accP = Delta_rQ + Delta_rQ_acc; //   upper bound for accurate predictions
      Real Delta_rH_accM = Delta_rQ - Delta_rQ_acc; //   lower bound for accurate predictions

      // Characterise change in residual Delta_rH
      ResidualImprovement improvement;  // fine-grained characterization

      // Place improvement in contiguous spectrum: Unacceptable > Poor > Ok > Good.
      if (Delta_rH > Delta_rH_req)
	improvement = Unacceptable;     // not even a tiny fraction of linear prediction
      else if (Delta_rH > Delta_rH_ok)
	improvement = Poor;             // worse than small fraction of quadratic prediction
      else if (Delta_rH < Delta_rH_ok && Delta_rH > Delta_rH_good)
	improvement = Ok;
      else {
	improvement = Good;             // not much worse or better than large fraction of prediction
	if (Delta_rH_accM <= Delta_rH &&  Delta_rH <= Delta_rH_accP)
	  improvement = Accurate;         // close to quadratic prediction
	else if (Delta_rH < Delta_rL)
	  improvement = NegaCurve;        // negative curvature in r(|s|) => try bigger step
      }
      const int w = 13;
      *os << "rx       == " << setw(w) << rx            << " residual at current position" << endl;
      *os << "rH       == " << setw(w) << rH            << " residual of newton/hookstep" << endl;
      *os << "Delta_rH == " << setw(w) << Delta_rH      << " actual improvement in residual from newton/hookstep" << endl;
      *os << endl;

      if (improvement==Unacceptable)
      *os << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Unacceptable <------ " << endl;
      *os << "            " << setw(w) << Delta_rH_req  << " lower bound for acceptable improvement" << endl;
      if (improvement==Poor)
      *os << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Poor <------" << endl;
      *os << "            " << setw(w) << Delta_rH_ok   << " upper bound for ok improvement." << endl;
      if (improvement==Ok)
      *os << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Ok <------" << endl;
      *os << "            " << setw(w) << Delta_rH_good << " upper bound for good improvement." << endl;
      if (improvement==Good)
      *os << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Good <------" << endl;
      *os << "            " << setw(w) << Delta_rH_accP << " upper bound for accurate prediction." << endl;
      if (improvement==Accurate)
      *os << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Accurate <------" << endl;
      *os << "            " << setw(w) << Delta_rH_accM << " lower bound for accurate prediction." << endl;
      *os << "            " << setw(w) << Delta_rL      << " local linear model of improvement" << endl;
      if (improvement==NegaCurve)
      *os << "Delta_rH == " << setw(w) << Delta_rH      << " ------> NegativeCurvature <------" << endl;

      bool recompute_hookstep = false;

      *os << "lambda       == " << lambda << " is the reduction/increase factor for delta suggested by quadratic model" << endl;
      *os << "lambda*delta == " << lambda*delta << " is the delta suggested by quadratic model" << endl;

      // See comments at end for outline of this control structure

      // Following Dennis and Schnable, if the Newton step lies within the trust region,
      // reset trust region radius to the length of the Newton step, and then adjust it
      // further according to the quality of the residual.
      //if (hookstep_equals_newtonstep)
      //delta = dxHnorm;

      // CASE 1: UNACCEPTABLE IMPROVEMENT
      // Improvement is so bad (relative to gradient at current position) that we'll
      // in all likelihood get better results in a smaller trust region.
      if (improvement == Unacceptable) {
	*os << "Improvement is unacceptable." << endl;

	if (have_backup) {
	  *os << "But we have a backup step that was acceptable." << endl;
	  *os << "Revert to backup step and backup delta, take the step, and go to next Newton-GMRES iteration." << endl;
	  dsigma = backup_dsigma;
	  du = backup_du;
	  tH = backup_tH;
	  dT = backup_dT;
	  GH = backup_GH;
	  rH = backup_rH;
	  dxHnorm = backup_dxHnorm;
	  delta = backup_delta;

	  recompute_hookstep = false;
	}
	else {
	  *os << "No backup step is available." << endl;
	  *os << "Reduce trust region by minimizing local quadratic model and recompute hookstep." << endl;
	  deltaMaxLocal = delta;
	  lambda = adjustLambda(lambda, searchflags.lambdaMin, searchflags.lambdaRequiredReduction);
	  delta  = adjustDelta(delta, lambda, searchflags.deltaMin, searchflags.deltaMax, *os);
	  if (delta < searchflags.deltaMin) {
	    *os << "    delta == " << delta << endl;
	    *os << "delta min == " << searchflags.deltaMin << endl;
	    *os << "Sorry, can't go below deltaMin. Returning current best solution" << endl;
	    return gx;
	  }
	  else if (delta > dxHnorm) {
	    *os << "That delta is still bigger than the Newton step." << endl;
	    //*os << "This shouldn't happen. Control structure needs review." << endl;
	    *os << "Reducing delta to half the length of the Newton step." << endl;
	    *os << "  old delta == " << delta << endl;
	    delta = 0.5*dxHnorm;
	    *os << "  new delta == " << delta << endl;
	    if (delta < searchflags.deltaMin) {
	      *os << "But that is less than deltaMin == " << searchflags.deltaMin << endl;
	      *os << "I am afraid we have to stop now. Returning best solution." << endl;
	      return gx;
	    }
	  }
	  //delta_has_decreased = true;
	  recompute_hookstep = true;
	}
      }

      // CASE 2: EXCELLENT IMPROVEMENT AND ROOM TO GROW
      // Improvement == Accurate or Negacurve means we're likely to get a significant improvement
      // by increasing trust region. Increase trust region by a fixed factor (rather than quadratic
      // model) so that trust-region search is monotonic.
      // Exceptions are
      //   have_backup && backup_rH < rH -- residual is getting wrose as we increase delta
      //   hookstep==newtonstep          -- increasing trust region won't change answer
      //   delta>=deltamax               -- we're already at the largest allowable trust region
      else if ((improvement == NegaCurve || improvement == Accurate)
	       && !(have_backup && backup_rH < rH)
	       && !hookstep_equals_newtonstep
	       && !(delta >= searchflags.deltaMax)) {

	*os << "Improvement is " << improvement << endl;

	//lambda = adjustLambda(lambda, searchflags.lambdaMin, searchflags.lambdaMax);
	Real new_delta = adjustDelta(delta, searchflags.lambdaMax, searchflags.deltaMin, searchflags.deltaMax, *os);

	if (new_delta < deltaMaxLocal) {
	  *os << "Continue adjusting trust region radius delta because" << endl;
	  *os << "Suggested delta has room: new_delta < deltaMaxLocal " << new_delta << " < " << deltaMaxLocal << endl;
	  if (have_backup)
	    *os << "And residual is improving:     rH < backup_sS     " << rH << " < " << backup_rH << endl;

	  *os << "Increase delta and recompute hookstep." << endl;
	  have_backup = true;
	  backup_dsigma = dsigma;
	  backup_du = du;
	  backup_dT = dT;
	  backup_tH = tH;
	  backup_GH = GH;
	  backup_rH = rH;
	  backup_dxHnorm = dxHnorm;
	  backup_delta = delta;

	  recompute_hookstep = true;
	  *os << " old delta == " << delta << endl;
	  delta = new_delta;
	  *os << " new delta == " << delta << endl;
	}
	else {
	  *os << "Stop adjusting trust region radius delta and take step because the new delta" << endl;
	  *os << "reached a local limit:  new_delta >= deltaMaxLocal " << new_delta << " >= " << deltaMaxLocal << endl;
	  *os << "Reset delta to local limit and go to next Newton iteration" << endl;
	  delta = deltaMaxLocal;
	  *os << "  delta == " << delta << endl;
	  recompute_hookstep = false;
	}
      }

      // CASE 3: MODERATE IMPROVEMENT, NO ROOM TO GROW, OR BACKUP IS BETTER
      // Remaining cases: Improvement is acceptable: either Poor, Ok, Good, or
      // {Accurate/NegaCurve and (backup is superior || Hookstep==NewtonStep || delta>=deltaMaxLocal)}.
      // In all these cases take the current step or backup if better.
      // Adjust delta according to accuracy of quadratic prediction of residual (Poor, Ok, etc).
      else {
	*os << "Improvement is " << improvement <<  " (some form of acceptable)." << endl;
	*os << "Stop adjusting trust region and take a step because" << endl;
	*os << "Improvement is merely Poor, Ok, or Good : " << (improvement == Poor || improvement == Ok || improvement == Good) << endl;
	*os << "Newton step is within trust region      : " << hookstep_equals_newtonstep << endl;
	*os << "Backup step is better                   : " << (have_backup && backup_rH < rH) << endl;
	*os << "Delta has reached local limit           : " << (delta >= searchflags.deltaMax) << endl;

	recompute_hookstep = false;

	// Backup step is better. Take it instead of current step.
	if (have_backup && backup_rH < rH) {
	  *os << "Take backup step and set delta to backup value." << endl;
	  dsigma = backup_dsigma;
	  du = backup_du;
	  dT = backup_dT;
	  tH = backup_tH;
	  GH = backup_GH;
	  rH = backup_rH;

	  *os << "  old delta == " << delta << endl;
	  dxHnorm = backup_dxHnorm;
	  delta = backup_delta;
	  *os << "  new delta == " << delta << endl;
	}

	// Current step is best available. Take it. Adjust delta according to Poor, Ok, etc.
	// Will start new monotonic trust-region search in Newton-hookstep, so be more
	// flexible about adjusting delta and use quadratic model.
	else {
	  if (have_backup) {
	    *os << "Take current step and keep current delta, since it's produced best result." << endl;
	    *os << "delta == " << delta << endl;
	    // Keep current delta because current step was arrived at through a sequence of
	    // adjustments in delta, and this value yielded best results.
	  }
	  else if (improvement == Poor) {
	    *os << "Take current step and reduce delta, because improvement is poor." << endl;
	    lambda = adjustLambda(lambda, searchflags.lambdaMin, 1.0);
	    delta  = adjustDelta(delta, lambda, searchflags.deltaMin, searchflags.deltaMax, *os);
	    *os << "delta new == " << delta << endl;
	    if (delta < searchflags.deltaMin) {
	      *os << "delta min == " << searchflags.deltaMin << endl;
	      *os << "Reduced delta is below deltaMin. That's too small, so reset delta to deltaMin" << endl;
	      delta = searchflags.deltaMin;
	      *os << "delta new == " << delta << endl;
	      return gx;
	    }
	  }
	  else if (improvement == Ok || hookstep_equals_newtonstep || (delta >= searchflags.deltaMax)) {
	    *os << "Take current step and leave delta unchanged, for the following reasons:" << endl;
	    *os << "improvement            : " << improvement << endl;
	    *os << "|newtonstep| <= delta  : " << (hookstep_equals_newtonstep ? "true" : "false") << endl;
	    *os << "delta >= deltaMax      : " << (delta >= searchflags.deltaMax    ? "true" : "false") << endl;
	    //*os << "delta has decreased    : " << (delta_has_decreased ? "true" : "false") << endl;
	    *os << "delta == " << delta << endl;
	  }
	  else {  // improvement == Good, Accurate, or NegaCurve and no restriction on increasing apply
	    *os << "Take step and increase delta, if there's room." << endl;
	    lambda = adjustLambda(lambda, 1.0, searchflags.lambdaMax);
	    delta  = adjustDelta(delta, lambda, searchflags.deltaMin, searchflags.deltaMax, *os);
	  }
	}
      }
      //*os << "Recompute hookstep ? " << (recompute_hookstep ? "yes" : "no") << endl;

      if (recompute_hookstep)
	continue;
      else
	break;
    }  // matches while (true) for finding good trust region and optimal hookstep within it

    *os << "Taking best step and continuing Newton iteration." <<endl;
    u += du;
    if (Tsearch)
      T += dT;
    else
      T = tH;
    sigma *= dsigma;
    dunorm = L2Norm(du);
    unorm  = L2Norm(u);

    Gx = GH;

    *os << "L2Norm(G(u)) == " << L2Norm(Gx) << endl;
    *os << "rx           == " << rx << endl;
    *os << "L2Norm(dxN)  == " << L2Norm(dxN) << endl;
    *os << "L2Norm(dxH)  == " << L2Norm(dxH) << endl;
    *os << "L2Norm(du)   == " << dunorm << endl;
    *os << "        T    == " <<  T << endl;
    *os << "       dT    == " << dT << endl;
    if (searchflags.dudxortho) {
      *os << "L2IP(du,edudx)/L2Norm(du) == " << L2IP(du,edudx)/dunorm << endl;
      *os << "L2IP(du,edudz)/L2Norm(du) == " << L2IP(du,edudz)/dunorm << endl;
    }
    if (searchflags.dudtortho) 
      *os << "L2IP(du,edudt)/L2Norm(du) == " << L2IP(du,edudt)/dunorm << endl;
    if (searchflags.poincsrch) 
      *os << "                     h(u) == " << (*hpoincare)(u) << endl;
    *os << "align(dxN, prev_dxN) == " << dxNalign << endl;
    *os << "align(dxH, prev_dxH) == " << align(dxH, prev_dxH) << endl;
    *os << "align(dxH, dxN)      == " << align(dxH, dxN) << endl;

    //prev_dxG = dxG;
    prev_dxH = dxH;
    prev_dxN = dxN;

    //du.save("duH" + i2s(newtonStep));
  }
  return gx;
}

// f^T(u) = time-T DNS integration of u
void f(const FlowField& u, Real T, FlowField& f_u, const DNSFlags& flags_, 
       const TimeStep& dtarg, int& fcount, Real& CFL, ostream& os) {

  DNSFlags flags(flags_);
  flags.logstream = &os;

  //os << "entering f..." << endl;

  if (T<0) {
    os << "f: negative integration time T == " << T << endl
		<< "returning f(u,T) == (1+abs(T))*u" << endl;
    f_u = u;
    f_u *= 1+abs(T);
    //os << "exiting f..." << endl;
    return;
  }
  if (!isfinite(L2Norm(u))) {
    os << "L2Norm(u) == " << L2Norm(u) << endl;
    os << "warning f: u is not finite. exiting." << endl;
    return;
  }

  // Special case #1: no time steps
  if (T == 0) {
    os << "f: T==0, no integration, returning u " << endl;
    f_u = u;
    //os << "exiting f..." << endl;
    return;
  }

  TimeStep dt(dtarg);
  //const Real nu = 1.0/Reynolds;
  FlowField p(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b());
  f_u = u;
  dt.adjust_for_T(T, false);
  flags.dt = dt;

  // Adjust dt for CFL if necessary
  DNS dns(f_u, flags);
  if (dt.variable()) {
    dns.advance(f_u, p, 1);
    dt.adjust(dns.CFL(), false);
    dns.reset_dt(dt);
    f_u = u;
  }


  //os << "\ndt == " << dt << endl;
  //os << ", CFL == " << dns.CFL() << endl;
  //os << "t == " << flush;

  //  t == current time in integration
  //  T == total integration time
  // dT == CFL-check interval
  // dt == DNS time-step
  //  N == T/dT,  n == dT/dt;
  //  T == N dT, dT == n dt
  //  t == s dT (s is loop index)
  //os << "f : 0" << flush;
  for (int s=1; s<=dt.N(); ++s) {
    dns.advance(f_u, p, dt.n());

    Real t = s*dt.dT();
    CFL = dns.CFL();

    if (s % 10 == 0)
      os << iround(t) << flush;
    else if (s % 2 == 0) {
      if (CFL < dt.CFLmin())
	os << '<' << flush;
      else if (CFL > dt.CFLmax())
	os << '>' << flush;
      else
	os << '.' << flush;
    }
    if (dt.adjust(CFL, false))
      dns.reset_dt(dt);
  }
  ++fcount;
  //os << "finished integration..." << endl;

  if (!isfinite(L2Norm(f_u))) {
    os << "L2Norm(f^t(u)) == " << L2Norm(f_u) << endl;
    os << "error in f: f^t(u) is not finite. exiting." << endl;
    exit(1);
  }
  //os << "exiting f..." << endl;

  return;
}

// return f^{N dt}(u) = time-(N dt) DNS integration of u
void f(const FlowField& u, int N, Real dt, FlowField& f_u, const DNSFlags& flags_, ostream& os) {

  os << "f(u, N, dt, f_u, flags, dt) : " << flush;
  DNSFlags flags(flags_);
  flags.logstream = &os;
  //const Real nu = 1.0/Reynolds;
  FlowField p;
  f_u = u;
  DNS dns(f_u, flags);
  dns.advance(f_u, p, N);
  return;
}

// return sigma f^{N dt}(u) 
void sf(const FlowField& u, Real T, const FieldSymmetry& sigma, FlowField& sfu, 
	DNSFlags& flags, TimeStep& dt, int& fcount, Real& CFL, ostream& os) {

  f(u, T, sfu, flags, dt, fcount, CFL, os);
  sfu *= sigma;
}


// Dsf(u,sigma,T,du) = sigma f^T(u+du) - sigma f^T(u)
void Dsf(const FlowField& u, Real T, const FieldSymmetry& sigma, 
	 const FlowField& sfu, const FlowField& du, FlowField& Dsf_du,
	 DNSFlags& flags, TimeStep& dt, Real eps, bool centerdiff, 
	 int& fcount, Real& CFL, ostream& os) {


 FlowField u_du(du);
 FlowField sfu_du(du);

  if (!centerdiff) {

    os << "Computing f^T(u + du) for approximation L du == f^T(u + du) - f^T(u)" << endl;
    u_du  = du;
    u_du *= eps;
    u_du += u;

    sfu_du = u_du;
    sf(u_du, T, sigma, sfu_du, flags, dt, fcount, CFL, os);
    //fu_du *= sigma;

    Dsf_du = sfu_du;
    Dsf_du -= sfu;

    os << "L2Norm(u_du)             == " << L2Norm(u_du) << endl;
    os << "L2Norm(du)               == " << L2Norm(du) << endl;
    os << "L2Norm(sf(u))            == " << L2Norm(sfu) << endl;
    os << "L2Norm(sf(u + du))       == " << L2Norm(sfu_du) << endl;
    os << "L2Norm(sf(u + du)-sf(u)) == " << L2Norm(Dsf_du) << endl;

    Dsf_du *= 1.0/eps;
  }
  else {

    os << "Computing sigma f^T(u +/- du/2) for approximation L du == sigma f^T(u + du/2) - sigma f^T(u - du/2)" << endl;

    // Compute sgima f^T(u+du)
    u_du  = du;
    u_du *= eps/2;
    u_du += u;

    sfu_du = u_du;
    sf(u_du, T, sigma, sfu_du, flags, dt, fcount, CFL, os);
    //sfu_du *= sigma;
    Dsf_du = sfu_du;

    // Compute f^T(u-du)
    u_du  = du;
    u_du *= -eps/2;
    u_du += u;

    sfu_du = u_du;
    sf(u_du, T, sigma, sfu_du, flags, dt, fcount, CFL, os);
    //sfu_du *= sigma;
    Dsf_du -= sfu_du;

    // Complete approximation L du == sigma f^T(u + du/2) - sigma f^T(u - du/2)
    Dsf_du *= 1.0/eps;
  }
}


// G(x) = G(u,sigma,T) = (sigma f^T(u) - u)/normalizer
// normalizer = (Tnormalize ? : T 1) * (Unormalize ? L2Norm(u) : 1)
//            
void G(const FlowField& u, Real T, const FieldSymmetry& sigma, FlowField& Gx,
       const DNSFlags& flags, const TimeStep& dt, bool Tnormalize, Real Unormalize, int& fcount, Real& CFL,
       ostream& os) {
  //os << "entering G..." << endl;
  f(u, T, Gx, flags, dt, fcount, CFL, os);
  Real funorm = L2Norm3d(u);
  //cout << "L2Norm(u)  == " << L2Norm(u) << endl;
  //cout << "L2Norm(Gu) == " << L2Norm(Gx) << endl;
  //cout << "T == " << T << endl;
  Gx *= sigma;
  Gx -= u;

  if (Tnormalize)
    Gx *= 1.0/T;
  if (Unormalize != 0.0)
    //Gx *= 1/funorm; 
    //Gx *= 1.0/tanh(funorm/Unormalize); 
    //Gx *= 1/(funorm-0.025); 
    Gx *= 1/(funorm-Unormalize); 
  //cout << "1.0/sqrt(abs(funorm*(1-funorm))) == " << 1.0/sqrt(abs(funorm*(1-funorm))) << endl; 
  //os << "exiting G..." << endl;
  return;
}

void DG(const FlowField& u, const FlowField& du, Real T, Real dT,
	const FieldSymmetry& sigma,  const FieldSymmetry& dsigmaArg,
	const FlowField& Gx, FlowField& DG_dx,
	const DNSFlags& flags, const TimeStep& dt, bool Tnormalize, Real Unormalize,
	Real epsDu, bool centdiff, int& fcount, Real& CFL, ostream& os) {

  FlowField u_du;
  FieldSymmetry dsigma;
  FieldSymmetry sigma_dsigma;
  Real T_dT;

  Real step_magn = sqrt(L2Norm2(du) + square(dT/T) + square(dsigma.ax()) + square(dsigma.az()));
  Real eps = step_magn < epsDu ? 1 : epsDu/step_magn;

  if (centdiff) {
    Real eps2 = 0.5*eps;
    u_du  = du;
    u_du *= eps2;
    u_du += u;
    T_dT = T + eps2*dT;
    dsigma = dsigmaArg;
    dsigma *= eps2;
    sigma_dsigma = dsigma*sigma;

    G(u_du, T_dT, sigma_dsigma, DG_dx, flags, dt, Tnormalize, Unormalize, fcount, CFL, os);
    u_du  = du;
    u_du *= -eps2;
    u_du += u;
    T_dT = T - eps2*dT;
    dsigma = dsigmaArg;
    dsigma *= -eps2;
    sigma_dsigma = dsigma*sigma;
    FlowField tmp;
    G(u_du, T_dT, sigma_dsigma, tmp, flags, dt, Tnormalize, Unormalize, fcount, CFL, os);

    DG_dx -= tmp;
    DG_dx *= 1.0/eps;
  }
  else {
    u_du  = du;
    u_du *= eps;
    u_du += u;
    T_dT = T + eps*dT;
    dsigma = dsigmaArg;
    dsigma *= eps;
    sigma_dsigma = dsigma*sigma;

    G(u_du, T_dT, sigma_dsigma, DG_dx, flags, dt, Tnormalize, Unormalize, fcount, CFL,os);
    DG_dx -= Gx;
    DG_dx *= 1.0/eps;
  }
  return;
  //if (Tnormalize)
  //DG_dx *= 1.0/T;
}


// f^T(u) = if h==0, time-T DNS integration of u
//        = if h!=0, integration to poincare section h(u)=0, choose crossing with t closest to T

void fp(const FlowField& u, Real& T, PoincareCondition* h,
	FlowField& f_u, const DNSFlags& flags_, const TimeStep& dt_, int& fcount,
	Real& CFL, ostream& os) {

  if (!isfinite(L2Norm(u))) {
    os << "error in f: u is not finite. exiting." << endl;
    exit(1);
  }

  DNSFlags flags(flags_);
  //cout <<"fp flags == " << flags << endl;
  flags.logstream = &os;
  TimeStep dt(dt_);
  //const Real nu = 1.0/Reynolds0;
  FlowField p(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b());
  f_u = u;

  // No Poincare section, just integration to time T
  if (h==0) {

    //os << "entering f..." << endl;

    if (T<0) {
      os << "f: negative integration time T == " << T << endl
		  << "returning f(u,T) == (1+abs(T))*u" << endl;
      f_u = u;
      f_u *= 1+abs(T);
      //os << "exiting f..." << endl;
      return;
    }

    // Special case #1: no time steps
    if (T == 0) {
      os << "f: T==0, no integration, returning u " << endl;
      f_u = u;
      //os << "exiting f..." << endl;
      return;
    }

    dt.adjust_for_T(T, false);
    flags.dt = dt;

    // Adjust dt for CFL if necessary
    DNS dns(f_u, flags);
    if (dt.variable()) {
      dns.advance(f_u, p, 1);
      dt.adjust(dns.CFL(), false);
      dns.reset_dt(dt);
      f_u = u;
    }

    //os << "\ndt == " << dt << endl;
    //os << ", CFL == " << dns.CFL() << endl;
    //os << "t == " << flush;

    //  t == current time in integration
    //  T == total integration time
    // dT == CFL-check interval
    // dt == DNS time-stepn
    //  N == T/dT,  n == dT/dt;
    //  T == N dT, dT == n dt
    //  t == s dT (s is loop index)
    //os << "f : 0" << flush;
    os << "f^T: " << flush;
    for (int s=1; s<=dt.N(); ++s) {

      Real t = s*dt.dT();
      CFL = dns.CFL();

      if (s % 10 == 0)
	os << iround(t) << flush;
      else if (s % 2 == 0) {
	if (CFL < dt.CFLmin())
	  os << '<' << flush;
	else if (CFL > dt.CFLmax())
	  os << '>' << flush;
	else
	  os << '.' << flush;
      }

      dns.advance(f_u, p, dt.n());

      if (dt.variable() && dt.adjust(CFL, false))
	dns.reset_dt(dt);
    }
    //os << "finished integration..." << endl;

    if (!isfinite(L2Norm(f_u))) {
      os << "L2Norm(f^t(u)) == " << L2Norm(f_u) << endl;
      os << "error in f: f^t(u) is not finite. exiting." << endl;
      exit(1);
    }
    //os << "exiting f..." << endl;

  }

  // Poincare section computation: return Poincare crossing nearest to t=T, with Tmin < t < Tmax.
  else {


    // Adjust dt for CFL if necessary
    DNSPoincare dns(f_u, h, flags);
    if (dt.variable()) {
      dns.advance(f_u, p, 1);
      dt.adjust(dns.CFL());
      dns.reset_dt(dt);
      f_u = u;
    }


    // Collect all Poincare crossings between Tfudgemin and Tfudgemax
    // If we don't find one in that range, go as far as Tlastchance
    Real dTfudge = 1.05;
    Real Tfudgemin = lesser( 0.90*T, T - dTfudge*dt.dT());
    Real Tfudgemax = Greater(1.02*T, T + dTfudge*dt.dT());
    Real Tlastchance = 10*T;

    vector<FlowField> ucross;
    vector<Real> tcross;
    vector<int>  scross;
    int s=0;
    int crosssign = 0; // look for crossings in either direction

    os << "f^t: " << flush;
    for (Real t=0; t<=Tlastchance; t += dt.dT(), ++s) {

      CFL = dns.CFL();

      if (s % 10 == 0)   os << iround(t);
      else if (s % 2 == 0) {
	if      (CFL > dt.CFLmax())  os << '>';
	else if (CFL < dt.CFLmin())  os << '<';
	else  os << '.';
	os << flush;
      }

      // Collect any Poincare crossings 
      bool crossed = dns.advanceToSection(f_u, p, dt.n(), crosssign, Tfudgemin);
      if (crossed && t >= Tfudgemin) {
	ucross.push_back(dns.ucrossing());
	tcross.push_back(dns.tcrossing());
	scross.push_back(dns.scrossing());
      }
      
      // If we've found at least one crossing within the fudge range, stop. 
      // Otherwise continue trying until Tlastchance
      if (ucross.size() > 0 && t >= Tfudgemax) 
	break;
    }
    //os << endl;

    if (ucross.size()<1) {
      os << "\nError in f(u, T, f_u, flags, dt, fcount, CFL, os) :\n";
      os << "the integration did not reach the Poincare section.\n";
      os << "Returning laminar solution and a b.s. value for the crossing time.\n";
      os << "I hope you can do something useful with them." << endl;
      f_u.setToZero();
      T = dns.time();
      ++fcount;
      return;
    }
    os << "  " << flush;

    // Now select the crossing that is closest to the estimated crossing time
    FlowField ubest = ucross[0];
    Real  Tbest = tcross[0];
    int   sbest = scross[0];
    int   nbest = 0;

    for (uint n=1; n<ucross.size(); ++n)
      if (abs(tcross[n]-T) < abs(Tbest-T)) {
	ubest = ucross[n];
	Tbest = tcross[n];
	sbest = scross[n];
	nbest = n;
      }
    os << nbest << (sbest==1 ? '+' : '-') << " at t== " << Tbest << flush;

    T = Tbest;
    f_u = ubest;

    // Now check if there are any crossings of opposite sign close by.
    // This signals near-tangency to Poincare section, which'll mess up
    // the search. Just print warning and let user intervene manually.
    for (uint n=0; n<ucross.size(); ++n)
      if (scross[n] != sbest) {
	os << "\nWARNING : There is a nearby Poincare crossing of opposite sign," << endl;
	os << "signalling near-tangency to section. You should probably switch " << endl;
	os << "to another Poincare crossing." << endl;
	os << "(ubest, unear) signs == " << sbest << ", " << scross[n] << endl;
	os << "(ubest, unear) times == " << Tbest << ", " << tcross[n] << endl;
	os << "(ubest, unear) dist  == " << L2Dist(ubest, ucross[n]) << endl;
      }
  }


  if (!isfinite(L2Norm(f_u))) {
    os << "error in f: f(u,t) is not finite. exiting." << endl;
    exit(1);
  }
  ++fcount;
  return;

}

void sfp(const FlowField& u, Real T, PoincareCondition* h, 
	 const FieldSymmetry& sigma, FlowField& sfu, const DNSFlags& flags, 
	 const TimeStep& dt, int& fcount, Real& CFL, ostream& os) {

  fp(u, T, h, sfu, flags, dt, fcount, CFL, os);
  sfu *= sigma;
}


// Dsf(u,sigma,T,du) = sigma f^T(u+du) - sigma f^T(u)
void Dsfp(const FlowField& u, Real T, PoincareCondition* h, 
	  const FieldSymmetry& sigma, const FlowField& sfu, const FlowField& du, 
	  FlowField& Dsf_du, const DNSFlags& flags, const TimeStep& dt, Real eps, 
	  bool centerdiff,  int& fcount, Real& CFL, ostream& os) {

 FlowField u_du(du);
 FlowField sfu_du(du);

  if (!centerdiff) {

    os << "Computing f^T(u + du) for approximation L du == f^T(u + du) - f^T(u)" << endl;
    u_du  = du;
    u_du *= eps;
    u_du += u;

    sfu_du = u_du;
    sfp(u_du, T, h, sigma, sfu_du, flags, dt, fcount, CFL, os);
    //fu_du *= sigma;

    Dsf_du = sfu_du;
    Dsf_du -= sfu;

    os << "L2Norm(u_du)             == " << L2Norm(u_du) << endl;
    os << "L2Norm(du)               == " << L2Norm(du) << endl;
    os << "L2Norm(sf(u))            == " << L2Norm(sfu) << endl;
    os << "L2Norm(sf(u + du))       == " << L2Norm(sfu_du) << endl;
    os << "L2Norm(sf(u + du)-sf(u)) == " << L2Norm(Dsf_du) << endl;

    Dsf_du *= 1.0/eps;
  }
  else {

    os << "Computing sigma f^T(u +/- du/2) for approximation L du == sigma f^T(u + du/2) - sigma f^T(u - du/2)" << endl;

    // Compute sgima f^T(u+du)
    u_du  = du;
    u_du *= eps/2;
    u_du += u;

    sfu_du = u_du;
    sfp(u_du, T, h, sigma, sfu_du, flags, dt, fcount, CFL, os);
    //sfu_du *= sigma;
    Dsf_du = sfu_du;

    // Compute f^T(u-du)
    u_du  = du;
    u_du *= -eps/2;
    u_du += u;

    sfu_du = u_du;
    sfp(u_du, T, h, sigma, sfu_du, flags, dt, fcount, CFL, os);
    //sfu_du *= sigma;
    Dsf_du -= sfu_du;

    // Complete approximation L du == sigma f^T(u + du/2) - sigma f^T(u - du/2)
    Dsf_du *= 1.0/eps;
  }
}


// G(x) = G(u,sigma) = (sigma f^T(u) - u) for orbits
void Gp(const FlowField& u, Real& T, PoincareCondition* h,
	const FieldSymmetry& sigma, FlowField& Gu, const DNSFlags& flags,
	const TimeStep& dt, bool Tnormalize, Real Unormalize, int& fcount, Real& CFL, ostream& os) {

  fp(u, T, h, Gu, flags, dt, fcount, CFL, os);
  Real funorm= L2Norm3d(Gu);
  //cout << "L2Norm(u)  == " << L2Norm(u) << endl;
  //cout << "L2Norm(Gu) == " << L2Norm(Gu) << endl;
  //cout << "T == " << T << endl;  
  Gu *= sigma;
  Gu -= u;
  //cout << "L2Norm(u-Gu) == " << L2Norm(Gu) << endl;
  if (Tnormalize)
    Gu *= 1.0/T;
  if (Unormalize)
    //Gu *= 1.0/tanh(funorm/Unormalize); 
    //Gu *= 1/funorm;
    //Gu *= 1.0/sqrt(abs(funorm*(0.035-funorm)));
    //Gu *= 1.0/(funorm-0.025);
    Gu *= 1.0/(funorm-Unormalize);

  //cout << "1.0/sqrt(abs(funorm*(1-funorm))) == " << 1.0/sqrt(abs(funorm*(1-funorm))) << endl; 
  //cout << "L2Norm(rtn) == " << L2Norm(Gu) << endl;
}


void DGp(const FlowField& u, const FlowField& du, Real& T, Real dT, 
	 PoincareCondition* h, const FieldSymmetry& sigma, const FieldSymmetry& dsigmaArg,
	 const FlowField& Gx, FlowField& DG_dx, const DNSFlags& flags, const TimeStep& dt,
	 bool Tnormalize, Real Unormalize, Real epsDu, bool centdiff, int& fcount, Real& CFL,
	 ostream& os) {

  FlowField u_du;
  FieldSymmetry dsigma;
  FieldSymmetry sigma_dsigma;
  Real T_dT;

  Real step_magn = sqrt(L2Norm2(du) + square(dsigma.ax()) + square(dsigma.az()));
  Real eps = step_magn < epsDu ? 1 : epsDu/step_magn;

  if (centdiff) {
    Real eps2 = 0.5*eps;
    u_du  = du;
    u_du *= eps2;
    u_du += u;
    T_dT = T + eps2*dT;
    dsigma = dsigmaArg;
    dsigma *= eps2;
    sigma_dsigma = dsigma*sigma;

    Gp(u_du, T_dT, h, sigma_dsigma, DG_dx, flags, dt, Tnormalize, Unormalize, fcount, CFL, os);
    u_du  = du;
    u_du *= -eps2;
    u_du += u;
    T_dT = T - eps2*dT;
    dsigma = dsigmaArg;
    dsigma *= -eps2;
    sigma_dsigma = dsigma*sigma;
    FlowField tmp;
    Gp(u_du, T_dT, h, sigma_dsigma, tmp, flags, dt, Tnormalize, Unormalize, fcount, CFL, os);

    DG_dx -= tmp;
    DG_dx *= 1.0/eps;
  }
  else {
    u_du  = du;
    u_du *= eps;
    u_du += u;
    T_dT = T + eps*dT;
    dsigma = dsigmaArg;
    dsigma *= eps;
    sigma_dsigma = dsigma*sigma;

    Gp(u_du, T_dT, h, sigma_dsigma, DG_dx, flags, dt, Tnormalize, Unormalize, fcount, CFL, os);
    DG_dx -= Gx;
    DG_dx *= 1.0/eps;
  }
}

GMRESHookstepFlags::GMRESHookstepFlags(SolutionType solntype_,
				       bool xrelative_,
				       bool zrelative_,
				       Real epsSearch_,
				       Real epsKrylov_,
				       Real epsDu_,
				       Real epsDt_,
				       Real epsGMRES_,
				       Real epsGMRESf_,
				       bool centdiff_,
				       int  Nnewton_,
				       int  Ngmres_,
				       int  Nhook_,
				       Real delta_,
				       Real deltaMin_,
				       Real deltaMax_,
				       Real deltaFuzz_,
				       Real lambdaMin_,
				       Real lambdaMax_,
				       Real lambdaRequiredReduction_,
				       Real improvReq_,
				       Real improvOk_,
				       Real improvGood_,
				       Real improvAcc_,
				       bool dudtortho_,
				       bool dudxortho_,
				       bool poincsrch_,
				       bool poinconst_,
				       Real orthoscale_,
				       Real TscaleRel_,
				       Real Rscale_,
				       bool unormalize_,
				       bool autostop_,
				       Real stopfactor_,
				       bool saveall_,
				       string outdir_,
				       ostream* logstream_)
  :
  solntype(solntype_),
  xrelative(xrelative_),
  zrelative(zrelative_),
  epsSearch(epsSearch_),
  epsKrylov(epsKrylov_),
  epsDu(epsDu_),
  epsDt(epsDt_),
  epsGMRES(epsGMRES_),
  epsGMRESf(epsGMRESf_),
  centdiff(centdiff_),
  Nnewton(Nnewton_),
  Ngmres(Ngmres_),
  Nhook(Nhook_),
  delta(delta_),
  deltaMin(deltaMin_),
  deltaMax(deltaMax_),
  deltaFuzz(deltaFuzz_),
  lambdaMin(lambdaMin_),
  lambdaMax(lambdaMax_),
  lambdaRequiredReduction(lambdaRequiredReduction_),
  improvReq(improvReq_),
  improvOk(improvOk_),
  improvGood(improvGood_),
  improvAcc(improvAcc_),
  dudtortho(dudtortho_),
  dudxortho(dudxortho_),
  poincsrch(poincsrch_),
  poinconst(poinconst_),
  orthoscale(orthoscale_),
  TscaleRel(TscaleRel_),
  Rscale(Rscale_),
  unormalize(unormalize_),
  autostop(autostop_),
  stopfactor(stopfactor_),
  saveall(saveall_),
  outdir(outdir_),
  logstream(logstream_)
{}

std::ostream& operator<<(std::ostream& os, const GMRESHookstepFlags& f) {
  os << "{\n";
  os << " solntype == " << f.solntype << "\n";
  os << " xrelative  == " << f.xrelative  << "\n";
  os << " zrelative  == " << f.zrelative  << "\n";
  os << " epsSearch  == " << f.epsSearch  << "\n";
  os << " epsKrylov  == " << f.epsKrylov  << "\n";
  os << " epsDu      == " << f.epsDu      << "\n";
  os << " epsDt      == " << f.epsDt      << "\n";
  os << " epsGMRES   == " << f.epsGMRES   << "\n";
  os << " epsGMRESf  == " << f.epsGMRESf  << "\n";
  os << " centdiff   == " << f.centdiff   << "\n";
  os << " Nnewton    == " << f.Nnewton    << "\n";
  os << " Ngmres     == " << f.Ngmres     << "\n";
  os << " Nhook      == " << f.Nhook      << "\n";
  os << " delta      == " << f.delta      << "\n";
  os << " deltaMin   == " << f.deltaMin   << "\n";
  os << " deltaMax   == " << f.deltaMax   << "\n";
  os << " deltaFuzz  == " << f.deltaFuzz  << "\n";
  os << " lambdaMin  == " << f.lambdaMin  << "\n";
  os << " lambdaMax  == " << f.lambdaMax  << "\n";
  os << " lambdaRequiredReduction == " << f.lambdaRequiredReduction << "\n";
  os << " improvReq   == " << f.improvReq   << "\n";
  os << " improvOk    == " << f.improvOk    << "\n";
  os << " improvGood  == " << f.improvGood  << "\n";
  os << " improvAcc   == " << f.improvAcc   << "\n";
  os << " dudtortho   == " << f.dudtortho   << "\n";
  os << " udxortho    == " << f.dudxortho   << "\n";
  os << " oincsrch    == " << f.poincsrch   << "\n";
  os << " oinconst    == " << f.poinconst   << "\n";
  os << " rthoscale   == " << f.orthoscale  << "\n";
  os << " TscaleRel   == " << f.TscaleRel   << "\n";
  os << " Rscale      == " << f.Rscale      << "\n";
  os << " unormalize  == " << f.unormalize  << "\n";
  os << " autostop    == " << f.autostop    << "\n";
  os << " stopfactor  == " << f.stopfactor << "\n";
  os << " saveall     == " << f.saveall     << "\n";
  os << "}\n";
  return os;
}

/***********
bool GMRESHookstepFlags::relative() const {
  return (solntype == TravelingWave || solntype == RelativePeriodicOrbit);
}
bool GMRESHookstepFlags::orbit() const {
  return (solntype == PeriodicOrbit || solntype == RelativePeriodicOrbit);
}
***********/

ostream& operator<<(ostream& os, SolutionType solntype) {
  os << (solntype == Equilibrium ? "Equilibrium" : "PeriodicOrbit");
  return os;
}

// The following is meant as a guide to the structure and notation
// of the above code. See Divakar Viswanath, "Recurrent motions
// within plane Couette turbulence", J. Fluid Mech. 580 (2007),
// http://arxiv.org/abs/physics/0604062 for a higher-level presentation
// of the algorithm.
//
// I've biased the notation towards conventions from fluids and from
// Predrag Cvitanovic's ChaosBook, and away from Dennis & Schnabel and
// Trefethen. That is, f stands for the finite-time map of plane Couette
// dynamics, not the residual being minimized.
//
// Note: in code, dt stands for the integration timestep and
//                dT stands for Newton-step increment of period T
// But for beauty's sake I'm using t and dt for the period and
// its Newton-step increment in these comments.
//
// Let
//     f^t(u) be the time-t map of Navier-Stokes computed by DNS.
//     sigma    be a symmetry of the flow
//
// We seek solutions of the equation
//
//    G(u,sigma,t) = sigma f^t(u) - u = 0                       (1)
//
// t can be a fixed or a free variable, as can sigma. For plane
// Couette the potential variables in sigma are ax,ax in the
// phase shifts x -> x + ax Lx and z -> z + az Lz.
//
// Let x be the vector of unknown real numbers being sought.
// For simplicity's sake in this exposition I will assume that
// we're looking for a relative periodic orbit, so that the
// period t of the orbit and the phase shifts ax and az are
// unknowns. Then x = (u, sigma, t). Of course by this I really
// that x is the finite set of real-valued variables that
// parameterize the discrete representation of u (e.g. the
// real and imaginary parts of the spectral coefficients),
// together with the real-valued variables that parameterize
// sigma, and the real number t. To be perfectly pedantic (and
// because it will come in handy later), let there be M
// independent real numbers {u_m} in the discrete representation
// of u, and let
//
//    {u_m} = P(u),  u_m = P_m(u)                           (2)
//
// represent the map between continuous fields u and the discrete
// representation {u_m}. Then x = (u, sigma, t) is an M+3 dimensional
// vector
//
//    x_m = (u_1, u_2, ..., u_M, ax, az, t)
//
// and the discrete equation to be solved is
//
//   G(x) = 0                                               (3)
//
// where G is a vector-valued function of dimension M.
//
//   G_m(x)   = P_m(sigma f^t(u) - u)                         (4)
//
// At this point have three fewer equations in (4) than unknowns
// in x because the equivariance of the flow means that solutions
// are indeterminate under time and phase shifts.
//
// The Newton algorithm for solving G(x) = 0 is
//
// Let x* = (u*,sigma*,t*) be the solution, i.e. G(x*) = G(u*,sigma*, t*) = 0.
// Suppose we start with an initial guess x near x*.
// Let
//     x* = x + dxN       (dxN is the Newton step)           (5)
// 
// componentwise 
//     u* = u + duN
// sigma* = sigma + dsigmaN
//     t* = t + dtN
//
// Then G(x*) = G(x + dxN)
//          0 = G(x) + DG(x) dxN + O(|dxN|^2)
//
// Drop h.o.t and solve the Newton equation
//
//    DG(x) dxN = -G(x)                                     (6)
//
// Because DG is a M x (M+3) matrix, we have to supplement this
// system with three constraint equations to have a unique solution
// dxN.
//
//   (duN, du/dt) = 0                                       (7)
//   (duN, du/dx) = 0
//   (duN, du/dz) = 0
//
// ( , ) here signifies an inner product, so these are orthogonality
// constraints preventing the Newton step from going in the directions
// of the time, x, and z equivariance. That forms an (M+3) x (M+3)
// dimensional system
//
//    A dxN = b                                             (8)
//
//  where the first M rows are given by (6) and the last three by (7).
//
//
// Since since M is very large, we will use the iterative GMRES algorithm
// to find an approximate solution dxN to the (M+3)x(M+3) system A dxN = b.
// GMRES requires multiple calculations A dx for test values of dx. The
// left-hand side of (6) can be approximated with finite differencing:
//
//   DG(x) dx = 1/e (G(x + e dx) - G(x)) where e |dx| << 1.
//            = 1/e P[(sigma+dsigma) f^{t+dt}(u+du) - (u+du) - (sigma f^{t}(u) - u)]
//            = 1/e P[(sigma+dsigma) f^{t+dt}(u+du) - sigma f^{t}(u) - du]
//
// And the left-hand sides of (7) are a simple evaluation of an inner
// product. For details of the GMRES algorithm, see Trefethen and Bau.

// That gives us the Newton step dxN. However if we are too far from
// the solution x*, the linearization inherent in the Newton algorithm
// will not be accurate. At this point we switch from the pure Newton
// algorithm to a constrained minimization algorithm called the
// "hookstep", specially adapted to work with GMRES.

// Hookstep algorithm: If the Newton step dxN does not decrease
// the residual || G(x+dxN) || sufficiently, we obtain a smaller step
// dxH by minimizing || A dxH - b ||^2 subject to ||dxH||^2 <= delta^2,
// rather using the Newton step, which solves A dx = b.

// GMRES-Hookstep algorithm: Since A is very high-dimensional, we
// further constrain the hookstep dxH to lie within the Krylov subspace
// obtained from the solution of the Newton step equations. I.e.
// we minimize the norm of the projection of A dxH - b onto the Krylov
// subspace. The nth GMRES iterate gives matrices Qn, Qn1, and Hn such
// that
//
//   A Qn = Qn1 Hn                                              (9)
//
// where Qn is M x n, Qn1 is M x (n+1), and Hn is (n+1) x n.
// Projection of A dx - b on (n+1)th Krylov subspace is Qn1^T (A dx - b)
// Confine dx to nth Krylov subspace: dxH = Qn sn, where sn is n x 1.
// Then minimize
//
//   || Qn1^T (A dxH - b) || = || Qn1^T A Qn sn   - Qn1^T b ||
//                           = || Qn1^T Qn1 Hn sn - Qn1^T b ||
//                           = || Hn sn - Qn1^T b ||            (10)
//
// subject to ||dxH||^2 = || sn ||^2 <= delta^2. Note that the quantity in
// the norm on the right-hand side of (10) is only n-dimensional, and n is
// small (order 10 or 100) compared to M (10^5 or 10^6).
//
// Do minimization of the RHS of (10) via SVD of Hn = U D V^T.
//   || Hn sn - Qn1^T b || = || U D V^T sn - Qn1^T b ||
//                         = || D V^T sn - U^T Qn1^T b ||
//                         = || D sh - bh ||
//
// where sh = V^T sn and bh = U^T Qn1^T b. Now we need to
// minimize || D sh - bh ||^2 subject to ||sh||^2 <= delta^2
//
// From Divakar Viswanath (personal communication):
//
// Since D is diagonal D_{i,i} = d_i, the constrained minimization problem
// can be solved easily with Lagrange multipliers. The solution is
//
//   sh_i = (bh_i d_i)/(d^2_i + mu)
//
// for the mu such that ||sh||^2 = delta^2. The value of mu can be
// found with a 1d Newton search for the zero of
//
//   Phi(mu) = ||sh(mu)||^2 - delta^2
//
// A straight Newton search a la mu -= Phi(mu)/Phi'(mu) is suboptimal
// since Phi'(mu) -> 0 as mu -> infty with Phi''(mu) > 0 everywhere, so
// we use a slightly modified update rule for the Newton search over mu.
// Please refer to Dennis and Schnabel regarding that. Then, given the
// solution sh(mu), we compute the hookstep solution s from
//
//   dxH = Qn sn = Qn V sh
//
//
// How do we know a good value for delta? Essentially, by comparing
// the reduction in residual obtained by actually taking the hookstep
// dxH to the reduction predicted by the linearized model  G(x+dxH) =
// G(x) + DG(x) dxH. If the reduction is accurate but small, we increase
// delta by small steps until the reduction is marginally accurate but larger.
// If the reduction is poor we reduce delta.
//
// (Note: the trust-region optimization is actually performed in terms of the
// squared residual, so the comments in the code refer to a quadratic rather than
// linear model of the residual.)
//
// The heuristics associated with adjusting the trust region are fairly complex.
// Dennis and Schnabel has a decent description, but it took quite a bit of effort
// to translate that description into an algorithm. Rather than attempt to
// translate the algorithm back into prose, I recommend you read Dennis and
// Schnabel and then refer to the code and comments. This portion of findorbit.cpp
// is very liberally commented.



// References:
//
// Divakar Viswanath, "Recurrent motions within plane Couette turbulence",
// J. Fluid Mech. 580 (2007), http://arxiv.org/abs/physics/0604062
//
// Lloyd N. Trefethen and David Bau, "Numerical Linear Algebra", SIAM,
// Philadelphia, 1997.
//
// Dennis and Schnabel, "Numerical Methods for Unconstrained Optimization
// and Nonlinear Equations", Prentice-Hall Series in Computational Mathematics,
// Englewood Cliffs, New Jersey, 1983.
//

//#endif // HAVE_OCTAVE
// End of Octave-dependent code
// ======================================================================


} // namespace channelflow
