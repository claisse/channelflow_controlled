/* flowfield.cpp: Class for N-dim Fourier x Chebyshev x Fourier expansions
 * channelflow-1.3, www.channelflow.org
 *
 * Copyright (C) 2001-2009  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu
 * Center for Nonlinear Sciences, School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */

#include <fstream>
#include <iomanip>
#include <string.h> // for strdupa
#include <stddef.h> // for strtok
#include <stdlib.h>

#include "config.h"
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"
#include "channelflow/utilfuncs.h"

#ifdef HDF5_FOUND  // set by cmake in config.h
#include "H5Cpp.h"
#define HAVE_HDF5 1
#else
#define HAVE_HDF5 0
#endif

using namespace std;

namespace channelflow {

const Real EPS=1e-12;

#ifdef HAVE_HDF5
void hdf5write(int  i, const string& name, H5::H5File& h5file);
void hdf5write(Real x, const string& name, H5::H5File& h5file);
void hdf5write(const Vector& v, const string& name, H5::H5File& h5file);
void hdf5write(const FlowField& u, const string& name, H5::H5File& h5file);

bool hdf5query(const string& name, H5::H5File& h5file); // does attribute exist?

void hdf5read(int&  i, const string& name, H5::H5File& h5file);
void hdf5read(Real& x, const string& name, H5::H5File& h5file);
//void hdf5read(Vector& v, const string& name, H5::H5File& h5file);
void hdf5read(FlowField& u, const string& name, H5::H5File& h5file);


#endif


FlowField::FlowField()
  :
  Nx_(0),
  Ny_(0),
  Nz_(0),
  Nzpad_(0),
  Nzpad2_(0),
  Nd_(0),
  Lx_(0),
  Lz_(0),
  a_(0),
  b_(0),
  nu_(0),
  padded_(false),
  rdata_(0),
  cdata_(0),
  scratch_(0),
  xzstate_(Spectral),
  ystate_(Spectral),
  xz_plan_(0),
  xz_iplan_(0),
  y_plan_(0)
{}

FlowField::FlowField(int Nx, int Ny, int Nz, int Nd, Real Lx, Real Lz,
		     Real a, Real b, fieldstate xzstate, fieldstate ystate,
		     uint fftw_flags)
  :
  Nx_(0),
  Ny_(0),
  Nz_(0),
  Nzpad_(0),
  Nzpad2_(0),
  Nd_(0),
  Lx_(0),
  Lz_(0),
  a_(0),
  b_(0),
  nu_(0),
  padded_(false),
  rdata_(0),
  cdata_(0),
  scratch_(0),
  xzstate_(xzstate),
  ystate_(ystate),
  xz_plan_(0),
  xz_iplan_(0),
  y_plan_(0)
{
  resize(Nx, Ny, Nz, Nd, Lx, Lz, a, b, fftw_flags);
}

FlowField::FlowField(int Nx, int Ny, int Nz, int Nd, int tensorOrder, Real Lx,
		     Real Lz, Real a, Real b, fieldstate xzstate,
		     fieldstate ystate, uint fftw_flags)
  :
  Nx_(0),
  Ny_(0),
  Nz_(0),
  Nzpad_(0),
  Nzpad2_(0),
  Nd_(intpow(Nd,tensorOrder)),
  Lx_(0),
  Lz_(0),
  a_(0),
  b_(0),
  nu_(0),
  padded_(false),
  rdata_(0),
  cdata_(0),
  scratch_(0),
  xzstate_(xzstate),
  ystate_(ystate),
  xz_plan_(0),
  xz_iplan_(0),
  y_plan_(0)
{
  resize(Nx, Ny, Nz, Nd_, Lx, Lz, a, b, fftw_flags);
}

FlowField::FlowField(const FlowField& f)
  :
  Nx_(0),
  Ny_(0),
  Nz_(0),
  Nzpad_(0),
  Nzpad2_(0),
  Nd_(0),
  Lx_(0),
  Lz_(0),
  a_(0),
  b_(0),
  nu_(f.nu_),
  padded_(f.padded_),
  rdata_(0),
  cdata_(0),
  scratch_(0),
  xzstate_(Spectral),
  ystate_(Spectral),
  xz_plan_(0),
  xz_iplan_(0),
  y_plan_(0)
{
  resize(f.Nx_, f.Ny_, f.Nz_, f.Nd_, f.Lx_, f.Lz_, f.a_, f.b_);

  setState(f.xzstate_, f.ystate_);
  padded_ = f.padded_;
  int N = Nd_*Ny_*Nx_*Nzpad_;
  for (int i=0; i<N; ++i)
    rdata_[i] = f.rdata_[i];

}

FlowField::FlowField(const string& filebase)
  :
  Nx_(0),
  Ny_(0),
  Nz_(0),
  Nzpad_(0),
  Nzpad2_(0),
  Nd_(0),
  Lx_(0.0),
  Lz_(0.0),
  a_(0.0),
  b_(0.0),
  nu_(0),
  padded_(false),
  rdata_(0),
  cdata_(0),
  scratch_(0),
  xzstate_(Spectral),
  ystate_(Spectral),
  xz_plan_(0),
  xz_iplan_(0),
  y_plan_(0)
{

  // Cases, in order of precedence:
  //    -------conditions---------   
  //    filebase exists   HDF5libs   read
  // 1  foo.h5   foo.h5   yes        foo.h5
  // 2  foo      foo.h5   yes        foo.h5
  // 3  foo.ff   foo.ff   either     foo.ff
  // 4  foo      foo.ff   either     foo.ff
  // else fail

  string h5name = appendSuffix(filebase, ".h5");
  string ffname = appendSuffix(filebase, ".ff");
  string filename;

  if (HAVE_HDF5 && isReadable(h5name)) // cases 1 and 2
    filename = h5name;

  else if (isReadable(ffname))         // cases 3 and 4
    filename = ffname;

  else {
    cerr << "error in FlowField(string filebase) : can't open filebase==" << filebase << endl;
    if (HAVE_HDF5)
      cerr << "neither " << h5name << " nor " << ffname << " is readable" << endl;
    else
      cerr << ffname << ".ff is unreadable" << endl;
    exit(1);
  }

  // At this point, filename should have an .h5 or .ff extension, and that
  // file should be readable. Further, if it has .h5, the HDF5 libs are installed.

  if (hasSuffix(filename, ".h5")) {

#ifndef HAVE_HDF5
    cerr << "error in FlowField(string filebase). This line should be unreachable" << endl;
    exit(1);

#else
    using namespace H5;

    H5::H5File h5file(filename.c_str(), H5F_ACC_RDONLY);

    int Nx, Ny, Nz;
    hdf5read(Nx_, "Nxpad", h5file);
    hdf5read(Ny_, "Nypad", h5file);
    hdf5read(Nz_, "Nzpad", h5file);
    hdf5read(Nd_, "Nd", h5file);
    hdf5read(Nx,  "Nx", h5file);
    hdf5read(Ny,  "Ny", h5file);
    hdf5read(Nz,  "Nz", h5file);
    hdf5read(Lx_, "Lx", h5file);
    hdf5read(Lz_, "Lz", h5file);
    hdf5read(a_,  "a", h5file);
    hdf5read(b_,  "b", h5file);
    /********
    try {
      hdf5read(nu_, "nu", h5file);
    }
    catch (H5::AttributeIException) {
      nu_ = 0;
    }
    ***************/

    if (hdf5query("nu", h5file)) 
      hdf5read(nu_, "nu", h5file);
    else 
      nu_ = 0;
    if (Ny != Ny_)
      cferror("error in FlowField(string h5filename) : Ny != Nypad in h5 file");

    resize(Nx_, Ny_, Nz_, Nd_, Lx_, Lz_, a_, b_);

    if (Nx == Nx_ && Ny == Ny_ && Nz == Nz_) {
      hdf5read(*this, "data/u", h5file);
      this->setPadded(false);
    }

    else {
      FlowField v(Nx, Ny, Nz, Nd_, Lx_, Lz_, a_, b_, Physical, Physical);
      hdf5read(v, "data/u", h5file);
      v.setnu(nu_);
      this->interpolate(v);
      //cout << "HDF5 read " << endl;
      //cout << "HDF5 Nx Ny Nz == " << Nx << " " << Ny << " " << Nz << endl;
      //cout << "pad  Nx Ny Nz == " << Nx_ << " " << Ny_ << " " << Nz_ << endl;
      this->setPadded( ((2*Nx_)/3 >= Nx && (2*Nz_)/3 >= Nz) ? true : false);
      //cout << "(2*Nxpad)/3 == " << (2*Nx_)/3 << endl;
      //cout << "(2*Nzpad)/3 == " << (2*Nz_)/3 << endl;
      //cout << "padded == " << padded_ << endl;
    }
#endif // HAVE_HDF5
  }

  else {
    ifstream is(filename.c_str());
    if (!is) {
      cerr << "FlowField::Flowfield(filebase) : can't open file " << filename << endl;
      exit(1);
    }

    int major_version, minor_version, update_version;
    read(is, major_version);
    read(is, minor_version);
    read(is, update_version);
    read(is, Nx_);
    read(is, Ny_);
    read(is, Nz_);
    read(is, Nd_);
    read(is, xzstate_);
    read(is, ystate_);
    read(is, Lx_);
    read(is, Lz_);
    read(is, a_);
    read(is, b_);
    read(is, padded_);
    resize(Nx_, Ny_, Nz_, Nd_, Lx_, Lz_, a_, b_);

    // Read data only for non-aliased modes, assume 0 for aliased.
    if (padded_ == true && xzstate_ == Spectral) {
      int Nxd=2*(Nx_/6);
      int Nzd=2*(Nz_/3)+1;

      // In innermost loop, array index is (nz + Nzpad2_*(nx + Nx_*(ny + Ny_*i))),
      // which is the same as the FlowField::flatten function.
      for (int i=0; i<Nd_; ++i) {
	for (int ny=0; ny<Ny_; ++ny) {

	  for (int nx=0; nx<=Nxd; ++nx) {
	    for (int nz=0; nz<=Nzd; ++nz)
	      read(is, rdata_[flatten(nx,ny,nz,i)]);
	    for (int nz=Nzd+1; nz<Nzpad_; ++nz)
	      rdata_[flatten(nx,ny,nz,i)] = 0.0;
	  }
	  for (int nx=Nxd+1; nx<=Nxd; ++nx)
	    for (int nz=0; nz<=Nzpad_; ++nz)
	      rdata_[flatten(nx,ny,nz,i)] = 0.0;

	  for (int nx=Nx_-Nxd; nx<Nx_; ++nx) {
	    for (int nz=0; nz<=Nzd; ++nz)
	      read(is, rdata_[flatten(nx,ny,nz,i)]);
	    for (int nz=Nzd+1; nz<Nzpad_; ++nz)
	      rdata_[flatten(nx,ny,nz,i)] = 0.0;
	  }
	}
      }
    }
    else {
      int N = Nd_*Ny_*Nx_*Nzpad_;
      for (int i=0; i<N; ++i)
	read(is, rdata_[i]);
    }
  }
}

FlowField::FlowField(const string& filebase, int major, int minor, int update)
  :
  Nx_(0),
  Ny_(0),
  Nz_(0),
  Nzpad_(0),
  Nzpad2_(0),
  Nd_(0),
  Lx_(0.0),
  Lz_(0.0),
  a_(0.0),
  b_(0.0),
  padded_(false),
  rdata_(0),
  cdata_(0),
  scratch_(0),
  xzstate_(Spectral),
  ystate_(Spectral),
  xz_plan_(0),
  xz_iplan_(0),
  y_plan_(0)
{

  ifstream is;
  string filename = ifstreamOpen(is, filebase, ".ff", ios::in | ios::binary);
  if (!is) {
    cerr << "FlowField::FlowField(filebase, major,minor,update) : "
	 << "can't open file "<< filename << " or " << filebase << endl;
    exit(1);
  }

  if (major > 0 || minor > 9) {
    cerr << "FlowField::FlowField(filebase, major,minor,update) :\n"
	 << major << '.' << minor << '.' << update << " is higher than 0.9.x" << endl;
    exit(1);
  }

  if (update >= 16) {
    (*this) = FlowField(filebase); // not efficient but saves code redundancy
    return;
  }
  else {
    is.read((char*) &Nx_, sizeof(int));
    is.read((char*) &Ny_, sizeof(int));
    is.read((char*) &Nz_, sizeof(int));
    is.read((char*) &Nd_, sizeof(int));
    is >> xzstate_;
    is >> ystate_;
    is.read((char*) &Lx_, sizeof(Real));
    is.read((char*) &Lz_, sizeof(Real));
    is.read((char*) &a_, sizeof(Real));
    is.read((char*) &b_, sizeof(Real));
    char s;
    is.get(s);
    padded_ = (s=='1') ? true : false;

    resize(Nx_, Ny_, Nz_, Nd_, Lx_, Lz_, a_, b_);

    // Read data only for non-aliased modes, assume 0 for aliased.
    if (padded_ == true && xzstate_ == Spectral) {
      int Nxd=2*(Nx_/6);
      int Nzd=2*(Nz_/3)+1;

      // In innermost loop, array index is (nz + Nzpad2_*(nx + Nx_*(ny+Ny_*i)))
      // which is the same as the FlowField::flatten function.
      for (int i=0; i<Nd_; ++i) {
	for (int ny=0; ny<Ny_; ++ny) {

	  for (int nx=0; nx<=Nxd; ++nx) {
	    for (int nz=0; nz<=Nzd; ++nz)
	      is.read((char*) (rdata_ + flatten(nx,ny,nz,i)), sizeof(Real));
	    for (int nz=Nzd+1; nz<Nzpad_; ++nz)
	      rdata_[flatten(nx,ny,nz,i)] = 0.0;
	  }
	  for (int nx=Nxd+1; nx<=Nxd; ++nx)
	    for (int nz=0; nz<=Nzpad_; ++nz)
	      rdata_[flatten(nx,ny,nz,i)] = 0.0;

	  for (int nx=Nx_-Nxd; nx<Nx_; ++nx) {
	    for (int nz=0; nz<=Nzd; ++nz)
	      is.read((char*) (rdata_ + flatten(nx,ny,nz,i)), sizeof(Real));
	    for (int nz=Nzd+1; nz<Nzpad_; ++nz)
	      rdata_[flatten(nx,ny,nz,i)] = 0.0;
	  }
	}
      }
    }
    else {
      int N = Nd_*Ny_*Nx_*Nzpad_;
      for (int i=0; i<N; ++i)
	is.read((char*) (rdata_ + i), sizeof(Real));
    }
  }
}

Vector FlowField::xgridpts() const {
  Vector xpts(Nx_);
  for (int nx=0; nx<Nx_; ++nx)
    xpts[nx] = x(nx);
  return xpts;
}
Vector FlowField::ygridpts() const {
  Vector ypts(Ny_);
  Real c = 0.5*(b_+a_);
  Real r = 0.5*(b_-a_);
  Real piN = pi/(Ny_-1);
  for (int ny=0; ny<Ny_; ++ny)
    ypts[ny] = c + r*cos(piN*ny);
  return ypts;
}
Vector FlowField::zgridpts() const {
  Vector zpts(Nz_);
  for (int nz=0; nz<Nz_; ++nz)
    zpts[nz] = z(nz);
  return zpts;
}

FlowField& FlowField::operator=(const FlowField& f) {
  resize(f.Nx_, f.Ny_, f.Nz_, f.Nd_, f.Lx_, f.Lz_, f.a_, f.b_);
  setState(f.xzstate_, f.ystate_);
  padded_ = f.padded_;
  nu_ = f.nu_;
  int Ntotal = Nx_ * Ny_ * Nzpad_ * Nd_;
  for (int i=0; i<Ntotal; ++i)
    rdata_[i] = f.rdata_[i];
  return *this;
}

FlowField::~FlowField() {
  if (rdata_)    fftw_free(rdata_);
  if (scratch_)  fftw_free(scratch_);
  if (y_plan_)   fftw_destroy_plan(y_plan_);
  if (xz_plan_)  fftw_destroy_plan(xz_plan_);
  if (xz_iplan_) fftw_destroy_plan(xz_iplan_);
  rdata_ = 0;
  cdata_ = 0;
  scratch_ = 0;
  y_plan_ = 0;
  xz_plan_ = 0;
  xz_iplan_ = 0;
}

bool FlowField::isNull() {
  return (Nx_ == 0 && Ny_ == 0 && Nz_ == 0 && Nd_ == 0);
}

void FlowField::reconfig(const FlowField& u, uint fftw_flags) {
  resize(u.Nx(), u.Ny(), u.Nz(), u.Nd(), u.Lx(),u.Lz(),u.a(),u.b(),fftw_flags);
}

void FlowField::resize(int Nx, int Ny, int Nz, int Nd, Real Lx, Real Lz,
		       Real a, Real b, uint fftw_flags) {

  assert(Nx>=0);
  assert(Ny>=0);
  assert(Nz>=0);
  assert(Nd>=0);
  assert(Lx>=0);
  assert(Lz>=0);
  assert(b >= a);

  //if (Nx % 2 == 1)
  //cferror("FlowField::resize(Nx,Ny,Nz,Nd,Lx,Lz,a,b,fftwflags) : Nx must be even");
  //if (Ny != 0 && Ny % 2 == 0)
  //cferror("FlowField::resize(Nx,Ny,Nz,Nd,Lx,Lz,a,b,fftwflags) : Ny must be odd");
  //if (Nz % 2 == 1)
  //cferror("FlowField::resize(Nx,Ny,Nz,Nd,Lx,Lz,a,b,fftwflags) : Nz must be even");

  Nx_ = Nx;
  Ny_ = Ny;
  Nz_ = Nz;
  Nd_ = Nd;
  Lx_ = Lx;
  Lz_ = Lz;
  a_ = a;
  b_ = b;

  Nzpad_ = 2*(Nz_/2+1);
  Nzpad2_ = Nz_/2+1;

  // INEFFICIENT if geometry doesn't change. Should check.
  if (rdata_) fftw_free(rdata_);
  rdata_ = (Real*) fftw_malloc((Nx_*Ny_*Nzpad_*Nd_)*sizeof(Real));
  cdata_ = (Complex*) rdata_;

  if (scratch_) fftw_free(scratch_);
  scratch_ = (Real*) fftw_malloc(Ny_*sizeof(Real));

  fftw_initialize(fftw_flags);

  int N = Nd_*Ny_*Nx_*Nzpad_;
  for (int i=0; i<N; ++i)
    rdata_[i] = 0.0;
}

//extern int nthreads;

void FlowField::fftw_initialize(uint fftw_flags) {
  fftw_flags = fftw_flags | FFTW_DESTROY_INPUT;
  //fftw_flags = FFTW_MEASURE | FFTW_DESTROY_INPUT;
  //fftw_flags = FFTW_ESTIMATE | FFTW_DESTROY_INPUT;

  if (y_plan_)   fftw_destroy_plan(y_plan_);
  if (xz_plan_)  fftw_destroy_plan(xz_plan_);
  if (xz_iplan_) fftw_destroy_plan(xz_iplan_);

  // Initialize xz plan
  if (Nx_ !=0 && Nz_ !=0) {
    fftw_complex* fcdata = (fftw_complex*) cdata_;

    const int howmany = Ny_*Nd_;
    const int rank = 2;

    // These params describe the structure of the real-valued array
    int real_n[rank];
    real_n[0] = Nx_;
    real_n[1] = Nz_;
    int real_embed[rank];
    real_embed[0] = Nx_;
    real_embed[1] = Nzpad_;
    const int real_stride = 1;
    const int real_dist = Nx_*Nzpad_;

    // These params describe the structure of the complex-valued array
    //int cplx_n[rank];
    //cplx_n[0] = Nx_;
    //cplx_n[1] = Nzpad2_;
    int cplx_embed[rank];
    cplx_embed[0] = Nx_;
    cplx_embed[1] = Nzpad2_;
    const int cplx_stride = 1;
    const int cplx_dist = Nx_*Nzpad2_;

    //fftw_plan_with_nthreads(nthreads);
    // Real -> Complex transform parameters
    xz_plan_ =
      fftw_plan_many_dft_r2c(rank, real_n, howmany,
			     rdata_, real_embed, real_stride, real_dist,
			     fcdata, cplx_embed, cplx_stride, cplx_dist,
			     fftw_flags);

    xz_iplan_ =
      fftw_plan_many_dft_c2r(rank, real_n, howmany,
			     fcdata, cplx_embed, cplx_stride, cplx_dist,
			     rdata_, real_embed, real_stride, real_dist,
			     fftw_flags);
    //fftw_plan_with_nthreads(1);

  }
  else {
    xz_plan_ = 0;
    xz_iplan_ = 0;
  }

  // Initialize y plan
  if (Ny_ >= 2)
    y_plan_  = fftw_plan_r2r_1d(Ny_,scratch_,scratch_,FFTW_REDFT00,fftw_flags);
  else
    y_plan_  = 0;

}

void FlowField::rescale(Real Lx, Real Lz) {
  assertState(Spectral, Spectral);
  Real scaleu = Lx/Lx_;
  Real scalew = Lz/Lz_;
  Real e = L2Norm(*this);
  for (int ny=0; ny<numYmodes(); ++ ny)
    for (int mx=0; mx<Mx(); ++mx)
      for (int mz=0; mz<Mz(); ++ mz) {
	cmplx(mx,ny,mz,0) *= scaleu;
	cmplx(mx,ny,mz,2) *= scalew;
      }
  Lx_ = Lx;
  Lz_ = Lz;

  // rescale to have same norm as original
  Real e2 = L2Norm(*this);
  if (e2 > 0)
    (*this) *= e/e2;
}

void FlowField::interpolate(const FlowField& ff) {
    FlowField& g = *this;
    FlowField& f = (FlowField&) ff;

    fieldstate fxstate = f.xzstate();
    fieldstate fystate = f.ystate();
    fieldstate gxstate = g.xzstate();
    fieldstate gystate = g.ystate();

    // Always check these, debugging or not.
    if (f.Lx() != g.Lx()  || f.Lz() != g.Lz() || f.a() != g.a()  ||
            f.b() != g.b() || f.Nd() != g.Nd()) {
        cerr << "FlowField::interpolate(const FlowField& f) error:\n"
            << "FlowField doesn't match argument f geometrically.\n";
        exit(1);
    }
    g.nu_ = f.nu_;

    const int fNy = f.Ny();
    const int gNy = g.Ny();
    const int Nd = f.Nd();
    g.setToZero();

    ComplexChebyCoeff fprof(f.Ny(), a_, b_, Spectral);
    ComplexChebyCoeff gprof(g.Ny(), a_, b_, Spectral);

    const int kxmax = lesser(f.kxmax(), g.kxmax());
    const int kzmax = lesser(f.kzmax(), g.kzmax());
    const int kxmin = Greater(f.kxmin(), g.kxmin());

    f.makeSpectral();
    g.setState(Spectral, Spectral);
    g.setToZero();

    for (int i=0; i<Nd; ++i) {

        // If going from smaller Chebyshev expansion to larger
        // Just copy the spectral coefficients and loop in fastest order
        if (fNy <= gNy) {
            for (int ny=0; ny<fNy; ++ny)
                for (int kx=kxmin; kx<kxmax; ++kx) {
                    int fmx = f.mx(kx);
                    int gmx = g.mx(kx);
                    for (int kz=0; kz<=kzmax; ++kz) {
                        int fmz = f.mz(kz);
                        int gmz = g.mz(kz);
                        g.cmplx(gmx,ny,gmz,i) = f.cmplx(fmx,ny,fmz,i);
                    }
                }
        }
        // Going from larger Chebyshev expansion to smaller
        // Interpolate the spectral coefficients. That requires an inner loop in y.
        else {
            for (int kx=kxmin; kx<kxmax; ++kx) {
                int fmx = f.mx(kx);
                int gmx = g.mx(kx);
                for (int kz=0; kz<=kzmax; ++kz) {
                    int fmz = f.mz(kz);
                    int gmz = g.mz(kz);
                    for (int ny=0; ny<fNy; ++ny)
                        fprof.set(ny, f.cmplx(fmx,ny,fmz,i));
                    gprof.interpolate(fprof);
                    for (int ny=0; ny<gNy; ++ny)
                        g.cmplx(gmx,ny,gmz,i) = gprof[ny];
                }
            }
        }

        // Fourier discretizations go from -Mx/2+1 <= kx <= Mx/2
        // So the last mode kx=Mx/2 has an implicit -kx counterpart that is
        // not included in the Fourier representation. For f, this mode is
        // implicitly/automatically included in the Fourier transforms.
        // But if g has more x modes than f, we need to assign
        // g(-kxmax) = conj(f(kxmax)) explicitly.
        if (g.Nx() > f.Nx()) {
            int kx = f.kxmax();
            int fmx = f.mx(kx);
            int gmx = g.mx(-kx);

            if (fNy <= gNy) {
                for (int ny=0; ny<fNy; ++ny) {
                    for (int kz=f.kzmin(); kz<=f.kzmax(); ++kz) {
                        int fmz = f.mz(kz);
                        int gmz = g.mz(kz);
                        g.cmplx(gmx,ny,gmz,i) = f.cmplx(fmx,ny,fmz,i);
                    }
                }
            }
            else {
                for (int kz=f.kzmin(); kz<=f.kzmax(); ++kz) {
                    int fmz = f.mz(kz);
                    int gmz = g.mz(kz);
                    for (int ny=0; ny<fNy; ++ny)
                        fprof.set(ny, f.cmplx(fmx,ny,fmz,i));
                    gprof.interpolate(fprof);
                    for (int ny=0; ny<gNy; ++ny)
                        g.cmplx(gmx,ny,gmz,i) = gprof[ny];
                }
            }
        }
    }

    f.makeState(fxstate, fystate);
    g.makeState(gxstate, gystate);
}


void FlowField::optimizeFFTW(uint fftw_flags) {
  fftw_flags = fftw_flags | FFTW_DESTROY_INPUT;
  fftw_initialize(fftw_flags);
}

ComplexChebyCoeff FlowField::profile(int mx, int mz, int i) const {
  ComplexChebyCoeff rtn(Ny_, a_, b_, ystate_);
  if (xzstate_== Spectral) {
    for (int ny=0; ny<Ny_; ++ny)
      rtn.set(ny, cmplx(mx, ny, mz, i));
  }
  else
    for (int ny=0; ny<Ny_; ++ny)
      rtn.re[ny] = (*this)(mx, ny, mz, i);

  return rtn;
}

BasisFunc FlowField::profile(int mx, int mz) const {
  assert(xzstate_== Spectral);
  BasisFunc rtn(Nd_, Ny_, kx(mx), kz(mz), Lx_, Lz_, a_, b_, ystate_);
  for (int i=0; i<Nd_; ++i)
    for (int ny=0; ny<Ny_; ++ny)
      rtn[i].set(ny, cmplx(mx, ny, mz, i));
  return rtn;
}

int sign_i(const FieldSymmetry& sigma, int n, int Nd) {
  switch (Nd) {
  case 0:
  case 1:
    return 1;
  case 3:
    return sigma.s(n);
  case 9:
    // implements inverse of i3j(i,j) -> n function
    return sigma.s(n/3) * sigma.s(n%3);
  default:
    cferror("error : symmetry operations defined only for 3d scalar, vector, tensor fields");
    return 0;
  }
}

/****************
FlowField& FlowField::project(const FieldSymmetry& sigma) {
  FlowField tmp = *this;
  tmp *= sigma;
  *this += tmp;
  *this *= 0.5;
  return *this;
}
*******************/

// 2007-03-12 notes

FlowField& FlowField::project(const FieldSymmetry& sigma) {
  // Identity escape clause
  if (sigma.sx()==1 && sigma.sy()==1 && sigma.sz()==1 &&
      sigma.ax()==0.0 && sigma.az()==0.0 && sigma.s() == 1)
    return *this;


  fieldstate xzs = xzstate_;
  fieldstate ys  = ystate_;
  makeState(Spectral, Spectral);

  const Real cx = 2*pi*sigma.ax();
  const Real cz = 2*pi*sigma.az();

  // (u,v,w)(x,y,z) -> (sa sx u, sa sy v, sa sz w) (sx x + fx*Lx, sy y, sz z + fz*Lz)
  const int s =  sigma.s();
  const int sx = sigma.sx();
  const int sy = sigma.sy();
  const int sz = sigma.sz();

  const int Kxmin = padded() ? kxminDealiased() : kxmin();
  const int Kxmax = padded() ? kxmaxDealiased() : kxmax();
  const int Kzmin = padded() ? kzminDealiased() : kzmin();
  const int Kzmax = padded() ? kzmaxDealiased() : kzmax();

  Complex tmp_p;
  Complex tmp_m;

  // If sx,sz are -1,1 or 1,1, we must average kx,kz and -kx,kz modes
  if (sx + sz == 0) {
    for (int i=0; i<Nd_; ++i) {
      int si = sign_i(sigma, i, Nd_);

      for (int ky=0; ky<Ny_; ++ky) {
	int syl = ((sy == -1) && (ky % 2 == 1)) ? -1 : 1;
	Real symmsign = Real(s*si*syl);

	for (int kx_m = Kxmin; kx_m<=0; ++kx_m) {
	  int kx_p = -kx_m;
	  int mx_m = mx(kx_m);  // mx minus
	  int mx_p = mx(kx_p);  // mx plus
	  for (int kz=0; kz<=Kzmax; ++kz) {
	    int mz = this->mz(kz);
	    Complex cxyz_p = symmsign*exp(Complex(0.0, cx*sx*kx_p + cz*sz*kz));
	    Complex cxyz_m = symmsign*exp(Complex(0.0, cx*sx*kx_m + cz*sz*kz));

	    if (sx == -1) {
	      // u_{ijkl}     -> (       u_{ijkl} + cxyz_p u_{i,-j,k,l})/2
	      // u_{i,-j,k,l} -> (cxyz_m u_{ijkl} +        u_{i,-j,k,l})/2

	      tmp_p = cmplx(mx_p,ky,mz,i);
	      tmp_m = cmplx(mx_m,ky,mz,i);

	      cmplx(mx_p,ky,mz,i) = 0.5*(       tmp_p + cxyz_p*tmp_m);
	      cmplx(mx_m,ky,mz,i) = 0.5*(cxyz_m*tmp_p +        tmp_m);
	    }
	    else {
	      // u_{ijkl}     -> cxyz_p u^*_{i,-j,k,l})
	      // u_{i,-j,k,l} -> cxyz_m u^*_{i,j,k,l}

	      // u_{ijkl}     -> (       u_{ijkl}   + cxyz_p u^*_{i,-j,k,l})/2
	      // u_{i,-j,k,l} -> (cxyz_m u^*_{ijkl} +          u_{i,-j,k,l})/2

	      tmp_p = cmplx(mx_p,ky,mz,i);
	      tmp_m = cmplx(mx_m,ky,mz,i);

	      cmplx(mx_p,ky,mz,i) = 0.5*(            tmp_p  + cxyz_p*conj(tmp_m));
	      cmplx(mx_m,ky,mz,i) = 0.5*(cxyz_m*conj(tmp_p) +             tmp_m);
	    }

	  }
	}
      }
    }
  }
  // If sx,sz are 1,1 or -1,-1, can apply symmetry by *=
  else {
    for (int i=0; i<Nd_; ++i) {
      int si = sign_i(sigma, i, Nd_);
      //int si = sigma.sign(i); // (u,v,w) -> (s0 u, s1 v, s2 w)

      for (int ky=0; ky<Ny_; ++ky) {
	int syl = (sy == -1 && (ky % 2 == 1)) ? -1 : 1;
	Real symmsign = Real(s*si*syl);

	for (int kx=Kxmin; kx<=Kxmax; ++kx) {
	  int mx = this->mx(kx);
	  for (int kz=Kzmin; kz<=Kzmax; ++kz) {
	    int mz = this->mz(kz);
	    Complex cxyz = symmsign*exp(Complex(0.0,  cx*sx*kx + cz*sz*kz));

	    if (sx == 1)
	      //cmplx(mx,ky,mz,i) *= cxyz;
	      cmplx(mx,ky,mz,i) *= 0.5*(1.0+cxyz);
	    else
	      cmplx(mx,ky,mz,i) = 0.5*(cmplx(mx,ky,mz,i) + cxyz*conj(cmplx(mx,ky,mz,i)));
	  }
	}
      }
    }
  }
  makeState(xzs, ys);
  return *this;
}

FlowField& FlowField::project(const array<FieldSymmetry>& sigma) {
  /****************************
  cout << "n == " << sigma.length() << endl;
  for (int n=0; n<sigma.length(); ++n) {
    cout << "sigma == " << sigma[n] << endl;
    FlowField tmp(*this);
    tmp *= sigma[n];
    *this += tmp;
  }
  cout << "n == " << sigma.length() << endl;
  cout << pow(0.5, sigma.length()) << endl;
  *this *= pow(0.5, sigma.length());
  ************************/

  /******************
  FlowField tmp;
  for (int n=0; n<sigma.length(); ++n) {
    tmp = *this;
    tmp *= sigma[n];
    *this += tmp;
  }
  *this *= pow(0.5, sigma.length());
  return *this;
  *************************/

  for (int n=0; n<sigma.length(); ++n)
    this->project(sigma[n]);
  return *this;
}





// 2007-03-12 notes
FlowField& FlowField::operator *= (const FieldSymmetry& sigma) {

  // Identity escape clause
  if (sigma.sx()==1 && sigma.sy()==1 && sigma.sz()==1 &&
      sigma.ax()==0.0 && sigma.az()==0.0 && sigma.s() == 1)
    return *this;


  const  fieldstate xzs = xzstate_;
  const fieldstate ys  = ystate_;
  makeState(Spectral, Spectral);

  const Real cx = 2*pi*sigma.ax();
  const Real cz = 2*pi*sigma.az();


  // (u,v,w)(x,y,z) -> (sa sx u, sa sy v, sa sz w) (sx x + fx*Lx, sy y, sz z + fz*Lz)
  const int s =  sigma.s();
  const int sx = sigma.sx();
  const int sy = sigma.sy();
  const int sz = sigma.sz();

  const int Kxmin = padded() ? kxminDealiased() : kxmin();
  const int Kxmax = padded() ? kxmaxDealiased() : kxmax();
  const int Kzmin = padded() ? kzminDealiased() : kzmin();
  const int Kzmax = padded() ? kzmaxDealiased() : kzmax();

  Complex tmp;
  // If one of sx,sz is -1,1 or 1,1, we must swap kx,kz and -kx,kz modes
  if (sx + sz == 0) {
    for (int i=0; i<Nd_; ++i) {
      int si = sign_i(sigma, i, Nd_);

      for (int ky=0; ky<Ny_; ++ky) {
	int syl = ((sy == -1) && (ky % 2 == 1)) ? -1 : 1;
	Real symmsign = Real(s*si*syl);

	for (int kx_m = Kxmin; kx_m<=0; ++kx_m) {
	  int kx_p = -kx_m;
	  int mx_m = mx(kx_m);  // mx minus
	  int mx_p = mx(kx_p);  // mx plus
	  for (int kz=0; kz<=Kzmax; ++kz) {
	    int mz = this->mz(kz);
	    Complex cxyz_p = symmsign*exp(Complex(0.0, cx*sx*kx_p + cz*sz*kz));
	    Complex cxyz_m = symmsign*exp(Complex(0.0, cx*sx*kx_m + cz*sz*kz));

	    if (sx == -1) {
	      // u_{ijkl}     -> cxyz_p u_{i,-j,k,l}
	      // u_{i,-j,k,l} -> cxyz_m u_{i,j,k,l}
	      tmp = cmplx(mx_p,ky,mz,i);
	      cmplx(mx_p,ky,mz,i) = cxyz_p*cmplx(mx_m,ky,mz,i);
	      cmplx(mx_m,ky,mz,i) = cxyz_m*tmp;
	    }
	    else {
	      // u_{ijkl}     -> cxyz_p u^*_{i,-j,k,l})
	      // u_{i,-j,k,l} -> cxyz_m u^*_{i,j,k,l}
	      tmp = cmplx(mx_p,ky,mz,i);
	      cmplx(mx_p,ky,mz,i) = cxyz_p*conj(cmplx(mx_m,ky,mz,i));
	      cmplx(mx_m,ky,mz,i) = cxyz_m*conj(tmp);
	    }

	  }
	}
      }
    }
  }
  // If sx,sz are 1,1 or -1,-1, can apply symmetry by *=
  else {
    for (int i=0; i<Nd_; ++i) {
      int si = sign_i(sigma, i, Nd_);
      //int si = sigma.sign(i); // (u,v,w) -> (s0 u, s1 v, s2 w)

      for (int ky=0; ky<Ny_; ++ky) {
	int syl = (sy == -1 && (ky % 2 == 1)) ? -1 : 1;
	Real symmsign = Real(s*si*syl);

	for (int kx=Kxmin; kx<=Kxmax; ++kx) {
	  int mx = this->mx(kx);
	  for (int kz=Kzmin; kz<=Kzmax; ++kz) {
	    int mz = this->mz(kz);
	    Complex cxyz = symmsign*exp(Complex(0.0,  cx*sx*kx + cz*sz*kz));

	    if (sx == 1)
	      cmplx(mx,ky,mz,i) *= cxyz;
	    else
	      cmplx(mx,ky,mz,i) = cxyz*conj(cmplx(mx,ky,mz,i));
	  }
	}
      }
    }
  }
  makeState(xzs, ys);
  return *this;
}

bool FlowField::geomCongruent(const FlowField& v, Real eps) const {
  return ((Nx_ == v.Nx_) &&
	  (Ny_ == v.Ny_) &&
	  (Nz_ == v.Nz_) &&
	  (abs(Lx_ - v.Lx_)/Greater(Lx_,1.0) < eps) &&
	  (abs(Lz_ - v.Lz_)/Greater(Lz_,1.0) < eps) &&
	  (abs(a_ - v.a_)/Greater(abs(a_),1.0) < eps) &&
	  (abs(b_ - v.b_)/Greater(abs(b_),1.0) < eps));
}

bool FlowField::congruent(const FlowField& v, Real eps) const {
  return ((Nx_ == v.Nx_) &&
	  (Ny_ == v.Ny_) &&
	  (Nz_ == v.Nz_) &&
	  (Nd_ == v.Nd_) &&
	  (abs(Lx_ - v.Lx_)/Greater(Lx_,1.0) < eps) &&
	  (abs(Lz_ - v.Lz_)/Greater(Lz_,1.0) < eps) &&
	  (abs(a_ - v.a_)/Greater(abs(a_),1.0) < eps) &&
	  (abs(b_ - v.b_)/Greater(abs(b_),1.0) < eps) &&
	  (xzstate_ == v.xzstate_) &&
	  (ystate_ == v.ystate_));
}

bool FlowField::congruent(const BasisFunc& f) const {
  return ((Ny_==f.Ny()) && (Lx_==f.Lx()) && (Lz_==f.Lz()) &&
	  (a_==f.a()) && (b_==f.b()) && (ystate_==f.state()));
}

bool FlowField::congruent(const RealProfileNG& e) const {
  return ((Ny_==e.Ny()) && (Lx_==e.Lx()) && (Lz_==e.Lz()) &&
	  (a_==e.a()) && (b_==e.b()) && (ystate_==e.state()));
}

FlowField& FlowField::operator *= (Real x) {
  int Ntotal = Nx_ * Ny_ * Nzpad_ * Nd_;
  for (int i=0; i<Ntotal; ++i)
    rdata_[i] *= x;
  return *this;
}

FlowField& FlowField::operator *= (Complex z) {
  assert(xzstate_ == Spectral);
  int Ntotal = Nx_ * Ny_ * Nzpad2_ * Nd_;
  for (int i=0; i<Ntotal; ++i)
    cdata_[i] *= z;
  return *this;
}

FlowField& FlowField::operator += (const ChebyCoeff& U) {
  assert(xzstate_== Spectral);
  assert(ystate_ == U.state());
  assert(Ny_ == U.N());
  for (int ny=0; ny<Ny_; ++ny)
    cmplx(0, ny, 0, 0) += Complex(U[ny], 0.0);
  return *this;
}

FlowField& FlowField::operator -= (const ChebyCoeff& U) {
  assert(xzstate_== Spectral);
  assert(ystate_ == U.state());
  assert(Ny_ == U.N());
  for (int ny=0; ny<Ny_; ++ny)
    cmplx(0, ny, 0, 0) -= Complex(U[ny], 0.0);
  return *this;
}

FlowField& FlowField::operator += (const ComplexChebyCoeff& U) {
  assert(xzstate_== Spectral);
  assert(ystate_ == U.state());
  assert(Ny_ == U.N());
  for (int ny=0; ny<Ny_; ++ny)
    cmplx(0, ny, 0, 0) += U[ny];
  return *this;
}

FlowField& FlowField::operator -= (const ComplexChebyCoeff& U) {
  assert(xzstate_== Spectral);
  assert(ystate_ == U.state());
  assert(Ny_ == U.N());
  for (int ny=0; ny<Ny_; ++ny)
    cmplx(0, ny, 0, 0) -= U[ny];
  return *this;
}

FlowField& FlowField::operator += (const BasisFunc& f){
  assert(xzstate_== Spectral);
  assert(ystate_ == f.state());
  assert(Nd_ == f.Nd());
  int m_x = mx(f.kx());
  int m_z = mz(f.kz());
  for (int i=0; i<Nd_; ++i)
    for (int ny=0; ny<Ny_; ++ny)
      cmplx(m_x, ny, m_z, i) += f[i][ny];
  return *this;
}

FlowField& FlowField::operator -= (const BasisFunc& f){
  assert(xzstate_== Spectral);
  assert(ystate_ == f.state());
  assert(Nd_ == f.Nd());
  int m_x = mx(f.kx());
  int m_z = mz(f.kz());
  for (int i=0; i<Nd_; ++i)
    for (int ny=0; ny<Ny_; ++ny)
      cmplx(m_x, ny, m_z, i) -= f[i][ny];
  return *this;
}

FlowField& FlowField::operator += (const RealProfile& f){
  assert(xzstate_== Spectral);
  assert(ystate_ == f.state());
  assert(Nd_ == f.Nd());
  int fkx = f.kx();
  int fkz = f.kz();

  if (fkx < kxmin() || fkx > kxmax() || fkz < kzmin() || fkz > kzmax())
    return *this;

  int m_x = mx(fkx);
  int m_z = mz(fkz);
  Complex minus_i(0.0, 1.0);
  const BasisFunc& psi = f.psi;

  // f = (psi + psi^*)   case Plus
  // f = (psi - psi^*)/i case Minus
  //
  // where psi.kz >= 0
  //       psi.kx is negative, zero, or positive.

  // Because u is real, u(-kx,-kz) = u^*(kx,kz)
  // or equivalently    u( kx,-kz) = u^*(-kx,kz)

  // Because of this symmetry and the way FFTW works, FlowField stores
  // only u(kx,kz) for kz > 0, u(kx,-kz) is obtained from the symmetry.
  // But for kz=0 and most kx, both u(kx,0) and u(-kx,0) are stored.
  // These are handled by the if statement below
  switch (f.sign()) {

  case Minus:
    for (int i=0; i<Nd_; ++i)
      for (int ny=0; ny<Ny_; ++ny)
	cmplx(m_x, ny, m_z, i) += minus_i*psi[i][ny];
    break;

  case Plus:
    for (int i=0; i<Nd_; ++i)
      for (int ny=0; ny<Ny_; ++ny)
	cmplx(m_x, ny, m_z, i) += psi[i][ny];
    break;
  }

  // When kz=0, both u(kx,0) and u(-kx,0) are stored in the flow field,
  // except for the very largest kx>0 when Nx is even. So to we need to
  // add u(-kx,0) = u^*(kx,0) for kx>1.
  if (fkz==0 && fkx>=0 && -fkx >= kxmin()) {
    int nmx = mx(-fkx);
    switch (f.sign()) {

    case Minus:
      for (int i=0; i<Nd_; ++i)
	for (int ny=0; ny<Ny_; ++ny)
	  cmplx(nmx, ny, m_z, i) += conj(minus_i*(psi[i][ny]));
      break;

    case Plus:
      for (int i=0; i<Nd_; ++i)
	for (int ny=0; ny<Ny_; ++ny)
	  cmplx(nmx, ny, m_z, i) += conj(psi[i][ny]);
      break;
    }
  }
  return *this;
}

FlowField& FlowField::operator -= (const RealProfile& f){
  assert(xzstate_== Spectral);
  assert(ystate_ == f.state());
  assert(Nd_ == f.Nd());

  int fkx = f.kx();
  int fkz = f.kz();

  if (fkx < kxmin() || fkx > kxmax() || fkz < kzmin() || fkz > kzmax())
    return *this;

  int m_x = mx(fkx);
  int m_z = mz(fkz);
  Complex minus_i(0.0, 1.0);
  const BasisFunc& psi = f.psi;

  switch (f.sign()) {

  case Minus:
    for (int i=0; i<Nd_; ++i)
      for (int ny=0; ny<Ny_; ++ny)
	cmplx(m_x, ny, m_z, i) -= minus_i*psi[i][ny];
    break;

  case Plus:
    for (int i=0; i<Nd_; ++i)
      for (int ny=0; ny<Ny_; ++ny)
	cmplx(m_x, ny, m_z, i) -= psi[i][ny];
    break;
  }

  if (fkz==0 && fkx>=0 && -fkx >= kxmin()) {
    int nmx = mx(-fkx);
    switch (f.sign()) {

    case Minus:
      for (int i=0; i<Nd_; ++i)
	for (int ny=0; ny<Ny_; ++ny)
	  cmplx(nmx, ny, m_z, i) -= conj(minus_i*(psi[i][ny]));
      break;

    case Plus:
      for (int i=0; i<Nd_; ++i)
	for (int ny=0; ny<Ny_; ++ny)
	  cmplx(nmx, ny, m_z, i) -= conj(psi[i][ny]);
      break;
    }
  }
  return *this;
}

FlowField& FlowField::operator += (const RealProfileNG& u)
{
  assert(xzstate_ == Spectral);
  assert(congruent(u));

  ComplexChebyCoeff tmp(Ny(), a(), b(), Spectral);
  const int mx_p = mx( abs( u.jx() ) );
  const int mx_m = mx(-abs(u.jx()));
  const int mz_p = mz(abs(u.jz()));

  for(int i = 0; i<Nd_; ++i)
  {
    const Complex norm_p = u.normalization_p(i);
    const Complex norm_m = u.normalization_m(i);


    tmp.setToZero();
    tmp.re = u.u_[i];

    tmp *= norm_p;
    for(int ny = 0; ny < Ny(); ++ny)
      cmplx(mx_p, ny, mz_p, i) += tmp[ny];
    if(norm_m != Complex(0,0))
    {
      tmp *= norm_m / norm_p;
      for(int ny = 0; ny < Ny(); ++ny)
        cmplx(mx_m, ny, mz_p, i) += tmp[ny];
    }
  }
  return *this;
}

FlowField& FlowField::operator -= (const RealProfileNG& u)
{
  assert(xzstate_ == Spectral);
  assert(congruent(u));

  ComplexChebyCoeff tmp(Ny(), a(), b(), Spectral);
  const int mx_p = mx( abs( u.jx() ) );
  const int mx_m = mx(-abs(u.jx()));
  const int mz_p = mz(abs(u.jz()));
  for(int i = 0; i<Nd_; ++i)
  {
    const Complex norm_p = u.normalization_p(i);
    const Complex norm_m = u.normalization_m(i);

    tmp.setToZero();
    tmp.re = u.u_[i];
    tmp *= norm_p;
    for(int ny = 0; ny < Ny(); ++ny)
      cmplx(mx_p, ny, mz_p, i) -= tmp[ny];
    if(norm_m != Complex(0,0))
    {
      tmp *= norm_m / norm_p;
      for(int ny = 0; ny < Ny(); ++ny)
        cmplx(mx_m, ny, mz_p, i) -= tmp[ny];
    }
  }
  return *this;
}



FlowField& FlowField::operator += (const FlowField& u) {
  assert(congruent(u));
  int Ntotal = Nx_ * Ny_ * Nzpad_ * Nd_;
  for (int i=0; i<Ntotal; ++i)
    rdata_[i] += u.rdata_[i];
  return *this;
}
FlowField& FlowField::operator -= (const FlowField& u) {
  assert(congruent(u));
  int Ntotal = Nx_ * Ny_ * Nzpad_ * Nd_;
  for (int i=0; i<Ntotal; ++i)
    rdata_[i] -= u.rdata_[i];
  return *this;
}

FlowField  FlowField::operator[](int i) const {
  FlowField ui(Nx_, Ny_, Nz_, 1, Lx_, Lz_, a_, b_, xzstate_, ystate_);
  ui.setnu(nu_);

  if (xzstate_ == Spectral)
    for (int my=0; my<My(); ++my)
      for (int mx=0; mx<Mx(); ++mx)
	for (int mz=0; mz<Mz(); ++mz)
	  ui.cmplx(mx,my,mz,0) = this->cmplx(mx,my,mz,i);
  else
    for (int ny=0; ny<Ny(); ++ny)
      for (int nx=0; nx<Nx(); ++nx)
	for (int nz=0; nz<Nz(); ++nz)
	  ui(nx,ny,nz,0) = (*this)(nx,ny,nz,i);
  return ui;
}

FlowField  FlowField::operator[](const array<int>& indices) const {
  const int Nd = indices.length();
  FlowField rtn(Nx_, Ny_, Nz_, Nd, Lx_, Lz_, a_, b_, xzstate_, ystate_);
  rtn.setnu(nu_);

  if (xzstate_ == Spectral) {
    for (int i=0; i<Nd; ++i) {
      const int j = indices[i];
      for (int my=0; my<My(); ++my)
	for (int mx=0; mx<Mx(); ++mx)
	  for (int mz=0; mz<Mz(); ++mz)
	    rtn.cmplx(mx,my,mz,i) = this->cmplx(mx,my,mz,j);
    }
  }
  else {
    for (int i=0; i<Nd; ++i) {
      const int j = indices[i];
      for (int ny=0; ny<Ny(); ++ny)
	for (int nx=0; nx<Nx(); ++nx)
	  for (int nz=0; nz<Nz(); ++nz)
	    rtn(nx,ny,nz,i) = (*this)(nx,ny,nz,j);
    }
  }
  return rtn;
}

Real FlowField::eval(Real x, Real y, Real z, int i) const {
  assertState(Spectral, Spectral);

  Real alpha_x = 2*pi*x/Lx_;
  Real gamma_z = 2*pi*z/Lz_;

  ChebyCoeff uprof(My(), a_, b_, Spectral);

  // At a given (x,z), collect u(y) as Chebyshev expansion
  // by evaluating complex exponentials of Fourier expansion
  for (int my=0; my<My(); ++my) {

    // Compute u_my = myth Chebyshev coefficient as tmp.
    // tmp += 2*Re(cmplx(mx,my,mz,i) in the mx,mz loop double-counts 
    // the 0,0 mode, so presubtract it
    //Real u_my = -Re(u.cmplx(0,0,0,i)); 
    Real u_my = 0;

    for (int mx=0; mx<Mx(); ++mx) {
      int kx = this->kx(mx);
      Real cx = kx*alpha_x;     // cx = 2pi kx x /Lx
      
      // Unroll kz=0 terms, so as not to double-count the real part 
      u_my += Re(this->cmplx(mx,my,0,i)*exp(Complex(0.0, cx))); 
      
      for (int mz=1; mz<Mz(); ++mz) {
	// RHS = u_{kx,kz} exp(2pi i (x kx/Lx + z kz/Lz)) + complex conj
	Real cz = mz*gamma_z; 
	u_my += 2*Re(this->cmplx(mx,my,mz,i)*exp(Complex(0.0, (cx+cz)))); 
      }
    }
    uprof[my] = u_my;
  }
  //uprof.save("uprof");
  uprof.makeSpectral();
  return uprof.eval(y);
} 


void FlowField::makeSpectral_xz() {
  if (xzstate_ == Spectral)
    return;

  fftw_execute(xz_plan_);

  int Ntotal = Nd_* Nx_ * Ny_ * Nzpad_;
  Real scale = 1.0/(Nx_*Nz_);
  for (int i=0; i<Ntotal; ++i)
    rdata_[i] *= scale;

  xzstate_ = Spectral;
}

void FlowField::makePhysical_xz() {
  if (xzstate_ == Physical)
    return;

  fftw_execute(xz_iplan_);
  xzstate_ = Physical;
}

void FlowField::makeSpectral_y() {
  if (ystate_ == Spectral)
    return;

  if (Ny_ < 2) {
    ystate_ = Spectral;
    return;
  }

  Real nrm = 1.0/(Ny_-1); // needed because FFTW does unnormalized transforms
  for (int i=0; i<Nd_; ++i)
    for (int nx=0; nx<Nx_; ++nx)
      for (int nz=0; nz<Nzpad_; ++nz) {

	// Copy data spread through memory into a stride-1 scratch array.
	for (int ny=0; ny<Ny_; ++ny)
	  scratch_[ny] = rdata_[flatten(nx, ny, nz, i)];

	// Transform data
	fftw_execute_r2r(y_plan_, scratch_, scratch_);

	// Copy back to multi-d arrays, normalizing on the way
	// 0th elem is different because of reln btwn cos and Cheb transforms
	rdata_[flatten(nx, 0, nz, i)] = 0.5*nrm*scratch_[0];
	for (int ny=1; ny<Ny_-1; ++ny)
	  rdata_[flatten(nx, ny, nz, i)] = nrm*scratch_[ny];
	rdata_[flatten(nx, Ny_-1, nz, i)] = 0.5*nrm*scratch_[Ny_-1];
      }
  ystate_ = Spectral;
  return;
}

void FlowField::makePhysical_y() {
  if (ystate_ == Physical)
    return;

  if (Ny_ < 2) {
    ystate_ = Physical;
    return;
  }
  for (int i=0; i<Nd_; ++i)
    for (int nx=0; nx<Nx_; ++nx)
      for (int nz=0; nz<Nzpad_; ++nz) {

	// Copy data spread through memory into a stride-1 scratch array.
	// 0th and last elems are different because of reln btwn cos and
	// Cheb transforms.
	scratch_[0] = rdata_[flatten(nx, 0, nz, i)];
	for (int ny=1; ny<Ny_-1; ++ny)
	  scratch_[ny] = 0.5*rdata_[flatten(nx, ny, nz, i)];
	scratch_[Ny_-1] = rdata_[flatten(nx, Ny_-1, nz, i)];

	// Transform data
	fftw_execute_r2r(y_plan_, scratch_, scratch_);

	// Copy transformed data back into main data array
	for (int ny=0; ny<Ny_; ++ny)
	  rdata_[flatten(nx, ny, nz, i)] = scratch_[ny];

      }
  ystate_ = Physical;
  return;
}

void FlowField::makeSpectral()  {makeSpectral_xz(); makeSpectral_y();}
void FlowField::makePhysical()  {makePhysical_xz(); makePhysical_y();}

void FlowField::makeState(fieldstate xzstate, fieldstate ystate)  {
  (xzstate == Physical) ? makePhysical_xz() : makeSpectral_xz();
  (ystate  == Physical) ? makePhysical_y()  : makeSpectral_y();
}

Complex FlowField::Dx(int mx, int n) const {
  Complex rot(0.0, 0.0);
  switch(n % 4) {
  case 0: rot = Complex( 1.0, 0.0); break;
  case 1: rot = Complex( 0.0, 1.0); break;
  case 2: rot = Complex(-1.0, 0.0); break;
  case 3: rot = Complex( 0.0,-1.0); break;
  default: cferror("FlowField::Dx(mx,n) : impossible: n % 4 > 4 !!");
  }
  int kx_ = kx(mx);
  return rot*(std::pow(2*pi*kx_/Lx_, n)*zero_last_mode(kx_,kxmax(),n));
}
Complex FlowField::Dz(int mz, int n) const {
  Complex rot(0.0, 0.0);
  switch(n % 4) {
  case 0: rot = Complex( 1.0, 0.0); break;
  case 1: rot = Complex( 0.0, 1.0); break;
  case 2: rot = Complex(-1.0, 0.0); break;
  case 3: rot = Complex( 0.0,-1.0); break;
  default: cferror("FlowField::Dx(mx,n) : impossible: n % 4 > 4 !!");
  }
  int kz_ = kz(mz);
  return rot*(std::pow(2*pi*kz_/Lz_, n)*zero_last_mode(kz_,kzmax(),n));
}

Complex FlowField::Dx(int mx) const {
  int kx_ = kx(mx);
  return Complex(0.0, 2*pi*kx_/Lx_*zero_last_mode(kx_,kxmax(),1));
}
Complex FlowField::Dz(int mz) const {
  int kz_ = kz(mz);
  return Complex(0.0, 2*pi*kz_/Lz_*zero_last_mode(kz_,kzmax(),1));
}


void FlowField::addPerturbation(int kx, int kz, Real mag, Real decay) {
  assertState(Spectral, Spectral);
  if (mag == 0.0)
    return;

  // Add a div-free perturbation to the base flow.
  ComplexChebyCoeff u(Ny_, a_, b_, Spectral);
  ComplexChebyCoeff v(Ny_, a_, b_, Spectral);
  ComplexChebyCoeff w(Ny_, a_, b_, Spectral);
  randomProfile(u,v,w, kx, kz, Lx_, Lz_, mag, decay);
  Real k = 2*pi*sqrt(kx*kx/(Lx_*Lx_) + kz*kz/(Lz_*Lz_));
  //Real e = sqrt(L2Norm2(u) + L2Norm2(v) + L2Norm2(w));
  //Real r =
  u *= pow(decay, k);
  v *= pow(decay, k);
  w *= pow(decay, k);
  int m_x = mx(kx);
  int m_z = mz(kz);
  for (int ny=0; ny<Ny_; ++ny) {
    cmplx(m_x, ny, m_z, 0) += u[ny];
    cmplx(m_x, ny, m_z, 1) += v[ny];
    cmplx(m_x, ny, m_z, 2) += w[ny];
  }
  if (kz==0 && kx!=0) {
    int m_x = mx(-kx);
    int m_z = mz(0); // -kz=0
    for (int i=0; i<Nd_; ++i)
      for (int ny=0; ny<Ny_; ++ny) {
	cmplx(m_x, ny, m_z, 0) += conj(u[ny]);
	cmplx(m_x, ny, m_z, 1) += conj(v[ny]);
	cmplx(m_x, ny, m_z, 2) += conj(w[ny]);
      }
  }
  return;
}

void FlowField::perturb(Real mag, Real decay, bool meanflow) {
  int Kx = padded() ? kxmaxDealiased() : kxmax();
  int Kz = padded() ? kzmaxDealiased() : kzmax();
  addPerturbations(Kx, Kz, mag, decay, meanflow);
}

void FlowField::addPerturbations(int Kx, int Kz, Real mag, Real decay, bool meanflow) {
  assertState(Spectral, Spectral);
  if (mag == 0.0)
    return;

  int Kxmin = Greater(-Kx, padded() ? kxminDealiased() : kxmin());
  int Kxmax = lesser(Kx, padded() ? kxmaxDealiased() : kxmax()-1);
  int Kzmax = lesser(Kz, padded() ? kzmaxDealiased() : kzmax()-1);

  // Add a div-free perturbation to the base flow.
  for (int kx=Kxmin; kx<=Kxmax; ++kx)
    for (int kz=0; kz<=Kzmax; ++kz) {
      //Real norm = pow(10.0, -(abs(2*pi*kx/Lx_) + abs(2*pi*kz/Lz_)));
      //Real norm = pow(decay, 2*(abs(kx) + abs(kz)));
      Real norm = std::pow(decay, abs(2*pi*kx/Lx_) + abs(2*pi*kz/Lz_));
      if (meanflow || !(kx==0 && kz==0))
	addPerturbation(kx, kz, mag*norm, decay);
    }
  makePhysical();
  makeSpectral();
  return;
}

void FlowField::addPerturbations(Real mag, Real decay, bool meanflow) {
  assertState(Spectral, Spectral);
  if (mag == 0.0)
    return;

  int Kxmin = padded() ? kxminDealiased() : kxmin();
  int Kxmax = padded() ? kxmaxDealiased() : kxmax()-1;
  int Kzmax = padded() ? kzmaxDealiased() : kzmax()-1;

  // Add a div-free perturbation to the base flow.
  for (int mx=0; mx<Mx(); ++mx) {
    int kx_ = kx(mx);
    if (kx_ < Kxmin || kx_ > Kxmax)
      continue;
    for (int mz=0; mz<Mz(); ++mz) {
      int kz_ = kz(mz);
      if (abs(kz_) > Kzmax)
	continue;
      Real norm = std::pow(decay, 2*(abs(kx_) + abs(kz_)));
      if (meanflow || !(kx_==0 && kz_==0))
	addPerturbation(kx_, kz_, mag*norm, decay);
    }
  }
  makePhysical();
  makeSpectral();
  return;
}
bool FlowField::padded() const { return padded_;}

void FlowField::setPadded(bool b) {padded_ = b;}

void FlowField::setToZero() {
  int Ntotal = Nx_*Ny_*Nzpad_*Nd_;
  for (int i=0; i<Ntotal; ++i)
    rdata_[i] = 0.0;
}

void FlowField::zeroPaddedModes() {
  fieldstate xzs = xzstate_;
  fieldstate ys =  ystate_;
  makeSpectral_xz();

  // Not as efficient as possible but less prone to error and more robust
  // to changes in aliasing bounds, and the efficiency diff is negligible
  Complex zero(0.0, 0.0);
  for (int mx=0; mx<Mx(); ++mx) {
    int kx_ = this->kx(mx);
    for (int mz=0; mz<Mz(); ++mz) {
      int kz_ = this->kz(mz);
      if (isAliased(kx_,kz_))
	for (int i=0; i<Nd_; ++i)
	  for (int ny=0; ny<Ny_; ++ny)
	    this->cmplx(mx,ny,mz,i) = zero;
    }
  }
  padded_ = true;
  makeState(xzs,ys);
}

void FlowField::print() const {
  cout << Nx_ << " x " << Ny_ << " x " << Nz_ << endl;
  cout << "[0, " << Lx_ << "] x [-1, 1] x [0, " << Lz_ << "]" << endl;
  cout << xzstate_ << " x " << ystate_ << " x " << xzstate_ << endl;
  cout << xzstate_ << " x " << ystate_ << " x " << xzstate_ << endl;
  if (xzstate_ == Spectral) {
  cout << "FlowField::print() real view " << endl;
    for (int i=0; i<Nd_; ++i) {
      for (int ny=0; ny<Ny_; ++ny) {
	for (int nx=0; nx<Nx_; ++nx) {
 	  cout << "i=" << i << " ny=" << ny << " nx= " << nx << ' ';
 	  int nz; // MSVC++ FOR-SCOPE BUG
	  for (nz=0; nz<Nz_; ++nz)
	    cout << rdata_[flatten(nx,ny,nz,i)] << ' ';
	  cout << " pad : ";
	  for (nz=Nz_; nz<Nzpad_; ++nz)
	    cout << rdata_[flatten(nx,ny,nz,i)] << ' ';
	  cout << endl;
	}
      }
    }
  }
  else {
    cout << "complex view " << endl;
    for (int i=0; i<Nd_; ++i) {
       for (int ny=0; ny<Ny_; ++ny) {
	 for (int nx=0; nx<Nx_; ++nx) {
	   cout << "i=" << i << " ny=" << ny << " nx= " << nx << ' ';
	   for (int nz=0; nz<Nz_/2; ++nz)
	     cout << cdata_[complex_flatten(nx,ny,nz,i)] << ' ';
	   cout << endl;
	 }
       }
    }
  }
}

// k == direction of normal    (e.g. k=0 means a x-normal slice in yz plane.
// i == component of FlowField (e.g. i=0 means u-component)
// n == nth gridpoint along k direction
void FlowField::saveSlice(int k, int i, int nk, const string& filebase,
			  int xstride, int ystride, int zstride) const {
  assert(k>=0 && k<3);
  assert(i>=0 && i<Nd_);


  string filename(filebase);
  filename += string(".asc");
  ofstream os(filename.c_str());

  FlowField& u = (FlowField&) *this;  // cast away constness
  fieldstate xzstate = xzstate_;
  fieldstate ystate = ystate_;

  u.makePhysical();

  switch (k) {
  case 0:
    os << "% yz slice\n";
    os << "% (i,j)th elem is field at (x_n, y_i, z_j) with x_n is fixed\n";
    for (int ny=0; ny<Ny_; ny += ystride) {
      for (int nz=0; nz<Nz_; nz += zstride)
	os << u(nk, ny, nz, i) << ' ';
      os << u(nk, ny, 0, i) << '\n';  // repeat nz=0 to complete [0, Lz]
    }
    break;
  case 1: // xz slice
    os << "% xz slice\n";
    os << "% (i,j)th elem is field at (x_j, y_n, z_i) with y_n fixed\n";
    for (int nz=0; nz<Nz_; nz += zstride) {
      for (int nx=0; nx<Nx_; nx += xstride)
	os << u(nx, nk, nz, i) << ' ';
      os << u(0, nk, nz, i) << '\n'; // repeat nx=0 to complete [0, Lx]
    }
    for (int nx=0; nx<Nx_; ++nx)     // repeat nz=0 to complete [0, Lz]
      os << u(nx, nk, 0, i) << ' ';
    os << u(0, nk, 0, i) << '\n';    // repeat nx=0 to complete [0, Lx]

    break;
  case 2:
    os << "% xy slice\n";
    os << "% (i,j)th elem is field at (x_j, y_i, z_n)\n";
    for (int ny=0; ny<Ny_; ny += ystride) {
      for (int nx=0; nx<Nx_; nx += xstride)
	os << u(nx, ny, nk, i) << ' ';
      os << u(0, ny, nk, i) << '\n';
    }
    break;
  }
  u.makeState(xzstate, ystate);
}

void FlowField::saveProfile(int mx, int mz, const string& filebase) const {
  ChebyTransform trans(Ny_);
  saveProfile(mx,mz,filebase, trans);
}

void FlowField::saveProfile(int mx, int mz, const string& filebase, const ChebyTransform& trans) const {
  assert(xzstate_ == Spectral);
  string filename(filebase);
  if (Nd_ == 3)
    filename += string(".bf");  // this convention is unfortunate, need to fix
  else
    filename += string(".asc");

  ofstream os(filename.c_str());
  os << setprecision(REAL_DIGITS);

  if (ystate_ == Physical) {
    for (int ny=0; ny<Ny_; ++ny) {
      for (int i=0; i<Nd_; ++i) {
	Complex c = (*this).cmplx(mx, ny, mz, i);
	os << Re(c) << ' ' << Im(c) << ' ';
      }
      os << '\n';
    }
  }
  else {
    ComplexChebyCoeff* f = new ComplexChebyCoeff[Nd_];
    for (int i=0; i<Nd_; ++i) {
      f[i] = ComplexChebyCoeff(Ny_, a_, b_, Spectral);
      for (int ny=0; ny<Ny_; ++ny)
	f[i].set(ny, (*this).cmplx(mx, ny, mz, i));
      f[i].makePhysical(trans);
    }
    for (int ny=0; ny<Ny_; ++ny) {
      for (int i=0; i<Nd_; ++i)
	os << f[i].re[ny] << ' ' << f[i].im[ny] << ' ';
      os << '\n';
    }
  }
}
void FlowField::saveSpectrum(const string& filebase, int i, int ny,
			     bool kxorder, bool showpadding) const {
  string filename(filebase);
  filename += string(".asc");
  ofstream os(filename.c_str());

  bool sum = (ny == -1) ? true : false;
  assert(xzstate_ == Spectral);
  
  if (kxorder) {
    int Kxmin = (showpadding || !padded_) ? kxmin() : kxminDealiased();
    int Kxmax = (showpadding || !padded_) ? kxmax() : kxmaxDealiased();
    int Kzmin = (showpadding || !padded_) ? kzmin() : kzminDealiased();
    int Kzmax = (showpadding || !padded_) ? kzmax() : kzmaxDealiased();
    for (int kx=Kxmin; kx<=Kxmax; ++kx) {
      for (int kz=Kzmin; kz<=Kzmax; ++kz) {
	if (sum)
	  os << sqrt(energy(mx(kx),mz(kz))) << ' ';
	else {
	  Complex f = this->cmplx(mx(kx), ny, mz(kz), i);
	  os << Re(f) << ' ' << Im(f) << ' ';
	}
      }
      os << endl;
    }
  }
  else {
    for (int mx=0; mx<Mx(); ++mx) {
      for (int mz=0; mz<Mz(); ++mz) {
	if (sum)
	  os << sqrt(energy(mx,mz)) << ' ';
	else {
	  Complex f = this->cmplx(mx, ny, mz, i);
	  os << Re(f) << ' ' << Im(f) << ' ';
	}
      }
      os << endl;
    }
  }
  os.close();
}

void FlowField::saveSpectrum(const string& filebase, bool kxorder,
			     bool showpadding) const {
  string filename(filebase);
  filename += string(".asc");
  ofstream os(filename.c_str());

  assert(xzstate_ == Spectral && ystate_ == Spectral);

  ComplexChebyCoeff u(Ny_,a_,b_,Spectral);

  if (kxorder) {
    int Kxmin = (showpadding || !padded_) ? kxmin() : kxminDealiased();
    int Kxmax = (showpadding || !padded_) ? kxmax() : kxmaxDealiased();
    int Kzmin = (showpadding || !padded_) ? kzmin() : kzminDealiased();
    int Kzmax = (showpadding || !padded_) ? kzmax() : kzmaxDealiased();
    for (int kx=Kxmin; kx<=Kxmax; ++kx) {
      for (int kz=Kzmin; kz<=Kzmax; ++kz) {
	Real e = 0.0;
	for (int i=0; i<Nd_; ++i) {
	  for (int ny=0; ny<Ny_; ++ny)
	    u.set(ny,this->cmplx(mx(kx),ny,mz(kz),i));
	  e += L2Norm2(u);
	}
	os << sqrt(e) << ' ';
      }
      os << endl;
    }
  }
  else {
    for (int mx=0; mx<Mx(); ++mx) {
      for (int mz=0; mz<Mz(); ++mz) {
	Real e = 0.0;
	for (int i=0; i<Nd_; ++i) {
	  for (int ny=0; ny<Ny_; ++ny)
	    u.set(ny,this->cmplx(mx,ny,mz,i));
	  e += L2Norm2(u);
	}
	os << sqrt(e) << ' ';
      }
      os << endl;
    }
  }
  os.close();
}


void FlowField::saveDivSpectrum(const string& filebase, bool kxorder,
				bool showpadding) const {
  string filename(filebase);
  filename += string(".asc");
  ofstream os(filename.c_str());

  assert(xzstate_ == Spectral && ystate_ == Spectral);

  //assert(congruent(div));
  assert(Nd_ >= 3);

  ComplexChebyCoeff v(Ny_, a_, b_, Spectral);
  ComplexChebyCoeff vy(Ny_, a_, b_, Spectral);

  // Rely on compiler to pull loop invariants out of loops
  if (kxorder) {
    int Kxmin = kxmin();
    int Kxmax = (showpadding || !padded_) ? kxmax() : kxmaxDealiased();
    int Kzmin = (showpadding || !padded_) ? kzmin() : kzminDealiased();
    int Kzmax = (showpadding || !padded_) ? kzmax() : kzmaxDealiased();
    for (int kx=Kxmin; kx<Kxmax; ++kx) {
      Complex d_dx(0.0, 2*pi*kx/Lx_*zero_last_mode(kx,kxmax(),1));

      for (int kz=Kzmin; kz<Kzmax; ++kz) {
	Complex d_dz(0.0, 2*pi*kz/Lz_*zero_last_mode(kz,kzmax(),1));

	for (int ny=0; ny<Ny_; ++ny)
	  v.set(ny, cmplx(mx(kx),ny,mz(kz),1));
	diff(v,vy);

	Real div = 0.0;
	for (int ny=0; ny<Ny_; ++ny) {
	  Complex ux = d_dx*cmplx(mx(kx),ny,mz(kz),0);
	  Complex wz = d_dz*cmplx(mx(kx),ny,mz(kz),2);
	  div += abs2(ux + vy[ny] + wz);
	}
	os << sqrt(div) << ' ';
      }
      os << endl;
    }
  }
  else {
    for (int mx=0; mx<Mx(); ++mx) {
      Complex d_dx(0.0, 2*pi*kx(mx)/Lx_*zero_last_mode(kx(mx),kxmax(),1));

      for (int mz=0; mz<Mz(); ++mz) {
	Complex d_dz(0.0, 2*pi*kz(mz)/Lz_*zero_last_mode(kz(mz),kzmax(),1));

	for (int ny=0; ny<Ny_; ++ny)
	  v.set(ny, cmplx(mx,ny,mz,1));
	diff(v,vy);

	Real div = 0.0;
	for (int ny=0; ny<Ny_; ++ny) {
	  Complex ux = d_dx*cmplx(mx,ny,mz,0);
	  Complex wz = d_dz*cmplx(mx,ny,mz,2);
	  div += abs2(ux + vy[ny] + wz);
	}
	os << sqrt(div) << ' ';
      }
      os << endl;
    }
  }
  os.close();
}

void FlowField::asciiSave(const string& filebase) const {
  string filename(filebase);
  filename += ".asc";

  ofstream os(filename.c_str());
  os << scientific << setprecision(REAL_DIGITS);

  const char s = ' ';
  const char nl = '\n';
  const int w = REAL_IOWIDTH;
  os << "% Channelflow FlowField data" << nl;
  os << "% xzstate == " << xzstate_ << nl;
  os << "%  ystate == " << ystate_ << nl;
  os << "% Nx Ny Nz Nd == " << Nx_ <<s<< Ny_ <<s<< Nz_ <<s<< Nd_ << " gridpoints\n";
  os << "% Mx My Mz Nd == " << Mx() <<s<< My() <<s<< Mz() <<s<< Nd_ << " spectral modes\n";
  os << "% Lx Lz == " << setw(w) << Lx_ <<s<< setw(w) << Lz_ << nl;
  os << "% Lx Lz == " << setw(w) << Lx_ <<s<< setw(w) << Lz_ << nl;
  os << "% a  b  == " << setw(w) << a_ <<s<< setw(w) << b_ << nl;
  os << "% loop order:\n";
  os << "%   for (int i=0; i<Nd; ++i)\n";
  os << "%     for(long ny=0; ny<Ny; ++ny) // note: Ny == My\n";
  if (xzstate_ == Physical) {
    os << "%       for (int nx=0; nx<Nx; ++nx)\n";
    os << "%         for (int nz=0; nz<Nz; ++nz)\n";
    os << "%           os << f(nx, ny, nz, i) << newline;\n";
  }
  else {
    os << "%       for (int mx=0; mx<Mx; ++mx)\n";
    os << "%         for (int mz=0; mz<Mz; ++mz)\n";
    os << "%           os << Re(f.cmplx(mx, ny, mz, i) << ' ' << Im(f.cmplx(mx, ny, mz, i) << newline;\n";
  }
  if (xzstate_ == Physical) {
    for (int i=0; i<Nd_; ++i)
      for(long ny=0; ny<Ny_; ++ny)
	for (int nx=0; nx<Nx_; ++nx)
	  for (int nz=0; nz<Nz_; ++nz)
	    os << setw(w) << (*this)(nx, ny, nz, i) << nl;
  }
  else {
    for (int i=0; i<Nd_; ++i)
      for(long ny=0; ny<Ny_; ++ny)
	for (int mx=0; mx<Mx(); ++mx)
	  for (int mz=0; mz<Mz(); ++mz)
	    os << setw(w) << Re(cmplx(mx, ny, mz, i)) << s
	       << setw(w) << Im(cmplx(mx, ny, mz, i)) << nl;
  }
  os.close();
}

void FlowField::save(const string& filebase) const {

  string filename;

  bool h5suffix = hasSuffix(filebase, ".h5");
  bool ffsuffix = hasSuffix(filebase, ".ff");

  if (ffsuffix)
    binarySave(filebase);

  else if (h5suffix)
    if (HAVE_HDF5)
      hdf5Save(filebase);
    else {
      cerr << "FlowField::save(filename) error : can't save to HDF5 file" << endl;
      cerr << "because HDF5 libraries are not installed. filename == " << filebase << endl;
      exit(1);
    }
  else
    if (HAVE_HDF5)
      hdf5Save(filebase);
    else
      binarySave(filebase);
}



void FlowField::binarySave(const string& filebase) const {

  /************************************
  string filename;
  if (filebase.length() >
      3 && filebase.substr(filebase.length()-3, 3) == ".ff")
    filename = filebase;
  else
    filename += string(".ff"); // "flow field"
  *******************************/
  string filename = appendSuffix(filebase, ".ff");

  ofstream os(filename.c_str(), ios::out | ios::binary);
  if (!os.good()) {
    cerr << "FlowField::binarySave(filebase) : can't open file " << filename << endl;
    exit(1);
  }

  int major;
  int minor;
  int update;
  channelflowVersion(major, minor, update);

  write(os, major);
  write(os, minor);
  write(os, update);
  write(os, Nx_);
  write(os, Ny_);
  write(os, Nz_);
  write(os, Nd_);
  write(os, xzstate_);
  write(os, ystate_);
  write(os, Lx_);
  write(os, Lz_);
  write(os, a_);
  write(os, b_);
  write(os, padded_);

  // Write data only for non-aliased modes.
  if (padded_ && xzstate_ == Spectral) {
    int Nxd=2*(Nx_/6);
    int Nzd=2*(Nz_/3)+1;

    // In innermost loop, array index is (nz + Nzpad2_*(nx + Nx_*(ny + Ny_*i))),
    // which is the same as the FlowField::flatten function.
    for (int i=0; i<Nd_; ++i) {
      for (int ny=0; ny<Ny_; ++ny) {

	for (int nx=0; nx<=Nxd; ++nx) {
	  for (int nz=0; nz<=Nzd; ++nz)
	    write(os, rdata_[flatten(nx,ny,nz,i)]);
	}
	for (int nx=Nx_-Nxd; nx<Nx_; ++nx) {
	  for (int nz=0; nz<=Nzd; ++nz)
	    write(os, rdata_[flatten(nx,ny,nz,i)]);
	}
      }
    }
  }
  else {
    int Ntotal = Nd_* Nx_ * Ny_ * Nzpad_;
    for (int i=0; i<Ntotal; ++i)
      write(os, rdata_[i]);
  }
}



#ifndef HAVE_HDF5

void FlowField::hdf5Save(const string& filebase) const {
   cferror("FlowField::hdf5save requires HDF5 libraries. Please install them and recompile channelflow.");
}

#else
void FlowField::hdf5Save(const string& filebase) const {
  H5std_string h5name = appendSuffix(filebase, ".h5");
  H5::H5File h5file(h5name, H5F_ACC_TRUNC);

  // create the groups we will need
  h5file.createGroup("/geom");
  h5file.createGroup("/data");

  // If this FlowField is padded (last 1/3 x,z modes are set to zero)
  // transfer to nopadded grid and save that
  if (this->padded()) {
    int Nx = (2*Nx_)/3;
    int Nz = (2*Nz_)/3;
    //cout << "HDF5 write " << endl;
    //cout << "HDF5 Nx Ny Nz == " << Nx << " " << Ny_ << " " << Nz << endl;
    //cout << "pad  Nx Ny Nz == " << Nx_ << " " << Ny_ << " " << Nz_ << endl;
    //cout << "padded == " << padded_ << endl;
    FlowField v(Nx, Ny_, Nz, Nd_, Lx_, Lz_, a_, b_);
    v.interpolate(*this);
    v.makePhysical();

    hdf5write(v.xgridpts(), "/geom/x", h5file);
    hdf5write(v.ygridpts(), "/geom/y", h5file);
    hdf5write(v.zgridpts(), "/geom/z", h5file);
    hdf5write(Nx,  "Nx", h5file);
    hdf5write(Ny_, "Ny", h5file);
    hdf5write(Nz,  "Nz", h5file);
    hdf5write(Nd_, "Nd", h5file);
    hdf5write(Nx_, "Nxpad", h5file);
    hdf5write(Ny_, "Nypad", h5file);
    hdf5write(Nz_, "Nzpad", h5file);
    hdf5write(v, "/data/u", h5file);
  }
  else {
    FlowField& u = (FlowField&) *this; // cast off constness
    fieldstate xzs = xzstate_;
    fieldstate ys =  ystate_;
    u.makePhysical();

    hdf5write(u.xgridpts(), "/geom/x", h5file);
    hdf5write(u.ygridpts(), "/geom/y", h5file);
    hdf5write(u.zgridpts(), "/geom/z", h5file);
    hdf5write(Nx_, "Nx", h5file);
    hdf5write(Ny_, "Ny", h5file);
    hdf5write(Nz_, "Nz", h5file);
    hdf5write(Nd_, "Nd", h5file);
    hdf5write(Nx_, "Nxpad", h5file);
    hdf5write(Ny_, "Nypad", h5file);
    hdf5write(Nz_, "Nzpad", h5file);
    hdf5write(u, "/data/u", h5file);

    u.makeState(xzs, ys);
  }
  hdf5write(Lx_, "Lx", h5file);
  hdf5write(Lz_, "Lz", h5file);
  hdf5write(a_,  "a",  h5file);
  hdf5write(b_,  "b",  h5file);
  hdf5write(nu_, "nu", h5file);
}
#endif // HAVE_HDF5

//==================================================================================

void FlowField::dump() const {
  for (int i=0; i<Nx_ * Ny_ * Nzpad_ * Nd_; ++i)
    cout << rdata_[i] << ' ';
  cout << endl;
}

Real FlowField::energy(bool normalize) const {
  assert(xzstate_ == Spectral && ystate_ == Spectral);
  return L2Norm2(*this, normalize);
}

Real FlowField::nu() const {
  return nu_;
}

void FlowField::setnu(Real gnu) {
  nu_ = gnu;
}

Real FlowField::energy(int mx, int mz, bool normalize) const {
  assert(xzstate_ == Spectral && ystate_ == Spectral);
  ComplexChebyCoeff u(Ny_,a_,b_,Spectral);
  Real e = 0.0;
  for (int i=0; i<Nd_; ++i) {
    for (int ny=0; ny<Ny_; ++ny)
      u.set(ny,this->cmplx(mx,ny,mz,i));
    e += L2Norm2(u, normalize);
  }
  if (!normalize)
    e *= Lx_*Lz_;
  return e;
}

Real FlowField::dudy_a() const {
  assert(ystate_ == Spectral);
  BasisFunc prof = profile(0,0);
  ChebyCoeff dudy = diff(Re(prof.u()));
  return dudy.eval_a();
}
Real FlowField::dudy_b() const {
  assert(ystate_ == Spectral);
  BasisFunc prof = profile(0,0);
  ChebyCoeff dudy = diff(Re(prof.u()));
  return dudy.eval_b();
}

Real FlowField::CFLfactor() const {
  assert(Nd_ == 3);
  FlowField& u = (FlowField&) *this;
  fieldstate xzstate = xzstate_;
  fieldstate ystate = ystate_;
  u.makePhysical();
  Vector y = chebypoints(Ny_, a_, b_);
  Real cfl = 0.0;
  Vector dx(3);
  dx[0] = Lx_/Nx_;
  dx[2] = Lz_/Nz_;
  for (int i=0; i<Nd_; ++i)
    for (int ny=0; ny<Ny_; ++ny) {
      dx[1] = (ny==0 || ny==Ny_-1) ? y[0]-y[1] : (y[ny-1]-y[ny+1])/2.0;
      for (int nx=0; nx<Nx_; ++nx)
	for (int nz=0; nz<Nz_; ++nz)
	  cfl = Greater(cfl, abs(u(nx,ny,nz,i)/dx[i]));
  }
  u.makeState(xzstate,ystate);

  return cfl;
}

Real FlowField::CFLfactor(const ChebyCoeff& Ubase_, const ChebyCoeff& Wbase_) const {
  if (Ubase_.N() == 0)
    return CFLfactor();

  assert(Nd_ == 3);
  FlowField& u = const_cast<FlowField&>(*this);
  ChebyCoeff& Ubase = const_cast<ChebyCoeff&>(Ubase_);
  ChebyCoeff& Wbase = const_cast<ChebyCoeff&>(Wbase_);

  fieldstate xzstate = xzstate_;
  fieldstate ystate = ystate_;
  fieldstate Ustate = Ubase.state();
  fieldstate Wstate = Wbase.state();
  u.makePhysical();
  Ubase.makePhysical();
  Wbase.makePhysical();
  Vector y = chebypoints(Ny_, a_, b_);
  Real cfl = 0.0;
  Vector dx(3);
  dx[0] = Lx_/Nx_;
  dx[2] = Lz_/Nz_;
  for (int i=0; i<3; ++i)
    for (int ny=0; ny<Ny_; ++ny) {
      dx[1] = (ny==0 || ny==Ny_-1) ? y[0]-y[1] : (y[ny-1]-y[ny+1])/2.0;
      Real Ui = 0.0;
      if (i==0) Ui = Ubase(ny);
      else if (i==2) Ui = Wbase(ny);
      for (int nx=0; nx<Nx_; ++nx)
	for (int nz=0; nz<Nz_; ++nz)
	  cfl = Greater(cfl, (u(nx,ny,nz,i)+Ui)/dx[i]);
    }
  u.makeState(xzstate,ystate);
  Ubase.makeState(Ustate);
  Wbase.makeState(Wstate);

  return cfl;
}


/********************************************************
Real FlowField::dissipation(int mx, int mz, Real nu) const {
  assert(xzstate_ == Spectral && ystate_== Spectral);
  BasisFunc prof;
  Real d=0.0;
  for (int _=0; _<Nd_; ++_) {
    prof = profile(mx, mz);
    d += nu*Re(L2InnerProduct(prof, ::laplacian(prof), false));
  }
  return d;
}
***********************************************************/

void FlowField::setState(fieldstate xz, fieldstate y) {
  xzstate_ = xz;
  ystate_ = y;
}
void FlowField::assertState(fieldstate xz, fieldstate y) const {
  assert(xzstate_ == xz && ystate_ == y);
}

void swap(FlowField& f, FlowField& g) {
  assert(f.congruent(g));
  Real* tmp = f.rdata_;
  f.rdata_ = g.rdata_;
  g.rdata_ = tmp;

  // An odd end: nu isn't part of geometry
  Real stmp = f.nu_;
  f.nu_ = g.nu_;
  g.nu_ = stmp;

  f.cdata_ = (Complex*)f.rdata_;
  g.cdata_ = (Complex*)g.rdata_;

  // We must swap the FFTW3 plans, as well, since they're tied to specific
  // memory locations. This burned me when I upgraded FFTW2->FFTW3.
  fftw_plan tmp_plan;
  tmp_plan = f.xz_plan_;
  f.xz_plan_ = g.xz_plan_;
  g.xz_plan_ = tmp_plan;

  tmp_plan = f.xz_iplan_;
  f.xz_iplan_ = g.xz_iplan_;
  g.xz_iplan_ = tmp_plan;

  tmp_plan = f.y_plan_;
  f.y_plan_ = g.y_plan_;
  g.y_plan_ = tmp_plan;
}

void orthonormalize(array<FlowField>& e) {

  // Modified Gram-Schmidt orthogonalization
  int N = e.length();
  FlowField em_tmp;

  for (int m=0; m<N; ++m) {
    FlowField& em = e[m];
    em *= 1.0/L2Norm(em);
    em *= 1.0/L2Norm(em); // reduces floating-point errors

    // orthogonalize
    for (int n=m+1; n<N; ++n) {
      em_tmp = em;
      em_tmp *= L2InnerProduct(em, e[n]);
      e[n] -= em_tmp;
    }
  }
  return;
}

void normalize(array<FlowField>& e) {
  int N = e.length();
  for (int m=0; m<N; ++m) {
    FlowField& em = e[m];
    em *= 1.0/L2Norm(em);
    em *= 1.0/L2Norm(em);
  }
  return;
}

void orthogonalize(array<FlowField>& e) {
  // Modified Gram-Schmidt orthogonalization
  int N = e.length();
  FlowField em_tmp;

  for (int m=0; m<N; ++m) {
    FlowField& em = e[m];
    Real emnorm2 = L2Norm2(em);
    for (int n=m+1; n<N; ++n) {
      em_tmp = em;
      em_tmp *= L2InnerProduct(em, e[n])/emnorm2;
      e[n] -= em_tmp;
    }
  }
  return;
}

FlowField quadraticInterpolate(array<FlowField>& un, array<Real>& mun, Real mu, Real eps) {
 // Extrapolate indpt param R and box params as function of mu

  if (un.length() != 3 || mun.length() !=3) {
    cerr << "error in quadraticInterpolate(array<FlowField>& un, array<Real>& mun, Real mu, Real eps)\n";
    cerr << "un.length() != 3 || mun.length() !=3\n";
    cerr << "un.length()  == " << un.length() << '\n';
    cerr << "mun.length() == " << mun.length() << '\n';
    cerr << "exiting" << endl;
    exit(1);
  }
  const int Nx = un[0].Nx();
  const int Ny = un[0].Ny();
  const int Nz = un[0].Nz();
  const int Nd = un[0].Nd();

  if (un[1].Nx() != Nx || un[2].Nx() != Nx ||
      un[1].Ny() != Ny || un[2].Ny() != Ny ||
      un[1].Nz() != Nz || un[2].Nz() != Nz ||
      un[1].Nd() != Nd || un[2].Nd() != Nd) {
    cerr << "error in quadraticInterpolate(array<FlowField>& un, array<Real>& mun, Real mu, Real eps)\n";
    cerr << "Incompatible grids in un. Exiting" << endl;
    exit(1);
  }

  array<fieldstate> xstate(3);
  array<fieldstate> ystate(3);

  for (int n=0; n<3; ++n) {
    xstate[n] = un[n].xzstate();
    ystate[n] = un[n].ystate();
  }
  for (int i=0; i<3; ++i)
    un[i].makePhysical();

  array<Real> fn(3);

  for (int i=0; i<3; ++i)
    fn[i] = un[i].Lx();
  const Real Lx = isconst(fn) ? fn[0] : quadraticInterpolate(fn, mun, mu);

  for (int i=0; i<3; ++i)
    fn[i] = un[i].Lz();
  const Real Lz = isconst(fn) ? fn[0] : quadraticInterpolate(fn, mun, mu);

  for (int i=0; i<3; ++i)
    fn[i] = un[i].a();
  const Real a = isconst(fn) ? fn[0] : quadraticInterpolate(fn, mun, mu);

  for (int i=0; i<3; ++i)
    fn[i] = un[i].b();
  const Real b = isconst(fn) ? fn[0] : quadraticInterpolate(fn, mun, mu);

  // Extrapolate gridpoint values as function of mu
  FlowField u(Nx, Ny, Nz, Nd, Lx,Lz,a,b, Physical,Physical);
  //u.makeState(Physical,Physical);
  //ug.binarySave("uginit");

  for (int i=0; i<Nd; ++i)
    for (int ny=0; ny<Ny; ++ny)
	for (int nx=0; nx<Nx; ++nx)
	  for (int nz=0; nz<Nz; ++nz) {
	    for (int n=0; n<3; ++n)
	      fn[n] = un[n](nx,ny,nz,i);
	    u(nx,ny,nz,i) = isconst(fn, eps) ? fn[0] : quadraticInterpolate(fn, mun, mu);
	  }

  for (int i=0; i<3; ++i)
    un[i].makeState(xstate[i],ystate[i]);

  u.makeSpectral();
  if (u[0].padded() && u[1].padded() && u[2].padded())
    u.zeroPaddedModes();
  return u;
}

FlowField polynomialInterpolate(array<FlowField>& un, array<Real>& mun, Real mu) {
  // Extrapolate indpt param R and box params as function of mu

  if (un.length() != mun.length() || un.length() < 1) {
    cerr << "error in quadraticInterpolate(array<FlowField>& un, array<Real>& mun, Real mu, Real eps)\n";
    cerr << "un.length() != mun.length() or un.length() < 1\n";
    cerr << "un.length()  == " << un.length() << '\n';
    cerr << "mun.length() == " << mun.length() << '\n';
    cerr << "exiting" << endl;
    exit(1);
  }
  const int N  = un.length();
  const int Nx = un[0].Nx();
  const int Ny = un[0].Ny();
  const int Nz = un[0].Nz();
  const int Nd = un[0].Nd();

  if (un[1].Nx() != Nx || un[2].Nx() != Nx ||
      un[1].Ny() != Ny || un[2].Ny() != Ny ||
      un[1].Nz() != Nz || un[2].Nz() != Nz ||
      un[1].Nd() != Nd || un[2].Nd() != Nd) {
    cerr << "error in quadraticInterpolate(array<FlowField>& un, array<Real>& mun, Real mu, Real eps)\n";
    cerr << "Incompatible grids in un. Exiting" << endl;
    exit(1);
  }

  array<fieldstate> xstate(N);
  array<fieldstate> ystate(N);

  for (int n=0; n<N; ++n) {
    xstate[n] = un[n].xzstate();
    ystate[n] = un[n].ystate();
  }
  for (int i=0; i<N; ++i)
    un[i].makePhysical();

  array<Real> fn(N);

  for (int i=0; i<N; ++i)
    fn[i] = un[i].Lx();
  const Real Lx = polynomialInterpolate(fn, mun, mu);

  for (int i=0; i<N; ++i)
    fn[i] = un[i].Lz();
  const Real Lz = polynomialInterpolate(fn, mun, mu);

  for (int i=0; i<N; ++i)
    fn[i] = un[i].a();
  const Real a = polynomialInterpolate(fn, mun, mu);

  for (int i=0; i<N; ++i)
    fn[i] = un[i].b();
  const Real b = polynomialInterpolate(fn, mun, mu);

  // Extrapolate gridpoint values as function of mu
  FlowField u(Nx, Ny, Nz, Nd, Lx,Lz,a,b, Physical,Physical);
  //u.makeState(Physical,Physical);
  //ug.binarySave("uginit");

  for (int i=0; i<Nd; ++i)
    for (int ny=0; ny<Ny; ++ny)
	for (int nx=0; nx<Nx; ++nx)
	  for (int nz=0; nz<Nz; ++nz) {
	    for (int n=0; n<3; ++n)
	      fn[n] = un[n](nx,ny,nz,i);
	    u(nx,ny,nz,i) = polynomialInterpolate(fn, mun, mu);
	  }

  for (int i=0; i<N; ++i)
    un[i].makeState(xstate[i],ystate[i]);

  u.makeSpectral();
  if (u[0].padded())
    u.zeroPaddedModes();
  return u;
}


//=======================================================================
#ifdef HAVE_HDF5

void hdf5write(int i, const string& name, H5::H5File& h5file) {
  herr_t status = H5Tconvert(H5T_NATIVE_INT, H5T_STD_I32BE, 1, &i, NULL, H5P_DEFAULT);
  if (status <0) {
    cout << "HDF5 datatype conversion failed! Status: " << status;
  }

  H5::Group rootgr = h5file.openGroup("/");
  H5::DataSpace dspace = H5::DataSpace(H5S_SCALAR);
  H5::Attribute attr = rootgr.createAttribute(name.c_str(), H5::PredType::STD_I32BE, dspace);
  attr.write(H5::PredType::STD_I32BE, &i);
}

void hdf5write(Real x, const string& name, H5::H5File& h5file) {
  herr_t status = H5Tconvert(H5T_NATIVE_DOUBLE, H5T_IEEE_F64BE, 1, &x, NULL, H5P_DEFAULT);
  if (status <0) {
    cout << "HDF5 datatype conversion failed! Status: " << status;
  }

  H5::Group rootgr = h5file.openGroup("/");
  H5::DataSpace dspace = H5::DataSpace(H5S_SCALAR);
  H5::Attribute attr = rootgr.createAttribute(name.c_str(), H5::PredType::IEEE_F64BE, dspace);
  attr.write(H5::PredType::IEEE_F64BE, &x);
}

void hdf5write(const Vector& v, const string& name, H5::H5File& h5file) {

  const int N = v.length();
  const hsize_t hnx[1] = {hsize_t(N)};

  Real* data = new Real[N];
  for (int n=0; n<N; ++n)  // need a tmp array for native -> ieee conversion
    data[n] = v(n);

  // create H5 dataspace, convert from native double to 64-bit IEE
  H5::DataSpace dspace(1, hnx);
  H5::DataSet dset = h5file.createDataSet(name.c_str(), H5::PredType::IEEE_F64BE, dspace);
  herr_t status = H5Tconvert(H5T_NATIVE_DOUBLE, H5T_IEEE_F64BE, N, data, NULL, H5P_DEFAULT);

  if (status <0) {
    cout << "HDF5 datatype conversion failed! Status: " << status;
  }
  dset.write(data, H5::PredType::IEEE_F64BE);
  dset.close();
  delete data;
}

void hdf5write(const FlowField& u, const string& name, H5::H5File& h5file) {

  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();
  const int Nd = u.Nd();

  const hsize_t hNx = hsize_t(u.Nx());
  const hsize_t hNy = hsize_t(u.Ny());
  const hsize_t hNz = hsize_t(u.Nz());
  const hsize_t hNd = hsize_t(u.Nd());

  double* data = new double[Nx*Ny*Nz*Nd]; // create array of data to be written

  for (int nx=0; nx<Nx; ++nx)
    for (int ny=0; ny<Ny; ++ny)
      for (int nz=0; nz<Nz; ++nz)
	for (int i=0; i<Nd; ++i)
	  data[nz + Nz*(ny + Ny*(nx + Nx*i))] = u(nx,ny,nz,i);
  //cout <<"data done \n";
  const hsize_t dimsf[4]   = {hNd,hNz,hNy,hNx};   // dataset dimensions
  //const hsize_t dimsf[4] = {Nd,Nx,Ny,Nz};   // dataset dimensions  
  const int dsize = Nx*Ny*Nz*Nd;

  H5::DataSpace dspace(4, dimsf);  // create dataspace
  H5::DataSet dset = h5file.createDataSet(name.c_str(), H5::PredType:: IEEE_F64BE, dspace);
  herr_t status = H5Tconvert(H5T_NATIVE_DOUBLE, H5T_IEEE_F64BE, dsize, data, NULL, H5P_DEFAULT);
  if (status <0) {
    cout << "HDF5 datatype conversion failed! Status: " << status;
  }
  dset.write(data, H5::PredType::IEEE_F64BE);  // write data to dataset
  dset.close();
  delete data;
}

bool hdf5query(const string& name, H5::H5File& h5file) {
  // Yucky. HDF5 does not have a "does attribute exist?" function,
  // so we have to loop over all attributes and test.
  H5::Group rootgr = h5file.openGroup("/");
  const uint N = rootgr.getNumAttrs();

  //cout << "looking for attribute name == " << name << endl;
  for (uint i=0; i<N; ++i) {
    H5::Attribute attr = rootgr.openAttribute(i);
    //cout << "attr name " << i << " == " << attr.getName() << endl;
    if (attr.getName() == name)
      return true;
  }
  return false;
}

void hdf5read(int& i, const string& name, H5::H5File& h5file) {
  H5::Group rootgr = h5file.openGroup("/");
  H5::Attribute attr = rootgr.openAttribute(name.c_str());
  attr.read(H5::PredType::STD_I32BE, &i);
  H5Tconvert(H5T_STD_I32BE, H5T_NATIVE_INT, 1, &i, NULL, H5P_DEFAULT);
  //herr_t status = H5Tconvert(H5T_STD_I32BE, H5T_NATIVE_INT, 1, &i, NULL, H5P_DEFAULT);

}

void hdf5read(Real& x, const string& name, H5::H5File& h5file) {
  H5::Group rootgr = h5file.openGroup("/");
  H5::Attribute attr = rootgr.openAttribute(name.c_str());
  attr.read(H5::PredType:: IEEE_F64BE, &x);
  H5Tconvert(H5T_IEEE_F64BE, H5T_NATIVE_DOUBLE, 1, &x, NULL, H5P_DEFAULT);
  //herr_t status = H5Tconvert(H5T_IEEE_F64BE, H5T_NATIVE_DOUBLE, 1, &x, NULL, H5P_DEFAULT);
}

void hdf5read(FlowField& u, const string& name, H5::H5File& h5file) {

  const uint Nx = u.Nx();
  const uint Ny = u.Ny();
  const uint Nz = u.Nz();
  const uint Nd = u.Nd();

  H5::DataSet dataset = h5file.openDataSet(name.c_str());
  H5::DataSpace dataspace = dataset.getSpace();
  //int rank = dataspace.getSimpleExtentNdims();
  //hsize_t Nh5[4]; // Nd x Nx x Ny x Nz dimensions of h5 data array
  hsize_t Nh5[4];   // Nd x Nz x Ny x Nx dimensions of h5 data array
  int rank = dataspace.getSimpleExtentDims(Nh5, NULL);

  if (rank != 4)
    cferror("error reading FlowField from hdf5 file : rank of data is not 4");

  /******************************************************************************************
  if (Nd != Nh5[0] || Nz != Nh5[1] || Ny != Nh5[2] || Nx != Nh5[3]) {
    cerr << "error in hdf5read(FlowField, varname, h5file) : " << endl;
    cerr << "dimensionality mismatch between attributes Nx,Ny,Nz,Nd and data array in h5file" << endl;
    exit(1);
  }
  ********************************************************************************************/

  Nh5[0] = Nd;
  Nh5[1] = Nz;
  Nh5[2] = Ny;
  Nh5[3] = Nx;

  H5::DataSpace memspace(4, Nh5);

  int N = Nx*Ny*Nz*Nd;
  Real* buff = new Real[N];
  dataset.read(buff, H5::PredType::IEEE_F64BE, memspace, dataspace);

  //herr_t status = H5Tconvert(H5T_IEEE_F64BE, H5T_NATIVE_DOUBLE, N, buff, NULL, H5P_DEFAULT);
  H5Tconvert(H5T_IEEE_F64BE, H5T_NATIVE_DOUBLE, N, buff, NULL, H5P_DEFAULT);

  u.setState(Physical, Physical);
  for (uint nz=0; nz<Nz; ++nz)
    for (uint ny=0; ny<Ny; ++ny)
      for (uint nx=0; nx<Nx; ++nx)
	for (uint i=0; i<Nd; ++i)
	  u(nx,ny,nz,i) = buff[nz + Nz*(ny + Ny*(nx + Nx*i))];

  u.makeSpectral();
  dataset.close();
  delete buff;
}

void hdf5addstuff(const std::string& filebase, Real nu, ChebyCoeff Ubase, ChebyCoeff Wbase) {
  H5std_string h5name = appendSuffix(filebase, ".h5");
  H5::H5File h5file(h5name, H5F_ACC_TRUNC);
  h5file.createGroup("/extras");  
  
  Ubase.makePhysical();
  Wbase.makePhysical();
  hdf5write(nu, "/extras/nu", h5file);
  hdf5write(Ubase, "/extras/Ubase", h5file);
  hdf5write(Wbase, "/extras/Wbase", h5file);
}

#endif // HAVE_HDF5


} //namespace channelflow
