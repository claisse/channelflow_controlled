/* utilfuncs.cpp: an assortment of mathematical functions convenient for
 * my research problems but probably irrelevant to everyone else
 *
 * channelflow-1.3, www.channelflow.org
 *
 * Copyright (C) 2001-2009  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu
 * Center for Nonlinear Sciences, School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */

#include <ctime>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "channelflow/utilfuncs.h"
#include "channelflow/periodicfunc.h"

using namespace std;

namespace channelflow {

bool fileExists(const string& filename) {
  struct stat st; 
  return (stat(filename.c_str(), &st) == 0) ? true : false;
}

// If s is numeric, convert to real using atof
// If s is alpha, try to open file s.asc
Real arg2real(const string& s) {
  Real rtn;
  if (fileExists(s)) 
    load(rtn, s);
  else
    rtn = atof(s.c_str());
  return rtn;
}

bool isconst(array<Real> f, Real eps) {
  if (f.length() == 0)
    return true;

  Real f0 = f[0];
  bool rtn = true;
  for (int n=0; n<f.length(); ++n)
    if (abs(f[n]-f0) > eps) {
      rtn = false;
      break;
    }
  return rtn;
}

Real  linearInterpolate(Real x0, Real y0, Real x1, Real y1, Real x) {
  return (y0*(x-x1) - y1*(x-x0)) / (x0 - x1);
}

// Neville algorithm, modeled after GPL v2 code from Google
Real polynomialInterpolate(const array<Real>& fn, const array<Real>& xn, Real x) {

  assert(fn.length() == xn.length());
  assert(fn.length() > 0);

  const int N=xn.length();
  const int Nbuffer=10;
  if (N>Nbuffer) {
    cerr << "error in polynomial Interpolate(array<Real> fn, array<Real> xn, Real x) :" << endl;
    cerr << "fn.length() == " << N << " > " << Nbuffer << " ==  static buffer size" << endl;
    cerr << "this function could be extended to use dynamic memory for such cases" << endl;
    exit(1);
  }

  if (isconst(fn))
    return fn[0];

  // If N is small, use a preallocated static buffer, else use dynamic memory
  Real y[2][Nbuffer];

  // Load input f data into y array
  for (int i=0; i<N; ++i)
    y[0][i] = fn[i];

  int j_curr=0;
  for (int i=1; i<N; i++) {
    j_curr = i%2;
    int j_prev = (i-1)%2;
    for (int k=0; k<(N-i); k++)
      y[j_curr][k] = linearInterpolate(xn[k] , y[j_prev][k] , xn[k+i] , y[j_prev][k+1] , x);
  }
  return y[j_curr][0];
}


// Neville algorithm, modled after GPL v2 code from Google
Real quadraticInterpolate(const array<Real>& fn, const array<Real>& xn, Real x) {
  assert(fn.N() >= 3);
  assert(xn.N() >= 3);
  if (fn[0] == fn[1] && fn[1]==fn[2])
    return fn[1];

  const int N=3; // three points for quadratic extrapolation
  Real y[2][N];  // two arrays for quadratic extrap recurrence relation

  // Load input f data into y array
  for (int i=0; i<N; ++i)
    y[0][i] = fn[i];

  int j_curr=0;
  for (int i=1; i<N; i++) {
    j_curr = i%2;
    int j_prev = (i-1)%2;
    for (int k=0; k<(N-i); k++)
      y[j_curr][k] = linearInterpolate(xn[k] , y[j_prev][k] , xn[k+i] , y[j_prev][k+1] , x);
  }
  return y[j_curr][0];
}

// Algorithm from Numerical Recipes in C pg 110;
Real polyInterp(const array<Real>& fa, const array<Real>& xa, Real x) {
  assert(fa.N() == xa.N());
  int N = fa.N();
  array<Real> C(N);
  array<Real> D(N);

  Real xdiff = abs(x-xa[0]);
  Real xdiff2 = 0.0;
  Real df;

  int i_closest = 0;
  for (int i=0; i<N; ++i) {
    if ((xdiff2 = abs(x-xa[i])) < xdiff) {
      i_closest = i;
      xdiff = xdiff2;
    }
    C[i] = fa[i];
    D[i] = fa[i];
  }

  // Initial approx to f(x)
  Real fx = fa[i_closest--];

  for (int m=1; m<N; ++m) {
    for (int i=0; i<N-m; ++i) {
      Real C_dx = xa[i]-x;
      Real D_dx = xa[i+m] - x;
      Real C_D = C[i+1]-D[i];
      Real denom = C_dx - D_dx;
      if (denom == 0.0)
	cferror("polyinterp(fa,xa,x) : two values of xa has are equal!");
      denom = C_D/denom;
      C[i] = C_dx*denom;
      D[i] = D_dx*denom;
    }
    fx += (df=(2*(i_closest+1) < (N-m)? C[i_closest+1] : D[i_closest--]));
  }
  return fx;
}

Real secantSearch(Real a, Real b, array<Real>& fn, const array<Real>& xn,
		  Real feps, int maxsteps) {
  Real fa = polyInterp(fn, xn, a);
  Real fb = polyInterp(fn, xn, b);
  if (fa*fb > 0)
    cferror("secantSearch(a,b,fn,xn) : a and b don't bracket a zero");

  Real c = a - fa * (b-a)/(fb-fa);
  Real fc;

  for (int n=0; n<maxsteps; ++n) {
    c = a - fa * (b-a)/(fb-fa);
    fc = polyInterp(fn,xn,c);
    if (abs(fc) < feps)
      break;
    if (fc*fa > 0) {
      a = c;
      fa = fc;
    }
    else {
      b = c;
      fb = fc;
    }
  }
  return c;
}


Real bisectSearch(Real a, Real b, array<Real>& fn, const array<Real>& xn,
		  Real feps, int maxsteps) {
  Real c = 0.5*(a+b)/2; // GCC-3.3.5 complains if not explicitly init'ed
  Real fc;
  Real fa = polyInterp(fn, xn, a);
  Real fb = polyInterp(fn, xn, b);
  if (fa*fb > 0)
    cferror("bisectSearch(a,b,fn,xn) : a and b don't bracket a zero");

  for (int n=0; n<maxsteps; ++n) {
    c = 0.5*(a+b);
    fc = polyInterp(fn,xn,c);
    if (abs(fc) < feps)
      break;
    if (fc*fa > 0) {
      a = c;
      fa = fc;
    }
    else {
      b = c;
      fb = fc;
    }
  }
  return c;
}

FieldSeries::FieldSeries()
  :
  t_(0),
  f_(0),
  emptiness_(0)
{}

FieldSeries::FieldSeries(int N)
  :
  t_(N),
  f_(N),
  emptiness_(N)
{}

//Still working on first draft of these functions


void FieldSeries::push(const FlowField& f, Real t) {
  for (int n=f_.N()-1; n>0; --n) {
    if (f_[n].congruent(f_[n-1]))
      swap(f_[n], f_[n-1]);
    else
      f_[n] = f_[n-1];
    t_[n] = t_[n-1];
  }
  if (f_.N() > 0) {
    f_[0] = f;
    t_[0] = t;
  }
  --emptiness_;
}

bool FieldSeries::full() const {
  return (emptiness_ == 0) ? true : false;
}

void FieldSeries::interpolate(FlowField& f, Real t) const {
  if (!(this->full()))
    cferror("FieldSeries::interpolate(Real t, FlowField& f) : FieldSeries is not completely initialized. Take more time steps before interpolating");

  for (int n=0; n<f_.N(); ++n) {
    assert(f_[0].congruent(f_[n]));
    ;
  }

  if (f_[0].xzstate() == Spectral) {
    int Mx = f_[0].Mx();
    int Mz = f_[0].Mz();
    int Ny = f_[0].Ny();
    int Nd = f_[0].Nd();
    int N = f_.N();
    array<Real> fn(N);
    for (int i=0; i<Nd; ++i)
      for (int ny=0; ny<Ny; ++ny)
	for (int mx=0; mx<Mx; ++mx)
	  for (int mz=0; mz<Mz; ++mz) {
	    for (int n=0; n<N; ++n)
	      fn[n] = Re(f_[n].cmplx(mx,ny,mz,i));
	    Real a = polyInterp(fn,t_,t);

	    for (int n=0; n<N; ++n)
	      fn[n] = Im(f_[n].cmplx(mx,ny,mz,i));
	    Real b = polyInterp(fn,t_,t);

	    f.cmplx(mx,ny,mz,i) = Complex(a,b);
	  }
  }
  else {
    int Nx = f_[0].Nx();
    int Ny = f_[0].Ny();
    int Nz = f_[0].Nz();
    int Nd = f_[0].Nd();
    int N = f_.N();
    array<Real> fn(N);
    for (int i=0; i<Nd; ++i)
      for (int ny=0; ny<Ny; ++ny)
	for (int nx=0; nx<Nx; ++nx)
	  for (int nz=0; nz<Nz; ++nz) {
	    for (int n=0; n<N; ++n)
	      fn[n] = f_[n](nx,ny,nz,i);
	    f(nx,ny,nz,i) = polyInterp(fn,t_,t);
	  }
  }
}

void save(const std::string& filebase, Real  T, const FieldSymmetry& tau) {
  string filename = appendSuffix(filebase, ".asc");
  ofstream os(filename.c_str());
  const int p = 16;  // digits of precision
  const int w = p+7; // width of field
  os << setprecision(p) << scientific;
  os << setw(w) << T << " %T\n";
  os << setw(w) << tau.s() << " %s\n";
  os << setw(w) << tau.sx() << " %sx\n";
  os << setw(w) << tau.sy() << " %sy\n";
  os << setw(w) << tau.sz() << " %sz\n";
  os << setw(w) << tau.ax() << " %ax\n";
  os << setw(w) << tau.az() << " %az\n";
};

void load(const std::string& filebase, Real& T, FieldSymmetry& tau) {
  string filename = appendSuffix(filebase, ".asc");
  ifstream is(filename.c_str());
  string junk;
  int su, sx, sy, sz;
  Real ax, az;
  bool error = false;
  is >> T >> junk;
  if (junk != "%T") error = true;
  is >> su >> junk;
  if (junk != "%s" || abs(su) != 1) error = true;
  is >> sx >> junk;
  if (junk != "%sx" || abs(sx) != 1) error = true;
  is >> sy >> junk;
  if (junk != "%sy" || abs(sy) != 1) error = true;
  is >> sz >> junk;
  if (junk != "%sz" || abs(sz) != 1) error = true;
  is >> ax >> junk;
  if (junk != "%ax" || abs(ax) > 0.5) error = true;
  is >> az >> junk;
  if (junk != "%az" || abs(az) > 0.5) error = true;

  if (error) {
    cout << "Error in loading T,su,sx,sy,sz,ax,az from file " << filename << endl;
    cout << "Exiting." << endl;
    exit(1);
  }
  tau = FieldSymmetry(sx,sy,sz,ax,az,su);
}

void plotfield(const FlowField& u_, const string& outdir, const string& label,
	       int xstride, int ystride, int zstride, int nx, int ny, int nz) {

  string preface = outdir;
  if (label.length() > 0)
    preface += label + "_";

  mkdir(outdir);

  FlowField& u = (FlowField&) u_;
  fieldstate xs = u.xzstate();
  fieldstate ys = u.ystate();

  if (ny < 0 || ny >=u.Ny())
    ny = (u.Ny()-1)/2;

  //Vector x = periodicpoints(Nx, u.Lx());
  //Vector y = chebypoints(Ny, u.a(), u.b());
  //Vector z = periodicpoints(Nz, u.Lz());
  //x.save(outdir + "x");
  //y.save(outdir + "y");
  //z.save(outdir + "z");

  u.makePhysical();

  int Ndim = u.Nd();
  switch (Ndim) {
  case 3:
    u.saveSlice(0, 2, nx, preface + "w_yz");
    u.saveSlice(1, 2, ny, preface + "w_xz");    
    u.saveSlice(2, 2, nz, preface + "w_xy");
    // fall through to case 2
  case 2:
    u.saveSlice(0, 0, nx, preface + "u_yz");
    u.saveSlice(0, 1, nx, preface + "v_yz");
    u.saveSlice(1, 0, ny, preface + "u_xz");
    u.saveSlice(1, 1, ny, preface + "v_xz");
    u.saveSlice(2, 0, nz, preface + "u_xy");
    u.saveSlice(2, 1, nz, preface + "v_xy");
    break;
  case 1:
    u.saveSlice(0, 0, nx, preface + "yz");
    u.saveSlice(1, 0, ny, preface + "xz");
    u.saveSlice(2, 0, nz, preface + "xy");
    break;
  case 0:
    break;
  default:
    string preface2 = preface + "_";
    for (int i=0; i<Ndim; ++i) {
      u.saveSlice(0, i, 0,  preface2 + i2s(i) + "_yz");
      u.saveSlice(1, i, ny, preface2 + i2s(i) + "_xz");    
      u.saveSlice(2, i, 0,  preface2 + i2s(i) + "_xy");
    }
  }
  u.makeSpectral();



  //FlowField vort = curl(u);
  //vort.makePhysical();
  //vort.saveSlice(0, 0, 0, preface + "vort_yz");

  //FlowField drag = ydiff(u);
  //drag.makePhysical();
  //drag.saveSlice(1, 0, Ny-1, preface + "drag_xz");

  //ChebyCoeff U = Re(u.profile(0,0,0));
  //U.makePhysical();
  //U.save("U");

  u.makeState(xs,ys);
}

// Produce plots of various slices, etc.
void plotxavg(const FlowField& u_, const std::string& outdir,
	      const std::string& label) {

  FlowField& u = (FlowField&) u_;
  fieldstate xs = u.xzstate();
  fieldstate ys = u.ystate();
  u.makeSpectral();

  FlowField uxavg(4, u.Ny(), u.Nz(), u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
  for (int i=0; i<u.Nd(); ++i)
    for (int my=0; my<u.My(); ++my)
      for (int mz=0; mz<u.Mz(); ++mz)
	uxavg.cmplx(0,my,mz,i) = u.cmplx(0,my,mz,i);

  mkdir(outdir);
  string preface = outdir;
  if (label.length() > 0)
    preface += label + "_";

  uxavg.makePhysical();
  uxavg.saveSlice(0, 0, 0,   preface + "u_yz_xavg");
  uxavg.saveSlice(0, 1, 0,   preface + "v_yz_xavg");
  uxavg.saveSlice(0, 2, 0,   preface + "w_yz_xavg");

  uxavg.makeSpectral();
  FlowField uxyavg(4, 1, u.Nz(), u.Nd(), u.Lx(), u.Lz(), u.a(), u.b(), Spectral, Spectral);
  for (int i=0; i<u.Nd(); ++i)
    for (int mz=0; mz<u.Mz(); ++mz) 
      uxyavg.cmplx(0,0,mz,i) = uxavg.cmplx(0,0,mz,i);
  
  ofstream os((preface + "uvw_xyavg.asc").c_str());
  uxyavg.makePhysical();
  int Nz = u.Nz();
  for (int nz=0; nz<=Nz; ++nz) {
    for (int i=0; i<u.Nd(); ++i)
      os << uxyavg(0,0, nz%Nz, i) << ' ';
    os << '\n';
  }

  u.makeState(xs,ys);
}

// Produce plots of various slices, etc.
void plotyavg(const FlowField& u_, const std::string& outdir,
	      const std::string& label) {
  
  FlowField& u = (FlowField&) u_;
  fieldstate xs = u.xzstate();
  fieldstate ys = u.ystate();

  mkdir(outdir);
  u.makeState(Physical, Spectral);
  
  string preface = outdir;
  if (label.length() > 0)
    preface += label + "_";

  ofstream us((preface + "u_xz_yavg.asc").c_str());  
  ofstream vs((preface + "v_xz_yavg.asc").c_str());  
  ofstream ws((preface + "w_xz_yavg.asc").c_str());  

  ChebyCoeff uprof(u.Ny(), u.a(), u.b(), Spectral);
  ChebyCoeff vprof(u.Ny(), u.a(), u.b(), Spectral);
  ChebyCoeff wprof(u.Ny(), u.a(), u.b(), Spectral);

  for (int nz=0; nz<=u.Nz(); ++nz) {
    int nzm = nz % u.Nz();
    for (int nx=0; nx<=u.Nx(); ++nx) {
      int nxm = nx % u.Nx();
      for (int ny=0; ny<u.Ny(); ++ny) {
	uprof[ny] = u(nxm,ny,nzm,0);
	vprof[ny] = u(nxm,ny,nzm,1);
	wprof[ny] = u(nxm,ny,nzm,2);
      }
      us << uprof.mean() << ' ';
      vs << uprof.mean() << ' ';
      ws << wprof.mean() << ' ';
    }
    us << '\n';
    vs << '\n';
    ws << '\n';
  }
  u.makeState(xs,ys);
}

// Produce plots of various slices, etc.
void plotyavg_energy(const FlowField& u_, const std::string& outdir,
	      const std::string& label) {
  
  FlowField& u = (FlowField&) u_;
  fieldstate xs = u.xzstate();
  fieldstate ys = u.ystate();

  mkdir(outdir);
  u.makeState(Physical, Physical);
  
  string preface = outdir;
  if (label.length() > 0)
    preface += label + "_";

  ofstream es((preface + "e_xz_yavg.asc").c_str());  
  ChebyCoeff eprof(u.Ny(), u.a(), u.b(), Physical);

  for (int nz=0; nz<=u.Nz(); ++nz) {
    int nzm = nz % u.Nz();
    for (int nx=0; nx<=u.Nx(); ++nx) {
      int nxm = nx % u.Nx();

      eprof.makePhysical();
      eprof.setToZero();
      for (int ny=0; ny<u.Ny(); ++ny) 
	for (int i=0; i<u.Nd(); ++i)
	  eprof[ny] += square(u(nxm,ny,nzm,i));

      eprof.makeSpectral();
      es << eprof.mean() << ' ';
    }
    es << '\n';
  }
  u.makeState(xs,ys);
}


void plotspectra(const FlowField& u_, const string& outdir, const string& label,
		 bool showpadding) {

  FlowField& u = (FlowField&) u_;
  fieldstate xs = u.xzstate();
  fieldstate ys = u.ystate();

  string preface = outdir;
  if (label.length() > 0)
    preface += label + "_";

  mkdir(outdir);
  u.makeSpectral();
  u.saveSpectrum(preface + "xzspec", true, showpadding);

  u.makePhysical_y();
  ofstream ucs((preface + "yspec.asc").c_str());

  ucs << "% sum of abs vals of Cheby coeffs of components u_{kx,kz}(y) " << endl;

  ChebyTransform trans(u.Ny());

  const int kxmax = lesser(u.kxmax(), 8);
  const int kzmax = lesser(u.kzmax(), 8);
  const int dk = 1;

  for (int kx=0; kx<=kxmax; kx += dk) {
    //for (int kz=0; kz<=kzmax; kz += dk) {
    int kz=lesser(kx,kzmax);
    BasisFunc prof = u.profile(u.mx(kx),u.mz(kz));
      prof.makeSpectral(trans);
      ucs << "% kx,kz == " << kx << ", " << kz << endl;
      for (int ny=0; ny<prof.Ny(); ++ny) {
	Real sum = 0.0;
	for (int i=0; i<prof.Nd(); ++i)
	  sum += abs(prof[i][ny]);
	ucs << sum << ' ';
      }
      ucs << '\n';
  }

  u.makeState(xs,ys);

}

/*****************
TimeStepParams::TimeStepParams(Real dt_, Real dtmin_, Real dtmax_,
			       Real CFLmin_, Real CFLmax_, bool variable_)
  :
  dt(dt_),
  dtmin(dtmin_),
  dtmax(dtmax_),
  CFLmin(CFLmin_),
  CFLmax(CFLmax_),
  variable(variable_)
{}


void params2timestep(const TimeStepParams& p, Real T, int& N, TimeStep& dt) {

  // Set CFL adjustment time dT to an integer division of T near 1
  N = Greater(iround(T), 1);
  const Real dT = T/N;
  if (p.variable)
    dt = TimeStep(p.dt, p.dtmin, p.dtmax, dT, p.CFLmin, p.CFLmax);
  else
    dt = TimeStep(p.dt, p.dt, p.dt, dT, p.CFLmin, p.CFLmax);
}
******************/

ArgList::ArgList()
  :
  args_(),
  used_(),
  helpmode_(false),
  errormode_(false)
{}


ArgList::ArgList(int argc, char* argv[], const string& purpose)
  :
  args_(argc),
  used_(argc),
  helpmode_(false),
  errormode_(false)
{
  string h0("-h");
  string h1("--help");
  for (int i=0; i<argc; ++i) {
    args_[i] = string(argv[i]);
    if (args_[i] == h0 || args_[i] == h1) {
      helpmode_ = true;
      used_[i] = true;
    }
    else
      used_[i] = false;
  }
  if (helpmode_) {
    cerr << argv[0] << " : \n\t"
	 << purpose << endl << endl;
    if (argc > 1)
      cerr << "options : " << endl;
  }
  used_[0] = true;
}


bool ArgList::helpmode() const {
  return helpmode_;
}

bool ArgList::errormode() const {
  return errormode_;
}

int ArgList::remaining() const {
  int unused = 0;
  for (int i=0; i<used_.length(); ++i)
    if (!used_[i])
      ++unused;
  return unused;
}

// The magic setw constants are a quick and dirty way to line up the columns
// nicely as long as the strings aren't too long. If you want to reimplement
// formatting more intelligently, please do!
void ArgList::printhelp(int position, const std::string& name,
			const string& helpstr) {
  cerr.setf(ios::left);
  cerr << "  ";
  cerr << setw(17) << name;
  cerr << setw(48) << string("(trailing arg " + i2s(position) + ")");
  cerr.unsetf(ios::left);
  cerr << helpstr << endl;
}

void ArgList::printhelp(const string& sopt, const string& lopt,
			const string& type, const string& defalt,
			const string& helpstr) {
  cerr.setf(ios::left);
  cerr << "  " << setw(8) << sopt << "  ";
  cerr << setw(20) << lopt
       << setw(10) << type;

  if (defalt.length() != 0)
    cerr << "  default == "
	 << setw(12) << defalt;
  else
    cerr << setw(25) << "";
  cerr.unsetf(ios::left);
  cerr << helpstr << endl;
}

bool ArgList::getflag(const string& sopt, const string& lopt,
		      const string& helpstr) {
  if (helpmode_) {
    printhelp(sopt, lopt, "", "", helpstr);
    return false;
  }

  bool b = false;
  int N = args_.length();
  for (int n=N-1; n>=1; --n) {
    if (args_[n] == sopt || args_[n] == lopt) {
      b = true;
      used_[n] = true;
      break;
    }
  }
  return b;
}



bool ArgList::getbool(const string& sopt, const string& lopt,
		      const string& helpstr) {
  bool b = false;

  int N = args_.length();
  bool found = false;
  for (int n=N-1; n>=1; --n) {
    if (args_[n] == sopt || args_[n] == lopt) {
      found = true;
      if (n<N-1 && args_[n+1] == "true")
	b = true;
      else if (n<N-1 && args_[n+1] == "false")
	b = false;
      else {
	cerr << "error : option " << sopt << " or " << lopt
	     << " should be followed by 'true' or 'false'." << endl;
	exit(1);
      }
      used_[n]   = true;
      used_[n+1] = true;
      break;
    }
  }
  if (helpmode_) {
    printhelp(sopt, lopt, "<bool>", "", helpstr);
    return b;
  }
  else if (!found) {
    //helpmode_ = true;
    errormode_ = true;
    cerr << "Missing required argument: " << sopt << " or " << lopt << endl;
    //printhelp(sopt, lopt, "<bool>", "", helpstr);
    //exit(1);
  }
  return b;
}

int  ArgList::getint (const string& sopt, const string& lopt,
		      const string& helpstr) {
  int i=0;
  bool found = false;
  int N = args_.length();
  for (int n=N-1; n>=1; --n) {
    if (args_[n] == sopt || args_[n] == lopt) {
      found = true;
      if (n<N-1)
	i = atoi(args_[n+1].c_str());
      else {
	cerr << "error : option " << sopt << " or " << lopt
	     << " should be followed by an integer." << endl;
	exit(1);
      }
      used_[n]   = true;
      used_[n+1] = true;
      break;
    }
  }
  if (helpmode_) {
    printhelp(sopt, lopt, "<int>", "", helpstr);
    return i;
  }
  else if (!found) {
    //helpmode_ = true;
    errormode_ = true;
    cerr << "Missing required argument: " << sopt << " or " << lopt << endl;
    //printhelp(sopt, lopt, "<int>", "", helpstr);
    //exit(1);
  }
  return i;
}

Real ArgList::getreal(const string& sopt, const string& lopt,
		      const string& helpstr) {
  Real r=0.0;

  int N = args_.length();
  bool found = false;
  for (int n=N-1; n>=1; --n) {
    if (args_[n] == sopt || args_[n] == lopt) {
      found = true;
      if (n<N-1)
	//r = atof(args_[n+1].c_str());
	r = arg2real(args_[n+1].c_str());
      else {
	cerr << "error : option " << sopt << " or " << lopt
	     << " should be followed by a real number." << endl;
	exit(1);
      }
      used_[n]   = true;
      used_[n+1] = true;
      break;
    }
  }
  if (helpmode_) {

    printhelp(sopt, lopt, "<real>", "", helpstr);
    return r;
  }
  else if (!found) {
    //helpmode_ = true;
    errormode_ = true;
    cerr << "Missing required argument: " << sopt << " or " << lopt << endl;
    //printhelp(sopt, lopt, "<real>", "", helpstr);
    //exit(1);
  }
  return r;
}

string ArgList::getpath(const string& sopt, const string& lopt,
			const string& helpstr) {
  return pathfix(getstr(sopt,lopt,helpstr));
}

string ArgList::getstr(const string& sopt, const string& lopt,
		       const string& helpstr) {
  string s("");

  bool found = false;
  int N = args_.length();
  for (int n=N-1; n>=1; --n) {
    if (args_[n] == sopt || args_[n] == lopt) {
      found = true;
      if (n<N-1)
	s = string(args_[n+1]);
      else {
	cerr << "error : option " << sopt << " or " << lopt
	     << " should be followed by a string." << endl;
	exit(1);
      }
      used_[n]   = true;
      used_[n+1] = true;
      break;
    }
  }
  if (helpmode_) {
    printhelp(sopt, lopt, "<string>", "", helpstr);
    return s;
  }
  else if (!found) {
    //helpmode_ = true;
    errormode_ = true;
    cerr << "Missing required argument: " << sopt << " or " << lopt << endl;
    //printhelp(sopt, lopt, "<string>", "", helpstr);
    //exit(1);
  }
  return s;
}

bool ArgList::getbool(const string& sopt, const string& lopt, bool defalt,
		      const string& helpstr) {
  bool b = defalt;
  if (helpmode_) {
    printhelp(sopt, lopt, "<bool>", string(b ? "true" : "false"), helpstr);
    return b;
  }

  int N = args_.length();
  for (int n=N-1; n>=1; --n) {
    if (args_[n] == sopt || args_[n] == lopt) {
      if (n<N-1 && args_[n+1] == "true")
	b = true;
      else if (n<N-1 && args_[n+1] == "false")
	b = false;
      else {
	cerr << "error : option " << sopt << " or " << lopt
	     << " should be followed by 'true' or 'false'." << endl;
	exit(1);
      }
      used_[n]   = true;
      used_[n+1] = true;
      break;
    }
  }
  return b;
}

int  ArgList::getint (const string& sopt, const string& lopt, int defalt,
		      const string& helpstr) {

  int i=defalt;
  if (helpmode_) {
    printhelp(sopt, lopt, "<int>", i2s(defalt), helpstr);
    return i;
  }
  int N = args_.length();
  for (int n=N-1; n>=1; --n) {
    if (args_[n] == sopt || args_[n] == lopt) {
      if (n<N-1)
	i = atoi(args_[n+1].c_str());
      else {
	cerr << "error : option " << sopt << " or " << lopt
	     << " should be followed by an integer." << endl;
	exit(1);
      }
      used_[n]   = true;
      used_[n+1] = true;
      break;
    }
  }
  return i;
}

Real ArgList::getreal(const string& sopt, const string& lopt, Real defalt,
		      const string& helpstr) {
  Real r=defalt;
  if (helpmode_) {
    printhelp(sopt, lopt, "<real>", r2s(defalt), helpstr);
    return r;
  }

  int N = args_.length();
  for (int n=N-1; n>=1; --n) {
    if (args_[n] == sopt || args_[n] == lopt) {
      if (n<N-1)
	//r = atof(args_[n+1].c_str());
	r = arg2real(args_[n+1].c_str());
      else {
	cerr << "error : option " << sopt << " or " << lopt
	     << " should be followed by a real number." << endl;
	exit(1);
      }
      used_[n]   = true;
      used_[n+1] = true;
      break;
    }
  }
  return r;
}

string ArgList::getpath(const string& sopt, const string& lopt,
			const string& defalt, const string& helpstr) {
  return pathfix(getstr(sopt,lopt,defalt,helpstr));
}

string ArgList::getstr(const string& sopt, const string& lopt,
		       const string& defalt, const string& helpstr) {
  string s(defalt);
  if (helpmode_) {
    printhelp(sopt, lopt, "<string>", defalt, helpstr);
    return defalt;
  }
  int N = args_.length();
  for (int n=N-1; n>=1; --n) {
    if (args_[n] == sopt || args_[n] == lopt) {
      if (n<N-1)
	s = string(args_[n+1]);
      else {
	cerr << "error : option " << sopt << " or " << lopt
	     << " should be followed by a string." << endl;
	exit(1);
      }
      used_[n]   = true;
      used_[n+1] = true;
      break;
    }
  }
  return s;
}

string ArgList::getstr(int position, const std::string& name,
		       const string& helpstr) {
  if (helpmode_) {
    printhelp(position, name, helpstr);
    return string();
  }

  int n = args_.length()-position;
  if (n<1 || used_[n]) {
    cerr << "error : " << name << " is required as the " << position
	 << "th trailing argument (counting backwards from end of arglist)"
	 << endl;;
    exit(1);
  }
  if (args_[n][0] == '-') {
    cerr << "error : should have a filename for " << position << "th trailing argument, ";
    cerr << "not option " << args_[n] << endl;
    exit(1);
  }
  used_[n] = true;
  return args_[n];
}

string ArgList::getpath(int position, const std::string& name,
			const string& helpstr) {
  return pathfix(getstr(position, name, helpstr));
}

Real ArgList::getreal(int position, const std::string& name,
		      const string& helpstr) {
  if (helpmode_) {
    printhelp(position, name, helpstr);
    return 0.0;
  }

  int n = args_.length()-position;
  if (n<1 || used_[n]) {
    cerr << "error : " << name << " is required as the " << position
	 << "th trailing argument (counting backwards from end of arglist)"
	 << endl;;
    exit(1);
  }
  /***********************8
  if (args_[n][0] == '-') {
    cerr << "error : should have a Real for " << position << "th trailing argument, ";
    cerr << "not option " << args_[n] << endl;
    exit(1);
  }
  ******************************/
  used_[n] = true;
  //return atof(args_[n].c_str());
  return arg2real(args_[n].c_str());
}
/*****************************************8
TimeStep ArgList::gettimestep() {

  const Real dt       = this->getreal("-dt",     "--dt",     0.03125, "timestep");
  const Real dtmin    = this->getreal("-dtmin",  "--dtmin",  0.001, "minimum time step");
  const Real dtmax    = this->getreal("-dtmax",  "--dtmax",  0.05,  "maximum time step");
  const Real dT       = this->getreal("-dT",     "--dT",     1.0, "save interval");
  const bool vardt    = this->getflag("-vdt",    "--variabledt", "adjust dt to keep CFLmin<=CFL<CFLmax");
  const Real CFLmin   = this->getreal("-CFLmin", "--CFLmin", 0.40, "minimum CFL number");
  const Real CFLmax   = this->getreal("-CFLmax", "--CFLmax", 0.60, "maximum CFL number");
  return TimeStep(dt, dtmin, dtmax, dT, CFLmin, CFLmax, vardt);
}

DNSFlags ArgList::getdnsflags() {
  DNSFlags flags;
  flags.baseflow     = s2baseflow(this->getstr("-bflow", "--baseflow",    "linear", "base flow: zero|linear|parabola"));
  flags.timestepping = s2stepmethod(this->getstr("-ts", "--timestepping", "sbdf3", "time step algorithm: sbdf[1234]|cnab2|cnrk2|smrk2"));
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = s2nonlmethod(this->getstr("-nl", "--nonlinearity", "rot", "nonlinearity method: rot|conv|skew|div|alt"));
  flags.constraint   = s2constraint(this->getstr("-mc", "--meanconstraint", "pressure", "constraint on mean flow: pressure|velocity"));
  flags.dPdx         = this->getreal("-dPdx", "--pressuregrad", 0.0, "mean pressure gradient");
  flags.Ubulk        = this->getreal("-Ubulk", "--bulkvelocity", 0.0, "bulk velocity (mean of total velocity)");
  flags.verbosity    = Silent;
  return flags;
}
*****************************************/

void ArgList::check() {
  for (int n=0; n<used_.length(); ++n) {
    if (!used_[n]) {
      cerr << "error : unrecognized/repeated option/value " << args_[n] << endl;
      errormode_ = true;
    }
  }
  if (errormode_) {
    cerr << "Error in specifying program arguments. Please rerun program with -h option " << endl;
    cerr << "and review the argument specifications. Exiting." << endl;
    exit(1);
  }
  else if (helpmode_)
    exit(0);
}

void ArgList::save() const {
  this->save("./");
}

void ArgList::save(const string& outdir) const {
  // Look for output directory and save command-line to outdir/argv[0].args
  string arg0 = args_[0];
  string exec;
  int slashpos = arg0.find_last_of('/');
  if (slashpos == -1) {
    exec = arg0;
  }
  else {
    exec = string(arg0, slashpos+1, arg0.length());
  }
  int extpos = exec.find(".x");
  if (extpos == -1)
    extpos = exec.find(".dx");
  if (extpos == -1)
    extpos = exec.length();

  string arse = string(exec, 0, extpos) + ".args";
  string afile = outdir + arse;
  ::mkdir(outdir.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);
  ofstream as(afile.c_str(), ios::app);
  ofstream cs("chflow.args", ios::app);

  // Four statements, and two types, and two possible memory leaks to get the current time.
  // Ain't C wonderful?
  time_t now;
  time(&now);               // get the calendar time
  tm *t = localtime(&now);  // convert to local (memory leak?)
  string s(asctime(t));     // memory leak?
  s.erase(s.length()-1);    // remove ridiculous newline from asctime output
  as << s << '\t';
  cs << s << '\t';

  // get process ID. equally wonderful
  as << getpid() << '\t';
  cs << getpid() << '\t';

  for (int n=0; n<args_.length(); ++n)
    as << args_[n] << ' ';
  as << endl;

  for (int n=0; n<args_.length(); ++n)
    cs << args_[n] << ' ';
  cs << endl;
}

string pathfix(const string& path) {
  string rtn = path;
  if (rtn.length() > 0 && rtn[rtn.length()-1] != '/')
    rtn += "/";
  return rtn;
}

void mkdir(const string& dirname) {
  ::mkdir(dirname.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);
}
void rename(const string& oldname, const string& newname) {
  ::rename(oldname.c_str(), newname.c_str());
}

Real PuFraction(const FlowField& u, const FieldSymmetry& s, int sign) {
  FlowField Pu = s(u);
  if (sign == 1)
    Pu += u;
  else
    Pu -= u;
  return 0.25*L2Norm2(Pu)/L2Norm2(u); // 0.25 is square of 1/2 from Pu = 1/2 (u+s(u))
}

FieldSymmetry xfixphasehack(FlowField& u, Real axguess, int i, parity p) {

  const int Ny2 = (u.Ny() - 1)/2;

  // move solution from centered about box center to center about origin
  FieldSymmetry halfshift(1,1,1, 0.5, 0.5);
  u *= halfshift;
  u.makePhysical();

  PeriodicFunc f(u.Nx(), u.Lx(), Physical);
  for (int nx=0; nx<u.Nx(); ++nx) 
    f(nx) = u(nx, Ny2, 0, i);

  u.makeSpectral();
  u *= halfshift;

  f.makeSpectral();
  if (p == Odd)
    f.cmplx(0) = Complex(0.0, 0.0); // set mean to zero
  else
    f = diff(f); // even parity means find zero of df/dz

  Real xcenter = newtonSearch(f, axguess*u.Lx());
  FieldSymmetry phaseshift(1,1,1, xcenter/u.Lx(), 0.0);  

  return phaseshift;
}

// z-shift until f'(z) == Lz/2 for f(z) = <u>_{xy}(z)
FieldSymmetry zfixphasehack(FlowField& u, Real azguess, int i, parity p) {

  // move solution from centered about z=Lz/2 to center about z=0
  FieldSymmetry halfshift(1,1,1, 0.0, 0.5);
  u.makeSpectral();
  u *= halfshift;


  PeriodicFunc f(u.Nz(), u.Lz(), Spectral);
  for (int mz=0; mz<u.Mz(); ++mz) 
    f.cmplx(mz) = u.cmplx(0, 0, mz, i); // should do zshift here

  u *= halfshift; // shift u back to original phase
  
  if (p == Odd)
    f.cmplx(0) = Complex(0.0, 0.0); // set mean to zero
  else
    f = diff(f); // even parity means find zero of df/dz
  
  Real zcenter = newtonSearch(f, azguess*u.Lz());
  FieldSymmetry phaseshift(1,1,1, 0.0, zcenter/u.Lz());  

  return phaseshift;

}

// Return a translation that maximizes the s-symmetry of u.
// (i.e. return tau // that minimizes L2Norm(s(tau(u)) - tau(u))).
FieldSymmetry optimizePhase(const FlowField& u, const FieldSymmetry& s,
			    int Nsteps, Real residual, Real damp,
			    bool verbose, Real x0, Real z0) {

  FieldSymmetry tau;
  if (s.sx()==1 && s.sz()==1)
    ;
  else if ((s.sx() == -1) ^ (s.sz() == -1)) {

    // Newton search for x-translation that maximizes shift-rotate symmetry
    if (verbose)
      cout << "\noptimizePhase: Newton search on translation" <<endl;

    int i   = (s.sx() == -1) ? 0 : 2; // are we optimizing on x or z?
    Real L  = (s.sx() == -1) ? u.Lx() : u.Lz();
    array<Real> x(3);
    x[0] = x0;
    x[2] = z0;

    FlowField tu, stu, tux, stux, tuxx, stuxx;

    for (int n=0; n<Nsteps; ++n) {
      tau = FieldSymmetry(x[0]/u.Lx(), x[2]/u.Lz());

      tu  = tau(u);
      stu = s(tu);
      tux   = diff(tu, i, 1);
      stux  = diff(stu, i, 1);
      tuxx  = diff(tux, i, 1);
      stuxx = diff(stux, i, 1);

      // g(x) is df/dx where f == (L2Norm(stu - tu)) and t = tau(x/Lx,0)
      Real g    =  L2IP(stux, tu) - L2IP(stu, tux);

      // Note: I think this could simplify via (stuxx, tu) = (stu,tuxx)
      Real dgdx = 2*L2IP(stux, tux) - L2IP(stuxx, tu) - L2IP(stu, tuxx);

      if (verbose) {
	Real f = L2Dist(stu, tu);
	cout << endl;
	cout << "Newton step n  == " << n << endl;
	cout << "tau.ax         == " << tau.ax() << endl;
	cout << "tau.az         == " << tau.az() << endl;
	cout << "zeroable g     == " << g << endl;
	cout << "L2Norm(stu-tu) == " << f << endl;
      }
      if (abs(g) < residual && verbose) {
	cout << "\noptimizePhase: Newton search converged." << endl;
	break;
      }

      Real dx = -g/dgdx;

      if (abs(damp*dx)/L > 0.5) {
	cout << "\nerror in optimizePhase(FlowField, FieldSymmetry) :" << endl;
	cout << "Newton search went beyond bounds. " << endl;
	cout << "     g == " << g << endl;
	cout << " dg/dx == " << dgdx << endl;
	cout << "    dx == " << damp*dx << endl;
	cout << "  L/2  == " << 0.5*L  << endl;
	cout << "Jostle by L/10 and try again." << endl;
	x[i] += L/10;
      }
      else
	x[i] += damp*dx;

    }
  }
  else {
    cout << "\nerror in optimizePhase(FlowField, FieldSymmetry) :" << endl;
    cout << "2d symmetrization is not yet implemented. Returning identity." << endl;
  }

  if (verbose) {
    FlowField tu = tau(u);
    FlowField stu = s(tu);
    cout << "\noptimal translation == " << tau << endl;
    cout << "Post-translation symmetry error == " << L2Dist(stu, tu) << endl;
  }
  return tau;
}

string t2s(Real t, bool inttime) {
  char buffer[12];
  if (inttime) {
    int it = iround(t);
    sprintf(buffer, "%d", it);
  }
  else
    sprintf(buffer, "%.3f", t);
  return string(buffer);
}

string fuzzyless(Real x, Real eps) {
  string rtn("false ");
  if (x < eps)
    rtn = "TRUE  ";
  else if (x < sqrt(eps))
    rtn = "APPROX";
  return rtn;
};

string pwd() {
  const int N=512;
  char* buff = new char[N];
  getcwd(buff, N);
  string rtn(buff);
  delete[] buff;
  return rtn;
}

string stub(const string& filename, const string& ext) {

  // Clip off path
  size_t s = filename.find_last_of("/");
  string f = (s == string::npos) ? filename : filename.substr(s+1, filename.length()-s);

  // Clip off extension
  size_t t = f.find(ext, f.length()-ext.length());
  string g = (t == string::npos) ? f : f.substr(0,t);

  return g;
}




void fixDiri(ChebyCoeff& f) {
  Real fa = f.eval_a();
  Real fb = f.eval_b();
  Real mean = 0.5*(fb+fa);
  Real slop = 0.5*(fb-fa);
  f[0] -= mean;
  f[1] -= slop;
}

void fixDiriMean(ChebyCoeff& f) {
  Real fa = f.eval_a();
  Real fb = f.eval_b();
  Real fm = f.mean();
  f[0] -= 0.125*(fa+fb) + 0.75*fm;
  f[1] -= 0.5*(fb-fa);
  f[2] -= 0.375*(fa+fb) - 0.75*fm;
}

void fixDiriNeum(ChebyCoeff& f)  {

  Real ya = f.a();
  Real yb = f.b();
  f.setBounds(-1,1);
  Real a = f.eval_a();
  Real b = f.eval_b();
  Real c = f.slope_a();
  Real d = f.slope_b();

  // The numercial coeffs are inverse of the matrix (values found with Maple)
  // T0(-1)  T1(-1)  T2(-1)  T3(-1)     s0      a
  // T0(1)   T1(1)   T2(1)   T3(1)      s1      b
  // T0'(-1) T1'(-1) T2'(-1) T3'(-1)    s2  ==  c
  // T0'(1)  T1'(1)  T2'(1)  T3'(1)     s3      d

  Real s0 = 0.5*(a + b) + 0.125*(c - d);
  Real s1 = 0.5625*(b - a) - 0.0625*(c + d);
  Real s2 = 0.125*(d - c);
  Real s3 = 0.0625*(a - b + c + d);

  f[0] -= s0;
  f[1] -= s1;
  f[2] -= s2;
  f[3] -= s3;
  f.setBounds(ya,yb);
}

void fixDiri(ComplexChebyCoeff& f) {
  fixDiri(f.re);
  fixDiri(f.im);
}
void fixDiriMean(ComplexChebyCoeff& f) {
  fixDiriMean(f.re);
  fixDiriMean(f.im);
}

void fixDiriNeum(ComplexChebyCoeff& f) {
  fixDiriNeum(f.re);
  fixDiriNeum(f.im);
}


void field2vector(const FlowField& u, Vector& a) {

  assert(u.xzstate() == Spectral && u.ystate() == Spectral);
  int Kx = u.kxmaxDealiased();
  int Kz = u.kzmaxDealiased();
  int Ny = u.Ny();

  int N = 4*(Kx+2*Kx*Kz+Kz)*(Ny-3)+2*(Ny-2);
  if (a.length() == 0)
    a.resize(N);
  else
    a.setToZero();
  assert(a.length() >= N);


  int n=0;

  // These loops follow Gibson, Halcrow, Cvitanovic table 1.
  for (int ny=2; ny<Ny; ++ny)
    a(n++) = Re(u.cmplx(0,ny,0,0)); // Ny-2 modes (line 1 of table)

  for (int ny=2; ny<Ny; ++ny)
    a(n++) = Re(u.cmplx(0,ny,0,2)); // Ny-2 modes (line 2)

  // Some coefficients from the FlowField are linearly dependent due
  // to BCs and divergence constraint. Omit these from a(n), and reconstruct
  // them in vector2field(v,u) using the constraint equations. In what follows,
  // which coefficients are omitted is determined by the changing ny loop index
  // e.g. (ny=3; n<Ny-1; ++ny) omits ny=0,1 and Ny-1, and the constraint eqns
  // that allow their reconstruction are mentioned in comments to the right
  // of the loop. 2007-06-07 jfg.
  for (int kx=1; kx<=Kx; ++kx) {
    int mx = u.mx(kx);
    for (int ny=2; ny<Ny; ++ny) {      // w BCs => ny=0,1 coefficients
      a(n++) = Re(u.cmplx(mx,ny,0,2)); // J(Ny-2) modes (line 3a)
      a(n++) = Im(u.cmplx(mx,ny,0,2)); // J(Ny-2) modes (line 3b)
    }
    for (int ny=3; ny<Ny-1; ++ny) {    // u BCs => 0,1; v BC => 2; div => Ny-1
      a(n++) = Re(u.cmplx(mx,ny,0,0)); // J(Ny-4) modes (line 4a)
      a(n++) = Im(u.cmplx(mx,ny,0,0)); // J(Ny-4) modes (line 4b)
    }
  }
  for (int kz=1; kz<=Kz; ++kz) {
    int mz = u.mz(kz);
    for (int ny=2; ny<Ny; ++ny) {      // u BCs => 0,1; v BC => 2; div => Ny-1
      a(n++) = Re(u.cmplx(0,ny,mz,0)); // K(Ny-2)  (line 5a)
      a(n++) = Im(u.cmplx(0,ny,mz,0)); // K(Ny-2)  (line 5b)
    }
    for (int ny=3; ny<Ny-1; ++ny) {    // w BCs => 0,1; v BC => 2; div => Ny-1
      a(n++) = Re(u.cmplx(0,ny,mz,2)); // K(Ny-2)  (line 6a)
      a(n++) = Im(u.cmplx(0,ny,mz,2)); // K(Ny-2)  (line 6b)
    }
  }
  for (int kx=-Kx; kx<=Kx; ++kx) {
    if (kx==0)
      continue;

    int mx = u.mx(kx);
    for (int kz=1; kz<=Kz; ++kz) {
      int mz = u.mz(kz);
      for (int ny=2; ny<Ny; ++ny) {     // u BCs => 0,1;
	a(n++) = Re(u.cmplx(mx,ny,mz,0)); // JK(Ny-2)  (line 7a)
	a(n++) = Im(u.cmplx(mx,ny,mz,0)); // JK(Ny-2)  (line 7b)
      }
      for (int ny=3; ny<Ny-1; ++ny) { // w BCs => 0,1; v BC => 2; div => Ny-1
	a(n++) = Re(u.cmplx(mx,ny,mz,2)); // JK(Ny-4)  (line 8a)
	a(n++) = Im(u.cmplx(mx,ny,mz,2)); // JK(Ny-4)  (line 8b)
      }
    }
  }
}

void vector2field(const Vector& a, FlowField& u) {

  assert(u.xzstate() == Spectral && u.ystate() == Spectral);
  int Kx=u.kxmaxDealiased();
  int Kz=u.kzmaxDealiased();
  int Ny=u.Ny(); // max polynomial order == number y gridpoints - 1
  Real ya = u.a();
  Real yb = u.b();
  Real Lx = u.Lx();
  Real Lz = u.Lz();

  assert(a.length() >= 4*(Kx+2*Kx*Kz+Kz)*(Ny-3)+2*(Ny-2));

  u.setToZero();

  Complex zero(0.0, 0.0);
  ComplexChebyCoeff f0(Ny, ya, yb, Spectral);
  ComplexChebyCoeff f1(Ny, ya, yb, Spectral);
  ComplexChebyCoeff f2(Ny, ya, yb, Spectral);

  // These loops follow Gibson, Halcrow, Cvitanovic table 1.
  // The vector contains the linearly independent real numbers in a
  // zero-div no-slip FlowField. The idea is to extract these from the
  // vector and reconstruct the others from the BCs and div constraint
  // See corresponding comments in field2vector(u,v);

  int n=0;

  // =========================================================
  // (0,0) Fourier mode
  for (int ny=2; ny<Ny; ++ny)
    f0.re[ny] = a(n++);
  fixDiri(f0.re);
  for (int ny=0; ny<Ny; ++ny)
    u.cmplx(0,ny,0,0) = f0[ny];

  for (int ny=2; ny<Ny; ++ny)
    f2.re[ny] = a(n++);
  fixDiri(f2.re);
  for (int ny=0; ny<Ny; ++ny)
    u.cmplx(0,ny,0,2) = f2[ny];

  // =========================================================
  // (kx,0) Fourier modes, 0<kx
  for (int kx=1; kx<=Kx; ++kx) {
    int mx = u.mx(kx);

    for (int ny=2; ny<Ny; ++ny) {
      f2.re[ny] = a(n++);
      f2.im[ny] = a(n++);
    }
    fixDiri(f2);

    for (int ny=3; ny<Ny-1; ++ny) {
      f0.re[ny] = a(n++);
      f0.im[ny] = a(n++);
    }
    f0.re[Ny-1] = 0.0;
    f0.im[Ny-1] = 0.0;
    fixDiriMean(f0);

    integrate(f0, f1);
    f1.sub(0, 0.5*(f1.eval_a() + f1.eval_b()));
    f1 *= Complex(0.0, -(2*pi*kx)/Lx);

    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mx,ny,0,0) = f0[ny];
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mx,ny,0,1) = f1[ny];
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mx,ny,0,2) = f2[ny];

    // ------------------------------------------------------
    // Now copy conjugates of u(kx,ny,0,i) to u(-kx,ny,0,i). These are
    // redundant modes stored only for the convenience of FFTW.
    int mxm = u.mx(-kx);
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mxm,ny,0,0) = conj(f0[ny]);
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mxm,ny,0,1) = conj(f1[ny]);
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(mxm,ny,0,2) = conj(f2[ny]);

  }


  // =========================================================
  // (0,kz) Fourier modes, 0<kz.
  for (int kz=1; kz<=Kz; ++kz) {
    int mz = u.mz(kz);

    for (int ny=2; ny<Ny; ++ny) {
      f0.re[ny] = a(n++);
      f0.im[ny] = a(n++);
    }
    fixDiri(f0);
    for (int ny=3; ny<Ny-1; ++ny) {
      f2.re[ny] = a(n++);
      f2.im[ny] = a(n++);
    }
    f2.re[Ny-1] = 0.0;
    f2.im[Ny-1] = 0.0;
    fixDiriMean(f2);

    integrate(f2, f1);
    f1.sub(0, 0.5*(f1.eval_a() + f1.eval_b()));
    f1 *= Complex(0.0, -(2*pi*kz)/Lz);

    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(0,ny,mz,0) = f0[ny];
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(0,ny,mz,1) = f1[ny];
    for (int ny=0; ny<Ny; ++ny)
      u.cmplx(0,ny,mz,2) = f2[ny];
  }

  // =========================================================
  for (int kx=-Kx; kx<=Kx; ++kx) {
    if (kx==0)
      continue;

    int mx = u.mx(kx);
    for (int kz=1; kz<=Kz; ++kz) {
      int mz = u.mz(kz);

      for (int ny=2; ny<Ny; ++ny) {
	f0.re[ny] = a(n++);
	f0.im[ny] = a(n++);
      }
      fixDiri(f0);

      // f0 is complete. Load it into u.
      for (int ny=0; ny<Ny; ++ny)
	u.cmplx(mx,ny,mz,0) = f0[ny];

      for (int ny=3; ny<Ny-1; ++ny) {
	f2.re[ny] = a(n++);
	f2.im[ny] = a(n++);
      }
      f2.re[Ny-1] = -f0.re[Ny-1]*(kx*Lz)/(kz*Lx);
      f2.im[Ny-1] = -f0.im[Ny-1]*(kx*Lz)/(kz*Lx);

      // Adjust 0,1,2 coeffs of f2 so that f2(+/-1) = 0 and
      // kz/Lz mean(f2) + kx/Lx mean(f0) == 0 (so that  v(1)==v(-1))
      Complex f2a = f2.eval_a();
      Complex f2b = f2.eval_b();
      Complex f0m = f0.mean();
      Complex f2m = f2.mean() + (kx*Lz)/(kz*Lx)*f0m;

      f2.sub(0, 0.125*(f2a+f2b) + 0.75*f2m);
      f2.sub(1, 0.5*(f2b-f2a));
      f2.sub(2, 0.375*(f2a+f2b) - 0.75*f2m);

      for (int ny=0; ny<Ny; ++ny)
	u.cmplx(mx,ny,mz,2) = f2[ny];

      f0 *= Complex(0, -2*pi*kx/Lx);
      f2 *= Complex(0, -2*pi*kz/Lz);
      f0 += f2;

      integrate(f0, f1);
      f1.sub(0, 0.5*(f1.eval_a() + f1.eval_b()));

      for (int ny=0; ny<Ny; ++ny)
	u.cmplx(mx,ny,mz,1) = f1[ny];
    }
  }
}

Real  linear_extrapolate(Real x0, Real f0, Real x1, Real f1, Real x) {
  return (f0*(x-x1) - f1*(x-x0)) / (x0 - x1);
}

// Neville algorithm.
Real quadratic_extrapolate(const array<Real>& f, const array<Real>& mu, Real mug) {

  const int N=3; // three points for quadratic extrapolation
  Real y[2][N];  // two arrays for quadratic extrap recurrence relation

  // Load input f data into y array
  for (int i=0; i<N; ++i)
    y[0][i] = f[i];

  int j_curr;
  for (int i=1; i<N; i++) {
    j_curr = i%2;
    int j_prev = (i-1)%2;
    for (int k=0; k<(N-i); k++)
      y[j_curr][k] = linear_extrapolate(mu[k] , y[j_prev][k] , mu[k+i] , y[j_prev][k+1] , mug);
  }
  return y[j_curr][0];
}


} //namespace channelflow
