#include <iostream>
//#include <fstream>
//#include <sstream>
//#include <iomanip>
#include "string.h"

#include <complex>
#include <Eigen/Dense>
#include <Eigen/Core>

//#include "channelflow/flowfield.h"
//#include "channelflow/periodicfunc.h"
//#include "channelflow/dns.h"
//#include "channelflow/utilfuncs.h"
#include "channelflow/controller.h"
//#include "channelflow/turbstats.h"

//#include "channelflow/riccati_solver_takamasa.h"

//#include <boost/numeric/odeint.hpp>
//#include <boost/numeric/odeint/external/eigen/eigen.hpp>

using namespace std;
using namespace channelflow;
//using namespace boost::numeric::odeint;

//using state_type = Eigen::VectorXcd;

void display_mat (
        Eigen::MatrixXcd &M,
        string name) {
    cout << " --- " << name << " --- "  << endl;
    for (size_t i = 0; (unsigned) i < M.rows(); i++) {
        for (size_t j = 0; (unsigned) j < M.cols(); j++)
            cout << M(i, j) << " ";
        cout << endl;
    }
};

class model_class
{
    private:
        Eigen::MatrixXcd A;
        Eigen::MatrixXcd B;
        Eigen::MatrixXcd q;

    public:
        model_class () {};
        model_class (const Eigen::MatrixXcd mat_A, const Eigen::MatrixXcd mat_B, const Eigen::MatrixXcd mat_q) :
            A(mat_A), B(mat_B), q(mat_q) {
                cout << "A : " << A.rows() << " / " << A.cols() << endl;
                cout << "B : " << B.rows() << " / " << B.cols() << endl;
                cout << "q : " << q.rows() << " / " << q.cols() << endl;
            };
        void operator()(const Eigen::VectorXcf &x, Eigen::VectorXcf &dxdt, double t) {
            dxdt = A*x + B*q;
        }
};

struct streaming_observer
{
    ostream &m_out;
    streaming_observer(ostream &out) : m_out(out) {}

    void operator() (const Eigen::VectorXcf &x, double t) const
    {
        m_out << t;
        //for(size_t i = 0; (unsigned) i < x.size(); i++)
        //    m_out << "\t" << x[i];
        m_out << "\n";
    }
};

int main(){
    cout << setprecision(8) << fixed;

    string user = "gcls";
    int eq = 1;
    int Re = 400;
    int Nx = 4;
    int Ny = 15;
    int Nz = 4;
    double tau = 0.05;

    const double dt = 0.00001;

    size_t N = (Nx*Nz + Nx*Nz - 1 + 2) * Ny;
    size_t N_act = (Nx*Nz + Nx*Nz - 1 + 2) * 2;
    cout << N << " / " << N_act << endl;

    string basename = "/home/"+user+"/wave-dmd/database/OSSE/eq"+to_string(eq)
        +"/Re="+to_string(Re)
        +"/"+to_string(Nx)+"x"+to_string(Ny)+"x"+to_string(Nz)+"/";
    //string basename = "/home/"+user+"/wave-dmd/database/test_files/";
    cout << basename << endl;

    //*
    cout << "Loading matrix A" << endl;
    Eigen::MatrixXcd A(N, N);
    MatIn_hdf5_complex(A, basename+"A.h5");
    //Eigen::MatrixXcd A(3, 2);
    //MatIn_hdf5_complex(A, basename+"MAT.h5");
    //display_mat(A, "A");
    cout << "A loaded" << endl << endl;

    cout << "Loading matrix B" << endl;
    cout << "N : " << N << endl;
    cout << "N_act : " << N_act << endl;
    Eigen::MatrixXcd B(N, N_act);
    MatIn_hdf5_complex(B, basename+"B.h5");
    //Eigen::MatrixXcd B(3, 2);
    //MatIn_hdf5_complex(B, basename+"MAT.h5");
    //display_mat(B, "B");
    cout << "B loaded" << endl << endl;

    Eigen::VectorXcf X(N);

    //*
    cout << "Loading matrix q" << endl;
    Eigen::VectorXcd q(N_act);
    cout << basename+"q_kx=+1_kz=+1.h5" << endl;
    MatIn_hdf5_real(q, basename+"q_kx=+1_kz=+1.h5");
    cout << "q loaded" << endl << endl;
    //*/

    for (int i = 0; (unsigned) i < N; i++) {
        X[i] = 1;
    }
    //*/

    Controller_K K = Controller_K(Nx, Ny, Nz, basename, "None", tau);

    /*
    cout << " INTEGRATION " << endl;
    //integrate_adaptive(controlled_runge_kutta
    //        <runge_kutta_cash_karp54
    //            <Eigen::VectorXcf, double, Eigen::VectorXcf, double, vector_space_algebra>
    //        >(),
    integrate_const(runge_kutta4<Eigen::VectorXcf, double, Eigen::VectorXcf, double, vector_space_algebra>(),
            model_class(A, B, q),
            X,
            0.0,
            0.5,
            dt,
            streaming_observer(cout));
    /*/

    //for (int i = 0; (unsigned) i < N; i++) {
    cout << X(0) << endl;
    //}

}
