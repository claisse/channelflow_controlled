//
//  Controller_K_eq1_rosse.cpp
//
//  Created by Geoffroy Claisse on 05/2019.
//  from Controller_Run.cpp from Peter Heins (05/2015).

#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <complex>
#include "channelflow/flowfield.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"
#include "channelflow/controller.h"
#include "channelflow/turbstats.h"

using namespace std;
using namespace channelflow;

int main(){

    string home             = string(getenv("HOME")) + "/";
    bool ReStart            = false;
    bool Controlled         = 1;
    bool Perturbed          = 0;
    int T_controller_start  = 0; // when controllers turns on
    int T_perturbed         = 10; // when perturbation is applied

    // Perturbation magnitude (if perturbation on the entire flowfield)
    const Real perturbMag = 1E-8;
    string perturbMag_str = "1E-8";

    // Perturbed mode (if perturbation on one given mode)
    // NOTE: if (kx, kz) is actuated, then (-kx, -kz) as well
    int kx = +1;
    int kz = +1;
    double forcing_mag = 0.0005;

    // Num of data points in x, y and z directions
    const int Nx_nopad      = 17;
    const int Ny_nopad      = 21;
    const int Nz_nopad      = 17;
    string baseflow_type    = "eq1"; // "couette"
    string baseflow_from    = "findsoln"; // "changegrid"
    string baseflow_file    =
            home + "osse/database/eq/"+baseflow_type+"_"
            +i2s(Nx_nopad)+"x"+i2s(Ny_nopad)+"x"+i2s(Nz_nopad)+"_"+baseflow_from;
            //home + "osse/database/eq/"+baseflow_type+"_"
            //+i2s(Nx_nopad)+"x"+i2s(Ny_nopad)+"x"+i2s(Nz_nopad);

    // controller spec
    const int Nx_controller = 5;
    const int Ny_controller = 4;
    const int Nz_controller = 7;
    const double kappa      = 1.0;
    const string kappa_str  = "1.0";
    double tau              = 0.005; // actuator time constant
    const string tau_str    = "0.005"; // actuator time constant

    // Loading velocity FlowField
    FlowField u(baseflow_file); // Velocity flowfield (u=object)
    FlowField u0(baseflow_file); // Velocity flowfield (u=object)
    u.makeState(Spectral, Spectral);
    u0.makeState(Spectral, Spectral);

    // Num of data points in x, y and z directions
    const int Nx            = u.Nx();
    const int Ny            = u.Ny();
    const int Nz            = u.Nz();
    //const int Nd            = u.Nd();

    // Size of domain
    const Real Lx           = u.Lx(); //2*pi/1.14;
    const Real a            = u.a(); //-1.0;
    const Real b            = u.b(); //1.0;
    const Real Lz           = u.Lz(); //2*pi/2.5;

    // Define flow parameters
    const Real Reynolds     = 400.0;
    const Real nu           = 1.0/Reynolds; // Kinematic viscosity

    // Variable time-stepping parameters
    const Real CFLmin       = 0.10;
    const Real CFLmax       = 0.30;
    const Real dtmax        = tau; // <<<< CONTROLLER time interval
    const Real dtmin        = 0.0000001;
    const Real dt0          = dtmax;
    const bool variable_dt  = true;
    const Real T0           = 0.0; // Start time
    const Real T1           = 100.0; // End time
    const Real dT           = 1; // 0.1; //
    const Real TSave        = 1; // time interval to save data

    // Save parameters
    string foldersave_base  = home + "channelflow/database/PHD/controller_K_ROSSE/";
    //string foldersave_base  = home + "osse/database/DEBUG/CHFL_export_debug_control/";
    Real SaveInt            = T1-T0;
    int nSave               = iround(SaveInt/dT);
    int itSave              = iround(TSave/dT); // time step interval to save data

    // DNS flags
    DNSFlags flags; // DNSFlags = class, flags = object
    flags.nu                = nu;
    flags.baseflow          = LinearBase; // Couette laminar solution, unitary
    string nonlinearity_str = "skewsymmetric";
    flags.nonlinearity      = SkewSymmetric;
    flags.initstepping      = CNRK2; // SBDF1 | CNRK2 | SMRK2 : defaut and original : CNRK2 
    flags.timestepping      = SBDF4;
    //flags.dealiasing        = DealiasXZ; //NoDealiasing;
    //string dealiasing_str   = "dealiasXZ";
    flags.dealiasing        = NoDealiasing; //DealiasXZ; //NoDealiasing;
    string dealiasing_str   = "nodealiasing";
    flags.taucorrection     = true;
    flags.constraint        = PressureGradient;
    flags.dPdx              = 0.0;
    //flags.constraint        = BulkVelocity;
    //flags.Ubulk             = Ubulk; //2.0/3.0;
    flags.controlled        = Controlled;
    flags.uupperwall        = +1.0;
    flags.ulowerwall        = -1.0;

    // TimeStep = class, dt = object
    TimeStep dt(dt0, dtmin, dtmax, dT, CFLmin, CFLmax, variable_dt);
    flags.dt                = dt;
    flags.t0                = T0;

    // Make data directory name
    string sign_kx;
    string sign_kz;
    if (kx > 0) {sign_kx = "+";} else {sign_kx = "";}
    if (kz > 0) {sign_kz = "+";} else {sign_kz = "";}

    /*string foldersave = foldersave_base + baseflow_type + "/"
        + "Re=" + i2s(Reynolds) + "/"
        + nonlinearity_str + "/"
        + i2s(Nx_nopad) + "x" + i2s(Ny_nopad) + "x" + i2s(Nz_nopad) + "/"
        + "kx=" + sign_kx + i2s(kx) + "_kz=" + sign_kz + i2s(kz) +"/";
        */
    string foldersave = foldersave_base + baseflow_type + "_"+ baseflow_from +"/"
        + "Re=" + i2s(Reynolds) + "/"
        + nonlinearity_str + "/"
        + "controller=" + i2s(Nx_controller) + "x" + i2s(Ny_controller) + "x" + i2s(Nz_controller)
        + "/" + dealiasing_str
        + "/kappa="+ kappa_str
        + "/tau="+ tau_str + "/"
        + i2s(Nx_nopad) + "x" + i2s(Ny_nopad) + "x" + i2s(Nz_nopad)
        + "_cont=" + (Controlled ? "true" : "false")
        + "_pert=" + (Perturbed ? perturbMag_str : "false") + "/" ;
    //string foldersave = foldersave_base + "test8/u/CHFL_32x65x32/NoDealiasing_48x65x48/";

    // Make data directory
    try {
        string command = "mkdir -p " + foldersave;
        system(command.c_str());
    } catch (int e) {
        cout << "FOLDERSAVE NOT CREATE TO PATH / ERROR " << e <<
            " \n" << foldersave << endl;
        exit(1);
    }

    /////////////////////////////////////////////////////
    // PRINT PARAMETERS
    /////////////////////////////////////////////////////

    cout << endl << "--------------------" << endl;
    cout << setprecision(8); // Sets number of output decimal places
    cout << "Restart        : " << ReStart << endl;
    cout << "Controlled     : " << Controlled << endl;
    cout << "Perturbed      : " << Perturbed << endl;
    cout << "Perturbed Mag  : " << perturbMag_str << endl;
    cout << "Reynolds       : " << Reynolds << endl;
    cout << "Non-linearity  : " << nonlinearity_str << endl;
    cout << "T1             : " << T1 << endl;
    cout << "baseflow       : " << baseflow_type << endl;
    cout << "Nx_nopad       : " << i2s(Nx_nopad) << endl;
    cout << "Ny_nopad       : " << i2s(Ny_nopad) << endl;
    cout << "Nz_nopad       : " << i2s(Nz_nopad) << endl;
    cout << "Nx             : " << i2s(Nx) << endl;
    cout << "Ny             : " << i2s(Ny) << endl;
    cout << "Nz             : " << i2s(Nz) << endl;
    cout << "saving folder  : " << foldersave << endl;

    cout << "CONTROLLER --" << endl;
    cout << "Nx_controller  : " << i2s(Nx_controller) << endl;
    cout << "Ny_controller  : " << i2s(Ny_controller) << endl;
    cout << "Nz_controller  : " << i2s(Nz_controller) << endl;
    cout << "kappa          : " << to_string(kappa) << endl;

    cout << "ACTUATION --" << endl;
    cout << "kx : " << i2s(kx) << endl;
    cout << "kz : " << i2s(kz) << endl;
    cout << "--------------------" << endl << endl;

    /////////////////////////////////////////////////////
    // SIMULATION INITIALIZATION
    /////////////////////////////////////////////////////

    // Diverse
    Vector x = periodicpoints(Nx, Lx); // x = object, periodicpoints = fn
    Vector y = chebypoints(Ny,a,b); // y = object, chebypoints = fn
    Vector z = periodicpoints(Nz, Lz); // z = object, pp = fn

    // FlowField = class
    //FlowField u(Nx,Ny,Nz,3,Lx,Lz,a,b, Spectral, Spectral); // Velocity flowfield (u=object)
    FlowField p(Nx,Ny,Nz,1,Lx,Lz,a,b, Spectral, Spectral); // Pressure flowfield (p=object)

    FlowField F(u);

    // FFTW
    //cout << "Optimising FFTW..." << flush;
    //fftw_loadwisdom();
    //u.optimizeFFTW();
    //fftw_savewisdom();
    //cout << "Done" << endl;

    // Construct DNS
    cout << "Constructing DNS..." << flush;
    // DNS = class, dns = object
    DNS dns(u, flags);
    cout << "Done" << endl;

    FlowField tmp(Nx,Ny,Nz,6,Lx,Lz,a,b);
    ChebyTransform trans(Ny);

    ChebyCoeff Ubase_Stat(Ny,a,b,Physical);
    for (int i=0; i<Ny; ++i)
        Ubase_Stat[i] = y[i];//1 - square(y[i]);
    //Ubase.save("Ubase");
    Ubase_Stat.makeSpectral(trans);

    TurbStats stats(Ubase_Stat, nu);

    /////////////////////////////////////////////////////
    // CONTROLLER
    /////////////////////////////////////////////////////

    // Initialize BCs
    FlowField BC(Nx,2,Nz,3,Lx,Lz,a,b,Physical,Physical);
    // Nx -> size in X periodic, 2 -> upper and lower wall, Nz -> same as Nx, 3 -> 3 velocity components at these points

    for (int ny=0;ny<2;++ny){
        for (int nx=0;nx<Nx;++nx){
            for (int nz=0;nz<Nz;++nz){
                for (int i=0;i<3;++i){
                    BC(nx,ny,nz,i) = 0.0;
                }
            }
        }
    }
    BC.makeState(Spectral, Physical);

    /*
    // Area of wavenumber space controlled
    int minKx = 0;
    int maxKx = 0;
    int minKz = 0;
    int maxKz = 10;

    int maxMx = (maxKx-minKx)+1;
    int maxMz = (maxKz-minKz)+1;
    */

    //int uvw = 1; // u=0, v=1, w=2
    //int NumInputs = 8; // inputs to controller i.e. no. of flow measurements - 2*(lower/upper)*(real/imag)
    //const char* SIFile = "Mult_Control_Mat/StateInfo.bin"; //state info file

    //bool Spectral_states = false;

    //cout << "kappa : " << typeid(kappa).name() << endl;
    //cout << "kappa : " << to_string(kappa) << endl;
    Controller_ROSSE_via_OSSE Kcont(
            u,
            Nx_controller,
            Ny_controller,
            Nz_controller,
            home+"osse/database/ROSSE/"+baseflow_type+"_"+baseflow_from+"/Re="+i2s(Reynolds)+"/"
                +i2s(Nx_controller)+"x"+i2s(Ny_controller)+"x"+i2s(Nz_controller)
                +"/kappa="+kappa_str+"_tau="+tau_str
                +"/K_CHFL_ROSSE_via_OSSE.h5",
            home+"osse/database/eq/"+baseflow_type+"_"+i2s(Nx_nopad)+"x"+i2s(Ny_nopad)+"x"+i2s(Nz_nopad)+"_"+baseflow_from+".h5",
            home+"osse/database/ROSSE/"+baseflow_type+"_"+baseflow_from+"/Re="+i2s(Reynolds)+"/"
                +i2s(Nx_controller)+"x"+i2s(Ny_controller)+"x"+i2s(Nz_controller)
                +"/kappa="+kappa_str+"_tau="+tau_str
                +"/CT_OSSE.h5",
            home+"osse/database/ROSSE/"+baseflow_type+"_"+baseflow_from+"/Re="+i2s(Reynolds)+"/"
                +i2s(Nx_controller)+"x"+i2s(Ny_controller)+"x"+i2s(Nz_controller)
                +"/kappa="+kappa_str+"_tau="+tau_str
                +"/CTinv_OSSE.h5",
            tau);
           //home+"osse/database/eq/"+baseflow_type+"_32x35x32.h5",
           //baseflow_file,
    //Kcont.test0();
    //Kcont.test1();
    //return(0);

    //double*** CStateMat = Kcont.ConStates(); // controller state array
    double*** CStateMat; // controller state array

    // From UserGuide.pdf :
    // IO is an array created on lines 171-187, and stores inputs and outputs from the controller
    // at each time-step for printing to files.
    // This array has been set up for the current number of inputs and outputs
    // and may need to be changed for your setup (both in the run file, and controller.cpp).
    // 3D input/output array
    double *** IO; // input/output data array
    //int NIO = 0;
    /*
    int NIO = maxMx*maxMz; // no. of kx,kz pairs
    double *** IO; // input/output data array
    IO = new double**[2];

    for (int i=0;i<2;++i){
        IO[i] = new double*[3+NumInputs];
        for (int j=0;j<3+NumInputs;++j){
            IO[i][j] = new double[NIO];
        }
    }

    for (int i=0;i<2;++i){
        for (int j=0;j<3+NumInputs;++j){
            for (int k=0;k<NIO;++k){
                IO[i][j][k] = 0.0;
            }
        }
    }
    */

    /*
    if (ReStart){
        // Load controller states
        cout << "Loading Controller States: ";
        double** CStateInf = Kcont.CStateInfo();
        fstream ConStatesfile(foldersave + "Controller_States.asc", ios_base::in|ios_base::out);
        ConStatesfile.seekg(0);
        for (int i=0;i<(maxKx-minKx+1);++i){
            for (int j=0;j<(maxKz-minKz+1);++j){
                if (i==0 && j==0){
                    continue;
                }
                else {
                    //cout << CStateInf[i][j] << endl;
                    for (int k=0;k<CStateInf[i][j];++k){
                        ConStatesfile >> CStateMat[i][j][k];
                    }
                }
            }
        }
        ConStatesfile.close();
        cout << "Done" << endl;

        // Load BCs
        cout << "loading BCs:";
        fstream BCfile(home+"/channelflow/database/PHD/Controller_run/BCs.asc", ios_base::in|ios_base::out);
        ConStatesfile.seekg(0);
        for (int UL=0;UL<2;++UL){
            for (int imxc=0;imxc<maxMx;++imxc){
                int ikx = Kcont.kx_c(imxc);
                int imx = u.mx(ikx);
                for (int imz=0;imz<maxMz;++imz){
                    BCfile >> BC.cmplx(imx,UL,imz,uvw);
                }
            }
        }
        BCfile.close();
        cout << "Done" << endl;
    }
    */
    // \fi(ReStart)

    /////////////////////////////////////////////////////
    // DATA OUTPUT
    /////////////////////////////////////////////////////

    mkdir(foldersave+"Input_Output");

    ofstream Outfile;
    Outfile.open((foldersave+"Input_Output/Shear_Stresses.asc").c_str());
    ofstream Infile;
    Infile.open((foldersave+"Input_Output/Control_Signals.asc").c_str());
    ofstream Dragfile;
    Dragfile.open((foldersave+"Input_Output/Drag.asc").c_str());
    ofstream Ustarfile;
    Ustarfile.open((foldersave+"Input_Output/Ustar.asc").c_str());
    ofstream Energyfile;
    Energyfile.open((foldersave+"Input_Output/Energy.asc").c_str());
    ofstream EnergyMxMz_File;
    EnergyMxMz_File.open((foldersave+"Input_Output/EnergyMxMz.asc").c_str());

    Outfile << "Sim Output / Controller Input \nTime  kx   kz  Str_R_Up  Str_I_Up   Str_R_Low   Str_I_Low Span_R_Up  Span_I_Up   Span_R_Low   Span_I_Low  \n";
    Infile << "Sim Input / Controller Output  \nTime  kx   kz  R_Up  I_Up   R_Low   I_Low\n";
    Dragfile << "Time  Drag_Lower  Drag_Upper\n";
    Ustarfile << "Time   Ustar\n";
    Energyfile << "Time  Perturbation Energy\n";
    EnergyMxMz_File << "Time   kx   kz   Energy\n";

    /////////////////////////////////////////////////////
    // Time-stepping loop - Controller and sim
    /////////////////////////////////////////////////////

    int it = 0; // time-step counter

    // it seems that reset_dtIH has an impact on the calculation precision
    //if (Controlled || Perturbed){
    //    dns.reset_dtIH(dt,BC);
    //}

    for (double t=T0; t<T1+(dT/2); t += dT) {

        if (false && Perturbed && t >= T_perturbed) {
            cout << "Perturbation on (" << i2s(kx) << ", " << i2s(kz) << ") [and conjugate]" << endl;
            int mx = BC.mx(kx);
            int mz = BC.mz(kz);

            // upper wall actuation
            BC.cmplx(mx,1,mz,1) = 1 * sin(2*pi*t/10) * forcing_mag;
            cout << "   BC upper " << mx << " / " << mz << "== " << BC.cmplx(mx,1,mz,1) << endl;

            // lower wall actuation
            BC.cmplx(mx,0,mz,1) = 1 * sin(2*pi*t/10) * forcing_mag;
            cout << "   BC lower " << mx << " / " << mz << "== " << BC.cmplx(mx,0,mz,1) << endl;

        } else if (Perturbed && t == T_perturbed) {
            cout << "Perturbation on the entire flowfield" << endl;
            const int kxmax = 3;
            const int kzmax = 3;
            const Real decay = 0.5;
            u.addPerturbations(kxmax,kzmax,perturbMag,decay); // Add pertubations to base flow
            //u *= perturbMag/L2Norm(u);
        }

        if (false) {
            cout << "Print BC" << endl;
            int KxC_1 = kx; // For printing BC at this pair to screen
            int KzC_1 = kz;
            int mxC_1 = BC.mx(KxC_1);
            int mzC_1 = BC.mz(KzC_1);

            int KxC_2 = -kx; // For printing BC at this pair to screen
            int KzC_2 = kz;
            int mxC_2 = BC.mx(KxC_2);
            int mzC_2 = BC.mz(KzC_2);

            Real RUpBC_1 = real(BC.cmplx(mxC_1,1,mzC_1,1)); // to print to screen
            Real IUpBC_1 = imag(BC.cmplx(mxC_1,1,mzC_1,1));
            Real RLowBC_1 = real(BC.cmplx(mxC_1,0,mzC_1,1));
            Real ILowBC_1 = imag(BC.cmplx(mxC_1,0,mzC_1,1));

            Real RUpBC_2 = real(BC.cmplx(mxC_2,1,mzC_2,1)); // to print to screen
            Real IUpBC_2 = imag(BC.cmplx(mxC_2,1,mzC_2,1));
            Real RLowBC_2 = real(BC.cmplx(mxC_2,0,mzC_2,1));
            Real ILowBC_2 = imag(BC.cmplx(mxC_2,0,mzC_2,1));

            cout << "Kx="+i2s(KxC_1)+", Kz="+i2s(KzC_1)+" Upper BC ==" << RUpBC_1 << "+(" << IUpBC_1 << ")i" << endl;
            cout << "Kx="+i2s(KxC_1)+", Kz="+i2s(KzC_1)+" Lower BC ==" << RLowBC_1 << "+(" << ILowBC_1 << ")i" << endl;
            cout << "Kx="+i2s(KxC_2)+", Kz="+i2s(KzC_2)+" Upper BC ==" << RUpBC_2 << "+(" << IUpBC_2 << ")i" << endl;
            cout << "Kx="+i2s(KxC_2)+", Kz="+i2s(KzC_2)+" Lower BC ==" << RLowBC_2 << "+(" << ILowBC_2 << ")i" << endl;
        }

        if (false) {
            cout << "Wall-normal velocity BC at upper wall" << endl;
            cout << "mx|mz : ";
            int mx; int mz;
            for (int kz = 0; kz < Nz; kz++) {
                cout <<  BC.mz(kz) << " | ";
            } cout << endl;

            for (int kx = 0; kx < Nx; kx++) {
                mx = BC.mx(kx);
                cout << mx << "     : ";
                for (int kz = 0; kz < Nz; kz++) {
                    mz = BC.mx(kz);
                    cout << real(BC.cmplx(mx,1,mz,1)) <<"+(" << imag(BC.cmplx(mx,1,mz,1)) << ")i | ";
                } cout << endl;
            } cout << "------------------------------" << endl << endl;
        }

        //------------------------------------------------
        // Wall-law and turb stats
        //------------------------------------------------
        ChebyCoeff u00 = Re(u.profile(0,0,0));
        ChebyCoeff du00dy = diff(u00);
        Real drag_L = nu*du00dy.eval_a();
        Real drag_U = nu*du00dy.eval_b();

        Real Energy = L2Norm2(u);

        u00.makePhysical();
        u00.save(foldersave+"u00");
        stats.addData(u,tmp);

        stats.msave(foldersave+"uu");
        stats.msave(foldersave+"uustar", true);

        Real ustar = stats.ustar();
        Vector yp = stats.yplus();
        yp.save(foldersave+"yp");

        ChebyCoeff Umean = stats.U();
        Umean.makeSpectral(trans);
        ChebyCoeff Umeany = diff(Umean);
        Umean.makePhysical(trans);
        Umean.save(foldersave+"Umean");
        Umeany.makePhysical(trans);
        Umeany.save(foldersave+"Umeany");

        Umean /= ustar;
        Umean.save(foldersave+"Uplus");

        ChebyCoeff ubase = stats.ubase();
        ubase.save(foldersave+"ubase");

        ChebyCoeff uv = stats.uv();
        uv.save(foldersave+"uv");
        save(ustar, foldersave+"ustar");
        save(nu, foldersave+"nu");

        Ustarfile << t << "  " << ustar << endl;
        Dragfile << t << " " << drag_L << " " << drag_U << endl;
        Energyfile << t << " " << Energy << endl;

        // Record energy at each mx,mz
        for (int imx=0;imx<1;++imx){
            for (int imz=0;imz<u.Mz();++imz){
                EnergyMxMz_File << t << " " << u.kx(imx) << " " << u.kz(imz) << " " << u.energy(imx,imz,true) << endl;
            }
        }

        cout << "          t == " << t << endl;
        cout << "         dt == " << dt << endl;
        cout << "        CFL == " << dns.CFL() << endl;
        cout << " L2Norm2(u) == " << Energy << endl;
        cout << "divNorm2(u) == " << divNorm(u)/L2Norm(u) << endl;
        cout << "      Ubulk == " << dns.Ubulk() << endl;
        cout << "      ubulk == " << Re(u.profile(0,0,0)).mean()/2 << endl;
        cout << "       dPdx == " << dns.dPdx() << endl;
        cout << "       drag == " << 0.5*(abs(drag_L)+abs(drag_U)) << endl;
        cout << "--------------STATS---------------" << endl;
        cout << "centerline Re = " << stats.centerlineReynolds() << endl;
        cout << " parabolic Re = " << stats.parabolicReynolds() << endl;
        cout << "      bulk Re = " << stats.bulkReynolds() << endl;
        cout << "        ustar = " << stats.ustar() << endl;
        cout << "--------------NORMS---------------" << endl;
        cout << "  L2Norm(u) == " << L2Norm(u) << endl;
        cout << "L2Dist(u,u0)== " << L2Dist(u,u0) << endl;
        cout << endl << "------------------------------" << endl;

        //------------------------------------------------
        // SAVE ITERATION
        //------------------------------------------------

        if (it % itSave == 0 || it == nSave)
        {
            cout << "Saving flowfields..." << endl;
            u.save(foldersave+"u"+i2s(iround(t)));
            p.save(foldersave+"p"+i2s(iround(t)));
            BC.save(foldersave+"BC"+i2s(iround(t)));
            cout << "done" << endl;

            /*
            cout << "Saving Controller States:";
            // Save Controller States to file
            double** CStateInf = Kcont.CStateInfo();
            ofstream ConStatesfile;
            ConStatesfile.open(home+"/channelflow/database/PHD/Controller_run/Controller_States.asc");
            for (int i=0;i<(maxKx-minKx+1);++i){
                for (int j=0;j<(maxKz-minKz+1);++j){
                    for (int k=0;k<CStateInf[i][j];++k){
                        if (i==0 && j==0){
                            continue;
                        }
                        else {
                            ConStatesfile << CStateMat[i][j][k] << endl;
                        }
                    }
                }
            }
            ConStatesfile.close();
            cout << " Done" << endl;
            //*/

            /*
            cout << "Saving BCs:";
            // Save BCs
            ofstream BCfile;
            BCfile.open(home+"/channelflow/database/PHD/Controller_run/BCs.asc");
            for (int UL=0;UL<2;++UL){
                for (int imxc=0;imxc<maxMx;++imxc){
                    int ikx = Kcont.kx_c(imxc);
                    int imx = u.mx(ikx);
                    for (int imz=0;imz<maxMz;++imz){
                        BCfile << BC.cmplx(imx,UL,imz,uvw) << endl;
                    }
                }

            }
            BCfile.close();
            cout << " Done" << endl;
            //*/
        } // \fi itSave

        //------------------------------------------------
        // ADVANCE SIMULATION
        //------------------------------------------------

        if (dt.adjust(dns.CFL()))
        {
            cerr << "resetting dt to " << dt << ", new CFL ==" << dt.CFL() << endl;
            if ((Controlled && t > T_controller_start) || (Perturbed and t>= T_perturbed))
            {
                cout << "controlled reset" << endl;
                dns.reset_dtIH(dt,BC); // Controlled
            }
            else
            {
                cout << "dt reset" << endl;
                dns.reset_dt(dt); // Not controlled
            }
        }

        if (dns.CFL()>1.0)
        {
            cout << "Error: CFL>1" << endl;
            return(0);
        }

        if (Controlled && t >= T_controller_start)
        {
            cout << "advance K" << endl;
            dns.advance_inhom_CON(Kcont,u,p,BC,F,dt.n(),IO,CStateMat); // Controlled
        }
        else if (Perturbed && t >= T_perturbed)
        {
            cout << "advance P" << endl;
            dns.advance_inhom(u,p,BC,dt.n()); // Perturbed
        }
        else
        {
            cout << "advance" << endl;
            dns.advance(u,p,dt.n()); // Not controlled
        }

        // Print IO data to file
        /*
        for (int k=0;k<NIO;++k){
            for (int j=0;j<3+NumInputs;++j){
                if (j<3+NumInputs-1){
                    Outfile << IO[0][j][k] << " ";
                }
                else {
                    Outfile << IO[0][j][k] << endl;
                }
            }
        }

        for (int k=0;k<NIO;++k){
            for (int j=0;j<3+4;++j){
                if (j<7-1){
                    Infile << IO[1][j][k] << " ";
                }
                else {
                    Infile << IO[1][j][k] << endl;
                }
            }
        }
        */

        it +=1; // timestep index
        cout << endl;
    } // \fi Time-stepping loop - Controller and sim

    cout << "Done!" << endl;

    Outfile.close();
    Infile.close();
    Dragfile.close();
    Ustarfile.close();
    Energyfile.close();
    EnergyMxMz_File.close();
}
