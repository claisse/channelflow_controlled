//
//  BC_Run.cpp
//
//
//  Created by Peter Heins on 27/05/2015.
//  Copyright 2015 University of Sheffield. All rights reserved.

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"
#include "channelflow/controller.h"
#include "channelflow/turbstats.h"

using namespace std;
using namespace channelflow;

int main(){
    string user             = "gcpc1m15";
    bool ReStart            = false;
    bool Controlled         = true;

    // Actuated mode
    int kx = +1;
    int kz = +1;
    double forcing = 0.0005;
    string forcing_str = i2s(forcing); //"0.0005";
    cout << "kx : " << i2s(kx) << endl;
    cout << "kz : " << i2s(kz) << endl;

    string baseflow_type = "eq1"; // "couette"
    // add condition on flowfield creation depending on baseflow_type
    string filesave_base = "/home/"+user+"/channelflow/database/PHD/CFBC_validation/";

    const int Nx_nopad      = 20;//20;
    const int Ny_nopad      = 65;
    const int Nz_nopad      = 20;//20;
    cout << "Nx_nopad : " << i2s(Nx_nopad) << endl;
    cout << "Ny_nopad : " << i2s(Ny_nopad) << endl;
    cout << "Nz_nopad : " << i2s(Nz_nopad) << endl;

    //*
    int delta_t = 0;
    FlowField u("/home/"+user+"/osse/database/eq/eq1_"
            +i2s(Nx_nopad)+"x"+i2s(Ny_nopad)+"x"+i2s(Nz_nopad));
    //*/

    /*
    int delta_t = 10;
    // Velocity flowfield (u=object)
    FlowField u("/home/"+user+"/osse/database/eq/eq1_cf/eq1_"
    +i2s(Nx_nopad)+"x"+i2s(Ny_nopad)+"x"+i2s(Nz_nopad)
    +"_m="+i2s(kx)+"x"+i2s(kz)
    +"_f="+forcing_str+
    +"_t="+i2s(delta_t));
    //*/

    /*
    int delta_t = 10;
    // Velocity flowfield (u=object)
    FlowField u("/home/"+user+"/osse/database/eq/eq1_"
    +i2s(Nx_nopad)+"x"+i2s(Ny_nopad)+"x"+i2s(Nz_nopad)); // Velocity flowfield (u=object)

    cout << "X0 : " << "/home/"+user+"/osse/database/eq/eq1_wd/eq1_"
    +i2s(Nx_nopad)+"x"+i2s(Ny_nopad)+"x"+i2s(Nz_nopad)
    +"_m="+i2s(kx)+"x"+i2s(kz)
    +"_f="+forcing_str+
    +"_t="+i2s(10) << endl;

    FlowField u_perturb("/home/"+user+"/osse/database/eq/eq1_wd/eq1_"
    +i2s(Nx_nopad)+"x"+i2s(Ny_nopad)+"x"+i2s(Nz_nopad)
    +"_m="+i2s(kx)+"x"+i2s(kz)
    +"_f="+forcing_str+
    +"_t="+i2s(10)
    );
    u += u_perturb;
    //*/

    // Num of data points in x, y and z directions
    const int Nx            = u.Nx();
    const int Ny            = u.Ny();
    const int Nz            = u.Nz();

    // Size of domain
    const Real Lx           = u.Lx(); //2*pi/1.14;
    const Real a            = u.a(); //-1.0;
    const Real b            = u.b(); //1.0;
    const Real Lz           = u.Lz(); //2*pi/2.5;

    // Define flow parameters
    const Real Reynolds     = 400.0;
    const Real nu           = 1/Reynolds; // Kinematic viscosity
    //const Real dPdx         = 0.0;
    //const Real Ubulk        = 0.0;
    cout << "Reynolds : " << i2s(Reynolds) << endl;

    // Variable time-stepping parameters
    const Real CFLmin       = 0.10;
    const Real CFLmax       = 0.30; // 0.70
    const Real dtmax        = 0.01; // <<<< CONTROLLER time interval (is it tau ?)
    const Real dtmin        = 0.00001;//0.000010;
    const Real dt0          = dtmax;
    const bool variable_dt  =true;
    const Real T0           = 0.0; // Start time
    const Real T1           = 150.0; // End time
    const Real dT           = 1; // dtmax; //
    const Real TSave        = 1; // time interval to save data

    // Save parameters
    Real SaveInt = T1-T0;
    int nSave = iround(SaveInt/dT);
    int itSave = iround(TSave/dT); // time step interval to save data

    DNSFlags flags; // DNSFlags = class, flags = object
    flags.baseflow          = LinearBase; // Couette laminar solution, unitary
    flags.nonlinearity      = SkewSymmetric;//LinearAboutProfile;
    flags.initstepping      = CNRK2; // SBDF1 | CNRK2 | SMRK2 : defaut and original : CNRK2 
    flags.timestepping      = SBDF4;
    flags.dealiasing        = DealiasXZ; //NoDealiasing;
    //flags.constraint        = PressureGradient;
    //flags.dPdx              = dPdx;
    //flags.constraint        = BulkVelocity;
    //flags.Ubulk             = Ubulk;
    flags.controlled        = Controlled;

    // TimeStep = class, dt = object
    TimeStep dt(dt0, dtmin, dtmax, dT, CFLmin, CFLmax, variable_dt);
    flags.nu = nu;
    flags.dt = dt;
    flags.t0 = T0;

    // Make data directory
    string nonlinear_str;
    if (flags.nonlinearity == SkewSymmetric) {
        nonlinear_str = "skewsymmetric";
    } else if (flags.nonlinearity == LinearAboutProfile) {
        nonlinear_str = "linearaboutprofile";
    } else {
        cout << "Nonlinearity folder path not defined." << endl;
        exit(1);
    }

    string sign_kx;
    string sign_kz;
    if (kx > 0) {sign_kx = "+";} else {sign_kx = "";}
    if (kz > 0) {sign_kz = "+";} else {sign_kz = "";}

    string filesave = filesave_base
        + nonlinear_str + "/"
        + baseflow_type + "/"
        + "Re=" + i2s(Reynolds) + "/"
        + i2s(Nx_nopad) + "x" + i2s(Ny_nopad) + "x" + i2s(Nz_nopad) + "/"
        + "kx=" + sign_kx + i2s(kx) + "_kz=" + sign_kz + i2s(kz) +"/";
    try {
        string command = "mkdir -p " + filesave;
        system(command.c_str());
    } catch (int e) {
        cout << "FILESAVE NOT CREATE TO PATH / ERROR " << e <<
            " \n" << filesave << endl;
        exit(1);
    }

    // 
    cout << setprecision(8); // Sets number of output decimal places

    Vector x = periodicpoints(Nx, Lx); // x = object, periodicpoints = fn
    Vector y = chebypoints(Ny,a,b); // y = object, chebypoints = fn
    Vector z = periodicpoints(Nz, Lz); // z = object, pp = fn

    // FlowField = class
    //FlowField u(Nx, Ny, Nz, 3,Lx,Lz,a,b); // Velocity flowfield (u=object)
    FlowField q(Nx,Ny,Nz,1,Lx,Lz,a,b); // Pressure flowfield (q=object)

    if (false){
        const int kxmax = 4;
        const int kzmax = 4;
        const Real decay = 0.5;
        const Real perturbMag = 0.10;
        u.addPerturbations(kxmax,kzmax,1,decay); // Add pertubations to base flow
        u *= perturbMag/L2Norm(u);
    }

    //cout << "Optimising FFTW..." << flush;
    //fftw_loadwisdom();
    //u.optimizeFFTW();
    //fftw_savewisdom();
    //cout << "Done" << endl;

    // Construct DNS
    cout << "Constructing DNS..." << flush;
    // DNS = class, dns = object
    DNS dns(u, flags);
    cout << "Done" << endl;

    FlowField tmp(Nx,Ny,Nz,6,Lx,Lz,a,b);
    ChebyTransform trans(Ny);

    ChebyCoeff Ubase_Stat(Ny,a,b,Physical);
    for (int i=0; i<Ny; ++i)
        Ubase_Stat[i] = y[i];//1 - square(y[i]);
    //Ubase.save("Ubase");
    Ubase_Stat.makeSpectral(trans);

    TurbStats stats(Ubase_Stat, nu);

    //*******---BCs ---*******************************
    // ***********************************************
    FlowField BC(Nx,2,Nz,3,Lx,Lz,a,b,Physical,Physical);
    // Nx -> size in X periodic, 2 -> upper and lower wall, Nz -> same as Nx, 3 -> 3 velocity components at these points

    for (int ny=0;ny<2;++ny){
        for (int nx=0;nx<Nx;++nx){
            for (int nz=0;nz<Nz;++nz){
                for (int i=0;i<3;++i){
                    BC(nx,ny,nz,i) = 0.0;
                }
            }
        }
    }

    BC.makeState(Spectral,Physical);

    cout << BC.Mx() << endl;
    cout << BC.Mz() << endl;

    /*
    if (ReStart){
        // Load BCs
        cout << "loading BCs:";
        fstream BCfile(filesave+"BCs.asc", ios_base::in|ios_base::out);
        for (int UL=0;UL<2;++UL){
            for (int imx=0;imx<u.Mx();++imx){
                for (int imz=0;imz<u.Mz();++imz){
                    for (int uvw=0;uvw<3;++uvw){
                        BCfile >> BC.cmplx(imx,UL,imz,uvw);
                    }
                }
            }
        }
        BCfile.close();
        cout << "Done" << endl;
    }
    */

    //*******************************************************

    int it=0; // time-step counter

    if (Controlled){
        dns.reset_dtIH(dt,BC);
    }

    // Time-stepping loop - Controller and sim
    for (double t=T0; t<T1+(dT/2); t += dT) {

        //*****************************************************
        // Assign BCs to v-component on kx=0,kz=1 at both walls
        // kx = 0, kz = 0, 1, -1
        //*****************************************************
        int mx = BC.mx(kx);
        int mz = BC.mz(kz);
        if (kx == 0 && kz == 0) {

        }
        else if (kx == +1 && kz == 0) {
            BC.cmplx(mx,1,mz,1) = 1 * sin(2*pi*t/10) * forcing * Complex(0.0, 1.0);
            //1 * (1 - exp(-(t+delta_t)/10)) * forcing;
            //* cos(0.05 *t + pi/2) * (0.05 - 0.01 * Complex(0.0, 1.0));
            BC.cmplx(mx,0,mz,1) = 1 * sin(2*pi*t/10) * forcing * Complex(0.0, 1.0);
            //1 * (1 - exp(-(t+delta_t)/10)) * forcing;
            //* cos(0.05 *t + pi/2) * (0.05 - 0.01 * Complex(0.0, 1.0));
        }
        else{
            BC.cmplx(mx,1,mz,1) = 1 * sin(2*pi*t/10) * forcing * Complex(0.0, 1.0);
            //1 * (1 - exp(-(t+delta_t)/10)) * forcing;
            //cos(0.05 *t + pi/2) * (0.05 - 0.01 * Complex(0.0, 1.0));
            BC.cmplx(mx,0,mz,1) = 1 * sin(2*pi*t/10) * forcing * Complex(0.0, 1.0);
            // 1 * (1 - exp(-(t+delta_t)/10)) * forcing;
            ////cos(0.05 *t + pi/2) * (0.05 - 0.01 * Complex(0.0, 1.0));
            // 1st nb : kx ,
            // 2nd nb : Y position (upper or lower wall),
            // 3th nb : kz,
            // 4th nb : which velocity component (u=0, v=1, w=2)
        }

        ChebyCoeff u00 = Re(u.profile(0,0,0));
        ChebyCoeff du00dy = diff(u00);
        Real drag_L = nu*du00dy.eval_a();
        Real drag_U = nu*du00dy.eval_b();

        Real Energy = L2Norm2(u);

        cout << "          t == " << t << endl;
        cout << "         dt == " << dt << endl;
        cout << "        CFL == " << dns.CFL() << endl;
        cout << " L2Norm2(u) == " << Energy << endl;
        cout << "divNorm2(u) == " << divNorm(u)/L2Norm(u) << endl;
        cout << "      Ubulk == " << dns.Ubulk() << endl;
        cout << "      ubulk == " << Re(u.profile(0,0,0)).mean()/2 << endl;
        cout << "       dPdx == " << dns.dPdx() << endl;
        cout << "       drag == " << 0.5*(abs(drag_L)+abs(drag_U)) << endl;
        
        cout << "   BC upper == " << BC.cmplx(mx,1,mz,1) << endl;
        cout << "   BC lower == " << BC.cmplx(mx,0,mz,1) << endl;
        
        //--------------------------------------------
        //--------------------------------------------
        // Wall-law and turb stats
        u00.makePhysical();
        u00.save(filesave+"u00");
        stats.addData(u,tmp);
        cout << "centerline Re = " << stats.centerlineReynolds() << endl;
        cout << " parabolic Re = " << stats.parabolicReynolds() << endl;
        cout << "      bulk Re = " << stats.bulkReynolds() << endl;
        cout << "        ustar = " << stats.ustar() << endl;
        
        stats.msave(filesave+"uu");
        stats.msave(filesave+"uustar", true);
        
        Real ustar = stats.ustar();
        Vector yp = stats.yplus();
        yp.save(filesave+"yp");
        
        ChebyCoeff Umean = stats.U();
        Umean.makeSpectral(trans);
        ChebyCoeff Umeany = diff(Umean);
        Umean.makePhysical(trans);
        Umeany.makePhysical(trans);
        Umean.save(filesave+"Umean");
        Umeany.save(filesave+"Umeany");
        
        Umean /= ustar;
        Umean.save(filesave+"Uplus");
        
        ChebyCoeff ubase = stats.ubase();
        ubase.save(filesave+"ubase");
        
        ChebyCoeff uv = stats.uv();
        uv.save(filesave+"uv");
        save(ustar, filesave+"ustar");
        save(nu, filesave+"nu");
        
        //-------------------------------------
        //-------------------------------------
        
        if (it % itSave == 0 || it == nSave) {
            cout << "Saving flowfields:";
            //u.makeState(Spectral,Physical);
            //cout << u.xzstate() << endl;
            //u.makeSpectral_xz();
            u.save(filesave+"u"+i2s(iround(t)));
            //u.makeState(Spectral,Spectral);
            //cout << u.xzstate() << endl;
            //q.save("/home/"+user+"/channelflow/database/PHD/CFBC_validation/q"+i2s(iround(t)));
            cout << " Done" << endl;
            
            cout << "Saving BCs:";
            
            // Save BCs
            ofstream BCfile;
            string BCfile_name = filesave+"BCs.asc";
            BCfile.open(BCfile_name.c_str());
            for (int UL=0;UL<2;++UL){
                for (int imx=0;imx<u.Mx();++imx){
                    for (int imz=0;imz<u.Mz();++imz){
                        for (int uvw=0;uvw<3;++uvw){
                            BCfile << BC.cmplx(imx,UL,imz,uvw) << endl;
                        }
                    }
                }
            }
            BCfile.close();
            cout << " Done" << endl;
        }
        
        if (dt.adjust(dns.CFL())) {
            cerr << "resetting dt to " << dt << ", new CFL ==" << dt.CFL() << endl;
            if (Controlled){
                dns.reset_dtIH(dt,BC); // Controlled
            }
            else {
                dns.reset_dt(dt); // Not controlled
            }
        }
        
        if (dns.CFL()>1.0){
            cout << "Error: CFL>1" << endl;
            return(0);
        }
        
        
        if (Controlled){
            dns.advance_inhom(u,q,BC,dt.n()); // Controlled
        }
        else {
            dns.advance(u,q,dt.n()); // Not controlled
        }
        
        it +=1; // timestep index
        cout << endl;
    }
    cout << "Done!" << endl;
}      
