//
// Created by geof on 09/06/19.
//

#include <stdlib.h>
#include <assert.h>
#include <Eigen/Dense>
#include <redsvd/redsvd.hpp>
#include <redsvd/redsvdFile.hpp>

#include "H5Cpp.h"

// TODO get this method out of channelflow project, as a standalone
// TODO how to get the smallest singular values ?
//   -> dont see any option in Eigen::SVDBase to choose which values to target
//   -> the non-zero singular values of A are the square roots of the non-zero eigenvalues of A* A or A A*
//       see if I can get the smallest eigenvalues ?
// Otherwise: find a way to get R by inversing R_inv ?
// TODO get resolvent matrix -> hdf5read_complex
// can REDSVD work wit complex matrices ?
//   -> it is based on Eigen::JacobiSVD, so it might work with complex matrices
//      if Eigen::Jacobi can handle it
//   -> create method in redsvd.hpp to handle MatrixXcf

const float EPS = 0.001f;

int hdf5read_real(Eigen::MatrixXf& matrix, H5::H5File& h5file)
{
    // try block to detect raised exceptions.
    try
    {
        // Turn-off the printing of exception so they are catched later.
        H5::Exception::dontPrint();

        // Open the dataset in h5file corresponding to the matrix
        H5::DataSet dataset = h5file.openDataSet("mat");

        // Get the dataspace of this dataset
        H5::DataSpace dataspace = dataset.getSpace();

        // Get the dimension given by the dataspace, and display them
        int rank = dataspace.getSimpleExtentNdims();
        if (rank != 2) {
            std::cout << "Matrix rank should be 2." << std::endl;
        }
        hsize_t dims_out[rank];
        dataspace.getSimpleExtentDims(dims_out, nullptr);
        //dataspace.getSimpleExtentDims(dims_out, nullptr);

        int N = dims_out[0] * dims_out[1];

        /*
        cout << rank << " / Dimensions : "
            << (unsigned long) (dims_out[0]) << " x "
            << (unsigned long) (dims_out[1]) << " / "
            << N << endl;
        //*/

        if ((unsigned) dims_out[0] != matrix.rows()
                || (unsigned) dims_out[1] != matrix.cols())
        {
            std::cout << "Expected dimensions given differ from array dimension in hdf5read(matrix, ...)" << std::endl;
        }

        // Define the memory dataspace
        // corresponding to the dataset matrix size
        H5::DataSpace memspace(rank, dims_out);

        // Read dataspace into matrix
        auto* buff = new double[N];
        dataset.read(buff, H5::PredType::IEEE_F64BE, memspace, dataspace);
        H5Tconvert(H5T_IEEE_F64BE,
                   H5T_NATIVE_DOUBLE,
                   N, buff, nullptr, H5P_DEFAULT);
        //H5T_NATIVE_DOUBLE,

        for (size_t ii = 0; ii < dims_out[0]; ii++) {
            for (size_t jj = 0; jj < dims_out[1]; jj++) {
                matrix(ii, jj) = buff[ii * dims_out[1] + jj];
            }
        }

        dataset.close();
        delete[] buff;
    } // end of try block

    // catch failure caused by H5::H5File operations
    catch( H5::FileIException &error ) {
        std::cout << "Error hdf5read_real FileIException" << std::endl;
        //error.printError();
        std::cout << error.getDetailMsg() << std::endl;
        std::cout << error.getFuncName() << std::endl;
        return -1;
    }
    // catch failure caused by H5::DataSet operations
    catch( H5::DataSetIException &error ) {
        std::cout << "Error hdf5read_real DataSetIException" << std::endl;
        //error.printError();
        std::cout << error.getDetailMsg() << std::endl;
        std::cout << error.getFuncName() << std::endl;
        return -1;
    }
    // catch failure caused by H5::DataSpace operations
    catch( H5::DataSpaceIException &error ) {
        std::cout << "Error hdf5read_real DataSpaceIEception" << std::endl;
        //error.printError();
        std::cout << error.getDetailMsg() << std::endl;
        std::cout << error.getFuncName() << std::endl;
        return -1;
    }
    // catch failure caused by H5::DataType operations
    catch( H5::DataTypeIException &error ) {
        std::cout << "Error hdf5read_real DataTypeIException" << std::endl;
        //error.printError();
        std::cout << error.getDetailMsg() << std::endl;
        std::cout << error.getFuncName() << std::endl;
        return -1;
    }
    // successfully executed
    return 0;
}

int MatIn_hdf5_real(
        Eigen::MatrixXf& matrix,
        const std::string& filename)
{
    using namespace H5;
    // Open the specifield HDF5 file
    H5::H5File h5file(filename, H5F_ACC_RDONLY);

    // Read the new specified file to get the matrix
    hdf5read_real(matrix, h5file);
    return 0;
}

void test_random(){
    int rowN = 20;
    int colN = 70;
    Eigen::MatrixXf A = Eigen::MatrixXf::Random(rowN, colN);

    int r = 20;
    REDSVD::RedSVD redsvd;

    redsvd.run(A, r);
    Eigen::MatrixXf U = redsvd.matrixU();
    Eigen::VectorXf S = redsvd.singularValues();
    Eigen::MatrixXf V = redsvd.matrixV();

    std::cout << S << std::endl;

    assert(rowN == U.rows());
    assert(r == U.cols());
    assert(colN == V.rows());
    assert(r == V.cols());
    assert(r == S.rows());
    assert(1 == S.cols());

    for (int i = 0; i < U.cols(); ++i){
        assert(std::abs(1.f - U.col(i).norm()) < EPS);
        for (int j = 0; j < i; ++j){
            assert(std::abs(0.f - U.col(i).dot(U.col(j))) < EPS);
        }
    }

    for (int i = 0; i < V.cols(); ++i){
        assert(std::abs(1.f - V.col(i).norm()) < EPS);
        for (int j = 0; j < i; ++j){
            assert(std::abs(0.f, V.col(i).dot(V.col(j))) < EPS);
        }
    }

    assert(std::abs(0.f - (A - U * S.asDiagonal() * V.transpose()).norm()) < EPS);
};

void test_random_cmplx(){
    int rowN = 20;
    int colN = 70;
    Eigen::MatrixXf A = Eigen::MatrixXf::Random(rowN, colN);
    /*
    Eigen::MatrixXcf Ar = Eigen::MatrixXcf::Random(rowN, colN);
    Eigen::MatrixXcf Ai = Eigen::MatrixXcf::Random(rowN, colN);
    Eigen::MatrixXcf A = Ar + 1i * Ai;
    //*/

    int r = 20;
    REDSVD::RedSVD redsvd;

    redsvd.run(A, r);
    Eigen::MatrixXf U = redsvd.matrixU();
    Eigen::VectorXf S = redsvd.singularValues();
    Eigen::MatrixXf V = redsvd.matrixV();

    std::cout << S << std::endl;

    assert(rowN == U.rows());
    assert(r == U.cols());
    assert(colN == V.rows());
    assert(r == V.cols());
    assert(r == S.rows());
    assert(1 == S.cols());

    for (int i = 0; i < U.cols(); ++i){
        assert(std::abs(1.f - U.col(i).norm()) < EPS);
        for (int j = 0; j < i; ++j){
            assert(std::abs(0.f - U.col(i).dot(U.col(j))) < EPS);
        }
    }

    for (int i = 0; i < V.cols(); ++i){
        assert(std::abs(1.f - V.col(i).norm()) < EPS);
        for (int j = 0; j < i; ++j){
            assert(std::abs(0.f, V.col(i).dot(V.col(j))) < EPS);
        }
    }

    assert(std::abs(0.f - (A - U * S.asDiagonal() * V.transpose()).norm()) < EPS);
};

void test_A() {
    int Mx = 5;
    int My = 4;
    int Nz = 7;
    int Mz = Nz / 2 + 1;

    // TODO CHECK DIMENSION OF MATRICES FOR ROSSE
    int M_A = 2 * (Mx * Mz + Mx * Mz - 1 + 2) * My;

    std::string home = std::string(getenv("HOME")) + "/";
    std::string matrix_filename = home + "osse/database/EPFL/data/ROSSE/eq1/Re=400/"
                                  + std::to_string(Mx) + "x" + std::to_string(My) + "x" + std::to_string(Nz) + "/A.h5";

    Eigen::MatrixXf A = Eigen::MatrixXf(M_A, M_A);
    std::cout << M_A << " x " << M_A << std::endl;
    MatIn_hdf5_real(A, matrix_filename);
    //std::cout << A << std::endl;

    int r = 100;
    REDSVD::RedSVD redsvd;

    redsvd.run(A, r);
    Eigen::MatrixXf U = redsvd.matrixU();
    Eigen::VectorXf S = redsvd.singularValues();
    Eigen::MatrixXf V = redsvd.matrixV();

    std::cout << S << std::endl;
}

int main(){
    test_A();
}
