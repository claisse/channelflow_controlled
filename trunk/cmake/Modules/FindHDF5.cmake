# HDF5_INCLUDE_DIR = hdf5.h
# HDF5_LIBRARIES = hdf5
# HDF5_FOUND = true if HDF53 is found

FIND_PATH(HDF5_INCLUDE_DIR hdf5.h HINTS "${WITH_HDF5}/../include" )
FIND_LIBRARY(HDF5_LIBRARY hdf5 HINTS "${WITH_HDF5}" ) 

include(LibFindMacros)
libfind_process(HDF5)


IF(HDF5_INCLUDE_DIR AND HDF5_LIBRARY)
  MESSAGE(STATUS "HDF5_INCLUDE_DIR=${HDF5_INCLUDE_DIR}")
  MESSAGE(STATUS "HDF5_LIBRARY=${HDF5_LIBRARY}")
  set(HDF5_FOUND TRUE)
ELSE()
  IF(HDF5_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "HDF5 required, please specify its location.")
  ELSE()
    MESSAGE(STATUS      "HDF5 was not found.")
  ENDIF()
ENDIF()

MARK_AS_ADVANCED(HDF5_INCLUDE_DIR HDF5_LIBRARY)
