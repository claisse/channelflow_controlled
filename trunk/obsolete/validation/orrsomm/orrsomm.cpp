#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "flowfield.h"
#include "cfvector.h"
#include "chebyshev.h"
#include "tausolver.h"
#include "dns.h"
#include "complexdefs.h"
#include "diffops.h"

// The purpose of this program is to diagnose initialization behavior
// of multistep schemes. It's a stripped-down version of orrsomm.cpp

// This program compares the growth of OrrSommerfeld eigenfuncs integrated 
// with three methods:
// (1) direct numerical simulation (fully 3d with nonlinearity)
// (2) analytic expression exp(-i omega t) * OS eigenfunction 
// (3) an ODE integration of the linearized equations

// The DNS is represented by  FlowField un (u nonlinear) 
// The analytic expression by FlowField ul (u linear)
// The ODE is represented by a Complex ark (a runge-kutta)
// Admittedly the names of these variables are poorly chosen.

// To compare the DNS and analytic expression to the ODE integration,
// we compute the inner product of un and ul with the OS eigenfunction:
// Complex an = L2InnerProduct(un, u_oseig);
// Complex al = L2InnerProduct(ul, u_oseig);
// If the three integration methods are identical, an == al == ark.

int main() {
  fftw_loadwisdom();

  const int Nx=8;
  const int Ny=65;
  const int Nz=4;
  const int Nd=3;
  const Real Lx=2*pi;
  const Real Lz=pi;
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Reynolds = 7500.0;
  const Real nu = 1.0/Reynolds;
  const Real dPdx = -2.0*nu;    // mean streamwise pressure gradient.
  const Real t0 = 0.00;
  const Real T = 50;
  int N = 10;  // plot results at interval N dt
  Real dt = 0.05;

  const bool pause = false;

  DNSFlags flags;
  flags.nonlinearity = Linearized;
  flags.timestepping = SBDF4;
  flags.initstepping = CNRK2;
  cout << "DNSFlags = " << flags << endl;

  Real scale =  0.01;
  const char s = ' ';
  
  //cout << setprecision(17);
  ChebyTransform trans(Ny);
  Vector y = chebypoints(Ny,a,b);
  y.save("y");

  // Get Orr-Sommerfeld eigenfunction from file. The data is the 
  // y-dependence of the eigenfunction's (u,v) components. x dependence
  // is exp(2 pi i x / Lx). Eigfunction is const in z. Reconstruct
  // w from velocity field, pressure from OS eqns. 
  Complex omega;
  load(omega, "../../data/os_omega10_65");
  BasisFunc ueig("../../data/os_ueig10_65");
  ComplexChebyCoeff peig("../../data/os_peig10_65");
  ueig.makeSpectral(trans);
  peig.makeSpectral(trans);  

  BasisFunc unit_ueig = ueig;
  BasisFunc unit_peig(Ny, 1, 0, Lx, Lz, a, b, Spectral);
  unit_peig[0] = peig;
  unit_ueig *= 1.0/L2Norm(ueig);
  unit_peig *= 1.0/L2Norm(peig);

  // The temporal variation of the eigenfunction is exp(lambda t)
  Complex lambda  = -1.0*I*omega;
  
  ChebyCoeff Ubase(Ny,a,b,Physical);
  for (int ny=0; ny<Ny; ++ny) 
    Ubase[ny] = 1.0 - square(y[ny]);

  FlowField un(Nx,Ny,Nz,Nd,Lx,Lz,a,b);  // numerical velocity field
  FlowField pn(Nx,Ny,Nz,1,Lx,Lz,a,b);   // numerical pressure field
  FlowField fn(Nx,Ny,Nz,Nd,Lx,Lz,a,b);  // numerical nonlinearity field
  FlowField ul(Nx,Ny,Nz,Nd,Lx,Lz,a,b);  // linear-solution velocity
  FlowField pl(Nx,Ny,Nz,1,Lx,Lz,a,b);   // linear-solution pressure
  FlowField fl(Nx,Ny,Nz,Nd,Lx,Lz,a,b);  // linear-solution nonlinearity
  
  // Calculate L2Norm of poisseuille base flow and perturbation field 
  // and rescale so that perturb/poisseuille is equal to the given scale.
  un.setState(Spectral, Physical);
  un += Ubase;
  un.makeSpectral();
  Real uPoissNorm = L2Norm(un);
  un.setToZero();
  un.addProfile(ueig, true);  
  Real ueigNorm = L2Norm(un);
  ueig *= scale*uPoissNorm/ueigNorm;
  peig *= scale*uPoissNorm/ueigNorm;

  un.setToZero();
  pn.setToZero();
  un.addProfile(ueig, true);  
  pn.addProfile(peig,1,0,0, true);
  ul.addProfile(ueig, true);  
  pl.addProfile(peig,1,0,0, true);

  Real t = t0;
  int  n = 0;   // t = t0 + n dt;
  Real u0norm   = L2Norm(un);
  Real p0norm   = L2Norm(pn);
  
  cout << "building DNS..." << flush;
  DNS dns(un, Ubase, nu, dt, flags, t0);
  cout << "done" << endl;

  ofstream norms("norms.asc");
  ofstream times("t.asc");

  ofstream uns("un.asc");
  ofstream uls("ul.asc");
  ofstream uds("ud.asc");
  uns << "% DNS Re(u(n)) Im(u(n)) Re(v(n)) Im(v(n)) Re(w(n)) Im(w(n)) Re(p(n)) Im(p(n)) n=0,1,...Ny-1\n";
  uls << "% Analytic Re(u(n)) Im(u(n)) Re(v(n)) Im(v(n)) Re(w(n)) Im(w(n)) Re(p(n)) Im(p(n)) n=0,1,...Ny-1\n";
  uds << "% DNS-Analytic Re(u(n)) Im(u(n)) Re(v(n)) Im(v(n)) Re(w(n)) Im(w(n)) Re(p(n)) Im(p(n)) n=0,1,...Ny-1\n";

  FlowField tmp(un);
  for (; t<=T; t += N*dt, n += N) {

    if (pause) {
      cfpause();
      cout << '\n';

      cout << "====================================================" << endl;
      cout << "un from DNS" << endl;
      navierstokesNL(un, Ubase, fn, tmp, flags.nonlinearity);
      printf("n=%2d t=%5.2f L2(un)=%13.10f L2(fn)=%13.10f L2(pn)=%13.10f\n",
	     n, t, L2Norm(un), L2Norm(fn), L2Norm(pn));

      cout << "ul from formula" << endl;
      navierstokesNL(ul, Ubase, fl, tmp, flags.nonlinearity);
      printf("n=%2d t=%5.2f L2(ul)=%13.10f L2(fl)=%13.10f L2(pl)=%13.10f\n",
	     n, t, L2Norm(ul), L2Norm(fl), L2Norm(pl));

    }

    Real udist  = L2Dist(un,ul);
    Real pdist  = L2Dist(pn,pl);
    Real fdist  = L2Dist(fn,fl);
    cout << "n=" << n << " t=" << t 
	 << " L2(un-ul)= " << udist 
	 << " L2(pn-pl)= " << pdist 
	 << " L2(fn-fl)= " << fdist << endl;

    norms << udist/u0norm << s << pdist/p0norm << s << divNorm(un)/u0norm << endl;
    times << t << endl;

    // Not the cleanest IO, but it packs everything into one (two) files....
    BasisFunc un10 = un.profile(1,0);
    BasisFunc ul10 = ul.profile(1,0);
    un10.makePhysical(trans);
    ul10.makePhysical(trans);
    ComplexChebyCoeff pn10 = pn.profile(1,0,0);
    ComplexChebyCoeff pl10 = pl.profile(1,0,0);
    pn10.makePhysical(trans);
    pl10.makePhysical(trans);
    for (int ny=0; ny<Ny; ++ny) {
      for (int i=0; i<3; ++i) {
	uns << Re(un10[i][ny]) <<s<< Im(un10[i][ny]) <<s;
	uls << Re(ul10[i][ny]) <<s<< Im(ul10[i][ny]) <<s;
	uds << Re(un10[i][ny])- Re(ul10[i][ny]) <<s
	    << Im(un10[i][ny])- Im(ul10[i][ny]) <<s;
      }
      uns << Re(pn10[ny]) <<s<< Im(pn10[ny]) <<s;
      uls << Re(pl10[ny]) <<s<< Im(pl10[ny]) <<s;
      uds << Re(pn10[ny])-Re(pl10[ny]) <<s
	  << Im(pn10[ny])-Im(pl10[ny]) <<s;
    }
    uns << endl;
    uls << endl;
    uds << endl;

    ueig *= exp(lambda*(N*dt));
    peig *= exp(lambda*(N*dt));
    ul.setToZero();
    pl.setToZero();
    ul.addProfile(ueig, true);
    pl.addProfile(peig, 1, 0, 0, true);

    dns.advance(un, pn, N);
    cout << endl;

    Real CFL = dns.CFL();
    if (CFL > 2.0 || udist > 10) {
      cerr << "DNS trouble: CFL==" << CFL << " L2Dist(un,ul)==" << udist << endl;
      //exit(1);
    }
  }    
  fftw_savewisdom();
}



  
