load un.asc;
load ul.asc;
load ud.asc;
load t.asc
load y.asc;

[M,N] = size(un);

eraserhead = moviein(M);

udax = 2*max(max(abs(ud(1,:))));

for m=1:M
  subplot(131);
  plot(un(m,1:8:N), y, 'b-',...
       un(m,2:8:N), y, 'b--',...
       un(m,3:8:N), y, 'r-',...
       un(m,4:8:N), y, 'r--',...
       un(m,5:8:N), y, 'g-',...
       un(m,6:8:N), y, 'g--',...
       un(m,7:8:N), y, 'k-',...
       un(m,8:8:N), y, 'k--');
  xlabel('y')
  ylabel(strcat('t=',num2str(floor(t(m)))));
  title('DNS')
  
  subplot(132);
  plot(ul(m,1:8:N), y, 'b-',...
       ul(m,2:8:N), y, 'b--',...
       ul(m,3:8:N), y, 'r-',...
       ul(m,4:8:N), y, 'r--',...
       ul(m,5:8:N), y, 'g-',...
       ul(m,6:8:N), y, 'g--',...
       ul(m,7:8:N), y, 'k-',...
       ul(m,8:8:N), y, 'k--');
  xlabel('y')
  title('Eigenfunction')
  
  subplot(133);
  plot(ud(m,1:8:N), y, 'b-',...
       ud(m,2:8:N), y, 'b--',...
       ud(m,3:8:N), y, 'r-',...
       ud(m,4:8:N), y, 'r--',...
       ud(m,5:8:N), y, 'g-',...
       ud(m,6:8:N), y, 'g--',...
       ud(m,7:8:N), y, 'k-',...
       ud(m,8:8:N), y, 'k--');
  xlabel('y')
  title('DNS-eigenfunction');

  if (max(max(abs(ud(m,:)))) > udax)
    udax = 2*max(max(abs(ud(m,:))));
  end
  axis([-udax udax -1 1])
  
  eraserhead(m) = getframe(gcf);
  m
  
  if m == 0
    print -depsc 'frame0.eps'
  end

end;  
  
% plot xy slice, uv with quiver, w with color,
% x horizonatal, y vertical
repeat = 1;
psearch = 0;  % 0,1,2
bsearch = 1;  % 0,1,2
refframe = 1;  % 0,1
pixrange = 5; % >0
ifqscale = 4; % 1-31
pfqscale = 5;  % 1-31
bfqscale = 12;  % 1-31

options = [repeat, psearch, bsearch, refframe, pixrange, ifqscale, pfqscale, bfqscale];

%mpgwrite(eraserhead, jet, 'orrsomm.mpg',options);


