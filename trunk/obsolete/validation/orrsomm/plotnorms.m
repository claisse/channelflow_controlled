load norms.asc
load t.asc
t = t+0.1;
loglog(t,norms);

legend('L2(un-ul)/L2(u0)', 'L2(pn-pl)/L2(p0)', 'L2(div(un)/L2(u0)',2)
legend('boxoff')

xlabel('t')
title('Relative error in velocity, pressure, divergence for DNS of Orr-Somm')

axis([1e-1 1e2 1e-16 1e-5])