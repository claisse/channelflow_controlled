#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "time.h"
#include "flowfield.h"
#include "cfvector.h"
#include "chebyshev.h"
#include "tausolver.h"
#include "dns.h"
#include "complexdefs.h"

// This program does an empirical determination of the stability limits of 
// various time-stepping algorithms by looping over dt, integrating, and
// seeing if the solution blows up. The system is plane Couette with small
// initial data.

int closest(Real x) {
  int n0 = int(x); // rounds down;
  int n1 = n0 + 1;
  return (abs(x-n0) < abs(n1-x)) ? n0 : n1;
}

int main() {
  fftw_loadwisdom();

  const int Nx=32;
  const int Ny=33;
  const int Nz=34;
  const int Nd=3;

  const Real Lx=1.75438596491228*pi; // wally's 2pi/1.14
  const Real Lz=0.8*pi;              // wally's 2pi/2.5
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Reynolds = 400.0;
  const Real nu = 1.0/Reynolds;
  const Real dPdx = 0.0;
  const Real magn = 0.01;
  const Real decay = 0.60;
  const Real T = 4;

  const int Ndt  = 20;
  const Real dt0 = pow(2.0, -Ndt);

  const Real scale =  0.01;
  const char s = ' ';
  
  ChebyTransform trans(Ny);
  Vector y = chebypoints(Ny,a,b);

  ChebyCoeff Ubase(Ny,a,b,Physical);
  for (int ny=0; ny<Ny; ++ny) 
    Ubase[ny] = y[ny];

  FlowField u0(Nx,Ny,Nz,Nd,Lx,Lz,a,b);
  u0.perturb(magn, decay);

  // Set values of dt
  Vector dt(Ndt);     // timestep
  array<int> N(Ndt);  // how many steps to get to T: N*dt = T

  cout << setprecision(6);

  dt[0] = 0.1;
  for (int n=1; n<Ndt; ++n) 
    dt[n] = dt[n-1] + 0.005;
  for (int n=0; n<Ndt; ++n) 
    N[n] = closest(T/dt[n]);
  dt.save("dt");
  
  cout << "N  == ";
  for (int n=0; n<Ndt; ++ n)
    cout << N[n] << ' ';
  cout << endl;

  cout << "dt == ";
  for (int n=0; n<Ndt; ++ n)
    cout << dt[n] << ' ';
  cout << endl;

  cout << "N dt == ";
  for (int n=0; n<Ndt; ++ n)
    cout << N[n]*dt[n] << ' ';
  cout << endl;

  // Set values of timestepping algorithms
  //TimeStepMethod mainalgorithm[] = {CNFE1, CNAB2, CNRK2, SMRK2, SBDF1, SBDF2,
  //			    SBDF3, SBDF4};

  //TimeStepMethod mainalgorithm[] = {CNFE1, CNAB2, SBDF2, SBDF3, SBDF4};
  TimeStepMethod mainalgorithm[] = {CNRK2, SMRK2};
  const int Nalg = 2;

  cout << "Algorithms: ";
  for (int n=0; n<Nalg; ++n) 
    cout << mainalgorithm[n] << ' ';
  cout << endl;

  DNSFlags flags;
  flags.nonlinearity  = Linearized;
  flags.dealiasing    = DealiasXZ;
  flags.constraint    = PressureGradient;
  flags.nonlinearity  = Rotational;
  flags.taucorrection = true;
  flags.verbosity     = PrintTicks;

  ofstream nos("unorms.asc");
  ofstream cos("cputimes.asc");

  for (int alg=0; alg<Nalg; ++alg) {
    flags.timestepping = mainalgorithm[alg];
    switch(flags.timestepping) {
    case CNFE1:
    case CNRK2: 
    case SMRK2:
      flags.initstepping = flags.timestepping;
      break;
    default:
      flags.initstepping = SMRK2;
    }

    Vector cputimes(Ndt);
    Vector unorms(Ndt);

    for (int n=0; n<Ndt; ++n) {
      
      // Set un to perturbation at t=-dt
      FlowField un(u0);
      FlowField pn(Nx,Ny,Nz,1,Lx,Lz,a,b);  

      cout << "building DNS with " << endl
	   << "timestepping == " << flags.timestepping << endl
	   << "initstepping == " << flags.initstepping << endl
	   << "dt = " << dt[n] << " N = " << N[n] << " T = " << N[n]*dt[n] << endl;
      DNS dns(un, Ubase, nu, dt(n), flags);
      cout << "done" << endl;

      // Advance DNS to t = T = N*dt    
      cout << "integrating DNS" << endl;
      clock_t start = clock();
      
      //int S = (N[n]/16 >= 1) ? 16 : 1;
      bool blowup = false;
      for (int s=0; s<N[n]/4; ++s) {
	dns.advance(un, pn, 4); 
	cout << ':';
	if (L2Norm(un) > 100) {
	  cout << "Quitting DNS because solution has blown up" << endl;
	  blowup = true;
	  break;
	}
      }
      clock_t stop = clock();

      unorms[n]  =  blowup ? 100 : L2Norm(un);
      cputimes[n] = blowup ? 100 : Real(stop-start)/CLOCKS_PER_SEC;
      cout << endl << "unorm == " << unorms[n] << ", cputime == " 
	   << cputimes[n] << endl;

      if (blowup) 
	break;
    }    
    for (int n=0; n<Ndt; ++n) {
      nos << unorms[n] << ' ';
      cos << cputimes[n] << ' ';
    }
    nos << endl;
    cos << endl;
  }
}

    


  
