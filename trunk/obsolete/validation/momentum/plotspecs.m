load dudt_spec.asc;
load un_spec.asc;
load force_spec.asc;
load error_spec.asc;

ep = 1e-8;
ax = [log10(ep) .2];

[Mx, Mz] = size(un_spec);

kz = 0:Mz-1;
kx = -Mx/2+1:Mx/2-1;

subplot(2,2,1,'align'); imagesc(kz,kx,log10(dudt_spec+ep), ax); colorbar;
title('du/dt spectrum');  xlabel('kz'); ylabel('kx');
subplot(2,2,2,'align'); imagesc(kz,kx,log10(force_spec+ep), ax);colorbar;
title('force spectrum'); xlabel('kz'); ylabel('kx');
subplot(2,2,3,'align'); imagesc(kz,kx,log10(error_spec+ep),ax); colorbar;
title('error spectrum'); xlabel('kz'); ylabel('kx');

subplot(2,2,4,'align'); imagesc(kz,kx,log10(un_spec+ep),ax); colorbar;
title('u spectrum'); xlabel('kz'); ylabel('kx');

