#include <iostream>
#include <fstream>
#include <iomanip>
#include "cfvector.h"
#include "chebyshev.h"
#include "flowfield.h"
#include "dns.h"
#include "diffops.h"
#include "complexdefs.h"

// This program check momentum balance of fields integrated with DNS
// by computing and comparing LHS and RHS of 
//
// du/dt = - dp/dx - u grad u - U du/dx - v Uy ex + nu lapl u - dPdx ex

// Compute - dp/dx - u grad u - U du/dx - v Uy ex + nu lapl u - dPdx ex
// from u,p,U,nu,dPdx, return in forces variable.

// In the code I call the RHS of this equation "forces"

void NS_forces(const FlowField& u, FlowField& p, ChebyCoeff& U, Real nu, 
	       Real dPdx, const DNSFlags& flags, FlowField& forces);

BasisFunc makeroll(Real Lx, Real Lz, Real a, Real b, int Ny);
BasisFunc makewave(Real Lx, Real Lz, Real a, Real b, int Ny); // kx,kz=1,0 wave
BasisFunc makemeen(Real Lx, Real Lz, Real a, Real b, int Ny); // kx,kz=0,0 mean
BasisFunc makewobl(Real Lx, Real Lz, Real a, Real b, int Ny); // kx,kz=1,1 wobl

int main(int argc, char** argv) {
  
  /******************
  if (argc != 5) {
    cerr << "Usage: " << argv[0] << " Nx Ny Nz tau" << endl;
    abort();
  }
  *******************/
  fftw_loadwisdom();

  // Define gridsize
  const int Nx = 24;
  const int Ny = 49;
  const int Nz = 24;
  const int Nd = 3;

  // Define box size
  const Real Lx=1.75*pi;
  const Real Lz=1.2*pi;
  const Real a= -1.0;
  const Real b=  1.0;
  
  // Define flow parameters
  const Real Reynolds = 400.0;
  const Real nu = 1.0/Reynolds;
  const Real dPdx = 0.0;

  const Real meenmagn = 0.4;
  const Real rollmagn = 0.15;
  const Real wavemagn = 0.03;
  const Real woblmagn = 0.025;

  const bool perturb = true;
  const Real decay = 0.5;   
  const Real magnitude  = 0.001;   
  const int kxmax = 3;
  const int kzmax = 3;

  // Define integration parameters
  const Real dt = 0.02;
  const int   N = 50;
  const Real T  = 200.0;

  DNSFlags flags; 
  flags.timestepping = SBDF4; 
  flags.initstepping = CNRK2; 
  flags.constraint  = PressureGradient; // enforce constant pressure gradient
  //flags.constraint  = BulkVelocity; // enforce constant pressure gradient
  //flags.nonlinearity = Linearized;
  //flags.nonlinearity = Convection;
  flags.nonlinearity = SkewSymmetric;
  //flags.nonlinearity = Rotational;
  //flags.nonlinearity = Alternating;
  flags.dealiasing = NoDealiasing;
  //flags.dealiasing = DealiasXZ;
  flags.taucorrection = true;

  const char s = ' ';
  ofstream ps("parameters.asc");
  ps << "Nx Ny Nz == " << Nx <<s<< Ny <<s<< Nz << endl;
  ps << "Lx Lz    == " << Lx <<s<< Lz << endl;
  ps << "Reynolds == " << Reynolds << endl;
  ps << "meen roll wave wobl == " << meenmagn <<s<< rollmagn <<s<< wavemagn <<s<< woblmagn << endl;
  ps << "perturb decay magn  == " << perturb <<s<< decay << magnitude << endl;
  ps << "dt N T == " << dt <<s<< N <<s<< T << endl;
  ps << "flags = " << flags << endl;
    
  cout << setprecision(17);
  Vector x = periodicpoints(Nx, Lx);
  Vector y = chebypoints(Ny,a,b);
  Vector z = periodicpoints(Nz, Lz);
  x.save("x");
  y.save("y");
  z.save("z");

  // Get Orr-Sommerfeld eigenfunction from file. The data is the 
  // y-dependence of the eigenfunction's (u,v) components. x dependence
  // is exp(2 pi i x / Lx). Eigfunction is const in z. Reconstruct
  // w from velocity field, pressure from OS eqns.
  ChebyTransform trans(Ny);

  ChebyCoeff U(Ny,a,b,Physical);
  for (int ny=0; ny<Ny; ++ny) 
    U[ny] = y[ny];

  U.makeSpectral();
  ChebyCoeff Uy = diff(U);
  ChebyCoeff Uyy = diff(Uy);

  // Fields for approximating dudt and forces midway between timesteps.
  FlowField un(Nx,Ny,Nz,Nd,Lx,Lz,a,b);
  FlowField qn(Nx,Ny,Nz,1,Lx,Lz,a,b);
  FlowField forces_n(Nx,Ny,Nz,3,Lx,Lz,a,b);

  FlowField un1(Nx,Ny,Nz,3,Lx,Lz,a,b);
  FlowField qn1(Nx,Ny,Nz,1,Lx,Lz,a,b);
  FlowField forces_n1(Nx,Ny,Nz,3,Lx,Lz,a,b);

  FlowField dudt(Nx,Ny,Nz,3,Lx,Lz,a,b);
  FlowField forces(Nx,Ny,Nz,3,Lx,Lz,a,b);
  FlowField error(Nx,Ny,Nz,3,Lx,Lz,a,b);
  FlowField utot(Nx,Ny,Nz,3,Lx,Lz,a,b);

  BasisFunc roll = makeroll(Lx,Lz,a,b,Ny);
  BasisFunc wave = makewave(Lx,Lz,a,b,Ny);
  BasisFunc meen = makemeen(Lx,Lz,a,b,Ny);
  BasisFunc wobl = makewobl(Lx,Lz,a,b,Ny);
  roll *= rollmagn;
  wave *= wavemagn;
  meen *= meenmagn;
  wobl *= woblmagn;
  
  un += meen;
  un += roll;
  un += wave;
  wave.conjugate();
  un += wave;
  un += wobl;

  un.addPerturbations(kxmax,kzmax, magnitude,decay);

  cout << "Building DNS..." << endl;
  DNS dns(un, U, nu, dt, flags);
  dns.reset_dPdx(dPdx);

  cout << "Initial data, prior to time stepping" << endl;
  cout << "div(un)    == " << divNorm(un) <<endl;
  cout << "L2Norm(un) == " << L2Norm(un) << endl;
  cout << "L2Norm(qn) == " << L2Norm(qn) << endl;

  Real dPdx_n1 = dPdx;
  Real dPdx_n  = dns.dPdx();


  cout << "dPdx_n  == " << dPdx_n << endl;
  cout << "dPdx_n1 == " << dPdx_n1 << endl;

  ofstream os("mb.asc");
  os << "% t L2Norm(u), L2Norm(dudt), L2Norm(forces), L2Norm(dudt-forces) div(u)" << endl;

  ofstream fos("fmodes.asc");
  
  cout << "Nx, Ny, Nz == " << Nx << ", "<< Ny << ", "<< Nz << endl;
  cout << "Mx, My, Mz == " << un.Mx() << ", "<< un.My() << ", "<< un.Mz() << endl;
  cout << "Kx, Ky, Kz == " << un.kxmaxDealiased() << ", "<< un.My() << ", "<< un.kzmaxDealiased() << endl;
  cout << "kxmax, kzmax == " << un.kxmax() << ", "<< un.kzmax() << endl;

  for (Real t=0; t<T; t += N*dt) {
    cout << "===============================================" << endl;
    cout << " t       == " << t << endl;
    cout << " CFL     == " << dns.CFL() << endl;
    cout << " dPdx    == " << dns.dPdx() << endl;
    cout << " div(un) == " << divNorm(un) << endl;

    un.saveSpectrum("un_spec");
    un1.saveSpectrum("un1_spec");
    dudt.saveSpectrum("dudt_spec");
    forces.saveSpectrum("force_spec");    
    error.saveSpectrum("error_spec");
    un.saveDivSpectrum("udiv_spec");

    //cfpause(); 

    int Ny2 = (Ny-1)/2+1;
    utot = un;
    utot += U;
    utot.makePhysical();
    utot.saveSlice(0, 0, 0,   "u_yz");
    utot.saveSlice(0, 1, 0,   "v_yz");
    utot.saveSlice(0, 2, 0,   "w_yz");
    utot.saveSlice(1, 0, Ny2, "u_xz");
    utot.saveSlice(1, 1, Ny2, "v_xz");
    utot.saveSlice(1, 2, Ny2, "w_xz");
    utot.saveSlice(2, 0, 0,   "u_xy");
    utot.saveSlice(2, 1, 0,   "v_xy");
    utot.saveSlice(2, 2, 0,   "w_xy");
    utot.makeSpectral();
 
    for (int kx=0; kx<=2; ++kx)
      for (int kz=0; kz<=2; ++kz) {
	BasisFunc uprof = un.profile(kx,kz);
	uprof.makePhysical(trans);
	uprof.save("u"+i2s(kx)+i2s(kz));
      }

    for (int kx=0; kx<=8; ++kx) 
      fos << L2Norm2(un.profile(kx,kx)) << s;
    fos << endl;

    // Take N time steps
    for (int n=0; n<N; ++n) {
      // Shift recorded fields in time.
      un1 = un;
      qn1 = qn;
      dPdx_n1 = dPdx_n;

      dns.advance(un, qn);
      dPdx_n = dns.dPdx(); 
    }
    cout << endl;

    NS_forces(un1, qn1, U, nu, dPdx_n1, flags, forces_n1);
    NS_forces(un,  qn,  U, nu, dPdx_n,  flags, forces_n);

    // Approximate forces = (forces_n + forces_n1)/2
    forces = forces_n;
    forces += forces_n1;
    forces *= 0.5;

    // Approximate dutot/dt = (utotn - utotn1)/dt;
    dudt  = un;
    dudt -= un1;
    dudt *= 1.0/dt;
 
    error = dudt;
    error -= forces;

    cout << "L2Norm(un)          == " << L2Norm(un) << endl;
    cout << "L2Norm(dudt)        == " << L2Norm(dudt) << endl;
    cout << "L2Norm(forces)      == " << L2Norm(forces) << endl;
    cout << "L2Dist(dudt,forces) == " << L2Dist(dudt,forces) << endl;
    cout << "L2Dist(dudt,forces)/L2Norm(dudt) == " << L2Dist(dudt,forces)/L2Norm(dudt) << endl;
    cout << "divergence(un)      == " << divNorm(un) << endl;
    cout << "divergence(dudt)    == " << divNorm(dudt) << endl;
    cout << "divergence(forces)  == " << divNorm(forces) << endl;

    os << t <<s<< L2Norm(un) <<s<< L2Norm(dudt) <<s<< L2Norm(forces) <<s
       << L2Dist(dudt,forces) <<s<< divNorm(un) <<s<< divNorm(dudt) <<s
       << divNorm(forces) << endl;

  }
  un.binarySave("u"+i2s(int(T)));
  qn.binarySave("q"+i2s(int(T)));
}

// This function is optimized for debugging utility rather than speed.
void NS_forces(const FlowField& u_, FlowField& p, ChebyCoeff& U, Real nu, 
	       Real dPdx, const DNSFlags& flags, FlowField& forces) {

  NonlinearMethod nonl = flags.nonlinearity;
  FlowField u(u_);
  int Nx = u.Nx();
  int Ny = u.Ny();
  int Nz = u.Nz();
  Real Lx = u.Lx();
  Real Lz = u.Lz();
  Real a = u.a();
  Real b = u.b();

  ChebyCoeff Uy = diff(U);
  ChebyCoeff Uyy = diff(Uy);
  ChebyCoeff Uzero(U);
  Uzero.setToZero();

  FlowField grad_p(Nx,Ny,Nz,3,Lx,Lz,a,b);
  FlowField grad_u(Nx,Ny,Nz,9,Lx,Lz,a,b);
  FlowField u_grad_u(Nx,Ny,Nz,3,Lx,Lz,a,b);
  FlowField lapl_u(Nx,Ny,Nz,3,Lx,Lz,a,b);
  FlowField tmp(Nx,Ny,Nz,9,Lx,Lz,a,b);
  
  grad(u, grad_u);
  grad(p, grad_p);
  lapl(u, lapl_u);
  navierstokesNL(u, Uzero, u_grad_u, tmp, nonl);

  u.makePhysical();
  grad_p.makePhysical();
  u_grad_u.makePhysical();
  grad_u.makePhysical();
  lapl_u.makePhysical();
  U.makePhysical();
  Uy.makePhysical();
  Uyy.makePhysical();
  
  forces.setToZero();
  forces.setState(Physical, Physical);

  // For plotting
  FlowField v_Uy_ex(Nx,Ny,Nz,3,Lx,Lz,a,b,Physical,Physical);
  FlowField U_dudx(Nx,Ny,Nz,3,Lx,Lz,a,b,Physical,Physical);
  FlowField Uyy_dPdx_ex(Nx,Ny,Nz,3,Lx,Lz,a,b,Physical,Physical);

  for (int ny=0; ny<Ny; ++ny)
    for (int nx=0; nx<Nx; ++nx)
      for (int nz=0; nz<Nz; ++nz)
	for (int i=0; i<3; ++i) {
	  
	  // Set forces
	  Real f = -grad_p(nx,ny,nz,i) + nu*lapl_u(nx,ny,nz,i);
	  f -= U[ny]*grad_u(nx,ny,nz,i3j(i,0));
	  f -= u_grad_u(nx,ny,nz,i); // will be zero for linearized
	  
	  if (i==0)
	    f += - Uy(ny)*u(nx,ny,nz,1) + nu*Uyy[ny] - dPdx;

	  forces(nx,ny,nz,i) = f;

	  // Set plotting fields 
	  U_dudx(nx,ny,nz,i) = U[ny]*grad_u(nx,ny,nz,i3j(i,0));
	  
	  if (i==0) {
	    v_Uy_ex(nx,ny,nz,i) = Uy(ny)*u(nx,ny,nz,1);
	    Uyy_dPdx_ex(nx,ny,nz,i) = nu*Uyy[ny] - dPdx;
	  }
	}

  U.makeSpectral();
  u.makeSpectral_xz();
  p.makeSpectral_xz();
  forces.makeSpectral_xz();

  lapl_u *= nu;
  grad_p.makeSpectral_xz();
  lapl_u.makeSpectral_xz();
  u_grad_u.makeSpectral_xz();
  v_Uy_ex.makeSpectral_xz();
  U_dudx.makeSpectral_xz();
  Uyy_dPdx_ex.makeSpectral_xz();

  Complex zero(0.0, 0.0);
  if (flags.dealiasing == DealiasXZ) {
    // Zero out aliased modes;
    for (int mx=0; mx<u.numXmodes(); ++mx) 
      for (int mz=0; mz<u.numZmodes(); ++mz)
	if (u.isAliased(u.kx(mx),u.kz(mz)))
	  for (int i=0; i<3; ++i)
	    for (int ny=0; ny<Ny; ++ny) {
	      forces.cmplx(mx,ny,mz,i) = zero;
	      u_grad_u.cmplx(mx,ny,mz,i) = zero;
	    }
  }

  int mx_max = u.mx(u.kxmax());
  for (int i=0; i<3; ++i)
    for (int ny=0; ny<Ny; ++ny) 
      for (int mz=0; mz<u.Mz(); ++mz)
	forces.cmplx(mx_max,ny,mz,i) = zero;

  int mz_max = u.mz(u.kzmax());
  for (int i=0; i<3; ++i)
    for (int ny=0; ny<Ny; ++ny) 
      for (int mx=0; mx<u.Mx(); ++mx)
	forces.cmplx(mx,ny,mz_max,i) = zero;

  /*****************************
  Complex zero(0.0, 0.0);
  for (int i=0; i<3; ++i)
    for (int ny=0; ny<Ny; ++ny) {
      int mz = u.mz(u.kzmax());
      for (int mx=0; mx<u.Mx(); ++mx) 
	forces.cmplx(mx,ny,mz,i) = zero;
	
      int mx = u.mx(u.kxmax());
      for (int mz=0; mz<u.Mz(); ++mz) 
	forces.cmplx(mx,ny,mz,i) = zero;
    }
  **********************************/
  /*****************************
  int kxmaxPlot = 2;
  int kzmaxPlot = 2;
  for (int kx=0; kx<=kxmaxPlot; ++kx) {
    for (int kz=0; kz<=kzmaxPlot; ++kz) {
      string lbl = i2s(kx) + i2s(kz);
      u.saveProfile(kx,kz, "un"+lbl);
      ComplexChebyCoeff pp = p.profile(kx,kz,0);
      pp.save("pnt"+lbl);
      pp.makePhysical();
      pp.save("pn"+lbl);

      BasisFunc uu = u.profile(kx,kz);
      uu.makeSpectral();
      uu.save("unt"+lbl);

      grad_p.saveProfile(kx,kz, "grad_pn"+lbl);
      lapl_u.saveProfile(kx,kz, "lapl_un"+lbl);
      u_grad_u.saveProfile(kx,kz, "un_grad_un"+lbl);
      v_Uy_ex.saveProfile(kx,kz, "vn_Uy_ex"+lbl);
      U_dudx.saveProfile(kx,kz, "U_dundx"+lbl);
      Uyy_dPdx_ex.saveProfile(kx,kz, "Uyy_dPdx_ex"+lbl);
    }
  }
  ***********************************/
  u.makeSpectral_y();
  p.makeSpectral_y();
  forces.makeSpectral_y();
  return;
}
  
// This formula recreates (roughly) the kx,kz=1,0 rolls seen in PCF flow.
// Derived in 9/5/05 notes and uinit.m, tinkered with until looked like DNS.
BasisFunc makeroll(Real Lx, Real Lz, Real a, Real b, int Ny) {
  if (Ny < 5) 
    cferror("Ny too small. Set Ny >= 5");
  
  BasisFunc rtn(Ny, 0, 1, Lx, Lz, a, b, Spectral);

  rtn.v().re[0] =  3.0/32.0;
  rtn.v().re[2] = -1.0/8.0;
  rtn.v().re[4] =  1.0/32.0;
  
  ChebyCoeff tmp = diff(rtn.v().re);
  rtn.w().im = tmp;
  rtn.w().im *= Lz/(2*pi);
  
  tmp.setToZero();
  tmp.setState(Physical);
  Vector y = chebypoints(Ny,-1,1);
  for (int n=0; n<Ny; ++n) {
    Real yp = 1 + 2*(y[n]-b)/(b-a);
    tmp[n] = 2*(pow(yp,6) - 0.7*pow(yp,2) - 0.3);
  }
  tmp.makeSpectral();
  rtn.u().re = tmp;
  
  rtn *= -1.0/L2Norm(rtn);
  return rtn;
}


BasisFunc makewave(Real Lx, Real Lz, Real a, Real b, int Ny) {
  BasisFunc rtn(Ny, 1, 0, Lx, Lz, a, b, Physical);
  
  Vector y = chebypoints(Ny,-1,1);
  for (int n=0; n<Ny; ++n) {
    Real yp = 1 + 2*(y[n]-b)/(b-a);
    Real p = 1-yp*yp;
    Real pp = p*p;
    rtn.w().re[n] = 0.7*p*sin(pi*yp);
    rtn.w().im[n] = p*cos(pi*yp);
    rtn.v().re[n] = 0.04*pp*sin(pi*yp);
    rtn.v().im[n] = 0.04*pp*cos(pi*yp);
  }
  rtn.makeSpectral();
  ChebyCoeff tmp = diff(rtn.v().re);
  rtn.u().im = tmp;
  rtn.u().im *= Lx/(2*pi);
  
  tmp = diff(rtn.v().im);
  rtn.u().re = tmp;
  rtn.u().re *= -Lx/(2*pi);
  
  rtn *= 1.0/L2Norm(rtn);
  return rtn;
}

BasisFunc makemeen(Real Lx, Real Lz, Real a, Real b, int Ny) {

  BasisFunc rtn(Ny, 0, 0, Lx, Lz, a, b, Physical);
  Vector y = chebypoints(Ny,-1,1);
  for (int n=0; n<Ny; ++n) 
    rtn.u().re[n] =  -sin(pi*y[n]);
  rtn.makeSpectral();
  rtn *= 1.0/L2Norm(rtn);
  return rtn;
}  

BasisFunc makewobl(Real Lx, Real Lz, Real a, Real b, int Ny) {
  BasisFunc rtn(Ny, 1, 1, Lx, Lz, a, b, Physical);
  
  Vector y = chebypoints(Ny,-1,1);
  for (int n=0; n<Ny; ++n) {
    Real yp = 1 + 2*(y[n]-b)/(b-a);
    Real p = 1-yp*yp;
    Real pp = p*p;
    rtn.u().re[n] = p*cos(pi*yp);
    rtn.u().im[n] = -p*sin(pi*yp);
    rtn.v().re[n] = 0.2*pp*cos(pi*yp);
    rtn.v().im[n] = -0.2*pp*sin(pi*yp);
  }
  rtn.makeSpectral();
  ChebyCoeff tmp;

  // Build up Re(w) = -Lz/Lx  Re(u) - Lz/2pikz Im(v')
  //                = -Lz/Lx [Re(u) + Lx/2pikz Im(v')]
  diff(rtn.v().im, tmp);
  rtn.w().re = tmp;
  rtn.w().re *= Lx/(2*pi);
  rtn.w().re += rtn.u().re;
  rtn.w().re *= -Lz/Lx;
  
  // Build up Im(w) = -Lz/Lx  Im(u) + Lz/2pikz Re(v')
  //                = -Lz/Lx [Im(u) - Lx/2pikz Re(v')]
  diff(rtn.v().re, tmp);
  rtn.w().im = tmp;
  rtn.w().im *= -Lx/(2*pi);
  rtn.w().im += rtn.u().im;
  rtn.w().im *= -Lz/Lx;
  
  rtn *= 1.0/L2Norm(rtn);
  return rtn;
}
