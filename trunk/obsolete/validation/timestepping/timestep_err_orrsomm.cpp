#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "time.h"
#include "flowfield.h"
#include "cfvector.h"
#include "chebyshev.h"
#include "tausolver.h"
#include "dns.h"
#include "complexdefs.h"

// See README file for a description of this program.

int main() {
  fftw_loadwisdom();

  const int Nx=16;
  const int Ny=65;
  const int Nz=4;
  const int Nd=3;
  const Real Lx=2*pi;
  const Real Lz=pi;
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Reynolds = 7500.0;
  const Real nu = 1.0/Reynolds;
  const Real dPdx = -2.0*nu;    // mean streamwise pressure gradient.

  const Real dt0 = 1.0;
  const int N0  = 4;
  const Real T = dt0*N0;

  const int Ndt = 11; // will go from dt = 1 to 1/256 = 2^(-8)

  // Set values of dt
  Vector dt(Ndt);     // timestep
  array<int> N(Ndt);  // how many steps to get to T: N*dt = T
  N[0] = N0;
  dt[0] = dt0;
  for (int n=1; n<Ndt; ++n) {
    N[n] = 2*N[n-1];
    dt[n] = T/N[n];
  }
  
  int Nalg = 7;
  TimeStepMethod mainalgorithm[] = {CNFE1, CNAB2, CNRK2, SMRK2, 
				    SBDF2, SBDF3, SBDF4};

  Real scale =  0.01;
  const char s = ' ';
  
  ChebyTransform trans(Ny);
  Vector y = chebypoints(Ny,a,b);

  // Get Orr-Sommerfeld eigenfunction from file. The data is the 
  // y-dependence of the eigenfunction's (u,v) components. x dependence
  // is exp(2 pi i x / Lx). Eigfunction is const in z. Reconstruct
  // w from velocity field, pressure from OS eqns. 
  Complex omega;
  load(omega, "../../data/os_omega10_65");
  BasisFunc ueig("../../data/os_ueig10_65");
  ComplexChebyCoeff peig("../../data/os_peig10_65");

  // The temporal variation of the eigenfunction is exp(lambda t)
  Complex lambda = -1.0*I*omega;
  ueig.makeSpectral(trans);
  peig.makeSpectral(trans);  

  ChebyCoeff Ubase(Ny,a,b,Physical);
  for (int ny=0; ny<Ny; ++ny) 
    Ubase[ny] = 1.0 - square(y[ny]);

  FlowField un(Nx,Ny,Nz,Nd,Lx,Lz,a,b);  // numerical velocity field
  FlowField pn(Nx,Ny,Nz,1,Lx,Lz,a,b);   // numerical pressure field
  FlowField ul(Nx,Ny,Nz,Nd,Lx,Lz,a,b);  // linear-solution velocity
  FlowField pl(Nx,Ny,Nz,1,Lx,Lz,a,b);   // linear-solution pressure
  
  // Calculate L2Norm of poisseuille base flow and perturbation field 
  // and rescale so that perturb/poisseuille is equal to the given scale.
  un.setState(Spectral, Physical);
  un += Ubase;
  un.makeSpectral();
  Real uPoissNorm = L2Norm(un);
  un.setToZero();
  un.addProfile(ueig, true);  
  pn.setToZero();
  pn.addProfile(peig, pn.mx(1), pn.mz(0), 0, true);  

  Real uPerturbNorm = L2Norm(un);
  ueig *= scale*uPoissNorm/uPerturbNorm;
  peig *= scale*uPoissNorm/uPerturbNorm;
  
  cout << setprecision(6);

  dt.save("dt");
  ofstream uerrs("uerr.asc");
  ofstream perrs("perr.asc");
  ofstream cpus("cputime.asc");
  ofstream algs("algorithms.asc");
  
  for (int alg=0; alg<Nalg; ++alg) {
    for (int n=0; n<Ndt; ++n) {
      cout << "alg, dt == " << mainalgorithm[alg] << ", " << dt(n) << endl;

      DNSFlags flags;
      flags.nonlinearity = Linearized;
      flags.timestepping = mainalgorithm[alg];

      // Set un to perturbation at t=-dt
      un.setToZero();
      un.addProfile(ueig, true);
      pn.setToZero();
      pn.addProfile(peig, pn.mx(1), pn.mz(0), 0, true);  

      cout << "building DNS..." << flush;
      DNS dns(un, Ubase, nu, dt[n], flags);
      cout << "done" << endl;

      // Advance DNS to t = T = N*dt    
      clock_t start = clock();
      dns.advance(un, pn, N[n]); 
      clock_t stop = clock();
      Real cputime = Real(stop-start)/ CLOCKS_PER_SEC;

      // Compute analytic solution at time t = T 
      BasisFunc exp_ueig(ueig);  
      ComplexChebyCoeff exp_peig(peig);
      exp_ueig *= exp(lambda*T);
      exp_peig *= exp(lambda*T);
      ul.setToZero();
      pl.setToZero();
      ul.addProfile(exp_ueig, true);
      pl.addProfile(exp_peig, 1, 0, 0, true);

      cout << endl;
      uerrs << L2Dist(un,ul)/L2Norm(ul) << s;
      perrs << L2Dist(pn,pl)/L2Norm(pl) << s;
      cpus << cputime << s;

    }
    uerrs << endl;
    perrs << endl;
    cpus << endl;
    algs << mainalgorithm[alg] << endl;
  }    
  fftw_savewisdom();
}

    


  
