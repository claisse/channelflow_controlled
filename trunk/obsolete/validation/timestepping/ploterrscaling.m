load cputime.asc
load dt.asc
load uerr.asc
load perr.asc

uerr = uerr';
perr = perr';
cputime = cputime';

fid = fopen('algorithms.asc');
algorithms = textscan(fid, '%s');
fclose(fid);

loglog(cputime, uerr)
xlabel('cpu seconds')
ylabel('L2 err in u at t=4')
axis([4e-2 1e2 1e-10 1])
% please do not ask me to explain the logic of Matlab strings
legend(algorithms{1},3)
legend('boxoff')
title('Computational efficiency, linearized DNS of Orr-Somm eigfunc')


print -depsc 'u_err_vs_cputime_orrsomm.eps'
pause;

hold off
loglog(dt, uerr)
hold on
loglog(dt, 0.3*uerr(1,1)*dt, 'k:', dt, 0.3*uerr(2,1)*dt.^2, 'k:', dt, 0.3*uerr(4,1)*dt.^3, 'k:', dt, 0.3*uerr(7,1)*dt.^4, 'k:')

xlabel('dt')
ylabel('L2 err in u at t=4')

legend(algorithms{1},4) 
legend('boxoff')

title('Velocity field error scaling, linearized DNS of Orr-Somm eigfunc')
axis([1e-3 2 1e-10 1])
print -depsc 'u_err_vs_dt_orrsomm.eps'
pause;


hold off
loglog(dt, perr)
hold on
loglog(dt, 0.3*perr(1,1)*dt, 'k:', dt, 0.3*perr(2,1)*dt.^2, 'k:', dt, 0.3*perr(4,1)*dt.^3, 'k:', dt, 0.3*perr(7,1)*dt.^4, 'k:')

xlabel('dt')
ylabel('L2 err in p at t=4')

legend(algorithms{1},4) 
legend('boxoff')

title('Pressure field error scaling, linearized DNS of Orr-Somm eigfunc')
axis([1e-3 2 1e-10 1])
print -depsc 'p_err_vs_dt_orrsomm.eps'
pause;





