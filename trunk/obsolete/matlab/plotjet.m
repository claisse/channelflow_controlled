je = jet;

subplot(211);
A = ones(64,1) *(1:64);
imagesc(A);

subplot(212);
n=1:64;
plot(n,je(:,1),'r', n,je(:,2),'g', n,je(:,3),'b');
axis([1 64 0 1])
