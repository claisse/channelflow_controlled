load jutu.asc

subplot(211);
A = ones(64,1) *(1:64);
imagesc(A);

subplot(212);
n=1:64;
plot(n,jutu(:,1),'r', n,jutu(:,2),'g', n,jutu(:,3),'b');
axis([1 64 0 1])
