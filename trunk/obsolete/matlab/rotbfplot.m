function x = rbfplot(Phi,y,theta);

c = cos(theta);
s = sin(theta);

phi(:,1) = c*Phi(:,1) - s*Phi(:,2);
phi(:,2) = s*Phi(:,1) + c*Phi(:,2);

phi(:,3) = c*Phi(:,3) - s*Phi(:,4);
phi(:,4) = s*Phi(:,3) + c*Phi(:,4);

phi(:,5) = c*Phi(:,5) - s*Phi(:,6);
phi(:,6) = s*Phi(:,5) + c*Phi(:,6);

bfplot(phi,y);