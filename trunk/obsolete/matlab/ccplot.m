function x = ccplot(f,y);

plot(f(:,1),y,'b', f(:,2),y,'b--');