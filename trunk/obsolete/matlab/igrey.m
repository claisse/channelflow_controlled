function g = igrey();

  h = grey;
  for i=1:64
    g(65-i, :) = h(i,:);
  end