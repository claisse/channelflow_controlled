function x = uvbfplot(Phi,y);

plot(Phi(:,1),y,'k', Phi(:,2),y,'k--', Phi(:,5),y, 'k-.', Phi(:,6),y,'k:');
pmax = 1.1*max(max(abs(Phi(:,[1 2 5 6]))));
if pmax==0
  pmax = 1;
end
%ax = [-pmax pmax 0.0 0.2]; axis(ax);  
bigfont;
