function x = ubfplot(Phi,y);

plot(Phi(:,1),y,'k', Phi(:,2),y,'k--');
bigfont;
