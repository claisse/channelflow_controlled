load oseig_u_10_65.bf;
load y.asc;

oseig = oseig_u_10_65;
rotoseig = zeros(65,6);
% rotate by pi/2 (mult by i)

rotoseig(:,1) = -1*oseig(:,2);
rotoseig(:,2) =    oseig(:,1);
rotoseig(:,3) = -1*oseig(:,4);
rotoseig(:,4) =    oseig(:,3);
rotoseig(:,5) = -1*oseig(:,6);
rotoseig(:,6) =    oseig(:,5);

kbfplot(rotoseig,y);
