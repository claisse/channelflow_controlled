function x = cplot(f,y);

plot(real(f),y,'r', imag(f),y,'g');