function bfdotplot(Phi);

[N,M] = size(Phi);
Phi = abs(Phi);
n = 0:N-1;
semilogy(n,Phi(:,1),'bo', n,Phi(:,2),'bs', ...
	 n,Phi(:,3),'ro', n,Phi(:,4),'rs', ...
	 n,Phi(:,5),'go', n,Phi(:,6),'gs');

%ax = [-pmax pmax min(y) max(y)];

%axis(ax);  

%axis_handle = gca;
%set(axis_handle, 'YTick', []);
%set(axis_handle, 'XTick', []);
%bigfont;
