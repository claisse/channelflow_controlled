function x = kbfplot(Phi,y);

nrm = sum(sum(abs(Phi)));

minnrm = 0.001;

hold off;
if sum(abs(Phi(:,1)))/nrm > minnrm
  plot(Phi(:,1),y,'k-');
  hold on;
end
if sum(abs(Phi(:,2)))/nrm > minnrm
  plot(Phi(:,2),y,'k--');
  hold on;
end
if sum(abs(Phi(:,5)))/nrm > minnrm
  plot(Phi(:,5),y,'k-.');
  hold on;
end
if sum(abs(Phi(:,6)))/nrm > minnrm
  plot(Phi(:,6),y,'k:');
  hold on;
end

pmax = 1.1*max(max(abs(Phi(:,[1 2 5 6]))));
%big = 0.8;
%sml = 0.4;

%if sum(abs(Phi(:,1)))/nrm > 0.01
%  plot(Phi(:,1),y,'k-', 'LineWidth', big);
%  hold on;
%end
%if sum(abs(Phi(:,2)))/nrm > 0.01
%  plot(Phi(:,2),y,'k-', 'LineWidth', sml);
%  hold on;
%end
%if sum(abs(Phi(:,3)))/nrm > 0.01
%  plot(Phi(:,3),y,'k:','LineWidth', big);
%  hold on;
%end
%if sum(abs(Phi(:,4)))/nrm > 0.01
%  plot(Phi(:,4),y,'k:','LineWidth', sml);
%  hold on;
%end
%if sum(abs(Phi(:,5)))/nrm > 0.01
%  plot(Phi(:,5),y,'k--','LineWidth', big);
%  hold on;
%end
%if sum(abs(Phi(:,6)))/nrm > 0.01
%  plot(Phi(:,6),y,'k--','LineWidth', sml);
%  hold on;
%end
     
%pmax = 1.1*max(max(abs(Phi(:,[1 2 3 4 5 6]))));

if pmax==0
  pmax = 1;
end
ax = [-pmax pmax min(y) max(y)];
axis(ax);  

%bigfont;
