function x = ksemilogy(f);

[T,N] = size(f);
t=1:T;

semilogy(t,f(t,1),'k-.', t,f(t,2),'k-', t,f(t,3),'k--', t,f(t,4),'k:')
