load mb.asc;
[M,N] = size(mb);
t = mb(:,1);
m = mb(:,2:5);
semilogy(t,m);
legend('u','dudt','mom err', 'div', 3);
