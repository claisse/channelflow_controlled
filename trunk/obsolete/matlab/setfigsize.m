function foo = setfigsize(width, height);

% Set figure size to width x height (inches) and center on page
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'inches');
pagesize = get(gcf, 'PaperSize');
left = (pagesize(1)-width)/2;
bottom = (pagesize(2)-height)/2;
set(gcf, 'PaperPosition', [left, bottom, width, height]);

