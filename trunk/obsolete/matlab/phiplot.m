% plot Phi[0-8].bf
load Phi0.bf;
load Phi1.bf;
load Phi2.bf;
load Phi3.bf
load Phi4.bf;
load Phi5.bf;
load Phi6.bf;
load Phi7.bf;
load Phi8.bf;
load y.asc;

subplot(331); bfplot(Phi0,y); %title('\Phi_0');
subplot(332); bfplot(Phi1,y); %title('\Phi_1');
subplot(333); bfplot(Phi2,y); %title('\Phi_2'); 
subplot(334); bfplot(Phi3,y); %title('\Phi_3');
subplot(335); bfplot(Phi4,y); %title('\Phi_4');
subplot(336); bfplot(Phi5,y); %title('\Phi_5');
subplot(337); bfplot(Phi6,y); %title('\Phi_6');
subplot(338); bfplot(Phi7,y); %title('\Phi_7');
subplot(339); bfplot(Phi8,y); %title('\Phi_8');
