function r = fscale(x);

N=length(x);

c = 0.8;
for n=1:N
 if x(n) <= 0.5
   r(n) = c*x(n);
 else
   r(n) = 1-c + c*x(n);
 end 
end
 
d = 0.2;
r = d + (1-d)*r;