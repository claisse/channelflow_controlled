function h = cbfplot(Phi,y);

h = plot(Phi(:,1),y,'b', Phi(:,2),y,'b--', ...
	 Phi(:,3),y,'r', Phi(:,4),y,'r--', ...
	 Phi(:,5),y,'g', Phi(:,6),y,'g--');

pmax = 1.1*max(max(abs(Phi)));
if pmax==0
  pmax = 1;
end
ax = [-pmax pmax min(y) max(y)];

axis(ax);  
axis_handle = gca;

%set(axis_handle, 'YTick', []);
%set(axis_handle, 'XTick', []);
%bigfont;
