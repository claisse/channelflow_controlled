function g = igray();

  h = gray;
  for i=1:64
    g(65-i, :) = h(i,:);
  end