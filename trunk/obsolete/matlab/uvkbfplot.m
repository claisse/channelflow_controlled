function x = uvkbfplot(Phi,y);

f = Phi;
f(:,4) = 5*f(:,4);
% 1: Re u
% 4: Im v
plot(f(:,1),y,'k-', ...  
     f(:,4),y,'k--')
     
pmax = 1.1*max(max(abs(f(:,[1 4]))));
if pmax==0
  pmax = 1;
end
ax = [-pmax pmax min(y) max(y)];
axis(ax);
% bigfont;
