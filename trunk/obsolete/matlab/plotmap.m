function plotmap(j);

A = ones(64,1) *(1:64);
imagesc(A);

subplot(212);
n=1:64;
plot(n,j(:,1),'r', n,j(:,2),'g', n,j(:,3),'b');
axis([1 64 0 1])
colormap(j);