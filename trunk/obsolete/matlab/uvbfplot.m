function x = uvbfplot(Phi,y);

Phi(:,3) = 5*Phi(:,3);
Phi(:,4) = 5*Phi(:,4);

plot(Phi(:,1),y,'k', Phi(:,2),y,'k--', Phi(:,3),y, 'k-.', Phi(:,4),y,'k:');
pmax = 1.1*max(max(abs(Phi(:,[1 2 3 4]))));
if pmax==0
  pmax = 1;
end
ax = [-pmax pmax min(y) max(y)];
axis(ax);  
bigfont;
