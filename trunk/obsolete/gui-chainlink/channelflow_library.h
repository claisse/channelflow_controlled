#ifndef channelflow_library_H
#define channelflow_library_H

#include "../chainlink_base/chainlink_base.h"
#include "flowfield.h"
#include "dns.h"

//[library] channelflow_library

#include "flowfield_ops.h"

typedef channelflow::ChebyCoeff ChebyCoeff;
//[datatype] ChebyCoeff
typedef channelflow::DNS DNS;
//[datatype] DNS
typedef channelflow::DNSFlags DNSFlags;
//[datatype] DNSFlags

//[conversion] vector ChebyCoeff always
//[conversion] matrix ChebyCoeff always
//[conversion] mda ChebyCoeff always
bool conversion(mda &ret,ChebyCoeff &CC);


//[function] ChebyCoeff ChebyCoeff2
//input: vector v, real a, real b, string fieldstate
//output: ChebyCoeff ret
//description: returns a ChebyCoeff object
//fieldstate must be either 'Physical' or 'Spectral'
bool ChebyCoeff2(ChebyCoeff &ret, vector &v, real a, real b, string &fieldstate);

//[function] chebypoints
//input: integer N, real a, real b, string fieldstate
//output: ChebyCoeff ret
//description: 
//fieldstate must be either 'Physical' or 'Spectral'
bool chebypoints(ChebyCoeff &ret, integer N, real a, real b, string &fieldstate);


//[function] DNS DNS2
//input: FlowField u, ChebyCoeff Ubase, real nu, real dt, DNSFlags flags
//output: DNS ret
//description: returns a DNS object
bool DNS2(DNS &ret, FlowField &u, ChebyCoeff &Ubase, real nu, real dt, DNSFlags &flags);

//[function] advance
//input: DNS dns, FlowField u, FlowField q, integer Nsteps
//output:
bool advance(DNS &dns, FlowField &u, FlowField &q, integer Nsteps);

//[function] CFL
//input: DNS &dns
//output: real &ret
bool CFL(real &ret,DNS &dns);

//[function] DNSFlags DNSFlags2
//input: string &constraint,string &timestepping,string &initstepping,string &nonlinearity,string &dealiasing,string &taucorrection,string &verbosity,real dPdx,real Ubulk
//output: DNSFlags ret
bool DNSFlags2(DNSFlags &ret,
						string &constraint,
						string &timestepping,
						string &initstepping,
						string &nonlinearity,
						string &dealiasing,
						string &taucorrection,
						string &verbosity,
						real dPdx, 
						real Ubulk);

#endif
