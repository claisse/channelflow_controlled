#include "channelflow_library.h"

bool conversion(vector &ret,ChebyCoeff &CC) {
	ret.allocate(MDA_TYPE_REAL,1,CC.N());
	int32 j;
	for (j=0; j<CC.N(); j++)
		ret.set(CC[j], j);
	return true;
}



bool ChebyCoeff2(ChebyCoeff &ret, vector &v, real a, real b, string &fieldstate) {
	if (strcmp(fieldstate.data,"Physical")==0) {
		ret.makePhysical();
	}
	else if (strcmp(fieldstate.data,"Spectral")==0) {
		ret.makeSpectral();
	}
	else {
		chprintf("Error, fieldstate must be 'Physical' or 'Spectral'\n");
		return false;
	}
	ret.resize(v.size());
	int32 j;
	for (j=0; j<v.size(); j++) {
		ret[j]=v.get(j).re;
	} 
	ret.setBounds(a,b);
	
	return true;
}

bool chebypoints(ChebyCoeff &ret, integer N, real a, real b,string &fieldstate) {
	channelflow::Vector holdvec;
	holdvec=channelflow::chebypoints(N,a,b);
	ret.resize(N);
	vector vec2;
	vec2.allocate(MDA_TYPE_REAL,N);
	int32 j;
	for (j=0; j<N; j++) {
		vec2.set(holdvec[j],j);
	}
	return ChebyCoeff2(ret,vec2,a,b,fieldstate);
}



bool DNS2(DNS &ret, FlowField &u, ChebyCoeff &Ubase, real nu, real dt, DNSFlags &flags) {
	ret=DNS(u, Ubase, nu, dt, flags);
	return true;
}

bool advance(DNS &dns, FlowField &u, FlowField &q, integer Nsteps) {
	dns.advance(u,q,Nsteps);
	return true;
}

bool CFL(real &ret,DNS &dns) {
	ret=dns.CFL();
	return true;
}

// Enum types for specifying the behavior of NSIntegrator, fields of DNSFlags.
//enum Dealiasing      {NoDealiasing, DealiasXZ, DealiasY, DealiasXYZ};
//enum TimeStepMethod  {CNFE1, CNAB2, CNRK2, SMRK2,
//		      SBDF1, SBDF2, SBDF3, SBDF4}; 
//enum MeanConstraint  {PressureGradient, BulkVelocity};
//enum Verbosity       {Silent, PrintTicks, PrintTime, VerifyTauSolve, PrintAll};
//enum NonlinearMethod {Rotational, Convection, Divergence, SkewSymmetric, 
//		      Alternating, Alternating_, Linearized, GenlLinearized}; 

//MeanConstraint  constraint;   // Enforce const press grad or const bulk vel
//  TimeStepMethod  timestepping; // Time-stepping algorithm
//  TimeStepMethod  initstepping; // Algorithm for initializing multistep methods
//  NonlinearMethod nonlinearity; // Method of calculating nonlinearity of NS eqn
//  Dealiasing      dealiasing;   // Use 3/2 rule to eliminate aliasing
//  bool            taucorrection;// Remove divergence caused by discretization
//  Verbosity       verbosity;    // Print diagnostics, times, ticks, or nothing
//  Real            dPdx;         // Constraint value
//  Real            Ubulk;        // Constraint value

bool DNSFlags2(DNSFlags &ret,
						string &constraint,
						string &timestepping,
						string &initstepping,
						string &nonlinearity,
						string &dealiasing,
						string &taucorrection,
						string &verbosity,
						real dPdx, 
						real Ubulk)
{
	if (strcmp(constraint.data,"PressureGradient")==0)
		ret.constraint=channelflow::PressureGradient;
	else if (strcmp(constraint.data,"BulkVelocity")==0)
		ret.constraint=channelflow::BulkVelocity;
	else {
		chprintf("Error in DNSFlags, constraint must be one of the following: PressureGradient, BulkVelocity\n");
		return false;
	}	
	
	if (strcmp(timestepping.data,"CNFE1")==0)
		ret.timestepping=channelflow::CNFE1;
	else if (strcmp(timestepping.data,"CNAB2")==0)
		ret.timestepping=channelflow::CNAB2;
	else if (strcmp(timestepping.data,"CNRK2")==0)
		ret.timestepping=channelflow::CNRK2;
	else if (strcmp(timestepping.data,"SMRK2")==0)
		ret.timestepping=channelflow::SMRK2;
	else if (strcmp(timestepping.data,"SBDF1")==0)
		ret.timestepping=channelflow::SBDF1;
	else if (strcmp(timestepping.data,"SBDF2")==0)
		ret.timestepping=channelflow::SBDF2;
	else if (strcmp(timestepping.data,"SBDF3")==0)
		ret.timestepping=channelflow::SBDF3;
	else if (strcmp(timestepping.data,"SBDF3")==0)
		ret.timestepping=channelflow::SBDF3;
	else {
		chprintf("Error in DNSFlags, timestepping must be one of the following: CNFE1, CNAB2, CNRK2, SMRK2, SBDF1, SBDF2, SBDF3, SBDF4 \n");
		return false;
	}	
	
	if (strcmp(initstepping.data,"CNFE1")==0)
		ret.initstepping=channelflow::CNFE1;
	else if (strcmp(initstepping.data,"CNAB2")==0)
		ret.initstepping=channelflow::CNAB2;
	else if (strcmp(initstepping.data,"CNRK2")==0)
		ret.initstepping=channelflow::CNRK2;
	else if (strcmp(initstepping.data,"SMRK2")==0)
		ret.initstepping=channelflow::SMRK2;
	else if (strcmp(initstepping.data,"SBDF1")==0)
		ret.initstepping=channelflow::SBDF1;
	else if (strcmp(initstepping.data,"SBDF2")==0)
		ret.initstepping=channelflow::SBDF2;
	else if (strcmp(initstepping.data,"SBDF3")==0)
		ret.initstepping=channelflow::SBDF3;
	else if (strcmp(initstepping.data,"SBDF3")==0)
		ret.initstepping=channelflow::SBDF3;
	else {
		chprintf("Error in DNSFlags, initstepping must be one of the following: CNFE1, CNAB2, CNRK2, SMRK2, SBDF1, SBDF2, SBDF3, SBDF4 \n");
		return false;
	}	
	
	if (strcmp(nonlinearity.data,"Rotational")==0)
		ret.nonlinearity=channelflow::Rotational;
	else if (strcmp(nonlinearity.data,"Convection")==0)
		ret.nonlinearity=channelflow::Convection;
	else if (strcmp(nonlinearity.data,"Divergence")==0)
		ret.nonlinearity=channelflow::Divergence;
	else if (strcmp(nonlinearity.data,"SkewSymmetric")==0)
		ret.nonlinearity=channelflow::SkewSymmetric;
	else if (strcmp(nonlinearity.data,"Alternating")==0)
		ret.nonlinearity=channelflow::Alternating;
	else if (strcmp(nonlinearity.data,"Alternating_")==0)
		ret.nonlinearity=channelflow::Alternating_;
//	else if (strcmp(nonlinearity.data,"Linearized")==0)
//		ret.nonlinearity=channelflow::Linearized;
//	else if (strcmp(nonlinearity.data,"GenlLinearized")==0)
//		ret.nonlinearity=channelflow::GenlLinearized;
	else {
		chprintf("Error in DNSFlags, nonlinearity must be one of the following: 	Rotational, Convection, Divergence, SkewSymmetric, Alternating, Alternating_, Linearized, GenlLinearized \n");
		return false;
	}		
	
	
	if (strcmp(dealiasing.data,"NoDealiasing")==0)
		ret.dealiasing=channelflow::NoDealiasing;
	else if (strcmp(dealiasing.data,"DealiasXZ")==0)
		ret.dealiasing=channelflow::DealiasXZ;
	else if (strcmp(dealiasing.data,"DealiasY")==0)
		ret.dealiasing=channelflow::DealiasY;
	else if (strcmp(dealiasing.data,"DealiasXYZ")==0)
		ret.dealiasing=channelflow::DealiasXYZ;
	else {
		chprintf("Error in DNSFlags, dealiasing must be one of the following: NoDealiasing, DealiasXZ, DealiasY, DealiasXYZ\n");
		return false;
	}	
	
	if (strcmp(taucorrection.data,"true")==0)
		ret.taucorrection=true;
	else if (strcmp(taucorrection.data,"false")==0)
		ret.taucorrection=false;
	else {
		chprintf("Error in DNSFlags, taucorrection must be one of the following: true, false\n");
		return false;
	}	
	
	if (strcmp(verbosity.data,"Silent")==0) 
		ret.verbosity=channelflow::Silent;
	else if (strcmp(verbosity.data,"PrintTicks")==0) 
		ret.verbosity=channelflow::PrintTicks;
	else if (strcmp(verbosity.data,"PrintTime")==0) 
		ret.verbosity=channelflow::PrintTime;
	else if (strcmp(verbosity.data,"VerifyTauSolve")==0) 
		ret.verbosity=channelflow::VerifyTauSolve;
	else if (strcmp(verbosity.data,"PrintAll")==0) 
		ret.verbosity=channelflow::PrintAll;
	else {
		chprintf("Error in DNSFlags, verbosity must be one of the following: Silent, PrintTicks, PrintTime, VerifyTauSolve, PrintAll\n");
		return false;
	}	
		
	ret.dPdx=dPdx;
	ret.Ubulk=Ubulk;
	
	return true;
}

