include($$(CHAINLINK_DIR)/chainlink.pri)

TEMPLATE = lib
CONFIG = dll release
TARGET = 
DEPENDPATH += .;channelflow/channelflow
INCLUDEPATH += .;channelflow;channelflow/PCF-utils 
#;$$(CHAINLINK_DIR)/3rdparty/include/fftw3;$$(CHAINLINK_DIR)/chainlink_base
LIBS +=  -lfftw3

# Input
HEADERS += channelflow_library.h \
			  flowfield_ops.h 
			  
SOURCES += channelflow_library.cpp \
			  flowfield_ops.cpp

SOURCES += bandedtridiag.cpp \
		   basisfunc.cpp \
		   chebyshev.cpp \
		   diffops.cpp \
		   dns.cpp \
		   flowfield.cpp \
		   helmholtz.cpp \
		   mathdefs.cpp \
		   periodicfunc.cpp \
		   poissonsolver.cpp \
		   realprofile.cpp \
		   tausolver.cpp \
		   turbstats.cpp \
		   utilfuncs.cpp \
		   vector.cpp