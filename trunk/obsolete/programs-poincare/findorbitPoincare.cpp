#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/CColVector.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/EIG.h>
#include <octave/dbleSVD.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/periodicfunc.h"
#include "channelflow/utilfuncs.h"
#include "octutils.h"
#include "dnspoincare.h"

// This program is an implementation of Divakar Viswanath's Newton-hookstep
// GMRES algorithm for computing invariant solutions of Navier-Stokes. The
// basic idea is to find solutions of sigma f^T(u) - u = 0 where f^T(u) is the
// time-T CFD integration of the fluid velocity field u and sigma is a symmetry
// operation. T and sigma can be held fixed or varied. Viswanath's algorithm
// combines a Newton-hookstep trust-region minimization of ||sigma f^T(u) - u||^2
// with iterative GMRES solution of the Newton-step equations.

// In this program, the details of GMRES algorithm are modeled on Lecture 35
// of Trefethen and Bau's "Numerical Linear Algebra". The details of the hookstep
// algorithm are based on section 6.4.2 of Dennis and Schnabel's "Numerical Methods
// for Unconstrained Optimization and Nonlinear Equations."
//
// See end of file for a detailed description of the algorithm, notation,
// and references.
//
// John Gibson, Wed Oct 10 16:10:46 EDT 2007

using namespace std;
using namespace channelflow;

// Some global data that defines the Poincare section and fundamental domain of comp
// These have global scope so they may be accessed in a number of routines

// f(u) = f^T(u) where T is determined by Poincare condition : h(u(T)) == 0.
void f(const FlowField& u, FlowField& f_u, Real& T, PoincareCondition* h, Real Reynolds,
       DNSFlags& flags, TimeStep& dt, int& fcount, Real& CFL, Real Tmin, Real Tmax, ostream* os);


// G(u,sigma) = (sigma f^T(u) - u), where T is determined by condition h(u(T)) == 0
void G(const FlowField& u, const FieldSymmetry& sigma, FlowField& G_u, Real& T,
       PoincareCondition* h, Real Reynolds, DNSFlags& flags, TimeStep& dt,
       int& fcount, Real& CFL,  Real Tmin, Real Tmax, ostream* os);

void DG(const FlowField& u, const FlowField& du, const FieldSymmetry& sigma,
	const FieldSymmetry& dsigma, const FlowField& Gx, FlowField& DG_dx,
	PoincareCondition* h, Real Reynolds, DNSFlags& flags, TimeStep& dt,
	Real eps, bool centerdiff, int& fcount, Real& CFL, Real Tmin, Real Tmax, ostream* os);

int crosssign;
array<FlowField> efund;
array<FieldSymmetry> sfund;

int main(int argc, char* argv[]) {

  string purpose("Newton-Krylov-hookstep search for (relative) equilibrium or\n"
		 "periodic orbit of plane Couette flow, solution (u,sigma,T) of\n"
		 "sigma f^T(u) - u == 0. Temporal phase of u determined by Poincare\n"
		 "condition h(u) == 0, where h(u) == I(u)-D(u) or (u-ustar, estar)");

  ArgList args(argc, argv, purpose);


  const bool xrelative = args.getflag("-xrel",  "--xrelative",    "search over phase shift ax for relative periodic orbit");
  const bool zrelative = args.getflag("-zrel",  "--zrelative",    "search over phase shift az for relative periodic orbit");

  const Real Tmin      = args.getreal("-Tmin",   "--Tmin",     10.0,  "minimum integration time for Poincare returns");
  const Real Tmax      = args.getreal("-Tmax",   "--Tmax",    100.0,  "maximum integration time for Poincare returns");

  const int    Nb      = args.getint("-Nb", "--Nbasis", 0, "number of basis fields");
  const string bdir    = args.getpath("-bd", "--bdir", "./", "dir of basis functions that define fundamental domain");

  /********/ crosssign = args.getint("-c", "--crosssign" ,  -1, "req. sign of dh/dt for crossing h(u)==0 (0 means either)");
  const bool energysection  = args.getflag("-esect", "--energySection", "poincare section defined by I-D == 0");

  const string ustar_str  = args.getstr("-us", "--ustar", "ustar", "a point (field) on poincare section");
  const string estar_str  = args.getstr("-es", "--estar", "estar", "a normal (field), defines poinc section");

  const Real epsSearch = args.getreal("-es",  "--epsSearch",   1e-13, "stop search if L2Norm(s f^T(u) - u) < epsEQB");
  const Real epsKrylov = args.getreal("-ek",  "--epsKrylov",   1e-14, "min. condition # of Krylov vectors");
  const Real epsDu     = args.getreal("-edu", "--epsDuLinear", 1e-7,  "rel size of du in finite diff linearization of");
  const Real epsDt     = args.getreal("-edt", "--epsDtLinear", 1e-7,  "rel size of dT in finite diff linearization of ");
  const Real epsDsigma = args.getreal("-eds", "--epsDsigmaLinear", 1e-7,  "size of dsigma in linearization of sigma f^T about T");
  const Real epsGMRES  = args.getreal("-eg",  "--epsGMRES",    1e-3,  "stop GMRES iteration when Ax=b residual is < this");
  const Real epsGMRESf = args.getreal("-egf", "--epsGMRESfinal",  0.05,  "accept final GMRES iterate if residual is < this");
  const bool centdiff  = args.getflag("-cd", "--centerdiff", "centered differencing to estimate differentials");

  const int  Nnewton   = args.getint( "-Nn", "--Nnewton",   20,    "max number of Newton steps ");
  const int  Ngmres    = args.getint( "-Ng", "--Ngmres",    80,    "max number of GMRES iterations per restart");
  const int  Nhook     = args.getint( "-Nh", "--Nhook",     20,    "max number of hookstep iterations per Newton");

  /***/ Real delta     = args.getreal("-d",    "--delta",      0.01,  "initial radius of trust region");
  const Real deltaMin  = args.getreal("-dmin", "--deltaMin",   1e-12, "stop if radius of trust region gets this small");
  const Real deltaMax  = args.getreal("-dmax", "--deltaMax",   0.1,   "maximum radius of trust region");
  const Real deltaFuzz = args.getreal("-df",   "--deltaFuzz",  0.01,  "accept steps within (1+/-deltaFuzz)*delta");
  const Real lambdaMin = args.getreal("-lmin",   "--lambdaMin", 0.2,  "minimum delta shrink rate");
  const Real lambdaMax = args.getreal("-lmax",   "--lambdaMax", 1.5,  "maximum delta expansion rate");
  const Real lambdaRequiredReduction = 0.5; // when reducing delta, reduce by at least this factor.
  const Real improvReq = args.getreal("-irq",  "--improveReq", 1e-3,  "reduce delta and recompute hookstep if improvement "
				      " is worse than this fraction of what we'd expect from gradient");
  const Real improvOk  = args.getreal("-iok",  "--improveOk",   0.10, "accept step and keep same delta if improvement "
				      "is better than this fraction of quadratic model");
  const Real improvGood= args.getreal("-igd",  "--improveGood", 0.75, "accept step and increase delta if improvement "
				      "is better than this fraction of quadratic model");
  const Real improvAcc = args.getreal("-iac",  "--improveAcc",  0.10, "recompute hookstep with larger trust region if "
				      "improvement is within this fraction quadratic prediction.");

  const Real TscaleRel = args.getreal("-Tsc", "--Tscale",     20,    "scale time in hookstep: |T| = Ts*|u|/L2Norm(du/dt)");
  //const bool dudtortho = args.getbool("-tc",  "--dudtConstr", true,  "require orthogonality of step to dudt");
  const Real orthoscale= args.getreal("-os",  "--orthoScale", 1,     "rescale orthogonality constraint");

  const string signame = args.getstr ("-sigma", "--sigma", "", "file containing symmetry of rel orbit");
  const string symmstr = args.getstr ("-symms", "--symmetries", "", "file containing generators of isotropy group for symmetry-constrained search");

  const bool Rscale    = args.getreal("-rs", "--relativeScale", 1, "scale relative-search variables by this factor");
  const bool dudxortho = args.getbool("-xc", "--dudxConstraint", true,"require orthogonality of step to dudx and dudz");

  const bool l2basis   = false; // old functionality, will remove soon.

  const Real Reynolds  = args.getreal("-R", "--Reynolds", 400, "Reynolds number");
  const bool vardt     = args.getflag("-vdt","--variableDt", "adjust dt to keep CFLmin<=CFL<CFLmax");
  const Real dtarg     = args.getreal("-dt","--timestep",  0.03125, "(initial) integration timestep");
  const Real dtmin     = args.getreal("-dtmin", "--dtmin", 0.001, "minimum timestep");
  const Real dtmax     = args.getreal("-dtmax", "--dtmax", 0.04,  "maximum timestep");
  const Real dTCFL     = args.getreal("-dTCFL", "--dTCFL", 1.00,  "check CFL # at this interval");
  const Real CFLmin    = args.getreal("-CFLmin", "--CFLmin", 0.40, "minimum CFL number");
  const Real CFLmax    = args.getreal("-CFLmax", "--CFLmax", 0.60, "maximum CFL number");
  const string stepstr = args.getstr("-ts", "--timestepping", "SBDF3", "timestep algorithm: See README.");
  const string nonlstr = args.getstr("-nl", "--nonlinearity", "Rotational", "nonlinearity method: See README.");

  const int  coutprec  = args.getint("-cp", "--coutprec", 6, "precison for std output");
  const bool savesteps = args.getflag("-ss", "--savesteps", "save each Newton-hookstep");
  const string outdir  = args.getpath("-o", "--outdir", "./", "output directory");
  const string uname   = args.getstr(1, "<flowfield>", "initial guess for Newton search");

  const Real EPSILON = 1e-13;
  args.check();

  //if (!(eqb^orb)) {
  //cerr << "Please choose either -eqb or -orb option to search for (relative) equilibrium or (relative) periodic orbit" << endl;
  //exit(1);
  //}
  args.save("./");

  const BasisType nrm = l2basis ? L2Basis : AdHocBasis;
  const int w = coutprec + 7;
  //const bool Tsearch    = orb ? true : false;
  //const bool Tnormalize = orb ? false : true;
  const bool Tsearch    = false;
  //const bool Tnormalize = true;
  const bool Rxsearch  = xrelative;
  const bool Rzsearch  = zrelative;

  cout << "Working directory == " << pwd() << endl;
  cout << "Command-line args == ";
  for (int i=0; i<argc; ++i) cout << argv[i] << ' '; cout << endl;

  DNSFlags dnsflags;
  dnsflags.baseflow     = PlaneCouette;
  dnsflags.timestepping = s2stepmethod(stepstr);
  dnsflags.dealiasing   = DealiasXZ;
  dnsflags.nonlinearity = s2nonlmethod(nonlstr);
  dnsflags.constraint   = PressureGradient;
  dnsflags.dPdx         = 0.0;
  dnsflags.verbosity    = Silent;

  if (symmstr.length() > 0) {
    SymmetryList symms(symmstr);
    cout << "Restricting flow to invariant subspace generated by symmetries" << endl;
    cout << symms << endl;
    dnsflags.symmetries = symms;
  }

  cout << "DNSFlags == " << dnsflags << endl << endl;

  // A basis set to define fundamental domain: (u(t), efund[n]) > 0
  // and symmetries that map crossings back into fundamental domain
  efund = array<FlowField>(Nb);
  sfund = array<FieldSymmetry>(Nb);
  for (int n=0; n<Nb; ++n) {
    efund[n] = FlowField(bdir + "e" + i2s(n));
    sfund[n] = FieldSymmetry(bdir + "s" + i2s(n));

    FlowField se = sfund[n](efund[n]);
    cout << "L2IP(e,se) == " << L2IP(efund[n], se) << " for n == " << n << endl;
  }


  PoincareCondition* h;
  if (energysection)
    h = new DragDissipation();
  else {
    FlowField estar(estar_str);
    FlowField ustar(ustar_str);
    h = new PlaneIntersection(ustar, estar);
  }

  int fcount_newton = 0;
  int fcount_hookstep = 0;

  TimeStep dt(dtarg, dtmin, dtmax, dTCFL, CFLmin, CFLmax, vardt);
  cout << "dt    == " << dt << endl;

  FlowField u(uname);
  project(dnsflags.symmetries, u, "initial field u", cout);
  fixdivnoslip(u);

  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();
  const Real Lx = u.Lx();
  const Real ya = u.a();
  const Real yb = u.b();
  const Real Lz = u.Lz();
  //const int kxmax = u.kxmaxDealiased();
  //const int kzmax = u.kzmaxDealiased();

  vector<RealProfileNG> basis;
  ColumnVector dxN;   // "dx Newton" : the Newton step, computed w GMRES
  field2vector(u, dxN, basis);

  // Mcon == number of constraints
  // Neqn == number of equations (total)
  // Nunk == number of unknowns

  const int uunk = dxN.length();                                // # of variables for u unknonwn
  const int Tunk = Tsearch  ?  uunk : -1;                       // index for T unknown
  const int xunk = Rxsearch ? (uunk + Tsearch) : -1;            // index for x-shift unknown
  const int zunk = Rzsearch ? (uunk + Tsearch + Rxsearch) : -1; // index for z-shift unknown
  const int Nunk = uunk + Tsearch + Rxsearch + Rzsearch;
  const int xeqn = xunk;
  const int zeqn = zunk;
  const int Teqn = Tunk;
  const int Neqn = Nunk;
  const int Ngmres2 = (Nunk < Ngmres) ? Nunk : Ngmres;


  cout << Neqn << " equations" << endl;
  cout << Nunk << " unknowns" << endl;
  if (Tsearch) {
    cout << "Teqn  == " << Teqn << endl;
    cout << "Tunk  == " << Tunk << endl;
  }
  if (Rxsearch) {
    cout << "xeqn  == " << xeqn << endl;
    cout << "xunk  == " << xunk << endl;
  }
  if (Rzsearch) {
    cout << "zeqn  == " << zeqn << endl;
    cout << "zunk  == " << zunk << endl;
  }

  dxN = ColumnVector(Nunk);

  // These are purely diagnostic variables. I want to see how these directions change per iteration
  ColumnVector prev_dxH(Nunk); // previous hookstep
  ColumnVector prev_dxN(Nunk); // previous newton

  // ==============================================================
  // Initialize Newton iteration
  // Notation:
  //    x = set of free variables: (u), (u,T), (u,sigma), or (u,sigma,T)
  // N means Newton, H means Hookstep, e.g.
  //   dxN is the Newton step
  //   dxH is the Hookstep

  FlowField      du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // temporary for increments in orbit velocity field
  FlowField      Gx(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // G(x) = G(u,sigma) = sigma f^T(u) - u, where h(f^T(u)) == 0
  FlowField   DG_dx(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // "DG*dx" = 1/e [G(u+e.du, sigma+e.dsigma) - G(u,sigma)]
  FlowField       p(Nx, Ny, Nz, 1, Lx, Lz, ya, yb); // pressure field for time integration
  FlowField      uH(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // stores Newton uN = u + duN and Hookstep uH = u + duH fields
  FlowField      GH(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // stores GN = G(dx+dxH) and GH = G(dx + dxH)

  Real tH  = 0.0;  // stores T of newton step and hookstep
  Real dax = 0.0;  // Newton step for x phase shift
  Real daz = 0.0;  // Newton step for z phase shift

  FieldSymmetry sigma;
  if (signame.length() != 0)
    sigma = FieldSymmetry(signame);

  //cout << "Initial integration time T == " << T << endl;
  cout << "Initial symmetry     sigma == " << sigma << endl;

  FieldSymmetry dsigma; // identity, for now
  FieldSymmetry sigmaH(sigma);

  ChebyCoeff U(Ny, ya, yb, Spectral);
  U[1] = 1;

  Real T = 0.0;
  Real rx = 0.0;        // Dennis & Schnabel residual r(x) = 1/2 ||G(x)||^2 == 1/2 VNorm2(G(x))
  Real gx = 0.0;        // g(x) = L2Norm(G(x))
  Real init_rx = 0.0;
  Real init_gx = 0.0;
  Real prev_rx = 0.0;
  Real prev_gx = 0.0;
  Real gmres_residual = 0.0;
  Real dxHnorm  = 0.0;   // norm of hookstep
  Real dxNnorm  = 0.0;   // norm of newton step
  Real dunorm  = 0.0;
  Real unorm   = 0.0;
  Real CFL = 0.0;
  bool stuck = false;

  // Determine which of four half-cell translations sigma minimizes 1/2 || sigma f^T(u) - u ||^2
  cout << "Search for solutions of sigma f^T(u) - u == 0 with " << endl;
  cout << "  sigma == " << sigma << endl;

  ofstream osconv((outdir + "convergence.asc").c_str());
  osconv.setf(ios::left);
  osconv << setw(14) << "% L2Norm(G)" << setw(14) << "r(u,T)" << setw(14) << "delta";
  //if (Tsearch)
  //osconv << setw(14) << "dT";
  if (Rxsearch || Rzsearch) {
    osconv << setw(14) << "dax";
    osconv << setw(14) << "daz";
  }
  osconv << setw(14) << "L2Norm(du)" << setw(14) << "L2Norm(dxN)" << setw(14) << "L2Norm(dxH)"
	 << setw(14) << "|DG dxN + G(x)|/|G(x)|" << setw(10) << "ftotal" << setw(10) << "fnewt"
	 << setw(10) << "fhook" << endl;

  cout << setprecision(coutprec);

  cout << "Computing G(x) = sigma f^T(u) - u, where T is determined by Poincare condition h(f^T(u)) == 0" << endl;
  G(u, sigma, Gx, T, h, Reynolds, dnsflags, dt, fcount_hookstep, CFL, Tmin, Tmax, &cout);
  cout << endl;

  for (int newtonStep = 0; newtonStep<=Nnewton; ++newtonStep) {

    cout << "========================================================" << endl;
    cout << "Newton iteration number " << newtonStep << endl;

    // Compute quantities that will be used multiple times in GMRES loop
    // Gx     = (sigma f^T(u) - u)
    // dfxdT  = 1/(dt) (f^(T+dt)(u) - f^(T)(u))
    // dudt   = 1/(dt) (f^(dt)(u) - u)
    //        = tangent vector to flow
    // edudx  = tangent vector of x translation
    // edudz  = tangent vector of z translation

    dunorm = L2Norm(du);
    unorm = L2Norm(u);
    rx = 0.5*Norm2(Gx, nrm);
    gx = L2Norm(Gx);

    cout << "Computing normalized edudx, edudz" << endl;
    // Calculate three unit vectors to which du must be orthogonal
    FlowField edudx = xdiff(u);
    FlowField edudz = zdiff(u);
    FlowField u_du(u);
    //f(u, 1, epsDt, edudt, Reynolds, dnsflags, &cout);   // using dt=epsDt timestep
    //edudt -= u;
    //edudt *= 1.0/epsDt;

    // Set Tscale so that |dT|*Tscale = TscaleRel |du|
    // Tscale = TscaleRel |du|/|dt|
    // O(t)*Tscale = O(u) => Tscale = u/t
    //const Real Tscale = TscaleRel*V2Norm(edudt);
    const Real Tscale = TscaleRel;
    cout << "delta  == " << delta << endl;
    cout << "Tscale == " << Tscale << endl;
    //edudt *= 1.0/L2Norm(edudt);
    edudx *= 1.0/L2Norm(edudx);
    edudz *= 1.0/L2Norm(edudz);

    rx = 0.5*Norm2(Gx, nrm);
    gx = L2Norm(Gx);

    if (newtonStep == 0) {
      init_gx = gx;
      prev_gx = gx;
      init_rx = rx;
      prev_rx = rx;
    }

    FlowField uU(u);
    uU += U;

    cout << "Current state of Newton iteration:" << endl;
    //cout << "dissip (u)     == " << dissipation(u) << endl;
    //cout << "forcing(u)     == " << forcing(u) << endl;
    //cout << "energy (u)     == " << 0.5*square(unorm) << endl;
    //cout << "dissip (u+U)   == " << dissipation(uU) << endl;
    //cout << "forcing(u+U)   == " << forcing(uU) << endl;
    //cout << "energy (u+U)   == " << 0.5*L2Norm2(uU) << endl;
    //cout << "fcount_newton  == " << fcount_newton << endl;
    //cout << "fcount_hookstep== " << fcount_hookstep << endl;
    cout << "    L2Norm(u)  == " << unorm << endl;
    cout << "   L2Norm(du)  == " << dunorm <<endl;
    cout << "  L2Norm(dxH)  == " << dxHnorm <<endl;
    cout << "           T   == " << T << endl;
    //cout << "          dT   == " << dT << endl;
    cout << "       sigma   == " << sigma << endl;
    cout << "      dsigma   == " << dsigma << endl;
    cout << "L2Dist(u,u0)   == " << L2Dist(u, FlowField(uname)) << endl;
    cout << "gx == L2Norm(G(x)) : " << endl;
    cout << "   initial  gx == " << init_gx << endl;
    cout << "   previous gx == " << prev_gx << endl;
    cout << "   current  gx == " << gx << endl;
    cout << "rx == 1/2 Norm2(G(x)) : " << endl;
    cout << "   initial  rx == " << init_rx << endl;
    cout << "   previous rx == " << prev_rx << endl;
    cout << "   current  rx == " << rx << endl;
    cout << "1/2 L2Norm2(G,u,T) : " << endl;
    cout << "               == " << 0.5*square(gx) << endl;
    cout << "CFL == " << CFL << endl;

    osconv.setf(ios::left); osconv << setw(14) << gx;
    osconv.setf(ios::left); osconv << setw(14) << rx;
    osconv.setf(ios::left); osconv << setw(14) << delta;
    //if (Tsearch) {
    //osconv.setf(ios::left); osconv << setw(14) << dT;
    //}
    if (Rxsearch || Rzsearch) {
      osconv.setf(ios::left); osconv << setw(14) << dsigma.ax();
      osconv.setf(ios::left); osconv << setw(14) << dsigma.az();
    }
    osconv.setf(ios::left); osconv << setw(14) << dunorm;
    osconv.setf(ios::left); osconv << setw(14) << dxNnorm;
    osconv.setf(ios::left); osconv << setw(14) << dxHnorm;
    osconv.setf(ios::left); osconv << setw(14) << gmres_residual;
    osconv.setf(ios::left); osconv << setw(10) << fcount_newton + fcount_hookstep;
    osconv.setf(ios::left); osconv << setw(10) << fcount_newton;
    osconv.setf(ios::left); osconv << setw(10) << fcount_hookstep;
    osconv << endl;

    if (savesteps) {
      u.save(outdir + "unewt" + i2s(newtonStep));
      sigma.save(outdir + "sigmanewt" + i2s(newtonStep));
      save(T, outdir + "Tnewt" + i2s(newtonStep));
      Gx.save(outdir + "errnewt" + i2s(newtonStep));
    }
    u.save(outdir + "ubest");
    sigma.save(outdir + "sigmabest");
    save(T, outdir + "Tbest");
    save(Reynolds, outdir + "Reynolds");

    //for (int p=0; p<P; ++p)
    //stepos << L2IP(u, e[p]) << ' ';
    //stepos << endl;

    if (gx < epsSearch) {
      cout << "Newton search converged. Breaking." << endl;
      cout << "L2Norm(G(x))) == " << gx << " < "
	   << epsSearch << " == " << epsSearch << endl;
      //u.save(outdir + "usoln");
      //if (Tsearch || Rsearch)
      //save(outdir + "sigmasoln", T, sigma);
      break;
    }
    else if (stuck) {
      cout << "Newton search is stuck. Breaking." << endl;
      break;
    }
    else if (newtonStep == Nnewton) {
      cout << "Reached maximum number of Newton steps. Breaking." << endl;
      break;
    }


    prev_gx = gx;
    prev_rx = rx;

    // Set up RHS vector b = (-field2vector(G(x)), 0, 0, 0);
    // (0, 0, 0) is the RHS of eqn enforcing orthog. to (edudt, edudx, edudz)
    ColumnVector b(Neqn);
    setToZero(b);
    field2vector(Gx, b, basis);
    if (Tsearch)
      b(Teqn) = 0;
    if (Rxsearch)
      b(xeqn) = 0;
    if (Rzsearch)
      b(zeqn) = 0;

    b *= -1.0;

    GMRES gmres(b, Ngmres2, epsKrylov);

    // ===============================================================
    // GMRES iteration to solve DG(x) dx = -G(x) for dx = (du, dsigma)
    for (int n=0; n<Ngmres2; ++n) {

      cout << "Newt,GMRES == " << newtonStep << ',' << n << ", " << flush;

      // Compute v = Ab in Arnoldi iteration terms, where b is Q.column(n)
      // In Navier-Stokes terms, the main quantity to compute is
      // DG dx = 1/e (G(u + e du, sigma + e dsigma) - G(u,sigma)) for e << 1

      ColumnVector q = gmres.testVector();
      vector2field(q, du, basis);
      //dT  = Tsearch ? q(Tunk)*Tscale : 0.0;
      dax = Rxsearch ? q(xunk)*Rscale : 0.0;
      daz = Rzsearch ? q(zunk)*Rscale : 0.0;
      dsigma = FieldSymmetry(1,1,1,dax,daz);

      // Compute DG dx = 1/e (G(u+e.du, sigma+e.dsigma) - G(u,sigma))
      DG(u, du, sigma, dsigma, Gx, DG_dx, h, Reynolds, dnsflags, dt, epsDu, centdiff, fcount_newton, CFL, Tmin, Tmax, &cout);

      ColumnVector Lq(Neqn);

      field2vector(DG_dx, Lq, basis);

      if (Tsearch) {
	u_du = u;
	u_du += du;
	Lq(Teqn) = (*h)(u_du);
      }
      if (Rxsearch)
	Lq(xeqn) = dudxortho ? orthoscale*L2InnerProduct(du, edudz) : 0.0;
      if (Rzsearch)
	Lq(zeqn) = dudxortho ? orthoscale*L2InnerProduct(du, edudx) : 0.0;

      gmres.iterate(Lq);
      gmres_residual = gmres.residual();

      cout << " res == " << gmres_residual << endl;

      if (gmres_residual < epsGMRES) {
        cout << "GMRES converged. Breaking." <<endl;
	dxN = gmres.solution();
	break;
      }
      else if (n==Ngmres-1 && gmres_residual < epsGMRESf ) {
        cout << "GMRES has not converged, but the final iterate is acceptable. Breaking." <<endl;
	dxN = gmres.solution();
      }
      else if (n==Ngmres-1) {
	cout << "GMRES failed to converge. Exiting." << endl;
	exit(1);
      }
    } // end GMRES iteration

    dxNnorm = L2Norm(dxN);

    // Diagnostic
    //vector2field(dxN, du, basis);
    //du.save("dxN" + i2s(newtonStep));

    // ==================================================================
    // Hookstep algorithm

    cout << "------------------------------------------------" << endl;
    cout << "Beginning hookstep calculations." << endl;

    int Nk = gmres.n(); // Krylov dimension
    Matrix Hn = gmres.Hn();
    Matrix Qn = gmres.Qn();
    Matrix Qn1t = gmres.Qn1().transpose(); // wasteful of memory
    SVD svd(Hn, SVD::std);

    Matrix V = svd.right_singular_matrix();
    Matrix Ut = svd.left_singular_matrix().transpose();
    DiagMatrix D = svd.singular_values();

    ColumnVector btmp = Qn1t*b;
    ColumnVector bh = Ut*btmp; // b hat

    int hookcount = 0;

    // Hookstep algorithm requires iterated tweaking. Remember the best hookstep
    // with these variables, so we can revert to it if necessary.
    bool have_backup  = false;
    Real backup_dxHnorm = 0.0;
    Real backup_delta = 0.0;
    Real backup_rH    = 0.0;
    Real backup_tH    = tH;
    FlowField backup_du;
    FlowField backup_GH;
    FieldSymmetry backup_dsigma;

    ColumnVector dxH;  // The hookstep

    Real deltaMaxLocal = deltaMax;

    // Find a good trust region and the optimal hookstep in it.
    while (true) {

      cout << "-------------------------------------------" << endl;
      cout << "Newton, hookstep number " << newtonStep << ", "
	   << hookcount++ << endl;
      cout << "delta == " << delta << endl;

      bool hookstep_equals_newtonstep;

      if (L2Norm(dxN) <= delta) {
	cout << "Newton step is within trust region: " << endl;
	cout << "L2Norm(dxN) == " << L2Norm(dxN) << " <= " << delta << " == delta" << endl;
	hookstep_equals_newtonstep = true;
	dxH = dxN;
      }
      else {
	cout << "Newton step is outside trust region: " << endl;
	cout << "L2Norm(dxN) == " << L2Norm(dxN) << " > " << delta << " == delta" << endl;
	cout << "Calculate hookstep dxH(mu) with radius L2Norm(dxH(mu)) == delta" << endl;

	hookstep_equals_newtonstep = false;

	// This for-loop determines the hookstep. Search for value of mu that
	// produces ||dxHmu|| == delta. That provides optimal reduction of
	// quadratic model of residual in trust region norm(dxHmu) <= delta.
	ColumnVector dxHmu(Nk); // dxH(mu) : Hookstep dxH as a function of parameter
	Real ndxHmu = 0;        // norm of dxHmu
	Real mu = 0;            // a parameter we search over to find dxH with radius delta

	// See Dennis and Schnabel for this search-over-mu algorithm
	for (int hookSearch=0; hookSearch<Nhook; ++hookSearch) {
	  for (int i=0; i<Nk; ++i)
	    dxHmu(i) = bh(i)/(D(i,i) + mu); // research notes
	  ndxHmu = L2Norm(dxHmu);
	  Real Phi = ndxHmu*ndxHmu - delta*delta;
	  cout << "mu, L2Norm(dxH(mu)) == " << mu << ", " << ndxHmu << endl;

	  // Found satisfactory value of mu and thus dxHmu and dxH s.t. |dxH| < delta
	  if (ndxHmu < delta || (ndxHmu > (1-deltaFuzz)*delta && ndxHmu < (1+deltaFuzz)*delta))
	    break;

	  // Update value of mu and try again. Update rule is a Newton search for
	  // Phi(mu)==0 based on a model of form a/(b+mu) for norm(sh(mu)). See
	  // Dennis & Schnabel.
	  else if (hookSearch<Nhook-1) {
	    Real PhiPrime = 0.0;
	    for (int i=0; i<Nk; ++i) {
	      Real di_mu = D(i,i) + mu;
	      Real bi = bh(i);
	      PhiPrime -= 2*bi*bi/(di_mu*di_mu*di_mu);
	    }
	    mu -= (ndxHmu/delta) * (Phi/PhiPrime);
	  }
	  else {
	    cout << "Couldn't find solution of hookstep optimization eqn Phi(mu)==0" << endl;
	    cout << "This shouldn't happen. It indicates an error in the algorithm." << endl;
	    cout << "Exiting.\n";
	    exit(1);
	  }
	} // search over mu for norm(s) == delta

	cout << "Found hookstep of proper radius" << endl;
	cout << "ndxHmu == " << L2Norm(dxHmu) << " ~= " << delta << " == delta " << endl;
	dxH = Qn*(V*dxHmu);
      } // end else clause for Newton step outside trust region


      // Compute
      // (1) actual residual from evaluation of
      //     rH == r(x+dxH) act  == 1/2 (G(x+dxH), G(x+dxH))
      //
      // (2) predicted residual from quadratic model of r(x)
      //     rP == r(x+dx) pred  == 1/2 (G(x), G(x)) + (G(x), DG dx)
      //
      // (3) slope of r(x)
      //     dr/dx == (r(x + dx) - r(x))/|dx|
      //           == (G(x), DG dx)/|dx|
      // where ( , ) is FlowField inner product V2IP, which equals L2 norm of vector rep

      // (1) Compute actual residual of step, rH
      cout << "Computing residual of hookstep dxH" << endl;
      vector2field(dxH, du, basis);
      project(dnsflags.symmetries, du, "Newton step du", cout);

      dunorm = L2Norm(du);
      uH  = u;
      uH += du;
      //dT  = Tsearch  ? dxH(Tunk)*Tscale : 0;
      dax = Rxsearch ? dxH(xunk)*Rscale : 0.0;
      daz = Rzsearch ? dxH(zunk)*Rscale : 0.0;
      dsigma = FieldSymmetry(1,1,1,dax,daz);

      cout << "L2Norm(du) == " << L2Norm(du) << endl;
      //cout << "       dT  == " << dT << endl;
      cout << "     dsigma  == " << dsigma << endl;
      //tH  = T + dT;
      sigmaH = dsigma*sigma;
      G(uH, sigmaH, GH, tH, h, Reynolds, dnsflags, dt, fcount_hookstep, CFL, Tmin, Tmax, &cout);
      cout << endl;

      Real rH = 0.5*Norm2(GH, nrm); // actual residual of hookstep
      Real Delta_rH = rH - rx;     // improvement in residual
      cout << "r(x), r(x+dxH) == " << rx << ", " << rH << endl;

      // (2) and (3) Compute quadratic model and slope
      cout << "Computing local quadratic model of residual" << endl;
      DG(u, du, sigma, dsigma, Gx, DG_dx, h, Reynolds, dnsflags, dt, epsDu,  centdiff, fcount_newton, CFL, Tmin, Tmax, &cout);
      cout << endl;

      // Local quadratic and linear models of residual, based on Taylor exp at current position
      // Quadratic model of r(x+dx): rQ == 1/2 |G(x) + DG dx|^2
      // Linear  model   of r(x+dx): rL == 1/2 (G(x), G(x)) + (G(x), DG dx)

      Real Delta_rL = IP(Gx,DG_dx,nrm); // rL - 1/2 (G(x), G(x)) == (G(x), DG dx)
      Real rL = rx + Delta_rL;          // rL == 1/2 (G(x), G(x)) + (G(x), DG dx)

      if (rL >= rx) {
	cout << "error : local linear model of residual is increasing, indicating\n"
	     << "        that the solution to the Newton equations is inaccurate\n";

	if (centdiff == true) {
	  cout << "exiting." << endl;
	  exit(1);
	}
	else {
	  cout << "Trying local linear model again, using centered finite differencing" << endl;
	  DG(u, du, sigma, dsigma, Gx, DG_dx, h, Reynolds, dnsflags, dt, epsDu,  true, fcount_newton, CFL, Tmin, Tmax, &cout);

	  Delta_rL = IP(Gx,DG_dx,nrm); // rL - 1/2 (G(x),G(x)) == (G(x), DG dx)
	  rL = rx + Delta_rL;          // rL == 1/2 (G(x),G(x)) + (G(x), DG dx)

	  if (rL >= rx) {
	    cout << "error : centered differencing didn't help\n"
		 << "exiting." << endl;
	    exit(1);
	  }
	  // if we get here, centered differencing does help, and we can continue
	}
      }

      DG_dx += Gx;
      Real rQ = 0.5*Norm2(DG_dx,nrm);   // rQ == 1/2 |G(x) + DG dx|^2
      Real Delta_rQ = rQ - rx;          // rQ == 1/2 (G(x),G(x)) - 1/2 |G(x) + DG dx|^2

      dxHnorm = L2Norm(dxH);

      // ===========================================================================
      cout << "Determining what to do with current Newton/hookstep and trust region" << endl;

      // Coefficients for non-local quadratic model of residual, based on r(x), r'(x) and r(x+dx)
      // dr/dx == (G(x), DG dx)/|dx|
      Real drdx  = Delta_rL/dxHnorm;

      // Try to minimize a quadratic model of the residual
      // r(x+dx) = r(x) + r' |dx| + 1/2 r'' |dx|^2
      Real lambda = -0.5*drdx*dxHnorm/(rH - rx - drdx*dxHnorm);

      // Compare the actual reduction in residual to the quadratic model of residual.
      // How well the model matches the actual reduction will determine, later on, how and
      // when the radius of the trust region should be changed.
      Real Delta_rH_req  = improvReq*drdx*dxHnorm;  // the minimum acceptable change in residual
      Real Delta_rH_ok   = improvOk*Delta_rQ;       // acceptable, but reduce trust region for next newton step
      Real Delta_rH_good = improvGood*Delta_rQ;     // acceptable, keep same trust region in next newton step
      Real Delta_rQ_acc  = abs(improvAcc*Delta_rQ); // for accurate models, increase trust region and recompute hookstep
      Real Delta_rH_accP = Delta_rQ + Delta_rQ_acc; //   upper bound for accurate predictions
      Real Delta_rH_accM = Delta_rQ - Delta_rQ_acc; //   lower bound for accurate predictions

      // Characterise change in residual Delta_rH
      ResidualImprovement improvement;  // fine-grained characterization

      // Place improvement in contiguous spectrum: Unacceptable > Poor > Ok > Good.
      if (Delta_rH > Delta_rH_req)
	improvement = Unacceptable;     // not even a tiny fraction of linear rediction
      else if (Delta_rH > Delta_rH_ok)
	improvement = Poor;             // worse than small fraction of quadratic prediction
      else if (Delta_rH < Delta_rH_ok && Delta_rH > Delta_rH_good)
	improvement = Ok;
      else {
	improvement = Good;             // not much worse or better than large fraction of prediction
	if (Delta_rH_accM <= Delta_rH &&  Delta_rH <= Delta_rH_accP)
	  improvement = Accurate;         // close to quadratic prediction
	else if (Delta_rH < Delta_rL)
	  improvement = NegaCurve;        // negative curvature in r(|s|) => try bigger step
      }

      cout << "rx       == " << setw(w) << rx            << " residual at current position" << endl;
      cout << "rH       == " << setw(w) << rH            << " residual of newton/hookstep" << endl;
      cout << "Delta_rH == " << setw(w) << Delta_rH      << " actual improvement in residual from newton/hookstep" << endl;
      cout << endl;

      if (improvement==Unacceptable)
      cout << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Unacceptable <------ " << endl;
      cout << "            " << setw(w) << Delta_rH_req  << " lower bound for acceptable improvement" << endl;
      if (improvement==Poor)
      cout << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Poor <------" << endl;
      cout << "            " << setw(w) << Delta_rH_ok   << " upper bound for ok improvement." << endl;
      if (improvement==Ok)
      cout << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Ok <------" << endl;
      cout << "            " << setw(w) << Delta_rH_good << " upper bound for good improvement." << endl;
      if (improvement==Good)
      cout << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Good <------" << endl;
      cout << "            " << setw(w) << Delta_rH_accP << " upper bound for accurate prediction." << endl;
      if (improvement==Accurate)
      cout << "Delta_rH == " << setw(w) << Delta_rH      << " ------> Accurate <------" << endl;
      cout << "            " << setw(w) << Delta_rH_accM << " lower bound for accurate prediction." << endl;
      cout << "            " << setw(w) << Delta_rL      << " local linear model of improvement" << endl;
      if (improvement==NegaCurve)
      cout << "Delta_rH == " << setw(w) << Delta_rH      << " ------> NegativeCurvature <------" << endl;

      bool recompute_hookstep = false;

      cout << "lambda       == " << lambda << " is the reduction/increase factor for delta suggested by quadratic model" << endl;
      cout << "lambda*delta == " << lambda*delta << " is the delta suggested by quadratic model" << endl;

      // See comments at end for outline of this control structure

      // Following Dennis and Schnable, if the Newton step lies within the trust region,
      // reset trust region radius to the length of the Newton step, and then adjust it
      // further according to the quality of the residual.
      //if (hookstep_equals_newtonstep)
      //delta = dxHnorm;

      // CASE 1: UNACCEPTABLE IMPROVEMENT
      // Improvement is so bad (relative to gradient at current position) that we'll
      // in all likelihood get better results in a smaller trust region.
      if (improvement == Unacceptable) {
	cout << "Improvement is unacceptable." << endl;

	if (have_backup) {
	  cout << "But we have a backup step that was acceptable." << endl;
	  cout << "Revert to backup step and backup delta, take the step, and go to next Newton-GMRES iteration." << endl;
	  dsigma = backup_dsigma;
	  du = backup_du;
	  tH = backup_tH;
	  GH = backup_GH;
	  rH = backup_rH;
	  dxHnorm = backup_dxHnorm;
	  delta = backup_delta;

	  recompute_hookstep = false;
	}
	else {
	  cout << "No backup step is available." << endl;
	  cout << "Reduce trust region by minimizing local quadratic model and recompute hookstep." << endl;
	  deltaMaxLocal = delta;
	  lambda = adjustLambda(lambda, lambdaMin, lambdaRequiredReduction);
	  delta  = adjustDelta(delta, lambda, deltaMin, deltaMax);

	  if (delta > dxHnorm) {
	    cout << "That delta is still bigger than the Newton step." << endl;
	    //cout << "This shouldn't happen. Control structure needs review." << endl;
	    cout << "Reducing delta to half the length of the Newton step." << endl;
	    cout << "  old delta == " << delta << endl;
	    delta = 0.5*dxHnorm;
	    cout << "  new delta == " << delta << endl;
	  }
	  //delta_has_decreased = true;
	  recompute_hookstep = true;
	}
      }

      // CASE 2: EXCELLENT IMPROVEMENT AND ROOM TO GROW
      // Improvement == Accurate or Negacurve means we're likely to get a significant improvement
      // by increasing trust region. Increase trust region by a fixed factor (rather than quadratic
      // model) so that trust-region search is monotonic.
      // Exceptions are
      //   have_backup && backup_rH < rH -- residual is getting wrose as we increase delta
      //   hookstep==newtonstep          -- increasing trust region won't change answer
      //   delta>=deltamax               -- we're already at the largest allowable trust region
      else if ((improvement == NegaCurve || improvement == Accurate)
	       && !(have_backup && backup_rH < rH)
	       && !hookstep_equals_newtonstep
	       && !(delta >= deltaMax)) {

	cout << "Improvement is " << improvement << endl;

	//lambda = adjustLambda(lambda, lambdaMin, lambdaMax);
	Real new_delta = adjustDelta(delta, lambdaMax, deltaMin, deltaMax);

	if (new_delta < deltaMaxLocal) {
	  cout << "Continue adjusting trust region radius delta because" << endl;
	  cout << "Suggested delta has room: new_delta < deltaMaxLocal " << new_delta << " < " << deltaMaxLocal << endl;
	  if (have_backup)
	    cout << "And residual is improving:     rH < backup_sS     " << rH << " < " << backup_rH << endl;

	  cout << "Increase delta and recompute hookstep." << endl;
	  have_backup = true;
	  backup_dsigma = dsigma;
	  backup_du = du;
	  backup_tH = tH;
	  backup_GH = GH;
	  backup_rH = rH;
	  backup_dxHnorm = dxHnorm;
	  backup_delta = delta;

	  recompute_hookstep = true;
	  cout << " old delta == " << delta << endl;
	  delta = new_delta;
	  cout << " new delta == " << delta << endl;
	}
	else {
	  cout << "Stop adjusting trust region radius delta and take step because the new delta" << endl;
	  cout << "reached a local limit:  new_delta >= deltaMaxLocal " << new_delta << " >= " << deltaMaxLocal << endl;
	  cout << "Reset delta to local limit and go to next Newton iteration" << endl;
	  delta = deltaMaxLocal;
	  cout << "  delta == " << delta << endl;
	  recompute_hookstep = false;
	}
      }

      // CASE 3: MODERATE IMPROVEMENT, NO ROOM TO GROW, OR BACKUP IS BETTER
      // Remaining cases: Improvement is acceptable: either Poor, Ok, Good, or
      // {Accurate/NegaCurve and (backup is superior || Hookstep==NewtonStep || delta>=deltaMaxLocal)}.
      // In all these cases take the current step or backup if better.
      // Adjust delta according to accuracy of quadratic prediction of residual (Poor, Ok, etc).
      else {
	cout << "Improvement is " << improvement <<  " (some form of acceptable)." << endl;
	cout << "Stop adjusting trust region and take a step because" << endl;
	cout << "Improvement is merely Poor, Ok, or Good : " << (improvement == Poor || improvement == Ok || improvement == Good) << endl;
	cout << "Newton step is within trust region      : " << hookstep_equals_newtonstep << endl;
	cout << "Backup step is better                   : " << (have_backup && backup_rH < rH) << endl;
	cout << "Delta has reached local limit           : " << (delta >= deltaMax) << endl;

	recompute_hookstep = false;

	// Backup step is better. Take it instead of current step.
	if (have_backup && backup_rH < rH) {
	  cout << "Take backup step and set delta to backup value." << endl;
	  dsigma = backup_dsigma;
	  du = backup_du;
	  tH = backup_tH;
	  GH = backup_GH;
	  rH = backup_rH;

	  cout << "  old delta == " << delta << endl;
	  dxHnorm = backup_dxHnorm;
	  delta = backup_delta;
	  cout << "  new delta == " << delta << endl;
	}

	// Current step is best available. Take it. Adjust delta according to Poor, Ok, etc.
	// Will start new monotonic trust-region search in Newton-hookstep, so be more
	// flexible about adjusting delta and use quadratic model.
	else {
	  if (have_backup) {
	    cout << "Take current step and keep current delta, since it's produced best result." << endl;
	    cout << "delta == " << delta << endl;
	    // Keep current delta because current step was arrived at through a sequence of
	    // adjustments in delta, and this value yielded best results.
	  }
	  else if (improvement == Poor) {
	    cout << "Take current step and reduce delta, because improvement is poor." << endl;
	    lambda = adjustLambda(lambda, lambdaMin, 1.0);
	    delta  = adjustDelta(delta, lambda, deltaMin, deltaMax);
	  }
	  else if (improvement == Ok || hookstep_equals_newtonstep || (delta >= deltaMax)) {
	    cout << "Take current step and leave delta unchanged, for the following reasons:" << endl;
	    cout << "improvement            : " << improvement << endl;
	    cout << "|newtonstep| <= delta  : " << (hookstep_equals_newtonstep ? "true" : "false") << endl;
	    cout << "delta >= deltaMax      : " << (delta >= deltaMax    ? "true" : "false") << endl;
	    //cout << "delta has decreased    : " << (delta_has_decreased ? "true" : "false") << endl;
	    cout << "delta == " << delta << endl;
	  }
	  else {  // improvement == Good, Accurate, or NegaCurve and no restriction on increasing apply
	    cout << "Take step and increase delta, if there's room." << endl;
	    lambda = adjustLambda(lambda, 1.0, lambdaMax);
	    delta  = adjustDelta(delta, lambda, deltaMin, deltaMax);
	  }
	}
      }
      //cout << "Recompute hookstep ? " << (recompute_hookstep ? "yes" : "no") << endl;

      if (recompute_hookstep)
	continue;
      else
	break;
    }

    cout << "Taking best step and continuing Newton iteration." <<endl;
    u += du;
    T = tH;
    sigma *= dsigma;
    dunorm = L2Norm(du);
    unorm  = L2Norm(u);

    Gx = GH;

    cout << "L2Norm(G(u)) == " << L2Norm(Gx) << endl;
    cout << "rx          == " << rx << endl;
    cout << "L2Norm(dxN)   == " << L2Norm(dxN) << endl;
    cout << "L2Norm(dxH)   == " << L2Norm(dxH) << endl;
    cout << "L2Norm(du)   == " << dunorm << endl;
    //cout << "       dT    == " << dT << endl;
    cout << "L2IP(du,edudx)/L2Norm(du) == " << L2IP(du,edudx)/dunorm << endl;
    cout << "L2IP(du,edudz)/L2Norm(du) == " << L2IP(du,edudz)/dunorm << endl;
    //cout << "L2IP(du,edudt)/L2Norm(du) == " << L2IP(du,edudt)/dunorm << endl;
    cout << "align(dxN, prev_dxN) == " << align(dxN, prev_dxN) << endl;
    cout << "align(dxH, prev_dxH) == " << align(dxH, prev_dxH) << endl;
    cout << "align(dxH, dxN)      == " << align(dxH, dxN) << endl;

    prev_dxH = dxH;
    prev_dxN = dxN;
  }
  //fftw_cleanup();
}

// f^T(u) = time-T DNS integration of u
void f(const FlowField& u, FlowField& f_u, Real& T, PoincareCondition* h,
       Real Reynolds, DNSFlags& flags, TimeStep& dt, int& fcount, Real& CFL,
       Real Tmin, Real Tmax, ostream* os) {
  ++fcount;

  if (!isfinite(L2Norm(u))) {
    if (os) *os << "error in f: u is not finite. exiting." << endl;
    exit(1);
  }

  const Real nu = 1.0/Reynolds;
  FlowField p(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b());
  f_u = u;
  //dt.adjust_for_T(T, false);

  // Adjust dt for CFL if necessary
  DNSPoincare dns(f_u, efund, sfund, h, nu, dt, flags, 0);
  /********************
  if (dt.variable()) {
    dns.advance(f_u, p, 1);
    dt.adjust(dns.CFL());
    dns.reset_dt(dt);
    f_u = u;
  }
  *********************/

  //if (os) *os << "\ndt == " << dt << endl;
  //if (os) *os << ", CFL == " << dns.CFL() << endl;
  //if (os) *os << "t == " << flush;

  //  t == current time in integration
  //  T == total integration time
  // dT == CFL-check interval
  // dt == DNS time-step
  //  N == T/dT,  n == dT/dt;
  //  T == N dT, dT == n dt
  //  t == s dT (s is loop index)

  if (os) *os << "fc : " << flush;

  int s=0;
  for (Real t=0; t<=Tmax; t += dt.dT(), ++s) {

    bool crossed = dns.advanceToSection(f_u, p, dt.n(), crosssign, Tmin);
    if (crossed && t > Tmin) {
      f_u = dns.ucrossing();
      T = dns.tcrossing();
      return;
    }

    if (os) {
      CFL = dns.CFL();
      if (s % 10 == 0)   *os << t;
      else if (s % 2 == 0) *os << '.';
      if (CFL > dt.CFLmax()) *os << '>';
      else if (CFL < dt.CFLmin()) *os << '<';
      *os << flush;
    }

    //if (dt.adjust(CFL, false))
    //dns.reset_dt(dt);

  }

  cerr << "Error in fc(u, T, f_u, Reynolds, flags, dt, fcount, CFL, os) :\n";
  cerr << "the integration did not reach the Poincare section. Exiting." << endl;
  exit(1);

  if (!isfinite(L2Norm(f_u))) {
    if (os) *os << "error in f: f(u,t) is not finite. exiting." << endl;
    exit(1);
  }
}

/***************************************************
/// f^{N dt}(u) = time-(N dt) DNS integration of u
void fc(const FlowField& u, int N, Real dt, FlowField& f_u, Real Reynolds,
        DNSFlags& flags, ostream* os) {

  if (os) *os << "f(u, N, dt, f_u, Reynolds, flags, dt) : " << flush;
  const Real nu = 1.0/Reynolds;
  FlowField p;
  f_u = u;
  //DNSPoincare dns(u, ustar, estar, efund, sfund, nu, dt, flags, 0);
  DNS dns(f_u, nu, dt, flags);
  dns.advance(f_u, p, N);

  //Real ax = cx/u.Lx() * N*dt;
  //FieldSymmetry tau(1,1,1,ax,0.0);
  //f_u *= tau;
  return;
}
***************************************/


// G(x) = G(u,sigma) = (sigma f^T(u) - u) for orbits
void G(const FlowField& u, const FieldSymmetry& sigma, FlowField& Gx, Real& T,
       PoincareCondition* h, Real Reynolds, DNSFlags& flags, TimeStep& dt,
	int& fcount, Real& CFL, Real Tmin, Real Tmax, ostream* os) {
   f(u, Gx, T, h, Reynolds, flags, dt, fcount, CFL, Tmin, Tmax, os);
   Gx *= sigma;
   Gx -= u;
}

void DG(const FlowField& u, const FlowField& du, const FieldSymmetry& sigma,
	const FieldSymmetry& dsigmaArg, const FlowField& Gx, FlowField& DG_dx,
	PoincareCondition* h, Real Reynolds, DNSFlags& flags, TimeStep& dt,
	Real epsDu, bool centdiff, int& fcount, Real& CFL, Real Tmin, Real Tmax, ostream* os) {

  FlowField u_du;
  FieldSymmetry dsigma;
  FieldSymmetry sigma_dsigma;
  Real T;

  Real step_magn = sqrt(L2Norm2(du) + square(dsigma.ax()) + square(dsigma.az()));
  Real eps = step_magn < epsDu ? 1 : epsDu/step_magn;

  if (centdiff) {
    Real eps2 = 0.5*eps;
    u_du  = du;
    u_du *= eps2;
    u_du += u;
    dsigma = dsigmaArg;
    dsigma *= eps2;
    sigma_dsigma = dsigma*sigma;

    G(u_du, sigma_dsigma, DG_dx, T, h, Reynolds, flags, dt, fcount, CFL, Tmin, Tmax, os);
    u_du  = du;
    u_du *= -eps2;
    u_du += u;
    dsigma = dsigmaArg;
    dsigma *= -eps2;
    sigma_dsigma = dsigma*sigma;
    FlowField tmp;
    G(u_du, sigma_dsigma, tmp, T, h, Reynolds, flags, dt, fcount, CFL, Tmin, Tmax, os);

    DG_dx -= tmp;
    DG_dx *= 1.0/eps;
  }
  else {
    u_du  = du;
    u_du *= eps;
    u_du += u;

    dsigma = dsigmaArg;
    dsigma *= eps;
    sigma_dsigma = dsigma*sigma;

    G(u_du, sigma_dsigma, DG_dx, T, h, Reynolds, flags, dt, fcount, CFL, Tmin, Tmax, os);
    DG_dx -= Gx;
    DG_dx *= 1.0/eps;
  }
  //if (Tnormalize)
  //DG_dx *= 1.0/T;

}


/* findorbit.cpp, channelflow-1.3, www.channelflow.org
 *
 * Copyright (C) 2001-2009  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu
 * Center for Nonlinear Sciences, School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
