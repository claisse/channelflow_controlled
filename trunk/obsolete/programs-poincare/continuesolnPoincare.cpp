#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/CColVector.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/EIG.h>
#include <octave/dbleSVD.h>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/utilfuncs.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/periodicfunc.h"
#include "octutils.h"
#include "dnspoincare.h"

// John Gibson, Wed Oct 10 16:10:46 EDT 2007

using namespace std;
using namespace channelflow;

typedef FieldSymmetry Symm;
template <class T> void push(const T& t, array<T>& arrayt);

Real meandissipation(const FlowField& u, Real T, const FieldSymmetry& sigma, Real Reynolds,
		     Real& err, const DNSFlags& dnsflags, const TimeStep& dt, SolutionType solntype);

Real observable(FlowField& u);

// Replace u with sigma f^t(u) as it crosses the Poincare section I-D == 0 for t near T
void temporalRegularize(FlowField& u, Real T, const FieldSymmetry& sigma, Real Reynolds,
			const DNSFlags& dnsflags, const TimeStep& dt);

int main(int argc, char* argv[]) {


  string purpose("(ALPHA VERSION!) continue invariant solution of plane Couette flow in Reynolds number");
  ArgList args(argc, argv, purpose);

  DNSFlags dnsflags;
  GMRESHookstepFlags srchflags;

  const bool restart    = args.getflag("-r",    "--restart",       "start from three previously computed solutions");
  const bool eqb        = args.getflag("-eqb",  "--equilibrium",   "search for equilibrium or relative equilibrium (trav wave)");
  const bool orb        = args.getflag("-orb",  "--periodicorbit", "search for periodic orbit or relative periodic orbit");
  srchflags.xrelative   = args.getflag("-xrel",  "--xrelative",    "search over x phase shift for relative orbit or eqb");
  srchflags.zrelative   = args.getflag("-zrel",  "--zrelative",    "search over z phase shift for relative orbit or eqb");

  const bool Recontinue = args.getflag("-contRe",  "--continueRe", "continue solution in Reynolds number");
  const bool Lxcontinue = args.getflag("-contLx",  "--continueLx", "continue solution in streamwise width Lx");
  const bool Lzcontinue = args.getflag("-contLz",  "--continueLz", "continue solution in spanwise width Lz");
  const bool aspectcont = args.getflag("-contAsp",    "--continueAspect",   "continue solution in aspect ratio Lx/Lz");
  const bool diagcont   = args.getflag("-contDiag",   "--continueDiagonal", "continue solution along constant aspect ratio Lx/Lz");
  const bool Pcontinue  = args.getflag("-contdPdx",  "--continuedPdx", "continue solution in imposed pressure gradient");

  const bool upwards    = args.getflag("-up",  "--upwards",        "for non-restart searches, search in dir of increasing free parameter");

  const Real T0         = args.getreal("-T",  "--maptime",   15.0,  "initial guess for orbit period or time of eqb/reqb map f^T(u)");
  const Real Re0        = args.getreal("-R",  "--Reynolds", 400.0,  "Reynolds number");
  const Real dPdx0      = args.getreal("-dPdx", "--dPdx",     0.0,  "imposed mean pressure gradient");
  const  Real s0        = args.getreal("-s0", "--s0" ,        0.0,  "start value for arclength (arbitrary)");
  /****/ Real ds        = args.getreal("-ds", "--ds" ,        0.001, "initial arclength increment for quadratic extrapolation");
  //const Real sfinal     = args.getreal("-sf",  "--sfinal",  1.0,  "final arclength (in normalized D,Re space)");

  const Real dsmin      = args.getreal("-dsmin", "--dsmin",   1e-04, "minimum arclength increment (in normalized parameter space)");
  const Real dsmax      = args.getreal("-dsmax", "--dsmax",   0.02,  "maximum arclength increment (in normalized parameter space)");
  const Real guesserrmin= args.getreal("-errmin", "--errmin", 1e-05, "minimum error for extrapolated guesses");
  const Real guesserrmax= args.getreal("-errmax", "--errmax", 1e-03, "maximum error for extrapolated guesses");

  const string sigmastr = args.getstr ("-sigma", "--sigma",      "", "file containing sigma of sigma f^T(u) - u = 0 (default == identity)");
  const string symmstr  = args.getstr ("-symms", "--symmetries", "", "file containing generators of isotropy group for symmetry-constrained search");

  dnsflags.baseflow     = PlaneCouette;
  dnsflags.timestepping = s2stepmethod(args.getstr("-ts", "--timestepping", "SBDF3", "time step algorithm: sbdf[1234]|cnab2|cnrk2|smrk2"));
  dnsflags.dealiasing   = DealiasXZ;
  dnsflags.nonlinearity = s2nonlmethod(args.getstr("-nl", "--nonlinearity", "Rotational", "nonlinearity method: rot|conv|skew|div|alt"));
  dnsflags.constraint   = PressureGradient;
  dnsflags.dPdx         = dPdx0;
  dnsflags.verbosity    = Silent;
  if (symmstr.length() > 0) {
    SymmetryList symms(symmstr);
    cout << "Restricting flow to invariant subspace generated by symmetries" << endl;
    cout << symms << endl;
    dnsflags.symmetries = symms;
  }

  const bool vardt     = args.getflag("-vdt","--variableDt", "adjust dt to keep CFLmin<=CFL<CFLmax");
  const Real dtarg     = args.getreal("-dt","--timestep",  0.03125, "(initial) integration timestep");
  const Real dtmin     = args.getreal("-dtmin", "--dtmin", 0.001, "minimum timestep");
  const Real dtmax     = args.getreal("-dtmax", "--dtmax", 0.04,  "maximum timestep");
  const Real dTCFL     = args.getreal("-dTCFL", "--dTCFL", 1.00,  "check CFL # at this interval");
  const Real CFLmin    = args.getreal("-CFLmin", "--CFLmin", 0.40, "minimum CFL number");
  const Real CFLmax    = args.getreal("-CFLmax", "--CFLmax", 0.60, "maximum CFL number");

  //const Real epsMax     = args.getreal("-emax","--epsMax",      1e-10, "minimum acceptable residual for GMRESHookstep");
  srchflags.epsSearch = args.getreal("-es",  "--epsSearch",   1e-10, "stop search if L2Norm(s f^T(u) - u) < epsSearch");
  srchflags.epsKrylov = args.getreal("-ek",  "--epsKrylov",   1e-13, "min. condition # of Krylov vectors");
  srchflags.epsDu     = args.getreal("-edu", "--epsDuLinear", 1e-5,  "relative size of du to u in linearization");
  srchflags.epsDt     = args.getreal("-edt", "--epsDtLinear", 1e-7,  "size of dT in linearization of f^T about T");
  srchflags.epsGMRES  = args.getreal("-eg",  "--epsGMRES",    1e-3,  "stop GMRES iteration when Ax=b residual is < this");
  srchflags.epsGMRESf = args.getreal("-egf",  "--epsGMRESfinal",  0.05,  "accept final GMRES iterate if residual is < this");
  srchflags.centdiff  = args.getflag("-cd", "--centerdiff", "centered differencing to estimate differentials");

  srchflags.Nnewton   = args.getint( "-Nn", "--Nnewton",   10,    "max number of Newton steps ");
  srchflags.Ngmres    = args.getint( "-Ng", "--Ngmres",    60,    "max number of GMRES iterations per restart");
  srchflags.Nhook     = args.getint( "-Nh", "--Nhook",     20,    "max number of hookstep iterations per Newton");

  srchflags.delta     = args.getreal("-d",    "--delta",      0.01,  "initial radius of trust region");
  srchflags.deltaMin  = args.getreal("-dmin", "--deltaMin",   0.009, "stop if radius of trust region gets this small");
  srchflags.deltaMax  = args.getreal("-dmax", "--deltaMax",   0.1,   "maximum radius of trust region");
  srchflags.deltaFuzz = args.getreal("-df",   "--deltaFuzz",  0.01,  "accept steps within (1+/-deltaFuzz)*delta");
  srchflags.lambdaMin = args.getreal("-lmin",   "--lambdaMin", 0.2,  "minimum delta shrink rate");
  srchflags.lambdaMax = args.getreal("-lmax",   "--lambdaMax", 1.5,  "maximum delta expansion rate");
  srchflags.lambdaRequiredReduction = 0.5; // when reducing delta, reduce by at least this factor.
  srchflags.improvReq = args.getreal("-irq",  "--improveReq", 1e-3,  "reduce delta and recompute hookstep if improvement "
				      " is worse than this fraction of what we'd expect from gradient");
  srchflags.improvOk  = args.getreal("-iok",  "--improveOk",   0.10, "accept step and keep same delta if improvement "
				      "is better than this fraction of quadratic model");
  srchflags.improvGood= args.getreal("-igd",  "--improveGood", 0.75, "accept step and increase delta if improvement "
				      "is better than this fraction of quadratic model");
  srchflags.improvAcc = args.getreal("-iac",  "--improveAcc",  0.10, "recompute hookstep with larger trust region if "
				      "improvement is within this fraction quadratic prediction.");

  srchflags.TscaleRel = args.getreal("-Tsc", "--Tscale",     20,    "scale time in hookstep: |T| = Ts*|u|/L2Norm(du/dt)");
  srchflags.dudtortho = args.getbool("-tc",  "--dudtConstr", true,  "require orthogonality of step to dudt");
  srchflags.orthoscale= args.getreal("-os",  "--orthoScale", 1,     "rescale orthogonality constraint");
  srchflags.dudxortho = args.getbool("-xc", "--dudxConstraint", true,"require orthogonality of step to dudx and dudz");
  srchflags.Rscale    = args.getreal("-rs", "--relativeScale", 1, "scale relative-search variables by this factor");
  srchflags.saveall   = args.getflag("-sa", "--saveall", "save each newton step");

  const string outdir   = args.getpath("-o",  "--outdir", "./", "output directory");
  const string logfile  = args.getstr ("-log","--logfile", "findsoln.log",  "output log (filename or \"stdout\")");

  string uname;
  string solndir[3];
  //Real ds0 = 0.01;

  if (!restart) {
    //ds0 = args.getreal("-ds0", "--ds0" ,  0.01, "arclength increment for perturbations of initial solution");
    //up  = args.getflag("-up", "--upwards",      "continue upwards in continuation param (Re, Lx, or Lz)");
    uname = args.getstr(1, "<flowfield>",       "initial guess for Newton search");
  }
  else {
    solndir[2]  = args.getpath(3, "<solndir0>", "directory containing previous solution data");
    solndir[1]  = args.getpath(2, "<solndir1>", "ditto");
    solndir[0]  = args.getpath(1, "<solndir2>", "ditto");
  }
  args.check();

  if (!(eqb^orb)) {
    cerr << "Please choose either -eqb or -orb option to search for (relative) equilibrium or (relative) periodic orbit" << endl;
    exit(1);
  }

  if (int(Recontinue) + int(Lxcontinue) + int(Lzcontinue) + int(aspectcont) + int(diagcont) + int(Pcontinue) != 1) {
    cerr << "Please choose one of -contRe, -contLx, -contLz, -contAsp, -contDiag, -contdPdx options for continuation in Re, Lx, Lz, Lx/Lz, along const Lx/Lz, or in dPdx" << endl;
    exit(1);
  }
  args.save("./");
  srchflags.solntype = orb ? PeriodicOrbit : Equilibrium;
  bool relative = srchflags.xrelative || srchflags.zrelative;
  bool Tnormalize = eqb;

  Symm sigma0;
  if (sigmastr.length() != 0)
    sigma0 = Symm(sigmastr);

  int W = 24;
  cout << setprecision(17);
  cout << "Working directory == " << pwd() << endl;
  cout << "Command-line args == ";
  for (int i=0; i<argc; ++i) cout << argv[i] << ' '; cout << endl;

  cout << "Re    == " << Re0 << endl;
  cout << "sigma == " << sigma0 << endl;
  cout << "T     == " << T0 << endl;
  cout << "DNSFlags == " << dnsflags << endl << endl;

  array<FlowField> u(3); // solutions
  array<Real>      T(3); // period
  array<Real>      R(3); // Reynolds
  array<Real>      P(3); // pressure gradient, shorthand for dPdx
  array<Real>      D(3); // dissipation (avg for orbits)
  array<Real>     mu(3); // continuation parameter
  array<Real>    obs(3); // observable
  array<Real>    res(3); // L2Dist(u, sigma f^t(u)) for u returned by GMREShookstep
  array<Real>    err(3); // L2Dist(u, sigma f^t(u)) for u after temporal regularization
  array<Real>      s(3); // (mu,obs) arclengths
  array<Symm>  sigma(3); // symmetry
  Real CFL = 0.0;
  int fcount = 0;

  ofstream dos((outdir + "ReD.asc").c_str());
  const TimeStep dt(dtarg, dtmin, dtmax, dTCFL, CFLmin, CFLmax, vardt);
  // TimeStep dt(dt0);

  string muname = "mu";
  if (Recontinue)      muname = "mu=Re";
  else if (Lxcontinue) muname = "mu=Lx";
  else if (Lzcontinue) muname = "mu=Lz";
  else if (aspectcont) muname = "mu=Lx/Lz";
  else if (diagcont)   muname = "mu=sqrt(Lx^2+Lz^2)";
  else if (Pcontinue)  muname = "mu=dPdx";

  // Compute initial data points for extrapolation from perturbations of given solution
  if (!restart) {
    u[2] = FlowField(uname);
    u[1] = u[2];
    u[0] = u[2];

    R[2] = Re0;
    R[1] = Re0;
    R[0] = Re0;

    T[2] = T0;
    T[1] = T0;
    T[0] = T0;

    P[2] = dPdx0;
    P[1] = dPdx0;
    P[0] = dPdx0;

    const int musign = upwards ? 1 : -1; // Go up or down in continuation parameter?

    // Revise above as necessary
    if (Recontinue) {
      mu[2] = R[2] = Re0*(1-musign*ds);
      mu[1] = R[1] = Re0;
      mu[0] = R[0] = Re0*(1+musign*ds);
    }
    else if (Lxcontinue) {
      mu[2] = u[2].Lx() * (1-musign*ds);
      mu[1] = u[1].Lx();
      mu[0] = u[0].Lx() * (1+musign*ds);

      u[2].rescale(mu[2], u[2].Lz());
      u[0].rescale(mu[0], u[0].Lz());
    }
    else if (Lzcontinue) {
      mu[2] = u[2].Lz() * (1-musign*ds);
      mu[1] = u[1].Lz();
      mu[0] = u[0].Lz() * (1+musign*ds);

      u[2].rescale(u[2].Lx(), mu[2]);
      u[0].rescale(u[0].Lx(), mu[0]);
    }
    else if (aspectcont) {
      Real aspect   = u[1].Lx()/u[1].Lz();
      //Real diagonal = pythag(u[1].Lx()/u[1].Lz();
      mu[2] = aspect * (1-musign*ds);
      mu[1] = aspect;
      mu[0] = aspect * (1+musign*ds);

      u[2].rescale(sqrt(mu[2]/mu[1])*u[1].Lx(), sqrt(mu[1]/mu[2])*u[2].Lz());
      u[0].rescale(sqrt(mu[0]/mu[1])*u[0].Lx(), sqrt(mu[1]/mu[0])*u[0].Lz());
      cout << "aspect ratio continuation : " << endl;
      for (int n=2; n>=0; --n)
	cout << "n == " << n << ", mu == " << mu[n] << ", aspect == " << u[n].Lx()/u[n].Lz() << ", diagonal == " << pythag(u[n].Lx(), u[n].Lz()) << endl;

    }
    else if (diagcont) {
      Real diagonal = sqrt(square(u[1].Lx()) + square(u[1].Lz()));

      mu[2] = diagonal * (1-musign*ds);
      mu[1] = diagonal;
      mu[0] = diagonal * (1+musign*ds);

      u[2].rescale(sqrt(mu[2]/mu[1]) * u[2].Lx(), sqrt(mu[2]/mu[1]) * u[2].Lz());
      u[0].rescale(sqrt(mu[0]/mu[1]) * u[0].Lx(), sqrt(mu[0]/mu[1]) * u[0].Lz());
      cout << "diagonal box continuation : " << endl;
      for (int n=2; n>=0; --n)
	cout << "n == " << n << ", mu == " << mu[n] << ", aspect == " << u[n].Lx()/u[n].Lz() << ", diagonal == " << pythag(u[n].Lx(), u[n].Lz()) << endl;
    }
    else if (Pcontinue) {
      if (dPdx0 != 0.0) {
	mu[2] = P[2] = dPdx0*(1-musign*ds);
	mu[1] = P[1] = dPdx0;
	mu[0] = P[0] = dPdx0*(1+musign*ds);
      }
      else {
	mu[2] = P[2] = -musign*ds;
	mu[1] = P[1] =  0.0;
	mu[0] = P[0] =  musign*ds;
      }
    }
    sigma[2] = sigma0;
    sigma[1] = sigma0;
    sigma[0] = sigma0;

  }

  // Read in some solutions from previous computations
  else {

    for (int i=0; i<3; ++i) {
      cout << "loading data from directory " << solndir[i] << endl;
      u[i] = FlowField(solndir[i] + "ubest");
      load(T[i], solndir[i] + "Tbest");
      load(R[i], solndir[i] + "Reynolds");
      load(P[i], solndir[i] + "dPdx");
      sigma[i] = Symm(solndir[i] + "sigmabest");
    }

    if (!relative && (sigma[0] != sigma[1] || sigma[1] != sigma[2]))
       cferror("continuesoln error : initial symmetries should be equal for non-relative searchs");
     sigma0 = sigma[1];

    if (Recontinue) {
      mu[2] = R[2];
      mu[1] = R[1];
      mu[0] = R[0];
    }
    else if (Lxcontinue) {
      mu[2] = u[2].Lx();
      mu[1] = u[1].Lx();
      mu[0] = u[0].Lx();
    }
    else if (Lzcontinue) {
      mu[2] = u[2].Lz();
      mu[1] = u[1].Lz();
      mu[0] = u[0].Lz();
    }
    else if (aspectcont) {
      mu[2] = u[2].Lx()/u[2].Lz();
      mu[1] = u[1].Lx()/u[1].Lz();
      mu[0] = u[0].Lx()/u[0].Lz();
    }
    else if (diagcont) {
      mu[2] = pythag(u[2].Lx(), u[2].Lz());
      mu[1] = pythag(u[1].Lx(), u[1].Lz());
      mu[0] = pythag(u[0].Lx(), u[0].Lz());
    }
    else if (Pcontinue) {
      mu[2] = P[2];
      mu[1] = P[1];
      mu[0] = P[0];
    }
    else {
      cerr << "Error in specification of continuation parameter! The control flow needs fixing (A)" << endl;
      exit(1);
    }

    cout << "loaded the following data..." << endl;
    cout << setw(4) << "i" << setw(W) << muname << setw(W) << "T" << setw(W) << "L2Norm(u)" << setw(W) << "sigma" << endl;
    for (int i=2; i>=0; --i)
      cout << setw(4) << i << setw(W) << mu[i] << setw(W) << T[i] << setw(W) << L2Norm(u[i]) << setw(W) << sigma[i] << endl;

  }

  // Now find solutions for initial data.
  for (int i=2; i>=0; --i) {

    // Allow for more newton steps and a looser hookstep search if we start with blind parameter perturbation
    GMRESHookstepFlags srchflags0 = srchflags;
    if (!restart) {
      srchflags0.Nnewton = 20;
      srchflags0.deltaMin = 1e-07;
    }

    cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    string searchdir = outdir + "restart-" + i2s(i) + "/";
    mkdir(searchdir);
    ofstream searchlog((searchdir + "findsoln.log").c_str());
    srchflags0.logstream = &searchlog;
    srchflags0.outdir    = searchdir;
    dnsflags.dPdx        = P[i];
    cout << "G" << endl;

    string label = "initial field u[" + i2s(i) + "]";
    fixdivnoslip(u[i]);
    project(dnsflags.symmetries, u[i], label, cout);
    cout << "computing solution for " << muname << " == " << mu[i] << " in dir " << searchdir << "..." << endl;

    res[i] = GMRESHookstep(u[i], T[i], sigma[i], R[i], srchflags0, dnsflags, dt);
    if (orb)
      temporalRegularize(u[i], T[i], sigma[i], R[i], dnsflags, dt);
    D[i]   = meandissipation(u[i], T[i], sigma[i], R[i], err[i], dnsflags, dt, srchflags0.solntype);
    obs[i] = D[i];

    cout << muname << " == " << mu[i] << "   obs == " << obs[i] << "   D == " << D[i] << "   searchresidual == " << res[i] << "    " << " err after temporal regularization == " << err[i] << endl;
    save(R[i], searchdir + "Reynolds");
    save(P[i], searchdir + "dPdx");
    save(mu[i], searchdir + "mu");
    save(D[i],  searchdir + "D");
    save(P[i],  searchdir + "dPdxbest");
    sigma[i].save(searchdir + "sigmabest");

    u[i].save(searchdir + "upoinc"); // resave u after symmetrization
  }

  Real obsnorm = obs[1];
  Real munorm  = mu[1];
  if (munorm == 0.0)
    munorm = 1.0;
  s[2] = s0 - sqrt(square((obs[2]-obs[1])/obsnorm) + square((mu[2]-mu[1])/munorm));
  s[1] = s0;
  s[0] = s0 + sqrt(square((obs[0]-obs[1])/obsnorm) + square((mu[0]-mu[1])/munorm));

  //Real ds = sqrt(square((obs[0]-obs[1])/obsnorm) + square((mu[0]-mu[1])/munorm));

  Real guesserr = 0;
  dos << setw(W) << (string("% ")+muname) << setw(W) << "D" << setw(W) << "s" << setw(W) << "guesserr" << setw(W) << "searcherr" << setw(W) << "poincerr" <<  " directory" << endl;
  for (int i=2; i>=0; --i)
    dos << setw(W) << mu[i] << setw(W) << D[i] << setw(W) << s[i] << setw(W) << guesserr << setw(W) << res[i] << setw(W) << err[i] << " restart-" << i << endl;

  int Nsteps = 100;
  bool failing=false;

  for (int n=0; n<Nsteps; ++n) {

    cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    cout << "Continuing previous solutions by pseudo-arclength quadratic extrapolation in s = (mu,obs)" << endl;
    cout << setw(8) << "i" << setw(W) << "s" << setw(W) << muname << setw(W) << "T" << setw(W) << "obs" << endl;
    for (int i=2; i>=0; --i)
      cout << setw(8) << i << setw(W) << s[i] << setw(W) << mu[i] << setw(W) << T[i] << setw(W) << obs[i] << endl;

    Real snew = s[0] + ds;
    FlowField unew = quadraticInterpolate(u, s, snew);
    Real Tnew      = quadraticInterpolate(T, s, snew);
    Real Rnew      = quadraticInterpolate(R, s, snew);
    Real Pnew      = quadraticInterpolate(P, s, snew);
    Symm sigmanew  = quadraticInterpolate(sigma, s, snew);

    fixdivnoslip(unew);
    project(dnsflags.symmetries, unew, "quadratic extrapolant guess unew", cout);

    Real munew;
    if (Recontinue)        munew = Rnew;
    else if (Lxcontinue)   munew = unew.Lx();
    else if (Lzcontinue)   munew = unew.Lz();
    else if (aspectcont)   munew = unew.Lx()/unew.Lz();
    else if (diagcont)     munew = pythag(unew.Lx(),unew.Lz());
    else if (Pcontinue)   {munew = Pnew; dnsflags.dPdx = Pnew;}
    else {
      cerr << "Error in specification of continuation parameter! The control flow needs fixing (B)" << endl;
      exit(1);
    }

    cout << setw(8) << "guess" << setw(W) << snew << setw(W) << munew << setw(W) << Tnew << endl;

    cout << "calculating error of quadratic extrapolated guess" << endl;
    FlowField Guguess;
    G(unew, Tnew, sigmanew, Guguess, Rnew, dnsflags, dt, Tnormalize, fcount, CFL);
    Real guesserr = L2Norm(Guguess);
    cout << '\n' << setw(8) << "guess" << setw(W) << snew << setw(W) << munew << setw(W) << Tnew << "   guess err == " << guesserr << endl;

    // Adjust ds on model err = k O(ds^3) , with fudge factor 2 or 1/2
    const int maxdsadjustments = 4;
    int dsadjustcount=0;
    Real guesserrtarget = sqrt(guesserrmin*guesserrmax); // aim between error bounds
    if (guesserr < guesserrmin)
      while (!failing && guesserr < guesserrmin && ds < dsmax && dsadjustcount++ < maxdsadjustments) {
	cout << "guess err == " << guesserr << " < guesserrmin == " << guesserrmin << ",  increasing ds..." << endl;
	// try to hit guesserrmindouble error based on err = O(ds^3) with 10% fudge factor
	// err = k ds^3 =>  err1/err0 = (ds1/ds0)^3 => ds1 = ds0 * (err1/err0)^(1/3)
	ds *=  pow(guesserrtarget/guesserr, 0.33);
	if (ds > dsmax) {
	  ds = dsmax;
	  dsadjustcount = maxdsadjustments;
	}
	snew = s[0] + ds;
	unew = quadraticInterpolate(u, s, snew);
	Tnew = quadraticInterpolate(T, s, snew);
	Rnew = quadraticInterpolate(R, s, snew);
	Pnew = quadraticInterpolate(P, s, snew);
	sigmanew  = quadraticInterpolate(sigma, s, snew);

	fixdivnoslip(unew);
	project(dnsflags.symmetries, unew, "quadratic extrapolant guess u", cout);

	if (Recontinue)        munew = Rnew;
	else if (Lxcontinue)   munew = unew.Lx();
	else if (Lzcontinue)   munew = unew.Lz();
	else if (aspectcont)   munew = unew.Lx()/unew.Lz();
	else if (diagcont)     munew = pythag(unew.Lx(), unew.Lz());
	else if (Pcontinue)   {munew = Pnew; dnsflags.dPdx = Pnew;}
	else {
	  cerr << "Error in specification of continuation parameter! The control flow needs fixing (C)" << endl;
	  exit(1);
	}

	G(unew, Tnew, sigmanew, Guguess, Rnew, dnsflags, dt, Tnormalize, fcount, CFL);
	guesserr = L2Norm(Guguess);
	cout << '\n' << setw(8) << "guess" << setw(W) << snew << setw(W) << munew << setw(W) << Tnew << "   guess err == " << guesserr << endl;
      }
    else if (guesserr > guesserrmax)
      while (guesserr > guesserrmax && ds > dsmin && dsadjustcount++ < 4) {
	cout << "guess err == " << guesserr << " > guesserrmax == " << guesserrmax << ",  decreasing ds..." << endl;
	ds *= pow(guesserrtarget/guesserr, 0.33);
	if (ds < dsmin) {
	  ds = dsmin;
	  dsadjustcount = maxdsadjustments;
	}
	snew = s[0] + ds;
	unew = quadraticInterpolate(u, s, snew);
	Tnew = quadraticInterpolate(T, s, snew);
	Rnew = quadraticInterpolate(R, s, snew);
	Pnew = quadraticInterpolate(P, s, snew);
	sigmanew  = quadraticInterpolate(sigma, s, snew);

	if (Recontinue)        munew = Rnew;
	else if (Lxcontinue)   munew = unew.Lx();
	else if (Lzcontinue)   munew = unew.Lz();
	else if (aspectcont)   munew = unew.Lx()/unew.Lz();
	else if (diagcont)     munew = pythag(unew.Lx(), unew.Lz());
	else if (Pcontinue)   {munew = Pnew; dnsflags.dPdx = Pnew;}
	else {
	  cerr << "Error in specification of continuation parameter! The control flow needs fixing (D)" << endl;
	  exit(1);
	}

	fixdivnoslip(unew);
	project(dnsflags.symmetries, unew, "quadratic extrapolant guess unew", cout);

	G(unew, Tnew, sigmanew, Guguess, Rnew, dnsflags, dt, Tnormalize, fcount, CFL);
	guesserr = L2Norm(Guguess);
	cout << '\n' << setw(8) << "guess" << setw(W) << snew << setw(W) << munew << setw(W) << Tnew << "   guess err == " << guesserr << endl;
      }

    if (guesserr < guesserrmin)
      cout << "\nFound guess whose error is too small but not too big. Going ahead with it.\n\n";
    else if (guesserr > guesserrmax)
      cout << "\nFound guess whose error is too big but not too small. Going ahead with it.\n\n" << endl;
    else
      cout << "\nFound guess whose error meets Goldilocks principle.\n\n";

    string searchdir = outdir + "search-" + i2s(n) + "/";
    mkdir(searchdir);
    save(Rnew, searchdir + "Reynolds");
    save(Pnew, searchdir + "dPdx");
    save(snew, searchdir + "arclength");
    save(munew, searchdir + "mu");
    ofstream searchlog((searchdir + "findsoln.log").c_str());

    //dnsflags.verbosity  = Silent;
    srchflags.logstream = &searchlog;
    srchflags.outdir    = searchdir;

    cout << "Computing new solution in directory " << searchdir << "  ..." << endl;

    //dt = dt0;
    Real resnew = GMRESHookstep(unew, Tnew, sigmanew, Rnew, srchflags, dnsflags, dt);
    if (orb)
      temporalRegularize(unew, Tnew, sigmanew, Rnew, dnsflags, dt);
    Real err = 0.0;
    Real Dnew = meandissipation(unew, Tnew, sigmanew, Rnew, err, dnsflags, dt, srchflags.solntype);
    Real obsnew = Dnew;

    if (resnew < srchflags.epsSearch) {
      cout << "Found new solution." << endl;

      save(Dnew,  searchdir + "D");
      save(Pnew,  searchdir + "dPdxbest");
      save(Tnew,  searchdir + "Tbest");
      sigmanew.save(searchdir + "sigmabest");
      unew.save(searchdir + "upoinc"); // resave u after temporal regularization

      ds = sqrt(square((obsnew-obs[0])/obsnorm) + square((munew-mu[0])/munorm));
      snew = s[0]+ds;

      push(unew, u);
      push(Tnew, T);
      push(Rnew, R);
      push(Pnew, P);
      push(Dnew, D);
      push(munew, mu);
      push(obsnew, obs);
      push(resnew, res);
      push(snew, s);
      push(sigmanew, sigma);

      cout << setw(8) << "solution" << setw(W) << snew << setw(W) << munew << setw(W) << Tnew << "   residual == " << resnew << "    error == " << err << endl;

      dos << setw(W) << munew << setw(W) << Dnew << setw(W) << snew << setw(W) << guesserr << setw(W) << resnew << setw(W) << err << "  " << searchdir << endl;
      //if (ds < dstarget)
      //ds = lesser(1.5*ds, dstarget);
      //else if (ds > dstarg)
      //ds = Greater(0.67*ds, dstarget);
      failing = false;
    }
    else {
      cout << "Failed to find new solution. Halving ds and trying again" << endl;
      cout << setw(8) << "failure" << setw(W) << snew << setw(W) << munew << setw(W) << Tnew << "   residual == " << resnew << endl;
      ds *= 0.5;
      failing = true;
      string failuredir(outdir + "failures");
      mkdir(failuredir);
      rename(searchdir, failuredir + "/search-" + i2s(n));
    }
  }
}

template <class T> void push(const T& t, array<T>& a) {
  for (int n=a.length()-1; n>0; --n)
    a[n] = a[n-1];
  a[0] = t;
}

// Return the I-D=0 Poincare crossing that is closest to u, for u on periodic orbit.
void temporalRegularize(FlowField& u, Real T, const FieldSymmetry& sigma, Real Reynolds,
			const DNSFlags& dnsflags, const TimeStep& dt) {

  cout << "temporal regularization..." << endl;
  DragDissipation h;
  array<FlowField> efund;
  array<FieldSymmetry> sfund;
  const  Real nu = 1.0/Reynolds;
  FlowField fu(u);
  FlowField p(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b());
  DNSPoincare dns(fu, efund, sfund, &h, nu, dt, dnsflags, 0);

  //Real Tmin = 0.95*T;
  const Real Tfudge = 1.05;
  const Real Tmax = Tfudge*T;

  int s=0;
  int crosssign = 0;

  // Growable lists of Pincare crossings
  vector<FlowField> ucross_pl;
  vector<FlowField> ucross_mi;
  vector<Real> tcross_pl;
  vector<Real> tcross_mi;

  cout << "Computing all crossings of Poincare section..." << endl;
  for (Real t=0; t<=Tmax; t += dt.dT(), ++s) {

    if (s % 10 == 0)   cout << t << flush;
    else if (s % 2 == 0) cout << '.' << flush;

    bool crossed = dns.advanceToSection(fu, p, dt.n(), crosssign);

    Real CFL = dns.CFL();
    if (CFL > dt.CFLmax()) cout << '>' << flush;
    else if (CFL < dt.CFLmin()) cout << '<' << flush;

    if (crossed) {
      if (dns.signcrossing() == -1) {
	ucross_mi.push_back(dns.ucrossing());
	tcross_mi.push_back(dns.tcrossing());
      }
      else if (dns.signcrossing() == 1) {
	ucross_pl.push_back(dns.ucrossing());
	tcross_pl.push_back(dns.tcrossing());
      }
    }

  }

  if (sigma != FieldSymmetry(1,1,1,0,0)) {
    cout << "Applying symmetry sigma if it brings crossing closer to initial field" << endl;

    FlowField sfu;
    for (uint n=0; n<ucross_pl.size(); ++n) {
      sfu = sigma(ucross_pl[n]);
      if (L2Dist(sfu,u) < L2Dist(ucross_pl[n], u))
	ucross_pl[n] = sfu;
    }
    for (uint n=0; n<ucross_mi.size(); ++n) {
      sfu = sigma(ucross_mi[n]);
      if (L2Dist(sfu,u) < L2Dist(ucross_mi[n], u))
	ucross_mi[n] = sfu;
    }
  }
  cout << "\nFind crossing closest to initial field" << endl;
  FlowField ubest = u;
  ubest.setToZero();
  Real dbest = L2Dist(u, ubest);
  for (uint n=0; n<ucross_pl.size(); ++n) {
    Real d = L2Dist(u, ucross_pl[n]);
    cout << "+ " << n << " d == " << d;
    if (d < dbest) {
      ubest = ucross_pl[n];
      crosssign = 1;
      cout << " BEST";
    }
    cout << endl;
  }
  for (uint n=0; n<ucross_mi.size(); ++n) {
    Real d = L2Dist(u, ucross_mi[n]);
    cout << "- " << n << " d == " << d;
    if (d < dbest) {
      ubest = ucross_mi[n];
      crosssign = -1;
      cout << " BEST ";
    }
    cout << endl;
  }

  if (crosssign == 0) {
    cerr << "Error in temporalRegularize(u, T, sigma, Reynolds, flags, dt) :\n";
    cerr << "the integration did not reach the Poincare section within the expected orbit period," << endl;
    cerr << "0 <= t <= " << Tmax << " == " << Tfudge << " T. Exiting." << endl;
    exit(1);
  }

  cout << "Check for nearby crossings of opposite sign, which indicate tangencies" << endl;
  vector<FlowField>& anticross = (crosssign == -1) ? ucross_pl : ucross_mi;
  Real unorm = L2Norm(ubest);
  for (uint n=0; n<anticross.size(); ++n) {
    Real d = L2Dist(ubest, anticross[n]);
    if (d/unorm < 0.05) {
      cerr << "WARNING: two nearby crossings of Poincare section. Nearing tangency to section." << endl;
      cerr << "L2Dist(ubest, nearby opposite crossing) == " << d << endl;
    }
  }
  u = ubest;
  return;
}


Real meandissipation(const FlowField& uarg, Real T, const FieldSymmetry& sigma, Real Reynolds,
		     Real& err, const DNSFlags& dnsflags, const TimeStep& dtarg, SolutionType solntype) {

  cout << "computing mean dissipation..." << flush;
  FlowField u(uarg);

  ChebyCoeff U(u.Ny(),u.a(),u.b(),Spectral);
  U[1] = 1;

  if (solntype == Equilibrium) {
    u += U;
    cout << "done" << endl;
    return dissipation(u);
  }
  else {

    //DNSFlags dnsflags = dnsflagsarg;
    //dnsflags.verbosity = PrintTicks;

    int N = iround(T);
    const Real dT = T/N;
    TimeStep dt(dtarg.dt(), dtarg.dtmin(), dtarg.dtmax(), dT, dtarg.CFLmin(), dtarg.CFLmax(), dtarg.variable());
    u.optimizeFFTW(FFTW_PATIENT);

    const int Nx = u.Nx();
    const int Ny = u.Ny();
    const int Nz = u.Nz();
    const Real Lx = u.Lx();
    const Real Lz = u.Lz();
    const Real a = u.a();
    const Real b = u.b();

    FlowField q(Nx,Ny,Nz,1,Lx,Lz,a,b);

    DNS dns(u, 1/Reynolds, dt, dnsflags, 0.0);

    int count = 0;
    Real D = 0.0;

    for (Real t=0; t<=T-dT/2; t += dT) {

      u += U;
      D += dissipation(u);
      u -= U;
      ++count;

      dns.advance(u, q, dt.n());

      if (dt.adjust(dns.CFL()))
	dns.reset_dt(dt);
    }
    u *= sigma;
    err = L2Dist(u, uarg);

    cout << "done" << endl;
    return D/count;
  }
}

Real observable(FlowField& u) {
  ChebyCoeff U(u.Ny(),u.a(),u.b(),Spectral);
  U[1] = 1;
  u += U;
  Real obs = dissipation(u);
  u -= U;
  return obs;
}


/* continuesoln.cpp, channelflow-1.3, www.channelflow.org
 *
 * Copyright (C) 2001-2009  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu
 * Center for Nonlinear Sciences, School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
