#include "dnspoincare.h"

using namespace std;
using namespace channelflow;

PoincareCondition::PoincareCondition() {}

PlaneIntersection::PlaneIntersection() {}
PlaneIntersection::PlaneIntersection(const FlowField& ustar, const FlowField& estar)
  :
  estar_(estar),
  cstar_(L2IP(ustar, estar))
{}

Real PlaneIntersection::operator()(const FlowField& u) {
  return L2IP(u, estar_) - cstar_;
}

DragDissipation::DragDissipation() {}

Real DragDissipation::operator()(const FlowField& u) {
  return forcing(u) - dissipation(u);
};

DNSPoincare::DNSPoincare()
  :
  DNS(),
  e_(),
  sigma_(),
  h_(0),
  ucrossing_(),
  pcrossing_(),
  tcrossing_(0),
  scrossing_(0),
  poincareCondition_(0.0),
  t0_(0)
{}


DNSPoincare::
DNSPoincare(const FlowField& u,  const array<FlowField>& e, const array<FieldSymmetry>& sigma,
	    PoincareCondition* h, Real nu, Real dt, const DNSFlags& flags, Real t0)
  :
  DNS(u, nu, dt, flags, t0),
  e_(e),
  sigma_(sigma),
  h_(h),
  ucrossing_(),
  pcrossing_(),
  tcrossing_(0),
  scrossing_(0),
  poincareCondition_(0.0),
  t0_(t0)
{
  // check that sigma[n] e[n] = -e[n]
  //cout << "Checking symmetries and basis sets for fundamental domain " << endl;
  //FlowField tmp;
  //cout << "n \t L2Norm(e[n] + s[n] e[n])/L2Norm(e[n])" << endl;
  //for (int n=0; n<e.length(); ++n) {
  //tmp = e_[n];
  //tmp += sigma_[n](e_[n]);
  //cout << n << '\t' << L2Norm(tmp)/L2Norm(e_[n]) << endl;
  //}
}

const FlowField& DNSPoincare::ucrossing() const {return ucrossing_;}
const FlowField& DNSPoincare::pcrossing() const {return pcrossing_;}
Real DNSPoincare::tcrossing() const {return tcrossing_;}
int  DNSPoincare::signcrossing() const {return scrossing_;}
Real DNSPoincare::poincareCondition() const {return poincareCondition_;}

//Real DNSPoincare::f(const FlowField& u) const {
//  return L2IP(u, estar_) - cstar_;
//}

bool DNSPoincare::advanceToSection(FlowField& u, FlowField& p, int nSteps, int crosssign, Real Tmin, Real epsilon) {


  FlowField uprev(u);
  FlowField pprev(p);

  // Take nSteps of length dt, advancing t -> t + nSteps dt
  advance(u, p, nSteps);

  // Check for u(t) cross of fundamental domain boundary, map back in
  // Map uprev, pprev back, too, so that
  FieldSymmetry s, identity; // both identity at this point
  for (int n=0; n<e_.length(); ++n) {
    if (L2IP(u, e_[n]) <0) {
      //cout << "Crossed " << n << "th boundary of fundamental domain" << endl;
      s  *= sigma_[n];
    }
  }
  if (s != identity) {
    //cout << "Mapping fields back into fund. domain w s = " << s << endl;
    u *= s;
    p *= s;
    uprev *= s;
    pprev *= s;
    (*this) *= s; // maps all FlowField members of DNS
  }

  Real tcoarse = DNS::time();
  if (tcoarse-t0_ > Tmin) {
    //Real cstar = L2IP(ustar_, estar_);
    Real hprev = (*h_)(uprev);
    Real hcurr = (*h_)(u);
    poincareCondition_ = hcurr;

    //cout << tcoarse << '\t' << c << endl;

    bool dhdt_pos_cross  =  (hprev<0 && 0<=hcurr) ? true : false;
    bool dhdt_neg_cross  =  (hprev>0 && 0>=hcurr) ? true : false;

    // If we cross the Poincare section in required direction, back up and
    // reintegrate, checking condition every integration time step dt.
    if ((crosssign > 0  && dhdt_pos_cross) ||
	(crosssign < 0  && dhdt_neg_cross) ||
	(crosssign == 0 && (dhdt_pos_cross || dhdt_neg_cross))) {

      //cout << "u(t) crossed Poincare section..." << endl;
      cout << (dhdt_pos_cross ? '+' : '-') << flush;
      scrossing_ = dhdt_pos_cross ? +1 : -1;

      // Allocate arrays to store time, velocity, and Poincare conditions
      // at three consecutive time steps for quadratic interpolation. E.g.
      // v[n],q[n],f[n] are u,p,f at three successive fine-scale timesteps
      array<Real> s(3);      // s[n] = tcoarse - dT + n dt    time-like variable
      array<Real> h(3);      // h[n] = h(s[n])                space-like variable
      array<FlowField> v(3); // v[n] = u(s[n])                velocity field
      array<FlowField> q(3); // q[n] = p(s[n])                pressure field

      Real dt = DNS::dt();
      Real dT = dt*nSteps;

      s[0] = tcoarse - dT;
      s[1] = 0.0;
      s[2] = 0.0;
      //s[3] = 0.0;
      h[0] = hprev;
      h[1] = 0.0;
      h[2] = 0.0;
      //h[3] = 0.0;
      v[0] = uprev;
      q[0] = pprev;

      //cout << "h:";
      //for (int i=0; i<3; ++i)
      //cout << h[i] << '\t';
      //cout << endl;
      //cout << "s:";
      //for (int i=0; i<3; ++i)
      //cout << s[i] << '\t';
      //cout << endl;

      DNSFlags fineflags = DNS::flags();
      fineflags.verbosity=Silent;
      //cout << "constructing DNS for fine-time integration..." << endl;
      DNS dns(v[0], DNS::nu(), DNS::dt(), fineflags, tcoarse-dT);
      //cout << "finished contructing DNS..." << endl;

      int count=1; // need four data points for cubic interpolation

      // Now take a number of small-scale (dt) time-steps until we hit the section
      // Hitting the section will be defined by 0.0 lying between d[0] and d[2]
      for (Real tfine=tcoarse-dT; tfine <= tcoarse+dt; tfine += dt) {

	//cout << "time  shifts..." << endl;
	// Time-shift velocity and Poincare condition arrays
	// v[2] <- v[1] <- v[0], same w d in prep for advancing v[0],q[0] under DNS
	for (int n=2; n>0; --n) {
	  s[n] = s[n-1];
	  v[n] = v[n-1];
	  q[n] = q[n-1];
	  h[n] = h[n-1];
	}

	//cout << "time step..." << endl;
	dns.advance(v[0], q[0], 1); // take one step of length dt
	h[0] = (*h_)(v[0]);
	s[0] = tfine + dt;
	cout << ':' << flush;

	//cout << "crossing check..." << endl;
	// Check for Poincare section crossing in midpoint of h[0],h[1],h[2],h[3]
	if (++count >= 3 && ((h[2]<0 && 0<=h[0]) || (h[2]>0 && 0>=h[0]))) {

	  // Newton search for zero of h(s) == h(v(s)) == 0 where v(s) is
	  // quadratic interpolant of v. Interpolating s as a function of h
	  // at h==0 gives a good initial guess for s

	  // Newton iteration variables
	  Real sN = polynomialInterpolate(s, h, 0);
	  Real eps = 1e-9; // used for approximation dh/ds = (h(s + eps s) - h(s))/(eps s)
	  Real hsN, hsN_ds;
	  FlowField vN;
	  //cout << "Newtown iteration on interpolated Poincare crossing" << endl;

	  int Newtsteps = 6;
	  for (int n=0; n<Newtsteps; ++n) {

	    vN = polynomialInterpolate(v, s, sN);
	    vN.makeSpectral();
	    hsN = (*h_)(vN);
	    //cout << n << flush;

	    if (abs(hsN) < epsilon/2 || n==Newtsteps-1) {
	      if (abs(hsN) < epsilon/2)
		cout << "|" << flush;  // signal an accurate computation of a Poincare crossing
	      else
		cout << "~|" << flush; // signal an inaccurate computation of a Poincare crossing

	      tcrossing_ = sN;
	      ucrossing_ = vN;
	      pcrossing_ = polynomialInterpolate(q, s, sN);
	      pcrossing_.makeSpectral();
	      break;
	    }
	    else {
	      vN = polynomialInterpolate(v, s, sN + eps*sN);
	      vN.makeSpectral();
	      hsN_ds = (*h_)(vN);
	      Real dhds = (hsN_ds - hsN)/(eps*sN);
	      Real ds = -hsN/dhds;
	      sN += ds;

	      //cout << "Not good enough. Taking Newton step. " << endl;
	      //cout << "dhds == " << dhds << endl;
	      //cout << "ds   == " << ds << endl;
	      //cout << "s+ds == " << sN << endl;

	    }
	  }

	  // output time of crossing
	  poincareCondition_ = hsN;
	  //Real cross = (*h_)(ucrossing_);
	  //cout << "Estimated poincare crossing: " << endl;
	  //cout << "  h(u) == " << cross << endl;
	  //cout << "  time == " << tcrossing_ << endl;

	  return true;
	}
      }
      cout << "Strange.... the large-scale steps crossed the Poincare section,\n";
      cout << "but when we went back and looked with finer-scale steps, there\n";
      cout << "was no crossing. Exiting." << endl;
      exit(1);
    }
  }
  return false; // didn't cross Poincare section
}
