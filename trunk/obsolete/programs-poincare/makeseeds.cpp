#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

#include "dnspoincare.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("construct initial conditions of form u_n = EQ2 + eps (cos 2npi/N ef2 - sin 2npi/N ef3)");

  ArgList args(argc, argv, purpose);

  const int  N   = args.getint("-N", "--N", 32, "number of initial conditions");
  const Real eps = args.getreal("-e", "--eps", 1e-03, "number of initial conditions");

  FlowField EQ2("EQ2");
  FlowField ef2("ef2");
  FlowField ef3("ef3");


  for (int n=0; n<N; ++n) {
    cout << n << ' ' << flush;
    Real theta = 2*n*(pi/N);
    FlowField u(EQ2);
    FlowField tmp;

    tmp = ef2;
    tmp *= eps*cos(theta);
    u += tmp;

    tmp = ef3;
    tmp *= -eps*sin(theta);
    u += tmp;

    string name = "u_";
    if (n<=9)
      name += "0";
    name += i2s(n);
    name += "pi";
    name += i2s(N);

    u.save(name);
  }
  cout << endl;
}
