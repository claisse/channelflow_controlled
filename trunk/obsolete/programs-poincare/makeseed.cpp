#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

#include "dnspoincare.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("construct initial conditions of form u_n = EQ2 + eps (cos theta ef2 - sin theta ef3)\n"
		 "where theta = 2pi(m/n + 23/64) and m and n are specified by a hex number with character\n"
		 "digits {a,b,c,d,e,f,g,h}, m = (hex value of string) and n = 8^(length of string)\n"
		 "e.g. string == cga => m/n == 176/512 == 2*8^2 + 6*8^1 + 0*8^0\n");

  ArgList args(argc, argv, purpose);

  const Real  eps       = args.getreal("-e", "--eps", 1e-03, "length of perturbation");
  const string hexcode  = args.getstr(1, "", "a string of hex digits abcdefgh");

  args.check();
  args.save("./");

  FlowField EQ2("EQ2");
  FlowField ef2("ef2");
  FlowField ef3("ef3");


  const uint N = hexcode.length();  // # hex digits
  int m = 0;
  for (uint n=0; n<N; ++n)
    m += (int(hexcode[n]) - int('a'))*pow(8,N-n-1);

  const int n = pow(8, N);

  cout << "m/n == " << m << '/' << n << endl;
  Real theta = 2*pi*(Real(m)/Real(n) + 23.0/64.0);
  FlowField u(EQ2);
  FlowField tmp;

  tmp = ef2;
  tmp *= eps*cos(theta);
  u += tmp;

  tmp = ef3;
  tmp *= -eps*sin(theta);
  u += tmp;

  string name = "u_" + hexcode;
  u.save(name);
}
