#ifndef CHFLOW_DNSPOINCARE_H
#define CHFLOW_DNSPOINCARE_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/array.h"
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

namespace channelflow {

// A class for integrating u to a Poincare section and mapping u back into
// a fundamental domain of a discrete symmetry group. For the Poincare section,
// the stopping condition is a geometric condition rather than a stopping time,
// namely (u(t) - ustar, estar) = 0. The fundamental domain is defined as
// (u(t), e[n]) >= 0. e[n] is antisymmetric under symmetry sigma[n], so that when
// (u(t), e[n]) <  0, we can get back to the fundamental domain by applying sigma[n],
// since (sigma[n] u(t), e[n]) = (u(t), sigma[n] e[n]) = (u(t), -e[n]) > 0.

// advanceToSection should be used this way
// 1. Start with some initial condition (u,q,t), t arbitrary
// 2. Make repeated calls to advanceToSection(u,q,nsteps,crosssign,eps). Each call will
//    advance (u,q,t) by dT = nSteps*dt and map u(t) back into the fundamental domain
//    should it leave. The sign argument determines which kinds of crossings will
//    return: sign<0 => only dh/dt < 0,
//            sign=0 => either direction
//            sign>0 => only dh/dt > 0,
// 3. Check the return value of advanceToSection. It will be
//      FALSE if u(t) does not cross the section during the the advancement and
//      TRUE  if u(t) does cross the section
// 4. When the return value is TRUE, you can then access the values of (u,q,t)
//    at the crossing through ucrossing(), pcrossing(), and tcrossing().
// 5. The signcrossing() member function returns the sign of dh/dt at h==0
// 6. Continue on with more calls to advanceToSection to find the next intersection,
//    if you like.
// The integration to the section is done in multiple steps and multiple calls
// to advanceToSection so that the field can be saved, projected, etc.
// over the course of integration by the caller.

class PoincareCondition {
public:
  PoincareCondition();
  virtual Real operator()(const FlowField& u) = 0;
};



// Section defined by (u - ustar, estar) == (u, estar) - (ustar, estar)
class PlaneIntersection : public PoincareCondition {
public:
  PlaneIntersection();
  PlaneIntersection(const FlowField& ustar, const FlowField& estar);
  Real operator()(const FlowField& u);
private:
  FlowField estar_; // A normal that defines orientation of section
  Real      cstar_; // L2IP(ustar, estar), defines location of section
};


// Section defined by drag(u) - dissipation(u) == 0
class DragDissipation : public PoincareCondition {
public:
  DragDissipation();
  Real operator()(const FlowField& u);
};


class DNSPoincare : public DNS {

public:
  DNSPoincare();

  DNSPoincare(const FlowField& u, const array<FlowField>& e, const array<FieldSymmetry>& sigma,
	      PoincareCondition* h, Real nu, Real dt, const DNSFlags& flags, Real t0=0);

  bool advanceToSection(FlowField& u, FlowField& q, int nSteps, int crosssign=0, Real Tmin=0,
			Real epsilon=1e-13);

  //Real f(const FlowField& u) const; // poincare condition is f(u) == 0

  const FlowField& ucrossing() const;
  const FlowField& pcrossing() const;
  Real poincareCondition() const; //
  Real tcrossing() const;
  int  signcrossing() const;      // -1 or 1 for dh/dt<0 or dh/dt>0

private:
  array<FlowField> e_;         // Defines fundamental domain. See comments above.
  array<FieldSymmetry> sigma_; // Maps u(t) back into fundamental domain. See above.
  PoincareCondition* h_;       // The poincare condition h(u) == 0.

  FlowField ucrossing_;        // velocity field at crossing
  FlowField pcrossing_;        // pressure field at crossing
  Real      tcrossing_;        // time at crossing
  int       scrossing_;        // sign of dh/dt at crossing
  Real poincareCondition_;     // value of (*h)(ucrossing_)

  Real t0_;                    // starting time, used to check t-t0 >= Tmin
};

}

#endif
