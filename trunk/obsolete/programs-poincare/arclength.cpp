#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

#include "dnspoincare.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("compute arclength along unstable manifold of EQ2");
   ArgList args(argc, argv, purpose);

  const int N  = args.getreal("-N", "--N", 300, "number of initial fields");

  ofstream sos("s.asc");
  ofstream dsos("ds.asc");

  FlowField uprev("EQ2");
  Real  s = 0.0;
  Real ds = 0.0;

  for (int n=0; n<N; ++n) {
    cout << n << ' ' << flush;
    FlowField u("u" + i2s(n));
    ds = L2Dist(u,uprev);
    s += ds;
    sos << s << '\n';
    dsos << ds << '\n';
    uprev = u;
  }
  cout << endl;
}
