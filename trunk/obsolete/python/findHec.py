#!/usr/bin/env python

from channelflow import *
from scipy.optimize import *
from scipy.interpolate import lagrange
from math import sin,  cos
from optparse import OptionParser
from sys import stdout

def polarAdd(r0, theta, src, e0, e1):
    Nx = src.Nx()
    Ny = src.Ny()
    Nz = src.Nz()
    Nd = 3
    Lx = src.Lx()
    Lz = src.Lz()
    a = src.a()
    b = src.b()
    
    u = FlowField(Nx, Ny, Nz, Nd, Lx, Lz,  a,  b, Spectral, Spectral)
    u.setToZero()
    tmp = FlowField(Nx, Ny, Nz, Nd, Lx, Lz,  a,  b, Spectral,  Spectral)
    tmp.setToZero()
    tmp += e0
    tmp *= r0*cos(theta)
    u += e1
    u *= r0*sin(theta)
    u += src
    u+= tmp
    return u
    
def closestPass(theta, r0,  src,sink,e0,e1,dnsargs):
    
    print theta
    Nx = src.Nx()
    Ny = src.Ny()
    Nz = src.Nz() 
    Nd = 3
    Lx = src.Lx()
    Lz = src.Lz()
    a = src.a()
    b = src.b()
    
    u = polarAdd(r0, theta, src, e0, e1)
    q = FlowField(Nx,Ny,Nz,1,Lx,Lz,a,b);
    
    dns = DNS(u, 1.0/dnsargs.Reynolds, dnsargs.dt.dt(), dnsargs.flags, 0);
    
    d0 = 0
    d1 = 0
    d2 = 0
    dt = dnsargs.dt
    for t in range(dnsargs.Tmax):
        dns.advance(u, q, dnsargs.dt.n());
        if(dt.adjust(dns.CFL())):
            dns.reset_dt(dt);
        d0 = d1
        d1 = d2
        d2 = L2Dist(u, sink)
        print str(t) + ' ' + str(d2)
        if(t > dnsargs.Tmin and d2 > d1 and d1 < d0):
            interp = lagrange((0, 1, 2), (d0, d1, d2)) #quadratic interpolation
            return interp(-interp[1] / (2*interp[2]))
        elif(t > dnsargs.Tmin and d2 > d1):
            return d0
    
    return d2
            
        
class DNSargs:
#TODO: method to generate DNSargs from command line options
    vardt = 1
    dtarg = 0.03125;
    dtmin = 0.001;
    dtmax = 0.05;
    dT = 1.0;
    CFLmin = 0.4;
    CFLmax = 0.6;
    Reynolds = 400;
    r0 = 1e-3
    Tmin = 170
    Tmax = 400
    
    def __init__(self):
        self.flags =  DNSFlags();
        self.flags.timestepping = SBDF3;
        self.flags.dealiasing   = DealiasXZ;
        self.flags.nonlinearity = Rotational;
        self.flags.constraint   = BulkVelocity;
        self.flags.baseflow     = PlaneCouette;
        self.flags.verbosity     = False;
        self.flags.dPdx         = 0;
        self.flags.Ubulk        = 0;
        self.dt=TimeStep(self.dtarg, self.dtmin, self.dtmax, 
                         self.dT, self.CFLmin, self.CFLmax, self.vardt);

def findHec(src, sink, e0, e1,r0,  theta0, dnsargs):
    f = lambda theta: closestPass(theta, r0,  src, sink, e0, e1, dnsargs)
    cb = lambda *x:sys.stdout.write(" ".join(map(str,x)))
    args = (r0,  src,sink,e0,e1,dnsargs)
    #thetaopt,  fopt,  iter,  funcalls, warnflag) = fmin(f,  theta0, fulloutput=1,  callback=cb)
    (thetaopt,  fopt,  iter,  funcalls) = brent(closestPass,  args,  brack=(-pi/2, 0, pi/4), full_output=1)
    if(warnflag == 1):
        print "Maximum number of function evaluations reached!"
    elif(warnflag == 2):
        print "Maximum number of iterations reached!"
    
    print "Number of iterations : " + str(iter)
    print "Closest pass : " + str(fopt)
    print "Phase : " + str(thetaopt)
    u = polarAdd(r0, theta, src, e0, e1)
    return u
    
    
def main():

    usage = "usage: %prog [options] src.ff sink.ff e0.ff e1.ff"
    parser = OptionParser(usage)
    parser.add_option("-o", "--output", dest="outfile", default="hec.ff", help="Solution file")
    parser.add_option("-i", "--theta0", dest="theta0", default=0,type="float",  help="Initial phase")
    parser.add_option("-r", "--radius", dest="r0", type="float", default=1e-3, help="Initial radius")
    dnsargs = DNSargs()
    
    (options, args) = parser.parse_args()
    if(len(args) != 4):
        print "Error: Not enough arguments"
        print usage
    
    src = FlowField(args[0])
    sink = FlowField(args[1])
    e0 = FlowField(args[2])
    e1 = FlowField(args[3])
    
    hec = findHec(src, sink, e0, e1, options.r0,  options.theta0, dnsargs)
    
    hec.binarySave(outfile)
    
if __name__ == '__main__':
    main()
