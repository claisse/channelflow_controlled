#!/usr/bin/python

# The lazy man's couette.x redone in python

import sys;
from channelflow import *;

vardt = 1;
dtarg = 0.03125;
dtmin = 0.001;
dtmax = 0.05;
dT = 1.0;
CFLmin = 0.4;
CFLmax = 0.6;
T0 = 0;
T1 = 10;

Reynolds = 400;

dt = TimeStep(dtarg, dtmin, dtmax, dT, CFLmin, CFLmax, vardt);

flags = DNSFlags();

flags.timestepping = SBDF3;
flags.dealiasing   = DealiasXZ;
flags.nonlinearity = Rotational;
flags.constraint   = BulkVelocity;
flags.baseflow     = PlaneCouette;
flags.dPdx         = 0;
flags.Ubulk        = 0;

u = FlowField(sys.argv[1]);
q = FlowField(u.Nx(),u.Ny(),u.Nz(),1,u.Lx(),u.Lz(),u.a(),u.b());

dns = DNS(u, 1.0/Reynolds, dt.dt(), flags, T0);

for t in range(T0,T1):
	print("t == " + str(t));
	print("L2Norm(u) == "+ str(L2Norm(u)));
	dns.advance(u, q, dt.n());
	if(dt.adjust(dns.CFL())):
		dns.reset_dt(dt);
	
	u.binarySave('u' + str(t));

