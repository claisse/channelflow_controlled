// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("project a series of flowfields onto a given basis");
  ArgList args(argc, argv, purpose);

  const Real T0        = args.getreal("-T0", "--T0", 0,   "start time");
  const Real T1        = args.getreal("-T1", "--T1", 500, "end time");
  const Real dT        = args.getreal("-dT", "--dT", 1.0, "save interval");
  const string bdir    = args.getpath("-b", "--basisdir", "./", "basis set directory");
  const int    Nbasis  = args.getint("-Nb", "--Nbasis", "number of basis fields");
  const string udir    = args.getpath("-d", "--datadir", "data/", "flowfield series directory");
  const string origin  = args.getstr("-or",  "--origin", "", "substract this field before projection");
  const string ulabel  = args.getstr("-ul", "--ulabel", "u",  "flowfield filename label");
  const string ofile   = args.getstr("-o",  "--outfile", "a", "output filename");
  const int  digits    = args.getint("-dg", "--digits", 8,    "# digits in output");
  const bool xshift    = args.getflag("-xs", "--xshift", "translate by Lx/2");
  const bool zshift    = args.getflag("-zs", "--zshift", "translate by Lz/2");
  const bool energy    = args.getflag("-e",  "--energy", "output forcing, dissipation, and energy");
  const bool symmetry  = args.getflag("-s",  "--symmetry", "output symmetries");

  const string blabel  = "e"; //args.getstr("-bl", "--basislabel", "e", "basis label");
  const bool inttime = (abs(dT - int(dT)) < 1e-12) ? true : false;
  args.check();
  args.save("./");

  const char s = ' ';

  FlowField etx("etx");
  FlowField etz("etz");
  FieldSymmetry taux(1, 1, 1, 0.5, 0);
  FieldSymmetry tauz(1, 1, 1, 0.0, 0.5);

  cout << "load basis set..." << endl;
  FlowField e[Nbasis];
  for (int n=0; n<Nbasis; ++n)
    e[n] = FlowField(bdir + blabel + i2s(n));

  bool recenter = (origin.length() != 0) ? true : false;
  FlowField uorigin;
  if (recenter) {
    cout << "loading origin.." << endl;
    uorigin = FlowField(origin);
  }

  FlowField su(udir + ulabel + t2s(T0, inttime));
  const int Ny = su.Ny();
  const Real ya = su.a();
  const Real yb = su.b();
  su = FlowField();

  Vector y = chebypoints(Ny, ya, yb);
  ChebyCoeff U(Ny,ya,yb,Spectral);
  U[1] = 1;

  FieldSymmetry s1(1, 1,-1, 0.5, 0);
  FieldSymmetry s2(-1,-1, 1, 0.5, 0.5);
  FieldSymmetry s3(-1,-1,-1, 0.0, 0.5);
  FlowField us; // symmetric portion of u
  FlowField ua; // antisymmetric portion of u

  ofstream os(appendSuffix(ofile, ".asc").c_str());

  os << "% ";
  for (int n=0; n<argc; ++n)
    os << argv[n] << s;
  os << endl;
  os << "% ";
  for (int n=0; n<Nbasis; ++n)
    os << (blabel + i2s(n)) << s;
  if (symmetry)
    os << "Ps1 Ps2 Ps3 ";
  if (energy)
    os << "forcing(u+U) dissipation(u+U) energy(u+U) energy(u)";
  os << endl;
  os << setprecision(lesser(digits, 17));

  cout << "Projecting fields onto basis..." << endl;

  for (Real t=T0; t<=T1; t += dT) {

    string uname = ulabel + t2s(t, inttime);
    cout << uname << s << flush;
    FlowField u(udir + uname);

    if (L2IP(u, etx) < 0)
      u *= taux;
    if (L2IP(u, etz) < 0)
      u *= tauz;

    if (recenter)
      u -= uorigin;

    /*************************
    if (xshift && zshift)
      u.translate(u.Lx()/2, u.Lz()/2);
    else if (xshift)
      u.translate(u.Lx()/2, 0.0);
    else if (zshift)
      u.translate(0.0, u.Lz()/2);
    **************************/

    for (int n=0; n<Nbasis; ++n)
      os << L2IP(u,e[n]) << s;

    if ((symmetry || energy) && recenter)
      u += uorigin;
    if (symmetry)
      os << PuFraction(u,s1,-1) <<s<< PuFraction(u,s2,-1) <<s<< PuFraction(u,s3,-1) <<s;
    if (energy) {
      Real l2norm2u = L2Norm2(u);
      u += U;
      os << forcing(u) <<s<< dissipation(u) <<s<< 0.5*L2Norm2(u) <<s<< 0.5*l2norm2u;
    }
    os << endl;
  }
  cout << "done!" << endl;
}

/* project.cpp: project time series of fields onto a given basis
 * channelflow-1.1 PCF-utils
 *
 * Copyright (C) 2001-2007  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu  jfg@member.fsf.org
 *
 * Center for Nonlinear Science
 * School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 * 404 385 2509
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation version 2
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
