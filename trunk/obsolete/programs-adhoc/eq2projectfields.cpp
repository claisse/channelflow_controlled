// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("project a set of fields onto a given basis\n"
		 "\tusage: projectfields.x -b <basisdir> -Nb <int> <field> <field> ...\n");
  ArgList args(argc, argv, purpose);

  const string bdir    = args.getpath("-b", "--basisdir", "", "basis set directory");
  const int    Nbasis  = args.getint("-Nb", "--Nbasis",   "number of basis fields");
  const string origin  = args.getstr("-or",  "--origin", "", "substract this field before projection");
  const bool energy    = args.getflag("-e",  "--energy", "output wallshear, dissipation, and energy");
  const bool symmetry  = args.getflag("-s",  "--symmetry", "output symmetries");
  string ofile         = args.getstr("-o", "--outfile", "",  "output file");
  const int digits     = 8;
  char s = ' ';

  if (args.helpmode())
    exit(1);
  args.save("./");


  // argc-3 is for argc - (1 each for argv[0], -Nb <nb>)
  const int Nfields = argc-3 - (bdir.length() == 0 ? 0:2) - (ofile.length() == 0 ? 0:2) - (origin.length() == 0 ? 0:2);
  if (ofile.length() == 0)
    ofile = "a.asc";

  ofstream os(appendSuffix(ofile, ".asc").c_str());

  bool recenter = (origin.length() != 0) ? true : false;
  FlowField uorigin;
  if (recenter) {
    cout << "loading origin.." << endl;
    uorigin = FlowField(origin);
  }

  array<string> uname(Nfields);
  for (int n=0; n<Nfields; ++n) {
    uname[n] = args.getstr(Nfields-n, "<flowfield>", "field to be projected");
  //cout << "u[" << n << "] == " << uname[n] << endl;
  }

  array<FlowField> e(Nbasis);
  for (int n=0; n<Nbasis; ++n) {
    cout << "reading in basis elem " << (bdir + "e" + i2s(n)) << endl;
    e[n] = FlowField(bdir + "e" + i2s(n));
  }
  os << "% ";
  for (int j=0; j<Nbasis; ++j)
    os << ("e" + i2s(j)) << s;
  if (symmetry)
    os << "Ps1 Ps2 Ps3 ";
  if (energy)
    os << "wallshear(u+U) dissipation(u+U) energy(u+U) energy(u)";
  os << endl;
  os << setprecision(lesser(digits, 17));

  FieldSymmetry s1(1, 1,-1, 0.5, 0);
  FieldSymmetry s2(-1,-1, 1, 0.5, 0.5);
  FieldSymmetry s3(-1,-1,-1, 0.0, 0.5);
  ChebyCoeff U;
  for (int n=0; n<Nfields; ++n) {
    cout << uname[n] << endl;
    FlowField un(uname[n]);
    if (recenter)
      un -= uorigin;

    for (int m=0; m<Nbasis; ++m)
      os << L2IP(un,e[m]) << ' ';

    if (n==0) {
      const int Ny = un.Ny();
      const Real ya = un.a();
      const Real yb = un.b();
      U = ChebyCoeff(Ny,ya,yb,Spectral);
      U[1] = 1;
    }

    if ((symmetry || energy) && recenter)
      un += uorigin;

    if (symmetry)
      os << PuFraction(un,s1,-1) <<s<< PuFraction(un,s2,-1) <<s<< PuFraction(un,s3,-1) <<s;

    if (energy) {
      Real l2norm2u = L2Norm2(un);
      un += U;
      os << wallshear(un) <<s<< dissipation(un) <<s<< 0.5*L2Norm2(un) <<s<< 0.5*l2norm2u;
    }

    os << " % " << uname[n] << endl;
  }
}

/* project.cpp: project time series of fields onto a given basis
 * channelflow-1.1 PCF-utils
 *
 * Copyright (C) 2001-2007  John F. Gibson
 *
 * gibson@cns.physics.gatech.edu  jfg@member.fsf.org
 *
 * Center for Nonlinear Science
 * School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 * 404 385 2509
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation version 2
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
