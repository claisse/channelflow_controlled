#include <iostream>
#include "chebyshev.h"

// This program tests the ChebyCoeff interpolation methods. It starts with
// a random function on one grid, and then interpolates it onto another grid.
// 
// There's no numerical error output since the interpolated function is
// on a different grid than the original. You have to graph and look at the
// results. For that, use the matlab script viewChebyInterp.m.

int main() {

  Real decay = 0.8;
  int N0=65;
  int N1=65;

  Real a0 = -1;
  Real b0 = 1;

  Real a1 = -1;
  Real b1 = 1;

  ChebyTransform trans0(N0);
  ChebyTransform trans1(N1);

  Vector y0 = chebypoints(N0, a0, b0);
  Vector y1 = chebypoints(N1, a1, b1);
  y0.save("y0");
  y1.save("y1");
  
  ChebyCoeff f(N0, a0, b0, Spectral);
  ChebyCoeff g(N1, a1, b1, Spectral);
  
  f.randomize(decay);

  f.makeSpectral(trans0);
  g.interpolate(f);

  f.makePhysical(trans0);
  g.makePhysical(trans1);
  f.save("f");
  g.save("g");

  f.save("fc", Spectral);
  g.save("gc", Spectral);

  // Now evaluate/interpolate f and g at the same, uniformly spaced, points
  int M=30;
  Vector f_unif(M);
  Vector g_unif(M);
  Vector y2(M);
  Real dy2 = (b1-a1)/(M-1);
  for (int m=0; m<M; ++m) {
    y2(m) = a1 +m*dy2;
    f_unif(m) = f.eval(y2(m));
    g_unif(m) = g.eval(y2(m));
  }
  Vector e_unif = f_unif - g_unif;

  e_unif.save("e_unif");
  f_unif.save("f_unif");
  g_unif.save("g_unif");


  y2.save("y2");
}
