#include <iostream>
#include <fstream>
#include "flowfield.h"
#include "cfvector.h"
#include "chebyshev.h"
#include "tausolver.h"

const Real EPSILON=1e-12;

void assignComp(FlowField& u, int kx, int kz) {
  u.setState(Physical, Physical);
  u.setToZero();

  Real cx = 2*pi*kx/u.Lx();
  Real cz = 2*pi*kz/u.Lz();
  Real dx = u.Lx()/u.Nx();
  Real dz = u.Lz()/u.Nz();
  for (int ny=0; ny<u.Ny(); ++ny) 
    for (int nx=0; nx<u.Nx(); ++nx) 
      for (int nz=0; nz<u.Nz(); ++nz) 
	u(nx, ny, nz, 0) = cos(cx*nx*dx) * cos(cz*nz*dz);
  
  u.makeSpectral();
}

// Set a flowfield to cos(2 pi kx x/Lx) cos(2 pi kz z/Lz) and then
// print out the 2d fourier transform. Shows you how the fourier coeffs
// are laid out.
int main(int argc, char* argv[]) {

  const int Nx=32;
  const int Ny=17;
  const int Nz=32;
  const int Nd=1;
  const Real Lx=2*pi;
  const Real Lz=2*pi;
  const Real a = -1.0;
  const Real b =  1.0;

  if (argc != 3) {
    cerr << "need two int args kx, kz" << endl;
    exit(1);
  }

  int kx=atoi(argv[1]);
  int kz=atoi(argv[2]);
    
  FlowField u(Nx,Ny,Nz,Nd,Lx,Lz,a,b);
  assignComp(u, kx, kz);
  u.saveSpectrum("uspec", false);
  
  
}
