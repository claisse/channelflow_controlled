#include <iostream>
#include "helmholtz.h"
#include "cfvector.h"
#include "flowfield.h"
#include "tausolver.h"
#include "chebyshev.h"

int main() {
  
  int N=33;
  Real a= -1.0;
  Real b=  1.0;


  ChebyCoeff f(N, a, b, Physical);
  Vector y = chebypoints(N, -1, 1);

  ChebyTransform t(N);

  for (int i=0; i<N; ++i) 
    f[i] = y[i] + 3*square(y[i]);
  
  f.chebyfft(t);
  cout << "f(-1) == " << f.eval_a() << endl;
  cout << "f(+1) == " << f.eval_b() << endl;
  
  ChebyCoeff Ptrue(N, a, b, Physical);
  ChebyCoeff vtrue(N, a, b, Physical);
  ChebyCoeff Ry(N, a, b, Physical);
  ChebyCoeff r(N, a, b, Physical);
  int kx = 2;
  int kz = 1;
  Real Lx = 3*pi;
  Real Lz =2.1;
  Real nu = 2.0;
  Real lambda = 3.0;
  
  Real kappa2 = 4*pi*pi*(square(kx/Lx) + square(kz/Lz));
  Real c = 3.0;
  //Real a = 1.0;
  //Real pi2 = pi/2;
  Real dPdy;

  for (int i=0; i<N; ++i) {
    Real yi = y[i];
    Real yiyi = yi*yi;
    Real theta = pi*yi;

    // Test case B
    Ptrue[i] = - yiyi + yi + sin(3*theta);
    vtrue[i] = 1.0 + cos(theta);
    dPdy     = -2*yi + 1 + 3*pi*cos(3*theta);
    r[i]     = -2-9*pi*pi*sin(3*theta) - kappa2*(Ptrue[i]);
    Ry[i]    = lambda + (lambda + nu*pi*pi)*cos(theta) + dPdy;

    // Test case A
    //Ptrue[i] = c + a*yiyi; // A
    //vtrue[i] = 1 + yiyi*(-2 + yiyi);
    //dPdy     = 2*a*yi;
    //r[i]     = a*(2-yiyi)-c; 
    //Ry[i]    = 5 +  yiyi*(-14.0 + yiyi) + dPdy;

    //-pi*sin(theta2) +(square(pi/2)-kappa2)*(yi+1.0)*cos(theta2);    
  }
  Ptrue.save("Pt");
  vtrue.save("vt");
  y.save("y");

  r.chebyfft(t);
  Ry.chebyfft(t);
  
  ChebyCoeff Psolve(N, a, b, Physical);
  ChebyCoeff vsolve(N, a, b, Physical);
  TauSolver tausolver(kx, kz, Lx, Lz, a, b, lambda, nu, N);
  Real sigmaNb1;
  Real sigmaNb;
  tausolver.solve_P_and_v(Psolve, vsolve, r, Ry, sigmaNb1, sigmaNb);
  tausolver.verify_P_and_v(Psolve, vsolve, r, Ry, sigmaNb1, sigmaNb);

  Psolve.ichebyfft(t);
  vsolve.ichebyfft(t);

  Psolve.save("Ps");
  vsolve.save("vs");
  
  cout << "normalized P error is " << L1Norm(Psolve-Ptrue)/L1Norm(Ptrue) << endl;
  cout << "normalized v error is " << L1Norm(vsolve-vtrue)/L1Norm(vtrue) << endl;

}
