#include <iostream>
#include <iomanip>
#include "cfvector.h"
#include "chebyshev.h"
#include "flowfield.h"
#include "diffops.h"
#include "dns.h"

// Compare skew-symmetric and rotational calculations of FlowField 
// nonlinearity methods.

int main() {

  // Define gridsize
  const int Nx=24;
  const int Ny=49;
  const int Nz=24;

  // Define box size
  const Real Lx=1.75*pi;
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Lz=1.2*pi;

  // Define flow parameters
  const Real Reynolds = 400.0;
  const Real nu = 1.0/Reynolds;

  // Define integration parameters
  const int n = 10;       // take n steps between printouts
  const Real dt = 0.01;
  const Real T0  = 0.0;
  const Real T1  = 10.0;

  // Define size and smoothness of initial disturbance
  Real decay = 0.6;   
  Real magnitude  = 0.01;   
  int kxmax = 3;
  int kzmax = 3;

  // Construct base flow for plane Couette: U(y) = y
  ChebyCoeff U(Ny,a,b,Physical);
  Vector y = chebypoints(Ny, a,b);
  for (int ny=0; ny<Ny; ++ny) 
    U[ny] = y[ny];
  U.save("U");
  y.save("y");
 
  // Construct data fields: 3d velocity and 1d pressure
  FlowField u_rot(Nx,Ny,Nz,3,Lx,Lz,a,b);
  FlowField q_rot(Nx,Ny,Nz,3,Lx,Lz,a,b);

  // Perturb velocity field
  u_rot.addPerturbations(kxmax,kzmax,magnitude,decay);
  
  // Construct rotational integrator 
  DNSFlags flags_rot; 
  flags_rot.nonlinearity = Rotational;
  flags_rot.dealiasing = NoDealiasing;
  DNS dns_rot(u_rot, U, nu, dt, flags_rot);

  // Advance rotational flow a few steps to knock out transients
  for (Real t=0; t<T0; t += n*dt) 
    dns_rot.advance(u_rot, q_rot, n);
  
  // Configure skewsymm integrator
  FlowField u_skw = u_rot;
  FlowField q_skw = q_rot;
  FlowField u0 = u_rot;
    
  DNSFlags flags_skw; 
  flags_skw.nonlinearity = SkewSymmetric;
  flags_skw.dealiasing = NoDealiasing;
  DNS dns_skw(u_skw, U, nu, dt, flags_skw,T0);

  for (Real t=T0; t<T1; t += n*dt) {
    ComplexChebyCoeff u12;
    u12 = u_rot.profile(1,2,0);
    u12.makePhysical();
    u12.save("urot12");
    u12 = u_skw.profile(1,2,0);
    u12.makePhysical();
    u12.save("uskw12");
    
    cout << "rot dns CFL == " << dns_rot.CFL() << endl;
    cout << "skw dns CFL == " << dns_skw.CFL() << endl;
    cout << "L2Dist2(u_rot,u_skw) == " << L2Dist2(u_rot,u_skw) << endl;
    cout << "   L2Dist2(u_rot,u0) == " << L2Dist2(u_rot,u0) << endl;
    cout << "   L2Dist2(u_skw,u0) == " << L2Dist2(u_skw,u0) << endl;
    cout << "      L2Norm2(u_rot) == " << L2Norm2(u_rot) << endl;
    cout << "      L2Norm2(u_skw) == " << L2Norm2(u_skw) << endl;
    cout << "         L2Norm2(u0) == " << L2Norm2(u0) << endl;

    dns_rot.advance(u_rot, q_rot, n);
    dns_skw.advance(u_skw, q_skw, n);
  }
}

