#include <iostream>
#include <iomanip>
#include "cfvector.h"
#include "chebyshev.h"
#include "flowfield.h"

int main() {

  // Define gridsize
  const int Nx=24;
  const int Ny=49;
  const int Nz=24;
  const int Nd=3;

  // Define box size
  const Real Lx=5;
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Lz=7;
  const Real Ly = b-a;
  const bool normalize = true;
  const Real fc = 1.0;
  const Real gc = 11.0;
  
  FlowField f(Nx,Ny,Nz,Nd,Lx,Lz,a,b,Physical,Physical);
  FlowField g(Nx,Ny,Nz,Nd,Lx,Lz,a,b,Physical,Physical);
  
  for (int i=0; i<Nd; ++i)
    for (int ny=0; ny<Ny; ++ny)
      for (int nx=0; nx<Nx; ++nx)
	for (int nz=0; nz<Nz; ++nz) {
	  f(nx,ny,nz,i) = fc;
	  g(nx,ny,nz,i) = gc;
	}

  f.makeSpectral();
  g.makeSpectral();

  cout << "L1Norm(f)    == " << L1Norm(f,normalize) << endl;
  cout << "L1Norm(g)    == " << L1Norm(g,normalize) << endl;
  cout << "L1Dist(f,g)  == " << L1Dist(f,g,normalize) << endl;

  cout << "LinfNorm(f)    == " << LinfNorm(f) << endl;
  cout << "LinfNorm(g)    == " << LinfNorm(g) << endl;
  cout << "LinfDist(f,g)  == " << LinfDist(f,g) << endl;

  Real nrm_xyz = (normalize) ? 1.0 : Lx*Ly*Lz;
  Real nrm_xz  = (normalize) ? 1.0 : Lx*Lz;

  cout << "L2InnerProduct(f,f)  == " << L2InnerProduct(f,f,normalize) << endl;
  cout << "L2InnerProduct(g,g)  == " << L2InnerProduct(g,g,normalize) << endl;
  cout << "L2InnerProduct(f,g)  == " << L2InnerProduct(f,g,normalize) << endl;
  cout << "L2Norm2(f)   should be " << nrm_xyz*Nd*square(fc) << endl;
  cout << "L2Norm2(g)   should be " << nrm_xyz*Nd*square(gc) << endl;
  cout << "L2Dist2(f,g) should be " << nrm_xyz*Nd*square(fc-gc) << endl;

  cout << "L2Norm2(f)   == " << L2Norm2(f,normalize) << endl;
  cout << "L2Norm2(g)   == " << L2Norm2(g,normalize) << endl;
  cout << "L2Dist2(f,g) == " << L2Dist2(f,g,normalize) << endl;
  cout << "L2Norm(f)    == " << L2Norm(f,normalize) << endl;
  cout << "L2Norm(g)    == " << L2Norm(g,normalize) << endl;
  cout << "L2Dist(f,g)  == " << L2Dist(f,g,normalize) << endl;

  cout << "bcNorm2(f)   should be " << 2*nrm_xz*Nd*square(fc) << endl;
  cout << "bcNorm2(g)   should be " << 2*nrm_xz*Nd*square(gc) << endl;
  cout << "bcDist2(f,g) should be " << 2*nrm_xz*Nd*square(fc-gc) << endl;

  cout << "bcNorm2(f)   == " << bcNorm2(f,normalize) << endl;
  cout << "bcNorm2(g)   == " << bcNorm2(g,normalize) << endl;
  cout << "bcDist2(f,g) == " << bcDist2(f,g,normalize) << endl;
  cout << "bcNorm(f)    == " << bcNorm(f,normalize) << endl;
  cout << "bcNorm(g)    == " << bcNorm(g,normalize) << endl;
  cout << "bcDist(f,g)  == " << bcDist(f,g,normalize) << endl;
}
