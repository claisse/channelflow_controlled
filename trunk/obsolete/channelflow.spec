Name: channelflow
Version: 1.3.3
Release: 2
License: GPLv2
Group: Productivity/Scientific/Physics
Source: http://www.channelflow.org/download/channelflow-1.3.3.tar.gz
Summary: Numerical analysis of channel and plane Couette flows
Vendor: School of Physics, Georgia Tech
Packager: John F. Gibson <gibson@cns.physics.gatech.edu>
BuildRoot: %{_tmppath}/%{name}-buildroot
Requires: fftw3
BuildRequires: fftw3-devel gcc-c++ libstdc++-devel octave octave-devel hdf5-devel

%description
Channelflow is a software system for numerical analysis of the
incompressible Navier-Stokes flow in channel geometries. The
core of channelflow is an algorithm for time-integration of
Navier-Stokes for Fourier x Chebyshev Fourier spectral expansions
of velocity fields. This engine drives a number of higher-level
algorithms that (for example) compute equilibria, traveling waves,
and periodic orbits of Navier-Stokes. The channelflow distribution
includes a number of predefined executable programs and a library
and header files for the development of custom codes.

%prep
%setup -q
%build
%configure
make

%install

%makeinstall
strip $RPM_BUILD_ROOT/usr/bin/*

%clean

%files
%defattr(-,root,root)
%doc INSTALL README TODO ChangeLog examples
/usr/bin/addfields
/usr/bin/ascii2field
/usr/bin/changegrid
/usr/bin/couette
/usr/bin/field2ascii
/usr/bin/fieldplots
/usr/bin/fieldprops
/usr/bin/L2Dist
/usr/bin/L2IP
/usr/bin/makebasis
/usr/bin/makeheatmode
/usr/bin/makestokesmode
/usr/bin/movieframes
/usr/bin/perturbfield
/usr/bin/projectfields
/usr/bin/projectseries
/usr/bin/randomfield
/usr/bin/seriesprops
/usr/bin/symmetrize
/usr/bin/symmetryop
/usr/bin/arnoldi
/usr/bin/findorbit
/usr/include/channelflow
/usr/lib64/libchflow.a
/usr/lib64/libchflow.la
/usr/lib64/libchflow.so
/usr/lib64/libchflow.so.0
/usr/lib64/libchflow.so.0.0.0

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%changelog
* Thu Oct  2 2008 John F. Gibson <gibson@cns.physics.gatech.edu? 1.3.2-2
- Modified spec file for use on openSUSE build farm
