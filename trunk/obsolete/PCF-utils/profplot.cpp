#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <sys/stat.h>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/periodicfunc.h"

#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {
  
 string purpose("create a set of orthonormal basis set from linearly "
		 "independent fields.\n"
		"usage: makebasis.x [options] u0 u1 u2 ...");  // Assign parameters
  array<string> args = args2str(argc, argv);
  bool help = fgetarg("-help", args, false);

  const string label  = sgetarg("-label", "", args, help);
  const string outdir = pathfix(sgetarg("-odir", "./", args, help));
  //const int Kx        = igetarg("-kx", args, help);
  //const int Kz        = igetarg("-kz", args, help);
  const string uname  = sgetarg(1, "<flowfield>", args, help);
  checkargs(args, help);

  FlowField u(uname);
  u.makeSpectral();
  
  int Kx = 0;
  int Kz = 1;

  

  for (int mx=0; mx<=Kx; ++kx) 
    for (int mz=0; mz<=uKz; ++kz) {
      BasisFunc profile = u.profile(mx,mz);
      profile.makePhysical();
      profile.save(outdir + "uprof" + i2s(kx) + i2s(kz));
    }

  Vector y = chebypoints(u.Ny(), u.a(), u.b());
  y.save(outdir + "y");
}
