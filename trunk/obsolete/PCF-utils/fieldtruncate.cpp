#include <iostream>
#include <fstream>
#include <iomanip>
#ifdef HAVE_OCTAVE
#include <octave/oct.h>
#endif
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("truncate the Fourier expansion of a given flowfield");
  ArgList args(argc, argv, purpose);  // Assign parameters

  const int Kx        = args.getint("-Kx", "--Kx", "zero modes with |kx|>Kx");
  const int Kz        = args.getint("-Kz", "--Kz", "zero modes with |kz|>Kz");
  const Real scale    = args.getreal("-s", "--scale", 0.0, "scale modes by this factor instead of zeroing");
  const string u0name = args.getstr(2, "<infield>",  "input flow field");
  const string u1name = args.getstr(1, "<outfield>", "output flow field");

  args.check();
  args.save("./");

  FlowField u0(u0name);
  FlowField u1(u0);
  u1.makeSpectral();
  Complex zero(0.0, 0.0);
  for (int mx=0; mx<u1.Mx(); ++mx) {
    int kx = u1.kx(mx);
    for (int mz=0; mz<u1.Mz(); ++mz) {
      int kz = u1.kz(mz);
      if (abs(kx)>Kx || abs(kz)>Kz)
	for (int i=0; i<u1.Nd(); ++i) 
	  for (int my=0; my<u1.My(); ++my)
	    u1.cmplx(mx,my,mz,i) *= scale;
    }
  }
  if (Kx <= u1.kxmaxDealiased() && Kz <= u1.kzmaxDealiased())
    u1.setDealiased(true);
  
  cout << setprecision(16);
  cout << "L2Dist(u0,u1)  == " << L2Dist(u0,u1) << endl;
  cout << "L2Norm(u0)     == " << L2Norm(u0) << endl;
  cout << "L2Norm(u1)     == " << L2Norm(u1) << endl;
  cout << "bcNorm(u0)     == " << bcNorm(u0) << endl;
  cout << "bcNorm(u1)     == " << bcNorm(u1) << endl;
  if (u0.Nd() == 3) {
    cout << "divNorm(u0) == " << divNorm(u0) << endl;
    cout << "divNorm(u1) == " << divNorm(u1) << endl;
  }
  u1.binarySave(u1name);
}

