#include <iostream>
#include <fstream>
#include <iomanip>
#ifdef HAVE_OCTAVE
#include <octave/oct.h>
#endif
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "utilities.h"

using namespace std;
using namespace channelflow;

//#include "helperfuncs.h"

int main(int argc, char* argv[]) {

  string purpose("convert from D. Viswanath ascii format to a channelflow FlowField");
  ArgList args(argc, argv, purpose);

  const string dvname  = args.getstr(2, "<dvname>", "DV input filename stub");
  const string ffname  = args.getstr(1, "<fieldname>", "output field name");

  args.check();

  const string c = ", ";
  // Define gridsize
  int L,M,N;
  ifstream is((dvname+"nlm.dat").c_str());
  is >> N >> L >> M;
  is.close();
  cout << "L,M,N == " << L <<c<< M <<c<< N << endl;

  Real lx,lz;
  is.open((dvname+"LxLzRe.dat").c_str());
  is >> lx >> lz;
  is.close();
  cout << "lx,lz == " << lx <<c<< lz << endl;
  // Define box size
  const int Nx = 2*L;
  const int Ny = M+1;
  const int Nz = 2*N;

  const Real Lx=2*pi*lx;
  const Real Lz=2*pi*lz;
  const Real a= -1.0;
  const Real b=  1.0;

  ifstream us((dvname + "u.dat").c_str());
  ifstream vs((dvname + "v.dat").c_str());
  ifstream ws((dvname + "w.dat").c_str());

  Complex zero(0.0, 0.0);
  FlowField u(Nx,Ny,Nz,3,Lx,Lz,a,b,Physical,Physical);

  // Read Divikar's data from disk 
  int Narray = Nx*Ny*Nz;
  cout << "Narray == " << Narray << endl;
  Real ua[Narray];
  Real va[Narray];
  Real wa[Narray];
  for (int n=0; n<Narray; ++n) {
    us >> ua[n];
    vs >> va[n];
    ws >> wa[n];
  }

  // My notation   Divikar's equivalent
  //         Nx == 2*N
  //         Ny == M+1
  //         Nz == 2*L
  //         nx == i
  //         ny == j
  //         nz == k

  for (int ny=0; ny<Ny; ++ny) {
    for (int nz=0; nz<Nz; ++nz) {
      for (int nx=0; nx<Nx; ++nx) {
	int n = nz*Nx*Ny + nx*Ny + ny;
	u(nx,ny,nz,0) = ua[n];
	u(nx,ny,nz,1) = va[n];
	u(nx,ny,nz,2) = wa[n];
      }
    }
  }

  u.makeSpectral();
  cout << " L2Norm(u+U) == " << L2Norm(u) << endl;
  u.makePhysical();


  Vector y = chebypoints(Ny, a, b);
  for (int ny=0; ny<Ny; ++ny) {
    for (int nz=0; nz<Nz; ++nz) {
      for (int nx=0; nx<Nx; ++nx) {
	u(nx,ny,nz,0) -= y[ny];
      }
    }
  }
  u.makeSpectral();

  cout << " L2Norm(u)   == " << L2Norm(u) << endl;
  cout << "divNorm(u)   == " << divNorm(u) << endl;
  cout << " bcNorm(u)   == " << bcNorm(u) << endl;

  u.binarySave(ffname);
}
