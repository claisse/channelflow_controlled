#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/CColVector.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/EIG.h>
#include <octave/dbleSVD.h>
#include "channelflow/vector.h"
#include "utilities.h"

// This program is an implementation of Divakar Viswanath's Newton-hookstep
// GMRES algorithm for computing invariant solutions of Navier-Stokes. The
// basic idea is to find solutions of f^T(u) - u = 0 where f^T(u) is the
// time-T CFD integration of the fluid velocity field u and tau is an x,z
// translation. T and tau can be held fixed or varied. Viswanath's algorithm
// combines a Newton-hookstep trust-region minimization of || f^T(u) - u||^2 = 0.
// with iterative GMRES solution of the Newton-step equation
// Df^T/du du + Df^T/dT dT + = -f^T(u).

// of Trefethen and Bau's "Numerical Linear Algebra". The details of the hookstep
// algorithm are based on section 6.4.2 of Dennis and Schnabel's "Numerical Methods
// for Unconstrained Optimization and Nonlinear Equations."
//
// See end of file for a detailed description of the algorithm and notation.
//
// References:
//
// Divakar Viswanath, "Recurrent motions within plane Couette turbulence",
// J. Fluid Mech. 580 (2007), http://arxiv.org/abs/physics/0604062
//
// Lloyd N. Trefethen and David Bau, "Numerical Linear Algebra", SIAM,
// Philadelphia, 1997.
//
// Dennis and Schnabel, "Numerical Methods for Unconstrained Optimization
// and Nonlinear Equations", Prentice-Hall Series in Computational Mathematics,
// Englewood Cliffs, New Jersey, 1983.
//
// John Gibson, Wed Oct 10 16:10:46 EDT 2007

using namespace std;
using namespace channelflow;

// G(x) = function to be minimixed
void G(const ColumnVector& x, ColumnVector& Gx);

enum HookstepPhase {ConstantDelta, ReducingDelta, IncreasingDelta, Finished};
ostream& operator<<(ostream& os, HookstepPhase p);

enum ResidualImprovement {Unacceptable, Poor, Ok, Good, Accurate, NegaCurve};
ostream& operator<<(ostream& os, ResidualImprovement i);

Real adjustLambda(Real lambda, Real lambdaMin, Real lambdaMax);
Real adjustDelta(Real delta, Real rescale, Real deltaMin, Real deltaMax);

void save(const Vector& x, const string& filebase);

Real L2IP(const ColumnVector& u, const ColumnVector& v);

void solve(const Matrix& Ut, const DiagMatrix& D, const Matrix& C, const ColumnVector& b,
	   ColumnVector& x);

int main(int argc, char* argv[]) {

  string purpose("Newton-Krylov-hookstep search for (relative) periodic orbit in U_S");
  ArgList args(argc, argv, purpose);

  const Real epsSearch = args.getreal("-es",  "--epsSearch",   1e-13, "stop search if L2Norm(s f^T(u) - u) < epsEQB");
  const Real epsKrylov = args.getreal("-ek",  "--epsKrylov",   1e-14, "min. condition # of Krylov vectors");
  const Real epsDu     = args.getreal("-edu", "--epsDuLinear", 1e-7,  "relative size of du to u in linearization");
  const bool centdiff  = args.getflag("-cd", "--centerdiff", "centered differencing to estimate differentials");

  const int  Nnewton   = args.getint( "-Nn", "--Nnewton",   20,    "max number of Newton steps ");
  const int  Nhook     = args.getint( "-Nh", "--Nhook",     20,    "max number of hookstep iterations per Newton");

  /***/ Real delta     = args.getreal("-d",    "--delta",      0.01,  "initial radius of trust region");
  const Real deltaMin  = args.getreal("-dmin", "--deltaMin",   1e-12, "stop if radius of trust region gets this small");
  const Real deltaMax  = args.getreal("-dmax", "--deltaMax",   0.1,   "maximum radius of trust region");
  const Real deltaFuzz = args.getreal("-df",   "--deltaFuzz",  0.01,  "accept steps within (1+/-deltaFuzz)*delta");
  const Real lambdaMin = args.getreal("-lmin",   "--lambdaMin", 0.2,  "minimum delta shrink rate");
  const Real lambdaMax = args.getreal("-lmax",   "--lambdaMax", 1.5,  "maximum delta expansion rate");
  const Real lambdaRequiredReduction = 0.5; // when reducing delta, reduce by at least this factor.
  const Real improvReq = args.getreal("-irq",  "--improveReq", 1e-3,  "reduce delta and recompute hookstep if improvement "
				      " is worse than this fraction of what we'd expect from gradient");
  const Real improvOk  = args.getreal("-iok",  "--improveOk",   0.10, "accept step and keep same delta if improvement "
				      "is better than this fraction of quadratic model");
  const Real improvGood= args.getreal("-igd",  "--improveGood", 0.75, "accept step and increase delta if improvement "
				      "is better than this fraction of quadratic model");
  const Real improvAcc = args.getreal("-iac",  "--improveAcc",  0.10, "recompute hookstep with larger trust region if "
				      "improvement is within this fraction quadratic prediction.");

  const int  coutprec  = args.getint("-cp", "--coutprec", 6, "precison for std output");
  const bool pausing   = args.getflag("-p", "--pausing", "pause between Newton steps");
  const string outdir  = args.getpath("-o", "--outdir", "./", "output directory");
  const string uname   = args.getstr(1, "<flowfield>", "initial guess for Newton search");

  args.check();
  args.save(outdir);

  const int w = coutprec + 7;

  cout << "Working directory == " << pwd() << endl;
  cout << "Command-line args == ";
  for (int i=0; i<argc; ++i) cout << argv[i] << ' '; cout << endl;

  int fcount_newton = 0;
  int fcount_hookstep = 0;

  ColumnVector x(2);
  x(0) = 2;
  x(1) = 0.5;

  ColumnVector x0 = x;
  const int Nunk = x.length();
  const int Neqn = Nunk;

  ColumnVector sN;      // unconstrained vector representation of eqb u
  sN = ColumnVector(Nunk);

  // ==============================================================
  // Initialize Newton iteration

  ColumnVector      dx(Nunk); // temporary for increments in unknowns
  ColumnVector      Gx(Nunk); // G(x) = f(x)
  ColumnVector   DG_dx(Nunk); // "DG*dx" = 1/eps (G(x + eps dx)
  ColumnVector      xS(Nunk); // stores xN and xH, the Newton and Hookstep unknowns
  ColumnVector      GS(Nunk); // stores GN and GH, GS = G(xS);

  Real rx = 0.0;        // Dennis & Schnabel residual r(x) = 1/2 ||G(x)||^2
  Real gx = 0.0;        // g(x) = L2Norm(G(x))
  Real init_rx = 0.0;
  Real init_gx = 0.0;
  Real prev_rx = 0.0;
  Real prev_gx = 0.0;
  Real snorm   = 0.0;   // norm of hookstep
  Real sNnorm  = 0.0;   // norm of newton step
  Real dxnorm  = 0.0;
  Real xnorm   = 0.0;
  bool stuck = false;

  ofstream osconv((outdir + "convergence.asc").c_str());
  osconv.setf(ios::left);
  osconv << setw(14) << "% L2Norm(G)" << setw(14) << "r(x,T)" << setw(14) << "delta";
  osconv << setw(14) << "L2Norm(dx)" << setw(14) << "L2Norm(sN)" << setw(14) << "L2Norm(sH)"
	 << setw(10) << "ftotal" << setw(10) << "fnewt" << setw(10) << "fhook" << endl;

  cout << setprecision(coutprec);

  ofstream xhos("xhook.asc");
  ofstream xnos("xnewt.asc");

  cout << "Computing G(x) = f(x) - x" << endl;
  G(x, Gx);
  ++fcount_hookstep;

  cout << endl;

  for (int newtonStep = 0; newtonStep<=Nnewton; ++newtonStep) {

    cout << "========================================================" << endl;
    cout << "Newton iteration number " << newtonStep << endl;

    // Compute quantities that will be used multiple times in GMRES loop
    // Gx     = f(x) - x
    dxnorm = L2Norm(dx);
    xnorm = L2Norm(x);
    rx = 0.5*L2Norm2(Gx);
    gx = L2Norm(Gx);

    cout << "delta  == " << delta << endl;

    if (newtonStep == 0) {
      init_gx = gx;
      prev_gx = gx;
      init_rx = rx;
      prev_rx = rx;
    }

    cout << "Current state of Newton iteration:" << endl;
    cout << "fcount_newton  == " << fcount_newton << endl;
    cout << "fcount_hookstep== " << fcount_hookstep << endl;
    cout << "    L2Norm(x)  == " << xnorm << endl;
    cout << "   L2Norm(dx)  == " << dxnorm <<endl;
    cout << "   L2Norm(sH)  == " << snorm <<endl;
    cout << "L2Dist(x,x0)   == " << L2Dist(x, x0) << endl;
    cout << "gx == L2Norm(G(x)) : " << endl;
    cout << "   initial  gx == " << init_gx << endl;
    cout << "   previous gx == " << prev_gx << endl;
    cout << "   current  gx == " << gx << endl;
    cout << "rx == 1/2 Norm2(G(x)) : " << endl;
    cout << "   initial  rx == " << init_rx << endl;
    cout << "   previous rx == " << prev_rx << endl;
    cout << "   current  rx == " << rx << endl;
    cout << "1/2 L2Norm2(G,x) : " << endl;
    cout << "               == " << 0.5*square(gx) << endl;
    cout << "             x == " << x(0) << ' ' << x(1) << endl;
    xnos << x(0) << ' ' << x(1) << endl;

    osconv.setf(ios::left); osconv << setw(14) << gx;
    osconv.setf(ios::left); osconv << setw(14) << rx;
    osconv.setf(ios::left); osconv << setw(14) << delta;
    osconv.setf(ios::left); osconv << setw(14) << dxnorm;
    osconv.setf(ios::left); osconv << setw(14) << sNnorm;
    osconv.setf(ios::left); osconv << setw(14) << snorm;
    osconv.setf(ios::left); osconv << setw(10) << fcount_newton + fcount_hookstep;
    osconv.setf(ios::left); osconv << setw(10) << fcount_newton;
    osconv.setf(ios::left); osconv << setw(10) << fcount_hookstep;
    osconv << endl;

    save(x, outdir + "xnewt" + i2s(newtonStep));
    save(x, outdir + "xbest");

    if (gx < epsSearch) {
      cout << "Newton search converged. Breaking." << endl;
      cout << "L2Norm(G(x)) == " << gx << " < "
	   << epsSearch << " == " << epsSearch << endl;
      break;
    }
    else if (stuck) {
      cout << "Newton search is stuck. Breaking." << endl;
      break;
    }
    else if (newtonStep == Nnewton) {
      cout << "Reached maximum number of Newton steps. Breaking." << endl;
      break;
    }
    if (pausing)
      cfpause();

    prev_gx = gx;
    prev_rx = rx;

    // Set up RHS vector b = -G(x)
    ColumnVector b(Neqn);
    setToZero(b);
    b = Gx; // field2vector(Gu, b, basis);
    b *= -1.0;

    // Estimate G_ij = dG_i/dx_j with finite differences
    //               = 1/eps (G_i(x + eps e_j) - G_i(x))
    Matrix DG(Nunk, Nunk);

    for (int j=0; j<Nunk; ++j) {
      ColumnVector G_x_dxj(Nunk);
      ColumnVector x_dxj = x;
      x_dxj(j) += epsDu;
      G(x_dxj, G_x_dxj);

      for (int i=0; i<Nunk; ++i)
	DG(i,j) = (G_x_dxj(i)- Gx(i))/epsDu;
    }
    fcount_newton += Nunk*Nunk;

    save(DG, "DG");

    SVD svd(DG, SVD::std);

    Matrix V = svd.right_singular_matrix();
    Matrix Ut = svd.left_singular_matrix().transpose();
    DiagMatrix D = svd.singular_values();

    solve(Ut, D, V, b, sN);
    save(DG, "DG");
    save(b, "b");
    sNnorm = L2Norm(sN);
    save(sN, "sN");

    cout << "newton step sN == " << sN(0) << ' ' << sN(1) << endl;
    cfpause();

    // ==================================================================
    // Hookstep algorithm

    cout << "------------------------------------------------" << endl;
    cout << "Beginning hookstep calculations." << endl;

    int Nk = Nunk;

    ColumnVector bh = Ut*b; // b hat

    int hookcount = 0;
    bool have_backup  = false;
    Real backup_snorm = 0.0;
    Real backup_delta = 0.0;
    Real backup_rS    = 0.0;

    ColumnVector backup_dx;
    ColumnVector backup_GS;

    ColumnVector sH;

    //bool delta_has_decreased = false;
    Real deltaMaxLocal = deltaMax;

    // Find a good trust region and the optimal hookstep in it.
    // Quality of trust region
    while (true) {

      cout << "-------------------------------------------" << endl;
      cout << "Newton, hookstep number " << newtonStep << ", "
	   << hookcount++ << endl;
      cout << "delta == " << delta << endl;

      bool hookstep_equals_newtonstep;

      if (L2Norm(sN) <= delta) {
	cout << "Newton step is within trust region: " << endl;
	cout << "L2Norm(sN) == " << L2Norm(sN) << " <= " << delta << " == delta" << endl;
	hookstep_equals_newtonstep = true;
	sH = sN;
      }
      else {
	cout << "Newton step is outside trust region: " << endl;
	cout << "L2Norm(sN) == " << L2Norm(sN) << " > " << delta << " == delta" << endl;
	cout << "Calculate hookstep sH with radius L2Norm(sH) == delta" << endl;

	hookstep_equals_newtonstep = false;

	// This for-loop determines the hookstep. Search for value of mu that
	// produces ||shmu|| == delta. That provides optimal reduction of
	// quadratic model of residual in trust region norm(shmu) <= delta.
	Real mu = 0;
	Real nshmu = 0;
	ColumnVector shmu(Nk); // (s hat)(mu)]

	for (int hookSearch=0; hookSearch<Nhook; ++hookSearch) {
	  for (int i=0; i<Nk; ++i)
	    shmu(i) = bh(i)/(D(i,i) + mu); // research notes
	  nshmu = L2Norm(shmu);
	  Real Phi = nshmu*nshmu - delta*delta;
	  cout << "mu, L2Norm(sH(mu)) == " << mu << ", " << nshmu << endl;

	  // Found satisfactory value of mu and thus shmu and sH s.t. |sH| < delta
	  if (nshmu < delta || (nshmu > (1-deltaFuzz)*delta && nshmu < (1+deltaFuzz)*delta))
	    break;

	  // Update value of mu and try again. Update rule is a Newton search for
	  // Phi(mu)==0 based on a model of form a/(b+mu) for norm(sh(mu)). See
	  // Dennis & Schnabel.
	  else if (hookSearch<Nhook-1) {
	    Real PhiPrime = 0.0;
	    for (int i=0; i<Nk; ++i) {
	      Real di_mu = D(i,i) + mu;
	      Real bi = bh(i);
	      PhiPrime -= 2*bi*bi/(di_mu*di_mu*di_mu);
	    }
	    mu -= (nshmu/delta) * (Phi/PhiPrime);
	  }
	  else {
	    cout << "Couldn't find solution of hookstep optimization eqn Phi(mu)==0" << endl;
	    cout << "This shouldn't happen. It indicates an error in the algorithm." << endl;
	    cout << "Exiting.\n";
	    exit(1);
	  }
	} // search over mu for norm(s) == delta

	cout << "Found hookstep of proper radius" << endl;
	cout << "nshmu == " << L2Norm(shmu) << " ~= " << delta << " == delta " << endl;
	sH = V*shmu;
	cout << "XXXXX: sH == " << sH(0) << ' ' << sH(1) << endl;
      } // else {Newton step outside trust region, so

      // Compute
      // (1) actual residual from evaluation of
      //     rS == r(s+ds) act  == 1/2 (G(x+dx),G(x+dx))
      //
      // (2) predicted residual from quadratic model of r(s)
      //     rP == r(s+ds) pred  == 1/2 (G(x),G(x)) + (G(x), DG dx)
      //
      // (3) slope of r(s)
      //     dr/ds == (r(s + ds) - r(s))/|ds|
      //           == (G(x,T), DG dx /|ds|
      // where ( , ) is Vector inner product V2IP, which equals L2 norm of vector rep

      // (1) Compute actual residual of step, rS
      cout << "Computing residual of hookstep sH" << endl;
      dx = sH;
      dxnorm = L2Norm(dx);
      xS  = x;
      xS += dx;

      cout << "L2Norm(dx) == " << L2Norm(dx) << endl;
      G(xS, GS);
      cout << endl;

      Real rS = 0.5*L2Norm2(GS);       // actual residual of hookstep
      Real Delta_rS = rS - rx;         // improvement in residual
      //cout << "r(s), r(s+ds) == " << rx << ", " << rS << endl;

      // (2) and (3) Compute quadratic model and slope
      cout << "Computing local quadratic model of residual" << endl;
      ColumnVector DG_dx = DG*dx;
      cout << endl;
      // Local quadratic and linear models of residual, based on Taylor exp at current position
      // Quadratic model of r(s+ds): rQ == 1/2 |G(x) + DG dx|^2
      // Linear  model   of r(s+ds): rL == 1/2 (G(x),G(x)) + (G(x), DG dx)

      Real Delta_rL = L2IP(Gx,DG_dx); // rL  - 1/2 (G(x),G(x)) == (G(x), DG dx)
      Real rL = rx + Delta_rL;        // rL == 1/2 (G(x),G(x))  + (G(x), DG dx)

      if (rL >= rx) {
	cout << "error : local linear model of residual is increasing, indicating\n"
	     << "        that the solution to the Newton equations is inaccurate\n"
	     << "exiting." << endl;
	exit(1);
      }

      DG_dx += Gx;
      Real rQ = 0.5*L2Norm2(DG_dx);  // rQ == 1/2 |G(x) + DG dx|^2
      Real Delta_rQ = rQ - rx;       // rQ == 1/2 |(G(x)|^2 - 1/2 |G(x) + DG dx|^2

      snorm = L2Norm(sH);

      // ===========================================================================
      cout << "Determining what to do with current Newton/hookstep and trust region" << endl;

      // Coefficients for non-local quadratic model of residual, based on r(s), r'(s) and r(d+ds)
      // dr/ds == (G(x), DG dx)/|ds|
      Real drds  = Delta_rL/snorm;

      // Try to minimize a quadratic model of the residual
      // r(s+ds) = r(s) + r' |ds| + 1/2 r'' |ds|^2
      Real lambda = -0.5*drds*snorm/(rS - rx - drds*snorm);

      // Compare the actual reduction in residual to the quadratic model of residual.
      // How well the model matches the actual reduction will determine, later on, how and
      // when the radius of the trust region should be changed.
      Real Delta_rS_req  = improvReq*drds*snorm;    // the minimum acceptable change in residual
      Real Delta_rS_ok   = improvOk*Delta_rQ;       // acceptable, but reduce trust region for next newton step
      Real Delta_rS_good = improvGood*Delta_rQ;     // acceptable, keep same trust region in next newton step
      Real Delta_rQ_acc  = abs(improvAcc*Delta_rQ); // for accurate models, increase trust region and recompute hookstep
      Real Delta_rS_accP = Delta_rQ + Delta_rQ_acc; //   upper bound for accurate predictions
      Real Delta_rS_accM = Delta_rQ - Delta_rQ_acc; //   lower bound for accurate predictions

      // Characterise change in residual Delta_rS
      ResidualImprovement improvement;  // fine-grained characterization

      // Place improvement in contiguous spectrum: Unacceptable > Poor > Ok > Good.
      if (Delta_rS > Delta_rS_req)
	improvement = Unacceptable;     // not even a tiny fraction of linear rediction
      else if (Delta_rS > Delta_rS_ok)
	improvement = Poor;             // worse than small fraction of quadratic prediction
      else if (Delta_rS < Delta_rS_ok && Delta_rS > Delta_rS_good)
	improvement = Ok;
      else {
	improvement = Good;             // not much worse or better than large fraction of prediction
	if (Delta_rS_accM <= Delta_rS &&  Delta_rS <= Delta_rS_accP)
	  improvement = Accurate;         // close to quadratic prediction
	else if (Delta_rS < Delta_rL)
	  improvement = NegaCurve;        // negative curvature in r(|s|) => try bigger step
      }

      cout << " rx      == " << setw(w) << rx            << " residual at current position" << endl;
      cout << " rS      == " << setw(w) << rS            << " residual of newton/hookstep" << endl;
      cout << "Delta_rS == " << setw(w) << Delta_rS      << " actual improvement in residual from newton/hookstep" << endl;
      cout << endl;

      if (improvement==Unacceptable)
      cout << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Unacceptable <------ " << endl;
      cout << "            " << setw(w) << Delta_rS_req  << " lower bound for acceptable improvement" << endl;
      if (improvement==Poor)
      cout << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Poor <------" << endl;
      cout << "            " << setw(w) << Delta_rS_ok   << " upper bound for ok improvement." << endl;
      if (improvement==Ok)
      cout << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Ok <------" << endl;
      cout << "            " << setw(w) << Delta_rS_good << " upper bound for good improvement." << endl;
      if (improvement==Good)
      cout << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Good <------" << endl;
      cout << "            " << setw(w) << Delta_rS_accP << " upper bound for accurate prediction." << endl;
      if (improvement==Accurate)
      cout << "Delta_rS == " << setw(w) << Delta_rS      << " ------> Accurate <------" << endl;
      cout << "            " << setw(w) << Delta_rS_accM << " lower bound for accurate prediction." << endl;
      cout << "            " << setw(w) << Delta_rL      << " local linear model of improvement" << endl;
      if (improvement==NegaCurve)
      cout << "Delta_rS == " << setw(w) << Delta_rS      << " ------> NegativeCurvature <------" << endl;

      bool recompute_hookstep = false;

      cout << "lambda       == " << lambda << " is the reduction/increase factor for delta suggested by quadratic model" << endl;
      cout << "lambda*delta == " << lambda*delta << " is the delta suggested by quadratic model" << endl;

      // See comments at end for outline of this control structure

      // Following Dennis and Schnable, if the Newton step lies within the trust region,
      // reset trust region radius to the length of the Newton step, and then adjust it
      // further according to the quality of the residual.
      //if (hookstep_equals_newtonstep)
      //delta = snorm;

      // CASE 1: UNACCEPTABLE IMPROVEMENT
      // Improvement is so bad (relative to gradient at current position) that we'll
      // in all likelihood get better results in a smaller trust region.
      if (improvement == Unacceptable) {
	cout << "Improvement is unacceptable." << endl;

	if (have_backup) {
	  cout << "But we have a backup step that was acceptable." << endl;
	  cout << "Revert to backup step and backup delta, take the step, and go to next Newton iteration." << endl;
	  dx = backup_dx;
	  GS = backup_GS;
	  rS = backup_rS;
	  snorm = backup_snorm;
	  delta = backup_delta;

	  recompute_hookstep = false;
	}
	else {
	  cout << "No backup step is available." << endl;
	  cout << "Reduce trust region by minimizing local quadratic model and recompute hookstep." << endl;
	  deltaMaxLocal = delta;
	  lambda = adjustLambda(lambda, lambdaMin, lambdaRequiredReduction);
	  delta  = adjustDelta(delta, lambda, deltaMin, deltaMax);

	  if (delta > snorm) {
	    cout << "That delta is still bigger than the Newton step." << endl;
	    //cout << "This shouldn't happen. Control structure needs review." << endl;
	    cout << "Reducing delta to half the length of the Newton step." << endl;
	    cout << "  old delta == " << delta << endl;
	    delta = 0.5*snorm;
	    cout << "  new delta == " << delta << endl;
	  }
	  //delta_has_decreased = true;
	  recompute_hookstep = true;
	}
      }

      // CASE 2: EXCELLENT IMPROVEMENT AND ROOM TO GROW
      // Improvement == Accurate or Negacurve means we're likely to get a significant improvement
      // by increasing trust region. Increase trust region by a fixed factor (rather than quadratic
      // model) so that trust-region search is monotonic.
      // Exceptions are
      //   have_backup && backup_rS < rS -- residual is getting wrose as we increase delta
      //   hookstep==newtonstep          -- increasing trust region won't change answer
      //   delta>=deltamax               -- we're already at the largest allowable trust region
      else if ((improvement == NegaCurve || improvement == Accurate)
	       && !(have_backup && backup_rS < rS)
	       && !hookstep_equals_newtonstep
	       && !(delta >= deltaMax)) {

	cout << "Improvement is " << improvement << endl;

	//lambda = adjustLambda(lambda, lambdaMin, lambdaMax);
	Real new_delta = adjustDelta(delta, lambdaMax, deltaMin, deltaMax);

	if (new_delta < deltaMaxLocal) {
	  cout << "Continue adjusting trust region radius delta because" << endl;
	  cout << "Suggested delta has room: new_delta < deltaMaxLocal " << new_delta << " < " << deltaMaxLocal << endl;
	  if (have_backup)
	    cout << "And residual is improving:     rS < backup_sS     " << rS << " < " << backup_rS << endl;

	  cout << "Increase delta and recompute hookstep." << endl;
	  have_backup = true;
	  backup_dx = dx;
	  backup_GS = GS;
	  backup_rS = rS;
	  backup_snorm = snorm;
	  backup_delta = delta;

	  recompute_hookstep = true;
	  cout << " old delta == " << delta << endl;
	  delta = new_delta;
	  cout << " new delta == " << delta << endl;
	}
	else {
	  cout << "Stop adjusting trust region radius delta and take step because the new delta" << endl;
	  cout << "reached a local limit:  new_delta >= deltaMaxLocal " << new_delta << " >= " << deltaMaxLocal << endl;
	  cout << "Reset delta to local limit and go to next Newton iteration" << endl;
	  delta = deltaMaxLocal;
	  cout << "  delta == " << delta << endl;
	  recompute_hookstep = false;
	}
      }

      // CASE 3: MODERATE IMPROVEMENT, NO ROOM TO GROW, OR BACKUP IS BETTER
      // Remaining cases: Improvement is acceptable: either Poor, Ok, Good, or
      // {Accurate/NegaCurve and (backup is superior || Hookstep==NewtonStep || delta>=deltaMaxLocal)}.
      // In all these cases take the current step or backup if better.
      // Adjust delta according to accuracy of quadratic prediction of residual (Poor, Ok, etc).
      else {
	cout << "Improvement is " << improvement <<  " (some form of acceptable)." << endl;
	cout << "Stop adjusting trust region and take a step because" << endl;
	cout << "Improvement is merely Poor, Ok, or Good : " << (improvement == Poor || improvement == Ok || improvement == Good) << endl;
	cout << "Newton step is within trust region      : " << hookstep_equals_newtonstep << endl;
	cout << "Backup step is better                   : " << (have_backup && backup_rS < rS) << endl;
	cout << "Delta has reached local limit           : " << (delta >= deltaMax) << endl;

	recompute_hookstep = false;

	// Backup step is better. Take it instead of current step.
	if (have_backup && backup_rS < rS) {
	  cout << "Take backup step and set delta to backup value." << endl;
	  dx = backup_dx;
	  GS = backup_GS;
	  rS = backup_rS;

	  cout << "  old delta == " << delta << endl;
	  snorm = backup_snorm;
	  delta = backup_delta;
	  cout << "  new delta == " << delta << endl;
	}

	// Current step is best available. Take it. Adjust delta according to Poor, Ok, etc.
	// Will start new monotonic trust-region search in Newton-hookstep, so be more
	// flexible about adjusting delta and use quadratic model.
	else {
	  if (have_backup) {
	    cout << "Take current step and keep current delta, since it's produced best result." << endl;
	    cout << "delta == " << delta << endl;
	    // Keep current delta because current step was arrived at through a sequence of
	    // adjustments in delta, and this value yielded best results.
	  }
	  else if (improvement == Poor) {
	    cout << "Take current step and reduce delta, because improvement is poor." << endl;
	    lambda = adjustLambda(lambda, lambdaMin, 1.0);
	    delta  = adjustDelta(delta, lambda, deltaMin, deltaMax);
	  }
	  else if (improvement == Ok || hookstep_equals_newtonstep || (delta >= deltaMax)) {
	    cout << "Take current step and leave delta unchanged, for the following reasons:" << endl;
	    cout << "improvement            : " << improvement << endl;
	    cout << "|newtonstep| <= delta  : " << (hookstep_equals_newtonstep ? "true" : "false") << endl;
	    cout << "delta >= deltaMax      : " << (delta >= deltaMax    ? "true" : "false") << endl;
	    //cout << "delta has decreased    : " << (delta_has_decreased ? "true" : "false") << endl;
	    cout << "delta == " << delta << endl;
	  }
	  else {  // improvement == Good, Accurate, or NegaCurve and no restriction on increasing apply
	    cout << "Take step and increase delta, if there's room." << endl;
	    lambda = adjustLambda(lambda, 1.0, lambdaMax);
	    delta  = adjustDelta(delta, lambda, deltaMin, deltaMax);
	  }
	}
      }
      //cout << "Recompute hookstep ? " << (recompute_hookstep ? "yes" : "no") << endl;

      if (pausing)
	cfpause();

      if (recompute_hookstep)
	continue;
      else
	break;
    }

    cout << "Taking best step and continuing Newton iteration." <<endl;
    x += dx;
    dxnorm = L2Norm(dx);
    xnorm  = L2Norm(x);

    Gx = GS;

    cout << "L2Norm(G(x)) == " << L2Norm(Gx) << endl;
    cout << "rx           == " << rx << endl;
    cout << "L2Norm(sN)   == " << L2Norm(sN) << endl;
    cout << "L2Norm(sH)   == " << L2Norm(sH) << endl;
    cout << "L2Norm(dx)   == " << dxnorm << endl;

    xhos << x(0) << ' ' << x(1) << endl;
  }
}

// G(x) = (x0^2 + x1^2 - 2, exp(x0-1) + x1^2 - 2), has zero at x = (1,1)
// G(x) = x0 + x1 -2 , x0 - x1 -2
void G(const ColumnVector& x, ColumnVector& Gx) {
  Real x0 = x(0);
  Real x1 = x(1);

  Gx(0) = exp(x0-1) + x1*x1 - 2;
  Gx(1) = x0*x0 + x1*x1 - 2;

  //Gx(0) = x0 + x1 - 2;
  //Gx(1) = x0 - x1 - 2;

  //Gx(0) = x0 - 1;
  //Gx(1) = x1 - 1;
}

ostream& operator<<(ostream& os, HookstepPhase p) {
  if      (p == ReducingDelta)   os << "ReducingDelta";
  else if (p == IncreasingDelta) os << "IncreasingDelta";
  else                           os << "Finished";
  return os;
}
ostream& operator<<(ostream& os, ResidualImprovement i) {
  if      (i == Unacceptable) os << "Unacceptable";
  else if (i == Poor)         os << "Poor";
  else if (i == Ok)           os << "Ok";
  else if (i == Good)         os << "Good";
  else if (i == Accurate)     os << "Accurate";
  else                        os << "NegativeCurvature";
  return os;
 }

Real adjustDelta(Real delta, Real deltaRate, Real deltaMin, Real deltaMax) {
  cout << "  old delta == " << delta << endl;

  // Check to see if we're already running with delta == deltaMin.
  // If so, and if deltaRate calls for furhter reduction, search has
  // ground to a halt. Error and exit.
  if (delta <= 1.00001*deltaMin && deltaRate < 1.0) {
    cout << "    delta == " << delta << endl;
    cout << "delta new == " << delta*deltaRate << endl;
    cout << "delta min == " << deltaMin << endl;
    cout << "Sorry, can't go below deltaMin. Exiting." << endl;
    exit(1);
  }

  delta *= deltaRate;

  if (delta < deltaMin) {
    delta = deltaMin;
    cout << "delta bottomed out at deltaMin" << endl;
  }
  else if (delta > deltaMax) {
    delta = deltaMax;
    cout << "delta topped out at deltaMax" << endl;
  }
  cout << "  new delta == " << delta << endl;
  return delta;
}

Real adjustLambda(Real lambda, Real lambdaMin, Real lambdaMax) {
  if (lambda < lambdaMin) {
    cout << "lambda == " << lambda << " is too small. Resetting to " << endl;
    lambda = lambdaMin;
    cout << "lambda == " << lambda << endl;
  }
  else if (lambda < lambdaMin) {
    cout << "lambda == " << lambda << " is too large. Resetting to " << endl;
    lambda = lambdaMax;
    cout << "lambda == " << lambda << endl;
  }
  return lambda;
}

void save(const ColumnVector& x, const string& filebase) {
  string filename(filebase + ".asc");
  ofstream os(filename.c_str());
  os << setprecision(17);
  for (int j=0; j<x.length(); ++j)
      os << x(j) << '\n';
}

Real L2IP(const ColumnVector& u, const ColumnVector& v) {
  if (u.length() != v.length()) {
    cerr << "error in L2IP(ColumnVector, ColumnVector) : vector length mismatch" << endl;
    exit(1);
  }
  Real sum = 0.0;
  for (int i=0; i<u.length(); ++i)
    sum += u(i) * v(i);
  return sum;
}

void solve(const Matrix& Ut, const DiagMatrix& D, const Matrix& V, const ColumnVector& b,
	   ColumnVector& x) {

  ColumnVector bh = Ut*b;
  const Real eps = 1e-12;
  for (int i=0; i<D.rows(); ++i) {
    if (abs(D(i,i)) > eps)
      bh(i) *= 1.0/D(i,i);
    else
      bh(i)  = 0.0;
  }
  x = V*bh;
}



// The following is meant as a guide to the structure and notation
// of the above code. See Divakar Viswanath, "Recurrent motions
// within plane Couette turbulence", J. Fluid Mech. 580 (2007),
// http://arxiv.org/abs/physics/0604062 for a higher-level presentation
// of the algorithm.
//
// I've biased the notation towards conventions from fluids and from
// Predrag Cvitanivic's ChaosBook, and away from Dennis & Schnabel and
// Trefethen. That is, f stands for the finite-time map of plane Couette
// dynamics, not the residual being minimized.
//
// Let
//  G(x) = 0 : the equation we're solving
//  r(x,t) = 1/2 L2Norm2(G(x)),
//
// We want to find zeros of G(x) or equiv minimum of r(x).
// Note that the residual is reported to the user in the form
//   residual(x) == L2Norm(G(x)) == sqrt(2*r(x))
//
// The Newton algorithm for solving G(x) = 0 is
//
// Let x* be the solution, i.e. G(x*) = 0
// Suppose we start with an initial guess x near x*.
// Let x* = x + dx
// Then G(x*) = G(u+du)
//          0 = G(x) + DG(x) dx + O(|dx|^2)
// Drop h.o.t and solve
//   DG(x) dx = -G(x)
// Then start with new guess x' = x+dx and iterate.
//
// We estimate DG_{ij} = dG_i/dx_j = 1/eps (G_i(x + eps e_j) - G_i(x))
//
// Hookstep algorithm: If the Newton step dx does not decrease
// the residual L2Norm(G(x+dx)) sufficiently, we obtain the a smaller step
// s by minimizing || DG s - b ||^2 subject to ||s||^2 <= delta^2, rather
// solving DG s = b directly.

// Do minimization of | DG - b | via SVD of DG = U D V^T.
//   | DG s - b | = | U D V^T s - b   |
//                = | D V^T s - U^T b |
//                = | D sh - bh |
// where sh = V^T s and bh = U^T b. So we need to minimize |D sh - bh|
// subject to constraint |sh| <= delta. (Pure Newton step has solution
// s = sN = V D^{-1} Ut b, by the way.)

// From Divakar Viswanath (personal communication):
//
// Since D is diagonal D_{i,i} = d_i, the constrained minimization
// problem can be solved easily with lagrange multipliers:
// Let sh = sh(mu) where
//   sh_i = (bh_i d_i)/(d^2_i + mu)
// for the mu such that ||sh||^2 = delta^2. The value of mu can be
// found with a 1d Newton search for zeros of
//   Phi(mu) = ||sh(mu)||^2 - delta^2
// A straight Newton search a la mu -= Phi(mu)/Phi'(mu) is suboptimal
// since Phi'(mu) -> 0 as mu -> infty with Phi''(mu) > 0 everywhere, so
// we use a slightly modified update rule for the Newton search. Please
// refer to Dennis and Schnabel regarding that. Then, given the solution
// sh(mu), we compute the hookstep solution s from
//   V^t s = sh
//   sh = V*sh;
