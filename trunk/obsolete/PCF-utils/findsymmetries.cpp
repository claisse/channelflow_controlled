#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("find the symmetries satisfied by a given field");

  ArgList args(argc, argv, purpose);
  const bool verbose = args.getflag("-v",  "--verbose", "print error of each checked symmetry");
  const int Nx       = args.getint ("-nx", "--nx", 4, "check x-trans over set {0,nx-1}/nx Lx");
  const int Nz       = args.getint ("-nz", "--nz", 4, "check z-trans over set {0,nz-1}/nz Lz");
  const Real eps     = args.getreal("-e", "--eps", 1e-6, "cut-off for symmetry error");
  const string uname = args.getstr (1, "<flowfield>", "a flowfield");
  args.check();

  FlowField u(uname);
  const Real unorm = L2Norm(u);
  const Real rNx = Real(Nx);
  const Real rNz = Real(Nz);

  vector<FieldSymmetry> symm;

  for (int nx=0; nx<Nx; ++nx) {
    for (int nz=0; nz<Nz; ++nz) {
      for (int sx=1; sx >= -1; sx -= 2)
	for (int sz=1; sz >= -1; sz -= 2) {

	  // skip identity
	  if (nx==0 && nz==0 && sx==1 && sz==1)
	    break;

	  FieldSymmetry s(sxy, sxy, sz, nx/rNx, nz/rNz);
	  Real err = L2Dist(u,s(u))/unorm;
	  if (verbose)
	    cout << "s == " << s << "  " << fuzzyless(err,eps)
		 << "   L2Dist(u,su)/L2Norm(u) == " << err << endl;

	  if (err < eps)
	    symm.push_back(s);
	}
    }
  }
  cout << "satisfied " <<  symm.size() << " symmetries to eps == " << eps << endl;
  ofstream os((removeSuffix(uname, ".ff") + ".symms").c_str());
  os << setprecision(17);
  os << symm.size() << '\n';
  for (uint n=0; n<symm.size(); ++n) {
    os   << symm[n] << '\n';
    cout << symm[n] << '\n';
  }
}
