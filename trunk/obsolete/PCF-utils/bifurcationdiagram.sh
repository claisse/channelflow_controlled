#!/bin/bash

alpha=*
gamma=*
Reynolds=*
prefix=

while getopts ":a:g:R:l:p:" Option
do
  case $Option in
      a ) alpha=$OPTARG ;;
      g ) gamma=$OPTARG ;;
      R ) Reynolds=$OPTARG;;
      l ) state=$OPTARG;;
      p ) prefix=$OPTARG;;
  esac
done
shift $(($OPTIND - 1))

filter=a${alpha}_g${gamma}_R${Reynolds}

for dir in `ls -d $filter`; do
    statef=${dir}/${state}/${prefix}${state}.ff
    if [ -e $statef ] 
	then
	thisa=`echo $dir | sed -e "s/a\([^_]*\).*/\1/"`
	thisg=`echo $dir | sed -e "s/[^g]*g\([^_]*\).*/\1/"`
	thisR=`echo $dir | sed -e "s/[^R]*R\(.*\)/\1/"`
	D=`dissipation.x $statef`
	printf "${thisa}\t${thisg}\t${thisR}\t${D}\n"
    fi
done
