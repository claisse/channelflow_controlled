#include <iostream>

#include "utilities.h"
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "octave/dMatrix.h"

using namespace std;
using namespace channelflow;

Real interpolate(const Real* R, const Real* u, const Real Rguess, const bool branch, const Real eps);

int main(int argc, char** argv)
{
  string purpose="Given 3 equilibria on a saddle node bifurcation branch guess one on the other branch";

  ArgList args(argc, argv, purpose);

  const Real   R1      = args.getreal ("-r1","--r1","Bifucation parameter value for first field");
  const Real   R2      = args.getreal ("-r2","--r2","Bifucation parameter value for second field");
  const Real   R3      = args.getreal ("-r3","--r3","Bifucation parameter value for third field");
  const Real   Rguess  = args.getreal ("-rg","--rguess","Bifucation parameter value for guess field");
  const bool branch = args.getflag   ("-ub","--ubranch","If set, give upper otherwise return lower branch");
  const Real eps       = args.getreal("-e","--epsilon",1e-3, "Threshold for quadratic interpolation");
  const bool s1        = args.getflag("-s1","--s1","Enforce s1 symmetry");
  const bool s2        = args.getflag("-s2","--s2","Enforce s2 symmetry");
  const bool s3        = args.getflag("-s3","--s3","Enforce s3 symmetry");
  const string out     = args.getstr ("-o","--outfile","Location to store guess");
  const string u0name  = args.getstr (3, "<flowfield>", "input field #1");
  const string u1name  = args.getstr (2, "<flowfield>", "input field #2");
  const string u2name  = args.getstr (1, "<flowfield>", "input field #3");

  args.check();
  args.save("./");
  FlowField u0(u0name);
  FlowField u1(u1name);
  FlowField u2(u2name);

  FlowField uguess(u0);

  const int Ny = u0.Ny();
  const Real Lx = u0.Lx();
  const Real Lz = u0.Lz();
  const Real a = u0.a();
  const Real b = u0.b();

  const int kxmax = u0.kxmax() / 3;
  const int kzmax = u0.kzmax() / 3;

  FieldSymmetry ss1( 1, 1,-1, 0.5, 0.0);
  FieldSymmetry ss2(-1,-1, 1, 0.5, 0.5);
  FieldSymmetry ss3(-1,-1, -1, 0.0, 0.5);
  vector<FieldSymmetry> symms;
  if(s1)
    symms.push_back(ss1);
  if(s2)
    symms.push_back(ss2);
  if(s3)
    symms.push_back(ss3);

  cout << "constructing basis set. this takes a long time..." << flush;
  vector<RealProfileNG> basis = realBasisNG( Ny, kxmax, kzmax, Lx, Lz, a, b);
  if(s1 || s2 || s3)
    selectSymmetries(basis, symms);

  orthonormalize(basis);
  cout << "done" << endl;

  FlowField tmp(u0);
  uguess.setToZero();
  for(vector<RealProfileNG>::const_iterator i = basis.begin(); i!=basis.end(); ++i)
    {
      const Real R[3] = {R1, R2, R3};
      const Real u[3] = {L2IP(u0,*i), L2IP(u1,*i), L2IP(u2,*i)};
      tmp.setToZero();
      tmp += *i;
      const Real ai = interpolate(R, u, Rguess, branch, eps);
      tmp *= ai;
      uguess += tmp;
    }

  uguess.makeSpectral();

  uguess.zeroAliasedModes();
  uguess.binarySave(out);
}

Real interpolate(const Real* R, const Real* u, const Real Rguess, const bool branch, const Real eps)
{
  Matrix Umat(3,3);
  ColumnVector Rvec(3);
  ColumnVector coeffs(3);

  if(abs(u[2]-u[0]) < eps)
    {
      Real dR = R[2] - R[0];
      return (u[0]-u[2])*(Rguess-R[0]) / dR + u[0];
    }

  for(int i=0; i<3; ++i)
    Rvec(i) = R[i];

  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      Umat(i,j)= pow(u[i],2-j);

  coeffs = Umat.solve(Rvec);
  const Real a = coeffs(0);
  const Real b = coeffs(1);
  const Real c = coeffs(2) - Rguess;

  const Real b24ac = b*b - 4*a*c;
  //  cout << a << ' ' << b << ' ' << c << endl;
  if(branch)
    return (-b + sqrt(b24ac)) / (2 * a);
  else
    return (-b - sqrt(b24ac)) / (2 * a);

}
