#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/CColVector.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/EIG.h>
#include <octave/dbleSVD.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "channelflow/periodicfunc.h"
#include "utilities.h"

// This program is a Newton-hookstep search code for PCF (relative) 
// equilibria and periodic orbits that uses GMRES to solve the Newton-step 
// and a hookstep trust-region algorithm to prevent the Newton steps from 
// extending outside the domain of validity of the linear Taylor expansion.
// The GMRES algorithm is modeled on the discussion in Lecture 35 of 
// Trefethen and Bau's "Numerical Linear Algebra". The hookstep algorithm 
// is based on section 6.4.2 of Dennis and Schnabel's "Numerical Methods 
// for Unconstrained Optimization and Nonlinear Equations." 
//
// The accuracy of this program could be improved by substituting DNS 
// computations linearized about u and t. 
// 
// See end of file for a detailed description of the algorithm and notation.
//
// John Gibson, Tue Jun 19 17:46:00 EDT 2007

using namespace std;
using namespace channelflow;

// L2Norm2(field2vector(u));
Real V2Norm2(const FlowField& u);
Real V2Norm(const FlowField& u);
Real V2IP(const FlowField& u, const FlowField& v);

void savewave(FlowField& u, const string& name);

// f^T(u) = time-T DNS integration of u
void f(const FlowField& u, Real T, FlowField& f_u, Real Reynolds, DNSFlags& flags, 
       TimeStepParams dtp, int& fcount);

// f^(n.dt)(u) == time-(n.dt) DNS integration of u
void f(const FlowField& u, int n, Real dt, FlowField& f_u, Real Reynolds, 
       DNSFlags& flags);

// G(u,T) = (tau f^T(u) - u) 
void G(const FlowField& u, Real T, FlowField& f_u, FlowField& G_u, const FieldSymmetry& tau,
       Real Reynolds, DNSFlags& flags, TimeStepParams dtp, int& fcount);

void params2timestep(TimeStepParams dtp, Real T, int& N, TimeStep& dt);

enum HookstepPhase {ConstantDelta, ReducingDelta, IncreasingDelta, Finished};
ostream& operator<<(ostream& os, HookstepPhase p);

enum ResidualImprovement {Unacceptable, Poor, Ok, Good, Accurate, NegaCurve}; 
ostream& operator<<(ostream& os, ResidualImprovement i);


void putBetween(Real& delta, Real deltaMin, Real deltaMax);

int main(int argc, char* argv[]) {

  string purpose("Newton-Krylov-hookstep search for (relative) periodic orbit in U_S");
  ArgList args(argc, argv, purpose);

  const Real pertsmooth= args.getreal("-sm",   "--smoothness", 0.3, "smoothness of perturbations in initial guess");
  const Real pertmagn  = args.getreal("-ma",   "--magnitude",  0.1, "magnitude of perturbations in initial guess");
  const Real epsKrylov = args.getreal("-ek",   "--epsKrylov", 1e-14, "min. condition # of Krylov vectors");
  const Real epsGMRES  = args.getreal("-eg",   "--epsGMRES",  1e-3,  "convergence criterion for Ax=b on GMRES");
  const Real epsSearch = args.getreal("-es",   "--epsSearch",    1e-14, "stop search if L2Norm(1/T (f^T(u)-u)) < epsEQB");
  const Real epsDu     = args.getreal("-edu",  "--epsDuLinear", 1e-7,  "relative size of du to u in linearization");
  const Real epsDt     = args.getreal("-edt",  "--epsDtLinear", 1e-7,  "size of dT in linearization of f^T about T");
  //const Real epsSymm   = args.getreal("-es", "--epsSymmetry", 1e-13, "maximum symmetry discrepancy in u0");
  const Real TscaleRel = args.getreal("-Tsc",  "--Tscale",    1,     "scale time in hookstep: |T| = Ts*|u|/L2Norm(du/dt)");
  const int  Nnewton   = args.getint( "-Nn",   "--Nnewton",   10,    "max number of Newton steps ");
  const int  Ngmres    = args.getint( "-Ng",   "--Ngmres",    40,    "max number of GMRES iterations per Newton step");
  const int  Nhook     = args.getint( "-Nh",   "--Nhook",     20,    "max number of hookstep iterations per Newton");
  /***/ Real delta     = args.getreal("-d",    "--delta",     0.10,  "initial radius of trust region");
  const bool fixedDelta= args.getflag("-fd",   "--fixedDelta",      "don't adjust trust region radius");
  const Real deltaMin  = args.getreal("-dmin", "--deltaMin",  1e-6,  "stop if radius of trust region gets this small");
  const Real deltaMax  = args.getreal("-dmax", "--deltaMax",  1,   "never let radius of trust region get this big");
  const Real deltaFuzz = args.getreal("-df",   "--deltaFuzz", 0.01,  "accept steps within (1+/-deltaFuzz)*delta");
  const Real deltaRate = args.getreal("-dr",   "--deltaRate", 2.0,   "delta *= deltaRate if hookstep is accurate");
  const Real improvReq = args.getreal("-irq",  "--improveReq",  0.01, "See README");
  const Real improvPoor= args.getreal("-ipr",  "--improvePoor",  0.10, "used in updating trust region. See README.");
  const Real improvGood= args.getreal("-igd",  "--improveGood",   0.25, "used in updating trust region. See README.");
  const Real improvAcc = args.getreal("-iac",  "--improveAcc",  0.10, "used in refining hookstep. See README.");
  const bool pureNewton= args.getflag("-pn",   "--pureNewton",   "do pure Newton search instead of hookstep");
  /***/ Real T         = args.getreal("-T",    "--maptime", 10.0, "(initial) integration time for DNS map f^T(u)");
  const bool Tsearch   = args.getbool("-Ts",   "--Tsearch", true, "include period T as a search variable");
  const Real Reynolds  = args.getreal("-R",    "--Reynolds", 400, "Reynolds number");
  const bool dtVariable= args.getbool("-vdt",  "--variableDt", true, "adjust dt to keep CFLmin<=CFL<CFLmax");
  const Real dt0       = args.getreal("-dt",   "--timestep",  0.02, "(initial) integration timestep");
  const Real dtmin     = args.getreal("-dtmin",  "--dtmin", 0.001, "minimum timestep");
  const Real dtmax     = args.getreal("-dtmax",  "--dtmax", 0.04,  "maximum timestep");
  const Real CFLmin    = args.getreal("-CFLmin", "--CFLmin", 0.40, "minimum CFL number");
  const Real CFLmax    = args.getreal("-CFLmax", "--CFLmax", 0.60, "maximum CFL number");
  const string stepstr = args.getstr("-ts", "--timestepping", "SBDF3", "timestep algorithm: See README.");
  const string nonlstr = args.getstr("-nl", "--nonlinearity", "Rotational", "nonlinearity method: See README.");
  //const bool dudxortho = args.getbool("-xc", "--dudxConstraint", false,"require orthogonality of step to dudx and dudz");
  const bool dudtortho = args.getbool("-tc", "--dudtConstraint", true,"require orthogonality of step to dudt");
  const Real orthoscale= args.getreal("-os", "--orthoScale", 1, "rescale orthogonality constraint");
  const bool pausing   = args.getflag("-p", "--pausing", "pause between Newton steps");
  const string outdir  = args.getpath("-o", "--outdir", "./", "output directory"); 
  //const string uname   = args.getstr(1, "<flowfield>", "initial guess for Newton search");

  const Real deltaShrinkMin = 0.1;
  const Real deltaShrinkMax = 0.5;

  args.check();
  args.save(outdir);

  DNSFlags flags; 
  flags.timestepping = s2stepmethod(stepstr);
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = s2nonlmethod(nonlstr);
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;
  flags.baseflow     = PlaneCouette;
  flags.verbosity    = Silent;
  cout << "DNSFlags == " << flags << endl;

  int fcount_forget = 0;
  int fcount_newton = 0;
  int fcount_hookstep = 0;

  // Used to communicate timestep parameters to integration functions.
  cout << "dt0    == " << dt0 << endl;
  cout << "dtmin  == " << dtmin << endl;
  cout << "dtmax  == " << dtmax << endl;
  cout << "CFLmin == " << CFLmin << endl;
  cout << "CFLmax == " << CFLmax << endl;
  TimeStepParams dtparams(dt0, dtmin, dtmax, CFLmin, CFLmax, dtVariable);

  // Define gridsize
  const int Nx = 32;
  const int Ny = 8;
  const int Nz = 8;
  const Real Lx = 4;
  const Real ya = -1;
  const Real yb = 1;
  const Real Lz = 2;
  
  Vector x = periodicpoints(Nx, Lx);
  x.save("x");
  // 
  const Real Tsoln = 8.0;
  FlowField usoln(Nx,Ny,Nz,3,Lx,Lz,ya,yb,Physical,Physical);
  FlowField ufinal(Nx,Ny,Nz,3,Lx,Lz,ya,yb);
  FlowField uguess(Nx,Ny,Nz,3,Lx,Lz,ya,yb);
  FlowField u(Nx,Ny,Nz,3,Lx,Lz,ya,yb);

  for (int nx=0; nx<Nx; ++nx) {
    Real x = u.x(nx);
    Real cx = sin(2*pi*x/Lx);
    for (int ny=0; ny<Ny; ++ny) {
      Real y = u.y(ny);
      Real cy = 1.0 - y*y;
      for (int nz=0; nz<Nz; ++nz) 
	usoln(nx,ny,nz,2) = cx*cy;
    }
  }
  usoln.makeSpectral();

  cout << "divNorm(usoln) == " << divNorm(usoln) << endl;
  cout << " bcNorm(usoln) == " <<  bcNorm(usoln) << endl;
  
  uguess.addPerturbations(usoln.kxmaxDealiased(), usoln.kzmaxDealiased(), pertmagn, pertsmooth);
  uguess += usoln;
  u = uguess;

  f(usoln, Tsoln, ufinal, Reynolds, flags, dtparams, fcount_forget);
  ufinal.binarySave("ufinal");

  // Determine which of four half-cell translations tau minimizes 1/2 || tau f^T(u) - u ||^2
  FieldSymmetry tau; // identity
  {
    FlowField fusoln;
    FlowField Gusoln;
    G(usoln, Tsoln, fusoln, Gusoln, tau, Reynolds, flags, dtparams, fcount_forget);

    save(Tsoln, "Ts"); 
    savewave(uguess, "u0");
    savewave(usoln,  "us");
    savewave(ufinal, "fus");

    cout << "L2Dist(f(usoln), ufinal) == " << L2Dist(fusoln, ufinal) << endl;
    cout << "L2Norm(G(usoln, Tsoln)   == " << L2Norm(Gusoln) << endl;
    //cfpause();
  }
  
  ColumnVector sN;      // Newton step in vector form, sN = (du, dT) 
  field2vector(u, sN);
  
  cout << "sN.length() == " << sN.length() << endl;
  // Mcon == number of constraints
  // Meqn == number of equations (total)
  // Munk == number of unknowns
  const int Teqn  = sN.length();
  const int Tunk  = sN.length();
  const int Munk  = sN.length() + (Tsearch ? 1 : 0);   
  const int Meqn  = sN.length() + (Tsearch ? 1 : 0);   
  const int Ngmres2 = (Munk < Ngmres) ? Munk : Ngmres;

  cout << Meqn << " equations" << endl;
  cout << Munk << " unknowns" << endl;
  cout << "Teqn  == " << Teqn << endl;
  cout << "Tunk  == " << Tunk << endl;

  sN = ColumnVector(Munk);

  // ==============================================================
  // Initialize Newton iteration 

  FlowField      du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // Newton step for orbit velocity field
  FlowField   f_u_T(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // f(u,T) = f^T(u) time-T DNS integration
  FlowField   G_u_T(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // G(u,T) = f^T(u) - u
  FlowField dfuT_dT(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // df(u,T)/dT = 1/dt (f^{T+dt}(u) - f^T(u))
  FlowField   DG_du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // "DG*du" = D[u] G(u,T) du = 1/e (G(u+e.du,T)
  FlowField   DG_dT(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // "DG*dT" = D[T] G(u,T) dT = df(u,T)/dT dT
  FlowField       p(Nx, Ny, Nz, 1, Lx, Lz, ya, yb); // pressure field for time integration
  FlowField     tmp(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); 
  FlowField    tmp2(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); 
  FlowField      uS(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // stores uN and uH, the Newton and Hookstep velcoity field
  FlowField      fS(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // stores fN and fH, fS = f(uS,tS)
  FlowField      GS(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // stores GN and GH, GS = G(uG,tG)
  
  Real dT = 0.0;                                    // Newton step for orbit period T
  Real tS = 0.0;

  ChebyCoeff U(Ny, ya, yb, Spectral);
  U[1] = 1;

  Real ruT = 0.0;             // Dennis & Schnabel residual r(u,T) = 1/2 ||G(u,T)||^2 == 1/2 VNorm2(G(u,T))
  Real guT = 0.0;        // g(u,T) = L2Norm(G(u,T)) 
  Real init_ruT = 0.0;
  Real init_guT = 0.0;
  Real prev_ruT = 0.0;
  Real prev_guT = 0.0;
  Real gmres_residual = 0.0;
  Real snorm   = 0.0;
  Real dunorm  = 0.0;
  Real unorm   = 0.0;
  bool stuck = false;


  ofstream osconv((outdir + "convergence.asc").c_str());
  osconv.setf(ios::left);
  osconv << setw(10) << "% fcount"
	 << setw(16) << "L2Norm(G(u,T))"
	 << setw(16) << "L2Norm(s)"
	 << setw(16) << "dT"
	 << setw(16) << "delta" << endl;
  cout << setprecision(10);

  cout << "Computing G(u,T) = tau f(u,T) - u" << endl; 
  G(u, T, f_u_T, G_u_T, tau,Reynolds, flags, dtparams, fcount_newton);

  for (int newtonStep = 0; newtonStep<Nnewton+1; ++newtonStep) {
    
    cout << "========================================================" << endl;
    cout << "Newton iteration number " << newtonStep << endl;
    
    // Compute quantities that will be used multiple times in GMRES loop
    // f_u_T = f^T(u)
    // G_u_T = tau f^T(u) - u
    // dfuTdT  = 1/(dt) (f^(T+dt)(u) - f^(T)(u))
    // dudt = 1/(dt) (f^(dt)(u) - u)
    //      = tangent vector to flow
    // edudx = tangent vector of x translation
    // edudz = tangent vector of z translation

    dunorm = L2Norm(du);
    unorm = L2Norm(u);
    ruT = 0.5*V2Norm2(G_u_T); 
    guT = L2Norm(G_u_T);

    cout << "Computing normalized dudt, edudx, edudz" << endl; 
    // Calculate three unit vectors to which du must be orthogonal
    FlowField edudx = xdiff(u);
    FlowField edudz = zdiff(u);
    FlowField edudt(u);
    f(u, 1, -epsDt, tmp, Reynolds, flags);    // using dt=epsDt timestep
    f(u, 1, epsDt, edudt, Reynolds, flags);   // using dt=epsDt timestep
    edudt -= tmp;
    edudt *= 1.0/(2*epsDt);

    // Set Tscale so that T*Tscale ~= TscaleRel*(size of coeffs in u);
    // O(t)*Tscale = O(u) => Tscale = u/t
    //const Real Tscale = TscaleRel*V2Norm(edudt);
    const Real Tscale = TscaleRel;
    cout << "delta  == " << delta << endl;
    cout << "Tscale == " << Tscale << endl;
    edudt *= 1.0/L2Norm(edudt); 
    edudx *= 1.0/L2Norm(edudx);
    edudz *= 1.0/L2Norm(edudz);

    //cout << "Computing f(u,T)" << endl; 
    //cout << "Computing G(u,T) = tau f(u,T) - u" << endl; 
    //G(u, T, f_u_T, G_u_T, tau, Reynolds, flags, dtparams, fcount_newton);

    cout << "Computing df(u,T)/dT" << endl; 
    f(f_u_T, 1, -epsDt, tmp, Reynolds, flags);
    f(f_u_T, 1,  epsDt, dfuT_dT, Reynolds, flags);
    dfuT_dT -= tmp;
    dfuT_dT *= 1.0/(2*epsDt);
    
    savewave(u,  "un");
    savewave(du, "du");
    savewave(f_u_T, "fun");
    save(T, "Tn");
    save(dT, "dT");
   
    if (newtonStep == 0) {
      init_guT = guT;
      prev_guT = guT;
      init_ruT = ruT;
      prev_ruT = ruT;
    }
 
    FlowField uU(u);
    uU += U;

    cout << "Current state of Newton iteration:" << endl;
    //cout << "dissip (u)     == " << dissipation(u) << endl;
    //cout << "forcing(u)     == " << forcing(u) << endl;
    //cout << "energy (u)     == " << 0.5*square(unorm) << endl;
    //cout << "dissip (u+U)   == " << dissipation(uU) << endl;
    //cout << "forcing(u+U)   == " << forcing(uU) << endl;
    //cout << "energy (u+U)   == " << 0.5*L2Norm2(uU) << endl;
    cout << "fcount_newton  == " << fcount_newton << endl;
    cout << "fcount_hookstep== " << fcount_hookstep << endl;
    cout << "L2Norm(u)      == " << unorm << endl;
    cout << "L2Norm(du)     == " << dunorm <<endl;
    cout << "L2Norm(sH)     == " << snorm <<endl;
    cout << "        T      == " << T << endl;
    cout << "       dT      == " << dT << endl;
    cout << "L2Dist(u,u0)   == " << L2Dist(u,uguess) << endl;
    cout << "L2Dist(u,soln) == " << L2Dist(u,usoln) << endl;
    cout << "L2Dist(fuT,uf) == " << L2Dist(f_u_T,ufinal) << endl;
    cout << "guT == L2Norm(G(u,T)) : " << endl;
    cout << "  initial  guT == " << init_guT << endl;
    cout << "  previous guT == " << prev_guT << endl;
    cout << "  current  guT == " << guT << endl;
    cout << "ruT == 1/2 V2Norm2(G(u,T)) : " << endl;
    cout << "  initial  ruT == " << init_ruT << endl;
    cout << "  previous ruT == " << prev_ruT << endl;
    cout << "  current  ruT == " << ruT << endl;
    cout << "1/2 L2Norm2(G,u,T) : " << endl;
    cout << "               == " << 0.5*square(guT) << endl;

    osconv.setf(ios::left); osconv << setw(10) << fcount_newton + fcount_hookstep;
    osconv.setf(ios::left); osconv << setw(16) << guT;
    osconv.setf(ios::left); osconv << setw(16) << dunorm;
    osconv.setf(ios::left); osconv << setw(16) << dT;
    osconv.setf(ios::left); osconv << setw(16) << delta << endl;
    
    //for (int p=0; p<P; ++p) 
    //stepos << L2IP(u, e[p]) << ' ';
    //stepos << endl;

    if (guT < epsSearch) {
      cout << "Newton search converged. Breaking." << endl;
      cout << "L2Norm(G(u,T))) == " << guT << " < " 
	   << epsSearch << " == " << epsSearch << endl;
      break;
    }
    else if (stuck) {
      cout << "Newton search is stuck. Exiting." << endl;
      exit(1);
    } 
    else if (newtonStep == Nnewton) {
      cout << "Reached maximum number of Newton steps. Exiting." << endl;
      exit(1);
    } 
    if (pausing)
      cfpause();

    savewave(edudt, "edudt");

    prev_guT = guT;
    prev_ruT = ruT;

    // Set up RHS vector b = (-field2vector(G(u)), 0, 0, 0); 
    // (0, 0, 0) is the RHS of eqn enforcing orthog. to (edudt, edudx, edudz)
    ColumnVector b(Meqn);
    setToZero(b);
    field2vector(G_u_T, b);
    if (Tsearch)
      b(Teqn) = 0;
    b *= -1.0;

    cout << "b.length() == " << b.length() << endl;
    cout << "Ngmres2    == " << Ngmres2 << endl;
    cout << "Meqn  == " << Meqn << endl;
    cout << "Munk  == " << Munk << endl;

    GMRES gmres(b, b.length(), epsKrylov);

    // ===============================================================
    // GMRES iteration to solve D[u]G(u,t) du + D[t]G(u,t) dt = -G(u) for du,dt
    for (int n=0; n<Ngmres2; ++n) { 

      cout << "------------------------------------------------" << endl;
      cout << "Newton, GMRES number " << newtonStep << ", " << n << endl;

      // Compute v = Ab in Arnoldi iteration terms, where b is Q.column(n)
      // In Navier-Stokes terms, the main quantity to compute is
      //   L v = D[u] G(u,T) du + D[T] G(u,T) dT

      // Use approximations
      // D[u]G(u,T) du = 1/e (G(u + e du, T) - G(u)) for e << 1
      // D[T]G(u,T) dT = 1/e (G(u, T + e dT) - G(u)) for e << 1

      ColumnVector q = gmres.testVector();
      vector2field(q, du);
      dT = Tsearch ? q(Tunk)*Tscale : 0;
      dunorm = L2Norm(du);

      cout << "L2Norm(du) == " << dunorm << endl;
      cout << "V2Norm(du) == " << V2Norm(du) << endl;
      cout << "       dT  == " << dT << endl;

      // Compute D[u]G(u,T) du = 1/e (G(u+e.du,T) - G(u,T))
      //                       = 1/e (tau f^T(u+e.du) - u+e.du - (tau f^T(u) - u)
      //                       = 1/e (e.du + tau(f^T(u) - f^T(u+e.du))
      // Hmmm. could be better numerically to use this last form, but I'll use first.
      cout << "Computing D[u]G(u,T) du..." << endl;
      if (dunorm/unorm < 1e-17)
	DG_du.setToZero();
      else {
	Real eps_du = lesser(epsDu*unorm/dunorm, 1.0);
	tmp  = du;
	tmp *= eps_du;
	tmp += u;
	G(tmp, T, tmp2, DG_du, tau, Reynolds, flags, dtparams, fcount_newton);
	DG_du -= G_u_T;
	DG_du *= 1/eps_du;
      }
      cout << "Computing D[T]G(u,T) dT..." << endl;
      DG_dT  = dfuT_dT;
      DG_dT *= dT;

      /// Set tmp = DG_du + DG_dT == D[u] G(u,T) du + D[T] G(u,T) dT
      tmp  = DG_du;
      tmp += DG_dT;

      ColumnVector Lq(Meqn);
      field2vector(tmp, Lq);
      if (Tsearch)
	Lq(Teqn) = dudtortho ? orthoscale*L2InnerProduct(du, edudt) : 0.0;
      
      gmres.iterate(Lq);
      gmres_residual = gmres.residual();
      
      cout << "L2Norm(DG du) == " << L2Norm(DG_du) << endl;
      cout << "L2Norm(DG dT) == " << L2Norm(DG_dT) << endl;
      cout << "L2Norm(Ax-b)/L2Norm(b) == " << gmres_residual << endl;

      /**************************************************
      cout << "Checking out current best GMRES solution" << endl;

      s = gmres.solution();
      vector2field(s, du);
      dT = Tsearch ? s(Tunk)*Tscale : 0;
      dunorm = L2Norm(du);

      G(uS, tS, fS, GS, tau, Reynolds, flags, dtparams, fcount_newton);
      Real GSnorm = L2Norm(GS);
      Real rS = 0.5*square(GSnorm);
      ****************************************************/

      if (gmres_residual < epsGMRES) {
        cout << "GMRES converged. Breaking." <<endl;
	sN = gmres.solution();
	break;
      }
      else if (n==Ngmres-1) {
	cout << "GMRES failed to converge. Continuing Newton with best guess." << endl;
	sN = gmres.solution();
      }
      else 
	cout << "Continuing GMRES iteration." << endl;
    } // end GMRES iteration


    // ==================================================================
    // Hookstep algorithm

    cout << "------------------------------------------------" << endl;
    cout << "Beginning hookstep calculations." << endl;

    int Nk = gmres.n(); // Krylov dimension
    Matrix Hn = gmres.Hn();
    Matrix Qn = gmres.Qn();
    Matrix Qn1t = gmres.Qn1().transpose(); // wasteful of memory
    SVD svd(Hn, SVD::std);
	
    Matrix V = svd.right_singular_matrix();
    Matrix Ut = svd.left_singular_matrix().transpose();
    DiagMatrix D = svd.singular_values();

    ColumnVector btmp = Qn1t*b;
    ColumnVector bh = Ut*btmp; // b hat

    // Find a good trust region and the optimal hookstep in it, three stages.
    // 1. ReducingDelta: Keep reducing trust region until residual at hookstep 
    //    gets is roughly as good as that predicted by gradient.
    // 2. IncreasingDelta: Increase the trust region until the quadratic 
    //    model is no longer accurate. 
    // 3. Finished: Back off from the last (bad) increase.
    // Hookstep increases trust region (delta) tentatively and backs down 
    // if it produces bad results, so remember the best steps. All values
    // will be initiated in the last step of the hookstep reduction phase.

    int hookcount = 0;

    bool have_backup = false; 
    Real backup_snorm    = 0.0;
    Real backup_delta    = 0.0;
    Real backup_rS = 0.0;
    Real backup_Delta_rS = 0.0;
    Real backup_dT = 0;

    FlowField backup_du;
    FlowField backup_fS;
    FlowField backup_GS;
      
    ColumnVector sH;
    
    while (true) {

      cout << "-------------------------------------------" << endl;
      cout << "Newton, hookstep number " << newtonStep << ", " 
	   << hookcount++ << endl;
      cout << "delta == " << delta << endl;

      bool hookstep_equals_newtonstep;

      if (L2Norm(sN) < delta) {
	hookstep_equals_newtonstep = true;
	sH = sN;
      }
      else {
	hookstep_equals_newtonstep = false;

	// This for-loop determines the hookstep. Search for value of mu that 
	// produces ||shmu|| == delta. That provides optimal reduction of 
	// quadratic model of residual in trust region norm(shmu) <= delta.
	Real mu = 0; 
	Real nshmu = 0;
	ColumnVector shmu(Nk); // (s hat)(mu)]

	for (int hookSearch=0; hookSearch<Nhook; ++hookSearch) {
	  for (int i=0; i<Nk; ++i)
	    shmu(i) = bh(i)/(D(i,i) + mu); // research notes 
	  nshmu = L2Norm(shmu);
	  Real Phi = nshmu*nshmu - delta*delta;
	  cout << "mu, L2Norm(sh(mU)) == " << mu << ", " << nshmu << endl;

	  // Found satisfactory value of mu and thus shmu and sH s.t. |sH| < delta 
	  if (nshmu < delta || (nshmu > (1-deltaFuzz)*delta && nshmu < (1+deltaFuzz)*delta))
	    break;

	  // Update value of mu and try again. Update rule is a Newton search for 
	  // Phi(mu)==0 based on a model of form a/(b+mu) for norm(sh(mu)). See 
	  // Dennis & Schnabel.
	  else if (hookSearch<Nhook-1) {
	    Real PhiPrime = 0.0;
	    for (int i=0; i<Nk; ++i) {
	      Real di_mu = D(i,i) + mu;
	      Real bi = bh(i);
	      PhiPrime -= 2*bi*bi/(di_mu*di_mu*di_mu);
	    }
	    mu -= (nshmu/delta) * (Phi/PhiPrime);
	  }
	  else {
	    cout << "Couldn't find solution of hookstep optimization eqn Phi(mu)==0" << endl;
	    cout << "This shouldn't happen. It indicates an error in the algorithm." << endl;
	    cout << "Exiting.\n";
	    exit(1);
	  }
	} // search over mu for norm(s) == delta
	  
	cout << "Found hookstep of proper radius" << endl;
	cout << "nshmu == " << L2Norm(shmu) << " ~= " << delta << " == delta " << endl;
	sH = Qn*(V*shmu);
      }

      // Compute
      // (1) actual residual from evaluation of 
      //     rS == r(s+ds) act  == 1/2 (G(u+du,T+dT),G(u+du,T+dT))
      //
      // (2) predicted residual from quadratic model of r(s)
      //     rP == r(s+ds) pred  == 1/2 (G(u,T),G(u,T)) + (G(u,T), D[u]G du + D[t]G dT)
      // 
      // (3) slope of r(s) 
      //     dr/ds == (r(s + ds) - r(s))/|ds|
      //           == (G(u,T), D[u]G du + D[t]G dT)/|ds|
      // where ( , ) is FlowField inner product V2IP, which equals L2 norm of vector rep
      
      // (1) Compute actual residual of step, rS 
      cout << "Computing residual of Newton/hookstep" << endl;
      cout << "  rS == r(s+ds) act  == 1/2 (G(u+du,T+dT),G(u+du,T+dT))" << endl;
      vector2field(sH, du);
      dunorm = L2Norm(du);
      uS  = u;
      uS += du;
      dT = Tsearch ? sH(Tunk)*Tscale : 0;
      tS  = T + dT;
      G(uS, tS, fS, GS, tau, Reynolds, flags, dtparams, fcount_hookstep);

      FlowField DG_act(GS);
      savewave(DG_act, "DG_act");

      Real rS = 0.5*V2Norm2(GS);   // actual residual of hookstep
      Real Delta_rS = rS - ruT;    // improvement in residual
      cout << "r(s), r(s+ds) == " << ruT << ", " << rS << endl;

      // (2) and (3) Compute quadratic model and slope
      cout << "Computing local quadratic model of residual" << endl;
      cout << "  rP == r(s+ds) pred  == 1/2 (G(u,T),G(u,T)) + (G(u,T), D[u]G du + D[t]G dT)" << endl;
      
      // Assign tmp = D[u]G du + d[t]G dT
      if (dunorm/unorm < 1e-17)
	DG_du.setToZero();
      else {
	Real eps_du = lesser(epsDu*unorm/dunorm, 1.0);
	tmp  = du;
	tmp *= eps_du;
	tmp += u;
	G(tmp, T, tmp2, DG_du, tau, Reynolds, flags, dtparams, fcount_hookstep);
	DG_du -= G_u_T;
	DG_du *= 1.0/eps_du;
      }
      savewave(DG_du, "DG_du");

      DG_dT  = dfuT_dT;
      DG_dT *= dT;
	  
      tmp  = DG_du;
      tmp += DG_dT;

      // Local quadratic and linear models of residual, based on Taylor exp at current position
      // Quadratic model of r(s+ds): rQ == 1/2 |G(u,T) + D[u]G du + D[t]G dT)|^2 
      // Linear  model   of r(s+ds): rL == 1/2 (G(u,T),G(u,T)) + (G(u,T), D[u]G du + D[t]G dT)

      Real Delta_rL = V2IP(G_u_T, tmp); // rL - 1/2 (G(u,T),G(u,T)) == (G(u,T), D[u]G du + D[t]G dT)
      Real rL = ruT + Delta_rL;         // rL == 1/2 (G(u,T),G(u,T)) + (G(u,T), D[u]G du + D[t]G dT)

      tmp += G_u_T;
      Real rQ = 0.5*V2Norm2(tmp);       // rQ == 1/2 |G(u,T) + D[u]G du + D[t]G dT)|^2 
      Real Delta_rQ = rQ - ruT;         // rL == 1/2 (G(u,T),G(u,T)) - 1/2 |G(u,T) + D[u]G du + D[t]G dT)|^2 

      snorm = L2Norm(sH);

      // Coefficients for non-local quadratic model of residual, based on r(s), r'(s) and r(d+ds)
      // dr/ds == (G(u,T), D[u]G du + D[t]G dT)/|ds|
      Real drds  = Delta_rL/snorm;
      Real drds2_global = 2*(rS - ruT - drds*snorm)/square(snorm);
      Real drds2_local  = 2*(rQ - ruT - drds*snorm)/square(snorm);
      
      cout << "drds2_global == " << drds2_global << endl;
      cout << "drds2_local  == " << drds2_local << endl;

      // ========================================================
      // Diagnostic stuff
      cout << "r  == " << ruT << endl;
      cout << "rS == " << rS << endl;
      cout << "rL == " << ruT +drds*snorm << endl;
      cout << "rQ == " << ruT +drds*snorm + 0.5*drds2_local*square(snorm) << endl;

      
      ofstream r2os("r2.asc");
      r2os << ruT << endl;
      r2os << rL << endl;
      r2os << rQ << endl;
      r2os << rS << endl;

      ofstream ros("r.asc");

      FlowField u_du;
      FlowField f_du;
      FlowField G_du;
      int forget = 0;
      int N = 100;
      for (int n=0; n<=N; ++n) {
	Real lambda = 10*Real(n)/N;
	Real sn = snorm*lambda;
	Real rquad_glob = ruT + drds*sn + 0.5*drds2_global*square(sn);
	Real rquad_loc  = ruT + drds*sn + 0.5*drds2_local*square(sn);
	Real r_line = ruT + drds*sn;
	
	u_du  = du;
	u_du *= lambda;
	u_du += u;
	G(u_du, T+lambda*dT, f_du, G_du, tau, Reynolds, flags, dtparams, forget);
	Real r_act = 0.5*V2Norm2(G_du);

	ros << lambda << ' ' 
	    << r_act << ' ' 
	    << r_line << ' ' 
	    << rquad_loc << ' '
	    << rquad_glob << ' '
	    << endl;
      }

      // ===========================================================================
      cout << "Determining what to do with current Newton/hookstep and trust region" << endl;

      Real Delta_rS_req  = improvReq*drds*snorm;   // At least a small fraction of linear reduction
      Real Delta_rS_poor = improvPoor*Delta_rQ;    //
      Real Delta_rS_good = improvGood*Delta_rQ;
      Real Delta_rS_err  = abs(Delta_rS-Delta_rQ);  //
      Real Delta_rS_acc  = abs(improvAcc*Delta_rS);

      cout << setprecision(6);
      cout << "Delta_rS      == " << Delta_rS      << "\t actual   improvement in residual from newton/hookstep" << endl;
      cout << "Delta_rS_req  == " << Delta_rS_req  << "\t required improvement in residual" << endl;
      cout << "Delta_rS_poor == " << Delta_rS_poor << "\t poor     improvement means greater than this." << endl;
      cout << "Delta_rS_good == " << Delta_rS_good << "\t good     improvement means less than this." << endl;
      cout << "Delta_rS_acc  == " << Delta_rS_acc  << "\t accurate improvement means this close to prediction." << endl;
      cout << "Delta_rQ      == " << Delta_rQ      << "\t quadratic prediction of improvement" << endl;
      cout << "Delta_rL      == " << Delta_rL      << "\t linear    prediction of improvement" << endl;

      // Characterise change in residual Delta_rS
      ResidualImprovement improvement;  // fine-grained characterization

      // Place improvement in contiguous spectrum: Unacceptable > Poor > Ok > Good.
      if (Delta_rS > Delta_rS_req) 
	improvement = Unacceptable;     // not even a tiny fraction of linear rediction
      else if (Delta_rS > Delta_rS_poor) 
	improvement = Poor;             // worse than small fraction of quadratic prediction
      else if (Delta_rS < Delta_rS_poor && Delta_rS > Delta_rS_good)
	improvement = Ok;               // bewteen Poor and Good 
      else 
	improvement = Good;             // not much worse or better than large fraction of prediction

      // Special cases, which are bands within above spectrum
      if (Delta_rS_err < Delta_rS_acc) 
	improvement = Accurate;         // really close to quadratic prediction
      else if (Delta_rS < Delta_rL) 
	improvement = NegaCurve;        // negative curvature in r(|s|) => try bigger step

      bool recompute_hookstep = false;
      bool delta_has_decreased = false;

      // See comments at end for outline of this control structure
      
      // Following Dennis and Schnable, if the Newton step lies within the trust region,
      // reset trust region radius to the length of the Newton step, and then adjust it
      // further according to the quality of the residual.
      if (hookstep_equals_newtonstep)
	delta = snorm;

      // Improvement is so bad (relative to gradient at current position) that we'll 
      // in all likelihood get better results in a smaller trust region.
      if (improvement == Unacceptable) {
	cout << "Unacceptable improvement from Newton-hook step." << endl; 
	
	if (have_backup) {
	  cout << "But we have a backup step that was acceptable." << endl;
	  cout << "Revert to backup step, take the step, and go to next Newton-GMRES iteration." << endl;
	  dT = backup_dT;
	  du = backup_du;
	  fS = backup_fS;
	  GS = backup_GS;
	  rS = backup_rS;
	  snorm = backup_snorm;
	  delta = backup_delta;
	    
	  recompute_hookstep = false;
	}
	else {
	  cout << "No backup step is available." << endl;
	  cout << "Reduce trust region and recompute hookstep." << endl;

	  // Try to minimize a quadratic model of the residual
	  // r(s+ds) = r(s) + r' |ds| + 1/2 r'' |ds|^2
	  // But require delta
	  Real lambda = -0.5*drds*snorm/(rS - ruT - drds*snorm);
	  lambda = Greater(deltaShrinkMin, lambda); // greatest lower bound
	  lambda =  lesser(deltaShrinkMax, lambda); // least upper bound
	  cout << "  old delta == " << delta << endl;
	  delta *= lambda;
	  cout << "  new delta == " << delta << endl;
	  if (delta > snorm) {
	    cout << "That delta is still bigger than the Newton step." << endl;
	    cout << "This shouldn't happen. Control structure needs review." << endl;
	    cout << "Setting delta to half the length of the Newton step." << endl;
	    cout << "  old delta == " << delta << endl;
	    delta = 0.5*snorm;
	    cout << "  new delta == " << delta << endl;
	  }
	  if (delta < deltaMin) {
	    cout << "Reached minimum trust radius delta. Search has ground to a halt. Exiting." << endl;
	    exit(1);
	  }
	  delta_has_decreased = true;
	  recompute_hookstep = true;
	}
      }

      // Improvement is so good that we're likely to get significantly better result
      // by increasing trust region. Exceptions are 
      //   delta_has_decreased  -- we got here from a larger, unacceptable trust region
      //   hookstep==newtonstep -- increasing trust region won't change answer
      //   delta>=deltamax      -- we're already at the largest allowable trust region
      else if ((improvement == Accurate || improvement == NegaCurve) 
	       && !delta_has_decreased
	       && !hookstep_equals_newtonstep
	       && delta < deltaMax) {

	if (improvement == Accurate)
	  cout << "Improvement is Accurate: close to prediction of quadratic model." << endl;
	else 
	  cout << "Improvement is NegaCurve: better than predicted by linear model, suggesting negative curvature." << endl;

	cout << "And we still have room to expand trust region." << endl;
	cout << "Increase delta and recompute hookstep." << endl;
	backup_dT = dT;
	backup_du = du;
	backup_fS = fS;
	backup_GS = GS;
	backup_rS = rS;
	backup_snorm = snorm;
	backup_delta = delta;
	cout << "  delta == " << delta << endl;
	delta *= deltaRate;
	cout << "  new delta == " << delta << endl;
	putBetween(delta, deltaMin, deltaMax);

	recompute_hookstep = true;
      }
	
      // Remaining cases: Improvement is acceptable: either Poor, Ok, Good, or 
      // {Accurate/NegaCurve and (delta_has_decreased || Hookstep==NewtonStep || delta>=deltaMax)}. 
      // In all these cases take the current step or backup if better. 
      // Adjust delta according to accuracy of quadratic prediction of residual (Poor, Ok, etc).
      else {
	cout << "Improvement is acceptable (fine-grained rating is " << improvement << ")." << endl;

	recompute_hookstep = false;

	// Backup step is better. Revert Take it instead of current step.
	if (have_backup && backup_Delta_rS < Delta_rS) {
	  cout << "But the backup step is better." << endl;
	  cout << "Take the backup step and set delta to backup value." << endl;
	  dT = backup_dT;
	  du = backup_du;
	  fS = backup_fS;
	  GS = backup_GS;
	  rS = backup_rS;
	  cout << "  old delta == " << delta << endl;
	  snorm = backup_snorm;
	  delta = backup_delta;
	  cout << "  new delta == " << delta << endl;
	}

	// Current step is best available. Take it. Adjust delta according to Poor, Ok, etc.
	else {

	  if (have_backup) {
	    cout << "Current step is better than backup step." << endl;
	    cout << "Take current step and keep current delta." << endl;
	    // Keep current delta because current step was best guess at a better delta,
	    // and it turned out to have a worse improvement in residual
	  }

	  // In all the following cases, we have no backup information, so delta adjustment
	  // is based entirely on quality of current residual. 
	  else if (improvement == Poor) {
	    cout << "Current step is acceptable but poor." << endl;
	    cout << "Reduce delta and take step." << endl;
	    cout << "  old delta == " << delta << endl;
	    delta /= deltaRate;
	    cout << "  new delta == " << delta << endl;
	    putBetween(delta, deltaMin, deltaMax);
	  }
	  else if (improvement == Ok) {
	    cout << "Current step is acceptable and Ok." << endl;
	    cout << "Leave delta unchanged and take step." << endl;
	    cout << "  delta == " << delta << endl;
	    putBetween(delta, deltaMin, deltaMax);
	  }
	  // improvement == Good, Accurate, or NegaCurve
	  else {
	    cout << "Current step is acceptable and " << improvement << endl;
	    if (improvement == Accurate || improvement == NegaCurve) {
	      cout << "But don't recomputing hookstep because ... " << endl;
	      cout << "  delta has decreased : " << (delta_has_decreased ? "true" : "false") << endl;
	      cout << "  hook equals newton  : " << (hookstep_equals_newtonstep ? "true" : "false") << endl;
	      cout << "  delta >= deltaMax   : " << (delta >= deltaMax ? "true" : "false") << endl;
	    }
 	    cout << "Increase delta, if there's room, and take step." << endl;
	    cout << "  old delta == " << delta << endl;
	    delta *= deltaRate;
	    cout << "  new delta == " << delta << endl;
	    putBetween(delta, deltaMin, deltaMax);
	  }
	}
      }
      cout << "|ds|          == " << snorm << endl;
      cout << "delta         == " << delta << endl;
      cout << "L2Norm(du)    == " << dunorm <<  endl;
      cout << "L2Norm(sH)    == " << snorm <<  endl;
      cout << "L2Norm(sN)    == " << L2Norm(sN) <<  endl;
      cout << "recompute_hookstep == " << recompute_hookstep << endl;
      cfpause();

      if (recompute_hookstep) 
	continue;
      else 
	break;
    }

    u += du;
    T += dT;
    dunorm = L2Norm(du);
    unorm  = L2Norm(u);

    //G(u, T, f_u_T, G_u_T, tau, Reynolds, flags, dtparams, fcount_newton);
    f_u_T = fS;
    G_u_T = f_u_T;
    //G_u_T *= tau;
    G_u_T -= ufinal;
    //G_u_T = GS;
    
    cout << "L2Dist(GS, G_u_T) == " << L2Dist(GS, G_u_T)  << endl;
    cout << "Taking best step and continuing Newton(-hookstep) iteration." <<endl;
    cout << "L2Norm(sN)  == " << L2Norm(sN) << endl;
    cout << "L2Norm(sH)  == " << L2Norm(sH) << endl;
    cout << "V2Norm(du) == " << V2Norm(du) << endl;
    cout << "L2Norm(du) == " << dunorm << endl;
    cout << "       dT  == " << dT << endl;
    cout << "L2IP(du,edudx)/L2Norm(du) == " << L2IP(du,edudx)/dunorm << endl;
    cout << "L2IP(du,edudz)/L2Norm(du) == " << L2IP(du,edudz)/dunorm << endl;
    cout << "L2IP(du,edudt)/L2Norm(du) == " << L2IP(du,edudt)/dunorm << endl;

    u.binarySave(outdir + "uiterate" + i2s(newtonStep));
    u.binarySave(outdir + "uapprox");
    save(T, "Titerate" + i2s(newtonStep));
    save(T, "Tapprox");
  }
  fftw_cleanup();
}

void params2timestep(TimeStepParams p, Real T, int& N, TimeStep& dt) {

  // Set CFL adjustment time T to an integer division of T near 1
  N = Greater(iround(T), 1);
  const Real dT = T/N;
  if (p.variable)
    dt = TimeStep(p.dt, p.dtmin, p.dtmax, dT, p.CFLmin, p.CFLmax);
  else 
    dt = TimeStep(p.dt, p.dt, p.dt, dT, p.CFLmin, p.CFLmax);
}


Real cx = 0.1;
Real cz = 0;

/// f^T(u) = time-T DNS integration of u
void f(const FlowField& u, int N, Real dt, FlowField& f_u, Real Reynolds, 
        DNSFlags& flags) {
  FieldSymmetry translate(1,1,1, -cx*N*dt, -cz*N*dt);
  f_u = translate(u);
}

void f(const FlowField& u, Real T, FlowField& f_u, Real Reynolds, DNSFlags& flags, 
       TimeStepParams dtp, int& fcount) {

  FieldSymmetry translate(1,1,1, -cx*T, -cz*T);
  f_u = translate(u);
  ++fcount;
}


void G(const FlowField& u, Real T, FlowField& f_u, FlowField& G_u, const FieldSymmetry& tau, 
       Real Reynolds, DNSFlags& flags, TimeStepParams dtp, int& fcount) {
  FlowField ufinal("ufinal");
  f(u, T, f_u, Reynolds, flags, dtp, fcount);
  G_u = f_u;
  //G_u *= tau;
  G_u -= ufinal;
}

Real V2Norm2(const FlowField& u) {
  ColumnVector v;
  field2vector(u,v);
  return L2Norm2(v);
}

Real V2Norm(const FlowField& u) {
  return sqrt(V2Norm2(u));
}

Real V2IP(const FlowField& u, const FlowField& v) {
  ColumnVector uV;
  ColumnVector vV;
  field2vector(u,uV);
  field2vector(v,vV);
  return (uV.transpose())*vV;
}

ostream& operator<<(ostream& os, HookstepPhase p) {
  if      (p == ReducingDelta)   os << "ReducingDelta";
  else if (p == IncreasingDelta) os << "IncreasingDelta";
  else                           os << "Finished";
  return os;
}
ostream& operator<<(ostream& os, ResidualImprovement i) {
  if      (i == Unacceptable) os << "Unacceptable";
  else if (i == Poor)         os << "Poor";
  else if (i == Ok)           os << "Ok";
  else if (i == Good)         os << "Good";
  else if (i == Accurate)     os << "Accurate";
  else                        os << "NegativeCurvature";
  return os;
 }

void savewave(FlowField& u, const string& name) {

  int Nx = u.Nx();
  int Ny2 = (u.Ny()-1)/2;
  
  u.makePhysical();
  Vector f(Nx+1);
  for (int nx=0; nx<Nx; ++nx)
    f(nx) = u(nx,Ny2,0,2);
  f(Nx) = u(0,Ny2,0,2);
  f.save(name);
  u.makeSpectral();
}
  
void putBetween(Real& delta, Real deltaMin, Real deltaMax) {
  assert(deltaMin <= deltaMax);
  if (delta < deltaMin) {
    delta = deltaMin;
    cout << "delta bottomed out at deltaMin" << endl;
    cout << "  new delta == " << delta << endl;
  }
  else if (delta > deltaMax) {
    delta = deltaMax;
    cout << "delta topped out at deltaMax" << endl;
    cout << "  new delta == " << delta << endl; 
  }
}

// I've biased the notation towards CNS ChaosBook/fluids conventions and
// away from Dennis & Schnabel and Trefethen. That is, f stands for the 
// finite-time map of plane Couette dynamics, not the residual being minimized.
//
// Note: in code, dt stands for the integration timestep and 
//                dT stands for Newton-step increment of period T
// But for beauty's sake I'm using t and dt for the period and 
// its Newton-step increment in the follwoing comments.
//
// Let 
//  f^t(u) = time-t map of Navier-Stokes computed by DNS. 
//  G(u,t) = (tau f^t(u) - u) for periodic orbit searches
//  r(u,t) = 1/2 L2Norm2(G(u,t)), 
//   
// We want to find zeros of G(u,t) or equiv minimum of r(u,t). 
// Note that the residual is reported to the user in the form
//   residual(u,t) == L2Norm(G(u,T)) == sqrt(2*r(u,t))
//
// The Newton algorithm for solving G(u,t) = 0 is
//
// Let u* be the solution, i.e. G(u*,t*) = 0
// Suppose we start with an initial guess u near u*.
// Let u* = u + du and t* = t + dt.
// Then G(u*,t*) = G(u+du, t+dt) 
//             0 = G(u,t) + D[u]G(u,t) du + D[t]G(u,t) dt + O(|du|^2, dt^2)
// Drop h.o.t and solve
//   D[u]G(u,t) du + D[t]G(u,t) dt = -G(u)
//                  (du, du/dt) = 0
// Then start with new guess u' = u+du t'=t+dt and iterate.
//
// Use GMRES to solve the Newton-step equation. GMRES requires multiple 
// calculations of the LHS DG(u,t) du + dG(u,t)/dt dt for trial values 
// du, dt. These can be approximated with finite differencing:
//   D[u]G(u,t) du = 1/e (G(u + e du, t) - G(u,t)) where e |du| << 1.
//   D[t]G(u,t) dt = 1/e (G(u, t + e dt) - G(u,t)) where e |dt| << 1.
//
// To use GMRES, we need to cast velocity fields such as du and DG(u) du 
// into vectors and vice versa. Do this with a real-valued, orthonormal, 
// div-free, dirichlet basis {Phi_n, n=0,...,N-1}
//   a_n = (u, Phi_n), n=0,..,N-1
//     u = a_n Phi_n,  n=0,..,N-1
//     t = a_N

// Then the Newton step equations
//  D[u]G(u,t) du + D[t]G(u,t) dt = -G(u)
//                  (du, edudt) = 0
// can be translated to a linear algebra problem   
//      A s = b 
// via
//      s_n = (du, Phi_n)    n=0,..,N-1
//      b_n = (-G(u,t), Phi_n) n=0,..,N-1
//  (A s)_n = (D[u]G(u,t) du + D[t]G(u,t) dt, Phi_n)
//      s_N = dt
//      b_N = 0
//  (A s)_N = (du, du/dt)

// We then solve this system approximately with GMRES.
// 
// Hookstep algorithm: If the Newton step du = s_n Phi_n does not decrease 
// the residual L2Norm(G(u+du, u+dt)) sufficiently, we obtain the a smaller step
// s by minimizing || A s - b ||^2 subject to ||s||^2 <= delta^2, rather 
// solving A s = b directly. 

// But since A is M x M, with M = O(10^5), we minimize the norm of the 
// projection of A s - b onto the Krylov subspace from GMRES. GMRES gives
//   A Qn = Qn1 Hn, where 
// where Qn is M x n, Qn1 is M x (n+1), and Hn is (n+1) x n.
// Projection of A s - b on (n+1)th Krylov subspace is Qn1^T (A s - b) 
// Confine s to nth Krylov subspace: s = Qn sn, where sn is n x 1.
// Then minimize 
//   || Qn1^T (A s - b) || = || Qn1^T A Qn sn - Qn1^T b ||
//                         = || Qn1^T Qn1 Hn sn - Qn1^T b ||
//                         = || Hn sn - Qn1^T b || 
// subject to ||s||^2 = ||sn||^2 < delta^2
//
// Do minimization via SVD of Hn = U D V^T.
//   || Qn1^T (A s - b) || = || Hn sn - Qn1^T b || 
//                         = || U D V^T sn - Qn1^T b ||
//                         = || D V^T sn - U^T Qn1^T b ||
//                         = || D sh - bh ||
// where sh = V^T sn and bh = U^T Qn1^T b. 
//
// Since D is diagonal D_{i,i} = d_i, the constrained minimization 
// problem can be solved easily with lagrange multipliers:
//   sh_i = (bh_i d_i)/(d^2_i + mu)
// for the mu such that ||sh||^2 = delta^2. The value of mu can be 
// found with a 1d Newton search for zeros of 
//   Phi(mu) = ||sh(mu)||^2 - delta^2
// A straight Newton search a la mu -= Phi(mu)/Phi'(mu) is suboptimal 
// since Phi'(mu) -> 0 as mu -> infty with Phi''(mu) > 0 everywhere, so 
// we use a slightly modified update rule for the Newton search. Please 
// refer to Dennis and Schnabel regarding that. Then, given the solution 
// sh(mu), we compute the hookstep solution s from 
//   s = Qn sn = Qn V sh

// D[t]G(u,t) dt == D[t] (tau f^(t+dt)(u) - u) - (tau f^(t)(u) - u) 
//               == tau (f^(t+dt)(u) - f^(t)(u))
//               == tau (f^dt(f^t(u)) - f^t(u))
//               == tau (f^dt(f^t(u)) - f^t(u))
//               == tau (df^T(u)/dT) * dT

// 


