#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>

#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"

#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("produce plot of energy in symmetric/antisymmetric subspace versus phase shift ax or az\n"
		 "1/2 ||P^{+/-}_s(tau(ax,az) u)||^2  == 1/8 ||si tau(ax,az) u +/- tau(ax,az) u||^2");

  ArgList args(argc, argv, purpose);
 
  const int  sn   = args.getint ("-s",  "--symmetry", 0,      "s1 or s2 symmetry");
  const bool sx   = args.getflag("-sx",  "--sx",              "flip u,x signs: u,x -> -u,-x");
  const bool sy   = args.getflag("-sy",  "--sy",              "flip v,y signs: v,y -> -v,-y");
  const bool sz   = args.getflag("-sz",  "--sz",              "flip w,z signs: w,z -> -w,-z");
  const Real ax   = args.getreal("-ax",  "--axshift",      0.0,    "x -> x + ax/Lx");
  const Real az   = args.getreal("-az",  "--azshift",      0.0,    "z -> z + az/Lz");
  const bool anti   = args.getflag("-a",  "--anti",           "plot energy in antisymmetric subspace");
  const bool normal = args.getflag("-n",  "--normalize",      "normalize ||P(u)||^2 by ||u||^2 at each t");
  const Real df     = args.getreal("-da", "--da",       0.02, "increment for ax or az ");
  const int  T0     = args.getint ("-T0", "--T0",       0,    "initial time");
  const int  T1     = args.getint ("-T1", "--T1",       1000, "end time");
  const int  Kx     = args.getint ("-Kx", "--Kx",       0,    "max kx for cheap norm evals");
  const int  Kz     = args.getint ("-Kz", "--Kz",       0,    "max kz for cheap norm evals");
  const int  dT     = args.getint ("-dT", "--dT",       10,    "time increment");
  const string dir  = args.getpath("-d",  "--datadir", "data/", "data directory");

  const string oname = args.getstr(1, "<ofile>",   "output file");

  args.check();
  args.save("./");

  FieldSymmetry s;
  if (sn == 1)
    s = FieldSymmetry( 1, 1,-1, 0.5, 0);
  else if (sn==2)
    s = FieldSymmetry(-1,-1, 1, 0.5, 0.5);
  else if (sn==3)
    s = FieldSymmetry(-1,-1,-1, 0.0, 0.5);
  else 
    s = FieldSymmetry(sx ? -1:1, sy ? -1:1, sz ? -1:1, ax, az);
  cout << "symmetry s == " << s << endl;

  ofstream os(oname.c_str());
  Real energy = 0.0;
  uint count = 0;

  os << "% fraction of energy in (anti)symmetric subspace of symmetry s == " << s << endl;
  os << "% with phase shifts in x and z." << endl;
  for (int t=T0; t<=T1; t += dT) {
    cout << t << ' ' << flush;
    FlowField u(dir + "u" + i2s(t));
    FlowField tu;
    FlowField stu;

    energy += L2Norm2(u);
    ++count;

    // Normalization: energy in P(u) == 1/2 ||P u||^2 
    //                               == 1/2 ||1/2 (1 +/- s) u||^2 
    //                               == 1/8 || (1 +/- s) u || ^2
    Real c = normal ? 0.25/L2Norm2(u) : 0.125;

    // Loop over x phase shifts or z?
    bool xphase = (s.sx() == -1) ? true : false;

    for (Real fn = 0.0; fn<=0.5; fn += df) {
      FieldSymmetry tau(1,1,1, (xphase ? fn : 0.0), (xphase ? 0.0 : fn));
      tu = tau(u);
      stu = s(tu);
      if (!anti)
	stu *= -1;
      os << c*L2Dist2(stu, tu, Kx, Kz) << ' ';
    }
    os << endl;
  }
  cout << "avg energy == " << energy/(2*count) << endl;
  cout << endl;
}
