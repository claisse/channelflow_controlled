#include <iostream>
#include <fstream>
#include <iomanip>

#include <octave/oct.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/dbleSVD.h>

#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"

#include "quicksort.h"
#include "utilities.h"

using namespace std;
using namespace channelflow;


int main(int argc, char* argv[]) {

  string purpose("computes a POD basis from a time series of data");
  ArgList args(argc, argv, purpose);

  const bool mean      = args.getflag("-m", "--mean",     "subtract mean before doing POD");
  const Real T0        = args.getreal("-T0", "--T0", 0,   "start time");
  const Real T1        = args.getreal("-T1", "--T1", 500, "end time");
  const Real dT        = args.getreal("-dT", "--dT", 1.0, "save interval");
  const int    Nbasis  = args.getint("-Nb", "--Nbasis",   "number of basis fields");
  const string bdir    = args.getpath("-b", "--basisdir", "basis/", "basis set directory");
  const string udir    = args.getpath("-d", "--datadir",  "data/", "flowfield series directory");
  const string ulabel  = args.getstr("-ul", "--ulabel",   "u", "flowfield filename label");

  args.check();
  args.save("./");

  const int Ndata = int((T1-T0)/dT);
  const bool inttime = (abs(dT - int(dT)) < 1e-12) ? true : false;

  cout << "T0 == " << T0 << endl;
  cout << "T1 == " << T1 << endl;
  cout << "T1 == " << t2s(T0 + Ndata*dT, inttime) << " after rounding and conversion to string." << endl;

  FlowField umean(udir + ulabel + t2s(T0, inttime));
  umean.setToZero();

  const string upath = udir + ulabel;

  mkdir(bdir);
  if (mean) {
    cout << "Calculating umean : " << flush;
    for (int n=0; n<Ndata; ++n) {
      Real t = T0 + n*dT;
      cout << n << ' ' << flush;
      FlowField u(upath + t2s(t, inttime));
      umean += u;
    }
    umean *= 1.0/Ndata;
    cout << "done" << endl;
    umean.binarySave(bdir + "umean");
  }

  cout << "Calculating inner product matrix : row " << flush;
  Matrix R(Ndata,Ndata);
  for (int m=0; m<Ndata; ++m) {
    Real tm = T0 + m*dT;
    cout << m << ' ' << flush;
    
    FlowField um(upath + t2s(tm, inttime));
    um -= umean;
    for (int n=m; n<Ndata; ++n) {
      Real tn = T0 + n*dT;
      FlowField un(upath + t2s(tn,inttime));
      un -= umean;
      Real ip = L2InnerProduct(um,un);
      R(m,n) = ip/Ndata;
      R(n,m) = ip/Ndata;
    }
  }
  
  cout << "Calculating SVD of innerproduct matrix..." << flush;
  SVD svd(R);
  DiagMatrix sigma2 = svd.singular_values();
  Matrix U = svd.left_singular_matrix();
  cout << "done" << endl;

  
  ColumnVector sigma(Nbasis);
  for (int n=0; n<Nbasis; ++n) {
    cout << "building e" << n << endl;
    FlowField e = umean;
    e.setToZero();
    for (int m=0; m<Ndata; ++m) {
      Real tm = T0 + m*dT;
      FlowField um(upath + t2s(tm, inttime));
      um -= umean;
      um *= U(m,n);
      e += um;
    }
    sigma(n) = sqrt(sigma2(n,n));
    e *= 1.0/L2Norm(e); // sigma_n;
    e.binarySave(bdir + "e" + i2s(n));
  }
  save(sigma, bdir + "sigma");
}

