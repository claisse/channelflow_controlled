#include <iostream>
#include <fstream>
#include <string>
#include "petsc-newton.h"
#include "utilities.h"

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::ofstream;
using std::streamsize;

using namespace channelflow;


char * help = "Attempts to locate a fixed point in the vicinity of a given point in phase space.";

void petscSetup(int argc, char**argv)
{
  int mpiSize;
  PetscInitialize ( &argc,&argv,PETSC_NULL,help );
  MPI_Comm_size ( PETSC_COMM_WORLD,&mpiSize );
}

PetscErrorCode jacobianMult_Wrapper(Mat J, Vec x, Vec b)
{
  DNSContext * dnsctx;
  MatShellGetContext(J, (void **)&(dnsctx));
  return dnsctx->jacobianMult(J,x,b);
}
// // If called, petscSetup is not necessary
// void slepcSetup(int argc, char**argv)
// {
//   int mpiSize;
//   SlepcInitialize ( &argc, &argv, PETSC_NULL,help );
//   MPI_Comm_size ( PETSC_COMM_WORLD,&mpiSize );
// }

// RPO PetscNewton::computeEigs(std::vector<RealProfileNG&> basis,RPO& rpo, const Real Re, bool fixSx, bool fixSz, bool fixT,
//                              Real dt, Real dtMin, Real dtMax, Real CFLmin, Real CFLmax,
//                              Real checkTime, DNSFlags & flags, string outdir, string label)
// {
//   // Solution & residual vectors
//   Vec x, r;
//   VecCreateSeq ( PETSC_COMM_WORLD, dnsctx.size(), &x );
//   VecDuplicate ( x, &r );
// 
//   // Setup Jacobian
//   Mat J;
//   MatCreateShell(PETSC_COMM_WORLD,size,size,size,size,&dnsctx,&J);
//   MatShellSetOperation(J, MATOP_MULT, (void(*)(void)) MatMult_Wrapper);
//   
//   MatCreateSNESMF(snes,&(dnsctx.innerJ));
//   MatMFFDSetFromOptions(dnsctx.innerJ);
//   SNESSetJacobian(snes,J,J,makeJacobianWrapper,(void *) &dnsctx);
//   makeJacobianWrapper(SNES snes,Vec x,Mat *J,Mat *B,MatStructure *flag,void *ctx) 
//       KSP ksp;
//   PC pc;
//   SNESGetKSP(snes,&ksp);
//   KSPGetPC(ksp,&pc);
//   PCSetType(pc,PCNONE);
//   SNESMonitorSet(snes, &RPOMonitor, &dnsctx, PETSC_NULL);
//   SNESSetFromOptions ( snes );
//   
//   rpo2vec(basis, rpo, x);
//   SNESComputeJacobian(SNES snes,x,&J,PETSC_NULL,PETSC_NULL)
//   
//   // Setup Eigenvalue problem solver
//   EPS eps;
//   EPSCreate( PETSC_COMM_WORLD, &eps );
//   EPSSetOperators( eps, J, PETSC_NULL );
//   EPSSetProblemType( eps, EPS_NHEP ); // Non-hermitian eigenvalue problem
//   EPSSetFromOptions( eps );
//   
//   //Solve
//   EPSSolve( eps );
//   
//   int nconv; // number of converged eigenpairs
//   EPSGetConverged( eps, &nconv );
//   Real wr, wi;
//   Vec ev_r, ev_i;
//   for( int i = 0; i<nconv; ++i )
//   {
//    EPSGetEigenpair( eps, &wr, &wi, ev_r, ev_i );   
//    if( wi == 0 )
//    {
//     vec2field(basis, ev_r, ef);
//     ef.binarySave(label + i);
//     saveEigenvalue(wr, wi, label, j);  
//    }
//    else
//    {
//      vec2field(vec2field(basis, ev_r, ef);
//      ef.binarySave(label "real_");
//      saveEigenvalue(wr, wr, label, i);
//      vec2field(vec2field(basis, ev_i, ef);
//      ef.binarySave(label "imag_");
//      i++;
//    }
//   }
//   
//   //Cleanup
//   EPSDestroy( eps );
// }

RPO PetscNewton::findInvariant(std::vector<RealProfileNG>& basis, RPO& rpo, const Real Re, 
			       bool fixSx, bool fixSz, bool fixT, bool fixRe, const Real targetD,
			       std::vector<FieldSymmetry>& symms,
                               Real dt, Real dtMin, Real dtMax, Real CFLmin, Real CFLmax,
                               Real checkTime, DNSFlags & flags, string outdir, string label)
{
  
  DNSContext dnsctx(basis, rpo,Re, fixSx, fixSz, fixT, fixRe, targetD, symms,
                     dt, dtMin, dtMax, CFLmin, CFLmax,
                     checkTime, flags, outdir, label);
  
  int dim = dnsctx.size();
  
  // Solution & residual vectors
  Vec x, r;
  VecCreateSeq ( PETSC_COMM_WORLD, dim, &x );
  VecDuplicate ( x, &r );

  // Setup nonlinear solver

  SNES snes;
  SNESCreate(PETSC_COMM_WORLD,&snes);
  SNESSetFunction ( snes, r, &computeRPOResidual, (void *) &dnsctx );
  

  Mat J;
  bool matrix_free = true;
  if(matrix_free)
    {
      MatCreateSNESMF( snes, &J);
      SNESSetJacobian( snes, J, J, MatMFFDComputeJacobian_wrapper, (void *) &dnsctx);
    }  
  else
    {
      // Setup wrapper Jacobian
 
      MatCreateShell(PETSC_COMM_WORLD,dim,dim,dim,dim,&dnsctx,&J);
  
      MatShellSetOperation(J, MATOP_MULT, (void(*)(void)) jacobianMult_Wrapper);
      SNESSetJacobian(snes,J,J,makeJacobian,(void *) &dnsctx);
    }
    
  KSP ksp;
  PC pc;
  SNESGetKSP(snes,&ksp);
  KSPGetPC(ksp,&pc);
  PCSetType(pc,PCNONE);
    

  SNESMonitorSet(snes, &RPOMonitor, (void *) &dnsctx, PETSC_NULL);
	SNESSetFromOptions ( snes );
  SNESConvergedReason reason;
  
  //Convert initial guess to Vec
  dnsctx.rpo2vec ( rpo, x );
//  dnsctx.setBase ( x );
  SNESSolve ( snes,PETSC_NULL,x );

  PetscInt its;
  SNESGetConvergedReason ( snes, &reason );//CHKERRQ ( ierr );
  SNESGetIterationNumber ( snes,&its );//CHKERRQ ( ierr );
  PetscPrintf ( PETSC_COMM_WORLD,"Number of Newton iterations = %D, %s\n",its,SNESConvergedReasons[reason] );
  
  dnsctx.vec2rpo ( x, rpo );
	
  // Cleanup
  
  VecDestroy ( x );
  VecDestroy ( r );
  MatDestroy ( J );
  SNESDestroy ( snes );

  return rpo;
}

PetscErrorCode RPOMonitor(SNES snes,PetscInt its, PetscReal norm,void *mctx)
{
  Vec x;
  SNESGetSolution(snes, &x);
  DNSContext *dnsctx = ( DNSContext * ) mctx;
  
  RPO soln;
  soln.u = &(dnsctx->tmp);
  dnsctx->vec2rpo(x, soln);
  if(dnsctx->fixSx)
    soln.sx = dnsctx->sx;
  if(dnsctx->fixSz)
    soln.sz = dnsctx->sz;
  if(dnsctx->fixT)
    soln.T = dnsctx->T;
  
  soln.u->binarySave(dnsctx->outdir + '/' + dnsctx->label + '_' + i2s(its));
  ofstream statsFile;
  string filename = dnsctx->outdir + '/' + dnsctx->label + '_' + i2s(its) + ".stats";
  statsFile.open(filename.c_str());
  statsFile << soln.sx << endl;
  statsFile << soln.sz << endl;
  statsFile << soln.T << endl;
  statsFile << soln.Re << endl;
  statsFile << norm << endl;
  statsFile.close();
  
  return 0;
}

PetscErrorCode computeRPOResidual(SNES snes, Vec x, Vec f, void *ctx)
{
  DNSContext * dnsctx = ( DNSContext * ) ctx;
  RPO e;
  e.u = &(dnsctx->tmp);

  dnsctx->vec2rpo(x, e);
  FlowField backup(*(e.u));

  FlowField du0_dx = xdiff(backup);
  FlowField du0_dt(backup);
  dnsctx->computeDuDt(backup, du0_dt);
  streamsize prec = cout.precision();
  cout.precision(12);
  cout << "T : "  << e.T << endl;
  cout << "dx : " << e.sx << endl;
  cout << "dz : " << e.sz << endl;
  cout << "L2Norm(u) : " << L2Norm(*(e.u)) << endl;
  
  dnsctx->advance(*(e.u), -e.sx, -e.sz, e.T, e.Re);
  *(e.u) -= backup;
  e.T = 0; e.sx = 0; e.sz = 0;
  if(!dnsctx->fixSx) {
    FlowField du0_dx = xdiff(backup);
    e.sx = 1 - pow(L2InnerProduct(du0_dt, du0_dx)*L2Norm(du0_dt)*L2Norm(du0_dx),2.0);
    e.sx *= 100;
  }

  if(!dnsctx->fixSz) {
    FlowField du0_dz = zdiff(backup);
    e.sz = 1 - pow(L2InnerProduct(du0_dt, du0_dz)*L2Norm(du0_dt)*L2Norm(du0_dz),2.0);
  }
      
  cout << "Residual : " << L2Norm(*(e.u)) << endl;
  cout.precision(prec);
  
  dnsctx->rpo2vec(e, f);
  return 0;
}

void DNSContext::computeDuDt(const FlowField &u, FlowField &dudt)
{
  dudt = u;
  DNS dns ( dudt, 1.0 / Re, DUDT_H, flags );
  dns.advance(dudt, p, 1);
  dudt -= u;
  dudt *= 1.0 / DUDT_H;
}

PetscNewton::PetscNewton()
{
  SNESCreate(PETSC_COMM_WORLD, &snes);
  SNESSetFromOptions(snes);
}

PetscNewton::~PetscNewton()
{
  //  VecDestroy(x);
  //  VecDestroy(r);
  SNESDestroy(snes);
}

PetscErrorCode MatMFFDComputeJacobian_wrapper(SNES snes, Vec x, Mat *J, Mat *B, MatStructure *flag, void *ctx)
{
  PetscErrorCode code = MatMFFDComputeJacobian(snes, x, J, B, flag, ctx);
  DNSContext * dnsctx = (DNSContext *) ctx;
  dnsctx->setBase(x);

  const int dim_null = 3;
  Vec * nullbasis;
  PetscMalloc( dim_null*sizeof(Vec), &nullbasis);
  int k = 0;
  VecCreateSeq(PETSC_COMM_WORLD, dnsctx->size(), nullbasis + k);
  dnsctx->field2vec(dnsctx->x0_T,nullbasis[k]);

  k++;
  VecCreateSeq(PETSC_COMM_WORLD, dnsctx->size(), nullbasis + k);
  dnsctx->field2vec(dnsctx->x0_x, nullbasis[k]);

  k++;
  VecCreateSeq(PETSC_COMM_WORLD, dnsctx->size(), nullbasis + k);
  dnsctx->field2vec(dnsctx->x0_z, nullbasis[k]);

  MatNullSpace nullsp;
  MatNullSpaceCreate(PETSC_COMM_WORLD, PETSC_FALSE, dim_null, nullbasis, &nullsp);
  MatNullSpaceAttach(*J, nullsp);
  return code;

}

PetscErrorCode makeJacobian(SNES snes,Vec x,Mat *J,Mat *B,MatStructure *flag,void *ctx)
{
  DNSContext * dnsctx = (DNSContext *) ctx;
  dnsctx->setBase(x);  

/*
  const int size = dnsctx->size();  
  MatCreateShell(PETSC_COMM_WORLD,size,size,size,size,dnsctx,J);
  MatShellSetOperation(*J, MATOP_MULT, (void(*)(void)) (jacobianMult_Wrapper));*/
  

  const int dim_null = 3;
  Vec * nullbasis;
  PetscMalloc( dim_null*sizeof(Vec), &nullbasis);
  int k = 0;
  VecCreateSeq(PETSC_COMM_WORLD, dnsctx->size(), nullbasis + k);
  dnsctx->field2vec(dnsctx->x0_T,nullbasis[k]);

  k++;
  VecCreateSeq(PETSC_COMM_WORLD, dnsctx->size(), nullbasis + k);
  dnsctx->field2vec(dnsctx->x0_x, nullbasis[k]);

  k++;
  VecCreateSeq(PETSC_COMM_WORLD, dnsctx->size(), nullbasis + k);
  dnsctx->field2vec(dnsctx->x0_z, nullbasis[k]);

  MatNullSpace nullsp;
  MatNullSpaceCreate(PETSC_COMM_WORLD, PETSC_FALSE, dim_null, nullbasis, &nullsp);
  MatNullSpaceAttach(*J, nullsp);
  
  // Set preconditioner to be the same
  B = J; 
  *flag = SAME_PRECONDITIONER;
  
  return 0;
}


DNSContext::DNSContext(std::vector<RealProfileNG>& basis, RPO& rpo, const Real Re, 
		       bool fixSx, bool fixSz, bool fixT,bool fixRe, const Real targetD, vector<FieldSymmetry>& symms,
                     Real dt, Real dtMin, Real dtMax, Real CFLmin, Real CFLmax,
                     Real checkTime, DNSFlags & flags, string outdir, string label, const Real erel)
                     : 
                       basis(basis),
                       sx(rpo.sx), sz(rpo.sz), T(rpo.T),
                       fixSx(fixSx), fixSz(fixSz), fixT(fixT), fixRe(fixRe), targetD(targetD),
		       symms(symms),
                       Re(Re), flags(flags),
                       dtGuess(dt), dtMin(dtMin), dtMax(dtMax), CFLmin(CFLmin), CFLmax(CFLmax), checkInterval(checkTime),
                       x0(*(rpo.u)), x0_x(*(rpo.u)), x0_z(*(rpo.u)), x0_T(*(rpo.u)),
                       y0(*(rpo.u)), y0_x(*(rpo.u)), y0_z(*(rpo.u)), y0_T(*(rpo.u)),
                       tmp(*(rpo.u)), tmp2(*(rpo.u)),
                       p(tmp.Nx(), tmp.Ny(), tmp.Nz(), 1, tmp.Lx(), tmp.Lz(),tmp.a(),tmp.b()),
                       outdir(outdir), label(label),
                       erel(erel)
{
  
}

void DNSContext::setBase(Vec x)
{
  RPO rpo; rpo.u = &x0;
  vec2rpo(x, rpo);
  
   if(!fixT)
     T = rpo.T;
   if(!fixSx)
     sx = rpo.sx;
   if(!fixSz)
     sz = rpo.sz;
   if(!fixRe)
     Re = rpo.Re;
  
  xdiff(x0, x0_x);
  zdiff(x0, x0_z);
  computeDuDt(x0, x0_T);
  
  y0 = x0;
  advance(y0, -sx, -sz, T, Re);
  
  xdiff(y0, y0_x);
  zdiff(y0, y0_z);
  computeDuDt(y0, y0_T);
}

int DNSContext::size()
{
  int size = basis.size();
  if(!fixSx)
    size++;
  if(!fixSz)
    size++;
  if(!fixT)
    size++;
  return size;
}

/**
 *  Multiplies Jacobian for forward time map,
 *  as described in Viswanath paper
 */
PetscErrorCode DNSContext::jacobianMult(Mat J, Vec x, Vec b)
{
  RPO du; 
  du.u = &tmp;
  vec2rpo(x, du);
  
  FlowField &dx0 = *(du.u);
  Real h = computeH(x0, dx0);
  cout << "h: " << h << endl;
  
  FlowField dx = dx0;
  dx *= h;
  dx += x0;
  
  FlowField residual = dx;
  advance(dx, -sx, -sz, T, du.Re);
  cout << "L2Norm(fT(dx))" << L2Norm(dx) << endl;
  residual -= dx;
  cout << "Residual : " << L2Norm(residual) << endl;
  
  dx -= y0; 
  dx *= 1.0 / h;
  
  dx -= dx0;
  
  RPO out;
  if(!fixT)
  {
    tmp2 = y0_T;
    tmp2 *= du.T;
    dx += tmp2;
    out.T  = L2InnerProduct(dx0,x0_T);  cout << "bT : " << out.T << endl;  
  }
  
  if(!fixSx)
  {
    tmp2 = y0_x;
    tmp2 *= -du.sx;
    dx += tmp2;
    out.sx = L2InnerProduct(dx0,x0_x); cout << "bsx : " << out.sx << endl;
  }
  
  if(!fixSz)
  {
    tmp2 = y0_z;
    tmp2 *= -du.sz;
    dx += tmp2;
    out.sz = L2InnerProduct(dx0,x0_z);  cout << "bsz : " << out.sz << endl;
  }

  if(!fixRe) {
    out.Re = dissipation(dx) - targetD; cout << "bD : " << out.Re << endl;
  }
    
  
  out.u = &dx;
  rpo2vec(out, b);
  return 0;
}

void DNSContext::vec2rpo(const Vec &v, RPO& rpo)
{
  FlowField &u= *(rpo.u);
  u.setToZero();
  RealProfileNG e;
  const Real EPSILON = 1e-16;

  PetscScalar *vv; 
  VecGetArray(v, &vv);        
  int count = 0;
  
  for(vector<RealProfileNG>::const_iterator n = basis.begin(); n!=basis.end(); ++n)
  {
      if(fabs(vv[count]) < EPSILON)
      {
        ++count;
        continue;
      }
      e = *n;
      e *= vv[count];
      u+= e;
      ++count;
  }
  if(!fixSx)
    rpo.sx = vv[count++];
  else
    rpo.sx = sx;
  
  if(!fixSz)
    rpo.sz = vv[count++];
  else
    rpo.sz = sz;
  
  if(!fixT)
    rpo.T = vv[count++];
  else
    rpo.T = T;

  if(!fixRe)
    rpo.Re = vv[count++];
  else
    rpo.Re = Re;

  
//  cout << "vec2rpo: L2Norm(u) = " << L2Norm(*(e.u)) << endl;
  VecRestoreArray(v, &vv);
}

void DNSContext::rpo2vec(const RPO& e, Vec &v) const
{
  int ct = 0;
  for(vector<RealProfileNG>::const_iterator i = basis.begin(); i!=basis.end(); ++i)
      VecSetValue(v, ct++, L2InnerProduct(*(e.u), *i), INSERT_VALUES);
  
  if(!fixSx)
    VecSetValue(v,ct++,e.sx,INSERT_VALUES);
  if(!fixSz)
    VecSetValue(v,ct++,e.sz,INSERT_VALUES);
  if(!fixT)
    VecSetValue(v,ct++,e.T,INSERT_VALUES);
  if(!fixRe)
    VecSetValue(v,ct++,e.Re,INSERT_VALUES);

  VecAssemblyBegin(v);
  VecAssemblyEnd(v);
}

void DNSContext::field2vec(FlowField &u, Vec &v) const
{
  RPO tmp;
  tmp.u = &u;
  tmp.sx = 0; tmp.sz = 0; tmp.T = 0;
  rpo2vec(tmp, v);
}
void DNSContext::advance(FlowField &u, const Real sx, const Real sz, const Real T, const Real Re)
{
 // cout << "Advancing..." << endl;

  // Make best effort to meet check interval, while
  // using an interval which evenly divides integration time
  cout << Re << endl;
  int Nchecks = int(T / checkInterval + 1);
  Real checkInterval = T / Nchecks;

  TimeStep dt(dtGuess, dtMin, dtMax, checkInterval, CFLmin, CFLmax);

  cout << "Constructing DNS" << endl;
  DNS dns ( u, 1.0 / Re, dt, flags );
  cout << "Done." << endl;
  if(dt.adjust(dns.CFL()))
    dns.reset_dt(dt);
  
  Real t = 0;
  for(int i = 0; i < Nchecks; ++i)
  {
    dns.advance(u, p, dt.n());
    t += dt.dT();


    if (dt.adjust(dns.CFL()))
      dns.reset_dt(dt);
  }
  u.translate(sx, sz);
  for(vector<FieldSymmetry>::iterator s = symms.begin(); s!=symms.end(); ++s)
    {
      FieldSymmetry tau = optimizePhase(u, *s);
      cout << tau << endl;
      u *= tau;
    }
  dtGuess = dt.dt();

   
#ifdef SHIFT_TEST
  u.translate(0.1, 0);
#endif
}

// Got from Petsc Docs, credited to:
// M. Pernice and H. F. Walker. NITSOL: A Newton iterative solver for nonlinear systems. SIAM J. Sci.
// Stat. Comput., 19:302?318, 1998.
Real DNSContext::computeH(const FlowField &u, const FlowField &a)
{
  return erel * sqrt(1 + L2Norm(u)) / L2Norm(a);
}


