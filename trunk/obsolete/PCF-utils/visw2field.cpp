#include <iostream>
#include <fstream>
#include "utilities.h"
#include "channelflow/flowfield.h"

using namespace std;
using namespace channelflow;

int main(int argc, char ** argv)
{

  const string purpose = "Converts Viswanath format data to flowfield, files\n should be labeled LABEL.dat, LABEL{u,v,w}.dat";

  ArgList args(argc, argv,purpose);

  const string label = args.getstr("-l","--label","Label for files");
  const string outfile = args.getstr("-o","--out","u.ff","Output file");

  const string param_fname = label + ".dat";
  const string u_fname = label + "u.dat";
  const string v_fname = label + "v.dat";
  const string w_fname = label + "w.dat";
  ifstream params(param_fname.c_str(), ios::in);

  int Nz; params >> Nz; Nz *= 2;
  int Nx; params >> Nx; Nx *= 2;
  int Ny; params >> Ny; Ny += 1; 
  Real Lx; params >> Lx; Lx *= (2*M_PI);
  Real Lz; params >> Lz; Lz *= (2*M_PI);

  const Real a = -1; // y lower limit
  const Real b = 1; // y upper limit
  const int Nd = 3; // number of dimensions

  FlowField u(Nx, Ny, Nz, Nd, Lx, Lz, a, b, Physical, Physical);
  ifstream ufile(u_fname.c_str());
  ifstream vfile(v_fname.c_str());
  ifstream wfile(w_fname.c_str());

  for(int k = 0; k < Nz; ++k)
    for(int i = 0; i < Nx; ++i)
      for(int j = 0; j < Ny; ++j)
	{
	  ufile >> u(i,j,k,0); u(i,j,k,0) -= cos(M_PI*j/(Ny-1));
	  vfile >> u(i,j,k,1);
	  wfile >> u(i,j,k,2);
	}

  u.makeSpectral();
  //  u.cmplx(0,1,0,0) = 0;
  u.binarySave(outfile);

}
