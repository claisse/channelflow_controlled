// License declaration at end of file

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#ifdef HAVE_OCTAVE
#include <octave/oct.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#endif

#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"

#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("compute coefficients of ODE obtained by projecting NS onto orthonormal basis,\n"
		 "\t d(a_i)/dt = L_ij a_j + N_ijk a_j a_k");
  
  ArgList args(argc, argv, purpose);

  const string bdir    = args.getpath("-b", "--basisdir", "", "basis set directory");
  const int    N       = args.getint("-Nb", "--Nbasis",     "number of basis fields");
  const Real   eps     = args.getreal("-e", "--eps", 1e-14, "drop coeffs smaller than this");
  const Real  Reynolds = args.getreal("-Re", "--Reynolds", 400, "Reynolds number");
  const string odir    = args.getpath("-o", "--outdir", "", "output directory");
  const string Lname   = args.getstr("-L", "--Lname", "L.asc",  "filename for linear operator L_ij");
  const string Nname   = args.getstr("-N", "--Nname", "N.asc",  "filename for nonlinear operator N_ijk");
  
  args.check();
  mkdir(odir);
  args.save(odir);

  const char sp = ' ';
  const Real nu = 1.0/Reynolds;

  array<FlowField> e(N);
  for (int n=0; n<N; ++n) {
    cout << "reading in basis elem " << (bdir + "e" + i2s(n)) << endl;
    e[n] = FlowField(bdir + "e" + i2s(n));
  }

  cout << "Calculating linear operator L_ij ... " << endl;
  ofstream Los((odir + Lname).c_str());
  Los << setprecision(17);
  Los << "% linear operator Lij of dot(a_i) = L_ij a_j = N_ijk a_j a_k\n";
  Los << "% i j L_ij\n";
  
  for (int j=0; j<N; ++j) {
    FlowField lapl_ej = lapl(e[j]);
    cout << j << sp << flush;
    for (int i=0; i<N; ++i) {
      Real Lij = nu*L2IP(lapl_ej, e[i]);
      if (abs(Lij) > eps)
	Los << i+1 << sp << j+1 << sp << Lij << endl;
    }
  }
  cout << endl;

  cout << "Calculating nonlinear operator N_ijk... " << endl;
  ofstream Nos((odir + Nname).c_str());
  Nos << setprecision(17);
  Los << "% nonlinear operator N_ijk of dot(a_i) = L_ij a_j + N_ijk a_j a_k\n";
  Los << "% i j k N_ijk\n";

  for (int k=0; k<N; ++k) {
    cout << k << sp << flush;
    FlowField grad_ek = grad(e[k]);
    for (int j=0; j<N; ++j) {
      FlowField ej_dotgrad_ek = dot(e[j], grad_ek);
      for (int i=0; i<N; ++i) {
	Real Nijk = L2IP(ej_dotgrad_ek, e[i]);
	if (abs(Nijk) > eps)
	  Nos << i+1 << sp << j+1 << sp << k+1 << sp << Nijk << endl;
      }
    }
  }
  cout << endl;
}
      
      



      
    

