#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"

#include "utilities.h"

using namespace std;
using namespace channelflow;

Real project(const FlowField& u, const FieldSymmetry& s, int sign);

int main(int argc, char* argv[]) {

  string purpose("produce a plot of ||si tau(ax,az) u - tau(ax,az) u|| versus ax,az");
  ArgList args(argc, argv, purpose);

  const int  rn   = args.getint ("-r",   "--refectioon", 1, "r1, r2, or r3 reflection");
  const bool anti = args.getflag("-a",   "--antisymmetry", "compute antisymmetry projection");
  const Real da   = args.getreal("-da",  "--da",  0.01, "increment for ax and az");
  const string oname = args.getstr("-o", "--outfile", "p.asc",   "output filename");
  const string uname = args.getstr(1, "<field>",   "input flow field");

  args.check();
  args.save("./");

  FieldSymmetry r;
  int sx = 1;
  int sy = 1;
  int sz = 1;
  if (rn == 1) 
    sz = -1;
  else if (rn==2)
    sx = sy = -1;
  else if (rn==3)
    sx = sy = sz = -1;
  else {
    cerr << "Please choose value 1, 2, or 3 for -r option." << endl;
    exit(1);
  }
  int asign = anti ? -1 : 1;

  FlowField u(uname);
  Real unorm2 = L2Norm2(u);
  ofstream os(oname.c_str());

  for (Real ax = 0.0; ax<=1.0; ax += da) {
    cout << ax << ' ' << flush;
    for (Real az = 0.0; az<=1.0; az += da) {
      FieldSymmetry s(sx,sy,sz,ax,az);
      os << project(u, s, asign)/unorm2 << ' ';
    }
    os << endl;
  }
  cout << endl;
}

Real project(const FlowField& u, const FieldSymmetry& s, int sign) {
  FlowField Pu(u);
  if (sign<0)
    Pu -= s(u);
  else
    Pu += s(u);
  Pu *= 0.5;
  return L2Norm2(Pu);
}

