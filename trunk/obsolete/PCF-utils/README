PCF-utils README 
$Author: gibson $ $Date: 2007-02-13 19:05:16 -0400 (Tue, 13 Feb 2007) $

==============================================================================
Quick guide to PCF-utils

PCF-utils is a set of channelflow programs that perform commonly needed 
computations for research in plane Couette flow dynamics at the Georgia 
Tech Center for Nonlinear Science. 

The programs can be used as black boxes to produce and analyze PCF data.
This file describes the programs, their command-line arguments, and their
output. The programs can be compiled with "make arnoldi.x", for example.
Get a list of possible command-line arguments by running and program with 
the -h or --help option. 

==============================================================================
Installation: 

Within the PCF-utils directory, run

  make all
  make install

That'll place a number of executables in your channelflow-1.2.1/bin directory.
If "make all" fails because the compiler can't find an Octave installation, try
compilng without Octave

  


==============================================================================
How to integrate a flow

  # Construct a random field
  joe@foo$ randomfield.x -Nx 48 -Ny 35 -Nz 48 -a 1.14 -g 2.5 -s 0.5 -m 0.3 urand.48.35.48

  # Integrate Navier-Stokes with random field as initial condition
  joe@foo$ nice couette.x -T0 0 -T1 100 -o data urand.48.35.48.ff


==============================================================================
Tips on running PCF-utils

Long-running programs like couette.x can benefit from a few Unix tricks. For brevity,
let cpuhog.x be a program that takes a long time to run and prints a bunch of 
diagnostic information to the screen. A good way to run cpuhog.x is 

  joe@foo$ nohup nice cpuhog.x  >& cpuhog.out & 

'nohup' means 'no hang-up'. The program will continue running on the machine even 
if you kill your shell and log off the computer. 

'nice' reduces the priority of the program within Unix multitasking. 'nice -20' is 
as nice as you can get.

'>& cpuhog.out' redirects all program output to the file 'cpuhog.out'. 
'>  cpuhog.out' would send error messages to the screen and other output to cpuhog.out
 
The trailing '&' makes the program run in the background, so that you can type further 
commands into the shell immediately.

I also frequently run programs like this:

  joe@foo$ nice cpuhog.x  | tee cpuhog.out 

That copies all program output to 'cpuhog.out' *and* prints it to the screen.


Lastly, note that most PCF-utils programs save the way they were called in a file 
with a '.args' extension, to either the output directory or the current directory. 
E.g.

  joe@foo$ nohup nice couette.x -T0 0 -T1 100 -o data urand.48.35.48.ff > data/couette.out &

saves 'couette.x -T0 0 -T1 100 -o data urand.48.35.48.ff' to the file data/couette.args.



==============================================================================
Data organization
 
Set up different directories for different runs so that you don't overwrite one data 
set with another. You can mostly do this with the -o or --outdir option. I also have 
a larger directory structure for my work, like this

simulations/UB400                         (calcs associated with UB Re=400 eqb.)
simulations/UB400/newton0, newton1, ...   (newton searches eqb.)
simulations/UB400/arnoldi0, arnoldi1, ... (arnoldi calcs of eigenfuncs)
simulations/UBLBNB400                     (calcs combining data from many UB,NB,LB eqb)
simulations/UBLBNB400/UBef0data           (data dir for integration of UB + eps* ef0)

But whatever works for you.



==============================================================================
Plotting with Matlab scripts

to be written 


