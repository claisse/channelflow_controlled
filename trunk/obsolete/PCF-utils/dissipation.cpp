#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <sys/stat.h>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/periodicfunc.h"

#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("Compute the energy dissipation rate of a field");

  ArgList args(argc, argv, purpose);

  const string uname  = args.getstr (1, "<flowfield>", "input field");

  args.check();

  FlowField u(uname);

  u.makeSpectral();
  ChebyCoeff U(u.Ny(), u.a(), u.b(), Spectral);
  U[1] = 1;
  FlowField uU(u);
  uU += U;

  cout << dissipation(uU) << endl;
}
