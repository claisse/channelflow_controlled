
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sys/stat.h>

#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"

#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {
  
  string purpose("compute a poincare section of plane Couette flow");

  ArgList args(argc, argv, purpose);

  const string u0name = args.getstr("-u0", "--u0", "initial flowfield");
  const string label  = args.getstr("-l", "--label", "u", 
				    "prefix for output fields");
  const string normalName = args.getstr("-n", "--normal", 
					"name of normal field");
  const string outdir = args.getpath("-o", "--outdir", "data", "output dir");

  const Real T0  = args.getreal("-T0", "--T0",   0, "integration start time");
  const Real T1  = args.getreal("-T1", "--T1", 100, "integration end time");
  const Real dt0 = args.getreal("-dt", "--dt", 0.03125,"integration timestep");
  const Real dT  = args.getreal("-dT", "--dT", 1.0, "save interval");

  args.check();
  args.save(outdir);

  // Construct Navier-Stoke Integrator
  DNSFlags flags; 
  flags.timestepping = SBDF3;
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = Rotational;
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;

  // Define flow parameters
  const Real Reynolds = 400.0;
  const Real nu = 1.0/Reynolds;

  const int Ndt = iround(dT/dt0);
  const Real dt = dT/Ndt;
  
  const bool inttime = (abs(dT - int(dT)) < 1e-12) ? true : false;

  FlowField u(u0name);
  FlowField normal(normalName);
  
  // Define gridsize
  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();
  const Real Lx = u.Lx();
  const Real  a = u.a();
  const Real  b = u.b();
  const Real Lz = u.Lz();

  // Construct base flow for plane Couette: U(y) = y
  Vector y = chebypoints(Ny,a,b);
  
  cout << "Nx,Ny,Nz == " << Nx << ',' << Ny << ',' << Nz << endl;
  ChebyTransform trans(Ny);
  ChebyCoeff Ubase(Ny,a,b,Physical);
  for (int ny=0; ny<Ny; ++ny) 
    Ubase[ny] = y[ny];

  // Construct data fields: 3d velocity and 1d pressure
  cout << "Constructing u,q, and optimizing FFTW..." << flush;
  FlowField q(Nx,Ny,Nz,1,Lx,Lz,a,b);
  
  cout << "constructing DNS..." << flush;
  DNS dns(u, Ubase, nu, dt, flags, Real(T0));
  cout << "done" << endl;
 
  FlowField u0(u);
  FlowField tmp(u);
  FlowField ulast(u);
  Real sectionDistLast = 0.0;  // Distance from poincare section
  for (Real t=T0; t<=T1; t += dT) {
    
    Real l2norm = L2Norm(u);
    //Real bcnorm = bcNorm(u);
    //Real divnorm = divNorm(u);

    cout << "===============================================" << endl;
    cout << "           t == " << t << endl;
    cout << "          dt == " << dt << endl;
    cout << "         CFL == " << dns.CFL() << endl;
    cout << "   L2Norm(u) == " << l2norm << endl;
    //cout << "  divNorm(u) == " << divnorm << endl;  
    //cout << "   bcNorm(u) == " << bcnorm << endl;  
    cout << "       Ubulk == " << dns.Ubulk() << endl;
    cout << "       ubulk == " << Re(u.profile(0,0,0)).mean() << endl;
    cout << "        dPdx == " << dns.dPdx() << endl;

    dns.advance(u, q, Ndt);
	
	tmp = u;
	tmp -= u0;
	Real sectionDist = L2InnerProduct(tmp,normal);
	if(sectionDist > 0 && sectionDistLast < 0)
	{
		// Compute intersection by linear interpolation
		tmp = u;
		tmp -= ulast;
		tmp *= fabs(sectionDistLast / (sectionDist - sectionDistLast));	
		tmp += ulast;
		
    	tmp.binarySave(outdir + label + t2s(t, inttime));
	}
	sectionDistLast = sectionDist;
	ulast = u;
  }
  
  cout << "done!" << endl;
}

