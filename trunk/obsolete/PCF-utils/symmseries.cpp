#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"

#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("produce the optimal translations for s1,s2 symmetry of a u(t) time series");
  ArgList args(argc, argv, purpose);

  const int  T0     = args.getint("-T0", "--T0",      0,    "initial time");
  const int  T1     = args.getint("-T1", "--T1",      1000,  "end time");
  const int  dT     = args.getint("-dT", "--dT",      1,    "time increment");
  const bool s1opt  = args.getbool("-s1",  "--s1",      true, "optimize s1 symmetry");
  const bool s2opt  = args.getbool("-s2",  "--s2",      true, "optimize s2 symmetry");
  const Real damp   = args.getreal("-dm",  "--damping", 1.0, "damping factor for Newton steps");
  const Real eps     = args.getreal("-e",  "--eps", 1e-14, "stop Newton search when err<eps");
  const int  Nsteps  = args.getint ("-N",  "--Nsteps", 10, "max # Newton steps");
  const bool verbose = args.getflag("-v",  "--verbose", "print Newton iteration information");
  const string dir   = args.getpath("-d",  "--datadir", "data/" "data directory");
  const string oname = args.getstr(1, "<ofile>",   "output file");
  const int w = 16;

  args.check();
  args.save("./");

  FieldSymmetry s1( 1, 1,-1, 0.5, 0);
  FieldSymmetry s2(-1,-1, 1, 0.5, 0.5);
  FieldSymmetry tau(1,1,1, 0.0, 0.0);
  FlowField uanti;

  ofstream os(oname.c_str());
  os.setf(ios::left);

  os << "% ax az |anti1|^2 |anti2|^2  " << endl;
  for (int t=T0; t<=T1; t += dT) {
    cout << t << flush;
    FlowField u(dir + "u" + i2s(t));
    if (s1opt || s2opt)
      u *= tau;
    FieldSymmetry dtau(1,1,1, 0.0, 0.0); 
    if (s1opt) {
      FieldSymmetry t1 = optimizePhase(u, s1, Nsteps, eps, damp, verbose);
      dtau *= t1;
      cout << '.' << flush;
    }
    if (s2opt) {
      FieldSymmetry t2 = optimizePhase(u, s2, Nsteps, eps, damp, verbose);
      cout << '.' << flush;
      dtau *= t2;
    }
    if (s1opt || s2opt) {
      u *= dtau;
      tau *= dtau;
    }
    Real unorm2 = L2Norm2(u);
    os << setw(w) << tau.ax() << ' ' << setw(w) << tau.az() << ' ';
    uanti = u;  uanti -= s1(u); 
    os << setw(w) << 0.25*L2Norm2(uanti)/unorm2 << ' ';
    uanti = u;  uanti -= s2(u);
    os << setw(w) << 0.25*L2Norm2(uanti)/unorm2 << endl;
  }
  cout << endl;
}
