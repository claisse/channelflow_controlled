
#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/EIG.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "utilities.h"


using namespace std;
using namespace channelflow;

// This program is a damped Newton search code for PCF equilibria that
// uses GMRES to solve the Newton-step equations. The GMRES algorithm is
// modeled on the discussion in Lecture 35 of Trefethen and Bau's "Numerical
// Linear Algebra". 
// The dampled Newton search converges well when the initial guess is close
// to the ultimate solution, but I need to implement "hook-step" Newton to
// make the algorithm robust enough for finding new solutions.

// John Gibson, Thu Dec  7 15:29:46 EST 2006

// Newton algorithm for solving G(u) = 0. 
// Let u* be the solution, i.e. G(u*) = 0
// Suppose we start with an initial guess u near u*.
// Let u* = u + du.
// Then G(u*) = G(u + du) 
//          0 = G(u) + DG(u) du + O(|du|^2)
// Drop h.o.t and solve
//   DG(u) du = -G(u)
// Then start with new guess u' = u + du and iterate.
//
// Use GMRES to solve the Newton-step equation. GMRES requires multiple 
// calculations of the LHS DG(u) du for trial values du. These can be 
// approximated with finite differencing:
//   DG(u) du = G(u+du) - G(u)
// To use GMRES, we need to cast velocity fields such as du and DG(u) du 
// into vectors and vice versa. Do this with a real-valued, orthonormal, 
// div-free, dirichlet basis {Phi_m}
//   a_n = (u, Phi_n)
//     u = a_n Phi_n
// Then the Newton step equation
//  DG(u) du = -G(u)
// can be translated to a linear algebra problem   
//      A dx = b 
// via
//      dx_n = (du, Phi_n)
//       b_n = (-G(u), Phi_n)
//  (A dx)_n = (DG(u) du, Phi_n) = (G(u+du) - G(u), Phi_n)


// In this case the function to be zered is G(u) = F^T(u) - u, where F^T 
// is the time-T map of Navier-Stokes. computed by DNS.
void G(FlowField& u, FlowField& G_u, Real Reynolds, Real dt, Real T);

int main(int argc, char* argv[]) {

  // Assign parameters
  array<string> args = args2str(argc, argv);
  bool help = fgetarg("-help", args, false);

  // Optional arguments
  const bool pausing = bgetarg("-pause", false, args, help);
  const int Ngmres  = igetarg("-Ngmres", 30, args, help);
  const int Nnewton = igetarg("-Nnewton", 10, args, help); 

  const Real newton_damp = rgetarg("-damp", 1.0, args, help); 
  const Real EPS_gmres   = rgetarg("-eps_gmres", 1e-3, args, help);
  const Real EPS_newton  = rgetarg("-eps_newton", 1e-13, args, help);
  const Real EPS_dv      = rgetarg("-eps_linearize", 1e-7, args, help);
  const Real dt          = rgetarg("-dt", 0.0, args, help); 

  // Required arguments
  const string outdir    = pathfix(sgetarg("-odir", args, help));
  const Real Reynolds    = rgetarg("-Reynolds", args, help); 
  const Real T           = rgetarg("-T", args, help); 

  const string indir    = pathfix(sgetarg("-idir", args, help));
  const int T0 = igetarg("-T0", 0, args, help);
  const int T1 = igetarg("-T1", 1, args, help);
  const int dT = igetarg("-dT", 1, args, help);

  checkargs(args, help);
  copyargs(argc, argv, outdir);

  BasisFlags basisflags;
  basisflags.aBC = Diri;
  basisflags.bBC = Diri;
  basisflags.orthonormalize = true;
  basisflags.zerodivergence = true;

  // Don't forget to change the Reynolds number in G(u)!
  FlowField u0(indir + "u" + i2s(T0));

  // Define gridsize
  const int Nx = u0.Nx();
  const int Ny = u0.Ny();
  const int Nz = u0.Nz();
  const Real Lx = u0.Lx();
  const Real ya = u0.a();
  const Real yb = u0.b();
  const Real Lz = u0.Lz();
  
  const int kxmax = u0.kxmaxDealiased();
  const int kxmin = -kxmax;
  const int kzmin = 0;
  const int kzmax = u0.kzmaxDealiased();;

  cout << "Nx == " << Nx << endl;
  cout << "Ny == " << Ny << endl; 
  cout << "Nz == " << Nz << endl;
  cout << "kxmin == " << kxmin << endl;
  cout << "kxmax == " << kxmax << endl;
  cout << "kzmin == " << kzmin << endl;
  cout << "kzmax == " << kzmax << endl;

  cout << "constructing basis set. this takes a long time..." << endl;
  vector<RealProfile> basis = 
    realBasis(Ny, kxmax, kzmax, Lx, Lz, ya, yb, basisflags);

  const int Munk = basis.size(); // dimension of basis set
  const int Meqn = Munk+2;      // number of equations
  const int Ngmres2 = (Munk < Ngmres) ? Munk : Ngmres;
  cout << Meqn << " equations" << endl;
  cout << Munk  << "unknowns" << endl;
  checkBasis(basis, basisflags);

  ofstream osconv((outdir + "convergence.asc").c_str());
  ofstream tos((outdir + "t.asc").c_str());
  cout << setprecision(10);


  for (int t=T0; t<T1; t += dT) {
    cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    cout << "Beginning of time loop, t == " << t << endl;
    // Initialize Newton iteration 
  
    u0 = FlowField(indir + "u" + i2s(t));
    FlowField    u(u0);
    FlowField    p(Nx, Ny, Nz, 1, Lx, Lz, ya, yb); // pressure 
    FlowField   du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // Newton step
    FlowField utmp(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // Newton iterate

    ColumnVector a(Munk);

    Real init_newton_residual = 0.0;
    Real prev_newton_residual = 0.0;
    Real prev_u0_error = 0.0;

    int colcount = 0;
    for (int newton_step = 0; newton_step<Nnewton+1; ++newton_step) {
    
      cout << "==================================================" << endl;
      cout << "Beginning of Newton loop for " << endl;
      cout << "          t == " << t << endl;
      cout << "Newton step == " << newton_step << endl;
  
      // Calculate G(u) = F^T(u) - u
      FlowField G_u;
      G(u, G_u, Reynolds, dt, T);

      Real u0_error = L2Dist(u,u0);
      Real newton_residual = L2Norm(G_u); 

      if (newton_step == 0) {
	init_newton_residual = newton_residual;
	prev_newton_residual = newton_residual;
      }

      // Calculate two unit vectors to which du must be orthogonal
      // dudx = tangent vector of x translation
      // dudz = tangent vector of z translation

      FlowField dudx = xdiff(u);
      FlowField dudz = zdiff(u);
      dudx *= 1.0/L2Norm(dudx);
      dudz *= 1.0/L2Norm(dudz);

      cout << "Current state of Newton iteration:" << endl;
      cout << " L2Norm(u) == " << L2Norm(u) << endl;
      cout << " bcNorm(u) == " << bcNorm(u) << endl;
      cout << "divNorm(u) == " << divNorm(u) << endl;
      cout << "residual L2Norm(G(u)) == " << newton_residual << endl;
      cout << "previous residual     == " << prev_newton_residual<< endl;
      cout << "initial  residual     == " << init_newton_residual << endl;
      cout << "prev step L2Norm(du)  == " << L2Norm(du) <<endl;
      cout << "total L2Dist(u,u0)    == " << u0_error << endl;

      osconv << newton_residual << ' ';
      tos << t << endl;
      ++colcount;

      prev_u0_error = u0_error;
      prev_newton_residual = newton_residual;

      if (newton_residual < EPS_newton) {
	cout << "Newton search converged. Breaking." << endl;
	break;
      }
      else if (newton_step == Nnewton) {
	cout << "Reached maximum number of Newton steps. Breaking." << endl;
	break;
      } 
      else {
	cout << "Continuing Newton search. Pausing...." << endl;
	if (pausing)
	  cfpause();
      }
	  
      // Initialize matrices for GMRES algorithm
      Matrix H(Ngmres+1,Ngmres);
      Matrix Q(Meqn,Ngmres+1);
      setToZero(H);
      setToZero(Q);


      // Set up RHS vector b = (-field2vector(G(u)), 0, 0); 
      // (0, 0) is the RHS of eqn enforcing orthog. to (dudx, dudz)
      ColumnVector b(Meqn);
      setToZero(b);
      field2vector(basis, G_u, b);
      for (int m=Meqn-2; m<Meqn; ++m)  
	b(m) = 0.0;
      b *= -1.0;

      // Place normalized initial b as first col of Q
      // b *= Real(1.0/L2Norm(b)); // huh?
      ColumnVector q0(b);
      q0 *= 1/L2Norm(q0);
      //cout << "L2Norm(q0)     == " << L2Norm(q0) << endl;
      Q.insert(q0,0,0);

      // ===============================================================
      // GMRES iteration to solve DG(u) du = -G(u) for du
      for (int n=0; n<Ngmres2; ++n) {
	bool breakdown = false;

	cout << "------------------------------------------------" << endl;
	cout << "Beginning of GMRES loop for " << endl;
	cout << "          t == " << t << endl;
	cout << "Newton step == " << newton_step << endl;
	cout << "GMRES  step == " << n << endl;

	// Compute v = Ab in Arnoldi iteration terms, where b is Q.column(n)
	// In Navier-Stokes terms, the main quantity to compute is
	//   L dv = DG(u) dv

	// Use approximation 
	// DG(u) dv = G(u+dv) - G(u)

	// But before the RHS expression, rescale dv to be very small so that 
	// the expression is approx linear in dv, and then unscale it and the 
	// result afterwards. Rescale so that L2Norm(dv) = EPS_dv << 1

	cout << "building field dv from Q(n,:) ..." << endl;
	FlowField dv(Nx,Ny,Nz,3,Lx,Lz,ya,yb);
	vector2field(basis, Q.column(n), dv);
	Real eps = EPS_dv/L2Norm(dv);
	dv *= eps;
	//cout << "       rescaled L2Norm(dv) == " << L2Norm(dv) << endl;

	cout << "computing G(u+dv)..." << endl;
	FlowField u_dv;
	u_dv = u;   
	u_dv += dv;
	FlowField G_u_dv;
	G(u_dv, G_u_dv, Reynolds, dt, T);

	//cout << "       L2Norm(Q(:,n)) == " << L2Norm(Q.column(n)) << endl;
	//cout << "           L2Norm(dv) == " << L2Norm(dv) << endl;
	//cout << "           bcNorm(dv) == " << bcNorm(dv) << endl;
	//cout << "          divNorm(dv) == " << divNorm(dv) << endl;
	//cout << "         L2Norm(u+dv) == " << L2Norm(u_dv) << endl;
	//cout << "         bcNorm(u+dv) == " << bcNorm(u_dv) << endl;
	//cout << "        divNorm(u+dv) == " << divNorm(u_dv) << endl;
	//cout << " L2Norm(G(u))         == " << L2Norm(G_u) << endl;
	//cout << " L2Norm(G(u+dv))      == " << L2Norm(G_u_dv) << endl;
	//cout << " L2Dist(G(u+dv),G(u)) == " << L2Dist(G_u_dv, G_u) << endl;

	// Compute DG(u) dv = G(u+dv) - G(u)
	FlowField DG_dv;
	DG_dv  = G_u_dv;
	DG_dv -= G_u;
      
	cout << "converting field L_dv to vector v..." << endl;
	ColumnVector v(Meqn);
	field2vector(basis, DG_dv, v);
	v *= 1.0/eps;
	Real dvnorm = L2Norm(dv);
	v(Meqn-2) = L2InnerProduct(dv, dudx)/dvnorm;
	v(Meqn-1) = L2InnerProduct(dv, dudz)/dvnorm;
	//cout << "                L2Norm(v)  == " << L2Norm(v) << endl;
    
	cout << "calculating arnoldi iterate..." << endl;
	for (int j=0; j<=n; ++j) {
	  cout << j << ' ' << flush;
	  ColumnVector Qj = Q.column(j);
	  H(j,n) = Qj.transpose() * v;
	  v -= H(j,n)*Qj;
	}
	cout << endl;
	Real vnorm = L2Norm(v);
	//cout << "                L2Norm(v)  == " << L2Norm(v) << endl;
	if (abs(vnorm) < EPS_gmres) {
	  H(n+1, n) = 0.0;
	  v *= 0.0;
	  breakdown = true;
	}
	else {
	  H(n+1, n) = vnorm;
	  v *= 1.0/vnorm;
	}

	Q.insert(v,0,n+1);

	// Now minimize ||Ax-b|| within nth Krylov subspace. That amounts to
	// finding least-squares soln of Hn y = bk via QR where Hn is n+1 x n
	// submatrix of H.

	// where bk = [norm(b) 0 0 0 ...]'
	cout << "extracting submatrices for subspace least squares..." << endl;
	Matrix Hn = H.extract(0,0,n+1,n);
	Matrix Qn = Q.extract(0,0,Meqn-1,n);
	ColumnVector bn(n+2);
	setToZero(bn);
	bn(0) = L2Norm(b);

	cout << "least squares solve..." << endl;
	ColumnVector dy(n+1);
	setToZero(dy);

	Real gmres_residual;
	leastsquares(Hn,bn,dy,gmres_residual);
	ColumnVector dx = Qn*dy;
	vector2field(basis, dx, du);

	// Approximate DG(u) du = G(u+du) - G(u)
	cout << "L2Norm(Ax-b)/L2Norm(b) == " << gmres_residual << endl;
      
	if (gmres_residual < EPS_gmres) {
	  cout << "GMRES converged. Breaking." <<endl << endl;
	  break;
	}
	else if (breakdown) {
	  cout << "GMRES breakdown. Exiting." << endl;
	  exit(1);
	}
	else if (n==Ngmres-1) 
	  cout << "GMRES failed to converge. Continuing Newton with best guess"
	       << endl;
	else 
	  cout << "Continuing GMRES iteration." << endl;
      
	cout << "End of GMRES loop" << endl;
      }
      cout << "Past end of GMRES loop" << endl;

      // End of GMRES iteration. Assume that we have a decent solution for du.
      // Update u and take another Newton step.
      FlowField u_du(u);
      u_du += du;

      FlowField G_u_du;
      G(u_du, G_u_du, Reynolds, dt, T);

      FlowField DG_du(G_u_du);
      DG_du -= G_u;

      cout << "L2Norm(u)               == " << L2Norm(u) <<endl;  
      //cout << "L2Norm(u0)              == " << L2Norm(u0) <<endl;  
      //cout << "L2Dist(u,u0)            == " << L2Dist(u,u0) <<endl;  
      cout << "L2Norm(du)              == " << L2Norm(du) <<endl;
      cout << "L2Norm(G(u))            == " << L2Norm(G_u) <<endl;
      cout << "L2Norm(DG(u) du)        == " << L2Norm(DG_du) <<endl;
      G_u *= -1;
      cout << "L2Dist(DG(u) du, -G(u)) == " << L2Dist(DG_du, G_u) <<endl;
      G_u *= -1;

      du *= newton_damp;
      u += du;

      u.binarySave(outdir + "uiterate" + i2s(newton_step));
      u.binarySave(outdir + "uEQB");
      cout << "End of Newton loop" << endl;

    }
    cout << "Past end of Newton loop" << endl;
    for (int n=colcount; n<Nnewton+1; ++n) 
      osconv << prev_newton_residual << endl;
    osconv << endl;

    cout << "End of time loop" << endl;
  }
  cout << "Past end of time loop" << endl;
}
      
void G(FlowField& u, FlowField& G_u, Real Reynolds, Real dtarg, Real T) {

  const Real nu = 1.0/Reynolds;
  const Real dtmin = 0.01;
  const Real dtmid = 0.14;
  const Real dtmax = 0.2;
  const Real CFLmin = 0.4;
  const Real CFLmax = 0.6;

  const bool variable_dt = (dtarg == 0.0) ? true : false;
  //const Real dtinit = (variable_dt) ? dtmax/2 : dtarg;
  const Real dT     = (variable_dt) ? lesser(1.0, T) : T;
  TimeStep dt(dtmid, dtmin, dtmax, dT, CFLmin, CFLmax);

  DNSFlags flags; 
  flags.timestepping = SBDF3;
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = Rotational;
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;

  const int Ny = u.Ny();
  const Real ya = u.a();
  const Real yb = u.b();

  cout << "G: setting up DNS..." << endl;
  G_u = u;
  FlowField p(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b());

  ChebyTransform trans(Ny);
  ChebyCoeff U(Ny,ya,yb,Physical);
  Vector y = chebypoints(Ny,ya,yb);
  for (int ny=0; ny<Ny; ++ny) 
    U[ny] = y[ny];
  U.makeSpectral(trans);
  
  cout << "G: initializing DNS..." << endl;
  DNS dns(G_u, U, nu, dt, flags);
  cout << "G: CFL == " << dns.CFL() << endl;
  cout << "G:  dt == " << dt << endl;

  if (variable_dt && dt.adjust(dns.CFL())) {
    cout << "G : adjusting timestep" << endl;
    dns.reset_dt(dt);
  }
  
  for (Real t=0; t<T; t+=dT) {
    dns.advance(G_u, p, dt.n());
    if (variable_dt && dt.adjust(dns.CFL())) {
      cout << "G : adjusting timestep" << endl;
      dns.reset_dt(dt);
    }
  }
      
  G_u -= u;

  cout << "G: CFL == " << dns.CFL() << endl;
  cout << "G: returning..." << endl;
}

