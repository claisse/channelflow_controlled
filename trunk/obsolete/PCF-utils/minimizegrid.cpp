#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("move flow field to minimal grid ");

  ArgList args(argc, argv, purpose);  // Assign parameters

  const Real eps  = args.getreal("-e", "--eps", 1e-15, "noise floor");
  string uinname  = args.getstr(1, "<infield>", "input field");
  string uoutname = args.getstr(2, "<outlabel>", "prefix for output file");
  args.check();

  FlowField u(uinname);

  u.makeSpectral();
  
  int kxmax=0;
  int kzmax=0;
  int kymax=0;
  for (int mx=0; mx<u.Mx(); ++mx) {
    const int kx = u.kx(mx);
    for (int mz=0; mz<u.Mz(); ++mz) {
      const int kz = u.kz(mz);
      BasisFunc prof = u.profile(kx,kz);
      if (L2Norm(u.profile(mx,mz)) > eps) {
	kxmax = Greater(kxmax, abs(kx));
	kzmax = Greater(kzmax, abs(kz));
      }
      for (int ky=0; ky<u.Ny(); ++ky) {
	Real sum = 0.0;
	for (int i=0; i<u.Nd(); ++i)
	  sum += abs(prof[i][ky]);
	if (sum > eps)
	  kymax = Greater(kymax, ky);
      }
    }
  }
  const int Nx = 3*(kxmax+1);
  const int Ny = kymax+1;
  const int Nz = 3*(kzmax+1);

  cout << "Energy over " << eps << " is confined to : \n";
  cout << "|kx| <= " << kxmax << endl;
  cout << "|ky| <= " << kymax << endl;
  cout << "|kz| <= " << kzmax << endl;
  cout << "Saving on minimum dealiased grid : " 
       << 3*(kxmax+1) << " x " 
       << kymax+1 << " x " 
       << 3*(kzmax+1) << endl;
  
  FlowField uout(Nx,Ny,Nz,u.Nd(),u.Lx(),u.Lz(),u.a(),u.b());
  uout.interpolate(u);

  string dot = ".";
  uoutname += dot + i2s(Nx) + dot + i2s(Ny) + dot + i2s(Nz);
  uout.binarySave(uoutname);

  cout << setprecision(16);
  cout << " L2Norm(uin)  == " << L2Norm(u) << endl;
  cout << " L2Norm(uout) == " << L2Norm(uout) << endl;
  cout << " bcNorm(uin)  == " << bcNorm(u) << endl;
  cout << " bcNorm(uout) == " << bcNorm(uout) << endl;
  if (u.Nd() == 3) {
    cout << "divNorm(uin)  == " << divNorm(u) << endl;
    cout << "divNorm(uout) == " << divNorm(uout) << endl;
  }
  
}

