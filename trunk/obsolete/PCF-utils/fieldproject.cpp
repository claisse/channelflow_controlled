#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"

#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("project a field onto a symmetric subspace");

  ArgList args(argc, argv, purpose);
 
  const int  sn   = args.getint ("-s",  "--symmetry", 0,  "s1, s2, or s3 symmetry (0 => use sx,sy,sx,ax,az params)");
  const bool anti = args.getflag("-a",  "--anti",         "project onto antisymmetric subspace");
  const int  sx   = args.getint ("-sx", "--sx",       1,  "u,x -> -u,-x");
  const int  sy   = args.getint ("-sy", "--sy",       1,  "v,y -> -v,-y");
  const int  sz   = args.getint ("-sz", "--sz",       1,  "w,z -> -w,-z");
  const Real ax   = args.getreal("-ax", "--ax",     0.0,  "x -> x + ax/Lx");
  const Real az   = args.getreal("-az", "--az",     0.0,  "z -> z + az/Lz");
  const string iname = args.getstr(1, "<ifile>",   "input field");
  const string oname = args.getstr(1, "<ofile>",   "output field");

  args.check();
  args.save("./");

  
  FieldSymmetry s;
  if (sn == 1)
    s = FieldSymmetry( 1, 1,-1, 0.5, 0);
  else if (sn==2)
    s = FieldSymmetry(-1,-1, 1, 0.5, 0.5);
  else if (sn==3)
    s = FieldSymmetry(-1,-1,-1, 0.0, 0.5);
  else {
    s = FieldSymmetry(sx,sy,sz,ax,az);
  }
  cout << "Projecting onto " << s << (anti ? " antisymmetric " : " ") << "subspace" << endl;

  FlowField u(iname);
  FlowField Pu(u);
  
  if (anti)
    Pu -= s(u);
  else 
    Pu += s(u);

  Pu *= 0.5;
  
  Pu.binarySave(oname);
}
