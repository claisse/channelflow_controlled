#include <iostream>
#include <fstream>
#include "channelflow/flowfield.h"
#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char** argv)
{

  string purpose("Projects the torus of translations of an equilibrium onto three fields");

  ArgList args(argc, argv, purpose);

  const int Nx = args.getint("-nx", "--nx", 16, "Number of points to use for x-translation");
  const int Nz = args.getint("-nz", "--nz", 16, "Number of points to use for x-translation");
  const string uname  = args.getstr(1, "<flowfield>", "input field");
  const string ef1_str = args.getstr(4, "<flowfield>", "first basis element");
  const string ef2_str = args.getstr(3, "<flowfield>", "second basis element");
  const string ef3_str = args.getstr(2, "<flowfield>", "third basis element");
  args.check();



  FlowField ef1(ef1_str);
  FlowField ef2(ef2_str);
  FlowField ef3(ef3_str);
  FlowField u(uname);


  FieldSymmetry tx(1, 1, 1, 1.0 / Nx, 0, 1);
  FieldSymmetry tz(1, 1, 1, 0, 1.0 / Nz, 1);
  for(int nx = 0; nx < Nx; ++nx)
  { 
    for(int nz = 0; nz < Nz; ++nz)
	{
	  Real a1 = L2IP(u, ef1);
	  Real a2 = L2IP(u, ef2);
	  Real a3 = L2IP(u, ef3);
	  u *= tz;
	  cout << a1 << '\t' << a2 << '\t' << a3 << endl;

	}
	u *= tx;
  }



  return 0;
}
