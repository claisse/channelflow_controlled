#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"

#include "quicksort.h"
#include "utilities.h"

using namespace std;
using namespace channelflow;


int main(int argc, char* argv[]) {

  string purpose("computes a translation-symmetric basis from a given field in s1,s2 symmetric subspace");
  ArgList args(argc, argv, purpose);

  const string bdir    = args.getpath("-b", "--basisdir", "basisTrans/", "basis set directory");
  const string uname   = args.getstr(1, "<flowfield>", "the given flowfield");

  args.check();
  args.save("./");

  mkdir(bdir);

  FieldSymmetry taux(1,1,1,0.5,0.0);
  FieldSymmetry tauz(1,1,1,0.0,0.5);

  FlowField u(uname);
  FlowField txu = taux(u);
  FlowField tzu = tauz(u);
  FlowField txzu = tauz(txu);
  
  FlowField e[4]; 

  e[0]  = u;
  e[0] += txu;
  e[0] += tzu;
  e[0] += txzu;
  
  e[1]  = u;
  e[1] += txu;
  e[1] -= tzu;
  e[1] -= txzu;

  e[2]  = u;
  e[2] -= txu;
  e[2] += tzu;
  e[2] -= txzu;

  e[3]  = u;
  e[3] -= txu;
  e[3] -= tzu;
  e[3] += txzu;

  for (int n=0; n<4; ++n) {
    e[n] *= 1.0/L2Norm(e[n]);
    e[n] *= 1.0/L2Norm(e[n]);
    e[n].binarySave(bdir + "e" + i2s(n));
  }

}
