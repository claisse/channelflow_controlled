// License declaration at end of file

#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/EIG.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "utilities.h"


using namespace std;
using namespace channelflow;

// This program is a damped Newton search code for PCF equilibria that
// uses GMRES to solve the Newton-step equations. The GMRES algorithm is
// modeled on the discussion in Lecture 35 of Trefethen and Bau's "Numerical
// Linear Algebra". 
// The dampled Newton search converges well when the initial guess is close
// to the ultimate solution, but I need to implement "hook-step" Newton to
// make the algorithm robust enough for finding new solutions.

// John Gibson, Thu Dec  7 15:29:46 EST 2006

// Newton algorithm for solving G(w) = G(u, tx, tz) = 0. 
// (tx,tz) are the finite translations of the relative EQN for integ time T.
// G(u,tx,tz) = translate_{-tx,-tz}(F^T(u))
//
// Let w* = (u*,tx*,tz*) be the solution, i.e. G(w*) = G(u*,tx*,tz*) = 0.
// Suppose we start with an initial guess w near w*. Let w* = w + dw, 
// Then G(w*) = G(w + dw)
//          0 = G(w) + DG(w) dw + O(|dw|^2)
// Drop h.o.t and solve
//   DG(w) dw = -G(w)
// Then start with new guess w' = w + dw and iterate.
//
// Use GMRES to solve the Newton-step equation. GMRES requires multiple 
// calculations of the LHS DG(w) dw for trial values dw. These can be 
// approximated with finite differencing:
//   DG(w) dw = G(w+dw) - G(w)
// To use GMRES, we need to cast velocity fields such as du and DG(u) du 
// into vectors and vice versa. Do this with a real-valued, orthonormal, 
// div-free, dirichlet basis {Phi_m}
//   a_m     = (u, Phi_m),  m = 0:(M-3)
//   a_{M-2} = tx
//   a_{M-1} = tz
//
//     u = a_m Phi_m, m = 0:(M-3)
// 
// Then the Newton step equation for dw = (du, d(tx), d(tz))
//
//  DG(w) dw = -G(w)
//
// with constraints
//   (du, du/dx) = 0  (step is orthogonal to x-translation direction)
//   (du, du/dz) = 0
//
// can be translated to a linear algebra problem   
//    A da = b 

// via unknowns
//  da_m     = (du, Phi_m),  m = 0:(M-3)
//  da_{M-2} = d(tx)
//  da_{M-1} = d(tz)

// and equations
// 
//  (A da)_m     = (DG(w) dw, Phi_m) = (G(w+dw) - G(w), Phi_m), m=0:M-3
//  (A da)_{M-2} = (du, du/dx) 
//  (A da)_{M-1} = (du, du/dz) 
//
//       b_m = (-G(u), Phi_m)
//   b_{M-2} = 0
//   b_{M-1} = 0

// In this case the function to be zered is G(u) = F^T(u) - u, where F^T 
// is the time-T map of Navier-Stokes. computed by DNS.
void G(FlowField& u, FlowField& G_u, Real Reynolds, Real dt, Real T,
       Real tx, Real tz);

int main(int argc, char* argv[]) {

  // Assign parameters
  array<string> args = args2str(argc, argv);
  bool help = fgetarg("-help", args, false);

  // Optional arguments
  const bool pausing = bgetarg("-pause", false, args, help);
  const int Ngmres  = igetarg("-Ngmres", 30, args, help);
  const int Nnewton = igetarg("-Nnewton", 10, args, help); 

  const Real newton_damp = rgetarg("-damp", 1.0, args, help); 
  const Real EPS_gmres   = rgetarg("-eps_gmres", 1e-3, args, help);
  const Real EPS_newton  = rgetarg("-eps_newton", 1e-13, args, help);
  const Real EPS_dv      = rgetarg("-eps_linearize", 1e-7, args, help);
  const Real dt          = rgetarg("-dt", 0.0, args, help); 
  const bool relativeEQB = bgetarg("-reqb", false, args, help);

  Real tx = rgetarg("-xtrans", 0.0, args, help);
  Real tz = rgetarg("-ztrans", 0.0, args, help);

  // Required arguments
  const string outdir    = pathfix(sgetarg("-odir", args, help));
  const Real Reynolds    = rgetarg("-Reynolds", args, help); 
  const Real T           = rgetarg("-T", args, help); 
  const string u0name    = sgetarg(1, "<u0>", args, help);

  checkargs(args, help);
  copyargs(argc, argv, outdir);

  BasisFlags basisflags;
  basisflags.aBC = Diri;
  basisflags.bBC = Diri;
  basisflags.orthonormalize = true;
  basisflags.zerodivergence = true;

  // Don't forget to change the Reynolds number in G(u)!
  FlowField u0(u0name);

  //const int P = 4;
  //const string plotbasis("UBproj0/basis/e");
  //array<FlowField> e(P);
  //for (int p=0; p<P; ++p)
  //e[p] = FlowField(plotbasis + i2s(p));

  // Define gridsize
  const int Nx = u0.Nx();
  const int Ny = u0.Ny();
  const int Nz = u0.Nz();
  const Real Lx = u0.Lx();
  const Real ya = u0.a();
  const Real yb = u0.b();
  const Real Lz = u0.Lz();
  
  const int kxmax = u0.kxmaxDealiased();
  const int kxmin = -kxmax;
  const int kzmin = 0;
  const int kzmax = u0.kzmaxDealiased();;

  cout << "Nx == " << Nx << endl;
  cout << "Ny == " << Ny << endl; 
  cout << "Nz == " << Nz << endl;
  cout << "kxmin == " << kxmin << endl;
  cout << "kxmax == " << kxmax << endl;
  cout << "kzmin == " << kzmin << endl;
  cout << "kzmax == " << kzmax << endl;

  cout << "constructing basis set. this takes a long time..." << endl;
  vector<RealProfile> basis = 
    realBasis(Ny, kxmax, kzmax, Lx, Lz, ya, yb, basisflags);

  // RelativeEQB EQB search has two extra unknowns for translation directions
  const int Munk = basis.size() + ((relativeEQB) ? 2 : 0); 
  const int Meqn = basis.size() + 2;
  const int Ngmres2 = (Munk < Ngmres) ? Munk : Ngmres;
  cout << Meqn << " equations" << endl;
  cout << Munk << " unknowns" << endl;
  checkBasis(basis, basisflags);


  // ==============================================================
  // Initialize Newton iteration 
  
  FlowField    u(u0);
  FlowField    p(Nx, Ny, Nz, 1, Lx, Lz, ya, yb); // pressure 

  FlowField utmp(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // Newton iterate
  FlowField   du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // Newton step
  Real dtx = 0.0; // Newton step for x-translation
  Real dtz = 0.0; // Newton step for z-translation

  cout << "Identity test on u0 and basis..." << endl;
  u.setToZero();
  ColumnVector a(Munk);
  field2vector(basis, u0, a);
  vector2field(basis, a, u);

  cout << "L2Dist(u, a_n Phi_n) == " << L2Dist(u0, u) << endl;

  Real init_newton_residual = 0.0;
  Real prev_newton_residual = 0.0;
  Real prev_u0_error = 0.0;
  ofstream osconv((outdir + "convergence.asc").c_str());
  cout << setprecision(10);
  
  for (int newton_step = 0; newton_step<Nnewton+1; ++newton_step) {
    
    cout << "========================================================" << endl;
    cout << "Newton iteration number " << newton_step << endl;
  
    // Calculate G(u, tx, tz) = translate_{-tx,-tz}(F^T(u)) - u
    FlowField G_u;
    G(u, G_u, Reynolds, dt, T, tx, tz);
    
    Real u0_error = L2Dist(u,u0);
    Real newton_residual = L2Norm(G_u); 

    if (newton_step == 0) {
      init_newton_residual = newton_residual;
      prev_newton_residual = newton_residual;
    }

    // Calculate two unit vectors to which du must be orthogonal
    // dudx = tangent vector of x translation
    // dudz = tangent vector of z translation

    FlowField dudx = xdiff(u);
    FlowField dudz = zdiff(u);
    dudx *= 1.0/L2Norm(dudx);
    dudz *= 1.0/L2Norm(dudz);

    cout << "Current state of Newton iteration:" << endl;
    cout << " L2Norm(u) == " << L2Norm(u) << endl;
    cout << " bcNorm(u) == " << bcNorm(u) << endl;
    cout << "divNorm(u) == " << divNorm(u) << endl;
    cout << "residual L2Norm(G(u)) == " << newton_residual << endl;
    cout << "previous residual     == " << prev_newton_residual<< endl;
    cout << "initial  residual     == " << init_newton_residual << endl;
    cout << "prev step L2Norm(du)  == " << L2Norm(du) <<endl;
    cout << "total L2Dist(u,u0)    == " << u0_error << endl;

    osconv << setw(12) << newton_residual 
	   << setw(12) << bcNorm(u)
	   << setw(12) << divNorm(u) << endl;
    
    //for (int p=0; p<P; ++p) 
    //stepos << L2IP(u, e[p]) << ' ';
    //stepos << endl;

    prev_u0_error = u0_error;
    prev_newton_residual = newton_residual;

    if (newton_residual < EPS_newton) {
      cout << "Newton search converged. Breaking." << endl;
      break;
    }
    else if (newton_step == Nnewton) {
      cout << "Reached maximum number of Newton steps. Breaking." << endl;
      break;
    } 
    else {
      cout << "Continuing Newton search. Pausing...." << endl;
      if (pausing)
	cfpause();
    }
	  
    // Initialize matrices for GMRES algorithm
    Matrix H(Ngmres+1,Ngmres);
    Matrix Q(Meqn,Ngmres+1);
    setToZero(H);
    setToZero(Q);

    // Set up RHS vector b = (-field2vector(G(u,tx,tz)), 0, 0); 
    // (0, 0) is the RHS of eqn enforcing orthog. to (du/dx, du/dz)
    ColumnVector b(Meqn);
    setToZero(b);
    field2vector(basis, G_u, b);
    for (int m=Meqn-2; m<Meqn; ++m)  
      b(m) = 0.0;
    b *= -1.0;

    // Place normalized initial b as first col of Q
    // b *= Real(1.0/L2Norm(b)); // huh?
    ColumnVector q0(b);
    q0 *= 1/L2Norm(q0);
    Q.insert(q0,0,0);

    // ===============================================================
    // GMRES iteration: solve DG(u,tx,tz) dot (du,dtx,dtz) = -G(u,tx,tz) 
    for (int n=0; n<Ngmres2; ++n) {
      bool breakdown = false;

      cout << "------------------------------------------------" << endl;
      cout << "Newton, GMRES number " << newton_step << ", " << n << endl;

      // Compute v = Ab in Arnoldi iteration terms, where b is Q.column(n)
      // In Navier-Stokes terms, the main quantity to compute is
      //   L (dv,dtx,dtz) = DG(u,tx,tz) (dv, dtx, dtz)

      // Use approximation 
      // DG(u,tx,tz) (dv, dtx, dtz) = G(u+dv, tx+dtx, tz+dtz) - G(u, tx, tz)

      // But before the RHS expression, rescale (dv,dtx,dtz) to be very small
      // so that expression is approx linear in dv, and then unscale it and 
      // the result afterwards. Rescale so that L2Norm(dv) = EPS_dv << 1

      cout << "building field dv and translates dtx,dtz from Q(n,:) ..."<<endl;
      FlowField dv(Nx,Ny,Nz,3,Lx,Lz,ya,yb);
      vector2field(basis, Q.column(n), dv);
      dtx = (relativeEQB) ? Q(Munk-2,n) : 0.0;
      dtz = (relativeEQB) ? Q(Munk-1,n) : 0.0;

      // Rescale to get dv small enough for linear approx 
      // L (dv,dtx,dtz) = G(u+dv, tx+dtx, tz+dtz)-G(u, tx, tz)
      Real eps = EPS_dv/L2Norm(dv);
      dv  *= eps;
      dtx *= eps;
      dtz *= eps;

      cout << "computing G(u+dv, tx+dtx, tz+dtz)..." << endl;
      FlowField u_dv;
      u_dv = u;   
      u_dv += dv;
      FlowField G_u_dv;
      G(u_dv, G_u_dv, Reynolds, dt, T, tx+dtx, tz+dtz);


      //cout << "       L2Norm(Q(:,n)) == " << L2Norm(Q.column(n)) << endl;
      //cout << "           L2Norm(dv) == " << L2Norm(dv) << endl;
      //cout << "           bcNorm(dv) == " << bcNorm(dv) << endl;
      //cout << "          divNorm(dv) == " << divNorm(dv) << endl;
      //cout << "         L2Norm(u+dv) == " << L2Norm(u_dv) << endl;
      //cout << "         bcNorm(u+dv) == " << bcNorm(u_dv) << endl;
      //cout << "        divNorm(u+dv) == " << divNorm(u_dv) << endl;
      //cout << " L2Norm(G(u))         == " << L2Norm(G_u) << endl;
      //cout << " L2Norm(G(u+dv))      == " << L2Norm(G_u_dv) << endl;
      //cout << " L2Dist(G(u+dv),G(u)) == " << L2Dist(G_u_dv, G_u) << endl;

      // Compute DG(u,tx,tz) (dv,dtx,dtz) = G(u+dv,tx+dtx,tz+dtz) - G(u,tx,tz)
      FlowField DG_dv;
      DG_dv  = G_u_dv;
      DG_dv -= G_u;
      
      cout << "converting field L_dv to vector v..." << endl;
      ColumnVector v(Meqn);
      field2vector(basis, DG_dv, v);
      v   *= 1.0/eps;
      dtx *= 1.0/eps;
      dtz *= 1.0/eps;

      Real dvnorm = L2Norm(dv);
      v(Meqn-2) = L2InnerProduct(dv, dudx)/dvnorm;
      v(Meqn-1) = L2InnerProduct(dv, dudz)/dvnorm;
    
      cout << "calculating arnoldi iterate..." << endl;
      for (int j=0; j<=n; ++j) {
	cout << j << ' ' << flush;
	ColumnVector Qj = Q.column(j);
	H(j,n) = Qj.transpose() * v;
	v -= H(j,n)*Qj;
      }
      cout << endl;
      Real vnorm = L2Norm(v);
      //cout << "                L2Norm(v)  == " << L2Norm(v) << endl;
      if (abs(vnorm) < EPS_gmres) {
	H(n+1, n) = 0.0;
	v *= 0.0;
	breakdown = true;
      }
      else {
	H(n+1, n) = vnorm;
	v *= 1.0/vnorm;
      }

      Q.insert(v,0,n+1);

      // Now minimize ||Ax-b|| within nth Krylov subspace. That amounts to
      // finding least-squares soln of Hn y = bk via QR where Hn is n+1 x n
      // submatrix of H.

      // where bk = [norm(b) 0 0 0 ...]'
      cout << "extracting submatrices for subspace least squares..." << endl;
      Matrix Hn = H.extract(0,0,n+1,n);
      Matrix Qn = Q.extract(0,0,Meqn-1,n);
      ColumnVector bn(n+2);
      setToZero(bn);
      bn(0) = L2Norm(b);

      cout << "least squares solve..." << endl;
      ColumnVector dy(n+1);
      setToZero(dy);
      
      Real gmres_residual;
      leastsquares(Hn,bn,dy,gmres_residual);
      ColumnVector dx = Qn*dy;
      vector2field(basis, dx, du);
      dtx = (relativeEQB) ? dx(Munk-2) : 0.0;
      dtz = (relativeEQB) ? dx(Munk-1) : 0.0;
	  
      // Approximate DG(u) du = G(u+du) - G(u)
      cout << "L2Norm(Ax-b)/L2Norm(b) == " << gmres_residual << endl;
      
      if (gmres_residual < EPS_gmres) {
        cout << "GMRES converged. Breaking." <<endl << endl;
	break;
      }
      else if (breakdown) {
	cout << "GMRES breakdown. Exiting." << endl;
	exit(1);
      }
      else if (n==Ngmres-1) 
	cout << "GMRES failed to converge. Continuing Newton with best guess." 
	     << endl;
      else 
	cout << "Continuing GMRES iteration." << endl;
      //cfpause();
    }

    // End of GMRES iteration. Assume that we have a decent solution for du.
    // Update u and take another Newton step.
    FlowField u_du(u);
    u_du += du;

    FlowField G_u_du;
    G(u_du, G_u_du, Reynolds, dt, T, tx+dtx, tz+dtz);

    FlowField DG_du(G_u_du);
    DG_du -= G_u;

    cout << "tx                      == " << tx <<endl;
    cout << "tz                      == " << tz <<endl;
    cout << "dtx                     == " << dtx <<endl;
    cout << "dtz                     == " << dtz <<endl;
    cout << "L2Norm(u)               == " << L2Norm(u) <<endl;  
    cout << "L2Norm(du)              == " << L2Norm(du) <<endl;
    cout << "L2Norm(G(u))            == " << L2Norm(G_u) <<endl;
    cout << "L2Norm(DG(u) du)        == " << L2Norm(DG_du) <<endl;

    G_u *= -1;
    cout << "L2Dist(DG(u) du, -G(u)) == " << L2Dist(DG_du, G_u) <<endl;
    G_u *= -1;

    du *= newton_damp;
    dtx *= newton_damp;
    dtz *= newton_damp;

    u += du;
    tx += dtx;
    tz += dtz;

    u.binarySave(outdir + "uiterate" + i2s(newton_step));
    u.binarySave(outdir + "uEQB");
  }
}
      
void G(FlowField& u, FlowField& G_u, Real Reynolds, Real dtarg, Real T, 
       Real tx, Real tz) {

  const Real nu = 1.0/Reynolds;
  const Real dtmin = 0.001;
  const Real dtmax = 0.05;
  const Real CFLmin = 0.4;
  const Real CFLmax = 0.6;

  //const Real fake_x_shift = 0.01;
  //const Real fake_z_shift = 0.02;
  
  const bool variable_dt = (dtarg == 0.0) ? true : false;
  const Real dtinit = (variable_dt) ? dtmax/2 : dtarg;
  const Real dT     = (variable_dt) ? lesser(1.0, T) : T;
  TimeStep dt(dtinit, dtmin, dtmax, dT, CFLmin, CFLmax);

  DNSFlags flags; 
  flags.timestepping = SBDF3;
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = Rotational;
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;

  const int Ny = u.Ny();
  const Real ya = u.a();
  const Real yb = u.b();

  cout << "G: setting up DNS..." << endl;
  G_u = u;
  //Gu.translate(fake_x_shift, fake_zshift);
  FlowField p(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b());

  ChebyTransform trans(Ny);
  ChebyCoeff U(Ny,ya,yb,Physical);
  Vector y = chebypoints(Ny,ya,yb);
  for (int ny=0; ny<Ny; ++ny) 
    U[ny] = y[ny];
  U.makeSpectral(trans);
  
  cout << "G: initializing DNS..." << endl;
  DNS dns(G_u, U, nu, dt, flags);
  cout << "G: CFL == " << dns.CFL() << endl;
  cout << "G:  dt == " << dt << endl;
  cout << "G:  tx == " << tx << endl;
  cout << "G:  tz == " << tz << endl;
  

  if (variable_dt && dt.adjust(dns.CFL())) {
    cout << "G : adjusting timestep" << endl;
    dns.reset_dt(dt);
  }
  
  for (Real t=0; t<T; t+=dT) {
    dns.advance(G_u, p, dt.n());
    if (variable_dt && dt.adjust(dns.CFL())) {
      cout << "G : adjusting timestep" << endl;
      dns.reset_dt(dt);
    }
  }
  u.translate(-tx, -tz);
  G_u -= u;
  //cout << "G: CFL == " << dns.CFL() << endl;
  cout << "G: returning..." << endl;
}

/* newtonREQB.cpp: Newton-Krylov search for relative equilibrium of 
 * plane Couette flow 
 * channelflow-1.1 PCF-utils
 *
 * Copyright (C) 2001-2007  John F. Gibson  
 *  
 * gibson@cns.physics.gatech.edu  jfg@member.fsf.org
 *
 * Center for Nonlinear Science
 * School of Physics
 * Georgia Institute of Technology
 * Atlanta, GA 30332-0430
 * 404 385 2509 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation version 2
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, U
 */
