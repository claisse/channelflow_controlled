#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"

#include "utilities.h"

using namespace std;
using namespace channelflow;

Real energy(FlowField& u, int i, bool normalize=true);

int main(int argc, char* argv[]) {

  string purpose("compute L2Dist(u0,u1) or \n"
		 "        L2Dist(u0,s(u1)) where s is a symmetry.");

  ArgList args(argc, argv, purpose);
  
   
  const bool norms = args.getbool("-n",  "--norms", true,  "show L2Norm(u0) and L2Norm(u1) as well");
  const int sn = args.getint ("-s",  "--symmetry", 0,  "s1, s2, or s3 symmetry (0 => use sx,sy,sx,ax,az params)");
  const int sx = args.getint("-sx", "--signX", 1, "s: x -> sx*x");
  const int sy = args.getint("-sy", "--signX", 1, "s: y -> sy*y");
  const int sz = args.getint("-sz", "--signZ", 1, "s: z -> sz*z");
  const Real ax = args.getreal("-ax", "--axshift", 0.0, "s: x -> x + ax/Lx");
  const Real az = args.getreal("-az", "--azshidt", 0.0, "s: z -> z + az/Lz");
  const string u0name = args.getstr(2, "<flowfield>", "u0 filename");
  const string u1name = args.getstr(1, "<flowfield>", "u1 filename");
  args.check();

  FlowField u0(u0name);
  FlowField u1(u1name);
  
  FieldSymmetry s;
  if (sn == 1)
    s = FieldSymmetry( 1, 1,-1, 0.5, 0);
  else if (sn==2)
    s = FieldSymmetry(-1,-1, 1, 0.5, 0.5);
  else if (sn==3)
    s = FieldSymmetry(-1,-1,-1, 0, 0.5);
  else 
    s = FieldSymmetry(sx,sy,sz,ax,az);
  u1 *= s;

  if (norms) {
    cout << "L2Norm(u0)       == " << L2Norm(u0) << endl;
    cout << "L2Norm(u1)       == " << L2Norm(u1) << endl;
  }
  cout << "L2Dist(u0,s(u1)) == " << L2Dist(u0,u1) << endl;
}

