
#include "mpi.h"
#include <string>
#include <iostream>
#include "channelflow/flowfield.h"
#include "petscsnes.h"
#include "channelflow/dns.h"
#include "channelflow/flowfield.h"
#include "utilities.h"
using namespace channelflow;
using namespace std;


PetscErrorCode findClosestPass(SNES snes, Vec x, Vec f, void *ctx);

struct HeteroclinicCtx
{
  FlowField * base;
  FlowField * eig;
  FlowField * target;
  Real Re;
  
  Real dtGuess;
  Real dtMin;
  Real dtMax;
  Real interval;
  Real CFLmin;
  Real CFLmax;

  Real Tmax;
  Real Tmin;
  DNSFlags flags;
};

int main(int argc, char** argv)
{
  int mpiSize;
  PetscInitialize ( &argc,&argv,PETSC_NULL,PETSC_NULL );
  PetscOptionsInsertString("-snes_mf");  // force matrix free
  MPI_Comm_size ( PETSC_COMM_WORLD,&mpiSize );
  
  string purpose("Searches for a flow invariant ie [relative] [equilibirum | periodic orbit] using Petsc");
  ArgList args(argc, argv, purpose);

  HeteroclinicCtx ctx;
  ctx.dtGuess       = args.getreal("-dt","--timestep",  0.03125, "(initial) integration timestep");
  ctx.dtMin         = args.getreal("-dtmin", "--dtmin", 0.001, "minimum timestep");
  ctx.dtMax         = args.getreal("-dtmax", "--dtmax", 0.05,  "maximum timestep");
  ctx.CFLmin        = args.getreal("-CFLmin", "--CFLmin", 0.40, "minimum CFL number");
  ctx.CFLmax        = args.getreal("-CFLmax", "--CFLmax", 0.60, "maximum CFL number");
  ctx.interval      = args.getreal("-DT","--interval", 1, "Interval between distance checks");
  ctx.Tmax          = args.getreal("-tmax", "--TMax", 200, "Maximum integration time");
  ctx.Tmin          = args.getreal("-tmin", "--TMin", 50, "Minimum integration time");
  ctx.Re            = args.getreal("-R", "--Reynolds", 400, "Reynolds number");

  const Real x0    = args.getreal("-x0","--xguess", 0.01, "Guess perturbation size");
  const string snes_type = args.getstr("-snes_type","-snes_type","ls",
                                       "Solver type, ls -- linesearch, tr -- trust region");
  const string baseName = args.getstr("-u0", "--u0", "u", "origin equilibrium");
  const string eigName  = args.getstr("-e", "--eig", "ef","direction to search along");
  const string targName = args.getstr("-t", "--target", "uf", "target equilibirum");

  const string outfile = args.getstr("-o", "--outfile", "outfile.ff", "Location to store answer.");

  args.check();
  args.save("./");
  FlowField base(baseName);
  FlowField eig(eigName);
  FlowField target(targName);
  ctx.base = &base;
  ctx.eig = &eig;
  ctx.target = &target;

  DNSFlags flags;
  flags.timestepping = SBDF3;
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = Rotational;
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;
  flags.baseflow     = PlaneCouette;
  flags.verbosity    = Silent;
  ctx.flags = flags;

  const int dim = 1;
  // Solution & residual vectors
  Vec x, r;
  VecCreateSeq ( PETSC_COMM_WORLD, dim, &x );
  VecDuplicate ( x, &r );

  VecSetValue(x,0,x0,INSERT_VALUES);
  // Setup nonlinear solver

  SNES snes;
  SNESCreate(PETSC_COMM_WORLD,&snes);
  SNESSetFunction ( snes, r, &findClosestPass, (void *) &ctx );
  
  
  //SNESMonitorSet(snes, &RPOMonitor, (void *) &dnsctx, PETSC_NULL);
  SNESSetFromOptions ( snes );
  
  SNESConvergedReason reason;
  
  SNESSolve ( snes,PETSC_NULL,x );

  PetscInt its;
  SNESGetConvergedReason ( snes, &reason );//CHKERRQ ( ierr );
  SNESGetIterationNumber ( snes,&its );//CHKERRQ ( ierr );


  PetscScalar *xx;
  VecGetArray(x, &xx);
  cout << setprecision(16);
  cout << xx[0] << endl;
  eig *= xx[0];
  base += eig;
  base.binarySave(outfile);
}





PetscErrorCode findClosestPass(SNES snes, Vec x, Vec f, void *vctx)
{
  PetscScalar *xx; 
  HeteroclinicCtx * ctx = (HeteroclinicCtx *) vctx;
  VecGetArray(x, &xx);
  FlowField u(*(ctx->eig));
  
  cout << "guess : " <<  xx[0] << endl;
  u *= xx[0];
  u += *(ctx->base);

  VecRestoreArray(x, &xx);
  Real dist0 = L2Dist(u, *(ctx->target));
  Real dist1 = L2Dist(u, *(ctx->target));
  Real dist2 = L2Dist(u, *(ctx->target));
//  int Nchecks = int(T / checkInterval + 1);
//  Real checkInterval = T / Nchecks;

  TimeStep dt(ctx->dtGuess, ctx->dtMin, ctx->dtMax, ctx->interval,
              ctx->CFLmin, ctx->CFLmax);

  DNS dns ( u, 1.0 / ctx->Re, dt, ctx->flags );
  if(dt.adjust(dns.CFL()))
    dns.reset_dt(dt);
  
  Real t = 0;
  FlowField p(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(),u.a(),u.b());
  
  while(t < ctx->Tmax)
  {
    cout << t << ' ' << flush;
    dns.advance(u, p, dt.n());
    t += dt.dT();

    dist0 = dist1;
    dist1 = dist2;
    dist2 = L2Dist(u, *(ctx->target));
    if(t > ctx->Tmin && dist2 > dist1)
      break;

    if (dt.adjust(dns.CFL()))
      dns.reset_dt(dt);
  }
  Real closest = 0;
  if(t >= ctx->Tmax)
  {
    closest = dist2;
  }
  else
  {
    //Quadratic approximation
    const Real a = (dist2 - 2*dist1 + dist0) / 2;
    const Real b = (dist2 - dist0) / 2;
    const Real c = dist1;
    closest = c - b*b / a  * 0.25;
  }
    cout << dist0 << ' ' << dist1 << ' ' << dist2 << endl;
    cout << "closest distance: " << closest << endl;

  VecSetValue(f, 0, closest, INSERT_VALUES);

  return 0;
}

