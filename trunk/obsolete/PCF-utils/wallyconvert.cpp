#include <iostream>
#include <fstream>

#include "utilities.h"
#include "channelflow/flowfield.h"

using namespace channelflow;
using namespace std;

void loadStreams(ifstream& u_asc, ifstream& v_asc, ifstream& w_asc, FlowField& u);

int main(int argc, char**argv)
{
  const string purpose("Convert data stored in ECS database style to a flowfield");
  ArgList args(argc, argv, purpose);

  const string label = args.getstr("-l","--label","Label for ECS data, files should be named label.{u,v,w}.asc");
  const string outfile = args.getstr("-o","--outfile","Place to store flowfield");
  
  const int Nx = args.getint("-nx","--nx",32,"Number of x modes");
  const int Ny = args.getint("-ny","--ny",34,"Number of x modes");
  const int Nz = args.getint("-nz","--nz",32,"Number of z modes");
  const Real alpha = args.getreal("-a","--alpha","2 * PI / Lx");
  const Real gamma = args.getreal("-g","--gamma","2 * PI / Lz");

  const bool test = args.getflag("-t","--test","Display statistics of converted file");
  const bool leaveLaminar = args.getflag("-s", "--preserve-laminar",
                                        "If set, the laminar profile is not subtracted off");
// hard wired constants
  const int Nd = 3;  // 3 dimensions
  const Real a = -1.0; // Lower y limit
  const Real b = 1.0; // Upper y limit
  const string uname = label + "u.asc";
  const string vname = label + "v.asc";
  const string wname = label + "w.asc";


  FlowField u(Nx,Ny,Nz,Nd, 2*M_PI / alpha, 2*M_PI / gamma, a,b,Physical,Spectral);

  ifstream u_asc(uname.c_str(), ifstream::in);
  ifstream v_asc(vname.c_str(), ifstream::in);
  ifstream w_asc(wname.c_str(), ifstream::in);
  try
  {
   loadStreams(u_asc,v_asc,w_asc,u);
  }
  catch(const char* error)
  {
   cerr << error << endl;
   return 1;
  }

  u_asc.close();
  v_asc.close();
  w_asc.close();

  ChebyCoeff U(Ny,a,b,Spectral);
  U[1] = 1;
  
  if(test)
  {
    const string stats_filename = label + "stats";
    ifstream stats(stats_filename.c_str(),iostream::in);
    if(!stats.good())
      cerr << "ERROR: Unable to open stats file " << stats_filename << endl;

    char garbage[1024];
//    while(stats.peek() == '%')
     stats.getline(garbage,1024);
 //    stats.ignore(1024,'\n');


  stats.getline(garbage,1024);

    u.makeSpectral();
    FlowField u2(u);
    u2 -= U;

    u2.makePhysical();

    for(int nd = 0; nd < Nd; ++nd)
      for(int ny = 0; ny < Ny; ++ny)
        for(int nx = 0 ; nx < Nx; ++nx)
          for(int nz = 0; nz < Nz; ++nz)
            u2(nx,ny,nz,nd) *= u2(nx,ny,nz,nd);

    u2.makeSpectral();

    
    Vector stats_y(Ny), stats_U(Ny), stats_u2(Ny),
               stats_v2(Ny), stats_w2(Ny);
     
    for(int ny = 0; ny < u.Ny(); ++ny)
    {
      stats >> stats_y[ny] >> stats_U[ny] 
            >> stats_u2[ny] >> stats_v2[ny] >> stats_w2[ny] ;
      
      // Square for comparison to u^2
      stats_u2[ny] *= stats_u2[ny];
      stats_v2[ny] *= stats_v2[ny];
      stats_w2[ny] *= stats_w2[ny];
    }
    cout << u2.profile(0,0,0).re.eval(stats_y[10]) << ' ' << stats_u2[10] << endl;
    ChebyCoeff interp_U  =  u.profile(0,0,0).re.eval(stats_y);
    ChebyCoeff interp_u2 = u2.profile(0,0,0).re.eval(stats_y);
    ChebyCoeff interp_v2 = u2.profile(0,0,1).re.eval(stats_y);
    ChebyCoeff interp_w2 = u2.profile(0,0,2).re.eval(stats_y);
    cout << "U error :     " << L2Dist(interp_U, stats_U) << endl;
    cout << "rms u error : " << L2Dist(interp_u2, stats_u2) << endl;
    cout << "rms v error : " << L2Dist(interp_v2, stats_v2) << endl;
    cout << "rms w error : " << L2Dist(interp_w2, stats_w2) << endl;
    
    u.makeSpectral();
    cout << "L2Norm(div(u)) : " << L2Norm(div(u)) << endl;
    
  }

  u.makeSpectral();
  if(!leaveLaminar)
    u -= U;
  
  if(test)
    cout << "bcNorm(u) : "      << bcNorm(u) << endl;


  u.binarySave(outfile);

  return 0;
}

void loadStreams(ifstream& u_asc, ifstream& v_asc, ifstream& w_asc, FlowField& u)
{
  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();

  for(int ny = 0; ny < Ny; ++ny)
    for(int nz = 0; nz < Nz; ++nz)
      for(int nx = 0; nx < Nx; ++nx)
        if(u_asc.good() && v_asc.good() && w_asc.good())
        {
          u_asc >> u(nx,ny,nz,0);
          v_asc >> u(nx,ny,nz,1);
          w_asc >> u(nx,ny,nz,2);
        } 
        else 
         throw "Mismatch between file lengths and Nx, Ny, Nz";

}
