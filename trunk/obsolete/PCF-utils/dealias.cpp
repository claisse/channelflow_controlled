#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("zero the padding modes of a given field");
  ArgList args(argc, argv, purpose);
  const Real   eps   = args.getreal("-e", "--eps", 1e-15, "noise floor");
  const string uname = args.getstr (1, "<field>", "field file name");
  args.check();

  FlowField u(uname);
  u.makeSpectral();

  int kxmax=0;
  int kzmax=0;
  int kymax=0;
  for (int mx=0; mx<u.Mx(); ++mx) {
    const int kx = u.kx(mx);
    for (int mz=0; mz<u.Mz(); ++mz) {
      const int kz = u.kz(mz);
      BasisFunc prof = u.profile(mx,mz);
      if (L2Norm(prof) > eps)
	kxmax = Greater(kxmax, abs(kx));
	kzmax = Greater(kzmax, abs(kz));
      }
      for (int ky=0; ky<u.Ny(); ++ky) {
	Real sum = 0.0;
	for (int i=0; i<u.Nd(); ++i)
	  sum += abs(prof[i][ky]);
	if (sum > eps)
	  kymax = Greater(kymax, ky);
      }
    }
  }
  if (kxmax > u.kxmaxDealiased() || kzmax > u.kzmaxDealiased()) {
    cerr << "warning: u has energy in aliased modes" << endl;
    cerr << "maximum kx,kz for dealiased modes in u is " 
	 << u.kxmaxDealiased() << ',' << u.kzmaxDealiased() << endl;
    cerr << "u has energy in kx == " << kxmax << " and ";
    cerr << "kz == " << kzmax << endl;
    cerr << "Zero these modes and save? (y/n) [n] :" << flush;
    char s;
    cin >> s;
    if (s!='y')
      exit(1);
  }
  u.zeroAliasedModes();
  u.binarySave(uname);
}

