#include <string>
#include <iostream>

#include "petsc-newton.h"
#include "utilities.h"
#include "channelflow/realprofileng.h"

using namespace std;
using namespace channelflow;

int main(int argc, char** argv)
{
  string purpose("Searches for a flow invariant ie [relative] [equilibirum | periodic orbit] using Petsc");

  ArgList args(argc, argv, purpose);

  const string outdir = args.getpath("-o", "--outdir", "data/", "output directory");
  const string label = args.getstr("-l", "--label", "eqba", "output file name");
  const Real Reynolds  = args.getreal("-R", "--Reynolds", 400, "Reynolds number");
  const Real T         = args.getreal("-T", "--Time", 10, "Integration time");
  const bool fix_T     = args.getflag("-fixT", "--fixed_T", "If true, integration time will not vary (to search for eqba)");
  const Real sx         = args.getreal("-sx", "--sx", 0, "Guess X shift");
  const bool fix_Sx    = args.getflag("-fixSx", "--fixed_Sx", "If true, will only consider given X Shift");
  const Real sz         = args.getreal("-sz", "--sz", 0, "Guess Z shift");
  const bool fix_Sz    = args.getflag("-fixSz", "--fixed_Sz", "If true, will only consider given X Shift");
  const bool fixRe     = args.getflag("-fixRe", "--fixed_Re", "If true, will fix Reynolds number");
  const Real targetD   = args.getreal("-d","--dissipation",0,"If Re not fixed, will vary it to match this dissipation");
  const bool opt_x       = args.getflag("-optx", "--optimize-xshift", "Optimizes x phase by s2 symmetry");
  const bool opt_z     = args.getflag("-optz","--optimize-zshift","Optimizes z phase by s1 symmetry");
//  const bool dtVariable= args.getbool("-vdt","--variableDt", true, "adjust dt to keep CFLmin<=CFL<CFLmax");
  const Real dt0       = args.getreal("-dt","--timestep",  0.03125, "(initial) integration timestep");
  const Real dtMin     = args.getreal("-dtmin", "--dtmin", 0.001, "minimum timestep");
  const Real dtMax     = args.getreal("-dtmax", "--dtmax", 0.05,  "maximum timestep");
  const Real CFLmin    = args.getreal("-CFLmin", "--CFLmin", 0.40, "minimum CFL number");
  const Real CFLmax    = args.getreal("-CFLmax", "--CFLmax", 0.60, "maximum CFL number");
  const bool s1        = args.getflag("-s1","--s1","Enforce s1 symmetry");
  const bool s2        = args.getflag("-s2","--s2","Enforce s2 symmetry");
  const bool s3        = args.getflag("-s3","--s3","Enforce s3 symmetry");
  //  const int nthreads   = args.getint("-nt","--nthreads",1,"Number of FFTW threads to use");  
  const Real newLx     = args.getreal("-lx","--lx",-1,"if > 0 , resize Lx to new value");
  const Real newLz     = args.getreal("-lz","--lz",-1,"if > 0 , resize Lz to new value");
  const Real newAlpha     = args.getreal("-a","--alpha",-1,"if > 0 , resize alpha to new value");
  const Real newGamma     = args.getreal("-g","--gamma",-1,"if > 0 , resize gamma to new value");
  const int newNx      = args.getint("-nx","--nx",-1,"if > 0 , truncate / pad input field to Nx modes");
  const int newNy      = args.getint("-ny","--ny",-1,"if > 0 , truncate / pad input field to Ny modes");
  const int newNz      = args.getint("-nz","--nz",-1,"if > 0 , truncate / pad input field to Nz modes");
  
  //Petsc args
  const string snes_type = args.getstr("-snes_type","-snes_type","ls","Solver type, ls -- linesearch, tr -- trust region");
  const bool  debug     = args.getflag("-on_error_attach_debugger", "-on_error_attach_debugger", "Attach GDB if a petsc error occurs.");
  const Real CFLCheckTime = 1;
  
  const string uname = args.getstr (1, "<flowfield>", "initial condition");
  
	//  fftw_init_threads();
	//  fftw_plan_with_nthreads(nthreads);
  args.check();
  args.save("./");
  args.save(outdir);
  mkdir(outdir);
  petscSetup(argc, argv);
  PetscNewton newt;
  FlowField u0(uname);

  
	// Define gridsize
  const int Nx = (newNx > 0) ? newNx : u0.Nx();
  const int Ny = (newNy > 0) ? newNy : u0.Ny();
  const int Nz = (newNz > 0) ? newNz : u0.Nz();
  if(newLx > 0 && newAlpha > 0)
  {
    cerr << "ERROR: Cannot set both Lx and alpha simultaneously" << endl;
    return 1;
  }
    
  if(newLz > 0 && newGamma > 0)
  {
    cerr << "ERROR: Cannot set both Lz and gamma simultaneously" << endl;
    return 1;
  }
  
  Real tLx = u0.Lx();
  if(newAlpha > 0)
    tLx = 2 * M_PI / newAlpha;
  if(newLx > 0)
    tLx = newLx;
  const Real Lx = tLx;
  
  Real tLz = u0.Lz();
  if(newGamma > 0)
    tLz = 2 * M_PI / newGamma;
  if(newLz > 0)
    tLx = newLz;
  const Real Lz = tLz;

  const Real ya = u0.a();
  const Real yb = u0.b();

  u0.rescale(Lx, Lz);
  FlowField tmp(u0);
  tmp.resize(Nx, Ny, Nz, u0.Nd(), Lx, Lz, ya, yb,FFTW_PATIENT);
  tmp.interpolate(u0);
  u0 = tmp;

  const int kxmax = Nx / 3;
  const int kxmin = -kxmax;
  const int kzmin = 0;
  const int kzmax = Nz / 3;

  cout << "Lx == " << Lx << endl;
  cout << "Lz == " << Lz << endl;
  cout << "alpha == " << 2.0 * M_PI / u0.Lx() << endl;
  cout << "gamma == " << 2.0 * M_PI / u0.Lz() << endl;
  cout << "Nx == " << Nx << endl;
  cout << "Ny == " << Ny << endl;
  cout << "Nz == " << Nz << endl;
  cout << "kxmin == " << kxmin << endl;
  cout << "kxmax == " << kxmax << endl;
  cout << "kzmin == " << kzmin << endl;
  cout << "kzmax == " << kzmax << endl;

  FieldSymmetry ss1( 1, 1,-1, 0.5, 0.0);
  FieldSymmetry ss2(-1,-1, 1, 0.5, 0.5);
  FieldSymmetry ss3(-1,-1, -1, 0.0, 0.5);

  
  DNSFlags flags;
  flags.timestepping = SBDF3;
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = Rotational;
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;
  flags.baseflow     = PlaneCouette;

  vector<FieldSymmetry> symms; 
  if(s1)
    symms.push_back(ss1);
  if(s2) 
    symms.push_back(ss2); 
  if(s3)
    symms.push_back(ss3);

  cout << "constructing basis set. this takes a long time..." << endl;
  vector<RealProfileNG> basis = realBasisNG( Ny, kxmax, kzmax, Lx, Lz, ya, yb);
  if(s1 || s2 || s3)
    selectSymmetries(basis, symms);
  
  orthonormalize(basis);
  const int M = basis.size(); // dimension of basis set
  cout << M  << "-dimensional state space." << endl;
  //checkBasis ( basis, basisflags );
  
  RPO rpo;
  rpo.u = &u0;
  rpo.sx = sx;
  rpo.sz = sz;
  rpo.T  = T;
  rpo.Re = Reynolds;
  
  vector<FieldSymmetry> optsymms;
  if(opt_x)
    optsymms.push_back(ss2);
  if(opt_z)
    optsymms.push_back(ss1);
  RPO ans = newt.findInvariant(basis, rpo, Reynolds, fix_Sx, fix_Sz, fix_T, fixRe, targetD, optsymms,
			       dt0, dtMin, dtMax, CFLmin, CFLmax, CFLCheckTime, flags,outdir,label);
  ans.u->binarySave(outdir + '/' + label);
  ofstream statsFile;
  string filename = outdir + '/' + label + "final.stats";
  statsFile.open(filename.c_str());
  statsFile << setprecision(12);
  statsFile << ans.sx << endl;
  statsFile << ans.sz << endl;
  statsFile << ans.T << endl;
  statsFile << ans.Re << endl;
  statsFile.close();
}
