#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/EIG.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"
#include "utilities.h"


using namespace std;
using namespace channelflow;

// This program is a damped Newton search code for PCF equilibria that
// uses GMRES to solve the Newton-step equations. The GMRES algorithm is
// modeled on the discussion in Lecture 35 of Trefethen and Bau's "Numerical
// Linear Algebra". 
// The dampled Newton search converges well when the initial guess is close
// to the ultimate solution, but I need to implement "hook-step" Newton to
// make the algorithm robust enough for finding new solutions.

// John Gibson, Thu Dec  7 15:29:46 EST 2006

// Newton algorithm for solving G(u) = 0. 
// Let u* be the solution, i.e. G(u*) = 0
// Suppose we start with an initial guess u near u*.
// Let u* = u + du.
// Then G(u*) = G(u + du) 
//          0 = G(u) + DG(u) du + O(|du|^2)
// Drop h.o.t and solve
//   DG(u) du = -G(u)
// Then start with new guess u' = u + du and iterate.
//
// Use GMRES to solve the Newton-step equation. GMRES requires multiple 
// calculations of the LHS DG(u) du for trial values du. These can be 
// approximated with finite differencing:
//   DG(u) du = G(u+du) - G(u)
// To use GMRES, we need to cast velocity fields such as du and DG(u) du 
// into vectors and vice versa. Do this with a real-valued, orthonormal, 
// div-free, dirichlet basis {Phi_m}
//   a_n = (u, Phi_n)
//     u = a_n Phi_n
// Then the Newton step equation
//  DG(u) du = -G(u)
// can be translated to a linear algebra problem   
//      A dx = b 
// via
//      dx_n = (du, Phi_n)
//       b_n = (-G(u), Phi_n)
//  (A dx)_n = (DG(u) du, Phi_n) = (G(u+du) - G(u), Phi_n)


// In this case the function to be zered is G(u) = F^T(u) - u, where F^T 
// is the time-T map of Navier-Stokes. computed by DNS.
void G(FlowField& u, FlowField& G_u, Real Reynolds, Real dt, Real T, DNSFlags& flags, int& Gcount);

int main(int argc, char* argv[]) {

  string purpose("Newton search for PCF equilibrium");
  ArgList args(argc, argv, purpose);

  const Real epsKrylov = args.getreal("-ek", "--epsKrylov", 1e-14, "min. condition # of Krylov vectors");
  const Real epsGMRES  = args.getreal("-eg", "--epsGMRES", 1e-3, "convergence criterion for Ax=b on GMRES");
  const Real epsNewton = args.getreal("-en", "--epsNewton", 1e-14, "stop Newton-search if L2Norm(F^T(u)-u) < epsNewton");
  const Real epsLin   = args.getreal("-el","--epsLinear", 1e-7, "relative size of du to u in linzation");
  const Real epsSymm  = args.getreal("-es", "--epsSymmetry", 1e-13, "maximum symmetry discrepancy in u0");
  const int  Ngmres   = args.getint("-Ng", "--Ngmres", 40,  "max number of GMRES iterations");
  const int  Nnewton  = args.getint("-Nn", "--Nnewton", 10,  "max number of Newton steps");
  const Real damping  = args.getreal("-d", "--damping", 1.0, "damping factor for Newton step");
  const Real dt       = args.getreal("-dt","--timestep", 0.0, "integration timestep (0 => auto-adjust)");
  const Real T        = args.getreal("-T", "--maptime", 10.0, "integration time for GMRES-DNS");
  const string stepstr= args.getstr("-ts", "--timestepping", "SBDF3", "timestep algorithm: See README for options");
  const string nonlstr= args.getstr("-nl", "--nonlinearity", "Rotational", "nonlinearity method: See README for options");
  const bool s1symm   = args.getflag("-s1", "--s1-symmetry", "restrict to s1-symmetric subspace");
  const bool s2symm   = args.getflag("-s2", "--s2-symmetry", "restrict to s2-symmetric subspace");
  const bool s3symm   = args.getflag("-s3", "--s3-symmetry", "restrict to s3-symmetric subspace");
  const string outdir = args.getpath("-o", "--outdir", "./", "output directory"); 
  const Real Reynolds = args.getreal("-R", "--Reynolds", 400, "Reynolds number");
  const string uname = args.getstr(1, "<flowfield>", "initial guess for Newton search");
  
  args.check();
  args.save(outdir);

  BasisFlags basisflags;
  basisflags.aBC = Diri;
  basisflags.bBC = Diri;
  basisflags.orthonormalize = true;
  basisflags.zerodivergence = true;

  DNSFlags flags; 
  flags.timestepping = s2stepmethod(stepstr);
  flags.dealiasing   = DealiasXZ;
  flags.nonlinearity = s2nonlmethod(nonlstr);
  flags.constraint   = PressureGradient;
  flags.dPdx         = 0.0;
  flags.baseflow     = PlaneCouette;
  flags.verbosity    = Silent;
  cout << "DNSFlags == " << flags << endl;

  // Don't forget to change the Reynolds number in G(u)!
  FlowField u(uname);

  FieldSymmetry s1( 1, 1,-1, 0.5, 0.0);
  FieldSymmetry s2(-1,-1, 1, 0.5, 0.5);
  FieldSymmetry s3(-1,-1,-1, 0.0, 0.5);
  
  // Check that u satisfies symmetry restrictions
  if (s1symm && L2Dist(u, s1(u)) > epsSymm)
    cerr << "warning: restricting to s1-subspace even though u0 doesn't satisfy s1" << endl;
  if (s2symm && L2Dist(u, s2(u)) > epsSymm)
      cerr << "warning: restricting to s1-subspace even though u0 doesn't satisfy s2" << endl;
  if (s3symm && L2Dist(u, s3(u)) > epsSymm)
      cerr << "warning: restricting to s1-subspace even though u0 doesn't satisfy s3" << endl;
	

  //const int P = 4;
  //const string plotbasis("UBproj0/basis/e");
  //array<FlowField> e(P);
  //for (int p=0; p<P; ++p)
  //e[p] = FlowField(plotbasis + i2s(p));

  // Define gridsize
  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();
  const Real Lx = u.Lx();
  const Real ya = u.a();
  const Real yb = u.b();
  const Real Lz = u.Lz();
  
  const int kxmax = u.kxmaxDealiased();
  const int kxmin = -kxmax;
  const int kzmin = 0;
  const int kzmax = u.kzmaxDealiased();;

  cout << "Nx == " << Nx << endl;
  cout << "Ny == " << Ny << endl; 
  cout << "Nz == " << Nz << endl;
  cout << "kxmin == " << kxmin << endl;
  cout << "kxmax == " << kxmax << endl;
  cout << "kzmin == " << kzmin << endl;
  cout << "kzmax == " << kzmax << endl;

  //cout << "constructing basis set. this takes a long time..." << endl;
  //vector<RealProfile> basis = 
  //realBasis(Ny, kxmax, kzmax, Lx, Lz, ya, yb, basisflags);
  //checkBasis(basis, basisflags);

  cout << "Identity test on u and basis..." << endl;
  ColumnVector a;
  FlowField    u0(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); 
  field2vector(u, a);
  vector2field(a, u0);
  cout << "L2Dist(u, a_n Phi_n) == " << L2Dist(u, u0) << endl;

  const int Munk = a.length();  // dimension of basis set
  const int Meqn = Munk+2;      // number of equations
  const int Ngmres2 = (Munk < Ngmres) ? Munk : Ngmres;
  cout << Meqn << " equations" << endl;
  cout << Munk << " unknowns" << endl;
  a = ColumnVector(Munk);

  // ==============================================================
  // Initialize Newton iteration 
  
  FlowField    p(Nx, Ny, Nz, 1, Lx, Lz, ya, yb); // pressure 
  FlowField   du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // Newton step
  FlowField utmp(Nx, Ny, Nz, 3, Lx, Lz, ya, yb); // Newton iterate

  Real init_newton_residual = 0.0;
  Real prev_newton_residual = 0.0;
  Real prev_u0_error = 0.0;
  int Gcount = 0;

  ofstream osconv((outdir + "convergence.asc").c_str());
  cout << setprecision(10);

  ChebyCoeff U(Ny, ya, yb, Spectral);
  U[1] = 1;

  for (int newton_step = 0; newton_step<Nnewton+1; ++newton_step) {
    
    cout << "========================================================" << endl;
    cout << "Newton iteration number " << newton_step << endl;
  
    // Calculate G(u) = F^T(u) - u
    FlowField G_u;
    G(u, G_u, Reynolds, dt, T, flags, Gcount);

    Real u0_error = L2Dist(u,u0);
    Real newton_residual = L2Norm(G_u); 

    if (newton_step == 0) {
      init_newton_residual = newton_residual;
      prev_newton_residual = newton_residual;
    }

    // Calculate two unit vectors to which du must be orthogonal
    // dudx = tangent vector of x translation
    // dudz = tangent vector of z translation

    FlowField dudx = xdiff(u);
    FlowField dudz = zdiff(u);
    dudx *= 1.0/L2Norm(dudx);
    dudz *= 1.0/L2Norm(dudz);

    FlowField uU(u);
    uU += U;

    cout << "Current state of Newton iteration:" << endl;
    cout << "dissip (u)    == " << dissipation(u) << endl;
    cout << "forcing(u)    == " << forcing(u) << endl;
    cout << "energy (u)    == " << 0.5*L2Norm2(u) << endl;
    cout << "dissip (u+U)  == " << dissipation(uU) << endl;
    cout << "forcing(u+U)  == " << forcing(uU) << endl;
    cout << "energy (u+U)  == " << 0.5*L2Norm2(uU) << endl;
    cout << "   L2Norm(u)  == " << L2Norm(u) << endl;
    cout << "L2Dist(u,s1(u)        == " << L2Dist(u,s1(u)) << endl;
    cout << "L2Dist(u,s2(u))       == " << L2Dist(u,s2(u)) << endl;
    cout << "L2Dist(u,s3(u))       == " << L2Dist(u,s3(u)) << endl;
    cout << "residual L2Norm(G(u)) == " << newton_residual << endl;
    cout << "previous residual     == " << prev_newton_residual<< endl;
    cout << "initial  residual     == " << init_newton_residual << endl;
    cout << "prev step L2Norm(du)  == " << L2Norm(du) <<endl;
    cout << "total L2Dist(u,u0)    == " << u0_error << endl;

    osconv  << setw(6)  << Gcount 
	    << setw(12) << newton_residual 
	    << setw(12) << L2Norm(du) << endl;
    
    //for (int p=0; p<P; ++p) 
    //stepos << L2IP(u, e[p]) << ' ';
    //stepos << endl;

    prev_u0_error = u0_error;
    prev_newton_residual = newton_residual;

    if (newton_residual < epsNewton) {
      cout << "Newton search converged. Breaking." << endl;
      break;
    }
    else if (newton_step == Nnewton) {
      cout << "Reached maximum number of Newton steps. Breaking." << endl;
      break;
    } 
	  
    // Set up RHS vector b = (-field2vector(G(u)), 0, 0); 
    // (0, 0) is the RHS of eqn enforcing orthog. to (dudx, dudz)
    ColumnVector b(Meqn);
    setToZero(b);
    //field2vector(basis, G_u, b);
    field2vector(G_u, b);
    for (int m=Meqn-2; m<Meqn; ++m)  
      b(m) = 0.0;
    b *= -1.0;

    GMRES gmres(b, Ngmres2, epsKrylov);

    // ===============================================================
    // GMRES iteration to solve DG(u) du = -G(u) for du
    for (int n=0; n<Ngmres2; ++n) {
      bool breakdown = false;

      cout << "------------------------------------------------" << endl;
      cout << "Newton, GMRES number " << newton_step << ", " << n << endl;

      // Compute v = Ab in Arnoldi iteration terms, where b is Q.column(n)
      // In Navier-Stokes terms, the main quantity to compute is
      //   L dv = DG(u) dv

      // Use approximation 
      // DG(u) dv = G(u+dv) - G(u)

      // But before the RHS expression, rescale dv to be very small so that 
      // the expression is approx linear in dv, and then unscale it and the 
      // result afterwards. Rescale so that L2Norm(dv) = epsLin << 1

      ColumnVector q = gmres.testVector();
      FlowField dv(Nx,Ny,Nz,3,Lx,Lz,ya,yb);
      //vector2field(basis, q, dv);
      vector2field(q, dv);
      
      // If forcing symmetries, project dv into symmetric subspace.
      if (s1symm)  {
	dv += s1(du);
	dv *= 0.5;
      }
      if (s2symm)  {
	dv += s2(du);
	dv *= 0.5;
      }
      if (s3symm)  {
	dv += s3(du);
	dv *= 0.5;
      }
      Real eps = epsLin/L2Norm(dv);
      dv *= eps;
      //cout << "       rescaled L2Norm(dv) == " << L2Norm(dv) << endl;

      cout << "computing G(u+dv)..." << endl;
      FlowField u_dv;
      u_dv = u;   
      u_dv += dv;
      FlowField G_u_dv;
      G(u_dv, G_u_dv, Reynolds, dt, T, flags, Gcount);

      // Compute DG(u) dv = G(u+dv) - G(u)
      FlowField DG_dv;
      DG_dv  = G_u_dv;
      DG_dv -= G_u;
      
      cout << "converting field L_dv to vector Lq..." << endl;
      ColumnVector Lq(Meqn);
      //field2vector(basis, DG_dv, Lq);
      field2vector(DG_dv, Lq);
      Lq *= 1.0/eps;
      Real dvnorm = L2Norm(dv);
      Lq(Meqn-2) = L2InnerProduct(dv, dudx)/dvnorm;
      Lq(Meqn-1) = L2InnerProduct(dv, dudz)/dvnorm;
    
      gmres.iterate(Lq);

      Real gmres_residual = gmres.residual();
      ColumnVector dx = gmres.solution();
      //vector2field(basis, dx, du);
      vector2field(dx, du);

      // Approximate DG(u) du = G(u+du) - G(u)
      cout << "L2Norm(Ax-b)/L2Norm(b) == " << gmres_residual << endl;
      
      if (gmres_residual < epsGMRES) {
        cout << "GMRES converged. Breaking." <<endl << endl;
	break;
      }
      else if (breakdown) {
	cout << "GMRES breakdown. Exiting." << endl;
	exit(1);
      }
      else if (n==Ngmres-1) 
	cout << "GMRES failed to converge. Continuing Newton with best guess." 
	     << endl;
      else 
	cout << "Continuing GMRES iteration." << endl;
      //cfpause();
    }

    // End of GMRES iteration. Assume that we have a decent solution for du.
    // Update u and take another Newton step.
    /***************************************************
    FlowField u_du(u);
    u_du += du;

    FlowField G_u_du;
    G(u_du, G_u_du, Reynolds, dt, T, flags, Gcount);

    FlowField DG_du(G_u_du);
    DG_du -= G_u;

    cout << "L2Norm(u)               == " << L2Norm(u) <<endl;  
    cout << "dissip(u)               == " << forcing(u)
    cout << "power(u)                == " << power(u)
    cout << "L2Dist(u,s1(u))         == " << L2Dist(u,s1(u)) << endl;
    cout << "L2Dist(u,s2(u))         == " << L2Dist(u,s2(u)) << endl;
    cout << "L2Dist(u,s3(u))         == " << L2Dist(u,s3(u)) << endl;
    //cout << "L2Norm(u0)              == " << L2Norm(u0) <<endl;  
    //cout << "L2Dist(u,u0)            == " << L2Dist(u,u0) <<endl;  
    cout << "L2Norm(du)              == " << L2Norm(du) <<endl;
    cout << "L2Dist(du,s1(du))       == " << L2Dist(du,s1(du)) << endl;
    cout << "L2Dist(du,s2(du))       == " << L2Dist(du,s2(du)) << endl;
    cout << "L2Dist(du,s3(du))       == " << L2Dist(du,s3(du)) << endl;
    cout << "L2Norm(G(u))            == " << L2Norm(G_u) <<endl;
    cout << "L2Norm(DG(u) du)        == " << L2Norm(DG_du) <<endl;
    G_u *= -1;
    cout << "L2Dist(DG(u) du, -G(u)) == " << L2Dist(DG_du, G_u) <<endl;
    G_u *= -1;
    ***************************************************/

    du *= damping;
    u += du;

    u.binarySave(outdir + "uiterate" + i2s(newton_step));
    u.binarySave(outdir + "uEQB");
  }
}
      
void G(FlowField& u, FlowField& G_u, Real Reynolds, Real dtarg, Real T, 
       DNSFlags& flags, int& Gcount) {

  const Real nu = 1.0/Reynolds;
  const Real dtmed = 0.03125;
  const Real dtmin = 0.001;
  const Real dtmax = 0.05;
  const Real CFLmin = 0.4;
  const Real CFLmax = 0.6;

  const bool variable_dt = (dtarg == 0.0) ? true : false;
  const Real dtinit = (variable_dt)  ? dtmed : dtarg;
  const Real dT     = (variable_dt) ? lesser(1.0, T) : T;
  TimeStep dt(dtinit, dtmin, dtmax, dT, CFLmin, CFLmax);

  cout << "G: " << flush; 
  G_u = u;
  FlowField p(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b());

  DNS dns(G_u, nu, dt, flags);

  for (Real t=0; t<T; t+=dT) {
    cout << t << ' ' << flush;
    if (variable_dt && dt.adjust(dns.CFL())) {
      cout << "adjusting timestep " << flush;
      dns.reset_dt(dt);
    }
    dns.advance(G_u, p, dt.n());
  }
  cout << endl;
  G_u -= u;
  G_u *= 1/T;

  cout << "G: CFL == " << dns.CFL() <<", # evals == " << ++Gcount << endl;
}

