#include <iostream>
#include <fstream>
#include <iomanip>
#include "channelflow/flowfield.h"
#include "channelflow/dns.h"
#include "utilities.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("convert from F. Waleffe ascii format to a channelflow FlowField");
  ArgList args(argc, argv, purpose);

  const int Nx = args.getint("-Nx", "--Nx", "# x gridpoints in ascii field");
  const int Ny = args.getint("-Ny", "--Ny", "# y gridpoints in ascii field");
  const int Nz = args.getint("-Nz", "--Nz", "# z gridpoints in ascii field");
  const Real alpha = args.getreal("-a", "--alpha","2pi/Lx");
  const Real gamma = args.getreal("-g", "--gamma", "2pi/Lz");
  //const bool dealias = args.getflag("-d", "--dealias", args, help);

  const string ufile = args.getstr(4, "<ufile>", "ascii u.dat file");
  const string vfile = args.getstr(3, "<vfile>", "ascii v.dat file");
  const string wfile = args.getstr(2, "<wfile>", "ascii w.dat file");
  const string uname = args.getstr(1, "<fieldname>", "output file");

  args.check();

  const int NyOdd = Ny + ((Ny+1) % 2);
  const char c = ',';
  cout << "input  Nx,Ny,Nz == " << Nx << c << Ny << c << Nz << endl;
  cout << "output Nx,Ny,Nz == " << Nx << c << NyOdd << c << Nz << endl;
  cout << "alpha, gamma == " << alpha << c << gamma << endl;

  ifstream us(ufile.c_str());
  ifstream vs(vfile.c_str());
  ifstream ws(wfile.c_str());

  const Real Lx=2*pi/alpha;
  const Real a= -1.0;
  const Real b=  1.0;
  const Real Lz=2*pi/gamma;


  FlowField u(Nx,NyOdd,Nz,3,Lx,Lz,a,b,Physical,Spectral);
  for (int ny=0; ny<Ny; ++ny) {
    for (int nz=0; nz<Nz; ++nz) {
      for (int nx=0; nx<Nx; ++nx) {
	us >> u(nx,ny,nz,0);
	vs >> u(nx,ny,nz,1);
	ws >> u(nx,ny,nz,2);
      }
    }
    //if (!us.good() || !vs.good() || !ws.good()) {
    //cerr << "error in reading data files. perhaps N,x,Ny,Nz are wrong.\n";
    //exit(1);
    //}
  } 

  for (int ny=Ny; ny<NyOdd; ++ny)
    for (int nz=0; nz<Nz; ++nz) {
      for (int nx=0; nx<Nx; ++nx) {
	u(nx,ny,nz,0) = 0.0;
	u(nx,ny,nz,1) = 0.0;
	u(nx,ny,nz,2) = 0.0;
      }
    }

  ChebyCoeff U(NyOdd, a,b,Physical);
  Vector y = chebypoints(NyOdd, a, b);
  for (int n=0; n<NyOdd; ++n) 
    U[n] = y[n];
  U.makeSpectral();
  u.makeSpectral();

  u -= U;

  //if (dealias)
  //u.zeroAliasedModes();

  cout << " L2Norm(u) == " << L2Norm(u) << endl;
  cout << "divNorm(u) == " << divNorm(u) << endl;
  cout << " bcNorm(u) == " << bcNorm(u) << endl;

  u.binarySave(uname);
}

