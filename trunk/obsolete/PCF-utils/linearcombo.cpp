#include "channelflow/flowfield.h"
#include "utilities.h"
#include <map>
#include <limits.h>

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[])
{
  string help("\nComputes a linear combination of FlowFields: \n");
  help += "Usage: linearcombo.x -u0 u0.ff -c0 2.0 -u1 u1.ff -c1 1.5 ... -o output.ff\n";
  help += "Would compute u0*2.0 + u1*1.5 + ... and store in output.ff (the default)\n"; 
  help += "If -ci is not specified it is assumed to be 1\n"; 
  help += "Note that valid usage also includes\n";
  help += "linearcombo.x -u0 myfield.ff -u1 myfield.ff to return 2*myfield\n";

  ArgList args(argc, argv, help);

  for(int i = 1; i < argc; ++i)
    {
      string tmp(argv[i]);
      if(tmp == "-h" || tmp == "--help")
	cout << help << endl;
      return 0;
    }
  string uname = "u0";
  string noopt = "";
  int fieldcount = 0;	
  map<string,Real> combos;
  FlowField * prototype = new FlowField();
  
  while(fieldcount < INT_MAX)
    {
      string ui = "-u"+i2s(fieldcount);
      uname = args.getstr(ui, "-"+ui, "flowfield ui of u_rtn += ci*ui" );

      if(uname != noopt)
	{
	  if(fieldcount == 0)
	    {
	      delete prototype;
	      prototype = new FlowField(uname);
	    }
	  string cistr = "-c" + i2s(fieldcount);
	  
	  Real ci = args.getreal(cistr, "-"+cistr, 1.0,
				"multiplicate constant ci of u_rtn += ci*ui");
	  if( (ci == 0) && (cistr.c_str()[0] != '0'))
	    {
	      cerr << "Invalid argument for -c" << fieldcount << " : " << cistr << endl;
	      return 1;
	    }
	  map<string,Real>::iterator found = combos.find(uname);
	  if(found == combos.end())
	    combos.insert( make_pair( uname , ci ) );
	  else
	    found->second += ci;
	}
      else
	break;
      fieldcount++;
    }

  if(fieldcount == 0)
    {
      cerr << help << endl;
      return 1;
    }
		
  const string outfile = args.getstr("-o", "--output", "output flowfield");

  map<string, Real>::iterator it = combos.begin();
  FlowField u(it->first);  u *= it->second; ++it;
	
  for( ; it != combos.end(); ++it)
    {
      FlowField tmp(it->first);
      if(!prototype->congruent(tmp))
	{
	  cerr << combos.begin()->first << " is not compatible with " << it->first << endl;
	  return 2;
	}
      tmp *= it->second;
      u += tmp;
    }
	
  u.binarySave(outfile);	
  delete prototype;
  return 0;
}
