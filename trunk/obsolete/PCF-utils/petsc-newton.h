#ifndef pcfutil_petscnewton_h
#define pcfutil_petscnewton_h

#include "mpi.h"
#include <string>
#include <vector>
#include "channelflow/flowfield.h"
#include "petscsnes.h"
#include "channelflow/dns.h"
#include "channelflow/flowfield.h"


using namespace channelflow;

struct RPO
{
  FlowField * u;
  Real sx;
  Real sz;
  Real T;
  Real Re;
};

class DNSContext 
{
  public:
  DNSContext(std::vector<RealProfileNG>& basis, RPO& rpo, const Real Re, 
	      bool fixSx, bool fixSz, bool fixT,bool fixRe, const Real targetD, std::vector<FieldSymmetry>& symms,
              Real dt, Real dtMin, Real dtMax, Real CFLmin, Real CFLmax,
              Real checkTime, DNSFlags & flags, std::string outdir, std::string label, const Real erel = 1e-8);
  
  void advance(FlowField &u, const Real sx, const Real sz, const Real T, const Real Re);
  Real computeH(const FlowField &u, const FlowField &a);
  void computeDuDt(const FlowField &u, FlowField &dudt);
  void rpo2vec(const RPO& e, Vec &v) const;
  void vec2rpo(const Vec &v, RPO& e);
  void field2vec(FlowField& f, Vec &v) const;
  // This funky declaration is needed so I can 
  // pass it as a function pointer to MatSetShellOperation
  PetscErrorCode jacobianMult(Mat J, Vec x, Vec b);
  void setBase(Vec x);
  int size();
  
  
  
  std::vector<RealProfileNG> basis;
  Real sx;
  Real sz;
  Real T;
  
  bool fixSx;
  bool fixSz;
  bool fixT;
  bool fixRe;

  Real targetD; 
  std::vector<FieldSymmetry> symms;
  Real Re;
    
  DNSFlags flags;
  Real dtGuess; // Initial integration interval to 
  Real dtMin;
  Real dtMax;
  Real CFLmin;
  Real CFLmax;
  Real checkInterval;  // Interval for checking CFL
  


  FlowField x0;
  FlowField x0_x;
  FlowField x0_z;
  FlowField x0_T;
  
  FlowField y0;
  FlowField y0_x;
  FlowField y0_z;
  FlowField y0_T;
  
  
  FlowField tmp;
  FlowField tmp2;
  FlowField p;
  

  std::string outdir;
  std::string label;
  
  Real erel; //Directional derivative relative error
};


void petscSetup(int argc, char**argv);

class PetscNewton
{
  public:
    PetscNewton();
    ~PetscNewton();
//     FlowField findEqba(std::vector<RealProfileNG>& basis, FlowField& u0, Real Re, 
// 		       Real dt, Real dtMin, Real dtMax, Real CFLmin, Real CFLMax, 
// 		       Real T, Real checkTime, DNSFlags& flags);

    
    RPO findInvariant(std::vector<RealProfileNG>& basis, RPO& rpo, const Real Re, 
		      bool fixSx, bool fixSz, bool fixT, bool fixRe, const Real targetD, 
		      std::vector<FieldSymmetry>& symms,
		      Real dt, Real dtMin, Real dtMax, Real CFLmin, Real CFLmax,
		      Real checkTime, DNSFlags & flags, std::string outdir, std::string label);
        
/*    RPO findRPO(std::vector<RealProfileNG>& basis, RPO& rpo, Real Re,
                Real dt, Real dtMin, Real dtMax, Real CFLmin, Real CFLmax, 
                Real checkTime, DNSFlags& flags)*/
    //FlowField findTraj(FlowField &u0, FlowField &uTarg, Real T);
    
   
    PetscErrorCode computeTrajResidual(SNES,Vec,Vec,void*);
    
  private:

    SNES snes;
    

};

//Step size for computing du/dt
#define DUDT_H 1e-7 

PetscErrorCode computeRPOResidual(SNES,Vec,Vec,void*);

PetscErrorCode RPOMonitor(SNES snes,PetscInt its, PetscReal norm,void *mctx);

PetscErrorCode makeJacobian(SNES snes,Vec x,Mat *J,Mat *B,MatStructure *flag,void *ctx);

PetscErrorCode MatMFFDComputeJacobian_wrapper(SNES snes, Vec x, Mat *J, Mat *B, MatStructure *flag, void *ctx);

#endif
