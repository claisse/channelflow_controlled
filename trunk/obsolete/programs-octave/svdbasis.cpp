#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/dbleSVD.h>
#include "channelflow/flowfield.h"
#include "channelflow/diffops.h"
#include "channelflow/symmetry.h"
#include "channelflow/quicksort.h"
#include "channelflow/utilfuncs.h"

using namespace std;
using namespace channelflow;

int main(int argc, char* argv[]) {

  string purpose("construct a SVD / POD basis from a series of flowfields");
  ArgList args(argc, argv, purpose);

  const Real T0        = args.getreal("-T0", "--T0", 0,   "start time");
  const Real T1        = args.getreal("-T1", "--T1", 500, "end time");
  /***/ Real dT        = args.getreal("-dT", "--dT", 1.0, "save interval");
  const bool adjustdT  = args.getflag("-adT", "--adjustdT", "adjust dT to evenly divide (T1-T0)");
  const bool orbit     = args.getflag("-orb", "--orbit",  "don't include last timestep == first");
  const int    Nbasis  = args.getint ("-Nb", "--Nbasis", 10, "number of basis fields");
  const bool   demean  = args.getflag("-mc", "--meancentered", "do mean-centered SVD");
  const string origin  = args.getstr ("-org", "--origin", "",   "do SVD centered on this field");
  const string odir    = args.getpath("-o", "--outdir", "./svdbasis", "ouput directory for basis elements");
  const string udir    = args.getpath("-d", "--datadir", "data/", "flowfield series directory");
  const string ulabel  = args.getstr ("-ul", "--ulabel", "u",  "flowfield filename label");
  const string blabel  = "e"; //args.getstr("-bl", "--basislabel", "e", "basis label");

  args.check();
  if (demean && origin.length() != 0) {
    cerr << "error: -mc/--meancentered and -or/--origin are mutually exclusive options" << endl;
    cerr << "please chose one and try again." << endl;
    exit(1);
  }
  args.save("./");
  mkdir(odir);

  if (adjustdT) 
    dT = (T1 - T0)/Greater(iround((T1-T0)/dT), 1);

  const bool inttime  = (abs(dT - int(dT)) < 1e-12) ? true : false;
  const bool centered = (demean || (origin.length() != 0)) ? true : false;
  const int Ndata = orbit ? iround((T1-T0)/dT) : iround((T1-T0)/dT) + 1;
  const string us = udir + ulabel;

  Matrix R(Ndata,Ndata);

  array<FlowField> u(Ndata);

  cout << "loading u[i] data...";
  for (int i=0; i<Ndata; ++i) {
    cout << T0 + i*dT << ' ' << flush;
    u[i] = FlowField(us + t2s(T0 + i*dT, inttime));
  }
  cout << endl;

  FlowField uorigin;
  if (origin.length() != 0)
    uorigin = FlowField(origin);
  else if (demean) {
    cout << "Calculating mean field..." << flush;
    uorigin = u[0];
    for (int i=1; i<Ndata; ++i) {
      cout << i << ' ' << flush;
      uorigin += u[i];
    }
    uorigin *= 1.0/Ndata;
    cout << "done" << endl;
  }

  if (centered) {
    cout << "Subtracting " << (demean ? "umean" : "origin") << " from u[i]..." << flush;
    for (int i=0; i<Ndata; ++i) {
      cout << i << ' ' << flush;
      u[i] -= uorigin;
    }
    cout << endl;
  }

  cout << "Calculating correlations..." << flush;
  for (int i=0; i<Ndata; ++i) {
    cout << i << ' ' << flush;
    for (int j=i; j<Ndata; ++j) {
      Real ip = L2InnerProduct(u[i],u[j]);
      R(i,j) = ip/Ndata;
      R(j,i) = ip/Ndata;
    }
  }
  cout << endl << "doing SVD..." << flush;

  SVD svd(R);
  DiagMatrix sigma2 = svd.singular_values();
  Matrix V = svd.right_singular_matrix();
  cout << "done" << endl;

  cout << "Allocating ei array..." << flush;
  array<FlowField> e(Nbasis);
  cout << "done" << endl;

  FlowField zero = u[0];
  FlowField utmp = u[0];
  zero.setToZero();
  utmp.setToZero();

  for (int k=0; k<Nbasis; ++k) {
    cout << "building e" << k << endl;
    e[k] = zero;
    for (int j=0; j<Ndata; ++j) {
      utmp = u[j];
      utmp *= V(j,k);
      e[k] += utmp;
    }
    e[k] *= 1.0/L2Norm(e[k]); // sigma_k;
    e[k].save(odir + "e" + i2s(k));
  }

  if (demean)
    uorigin.save(odir + "umean");
  if (centered)
    uorigin.save(odir + "uorigin");

  ofstream sos((odir + "sigma.asc").c_str());
  sos << setprecision(17);
  for (int n=0; n<Ndata; ++n)
    sos << sqrt(sigma2(n,n)) << '\n';

  /*********************************
  Matrix IP(Nbasis, Nbasis);

  for (int m=0; m<Nbasis; ++m) {
    cout << "doing IP row " << m << endl;
    for (int n=0; n<=m; ++n) {
      Real ip = L2InnerProduct(e[m], e[n]);
      IP(m,n) = ip;
      IP(n,m) = ip;
    }
    msave(IP, "IP");
  }
  ******************************/
}


void msave(const Matrix& R, const string& filebase) {
  string filename = filebase + ".asc";
  ofstream os(filename.c_str());
  os << setprecision(17);
  for (int m=0; m<R.rows(); ++m) {
    for (int n=0; n<R.cols(); ++n)
      os << R(m,n) << ' ';
    os << '\n';
  }
}

void msave(const DiagMatrix& D, const string& filebase) {
  string filename = filebase + ".asc";
  ofstream os(filename.c_str());
  os << setprecision(17);
  for (int m=0; m<D.rows(); ++m)
    os << D(m,m) << '\n';
}
