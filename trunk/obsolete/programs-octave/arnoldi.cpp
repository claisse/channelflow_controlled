#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <octave/oct.h>
#include <octave/dColVector.h>
#include <octave/dMatrix.h>
#include <octave/EIG.h>
#include "channelflow/flowfield.h"
#include "channelflow/vector.h"
#include "channelflow/chebyshev.h"
#include "channelflow/tausolver.h"
#include "channelflow/dns.h"
#include "channelflow/symmetry.h"

#include "channelflow/quicksort.h"
#include "octutils.h"

using namespace std;
using namespace channelflow;

// This program calculates eigenvalues of fixed point of plane Couette flow
// using Arnoldi iteration. The ideas and algorithm are based on Divakar
// Viswanath, "Recurrent motions within plane Couette turbulence",
// J. Fluid Mech.</em> 580 (2007), http://arxiv.org/abs/physics/0604062.
// Notation and details of the algorithm are based on Lecture 33 of
// "Numerical Linear Algebra" by Trefethen and Bau. Production runs began
// in early December 2006.

// Arnoldi iteration estimates the eigenvalues of a matrix A by iteratively
// constructing a QR decomposition of a matrix whose columns are
// Ab, A^2 b, etc., where b is an arbitrary starting vector.

// For the present problem, A is a discretized form of PCF dynamics,
// linearized about the equilibrium solutions, and b is the discretized
// form of an arbitrary starting velocity field.

// In continuous terms, the matrix A corresponds to a differential operator
// L and b to a perturbation du from an invariant solution u* of Navier-Stokes. 
//
// Let f^T be the time-T Navier-Stokes map 
//   f^T : u(t) -> u(t+T), 
//
// Let u*,T,sigma be an equilibrium solution of f (e.g. a relative periodic orbit)
//   u* = sigma f^T(u*). 
//
// Define the (approximately) linear operator L as
//  L du = sigma f^T(u* + du) - sigma f^T(u*) =  sigma f^T(u* + du) - u*
//
// This program computes L du, L^2 du , L^3 du, etc. with a DNS algorithm
// and translates between velocity fields du^n = L^n du and vectors
// v^n = A^n b via by choosing a set of linearly independent spectral 
// coefficients from the expansion for du.

// f^T(u)  = time-T DNS integration of u,

// sf^T(u) = sigma f^T(u), where sigma is a symmetry of the flow


void sf(const FlowField& u, Real T, const FieldSymmetry& sigma, FlowField& sfu, 
	Real Reynolds, DNSFlags& flags, TimeStep& dt, int& fcount);

void Dsf(const FlowField& u, const FlowField& sfu, const FlowField& du, FlowField& Df_du,
	 Real T, const FieldSymmetry& sigma, Real Reynolds, DNSFlags& flags,
	 TimeStep& dt, Real eps, bool centerdiff, int& fcount);

void checkConjugacy(const ComplexColumnVector& u, const ComplexColumnVector& v);

void projectout(FlowField& u, const FlowField& e); // remove from u component along e direction

const Real EPS_stability = 1e-06;

int main(int argc, char* argv[]) {

  string purpose("compute eigenvalues of EQB, TW, or periodic orbit using Arnoldi iteration");
  ArgList args(argc, argv, purpose);

  const Real Reynolds = args.getreal("-R", "--Reynolds", 400,  "pseudo-Reynolds number == 1/nu");
  const Real nuarg    = args.getreal("-nu", "--nu",       0,  "kinematic viscosity (takes precedence over Reynolds, if nonzero)");
  const Real T        = args.getreal("-T",  "--maptime", 20.0, "integration time for Navier-Stokes.");
  const bool poincare = args.getflag("-poinc",  "--poincare", "use I-D==0 Poincare section as stopping condition for time integration");
  const string sigstr   = args.getstr ("-sigma", "--sigma", "",       "file containing symmetry of relative solution (default == identity)");
  const string usymmstr = args.getstr ("-usymms","--usymmetries", "", "constrain the solution u to invariant symmetric subspace, argument is the filename for a file listing the generators of the symmetry group");
  const string dusymmstr = args.getstr ("-dusymms","--dusymmetries", "", "constrain perturbation du(t) to a set of symmetries, argument is the filename for a file listing the generators of the symmetry group");
  const string initstr  = args.getstr ("-is", "--initstepping", "smrk2", "timestepping algorithm for initializing multistep algorithms, "
					" one of [cnfe1 cnrk2 smrk2 sbdf1]");
  const string stepstr  = args.getstr ("-ts", "--timestepping", "sbdf3", "timestepping algorithm, one of [cnfe1 cnab2 smrk2 sbdf1 sbdf2 sbdf3 sbdf4]");
  const string nonlstr  = args.getstr ("-nl", "--nonlinearity", "rot", "method of calculating nonlinearity, one of [rot conv div skew alt]");

  const string meanstr= args.getstr ("-mc", "--meanconstraint", "gradp", "gradp|bulkv : hold one fixed");
  const Real dPdx     = args.getreal("-dPdx", "--dPdx",   0.0, "imposed pressure gradient, x-dir");
  const Real dPdz     = args.getreal("-dPdz", "--dPdx",   0.0, "imposed pressure gradient, z-dir");
  const Real Ubulk    = args.getreal("-Ubulk", "--Ubulk",  0.0, "imposed bulk u velocity");
  const Real Wbulk    = args.getreal("-Wbulk", "--Wbulk",  0.0, "imposed bulk w velocity");
  const Real Uwall    = args.getreal("-Uwall", "--Uwall",  1.0, "magnitude of imposed wall velocity, +/-Uwall at y = +/-h");
  const Real theta    = args.getreal("-theta", "--theta",  0.0, "angle of wall velocity to x axis");

  const int  Narnoldi = args.getint ("-Na",  "--Narnoldi", 100, "number of Arnoldi iterations");
  const int  Nstable  = args.getint ("-Ns",  "--Nstable",  5,   "save all unstable,marginal eigfuncs plus Ns stable eigfuncs to save");
  const bool fixedNs  = args.getflag("-fNs", "--fixedNsave",    "save fixed number of eigfuncs: unstable,marginal,eigfuncs");
  const int  saveint  = args.getint ("-si",  "--saveinterval",  20,   "save eigfuncs every nth iteration");

  const Real EPS_arn  = args.getreal("-ek", "--epsKrylov", 1e-10, "min. condition # of Krylov vectors");
  const Real EPS_du   = args.getreal("-edu","--epsdu", 1e-7, "magnitude of Arnoldi perturbation");
  const bool centdiff = args.getflag("-cd", "--centerdiff", "centered differencing to estimate differentials");
  const int  seed     = args.getint ("-sd",  "--seed",       1, "seed for random number generator");
  const Real smooth   = args.getreal("-s",  "--smoothness", 0.4, "smoothness of initial perturb, 0 < s < 1");
  const bool orthochk = args.getflag("-oc", "--orthocheck",  "save Q' * Q into QtQ.asc at every iteration");

  const bool vardt     = args.getflag("-vdt","--variableDt", "adjust dt to keep CFLmin<=CFL<CFLmax");
  const Real dtarg     = args.getreal("-dt","--timestep",  0.03125, "(initial) integration timestep");
  const Real dtmin     = args.getreal("-dtmin", "--dtmin", 0.001, "minimum timestep");
  const Real dtmax     = args.getreal("-dtmax", "--dtmax", dtarg,  "maximum timestep");
  const Real dTCFL     = args.getreal("-dTCFL", "--dTCFL", 1.00,  "check CFL # at this interval");
  const Real CFLmin    = args.getreal("-CFLmin", "--CFLmin", 0.50, "minimum CFL number");
  const Real CFLmax    = args.getreal("-CFLmax", "--CFLmax", 0.65, "maximum CFL number");

  const string duname = args.getstr ("-du", "--perturb", "", "initial perturbation field, random if unset");
  const string outdir = args.getpath("-o", "--outdir", "./", "output directory");

  const string uname  = args.getstr(1, "<flowfield>", "filename for EQB, TW, or PO solution");

  args.check();
  args.save("./");
  mkdir(outdir);

  //if (int(eqb) + int(orb) != 1)
  //cferror("please choose either -orb or -eqb flag");

  srand48(seed);
  //const Real nu = 1.0/Reynolds;
  const Real decay = 1.0-smooth;
  const int prec = 16;

  PoincareCondition* h = poincare ? new DragDissipation() : 0;
  if (h) 
    cout << "Using Poincare section as time-integration stopping condition" << endl;

  TimeStep dt(dtarg, dtmin, dtmax, dTCFL, CFLmin, CFLmax, vardt);

  FlowField u(uname); // u*, the solution of sigma f^T(u*) - u* = 0

  //project(dnsflags.symmetries, u, "initial field u", cout);
  //fixdivnoslip(u);

  const int Nx = u.Nx();
  const int Ny = u.Ny();
  const int Nz = u.Nz();

  const Real Lx = u.Lx();
  const Real ya = u.a();
  const Real yb = u.b();
  const Real Lz = u.Lz();

  /*******************************************************************
  const int kxmin = -u.kxmaxDealiased();
  const int kxmax =  u.kxmaxDealiased();
  const int kzmin =  0;
  const int kzmax =  u.kzmaxDealiased();

  cout << setprecision(17);
  cout << "   Nx == " << Nx << endl;
  cout << "   Ny == " << Ny << endl;
  cout << "   Nz == " << Nz << endl;
  cout << "kxmin == " << kxmin << endl;
  cout << "kxmax == " << kxmax << endl;
  cout << "kzmin == " << kzmin << endl;
  cout << "kzmax == " << kzmax << endl;
  //cout << "building basis (this takes a long time)..." << endl;
  // Used to communicate timestep parameters to integration functions.
  cout << "dt     == " << dtarg << endl;
  cout << "dtmin  == " << dtmin << endl;
  cout << "dtmax  == " << dtmax << endl;
  cout << "CFLmin == " << CFLmin << endl;
  cout << "CFLmax == " << CFLmax << endl;
  *********************************************************************/


  if (!poincare)
    dt.adjust_for_T(T);

  // Construct time-stepping algorithm
  DNSFlags dnsflags;
  dnsflags.dealiasing   = DealiasXZ;
  dnsflags.initstepping = s2stepmethod(initstr);
  dnsflags.timestepping = s2stepmethod(stepstr);
  dnsflags.nonlinearity = s2nonlmethod(nonlstr);
  dnsflags.constraint   = s2constraint(meanstr);
  dnsflags.baseflow     = LaminarBase; // s2baseflow(basestr);
  dnsflags.ulowerwall   = -Uwall*cos(theta);
  dnsflags.uupperwall   =  Uwall*cos(theta);
  dnsflags.wlowerwall   = -Uwall*sin(theta);
  dnsflags.wupperwall   =  Uwall*sin(theta);
  dnsflags.nu           = (nuarg != 0) ? nuarg : 1.0/Reynolds;
  dnsflags.dPdx         = dPdx;
  dnsflags.dPdz         = dPdz;
  dnsflags.Ubulk        = Ubulk;
  dnsflags.Wbulk        = Wbulk;
  dnsflags.t0           = 0.0;
  dnsflags.dt           = dt;
  dnsflags.verbosity    = Silent;

  FieldSymmetry sigma;       // defaults to identity
  if (sigstr.length() != 0)
    sigma = FieldSymmetry(sigstr);

  SymmetryList usymmetries;
  if (usymmstr.length() > 0) {
    usymmetries = SymmetryList(usymmstr);    
    cout << "Invariant solution u symmetry group " << endl;
    cout << usymmetries << endl;
    
    cout << "Projecting u onto u symmetries..." << endl;
    project(usymmetries, u, "invariant solution u", cout);
    //cout << "L2Dist(u, usymm)/L2Norm(u) == " << L2Dist(u, utmp)/L2Norm(u) << endl;
    //u = utmp;
  }

  SymmetryList dusymmetries;
  if (dusymmstr.length() > 0) {
    dusymmetries = SymmetryList(dusymmstr);
    cout << "Eigenfunction du symmetry group" << endl;
    cout << dusymmetries << endl;
  }

  cout << "arnoldi dnsflags == " << dnsflags << endl;

  int fcount = 0;
  Real CFL = 0.0;
  const Real eps = EPS_du/L2Norm(u);

  /****************************************************************************************
  cerr << "Testing sigma f^T(u) - u == 0 using G(u,sigma,T) == sigma f^T(u) - u" << endl;
  bool Tnormalize = false;
  bool Unormalize = false;
  Real Ttmp = T; // need nonconst T since T varies for poincare section seraches
  
  FlowField Gu;
  cout << setprecision(16);
  cout << " L2Norm(u) == " << L2Norm(u) << endl;
  cout << "         T == " << Ttmp << endl;
  cout << "         h == " << (h ? 1 : 0) << endl;
  cout << "     sigma == " << sigma << endl;
  cout << "  dnsflags == " << dnsflags << endl;
  cout << "        dt == " << dt << endl;
  cout << "Tnormalize == " << Tnormalize << endl;
  cout << "Unormalize == " << Unormalize << endl;
  cout << "    fcount == " << fcount << endl;
  cout << "       CFL == " << CFL << endl;

  Gp(u,Ttmp,h,sigma, Gu, dnsflags, dt, Tnormalize, Unormalize, fcount, CFL, cout);
  cout << "L2Norm(G(u)) == " << L2Norm(Gu) << endl; 
  *********************************************************************************************/

  // Calculate sfu = sigma f^T u. 
  cout << "computing sigma f^T(u)..." << endl;
  FlowField sfu(u); 
  dnsflags.symmetries = usymmetries;     // enforce usymmetries durign time-integration of u
  sfp(u, T, h, sigma, sfu, dnsflags, dt, fcount, CFL, cout);
  dnsflags.symmetries = SymmetryList();  // turn off usymmetry enforcement for upcoming du integration

  // Should be no need for this if symmetries are enforced during time integration. 
  if (usymmetries.length() != 0) {
    cout << "Projecting sfu = sigma F^T(u) onto u symmetries..." << endl;
    project(dnsflags.symmetries, u, "sigma f^T(u)", cout);
    //FlowField utmp = project(usymmetries, sfu);
    //cout << "L2Dist(sfu, sfusymm)/L2Norm(sfu) == " << L2Dist(sfu, utmp)/L2Norm(sfu) << endl;
    //sfu = utmp;
  }
  
  cout << "\nCFL == " << CFL << endl;

  FlowField du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb);
  FlowField u_du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb);     // u* + du
  FlowField sfu_du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb);   // sigma f^T(u*+du)
  FlowField Dsf_du(Nx, Ny, Nz, 3, Lx, Lz, ya, yb);   // sigma f^T(u*+du) - sigma f(u*)

  // Double-check that u,sigma,T are a solution of sigma f^T(u) = u, using du as tmp
  du = sfu;
  du -= u;
  cout << "L2Norm(sigma f^T(u) - u) == " << L2Norm(du) << endl;

  if (L2Norm(du) > 1e-08) {
    cerr << "WARNING: that error seems kinda large. Are you sure you have the solution and parameters right?" << endl;
  }


  du.setToZero();

  // Set du  = EPS_du (random unit perturbation, "b" in Arnoldi A*b terms)
  if (duname.length() == 0) {
    //du.addPerturbations(kxmax, kzmax, 1.0, decay);
    //du.addPerturbation(0, 0, 1.0, decay);
    cout << "Constructing du..." << endl;
    
    bool meanflow_perturb = true;
    du.addPerturbations(u.kxmaxDealiased(), u.kzmaxDealiased(), 1.0, decay, meanflow_perturb);

    ChebyCoeff du00 =  Re(du.profile(0,0,0));
    ChebyCoeff dw00 =  Re(du.profile(0,0,2));

    if (dnsflags.constraint == PressureGradient) {
      // Modify du so that (du/dy|a + du/dy|b) == (dw/dy|a + dw/dy|b) == 0
      // i.e. mean wall shear == 0
      Real h =  (du.b() - du.a())/2;
      ChebyCoeff du00y = diff(du00);
      ChebyCoeff dw00y = diff(dw00);
      Real duy =  (du00y.eval_a() + du00y.eval_b())/2;
      Real dwy =  (dw00y.eval_a() + dw00y.eval_b())/2;

      cout << "Modifying du so that it doesn't change mean pressure balance..." << endl;
      cout << "pre-mod : " << endl;
      cout << "(duya + duyb)/2 == " << (du00y.eval_a() + du00y.eval_b())/2 << endl;
      cout << "(dwya + dwyb)/2 == " << (dw00y.eval_a() + dw00y.eval_b())/2 << endl;
      du.cmplx(0,1,0,0) -= h*Complex(duy,0); // modify coeff of 1st chebyshev fucntion T_1(y/h) = y/h for u
      du.cmplx(0,1,0,2) -= h*Complex(dwy,0); // modify coeff of 1st chebyshev fucntion T_1(y/h) = y/h for w

      du00 =  Re(du.profile(0,0,0));
      dw00 =  Re(du.profile(0,0,2));
      du00y = diff(du00);
      dw00y = diff(dw00);
      cout << "post-mod : " << endl;
      cout << "(duya + duyb)/2 == " << (du00y.eval_a() + du00y.eval_b())/2 << endl;
      cout << "(dwya + dwyb)/2 == " << (dw00y.eval_a() + dw00y.eval_b())/2 << endl;
    }
    else { // (dnsflags.constraint == BulkVelocity)
      // modify du to have zero mean value
      cout << "Modifying du so that it doesn't change mean flow..." << endl;
      Real umean = du00.mean();
      Real wmean = dw00.mean();
      cout << "pre-mod : " << endl;
      cout << "u mean == " << du00.mean() << endl;
      cout << "w mean == " << dw00.mean() << endl;
      du.cmplx(0,0,0,0) -= Complex(umean,0); // modify coeff of 0th chebyshev fucntion T_0(y/h) = 1 for u
      du.cmplx(0,0,0,2) -= Complex(wmean,0); // modify coeff of 0th chebyshev fucntion T_0(y/h) = 1 for w
      du00 =  Re(du.profile(0,0,0));
      dw00 =  Re(du.profile(0,0,2));
      cout << "post-mod : " << endl;
      cout << "u mean == " << du00.mean() << endl;
      cout << "w mean == " << dw00.mean() << endl;
    }
  }
  else
    du = FlowField(duname);

  
  if (dusymmetries.length() != 0) {
    cout << "Projecting du onto du symmetries..." << endl;
    FlowField dusymm = project(dusymmetries, du);
    cout << "L2Dist(du, dusymm)/L2Norm(du) == " << L2Dist(du, dusymm)/L2Norm(du) << endl;
    du = dusymm;
  }

  cout << "L2Norm(du) == " << L2Norm(du) << endl;
  cout << "rescaling du by eps_du == " << EPS_du << endl;
  du *= EPS_du/L2Norm(du);
  cout << "L2Norm(du) == " << L2Norm(du) << endl;


  vector<RealProfileNG> basis;

  // Project field du onto basis, producing vector b
  ColumnVector b; //(M);
  field2vector(du,b, basis);
  //field2vector(du,b);
  b *= 1.0/L2Norm(b);

  Arnoldi arnoldi(b, Narnoldi, EPS_arn);

  // Initialize output streams
  ofstream osLamconv((outdir + "Lamconv.asc").c_str());
  ofstream oslamconv;

  oslamconv.open((outdir + "lamconv.asc").c_str());
  oslamconv << setprecision(17);

  

  for (int n=0; n<Narnoldi; ++n) {
    cout << "======================================================" << endl;
    cout << n << "th iteration:" << endl;

    // Compute v = Aq in Arnoldi iteration terms, where q test vector
    // In fluid terms, this is
    //      L du == F^T(u + du) - F^T(u)
    // where |du| << |u|, so that RHS expression is approximately linear
    // and where f^T is the forward map for time T of Navier-Stokes

    // Relation of v and du :
    //   du == eps * v_m phi_m
    //    v == 1/eps (du, phi_m)

    const ColumnVector& q = arnoldi.testVector();

    cout << "building field du..." << endl;
    vector2field(q, du, basis);

    if (dusymmetries.length() != 0) {
      cout << "Projecting du onto du symmetries..." << endl;
      FlowField dusymm = project(dusymmetries, du);
      cout << "L2Dist(du, dusymm)/L2Norm(du) == " << L2Dist(du, dusymm)/L2Norm(du) << endl;
      du = dusymm;
    }


    cout << "computing Df du..." << endl;
    // Compute Dsf du == 1/eps (sigma f(u + eps du) - sigma f(u)) or centerdiff equiv
    //Dsf(u, T, sigma, sfu, du, Dsf_du, Reynolds, dnsflags, dt, eps, centdiff, fcount, CFL, cout);
    Dsfp(u, T, h, sigma, sfu, du, Dsf_du, dnsflags, dt, eps, centdiff, fcount, CFL, cout);

    if (dusymmetries.length() != 0) {
      cout << "Projecting Df du onto du symmetries..." << endl;
      FlowField dusymm = project(dusymmetries, Dsf_du);
      cout << "L2Dist(Df du, P(Df du)))/L2Norm(Df du) == " << L2Dist(Dsf_du, dusymm)/L2Norm(Dsf_du) << endl;
      Dsf_du = dusymm;
    }

    cout << "L2Norm(du)     == " << L2Norm(du) << endl;
    cout << "L2Norm(Dsf_du) == " << L2Norm(Dsf_du) << endl;

    cout << "reexpressing Df du as column vector..." << endl;
    ColumnVector Aq; //(M);
    field2vector(Dsf_du, Aq, basis);

    // Send the product Aq back to Arnoldi
    cout << "computing new orthogonal Krylov subspace basis vector..." << endl;
    arnoldi.iterate(Aq);

    cout << "results..." << endl;
    // Now do some output: Lambda, the eigenvalues of map f^T,
    const ComplexColumnVector& Lambda = arnoldi.ew();
    /***/ ComplexColumnVector lambda(Lambda.length());

    int Nnonstable = 0; 
    for (int j=0; j<=n; ++j) {
      lambda(j) = (1.0/T) * log(Lambda(j));
      if (Re(lambda(j)) >= -EPS_stability) 
	++Nnonstable;
    } 
    save(Lambda, outdir + "Lambda");
    save(lambda, outdir + "lambda");

    //const ComplexMatrix& Vn = arnoldi.ev();
    // save either Nstable or 
    int Nsave = lesser(Lambda.length(), (fixedNs) ?  Nstable : Nnonstable + Nstable);
  
    cout << "\nabs, arg Lambda == " << endl;
    for (int j=0; j<Nsave; ++j) 
      cout << setw(prec+7) << abs(Lambda(j)) << setw(prec+7) << arg(Lambda(j)) << endl;
    cout << "lambda = 1/T log(Lambda) == " << endl;
    for (int j=0; j<Nsave; ++j)
      cout << setw(prec+7) << Re(lambda(j)) << setw(prec+7) << Im(lambda(j)) << endl;


    // Save eigenfunctions
    if (n % saveint == 0 || n == Narnoldi-1 ) {

      const ComplexMatrix& Vn = arnoldi.ev();  
      const ComplexColumnVector& Lambda = arnoldi.ew();
      /***/ ComplexColumnVector lambda(Lambda.length());

      int Nnonstable = 0;
      for (int j=0; j<lambda.length(); ++j) {
	lambda(j) = (1.0/T) * log(Lambda(j));

	// Count the nonstable (marginal or unstable) eigvals
	if (Re(lambda(j)) >= -EPS_stability) 
	  ++Nnonstable;
      }

      int Nsave = lesser(Lambda.length(), (fixedNs) ?  Nstable : Nnonstable + Nstable);

      // Reconstruct and save leading eigenfunctions
      for (int j=0; j<lesser(Nsave,n+1); ++j) {

	string sj1 = i2s(j+1);
	string sj2 = i2s(j+2);
	
	// Real-valued eigenvalue
	if (Im(Lambda(j)) == 0.0 || Im(Lambda(j)) == pi) {
	  FlowField ef(u); // set ef to have same size as u
	  ColumnVector ev = real(Vn.column(j));
	  //vector2field(ev, ef);
	  vector2field(ev, ef, basis);

	  if (dusymmetries.length() != 0) {
	    cout << "Projecting ef onto du symmetries..." << endl;
	    FlowField efsymm = project(dusymmetries, ef);
	    cout << "L2Dist(ef, efsymm)/L2Norm(ef) == " << L2Dist(ef, efsymm)/L2Norm(ef) << endl;
	    ef = efsymm;
	  }

	  ef *= 1.0/L2Norm(ef);
	  ef.save(outdir + "ef" + sj1);
	}

	// Complex eigenvalue pair
	else if (j+1 < Lambda.length()) {
	  Complex LambdaA = Lambda(j);
	  Complex LambdaB = Lambda(j+1);
	  if (LambdaA != conj(LambdaB)) {
	    cerr << "Warning! Non-conjugate complex eigenvalues!" << endl;
	    cerr << "abs, arg Lambda(" << sj1 << ") == "
		 << setw(prec+7) << abs(LambdaA) << ' '
		 << setw(prec+7) << arg(LambdaA) << endl;
	    cerr << "abs, arg Lambda(" << sj2 << ") == "
		 << setw(prec+7) << abs(LambdaB)
		 << setw(prec+7) << arg(LambdaB) << endl;
	  }
	  checkConjugacy(Vn.column(j), Vn.column(j+1));

	  ColumnVector evA = real(Vn.column(j));
	  ColumnVector evB = imag(Vn.column(j));
	  FlowField efA(u); 
	  FlowField efB(u); 

	  vector2field(evA, efA, basis);
	  vector2field(evB, efB, basis);

	  if (dusymmetries.length() != 0) {
	    cout << "Projecting efA, efB onto du symmetries..." << endl;
	    FlowField efsymm = project(dusymmetries, efA);
	    cout << "L2Dist(efA, efAsymm)/L2Norm(efA) == " << L2Dist(efA, efsymm)/L2Norm(efA) << endl;
	    efA = efsymm;

	    efsymm = project(dusymmetries, efB);
	    cout << "L2Dist(efB, efBsymm)/L2Norm(efB) == " << L2Dist(efB, efsymm)/L2Norm(efB) << endl;
	    efB = efsymm;
	  }

	  Real c = 1.0/sqrt(L2Norm2(efA) + L2Norm2(efB));
	  efA *= c;
	  efB *= c;
	  efA.save(outdir + "ef" + sj1);
	  efB.save(outdir + "ef" + sj2);

	  ++j; // have already computed, saved ef(j+1)
	}
      }
    }
    if (orthochk)
      arnoldi.orthocheck();
  }
}

// f^T(u) = time-T DNS integration of u
void sf(const FlowField& u, Real T, const FieldSymmetry& sigma, FlowField& sfu, 
	Real Reynolds, DNSFlags& flags, TimeStep& dt, int& fcount) {

  if (T<0) {
    cerr << "sf: negative integration time T == " << T << endl;
    cerr << "returning sigma f(u,T) == sigma u" << endl;
    sfu = u;
    sfu *= sigma;
    return;
  }

  // Special case #1: no time steps
  if (T == 0) {
    cout << "sf: T==0, no integration, returning" << endl;
    sfu = u;
    sfu *= sigma;
    return;
  }

  //cout << "sf(u,T) : L2Norm(u) == " << L2Norm(u) << ",  T == " << T << flush;

  FlowField p(u.Nx(), u.Ny(), u.Nz(), 1, u.Lx(), u.Lz(), u.a(), u.b());
  sfu = u;

  // Adjust dt for CFL if necessary
  DNS dns(sfu, flags);
  if (dt.variable()) {
    dns.advance(sfu, p, 1);
    dt.adjust(dns.CFL());
    dns.reset_dt(dt);
    sfu = u;
  }

  //cout << ", CFL == " << dns.CFL() << endl;
  //cout << "t == " << flush;

  //  t == current time in integration
  //  T == total integration time
  // dT == CFL-check interval
  // dt == DNS time-step
  //  N == T/dT, n == dT/dt;
  //  T == N dT, dT == n dt
  //  t == s dT (s is loop index)
  cout << "f(u,t) : t == " << flush;
  for (int s=1; s<=dt.N(); ++s) {
    dns.advance(sfu, p, dt.n());
    Real t = s*dt.dT();
    if (s % 10 == 0)
      cout << iround(t) << flush;
    else if (s % 2 == 0)
      cout << '.' << flush;
    if (dt.adjust(dns.CFL()))
      dns.reset_dt(dt);
  }
  sfu *= sigma;

  cout << "  CFL == " << dns.CFL() << endl;
  ++fcount;
}

void Dsf(const FlowField& u, const FlowField& sfu, const FlowField& du, FlowField& Dsf_du,
	Real T, const FieldSymmetry& sigma, Real Reynolds, DNSFlags& flags,
	TimeStep& dt, Real eps, bool centerdiff, int& fcount) {

  FlowField u_du(du);
  FlowField sfu_du(du);

  if (!centerdiff) {

    cout << "Computing f^T(u + du) for approximation L du == f^T(u + du) - f^T(u)" << endl;
    u_du  = du;
    u_du *= eps;     cout << "L2Norm(du)               == " << L2Norm(du) << endl;
    u_du += u;       cout << "L2Norm(u_du)             == " << L2Norm(u_du) << endl;


    sfu_du = u_du;
    sf(u_du, T, sigma, sfu_du, Reynolds, flags, dt, fcount);

    Dsf_du = sfu_du;
    Dsf_du -= sfu;

    cout << "\n";
    cout << "L2Norm(sf(u))            == " << L2Norm(sfu) << endl;
    cout << "L2Norm(sf(u + du))       == " << L2Norm(sfu_du) << endl;
    cout << "L2Norm(sf(u + du)-sf(u)) == " << L2Norm(Dsf_du) << endl;

    Dsf_du *= 1.0/eps;
  }
  else {

    cout << "Computing f^T(u +/- du/2) for approximation L du == f^T(u + du/2) - f^T(u - du/2)" << endl;

    // Compute f^T(u+du)
    u_du  = du;
    u_du *= eps/2;
    u_du += u;

    sfu_du = u_du;
    sf(u_du, T, sigma, sfu_du, Reynolds, flags, dt, fcount);
    //sfu_du *= sigma;
    Dsf_du = sfu_du;

    // Compute f^T(u-du)
    u_du  = du;
    u_du *= -eps/2;
    u_du += u;

    sfu_du = u_du;
    sf(u_du, T, sigma, sfu_du, Reynolds, flags, dt, fcount);
    //sfu_du *= sigma;
    Dsf_du -= sfu_du;

    // Complete approximation L du == sigma f^T(u + du/2) - sigma f^T(u - du/2)
    Dsf_du *= 1.0/eps;
  }
}


void checkConjugacy(const ComplexColumnVector& u, const ComplexColumnVector& v) {
  ColumnVector f = real(u);
  ColumnVector g = real(v);
  Real rerr = L2Dist(f,g);
  f = imag(u);
  g = imag(v);
  g *= -1;
  Real ierr = L2Dist(f,g);
  if (rerr + ierr > 1e-14*(L2Norm(u)+L2Norm(v))) {
    cout << "error : nonconjugate u,v, saving these vectors" << endl;
    save(u, "uerr");
    save(v, "verr");
  }
}

void projectout(FlowField& u, const FlowField& e) {
    FlowField utmp;
    Real c;

    // Remove component of du in x-translation direction
    c = L2IP(u, e);
    utmp = e;
    utmp *= c;
    u -= utmp;
}



